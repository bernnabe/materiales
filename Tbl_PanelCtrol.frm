VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Tbl_PanelCtrol 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Panel de Control"
   ClientHeight    =   5760
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7710
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5760
   ScaleWidth      =   7710
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Conta 
      Enabled         =   0   'False
      Height          =   3735
      Left            =   480
      TabIndex        =   27
      Top             =   600
      Width           =   6735
      Begin VB.ComboBox Mercaderia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   54
         Top             =   720
         Width           =   5055
      End
      Begin VB.ComboBox Impuestos 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   52
         Top             =   3240
         Width           =   5055
      End
      Begin VB.ComboBox Cred_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   51
         Top             =   2880
         Width           =   5055
      End
      Begin VB.ComboBox Deb_STA 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   50
         Top             =   2520
         Width           =   5055
      End
      Begin VB.ComboBox Deb_IVA 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   49
         Top             =   2160
         Width           =   5055
      End
      Begin VB.ComboBox Deudores_Vtas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   48
         Top             =   1080
         Width           =   5055
      End
      Begin VB.ComboBox Proveedores 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   47
         Top             =   1440
         Width           =   5055
      End
      Begin VB.ComboBox Ventas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   46
         Top             =   1800
         Width           =   5055
      End
      Begin VB.ComboBox Caja 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   45
         Top             =   360
         Width           =   5055
      End
      Begin VB.Label Label23 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Mercader�a:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   55
         Top             =   720
         Width           =   1335
      End
      Begin VB.Label Label22 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Proveedores:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   53
         Top             =   1440
         Width           =   1335
      End
      Begin VB.Label Label21 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Ventas:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   44
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Caja:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Deud. x vtas:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. D�bito IVA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   31
         Top             =   2160
         Width           =   1215
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Cr�dito IVA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Impuestos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. D�bito STA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   2520
         Width           =   1215
      End
   End
   Begin VB.Frame Marco_Opciones 
      Height          =   3735
      Left            =   480
      TabIndex        =   42
      Top             =   600
      Visible         =   0   'False
      Width           =   6735
      Begin VB.CheckBox Desplegar 
         Caption         =   "Desplegar combos autom�ticamente.-"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   43
         Top             =   1680
         Width           =   6255
      End
   End
   Begin VB.Frame Marco_Empresa 
      Height          =   3735
      Left            =   480
      TabIndex        =   15
      Top             =   600
      Width           =   6735
      Begin VB.TextBox email 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   30
         TabIndex        =   4
         Top             =   1680
         Width           =   5055
      End
      Begin VB.TextBox Telefono 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   30
         TabIndex        =   3
         Top             =   1320
         Width           =   5055
      End
      Begin VB.TextBox Direccion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   30
         TabIndex        =   2
         Top             =   960
         Width           =   5055
      End
      Begin VB.TextBox Empresa 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   0
         Top             =   240
         Width           =   5055
      End
      Begin VB.TextBox Slogan 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   1
         Top             =   600
         Width           =   5055
      End
      Begin VB.Label Label19 
         Alignment       =   1  'Right Justify
         Caption         =   "e - mail:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label Label18 
         Alignment       =   1  'Right Justify
         Caption         =   "Tel�fonos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label17 
         Alignment       =   1  'Right Justify
         Caption         =   "Direcci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   34
         Top             =   960
         Width           =   1215
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre Empresa:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label16 
         Alignment       =   1  'Right Justify
         Caption         =   "Slogan:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   600
         Width           =   1335
      End
   End
   Begin VB.Frame Marco_Impresora 
      Enabled         =   0   'False
      Height          =   3735
      Left            =   480
      TabIndex        =   21
      Top             =   600
      Width           =   6735
      Begin VB.ComboBox Print_Aux 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         TabIndex        =   13
         Top             =   3120
         Width           =   2895
      End
      Begin VB.TextBox Copias 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6240
         MaxLength       =   1
         TabIndex        =   14
         Top             =   2640
         Width           =   375
      End
      Begin VB.TextBox Sta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   6
         TabIndex        =   7
         Top             =   1080
         Width           =   855
      End
      Begin VB.TextBox Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   6
         TabIndex        =   6
         Top             =   720
         Width           =   855
      End
      Begin VB.TextBox Emisor 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   4
         TabIndex        =   5
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Fc_A 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   10
         TabIndex        =   8
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox Fc_B 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         MaxLength       =   10
         TabIndex        =   9
         Top             =   1680
         Width           =   1335
      End
      Begin VB.TextBox Nc_A 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   10
         TabIndex        =   10
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Nc_B 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         MaxLength       =   10
         TabIndex        =   11
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Puerto 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   1
         TabIndex        =   12
         Top             =   2640
         Width           =   375
      End
      Begin VB.Label Label24 
         Caption         =   "Puerto impresora aux.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   56
         Top             =   3120
         Width           =   1575
      End
      Begin VB.Label Label20 
         Alignment       =   1  'Right Justify
         Caption         =   "Copias de Comprobantes a emitir:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2160
         TabIndex        =   41
         Top             =   2640
         Width           =   3975
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Alicuota STA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   480
         TabIndex        =   40
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Alicuota IVA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   480
         TabIndex        =   39
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Centro Emisior:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   480
         TabIndex        =   38
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "�ltima FC 'A':"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   26
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "�ltima FC 'B':"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   25
         Top             =   1680
         Width           =   1335
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "�ltima N. Cr�d. 'B':"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   24
         Top             =   2040
         Width           =   1335
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "�ltima N. Cred. 'A':"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   240
         TabIndex        =   23
         Top             =   2040
         Width           =   1440
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Caption         =   "Puerto Impresor fiscal:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   2640
         Width           =   1575
      End
   End
   Begin MSComctlLib.TabStrip Panel 
      Height          =   4455
      Left            =   120
      TabIndex        =   37
      Top             =   120
      Width           =   7455
      _ExtentX        =   13150
      _ExtentY        =   7858
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   4
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Datos de la Empresa"
            Object.ToolTipText     =   "Modifique los Datos de su empresa."
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Facturaci�n"
            Object.ToolTipText     =   "N�meraci�n de Comprobantes y configuraci�n del Controlador."
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Contabilidad"
            Object.ToolTipText     =   "Configuraci�n de las cuentas contables."
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab4 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Opciones"
            Object.ToolTipText     =   "Opciones varias"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   120
      TabIndex        =   18
      Top             =   4680
      Width           =   7455
      Begin VB.CommandButton Grabar 
         Height          =   615
         Left            =   4920
         Picture         =   "Tbl_PanelCtrol.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   6240
         Picture         =   "Tbl_PanelCtrol.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Tbl_PanelCtrol"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Caja_GotFocus()
    Caja.SelStart = 0
    Caja.SelText = ""
    Caja.SelLength = Len(Caja.Text)
End Sub

Private Sub Caja_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Copias_GotFocus()
    Copias.SelStart = 0
    Copias.SelLength = Len(Copias.Text)
End Sub

Private Sub Copias_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Copias_LostFocus()
    If Val(Copias.Text) = 0 Then Copias.Text = "1"
    Copias.Text = Formateado(Str(Val(Copias.Text)), 0, 1, " ", False)
End Sub

'Private Sub Cta_Credito_Iva_GotFocus()
'    Cta_Credito_Iva.SelStart = 0
'    Cta_Credito_Iva.SelText = ""
'    Cta_Credito_Iva.SelLength = Len(Cta_Credito_Iva.Text)
'End Sub

Private Sub Cta_Credito_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

'Private Sub Cta_Debito_Iva_GotFocus()
'    Cta_Debito_Iva.SelStart = 0
'    Cta_Debito_Iva.SelText = ""
'    Cta_Debito_Iva.SelLength = Len(Cta_Debito_Iva.Text)
'End Sub

Private Sub Cta_Debito_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

'Private Sub Cta_Debito_STA_GotFocus()
'    Cta_Debito_STA.SelStart = 0
'    Cta_Debito_STA.SelText = ""
'    Cta_Debito_STA.SelLength = Len(Cta_Debito_STA.Text)
'End Sub

Private Sub Cta_Debito_STA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

'Private Sub Cta_Impuestos_GotFocus()
'    Cta_Impuestos.SelStart = 0
'    Cta_Impuestos.SelText = ""
'    Cta_Impuestos.SelLength = Len(Cta_Impuestos.Text)
'End Sub

Private Sub Cta_Impuestos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

'Private Sub CtaCte_GotFocus()
'    CtaCte.SelStart = 0
'    CtaCte.SelText = ""
'    CtaCte.SelLength = Len(CtaCte.Text)
'End Sub

Private Sub CtaCte_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desplegar_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub Direccion_GotFocus()
    Direccion.SelStart = 0
    Direccion.SelLength = Len(Direccion.Text)
End Sub

Private Sub Direccion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Direccion_LostFocus()
    Direccion.Text = FiltroCaracter(Direccion.Text)
End Sub

Private Sub Email_GotFocus()
    email.SelStart = 0
    email.SelLength = Len(email.Text)
End Sub

Private Sub Email_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Emisor_GotFocus()
    Emisor.SelStart = 0
    Emisor.SelText = ""
    Emisor.SelLength = Len(Emisor.Text)
End Sub

Private Sub Emisor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Emisor_LostFocus()
    Emisor.Text = Formateado(Str(Val(Emisor.Text)), 0, 4, "0", False)
End Sub

Private Sub Empresa_GotFocus()
    Empresa.Text = Trim(Empresa.Text)
    Empresa.SelStart = 0
    Empresa.SelText = ""
    Empresa.SelLength = Len(Empresa.Text)
End Sub

Private Sub Empresa_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Empresa_LostFocus()
    Empresa.Text = FiltroCaracter(Empresa.Text)
End Sub

Private Sub Fc_A_GotFocus()
    Fc_A.SelStart = 0
    Fc_A.SelText = ""
    Fc_A.SelLength = Len(Fc_A.Text)
End Sub

Private Sub Fc_A_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fc_A_LostFocus()
    Fc_A.Text = Formateado(Str(Val(Fc_A.Text)), 0, 10, "0", False)
End Sub

Private Sub Fc_B_GotFocus()
    Fc_B.SelStart = 0
    Fc_B.SelText = ""
    Fc_B.SelLength = Len(Fc_B.Text)
End Sub

Private Sub Fc_B_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fc_B_LostFocus()
    Fc_B.Text = Formateado(Str(Val(Fc_B.Text)), 0, 10, "0", False)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Panel de Control.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Print_Aux.AddItem "LPT1:"
    Print_Aux.AddItem "LPT2:"
    Print_Aux.AddItem "LPT3:"
    Print_Aux.AddItem "\\Cliente\espon"
    
    Cargar_Datos
End Sub

Private Sub Cargar_Datos()
    qy = "SELECT * FROM Empresa WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
    AbreRs
    
    Empresa.Text = Trim(Rs.Fields("Denominacion"))
    Slogan.Text = Trim(Rs.Fields("Slogan"))
    Direccion.Text = Trim(Rs.Fields("Direccion"))
    Telefono.Text = Trim(Rs.Fields("Telefono"))
    email.Text = Trim(Rs.Fields("email"))
    Emisor.Text = Formateado(Str(Val(Rs.Fields("Centro_Emisor"))), 0, 4, "0", False)
    Iva.Text = Formateado(Str(Val(Rs.Fields("Alicuota_Iva"))), 2, 6, " ", False)
    Sta.Text = Formateado(Str(Val(Rs.Fields("Alicuota_Sta"))), 2, 6, " ", False)
    Fc_A.Text = Formateado(Str(Val(Rs.Fields("Ultima_FC_A"))), 0, 10, "0", False)
    Nc_A.Text = Formateado(Str(Val(Rs.Fields("Ultima_NC_A"))), 0, 10, "0", False)
    Fc_B.Text = Formateado(Str(Val(Rs.Fields("Ultima_FC_B"))), 0, 10, "0", False)
    Nc_B.Text = Formateado(Str(Val(Rs.Fields("Ultima_NC_B"))), 0, 10, "0", False)
    Copias.Text = Val(Rs.Fields("Copias"))
    Puerto.Text = Rs.Fields("Puerto_Impresora")
    Desplegar.Value = IIf(Rs.Fields("Desplegar_combos") = "Falso", 0, 1)
    
    qy = "SELECT * FROM Usuario WHERE Id_Usuario = " & Trim(Str(Val(Numero_Usuario))) & " "
    AbreRs
    
    Print_Aux.Text = Rs.Fields("Puerto_MPto")
End Sub

Private Sub Panel_Click()
    If Panel.SelectedItem.Index = 1 Then
        Marco_Impresora.Visible = False
        Marco_Impresora.Enabled = False
        Marco_Conta.Visible = False
        Marco_Conta.Enabled = False
        Marco_Opciones.Enabled = False
        Marco_Opciones.Visible = False
        Marco_Empresa.Visible = True
        Marco_Empresa.Enabled = True
        
        Empresa.SetFocus
    ElseIf Panel.SelectedItem.Index = 2 Then
        Marco_Empresa.Visible = False
        Marco_Empresa.Enabled = False
        Marco_Opciones.Enabled = False
        Marco_Opciones.Visible = False
        Marco_Conta.Visible = False
        Marco_Conta.Enabled = False
        Marco_Impresora.Visible = True
        Marco_Impresora.Enabled = True
        
      
        Emisor.SetFocus
    ElseIf Panel.SelectedItem.Index = 3 Then
        Marco_Empresa.Visible = False
        Marco_Empresa.Enabled = False
        Marco_Impresora.Visible = False
        Marco_Impresora.Enabled = False
        Marco_Opciones.Enabled = False
        Marco_Opciones.Visible = False
        Marco_Conta.Visible = True
        Marco_Conta.Enabled = True
        
        Caja.SetFocus
    ElseIf Panel.SelectedItem.Index = 4 Then
        Marco_Empresa.Visible = False
        Marco_Empresa.Enabled = False
        Marco_Impresora.Visible = False
        Marco_Impresora.Enabled = False
        Marco_Conta.Visible = False
        Marco_Conta.Enabled = False
        Marco_Opciones.Enabled = True
        Marco_Opciones.Visible = True
        
        Desplegar.SetFocus
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    'Actualizar_NumComp
    'Cargar_Datos
    
    qy = "UPDATE Empresa SET "
    qy = qy & "Denominacion = '" & Trim(Empresa.Text) & "', "
    qy = qy & "Slogan = '" & Trim(Slogan.Text) & "',"
    qy = qy & "Direccion = '" & Trim(Direccion.Text) & "',"
    qy = qy & "Telefono = '" & Trim(Telefono.Text) & "',"
    qy = qy & "email = '" & Trim(email.Text) & "',"
    qy = qy & "Centro_Emisor = '" & Str(Val(Emisor.Text)) & "',"
    qy = qy & "Alicuota_Iva = '" & Val(Iva.Text) & "',"
    qy = qy & "Alicuota_Sta = '" & Val(Sta.Text) & "',"
    qy = qy & "Ultima_Fc_A = '" & Val(Fc_A.Text) & "',"
    qy = qy & "Ultima_Fc_B = '" & Val(Fc_B.Text) & "',"
    qy = qy & "Ultima_Nc_A = '" & Val(Nc_A.Text) & "',"
    qy = qy & "Ultima_Nc_B = '" & Val(Nc_B.Text) & "',"
    qy = qy & "Copias = " & Trim(Str(Val(Copias.Text))) & ","
    'Qy = Qy & "Cta_Caja = '" & trim(Caja.Text) & "',"
    'Qy = Qy & "Cta_CtaCte = '" & trim(CtaCte.Text) & "',"
    'Qy = Qy & "Cta_Debito_Iva = '" & trim(Cta_Debito_Iva.Text) & "',"
    'Qy = Qy & "Cta_Debito_Sta = '" & trim(Cta_Debito_STA.Text) & "',"
    'Qy = Qy & "Cta_Credito_Iva = '" & trim(Cta_Credito_Iva.Text) & "',"
    'Qy = Qy & "Cta_Impuestos = '" & trim(Cta_Impuestos.Text) & "',"
    'Qy = Qy & "Desplegar_Combos = " & Val(Desplegar.Value) & ", "
    qy = qy & "Puerto_Impresora = '" & Trim(Str(Val(Puerto.Text))) & "' "
    qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
    Db.Execute (qy)
    
    qy = "UPDATE Usuario SET "
    qy = qy & "Puerto_MPto = '" & Trim(Print_Aux.Text) & "' "
    qy = qy & "WHERE Id_Usuario = " & Val(Numero_Usuario)
    Db.Execute (qy)
    
    Cargar_Datos
    Parametrizar
    
    MsgBox "los datos se han actualizado correctamente.! ", vbInformation, "Atenci�n.!"
    Salir.SetFocus
End Sub

Private Sub Iva_GotFocus()
    Iva.Text = Trim(Iva.Text)
    Iva.SelStart = 0
    Iva.SelText = ""
    Iva.SelLength = Len(Iva.Text)
End Sub

Private Sub Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Iva_LostFocus()
    Iva.Text = Formateado(Str(Val(Iva.Text)), 2, 6, " ", False)
End Sub

Private Sub Nc_A_GotFocus()
    Nc_A.SelStart = 0
    Nc_A.SelText = ""
    Nc_A.SelLength = Len(Nc_A.Text)
End Sub

Private Sub Nc_A_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nc_A_LostFocus()
    Nc_A.Text = Formateado(Str(Val(Nc_A.Text)), 0, 10, "0", False)
End Sub

Private Sub Nc_B_GotFocus()
    Nc_B.SelStart = 0
    Nc_B.SelText = ""
    Nc_B.SelLength = Len(Nc_B.Text)
End Sub

Private Sub Nc_B_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nc_B_LostFocus()
    Nc_B.Text = Formateado(Str(Val(Nc_B.Text)), 0, 10, "0", False)
End Sub

Private Sub Print_Aux_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Puerto_GotFocus()
    Puerto.SelStart = 0
    Puerto.SelText = ""
    Puerto.SelLength = Len(Puerto.Text)
End Sub

Private Sub Puerto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Puerto_LostFocus()
    Puerto.Text = Formateado(Str(Val(Puerto.Text)), 0, 1, "", False)
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub

Private Sub Slogan_GotFocus()
    Slogan.Text = Trim(Slogan.Text)
    Slogan.SelStart = 0
    Slogan.SelText = ""
    Slogan.SelLength = Len(Slogan.Text)
End Sub

Private Sub Slogan_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Slogan_LostFocus()
    Slogan.Text = FiltroCaracter(Slogan.Text)
End Sub

Private Sub Sta_GotFocus()
    Sta.Text = Trim(Sta.Text)
    Sta.SelStart = 0
    Sta.SelText = ""
    Sta.SelLength = Len(Sta.Text)
End Sub

Private Sub Sta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Sta_LostFocus()
    Sta.Text = Formateado(Str(Val(Sta.Text)), 2, 6, " ", False)
End Sub

Private Sub Telefono_GotFocus()
    Telefono.SelStart = 0
    Telefono.SelLength = Len(Telefono.Text)
End Sub

Private Sub Telefono_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Telefono_LostFocus()
    Telefono.Text = FiltroCaracter(Telefono.Text)
End Sub
