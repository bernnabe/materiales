VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Fac_Vta_Obser 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Observaciones.-"
   ClientHeight    =   8400
   ClientLeft      =   45
   ClientWidth     =   7575
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   8400
   ScaleWidth      =   7575
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   690
      Left            =   0
      TabIndex        =   49
      Top             =   4575
      Width           =   7590
      Begin VB.TextBox txtBonificacionDescripcion 
         Height          =   315
         Left            =   4050
         TabIndex        =   12
         Top             =   225
         Width           =   3390
      End
      Begin VB.TextBox txtBonificacionGeneral 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2025
         MaxLength       =   6
         TabIndex        =   11
         Top             =   225
         Width           =   840
      End
      Begin VB.Label Label20 
         Alignment       =   1  'Right Justify
         Caption         =   "Descripci�n:"
         ForeColor       =   &H00800000&
         Height          =   240
         Index           =   1
         Left            =   2175
         TabIndex        =   51
         Top             =   300
         Width           =   1815
      End
      Begin VB.Label Label20 
         Alignment       =   1  'Right Justify
         Caption         =   "% Bonificaci�n General:"
         ForeColor       =   &H00800000&
         Height          =   165
         Index           =   0
         Left            =   75
         TabIndex        =   50
         Top             =   300
         Width           =   1815
      End
   End
   Begin VB.Frame frame1 
      Height          =   690
      Left            =   0
      TabIndex        =   46
      Top             =   -75
      Width           =   7575
      Begin MSComCtl2.DTPicker dtPeriodo 
         Height          =   330
         Left            =   3075
         TabIndex        =   47
         Top             =   225
         Width           =   1425
         _ExtentX        =   2514
         _ExtentY        =   582
         _Version        =   393216
         CustomFormat    =   "MM/yyyy"
         Format          =   49283075
         CurrentDate     =   39704
      End
      Begin VB.Label Label19 
         Alignment       =   1  'Right Justify
         Caption         =   "Per�odo contable:"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   1575
         TabIndex        =   48
         Top             =   300
         Width           =   1440
      End
   End
   Begin VB.Frame Marco_Forma_Pago 
      Height          =   3975
      Left            =   0
      TabIndex        =   31
      Top             =   600
      Width           =   7575
      Begin VB.ComboBox Banco 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         ItemData        =   "Fac_Vta_Obser.frx":0000
         Left            =   4560
         List            =   "Fac_Vta_Obser.frx":0002
         TabIndex        =   3
         Top             =   1560
         Width           =   2895
      End
      Begin VB.TextBox Titular_Targeta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   10
         Top             =   3600
         Width           =   4335
      End
      Begin VB.TextBox Cobranza 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6120
         MaxLength       =   10
         TabIndex        =   5
         Top             =   1920
         Width           =   1335
      End
      Begin VB.TextBox Emision 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         MaxLength       =   10
         TabIndex        =   4
         Top             =   1920
         Width           =   1335
      End
      Begin VB.ComboBox Nro_Targeta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4560
         TabIndex        =   8
         Top             =   2880
         Width           =   2895
      End
      Begin VB.TextBox Nro_Cheque 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   2
         Top             =   1560
         Width           =   1335
      End
      Begin VB.TextBox Targeta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         MaxLength       =   10
         TabIndex        =   7
         Top             =   2880
         Width           =   1335
      End
      Begin VB.TextBox Cheque 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         MaxLength       =   10
         TabIndex        =   1
         Top             =   1200
         Width           =   1335
      End
      Begin VB.TextBox Efectivo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3075
         MaxLength       =   10
         TabIndex        =   0
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox Titular 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   6
         Top             =   2280
         Width           =   4335
      End
      Begin VB.TextBox Denominacion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   9
         Top             =   3240
         Width           =   4335
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Denominaci�n de tarjeta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   45
         Top             =   3240
         Width           =   1935
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Titular:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1440
         TabIndex        =   44
         Top             =   3600
         Width           =   1575
      End
      Begin VB.Label Label2 
         BackStyle       =   0  'Transparent
         Caption         =   "N�mero de Tarjeta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4560
         TabIndex        =   43
         Top             =   2640
         Width           =   1575
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   1560
         TabIndex        =   42
         Top             =   2880
         Width           =   1455
      End
      Begin VB.Line Line1 
         Index           =   2
         X1              =   240
         X2              =   7440
         Y1              =   2640
         Y2              =   2640
      End
      Begin VB.Label Label18 
         Caption         =   "Banco:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4560
         TabIndex        =   41
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label17 
         Alignment       =   1  'Right Justify
         Caption         =   "Titular del cheque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1680
         TabIndex        =   40
         Top             =   2280
         Width           =   1335
      End
      Begin VB.Label Label16 
         Alignment       =   1  'Right Justify
         Caption         =   "Cobranza:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5160
         TabIndex        =   39
         Top             =   1920
         Width           =   855
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Emisi�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1320
         TabIndex        =   38
         Top             =   1920
         Width           =   1695
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "N�mero de Cheque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1440
         TabIndex        =   37
         Top             =   1560
         Width           =   1575
      End
      Begin VB.Label Label13 
         BackStyle       =   0  'Transparent
         Caption         =   "Efectivo:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   36
         Top             =   240
         Width           =   2415
      End
      Begin VB.Line Line1 
         Index           =   1
         X1              =   240
         X2              =   7440
         Y1              =   240
         Y2              =   240
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Cheque:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   35
         Top             =   840
         Width           =   2655
      End
      Begin VB.Line Line1 
         Index           =   0
         X1              =   240
         X2              =   7440
         Y1              =   840
         Y2              =   840
      End
      Begin VB.Label Label11 
         Caption         =   "Tarjeta de Cr�dito:"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Left            =   240
         TabIndex        =   34
         Top             =   2640
         Width           =   3375
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   1560
         TabIndex        =   33
         Top             =   1200
         Width           =   1455
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1560
         TabIndex        =   32
         Top             =   480
         Width           =   1455
      End
   End
   Begin VB.Frame Marco_Comp 
      Height          =   735
      Left            =   0
      TabIndex        =   23
      Top             =   5250
      Width           =   7575
      Begin VB.TextBox Remito_Cuatro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6120
         TabIndex        =   16
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Remito_Tres 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2760
         TabIndex        =   14
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Remito_Dos 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4440
         MaxLength       =   20
         TabIndex        =   15
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Remito_Uno 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   20
         TabIndex        =   13
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Remito:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   26
         Top             =   240
         Width           =   900
      End
   End
   Begin VB.Frame Marco_Obser 
      Caption         =   "Obervaciones"
      Height          =   1695
      Left            =   0
      TabIndex        =   24
      Top             =   6000
      Width           =   7575
      Begin VB.TextBox Linea_Cuatro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   39
         TabIndex        =   20
         Top             =   1320
         Width           =   6375
      End
      Begin VB.TextBox Linea_Tres 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   39
         TabIndex        =   19
         Top             =   960
         Width           =   6375
      End
      Begin VB.TextBox Linea_Dos 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   39
         TabIndex        =   18
         Top             =   600
         Width           =   6375
      End
      Begin VB.TextBox Linea_Uno 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   39
         TabIndex        =   17
         Top             =   240
         Width           =   6375
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea 4:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea 3:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea 2:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea 1:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Botonera 
      Height          =   735
      Left            =   0
      TabIndex        =   25
      Top             =   7650
      Width           =   7575
      Begin VB.CommandButton Cancelar 
         Cancel          =   -1  'True
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   6360
         TabIndex        =   22
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Aceptar 
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   5160
         TabIndex        =   21
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Fac_Vta_Obser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit




'------------------------------------------------------------------------------------
'                           Variables privadas del modulo
'------------------------------------------------------------------------------------




Private mvarImporteOriginal As Double
Private mvarOperacionConfirmada As Boolean
Private ImporteBonificacionFactura As Double




'------------------------------------------------------------------------------------
'                               Propiedades p�blicas
'------------------------------------------------------------------------------------


Public Property Get OperacionConfirmada() As Boolean
    OperacionConfirmada = mvarOperacionConfirmada
End Property

Public Property Let OperacionConfirmada(vdata As Boolean)
    mvarOperacionConfirmada = vdata
End Property


'------------------------------------------------------------------------------------
'                             Procedimientos y Funciones
'------------------------------------------------------------------------------------




Private Sub Grabar_Comprobante()
    
    Dim i                 As Long
    Dim l                 As Long
    Dim j                 As Long
    
    Dim Rec_Rubro         As Long
    Dim Rec_Articulo      As Long
    Dim Rec_Descripcion   As String
    Dim Rec_Presentacion  As Long
    Dim Rec_Cantidad      As Single
    Dim Rec_Espesor       As Single
    Dim Rec_Ancho         As Single
    Dim Rec_Largo         As Single
    Dim Rec_Precio        As Double
    Dim Rec_Precio_Vta    As Double
    Dim Rec_Impuesto      As Single
    Dim Rec_ImporCta      As Single
    Dim Rec_Existencia    As Long
    
    Dim Rec_Asiento       As Long
    Dim Rec_Cuenta        As String
    Dim Alm_Cta           As String
    
    Dim Total_Neto        As Single 'Para grabar el neto en el asiento
    Dim Diferencia        As Single 'Para guardar la diferencia entre entre netofactura y netoasiento en los art�culos
    Dim Detalle           As String 'Para guardar el detalle del asiento (dep rueba)
    
    On Error GoTo Errores
    
    Db.BeginTrans
    
    If Val(Fac_Vta.List1.ListCount) > 0 Then
        MousePointer = 11
        'Primero Imprimo la Factura y si no surge ning�n error la grabo
        
        If Trim(UCase(Fac_Vta.Cliente.Text)) = "M" Then
            qy = "SELECT MAX(Id_Cliente) FROM Cliente"
            AbreRs
            
            Fac_Vta.Cliente.Text = 1
            If Not Rs.Fields(0) = "" Then Fac_Vta.Cliente.Text = Val(Rs.Fields(0)) + 1
            
            qy = "INSERT INTO Cliente VALUES ('"
            qy = qy & Trim(Str(Val(Fac_Vta.Cliente.Text))) & "'"
            qy = qy & ", '" & Trim(Fac_Vta.Nombre.Text) & "'"
            qy = qy & ", '" & Trim(Fac_Vta.Direccion.Text) & "'"
            qy = qy & ", '0', '0', ' '"
            qy = qy & ", '" & Trim(Str(Val(Fac_Vta.Localidad.Text))) & "'"
            qy = qy & ", ' ', ' '"
            qy = qy & ", '" & Trim(UCase(Mid(Fac_Vta.Condicion_Iva.Text, 1, 2))) & "'"
            qy = qy & ", '00'"
            qy = qy & ", '" & Trim(Fac_Vta.Cuit.Text) & "'"
            qy = qy & ", '00', '00'"
            qy = qy & ", " & IIf(Trim(Fac_Vta.Tipo_Cliente.Text) = "R", "1", "2")
            qy = qy & ", 0.00 "
            qy = qy & ", '" & Trim(Fac_Vta.Fecha.Text) & "'"
            qy = qy & ", ' ')"
            Db.Execute (qy)
                
            MsgBox "El Cliente ha sido registrado en la base de datos con el n�mero: " & Trim(Str(Val(Fac_Vta.Cliente.Text))), vbInformation, "Atenci�n.!"
        End If
    
        qy = "SELECT MAX(Nro_Asiento) FROM Asiento "
        qy = qy & "WHERE Ejercicio = " & Trim(Str(Val(Mid(Fac_Vta.Fecha.Text, 7, 4)))) & " "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        'AbreRs
        
        Rec_Asiento = 1
        'If IsNull(Rs.Fields(0)) = False Then Rec_Asiento = Val(Rs.Fields(0)) + 1
        
        qy = "INSERT INTO Factura_Venta VALUES("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", '" & Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) & "'"
        qy = qy & ", '" & Trim(UCase(Fac_Vta.Letra.Text)) & "'"
        qy = qy & ", " & Trim(Str(Val(Fac_Vta.Centro_Emisor.Text)))
        qy = qy & ", " & Trim(Str(Val(Fac_Vta.Numero.Text)))
        qy = qy & ", " & Trim(dtPeriodo.Month)
        qy = qy & ", " & Trim(dtPeriodo.Year)
        qy = qy & ", " & Trim(Str(Val(Fac_Vta.Plazo.Text)))
        qy = qy & ", '" & Format(Trim(Fac_Vta.Fecha.Text), "dd/MM/yyyy") & " " & Trim(Hora_Fiscal) & "'"
        qy = qy & ", " & Trim(Str(Val(Fac_Vta.Cliente.Text)))
        qy = qy & ", " & IIf(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str(Val(Fac_Vta.Neto_Gravado.Text)) * -1), (Str(Val(Fac_Vta.Neto_Gravado.Text))))
        qy = qy & ", " & Val(ImporteBonificacionFactura)
        qy = qy & ", 0, " & IIf(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str(Val(Fac_Vta.Exento.Text)) * -1), Trim(Str(Val(Fac_Vta.Exento.Text))))
        qy = qy & ", " & IIf(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str(Val(Fac_Vta.Sub_Total.Text)) * -1), Str(Val(Fac_Vta.Sub_Total.Text)))
        qy = qy & ", " & IIf(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", Str(Val(Fac_Vta.Iva_Inscrip.Text)) * -1, Str(Val(Fac_Vta.Iva_Inscrip.Text)))
        qy = qy & ", 0, " & IIf(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str(Val(Fac_Vta.Retencion_Iva.Text)) * -1), Str(Val(Fac_Vta.Retencion_Iva.Text)))
        qy = qy & ", " & IIf(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str(Val(Fac_Vta.Retencion_IIBB.Text)) * -1), Str(Val(Fac_Vta.Retencion_IIBB.Text)))
        qy = qy & ", " & IIf(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str(Val(Fac_Vta.Total_Factura.Text)) * -1), (Str(Val(Fac_Vta.Total_Factura.Text))))
        qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
        qy = qy & ", " & Trim(Str(Val(Mid(Fac_Vta.Fecha.Text, 7, 4)))) & ")"
        Db.Execute (qy)
        
        If Fac_Vta.DatosImportados Then
        
            Dim k As Integer
            Dim CodigosComprobantes() As String
            
            CodigosComprobantes = Split(Fac_Vta.CodigoDatosImportados, ",")
            
            For k = 0 To UBound(CodigosComprobantes)
                
                Select Case Fac_Vta.OrigenDatosImportados
                
                    Case TiposComprobantes.Presupuesto
                    
                        qy = "UPDATE Presupuesto Set "
                        qy = qy & "Estado = " & ComprobanteEstados.Facturado
                        qy = qy & "WHERE Id_Nro_Presupuesto = " & CodigosComprobantes(k) & " "
                        qy = qy & "AND Empresa = " & Id_Empresa
                    
                    Case TiposComprobantes.OrdenEntrega
                
                        qy = "UPDATE Orden_Entrega Set "
                        qy = qy & "Estado = " & ComprobanteEstados.Facturado
                        qy = qy & "WHERE Id_Nro_Orden_Entrega = " & CodigosComprobantes(k) & " "
                        qy = qy & "AND Empresa = " & Id_Empresa
                        
                End Select
                
                Db.Execute (qy)
                
            Next
                
        End If
        
        'Grabo la Primera l�nea de la Contabilidad.-
        qy = "INSERT INTO Asiento VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
        qy = qy & ", " & Trim(Str(Val(Mid(Fac_Vta.Fecha.Text, 7, 4))))
        qy = qy & ", '" & Trim(Fac_Vta.Fecha.Text) & "'"
        qy = qy & ", 0"
        If Val(Fac_Vta.Plazo.Text) > 0 Then
            qy = qy & ", 1, 3, 2, 1, 0, 0"
        Else
            qy = qy & ", 1, 1, 1, 1, 0, 0"
        End If
        qy = qy & ", '" & Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) & " " & Trim(Fac_Vta.Letra.Text) & " " & Formateado(Str(Val(Fac_Vta.Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Fac_Vta.Numero.Text) & " V'"
        qy = qy & ", " & Trim(Str(Val(Fac_Vta.Total_Factura.Text)))
        qy = qy & ", 0)"
        'Db.Execute (Qy)
        
        Detalle = Trim(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) & " " & Trim(Fac_Vta.Letra.Text) & " " & Formateado(Str(Val(Fac_Vta.Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Fac_Vta.Numero.Text) & " V")
        
        For i = 0 To Fac_Vta.List1.ListCount - 1
            
            Rec_Rubro = Val(Mid(Fac_Vta.List1.List(i), 1, 4))
            Rec_Articulo = Val(Mid(Fac_Vta.List1.List(i), 6, 5))
            Rec_Descripcion = Trim(Mid(Fac_Vta.List1.List(i), 12, 30))
            Rec_Presentacion = Val(Mid(Fac_Vta.List1.List(i), 159, 1))
            Rec_Espesor = Mid(Fac_Vta.List1.List(i), 119, 9)
            Rec_Ancho = Mid(Fac_Vta.List1.List(i), 129, 9)
            Rec_Largo = Mid(Fac_Vta.List1.List(i), 139, 9)
            Rec_Precio_Vta = Mid(Fac_Vta.List1.List(i), 149, 9)
            Rec_Precio = Trim(Mid(Fac_Vta.List1.List(i), 73, 10))
            Rec_Impuesto = Val(Rec_Precio) / Val("1." & Val(Alicuota_Iva))
            Rec_Impuesto = ((Val(Rec_Impuesto) * Val(Alicuota_Iva)) / 100)
            Rec_Cantidad = Trim(Mid(Fac_Vta.List1.List(i), 84, 10))
            Rec_Existencia = Trim(Mid(Fac_Vta.List1.List(i), 98, 10))
            Rec_Cuenta = Mid(Fac_Vta.List1.List(i), 172, 30)
            Rec_ImporCta = Val(Mid(Fac_Vta.List1.List(i), 95, 10)) / Val("1." & Alicuota_Iva)
            Total_Neto = Val(Total_Neto) + Val(Rec_ImporCta)
            
            'Si es una Nota de Cr�dito los importes se graban en Negativo
            qy = "INSERT INTO Factura_Venta_Item VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", '" & Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) & "'"
            qy = qy & ", '" & Trim(UCase(Fac_Vta.Letra.Text)) & "'"
            qy = qy & ", " & Trim(Str(Val(Fac_Vta.Centro_Emisor.Text)))
            qy = qy & ", " & Trim(Str(Val(Fac_Vta.Numero.Text)))
            qy = qy & ", " & Trim(Str(Val(i)))
            qy = qy & ", " & Trim(Str(Val(Rec_Rubro)))
            qy = qy & ", " & Trim(Str(Val(Rec_Articulo)))
            qy = qy & ", '" & Trim(Rec_Descripcion) & "'"
            qy = qy & ", " & Trim(Str(Val(Rec_Presentacion)))
            qy = qy & ", " & IIf(Val(Rec_Espesor) > 0, Trim(Str(Val(Rec_Espesor))), 0)
            qy = qy & ", " & IIf(Val(Rec_Ancho) > 0, Trim(Str(Val(Rec_Ancho))), 0)
            qy = qy & ", " & IIf(Val(Rec_Largo) > 0, Trim(Str(Val(Rec_Largo))), 0)
            qy = qy & ", " & Trim(Str(Val(Rec_Cantidad)))
            qy = qy & ", " & Trim(Str(Val(Rec_Impuesto)))
            If (Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "NC" Then
                qy = qy & ", " & ((Val(Rec_Precio) * Val(Rec_Cantidad)) * -1)
            Else
                qy = qy & ", " & (Val(Rec_Precio) * Val(Rec_Cantidad))
            End If
            qy = qy & ", " & Trim(Str(Val(Rec_Precio_Vta))) & ")"
            Db.Execute (qy)
            
'           12/03/2011
'           La actualizaci�n de la existencia de stock se realiza desde las ordenes de entrega.
'           21/01/2011
'           Tambien desde las notas de devolvuci�n.
'            If Val(Rec_Articulo) > 0 And Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) <> "FC" Then
'
'                qy = "UPDATE Articulo SET "
'                If Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "ND" Then
'                    qy = qy & "Existencia = Existencia - " & Trim(Str(Val(Rec_Cantidad))) & " "
'                ElseIf Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "NC" Then
'                    qy = qy & "Existencia = Existencia + " & Trim(Str(Val(Rec_Cantidad))) & " "
'                End If
'                qy = qy & "WHERE Id_Articulo = " & Trim(Str(Val(Rec_Articulo))) & " "
'                qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Rec_Rubro)))
'                Db.Execute (qy)
'
'            End If
        Next
        
        If Val(Fac_Vta.Plazo.Text) > 0 Then
            qy = "INSERT INTO CtaCte_Cliente VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Fac_Vta.Cliente.Text)))
            qy = qy & ", '" & Trim(Fac_Vta.Fecha.Text) & " " & Trim(Hora_Fiscal) & "'"
            qy = qy & ", '" & Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) & " " & Trim(Fac_Vta.Letra.Text) & " " & Formateado(Str(Val(Fac_Vta.Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Fac_Vta.Numero.Text) & "'"
            qy = qy & ", '" & "OPERACIONES EN CTACTE" & "'"
            qy = qy & ", '" & Trim(Fac_Vta.Vto.Text) & "'"
            If Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "FC" Or Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "ND" Then
                qy = qy & ", '" & Trim(Str(Val(Fac_Vta.Total_Factura.Text))) & "'"
                qy = qy & ", '0')"
            Else
                qy = qy & ", '0'"
                qy = qy & ", '" & Trim(Str(Val(Fac_Vta.Total_Factura.Text))) & "')"
            End If
            Db.Execute (qy)
        End If
        
        If Val(Cheque.Text) > 0 Then
            qy = "INSERT INTO Cheque VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Nro_Cheque.Text)))
            qy = qy & ", " & Trim(Val(Mid(Banco.Text, 26)))
            qy = qy & ", '" & Mid(Fac_Vta.Tipo_Comp.Text, 1, 2) & " " & Trim(Fac_Vta.Letra.Text) & " " & Formateado(Str(Val(Fac_Vta.Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Fac_Vta.Numero.Text) & "'"
            qy = qy & ", 'C'"
            qy = qy & ", '" & Trim(Titular.Text) & "'"
            qy = qy & ", 0"
            qy = qy & ", '" & Trim(Emision.Text) & "'"
            qy = qy & ", " & Val(DateDiff("d", Emision.Text, Cobranza.Text))
            qy = qy & ", '" & Trim(Cobranza.Text) & "'"
            qy = qy & ", 1, " & Trim(Cheque.Text)
            qy = qy & ", 1, ' ')"
            Db.Execute (qy)
        End If
            
        MousePointer = 0
    End If
    
    Db.CommitTrans
    
    Exit Sub
    
Errores:
    
    Db.RollbackTrans
    MsgBox "Ha ocurrido un error al intentar grabar el comprobante.", vbCritical, "Atenci�n"
    
End Sub

Private Sub Imprimir_Comun()
    Dim i As Long
    
    Close #1
    Open "LPT1:" For Output As #1
    
    Print #1,
    Print #1,
    Print #1,
    Print #1, "                                                           " & Fac_Vta.Fecha.Text
    Print #1,
    Print #1,
    Print #1,
    Print #1,
    Print #1, "             " & Trim(UCase(Fac_Vta.Nombre.Text)) + Space(30 - Len(Trim(UCase(Fac_Vta.Nombre.Text)))) & "                 " & Val(Fac_Vta.Cliente.Text)
    Print #1, "             " & Trim(UCase(Fac_Vta.Direccion)) + Space(30 - Len(Trim(Fac_Vta.Direccion.Text)))
    Print #1, "             " & Trim(UCase(Fac_Vta.Localidad.Text))
    Print #1, "             " & Trim(Mid(Fac_Vta.Condicion_Iva.Text, 2)) + Space(30 - Len(Trim(Mid(Fac_Vta.Condicion_Iva.Text, 2)))) & "             " & Trim(Fac_Vta.Cuit.Text)
    Print #1,
    Print #1, "             " & IIf(Val(Fac_Vta.Plazo.Text) = 0, "CONTADO  ", "CTA. CTE.") & "                                  " & Trim(Remito_Uno.Text)
    Print #1, "                                                        " & Trim(Remito_Dos.Text)
    Print #1, "                                     " & IIf(Val(Fac_Vta.Plazo.Text) = 0, "          ", Trim(Fac_Vta.Vto.Text)) & "         " & Trim(Remito_Tres.Text)
    Print #1, "                                                        " & Trim(Remito_Cuatro.Text)
    Print #1, "    CODIGO  DESCRIPCION                        UNITARIO   CANTIDAD    IMPORTE"
    Print #1,
    
    For i = 0 To Fac_Vta.List1.ListCount - 1
        Print #1, Mid(Fac_Vta.List1.List(i), 1, 10) & "  " & Mid(Fac_Vta.List1.List(i), 12, 32) & " " & Mid(Fac_Vta.List1.List(i), 73, 10) & " " & Mid(Fac_Vta.List1.List(i), 84, 10) & " " & Mid(Fac_Vta.List1.List(i), 95, 10)
    Next
    
    For i = Fac_Vta.List1.ListCount To 15
        Print #1,
    Next
    
    Print #1,
    Print #1,
    Print #1,
    Print #1, Formateado(Str(Val(Fac_Vta.Neto_Gravado.Text)), 2, 9, " ", False) & " " & Formateado(Str(Val(0)), 2, 10, " ", False) & " " & Formateado(Str(Val(0)), 2, 9, " ", False) & " " & Formateado(Str(Val(Fac_Vta.Neto_Gravado.Text)), 2, 8, " ", False) & " " & Formateado(Str(Val(Fac_Vta.Iva_Inscrip.Text)), 2, 8, " ", False) & "      0.00         "; Formateado(Str(Val(Fac_Vta.Total_Factura.Text)), 2, 8, " ", True)
    Print #1,
    Print #1,
    Print #1, " " & Linea_Uno.Text
    Print #1, " " & Linea_Dos.Text
    Print #1, " " & Linea_Tres.Text
    Print #1, " " & Linea_Cuatro.Text
    
    Print #1,
    Print #1,
    Print #1,
    Print #1,
    
    Print #1, Chr$(12);
    Close #1
    
    If Not UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "ND" Then
        qy = "UPDATE Empresa SET "
        If Trim(UCase(Fac_Vta.Letra.Text)) = "A" And UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "FC" Then
            qy = qy & "Ultima_FC_A = '" & Trim(Fac_Vta.Numero.Text) & "' "
        ElseIf Trim(UCase(Fac_Vta.Letra.Text)) = "B" And UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "FC" Then
            qy = qy & "Ultima_FC_B = '" & Trim(Fac_Vta.Numero.Text) & "' "
        ElseIf Trim(UCase(Fac_Vta.Letra.Text)) = "A" And UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "NC" Then
            qy = qy & "Ultima_NC_A = '" & Trim(Fac_Vta.Numero.Text) & "' "
        ElseIf Trim(UCase(Fac_Vta.Letra.Text)) = "B" And UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "NC" Then
            qy = qy & "Ultima_NC_B = '" & Trim(Fac_Vta.Numero.Text) & "' "
        End If
        
        qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        Db.Execute (qy)
    End If
End Sub

Private Sub Imprimir_Hasar()
    Dim i                    As Long
    Dim Nombre_Cliente       As String
    Dim Direccion_Cliente    As String
    Dim Cuit_Cliente         As String
    Dim Articulo_Desc        As String
    Dim Articulo_Cant        As String
    Dim Articulo_Importe     As String
    
    Inicializar_Impresora
    
    'If trim(mid(Fecha_Fiscal, 1, 10)) <> trim(mid(Menu.HASAR1.FechaHoraFiscal, 1, 10)) Then
    '    MsgBox "La fecha de la factura a ingresar es distinta a la fecha a la factura a imprimir, verifique e ingrese nuevamente.", vbInformation, "Atenci�n.!"
    '
    '    Menu.HASAR1.Finalizar
    '    Cancelar_Click
    '
    '    Fac_Vta.Marco_Factura.Enabled = True
    '
    '    Exit Sub
    'End If
    
    Menu.HASAR1.Encabezado(11) = Trim(Linea_Uno.Text)
    Menu.HASAR1.Encabezado(12) = Trim(Linea_Dos.Text)
    Menu.HASAR1.Encabezado(13) = Trim(Linea_Tres.Text)
    Menu.HASAR1.Encabezado(14) = Trim(Linea_Cuatro.Text)

    Nombre_Cliente = Fac_Vta.Nombre.Text
    Direccion_Cliente = Mid(Fac_Vta.Direccion.Text, 1, 30)
    Cuit_Cliente = Mid(Fac_Vta.Cuit.Text, 1, 2) & Mid(Fac_Vta.Cuit.Text, 4, 8) & Mid(Fac_Vta.Cuit.Text, 13, 1)
    Cuit_Cliente = Val(Cuit_Cliente)
    
    'Inidico la condici�n de iva del cliente
    If Mid(Fac_Vta.Condicion_Iva.Text, 1, 2) = "RI" Then
        Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, RESPONSABLE_INSCRIPTO, Direccion_Cliente
    ElseIf Mid(Fac_Vta.Condicion_Iva.Text, 1, 2) = "CF" Then
   
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, CONSUMIDOR_FINAL, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, CONSUMIDOR_FINAL, Direccion_Cliente
        End If
    ElseIf Mid(Fac_Vta.Condicion_Iva.Text, 1, 2) = "NI" Then
   
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, RESPONSABLE_NO_INSCRIPTO, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, RESPONSABLE_NO_INSCRIPTO, Direccion_Cliente
        End If
    ElseIf Mid(Fac_Vta.Condicion_Iva.Text, 1, 2) = "MT" Then
   
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, MONOTRIBUTO, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, MONOTRIBUTO, Direccion_Cliente
        End If
    ElseIf Mid(Fac_Vta.Condicion_Iva.Text, 1, 2) = "NC" Then
   
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, NO_CATEGORIZADO, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, NO_CATEGORIZADO, Direccion_Cliente
        End If
    ElseIf Mid(Fac_Vta.Condicion_Iva.Text, 1, 2) = "ET" Then
   
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, RESPONSABLE_EXENTO, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, RESPONSABLE_EXENTO, Direccion_Cliente
        End If
    End If
    
    Menu.HASAR1.InformacionRemito(1) = Remito_Uno.Text
    Menu.HASAR1.InformacionRemito(2) = Remito_Dos.Text
    Menu.HASAR1.InformacionRemito(3) = Remito_Tres.Text
    Menu.HASAR1.InformacionRemito(4) = Remito_Cuatro.Text
    
    'Indico el comprobante a imprimir
    If Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "FC" And Trim(UCase(Fac_Vta.Letra.Text)) = "A" Then
        Menu.HASAR1.AbrirComprobanteFiscal FACTURA_A
    ElseIf Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "FC" And Trim(UCase(Fac_Vta.Letra.Text)) = "B" Then
        Menu.HASAR1.AbrirComprobanteFiscal FACTURA_B
    ElseIf Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "ND" And Trim(UCase(Fac_Vta.Letra.Text)) = "A" Then
        Menu.HASAR1.AbrirComprobanteFiscal NOTA_DEBITO_A
    ElseIf Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "ND" And Trim(UCase(Fac_Vta.Letra.Text)) = "B" Then
        Menu.HASAR1.AbrirComprobanteFiscal NOTA_DEBITO_B
    ElseIf Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "NC" And Trim(UCase(Fac_Vta.Letra.Text)) = "A" Then
        Menu.HASAR1.AbrirComprobanteNoFiscalHomologado NOTA_CREDITO_A
    ElseIf Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "NC" And Trim(UCase(Fac_Vta.Letra.Text)) = "B" Then
        Menu.HASAR1.AbrirComprobanteNoFiscalHomologado NOTA_CREDITO_B
    End If
    
    i = 0
    
    For i = 0 To Fac_Vta.List1.ListCount - 1
    
        Articulo_Desc = ""
  
        If Val(Mid(Fac_Vta.List1.List(Fac_Vta.List1.ListIndex), 1, 4)) > 0 Then
            Articulo_Desc = Val(Mid(Fac_Vta.List1.List(Fac_Vta.List1.ListIndex), 1, 4)) & "/" & Val(Mid(Fac_Vta.List1.List(Fac_Vta.List1.ListIndex), 6, 5))
        End If
  
        Articulo_Desc = Trim(Articulo_Desc) + Space(50 - Len(Trim(Articulo_Desc))) & " " & Trim(Mid(Fac_Vta.List1.List(i), 12, 50))
   
        Articulo_Importe = Trim(Mid(Fac_Vta.List1.List(i), 73, 10))
        If UCase(Trim(Fac_Vta.Letra.Text)) = "A" Then
            Articulo_Importe = (Val((Articulo_Importe) * Val(Alicuota_Iva)) / 100) + Val(Articulo_Importe)
        End If
        Articulo_Cant = Trim(Mid(Fac_Vta.List1.List(i), 84, 10))
   
        Menu.HASAR1.ImprimirItem Articulo_Desc, Articulo_Cant, Articulo_Importe, Val(Alicuota_Iva), 0#
    Next
    
    Menu.HASAR1.Subtotal True
    
    If Val(txtBonificacionGeneral.Text) > 0 And Not Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "NC" Then
        
        Dim dblNeto As Double
        Dim dblTotalFactura As Double
        Dim dblImporteBonificacion As Double
        Dim dblIvaBonificacion As Double
        Dim strDescripcionBonificacion As String
        
        dblNeto = Val(Fac_Vta.Neto_Gravado.Text)
        dblTotalFactura = dblNeto + (dblNeto * Alicuota_Iva) / 100
        
        dblImporteBonificacion = dblTotalFactura * Val(txtBonificacionGeneral.Text) / 100
        dblIvaBonificacion = (dblImporteBonificacion * Alicuota_Iva) / 100
        dblIvaBonificacion = Formateado(Str(dblIvaBonificacion), 2, 0, "", False)
        
        strDescripcionBonificacion = Trim(txtBonificacionDescripcion.Text)
        
        If strDescripcionBonificacion = Empty Then
            strDescripcionBonificacion = "** BONIFICACION " & txtBonificacionGeneral.Text & "% **"
        End If
        
        Menu.HASAR1.DevolucionDescuento strDescripcionBonificacion, dblImporteBonificacion, Alicuota_Iva, 0, True, DESCUENTO_RECARGO
                
        ImporteBonificacionFactura = dblImporteBonificacion
        Fac_Vta.Neto_Gravado.Text = dblNeto - (dblImporteBonificacion - dblIvaBonificacion)
        Fac_Vta.Exento.Text = 0
        Fac_Vta.Sub_Total.Text = Val(Fac_Vta.Neto_Gravado.Text) + Val(Fac_Vta.Exento.Text)
        Fac_Vta.Iva_Inscrip.Text = (Fac_Vta.Neto_Gravado.Text * Alicuota_Iva) / 100
        Fac_Vta.Retencion_Iva.Text = 0
        Fac_Vta.Retencion_IIBB.Text = 0
        Fac_Vta.Total_Factura.Text = Val(Fac_Vta.Sub_Total.Text) + Val(Fac_Vta.Iva_Inscrip.Text)
    
    Else
        ImporteBonificacionFactura = 0
    End If
    
    If Val(Fac_Vta.Plazo.Text) = 0 Then
        Menu.HASAR1.ImprimirPago "EFECTIVO: ", Trim(Str(Val(Fac_Vta.Total_Factura.Text)))
    Else
        Menu.HASAR1.ImprimirPago "CTA. CTE.: ", Trim(Str(Val(Fac_Vta.Total_Factura.Text)))
    End If
    
    If Not UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "ND" Then
        qy = "UPDATE Empresa SET "
        If Trim(UCase(Fac_Vta.Letra.Text)) = "A" And UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "FC" Then
            'Qy = Qy & "Ultima_FC_A = '" & trim(Fac_Vta.Numero.Text) & "' "
                qy = qy & "Ultima_FC_A = '" & Trim(Menu.HASAR1.UltimaFactura) & "'"
        ElseIf Trim(UCase(Fac_Vta.Letra.Text)) = "B" And UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "FC" Then
            'Qy = Qy & "Ultima_FC_B = '" & trim(Fac_Vta.Numero.Text) & "' "
                qy = qy & "Ultima_FC_B = '" & Trim(Menu.HASAR1.UltimoTicket) & "'"
        ElseIf Trim(UCase(Fac_Vta.Letra.Text)) = "A" And UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "NC" Then
            qy = qy & "Ultima_NC_A = '" & Trim(Fac_Vta.Numero.Text) & "' "
        ElseIf Trim(UCase(Fac_Vta.Letra.Text)) = "B" And UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "NC" Then
            qy = qy & "Ultima_NC_B = '" & Trim(Fac_Vta.Numero.Text) & "' "
        End If
        
        qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        Db.Execute (qy)
    End If
    
    If Trim(UCase(Mid(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "NC" Then
        Menu.HASAR1.CerrarComprobanteNoFiscalHomologado
    Else
        Menu.HASAR1.CerrarComprobanteFiscal
    End If
    
    Menu.HASAR1.Finalizar
End Sub

    
'------------------------------------------------------------------------------------
'                                   Control De Eventos
'------------------------------------------------------------------------------------


Private Sub Targeta_GotFocus()
    Nro_Targeta.Enabled = True
    Titular_Targeta.Enabled = True
    Denominacion.Enabled = True
    Nro_Targeta.BackColor = vbWhite
    Titular_Targeta.BackColor = vbWhite
    Denominacion.BackColor = vbWhite

    Targeta.Text = Trim(Targeta.Text)
    Targeta.SelStart = 0
    Targeta.SelLength = Len(Targeta.Text)
End Sub

Private Sub Targeta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Targeta_LostFocus()
    Targeta.Text = Formateado(Str(Val(Targeta.Text)), 2, 10, " ", False)
    
    If Val(Targeta.Text) = 0 Then
        Nro_Targeta.Enabled = False
        Titular_Targeta.Enabled = False
        Denominacion.Enabled = False
        Nro_Targeta.BackColor = &HE0E0E0
        Titular_Targeta.BackColor = &HE0E0E0
        Denominacion.BackColor = &HE0E0E0
    End If
End Sub

Private Sub Text1_Change()

End Sub

Private Sub Titular_Targeta_GotFocus()
    Titular_Targeta.Text = Trim(Titular_Targeta.Text)
    Titular_Targeta.SelStart = 0
    Titular_Targeta.SelLength = Len(Titular_Targeta.Text)
End Sub

Private Sub Titular_Targeta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Titular_Targeta_LostFocus()
    Titular_Targeta.Text = FiltroCaracter(Titular_Targeta.Text)
End Sub

Private Sub Titular_GotFocus()
    Titular.Text = Trim(Titular.Text)
    Titular.SelStart = 0
    Titular.SelLength = Len(Titular.Text)
End Sub

Private Sub Titular_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Titular_LostFocus()
    Titular.Text = FiltroCaracter(Titular.Text)
End Sub

Private Sub txtBonificacionDescripcion_GotFocus()
    txtBonificacionDescripcion.Text = Trim(txtBonificacionDescripcion.Text)
    txtBonificacionDescripcion.SelStart = 0
    txtBonificacionDescripcion.SelLength = Len(txtBonificacionDescripcion.Text)
End Sub

Private Sub txtBonificacionDescripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub txtBonificacionDescripcion_LostFocus()
    txtBonificacionDescripcion.Text = FiltroCaracter(txtBonificacionDescripcion.Text)
End Sub

Private Sub txtBonificacionGeneral_GotFocus()
    txtBonificacionGeneral.Text = Trim(txtBonificacionGeneral.Text)
    txtBonificacionGeneral.SelStart = 0
    txtBonificacionGeneral.SelLength = Len(txtBonificacionGeneral.Text)
End Sub

Private Sub txtBonificacionGeneral_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub txtBonificacionGeneral_LostFocus()
    txtBonificacionGeneral.Text = Formateado(Str(Val(txtBonificacionGeneral.Text)), 2, 6, " ", False)
End Sub

Private Sub Linea_Cuatro_GotFocus()
    Linea_Cuatro.Text = Trim(Linea_Cuatro.Text)
    Linea_Cuatro.SelStart = 0
    Linea_Cuatro.SelText = ""
    Linea_Cuatro.SelLength = Len(Linea_Cuatro.Text)
End Sub

Private Sub Linea_Cuatro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Linea_Cuatro_LostFocus()
    Linea_Cuatro.Text = FiltroCaracter(Linea_Cuatro.Text)
End Sub

Private Sub Linea_Dos_GotFocus()
    Linea_Dos.Text = Trim(Linea_Dos.Text)
    Linea_Dos.SelStart = 0
    Linea_Dos.SelText = ""
    Linea_Dos.SelLength = Len(Linea_Dos.Text)
End Sub

Private Sub Linea_Dos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Linea_Dos_LostFocus()
    Linea_Dos.Text = FiltroCaracter(Linea_Dos.Text)
End Sub

Private Sub Linea_Tres_GotFocus()
    Linea_Tres.Text = Trim(Linea_Tres.Text)
    Linea_Tres.SelStart = 0
    Linea_Tres.SelText = ""
    Linea_Tres.SelLength = Len(Linea_Tres.Text)
End Sub

Private Sub Linea_Tres_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Linea_Tres_LostFocus()
    Linea_Tres.Text = FiltroCaracter(Linea_Tres.Text)
End Sub

Private Sub Linea_Uno_GotFocus()
    Linea_Uno.Text = Trim(Linea_Uno.Text)
    Linea_Uno.SelStart = 0
    Linea_Uno.SelText = ""
    Linea_Uno.SelLength = Len(Linea_Uno.Text)
End Sub

Private Sub Linea_Uno_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Linea_Uno_LostFocus()
    Linea_Uno.Text = FiltroCaracter(Linea_Uno.Text)
End Sub

Private Sub Nro_Cheque_GotFocus()
    Nro_Cheque.Text = Trim(Nro_Cheque.Text)
    Nro_Cheque.SelStart = 0
    Nro_Cheque.SelLength = Len(Nro_Cheque.Text)
End Sub

Private Sub Nro_Cheque_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nro_Cheque_LostFocus()
    Nro_Cheque.Text = Formateado(Str(Val(Nro_Cheque.Text)), 0, 10, " ", False)
End Sub

Private Sub Nro_Targeta_GotFocus()
    Nro_Targeta.Text = Trim(Nro_Targeta.Text)
    Nro_Targeta.SelStart = 0
    Nro_Targeta.SelLength = Len(Nro_Targeta.Text)
End Sub

Private Sub Nro_Targeta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nro_Targeta_LostFocus()
    Nro_Targeta.Text = FiltroCaracter(Nro_Targeta.Text)
End Sub

Private Sub Remito_Cuatro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Remito_Dos_GotFocus()
    Remito_Dos.Text = Trim(Remito_Dos.Text)
    Remito_Dos.SelStart = 0
    Remito_Dos.SelText = ""
    Remito_Dos.SelLength = Len(Remito_Dos.Text)
End Sub

Private Sub Remito_Dos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Remito_Dos_LostFocus()
    Remito_Dos.Text = FiltroCaracter(Remito_Dos.Text)
End Sub

Private Sub Remito_Tres_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Remito_Uno_GotFocus()
    Remito_Uno.Text = Trim(Remito_Uno.Text)
    Remito_Uno.SelStart = 0
    Remito_Uno.SelText = ""
    Remito_Uno.SelLength = Len(Remito_Uno.Text)
End Sub

Private Sub Remito_Uno_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Remito_Uno_LostFocus()
    Remito_Uno.Text = FiltroCaracter(Remito_Uno.Text)
End Sub

Private Sub Banco_GotFocus()
    Banco.Text = Trim(Banco.Text)
    Banco.SelStart = 0
    Banco.SelLength = Len(Banco.Text)
End Sub

Private Sub Banco_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Banco_LostFocus()
    Banco.Text = FiltroCaracter(Banco.Text)
End Sub

Private Sub Cancelar_Click()
    If Fac_Vta.Cliente.Text = "" Then
        Unload Fac_Vta
    Else
        Me.OperacionConfirmada = False
        Unload Me
    End If
End Sub

Private Sub Cheque_GotFocus()
    Nro_Cheque.Enabled = True
    Titular.Enabled = True
    Emision.Enabled = True
    Cobranza.Enabled = True
    Banco.Enabled = True
    Nro_Cheque.BackColor = vbWhite
    Titular.BackColor = vbWhite
    Emision.BackColor = vbWhite
    Cobranza.BackColor = vbWhite
    Banco.BackColor = vbWhite
    
    Cheque.Text = Trim(Cheque.Text)
    Cheque.SelStart = 0
    Cheque.SelLength = Len(Cheque.Text)
End Sub

Private Sub Cheque_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cheque_LostFocus()
    Cheque.Text = Formateado(Str(Val(Cheque.Text)), 2, 10, " ", False)
    
    If Val(Cheque.Text) = 0 Then
        Nro_Cheque.Enabled = False
        Titular.Enabled = False
        Emision.Enabled = False
        Cobranza.Enabled = False
        Banco.Enabled = False
        
        Nro_Cheque.BackColor = &HE0E0E0
        Titular.BackColor = &HE0E0E0
        Emision.BackColor = &HE0E0E0
        Cobranza.BackColor = &HE0E0E0
        Banco.BackColor = &HE0E0E0
    End If
End Sub

Private Sub Cobranza_GotFocus()
    If Val(Cobranza.Text) = 0 And Val(Cheque.Text) > 0 Then Cobranza.Text = DateAdd("d", 30, Emision.Text)
    Cobranza.Text = Trim(Cobranza.Text)
    Cobranza.SelStart = 0
    Cobranza.SelLength = Len(Cobranza.Text)
End Sub

Private Sub Cobranza_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cobranza_LostFocus()
    Cobranza.Text = ValidarFecha(Cobranza.Text)
End Sub

Private Sub Denominacion_GotFocus()
    Denominacion.Text = Trim(Denominacion.Text)
    Denominacion.SelStart = 0
    Denominacion.SelLength = Len(Denominacion.Text)
End Sub

Private Sub Denominacion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Denominacion_LostFocus()
    Denominacion.Text = FiltroCaracter(Denominacion.Text)
End Sub

Private Sub Efectivo_GotFocus()
    Efectivo.Text = Trim(Efectivo.Text)
    Efectivo.SelStart = 0
    Efectivo.SelLength = Len(Efectivo.Text)
End Sub

Private Sub Efectivo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Efectivo_LostFocus()
    Efectivo.Text = Formateado(Str(Val(Efectivo.Text)), 2, 10, " ", False)
End Sub

Private Sub Emision_GotFocus()
    If Val(Emision.Text) = 0 And Val(Cheque.Text) > 0 Then Emision.Text = Fecha_Fiscal
    Emision.Text = Trim(Emision.Text)
    Emision.SelStart = 0
    Emision.SelLength = Len(Emision.Text)
End Sub

Private Sub Emision_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Emision_LostFocus()
    Emision.Text = ValidarFecha(Emision.Text)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Observaciones y confirma de la emisi�n del comprobante.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    
    dtPeriodo.Value = Fac_Vta.Fecha.Text
    
    Linea_Uno.Text = "CLIENTE: " & Trim(Fac_Vta.Cliente.Text)
    If Val(Fac_Vta.Plazo.Text) > 0 Then
        Marco_Forma_Pago.Enabled = False
        
        Efectivo.BackColor = &HE0E0E0
        Cheque.BackColor = &HE0E0E0
        Titular.BackColor = &HE0E0E0
        Banco.BackColor = &HE0E0E0
        Emision.BackColor = &HE0E0E0
        Cobranza.BackColor = &HE0E0E0
        Nro_Cheque.BackColor = &HE0E0E0
        Targeta.BackColor = &HE0E0E0
        Nro_Targeta.BackColor = &HE0E0E0
        Titular_Targeta.BackColor = &HE0E0E0
        Denominacion.BackColor = &HE0E0E0
        Linea_Dos.Text = "ESTA FACTURA VENCE EL D�A: " & Trim(Fac_Vta.Vto.Text)
    Else
        Linea_Dos.Text = "ESTA FACTURA ES DE CONTADO."
        Efectivo.Text = Fac_Vta.Total_Factura.Text
    End If
    
    If Val(Fac_Vta.Total_Factura.Text) > 0 Then
        'Linea_Tres.Text = "IMPORTE: " & Trim(UCase(NumerosALetras(Val(Fac_Vta.Total_Factura.Text), False, False)))
    End If
    
    qy = "SELECT * FROM Banco ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Banco.AddItem Trim(Mid(Rs("Denominacion"), 1, 25)) + Space(25 - Len(Trim(Mid(Rs("Denominacion"), 1, 25)))) & Trim(Rs("Id_Banco"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Aceptar_Click()

    If Val(txtBonificacionGeneral.Text) < 0 Then
        MsgBox "El porcentaje de bonificacion debe ser mayor a cero", vbCritical, "Atenci�n!"
        Exit Sub
    End If
    
    Remito_Uno.Enabled = False
    Remito_Dos.Enabled = False
    Remito_Tres.Enabled = False
    Remito_Cuatro.Enabled = False
    
    Efectivo.Enabled = False
    Cheque.Enabled = False
    Nro_Cheque.Enabled = False
    Banco.Enabled = False
    Titular.Enabled = False
    Emision.Enabled = False
    Cobranza.Enabled = False
    Targeta.Enabled = False
    Nro_Targeta.Enabled = False
    Titular_Targeta.Enabled = False
    Denominacion.Enabled = False
    Linea_Uno.Enabled = False
    Linea_Dos.Enabled = False
    Linea_Tres.Enabled = False
    Linea_Cuatro.Enabled = False
    
    Aceptar.Enabled = False
    Cancelar.Enabled = False
        
    If (RunTimeEnviropment = Prodution) Then
        
        If Val(Id_Empresa) = 1 Then
           Imprimir_Hasar
        End If
        
        If Val(Id_Empresa) = 2 Then
            Imprimir_Comun
        End If
        
    Else
        
        MsgBox "El comprobante no ha sido impreso porque se encuentra en un ambiente no productivo.", vbInformation, "Atenci�n"
        
    End If
        
    
    Grabar_Comprobante
    
    Me.OperacionConfirmada = True
        
    Unload Me
    
End Sub

