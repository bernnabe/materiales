VERSION 5.00
Begin VB.Form Dat_Art_2 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaci�n de Art�culos.-"
   ClientHeight    =   7215
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8790
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7215
   ScaleWidth      =   8790
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Articulo 
      Height          =   975
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   8775
      Begin VB.CommandButton Buscar_Articulos 
         Caption         =   "Art�culo:"
         Height          =   255
         Left            =   120
         TabIndex        =   39
         ToolTipText     =   "Mostrar lista de Art�culos, seleccionando un rubro se muestran los art�culos correspondientes al mismo.-"
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox Fecha_Alta 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7320
         TabIndex        =   37
         Top             =   600
         Width           =   1335
      End
      Begin VB.ComboBox Articulos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   36
         Top             =   600
         Visible         =   0   'False
         Width           =   4575
      End
      Begin VB.TextBox Descripcion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   3
         Top             =   600
         Width           =   4575
      End
      Begin VB.TextBox Articulo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   1
         Top             =   600
         Width           =   735
      End
      Begin VB.ComboBox Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   0
         Top             =   240
         Width           =   4575
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   41
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Fecha de Alta"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7320
         TabIndex        =   38
         Top             =   360
         Width           =   1335
      End
   End
   Begin VB.Frame Marco_Datos 
      Height          =   5295
      Left            =   0
      TabIndex        =   35
      Top             =   960
      Width           =   8775
      Begin VB.TextBox Memo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   5055
         Left            =   5160
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   69
         Top             =   -4920
         Visible         =   0   'False
         Width           =   8535
      End
      Begin VB.TextBox Precio_Compra 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   16
         Top             =   2640
         Width           =   1215
      End
      Begin VB.ComboBox Proveedor 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   20
         Top             =   3000
         Width           =   6735
      End
      Begin VB.TextBox Margen_Util_Rev 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6840
         MaxLength       =   6
         TabIndex        =   11
         Top             =   1680
         Width           =   855
      End
      Begin VB.TextBox Margen_Util_Pub 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6840
         MaxLength       =   6
         TabIndex        =   15
         Top             =   2040
         Width           =   855
      End
      Begin VB.Frame Marco_Recargos 
         Enabled         =   0   'False
         Height          =   1095
         Left            =   0
         TabIndex        =   53
         Top             =   3360
         Width           =   8775
         Begin VB.TextBox Recargo_Largo_Mayor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7800
            TabIndex        =   28
            Top             =   600
            Width           =   855
         End
         Begin VB.TextBox Largo_Mayor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5880
            TabIndex        =   27
            Top             =   600
            Width           =   1095
         End
         Begin VB.TextBox Recargo_Ancho_Mayor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7800
            TabIndex        =   24
            Top             =   240
            Width           =   855
         End
         Begin VB.TextBox Ancho_Mayor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5880
            TabIndex        =   23
            Top             =   240
            Width           =   1095
         End
         Begin VB.TextBox Recargo_Largo_Menor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3720
            TabIndex        =   26
            Top             =   600
            Width           =   855
         End
         Begin VB.TextBox Recargo_Ancho_Menor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3720
            TabIndex        =   22
            Top             =   240
            Width           =   855
         End
         Begin VB.TextBox Largo_Menor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1920
            TabIndex        =   25
            Top             =   600
            Width           =   1095
         End
         Begin VB.TextBox Ancho_Menor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1920
            TabIndex        =   21
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Caption         =   "% Rec.:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   0
            Left            =   3000
            TabIndex        =   61
            Top             =   600
            Width           =   855
         End
         Begin VB.Label Label22 
            Alignment       =   1  'Right Justify
            Caption         =   "Largo > de:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4800
            TabIndex        =   60
            Top             =   600
            Width           =   975
         End
         Begin VB.Label Label21 
            Alignment       =   1  'Right Justify
            Caption         =   "Ancho > de:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4800
            TabIndex        =   59
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label20 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Largo < de:"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   960
            TabIndex        =   58
            Top             =   600
            Width           =   810
         End
         Begin VB.Label Label19 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Ancho < de:"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   960
            TabIndex        =   57
            Top             =   240
            Width           =   870
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Caption         =   "% Rec.:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   2
            Left            =   2880
            TabIndex        =   56
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Caption         =   "% Rec.:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   1
            Left            =   6960
            TabIndex        =   55
            Top             =   600
            Width           =   855
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Caption         =   "% Rec.:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   3
            Left            =   6840
            TabIndex        =   54
            Top             =   240
            Width           =   1095
         End
      End
      Begin VB.TextBox Precio_Cta_Pub 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5520
         MaxLength       =   9
         TabIndex        =   14
         Top             =   2040
         Width           =   1215
      End
      Begin VB.CheckBox Integra_Listado 
         Alignment       =   1  'Right Justify
         Caption         =   "Integra Listado:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7200
         TabIndex        =   19
         Top             =   2640
         Width           =   1455
      End
      Begin VB.TextBox Stock_Minimo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5640
         MaxLength       =   9
         TabIndex        =   18
         Top             =   2640
         Width           =   1215
      End
      Begin VB.TextBox Existencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         MaxLength       =   9
         TabIndex        =   17
         Top             =   2640
         Width           =   1215
      End
      Begin VB.TextBox Precio_Ctdo_Pub 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         MaxLength       =   9
         TabIndex        =   13
         Top             =   2040
         Width           =   1215
      End
      Begin VB.TextBox Precio_Cta_Rev 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5520
         MaxLength       =   9
         TabIndex        =   10
         Top             =   1680
         Width           =   1215
      End
      Begin VB.ComboBox Presentacion_Publicos 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   12
         Top             =   2040
         Width           =   2295
      End
      Begin VB.ComboBox Presentacion_Revendedor 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   8
         Top             =   1680
         Width           =   2295
      End
      Begin VB.TextBox Precio_Ctdo_Rev 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         MaxLength       =   9
         TabIndex        =   9
         Top             =   1680
         Width           =   1215
      End
      Begin VB.TextBox Largo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7680
         MaxLength       =   7
         TabIndex        =   7
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox Ancho 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4800
         MaxLength       =   7
         TabIndex        =   6
         Top             =   960
         Width           =   975
      End
      Begin VB.TextBox Espesor 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         MaxLength       =   7
         TabIndex        =   5
         Top             =   960
         Width           =   975
      End
      Begin VB.ComboBox Sistema_Medida 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   4
         Top             =   240
         Width           =   6735
      End
      Begin VB.Frame Frame1 
         Height          =   975
         Left            =   0
         TabIndex        =   66
         Top             =   4320
         Width           =   8775
         Begin VB.ComboBox Cuenta_Venta 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1920
            TabIndex        =   30
            Top             =   600
            Width           =   6735
         End
         Begin VB.ComboBox Cuenta_Compra 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1920
            TabIndex        =   29
            Top             =   240
            Width           =   6735
         End
         Begin VB.Label Label24 
            Alignment       =   1  'Right Justify
            Caption         =   "Cuenta Ventas:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   68
            Top             =   600
            Width           =   1695
         End
         Begin VB.Label Label18 
            Alignment       =   1  'Right Justify
            Caption         =   "Cuenta Compras:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   67
            Top             =   240
            Width           =   1695
         End
      End
      Begin VB.Label Label17 
         Alignment       =   1  'Right Justify
         Caption         =   "Precio de Compra:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   65
         Top             =   2640
         Width           =   1695
      End
      Begin VB.Label Label16 
         Alignment       =   1  'Right Justify
         Caption         =   "Proveedor:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   63
         Top             =   3000
         Width           =   1695
      End
      Begin VB.Label Label15 
         Alignment       =   2  'Center
         Caption         =   "% Margen Util."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6720
         TabIndex        =   62
         Top             =   1440
         Width           =   1095
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "Stock M�nimo (Avisa):"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5400
         TabIndex        =   52
         Top             =   2400
         Width           =   1695
      End
      Begin VB.Label Label13 
         Alignment       =   2  'Center
         Caption         =   "Existencia:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3480
         TabIndex        =   51
         Top             =   2400
         Width           =   1695
      End
      Begin VB.Label Label12 
         Alignment       =   2  'Center
         Caption         =   "Largo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7680
         TabIndex        =   50
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label11 
         Alignment       =   2  'Center
         Caption         =   "Ancho:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4800
         TabIndex        =   49
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label10 
         Alignment       =   2  'Center
         Caption         =   "Espesor:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1920
         TabIndex        =   48
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Medidas del Art�culo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   47
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label8 
         Alignment       =   2  'Center
         Caption         =   "Precio Cta.Cte."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5520
         TabIndex        =   46
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label7 
         Alignment       =   2  'Center
         Caption         =   "Precio Ctdo."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4320
         TabIndex        =   45
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label6 
         Caption         =   "Presentaci�n"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1920
         TabIndex        =   44
         Top             =   1440
         Width           =   3615
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Revendedores:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   1680
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "P�blicos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   42
         Top             =   2040
         Width           =   1695
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Sistema de Medida:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   240
         Width           =   1695
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   64
      Top             =   6240
      Width           =   8775
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   6240
         Picture         =   "Dat_Art_2.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   32
         ToolTipText     =   "Borrar el Art�culo.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   7560
         Picture         =   "Dat_Art_2.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   33
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   5160
         Picture         =   "Dat_Art_2.frx":0614
         Style           =   1  'Graphical
         TabIndex        =   31
         ToolTipText     =   "Grabar los Datos.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Comentarios 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Dat_Art_2.frx":091E
         Style           =   1  'Graphical
         TabIndex        =   34
         ToolTipText     =   "Mostrar el Campo Comentarios.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Dat_Art_2"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Ancho_GotFocus()
    Ancho.Text = Trim$(Ancho.Text)
    Ancho.SelStart = 0
    Ancho.SelText = ""
    Ancho.SelLength = Len(Ancho.Text)
End Sub

Private Sub Ancho_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_LostFocus()
    Ancho.Text = Formateado(Str$(Val(Ancho.Text)), 2, 7, " ", False)
End Sub

Private Sub Articulo_GotFocus()
    Articulo.Text = Trim$(Articulo.Text)
    
    Articulo.SelStart = 0
    Articulo.SelText = ""
    Articulo.SelLength = Len(Articulo.Text)
End Sub

Private Sub Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: If Val(Articulo.Text) > 0 Then SendKeys "{TAB}"
End Sub

Private Sub Articulo_LostFocus()
    If Val(Articulo.Text) > 0 Then
        Leer_Articulo
    ElseIf Val(Mid$(Rubro.Text, 31, 5)) > 0 Then
        Qy = "SELECT MAX(Id_Articulo) FROM Articulo WHERE Id_Rubro = " & Trim$(Str$(Val(Mid$(Rubro.Text, 31, 5))))
        Set Rs = Db.OpenRecordset(Qy)
        
        Articulo.Text = 1
        If Not IsNull(Rs.Fields(0)) = True Then Articulo.Text = Val(Rs.Fields(0)) + 1
    End If
End Sub

Private Sub Leer_Articulo()
    If Val(Articulo.Text) > 0 And Val(Mid$(Rubro.Text, 31, 5)) > 0 Then
        
        Qy = "SELECT * FROM Articulo WHERE Id_Articulo = " & Trim$(Str$(Val(Articulo.Text))) & " "
        Qy = Qy & "AND Id_Rubro = " & Trim$(Str$(Val(Mid$(Rubro.Text, 31, 5))))
        Set Rs = Db.OpenRecordset(Qy)
        
        If Rs.EOF Then
            If MsgBox("El Art�culo es inexistente, desea incorporarlo ahora ?", vbInformation + vbYesNo, "Atenci�n.!") = vbYes Then
                Grabar.Tag = "Alta"
                Fecha_Alta.Text = Fecha_Fiscal
                Marco_Datos.Enabled = True
                Grabar.Enabled = True
                Comentarios.Enabled = True
                Descripcion.SetFocus
            Else
                Salir_Click
            End If
        Else
            Marco_Datos.Enabled = True
            Grabar.Tag = "Modificaci�n"
            Grabar.Enabled = True
            Borrar.Enabled = True
            Comentarios.Enabled = True
            Mostrar_Articulo
            Descripcion.SetFocus
        End If
    Else
        Rubro.Text = ""
        Articulo.Text = ""
        Rubro.SetFocus
    End If
End Sub

Private Sub Mostrar_Articulo()
    Dim i      As Integer
    Dim Nivel  As String
    
    Descripcion.Text = Trim$(Rs.Fields("Descripcion"))
    
    Sistema_Medida.ListIndex = Val(Rs.Fields("Sistema_Medida"))
    Sistema_Medida_LostFocus
    
    If Sistema_Medida.ListIndex = 0 Then
        Espesor.Text = Formateado(Str$(Val(Rs.Fields("Espesor"))), 2, 7, " ", False)
        Ancho.Text = Formateado(Str$(Val(Rs.Fields("Ancho"))), 2, 7, " ", False)
        Largo.Text = Formateado(Str$(Val(Rs.Fields("Largo"))), 2, 7, " ", False)
    End If
    
    Presentacion_Revendedor.ListIndex = Val(Rs.Fields("Presentacion_Revendedor"))
    Precio_Ctdo_Rev.Text = Formateado(Str$(Val(Rs.Fields("Precio_Ctdo_Rev"))), 2, 9, " ", False)
    Precio_Cta_Rev.Text = Formateado(Str$(Val(Rs.Fields("Precio_CtaCte_Rev"))), 2, 9, " ", False)
    Margen_Util_Rev.Text = Formateado(Str$(Val(Rs.Fields("Margen_util_Rev"))), 2, 6, " ", False)
    
    Presentacion_Publicos.ListIndex = Val(Rs.Fields("Presentacion_Publico"))
    Precio_Ctdo_Pub.Text = Formateado(Str$(Val(Rs.Fields("Precio_Ctdo_Pub"))), 2, 9, " ", False)
    Precio_Cta_Pub.Text = Formateado(Str$(Val(Rs.Fields("Precio_CtaCte_Pub"))), 2, 9, " ", False)
    Margen_Util_Pub.Text = Formateado(Str$(Val(Rs.Fields("Margen_util_Pub"))), 2, 6, " ", False)
    
    Precio_Compra.Text = Formateado(Str$(Val(Rs.Fields("Precio_Compra"))), 2, 9, " ", False)
    Existencia.Text = Formateado(Str$(Val(Rs.Fields("Existencia"))), 2, 9, " ", False)
    Stock_Minimo.Text = Formateado(Str$(Val(Rs.Fields("Stock_Minimo"))), 2, 9, " ", False)
    
    Integra_Listado.Value = Val(Rs.Fields("Integra_Listado"))
    
    Ancho_Menor.Text = Formateado(Str$(Val(Rs.Fields("Ancho_Menor"))), 2, 8, " ", False)
    Recargo_Ancho_Menor.Text = Formateado(Str$(Val(Rs.Fields("Rec_Ancho_Menor"))), 2, 6, " ", False)
    Ancho_Mayor.Text = Formateado(Str$(Val(Rs.Fields("Ancho_Mayor"))), 2, 8, " ", False)
    Recargo_Ancho_Mayor.Text = Formateado(Str$(Val(Rs.Fields("Rec_Ancho_Mayor"))), 2, 6, " ", False)
    
    Largo_Menor.Text = Formateado(Str$(Val(Rs.Fields("Largo_Menor"))), 2, 8, " ", False)
    Recargo_Largo_Menor.Text = Formateado(Str$(Val(Rs.Fields("Rec_Largo_Menor"))), 2, 6, " ", False)
    Largo_Mayor.Text = Formateado(Str$(Val(Rs.Fields("Largo_Mayor"))), 2, 8, " ", False)
    Recargo_Largo_Mayor.Text = Formateado(Str$(Val(Rs.Fields("Rec_Largo_Mayor"))), 2, 6, " ", False)
    
    Proveedor.Text = Val(Rs.Fields("Proveedor"))
    Cuenta_Compra.Text = Trim$(Rs.Fields("Cuenta_Compra"))
    Cuenta_Venta.Text = Trim$(Rs.Fields("Cuenta_Venta"))
    Fecha_Alta.Text = Format(Rs.Fields("Fecha_Alta"), "dd/mm/yyyy")
    
    If Val(Proveedor.Text) > 0 Then
        Qy = "SELECT * FROM Proveedor WHERE Id_Proveedor = " & Trim$(Str$(Val(Proveedor.Text)))
        Set Rs = Db.OpenRecordset(Qy)
        
        If Not Rs.EOF Then
            Proveedor.Text = Trim$(Rs.Fields("Nombre")) + Space$(30 - Len(Trim$(Rs.Fields("Nombre")))) & " " & Trim$(Rs.Fields("Id_Proveedor"))
        End If
    End If
    
    If Trim$(Cuenta_Compra.Text) <> "" Then
        Nivel = 1
        Cuenta_Compra.Tag = ""
        Qy = "SELECT * FROM Cuenta WHERE Id_Nivel_1 = Id_Nivel_1 "
        
        For i = 1 To Len(Cuenta_Compra.Text)
            If Mid$(Cuenta_Compra.Text, i, 1) <> "." Then
                Cuenta_Compra.Tag = Val(Cuenta_Compra.Tag) & Val(Mid$(Cuenta_Compra.Text, i, 1))
            Else
                Qy = Qy & " AND Id_Nivel_" & Trim$(Nivel) & " = " & Trim$(Str$(Val(Cuenta_Compra.Tag))) & " "
                Cuenta_Compra.Tag = ""
                Nivel = Val(Nivel) + 1
            End If
        Next
        
        Qy = Qy & "AND Id_Nivel_" & Trim$(Nivel) & " = " & Trim$(Str$(Val(Cuenta_Compra.Tag)))
        Set Rs = Db.OpenRecordset(Qy)

        Cuenta_Compra.Text = Trim$(Rs.Fields("Denominacion")) + Space$(30 - Len(Trim$(Rs.Fields("Denominacion")))) & " " & Trim$(Rs.Fields("Id_Nivel_1")) & "." & Trim$(Rs.Fields("Id_Nivel_2")) & "." & Trim$(Rs.Fields("Id_Nivel_3")) & "." & Trim$(Rs.Fields("Id_Nivel_4")) & "." & Trim$(Rs.Fields("Id_Nivel_5")) & "." & Trim$(Rs.Fields("Id_Nivel_6"))
    End If
    
    If Trim$(Cuenta_Venta.Text) <> "" Then
        Nivel = 1
        Cuenta_Venta.Tag = ""
        
        Qy = "SELECT * FROM Cuenta WHERE Id_Nivel_1 = Id_Nivel_1 "
        
        For i = 1 To Len(Cuenta_Venta.Text)
            If Mid$(Cuenta_Venta.Text, i, 1) <> "." Then
                Cuenta_Venta.Tag = Val(Cuenta_Venta.Tag) & Val(Mid$(Cuenta_Venta.Text, i, 1))
            Else
                Qy = Qy & " AND Id_Nivel_" & Trim$(Nivel) & " = " & Trim$(Str$(Val(Cuenta_Venta.Tag))) & " "
                Cuenta_Venta.Tag = ""
                Nivel = Val(Nivel) + 1
            End If
        Next
        
        Qy = Qy & "AND Id_Nivel_" & Trim$(Nivel) & " = " & Trim$(Str$(Val(Cuenta_Venta.Tag)))
        Set Rs = Db.OpenRecordset(Qy)
        
        Cuenta_Venta.Text = Trim$(Rs.Fields("Denominacion")) + Space$(30 - Len(Trim$(Rs.Fields("Denominacion")))) & " " & Trim$(Rs.Fields("Id_Nivel_1")) & "." & Trim$(Rs.Fields("Id_Nivel_2")) & "." & Trim$(Rs.Fields("Id_Nivel_3")) & "." & Trim$(Rs.Fields("Id_Nivel_4")) & "." & Trim$(Rs.Fields("Id_Nivel_5")) & "." & Trim$(Rs.Fields("Id_Nivel_6"))
    End If
End Sub

Private Sub Buscar_Articulos_Click()
    Articulos_Encontrados.Visible = True
    Articulo.Text = ""
    MousePointer = 11
    
    If Trim$(Str$(Val(Mid$(Rubro.Text, 31, 5)))) > 0 Then
        Qy = "SELECT * FROM Articulo WHERE Id_Rubro = " & Trim$(Str$(Val(Mid$(Rubro.Text, 31, 5)))) & " ORDER BY Descripcion"
    Else
        Qy = "SELECT * FROM Articulo ORDER BY Descripcion"
    End If
    Set Rs = Db.OpenRecordset(Qy)
        
    While Not Rs.EOF
        Articulos_Encontrados.AddItem Trim$(Rs.Fields("Descripcion")) + Space$(30 - Len(Trim$(Rs.Fields("Descripcion")))) & " " & Formateado(Str$(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & (Rs.Fields("Id_Articulo"))
        Rs.MoveNext
    Wend
    MousePointer = 0
    Articulos_Encontrados.SetFocus
End Sub

Private Sub Cuenta_Compra_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_Venta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Descripcion_GotFocus()
    Descripcion.SelStart = 0
    Descripcion.SelText = ""
    Descripcion.SelLength = Len(Descripcion.Text)
End Sub

Private Sub Descripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Descripcion_LostFocus()
    Descripcion.Text = FiltroCaracter(Descripcion.Text)
End Sub

Private Sub Espesor_GotFocus()
    Espesor.Text = Trim$(Espesor.Text)
    Espesor.SelStart = 0
    Espesor.SelText = ""
    Espesor.SelLength = Len(Espesor.Text)
End Sub

Private Sub Espesor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Espesor_LostFocus()
    Espesor.Text = Formateado(Str$(Val(Espesor.Text)), 2, 7, " ", False)
End Sub

Private Sub Existencia_GotFocus()
    Existencia.Text = Trim$(Existencia.Text)
    Existencia.SelStart = 0
    Existencia.SelText = ""
    Existencia.SelLength = Len(Existencia.Text)
End Sub

Private Sub Existencia_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Existencia_LostFocus()
    Existencia.Text = Formateado(Str$(Val(Existencia.Text)), 2, 9, " ", False)
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    Me.Top = (Screen.Height - Me.Height) / 60
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Sistema_Medida.AddItem "00 - Ancho X Largo (Axl) "
    Sistema_Medida.AddItem "01 - Unidades (Unds)     "
    
    For i = 0 To 6
        Presentacion_Publicos.AddItem Articulo_Presentacion(i)
        Presentacion_Revendedor.AddItem Articulo_Presentacion(i)
    Next
    
    Cargar_Registros
End Sub

Private Sub Cargar_Registros()
    MousePointer = 11

    Qy = "SELECT * FROM Rubro ORDER BY Denominacion"
    Set Rs = Db.OpenRecordset(Qy)
    
    While Not Rs.EOF
        Rubro.AddItem Trim$(Rs.Fields("Denominacion")) + Space$(30 - Len(Trim$(Rs.Fields("Denominacion")))) & " " & Trim$(Rs.Fields("Id_Rubro"))
        Rs.MoveNext
    Wend
    
    Qy = "SELECT * FROM Proveedor ORDER BY Nombre"
    Set Rs = Db.OpenRecordset(Qy)
    
    While Not Rs.EOF
        Proveedor.AddItem Trim$(Rs.Fields("Nombre")) + Space$(30 - Len(Trim$(Rs.Fields("Nombre")))) & " " & Trim$(Rs.Fields("Id_Proveedor"))
        Rs.MoveNext
    Wend
    
    Qy = "SELECT * FROM Cuenta ORDER BY Denominacion"
    Set Rs = Db.OpenRecordset(Qy)
    
    While Not Rs.EOF
        Cuenta_Compra.AddItem Trim$(Rs.Fields("Denominacion")) + Space$(30 - Len(Trim$(Rs.Fields("Denominacion")))) & " " & Trim$(Rs.Fields("Id_Nivel_1")) & "." & Trim$(Rs.Fields("Id_Nivel_2")) & "." & Trim$(Rs.Fields("Id_Nivel_3")) & "." & Trim$(Rs.Fields("Id_Nivel_4")) & "." & Trim$(Rs.Fields("Id_Nivel_5")) & "." & Trim$(Rs.Fields("Id_Nivel_6"))
        Cuenta_Venta.AddItem Trim$(Rs.Fields("Denominacion")) + Space$(30 - Len(Trim$(Rs.Fields("Denominacion")))) & " " & Trim$(Rs.Fields("Id_Nivel_1")) & "." & Trim$(Rs.Fields("Id_Nivel_2")) & "." & Trim$(Rs.Fields("Id_Nivel_3")) & "." & Trim$(Rs.Fields("Id_Nivel_4")) & "." & Trim$(Rs.Fields("Id_Nivel_5")) & "." & Trim$(Rs.Fields("Id_Nivel_6"))
        Rs.MoveNext
    Wend
    
    MousePointer = 0
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        
        Qy = "INSERT INTO Articulo VALUES ("
        Qy = Qy & Trim$(Str$(Val(Mid$(Rubro.Text, 31, 5))))
        Qy = Qy & ", " & Trim$(Str$(Val(Articulo.Text)))
        Qy = Qy & ", '" & Trim$(Descripcion.Text) & "'"
        Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Sistema_Medida.Text, 1, 2))))
        Qy = Qy & ", " & Trim$(Str$(Val(Espesor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Ancho.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Largo.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Presentacion_Revendedor.Text, 1, 1))))
        Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Presentacion_Publicos.Text, 1, 1))))
        Qy = Qy & ", " & Trim$(Str$(Val(Precio_Compra.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Precio_Ctdo_Rev.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Precio_Cta_Rev.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Margen_Util_Rev.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Precio_Ctdo_Pub.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Precio_Cta_Pub.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Margen_Util_Pub.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Existencia.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Stock_Minimo.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Integra_Listado.Value)))
        Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Proveedor.Text, 31))))
        Qy = Qy & ", " & Trim$(Str$(Val(Ancho_Menor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Recargo_Ancho_Menor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Largo_Menor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Recargo_Largo_Menor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Ancho_Mayor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Recargo_Ancho_Mayor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Largo_Mayor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Recargo_Largo_Mayor.Text)))
        Qy = Qy & ", '" & Trim$(Mid$(Cuenta_Compra.Text, 31)) & "'"
        Qy = Qy & ", '" & Trim$(Mid$(Cuenta_Venta.Text, 31)) & "'"
        Qy = Qy & ", '" & Trim$(Fecha_Alta.Text) & "'"
        Qy = Qy & ", '" & Trim$(Memo.Text) & "')"
        Db.Execute (Qy)
    Else
    
        Qy = "UPDATE Articulo SET "
        Qy = Qy & "Descripcion = '" & Trim$(Descripcion.Text) & "',"
        Qy = Qy & "Sistema_Medida = " & Trim$(Str$(Val(Mid$(Sistema_Medida.Text, 1, 2)))) & ", "
        Qy = Qy & "Espesor = " & Trim$(Str$(Val(Espesor.Text))) & ", "
        Qy = Qy & "Ancho = " & Trim$(Str$(Val(Ancho.Text))) & ", "
        Qy = Qy & "Largo = " & Trim$(Str$(Val(Largo.Text))) & ", "
        Qy = Qy & "Presentacion_Revendedor = " & Trim$(Str$(Val(Mid$(Presentacion_Revendedor.Text, 1, 1)))) & ", "
        Qy = Qy & "Presentacion_Publico = " & Trim$(Str$(Val(Mid$(Presentacion_Publicos.Text, 1, 1)))) & ", "
        Qy = Qy & "Precio_Compra = " & Trim$(Str$(Val(Precio_Compra.Text))) & ", "
        Qy = Qy & "Precio_Ctdo_Rev = " & Trim$(Str$(Val(Precio_Ctdo_Rev.Text))) & ", "
        Qy = Qy & "Precio_CtaCte_Rev = " & Trim$(Str$(Val(Precio_Cta_Rev.Text))) & ", "
        Qy = Qy & "Margen_Util_Rev = " & Trim$(Str$(Val(Margen_Util_Rev.Text))) & ", "
        Qy = Qy & "Precio_Ctdo_Pub = " & Trim$(Str$(Val(Precio_Ctdo_Pub.Text))) & ", "
        Qy = Qy & "Precio_CtaCte_Pub = " & Trim$(Str$(Val(Precio_Cta_Pub.Text))) & ", "
        Qy = Qy & "Margen_Util_Pub = " & Trim$(Str$(Val(Margen_Util_Pub.Text))) & ", "
        Qy = Qy & "Existencia = " & Trim$(Str$(Val(Existencia.Text))) & ", "
        Qy = Qy & "Stock_Minimo = " & Trim$(Str$(Val(Stock_Minimo.Text))) & ", "
        Qy = Qy & "Integra_Listado = " & Trim$(Str$(Val(Integra_Listado.Value))) & ","
        Qy = Qy & "Proveedor = " & Trim$(Str$(Val(Mid$(Proveedor.Text, 31)))) & ", "
        Qy = Qy & "Ancho_Menor = " & Trim$(Str$(Val(Ancho_Menor.Text))) & ", "
        Qy = Qy & "Rec_Ancho_Menor = " & Trim$(Str$(Val(Recargo_Ancho_Menor.Text))) & ", "
        Qy = Qy & "Largo_Menor = " & Trim$(Str$(Val(Largo_Menor.Text))) & ", "
        Qy = Qy & "Rec_Largo_Menor = " & Trim$(Str$(Val(Recargo_Largo_Menor.Text))) & ", "
        Qy = Qy & "Ancho_Mayor = " & Trim$(Str$(Val(Ancho_Mayor.Text))) & ", "
        Qy = Qy & "Rec_Ancho_Mayor = " & Trim$(Str$(Val(Recargo_Ancho_Mayor.Text))) & ", "
        Qy = Qy & "Largo_Mayor = " & Trim$(Str$(Val(Largo_Mayor.Text))) & ", "
        Qy = Qy & "Rec_Largo_Mayor = " & Trim$(Str$(Val(Recargo_Largo_Mayor.Text))) & ", "
        Qy = Qy & "Cuenta_Compra = '" & Trim$(Mid$(Cuenta_Compra.Text, 31)) & "',"
        Qy = Qy & "Cuenta_Venta = '" & Trim$(Mid$(Cuenta_Venta.Text, 31)) & "',"
        Qy = Qy & "Memo = '" & Trim$(Memo.Text) & "' "
        Qy = Qy & "WHERE Id_Articulo = " & Trim$(Str$(Val(Articulo.Text))) & " "
        Qy = Qy & "AND Id_Rubro = " & Trim$(Str$(Val(Mid$(Rubro.Text, 31, 5))))
        Db.Execute (Qy)
    End If
    
    Salir_Click
End Sub

Private Sub Integra_Listado_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_GotFocus()
    Largo.Text = Trim$(Largo.Text)
    Largo.SelStart = 0
    Largo.SelText = ""
    Largo.SelLength = Len(Largo.Text)
End Sub

Private Sub Largo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_LostFocus()
    Largo.Text = Formateado(Str$(Val(Largo.Text)), 2, 7, " ", False)
End Sub

Private Sub Margen_Util_Pub_GotFocus()
    Margen_Util_Pub.Text = Trim$(Margen_Util_Pub.Text)
    Margen_Util_Pub.SelStart = 0
    Margen_Util_Pub.SelText = ""
    Margen_Util_Pub.SelLength = Len(Margen_Util_Pub.Text)
End Sub

Private Sub Margen_Util_Pub_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Margen_Util_Pub_LostFocus()
    Margen_Util_Pub.Text = Formateado(Str$(Val(Margen_Util_Pub.Text)), 2, 6, " ", False)
End Sub

Private Sub Margen_Util_Rev_GotFocus()
    Margen_Util_Rev.Text = Trim$(Margen_Util_Rev.Text)
    Margen_Util_Rev.SelStart = 0
    Margen_Util_Rev.SelText = ""
    Margen_Util_Rev.SelLength = Len(Margen_Util_Rev.Text)
End Sub

Private Sub Margen_Util_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Margen_Util_Rev_LostFocus()
    Margen_Util_Rev.Text = Formateado(Str$(Val(Margen_Util_Rev.Text)), 2, 6, " ", False)
End Sub

Private Sub Precio_Compra_GotFocus()
    Precio_Compra.Text = Trim$(Precio_Compra.Text)
    Precio_Compra.SelStart = 0
    Precio_Compra.SelText = ""
    Precio_Compra.SelLength = Len(Precio_Compra.Text)
End Sub

Private Sub Precio_Compra_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Compra_LostFocus()
    Precio_Compra.Text = Formateado(Str$(Val(Precio_Compra.Text)), 2, 9, " ", False)
End Sub

Private Sub Precio_Cta_Pub_GotFocus()
    Precio_Cta_Pub.Text = Trim$(Precio_Cta_Pub.Text)
    Precio_Cta_Pub.SelStart = 0
    Precio_Cta_Pub.SelText = ""
    Precio_Cta_Pub.SelLength = Len(Precio_Cta_Pub.Text)
End Sub

Private Sub Precio_Cta_Pub_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Cta_Pub_LostFocus()
    Precio_Cta_Pub.Text = Formateado(Str$(Val(Precio_Cta_Pub.Text)), 2, 9, " ", False)
End Sub

Private Sub Precio_Cta_Rev_GotFocus()
    Precio_Cta_Rev.Text = Trim$(Precio_Cta_Rev.Text)
    Precio_Cta_Rev.SelStart = 0
    Precio_Cta_Rev.SelText = ""
    Precio_Cta_Rev.SelLength = Len(Precio_Cta_Rev.Text)
End Sub

Private Sub Precio_Cta_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Cta_Rev_LostFocus()
    Precio_Cta_Rev.Text = Formateado(Str$(Val(Precio_Cta_Rev.Text)), 2, 9, " ", False)
End Sub

Private Sub Precio_Ctdo_Pub_GotFocus()
    Precio_Ctdo_Pub.Text = Trim$(Precio_Ctdo_Pub.Text)
    Precio_Ctdo_Pub.SelStart = 0
    Precio_Ctdo_Pub.SelText = ""
    Precio_Ctdo_Pub.SelLength = Len(Precio_Ctdo_Pub.Text)
End Sub

Private Sub Precio_Ctdo_Pub_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Ctdo_Pub_LostFocus()
    Precio_Ctdo_Pub.Text = Formateado(Str$(Val(Precio_Ctdo_Pub.Text)), 2, 9, " ", False)
End Sub

Private Sub Precio_Ctdo_Rev_GotFocus()
    Precio_Ctdo_Rev.Text = Trim$(Precio_Ctdo_Rev.Text)
    Precio_Ctdo_Rev.SelStart = 0
    Precio_Ctdo_Rev.SelText = ""
    Precio_Ctdo_Rev.SelLength = Len(Precio_Ctdo_Rev.Text)
End Sub

Private Sub Precio_Ctdo_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Ctdo_Rev_LostFocus()
    Precio_Ctdo_Rev.Text = Formateado(Str$(Val(Precio_Ctdo_Rev.Text)), 2, 9, " ", False)
End Sub

Private Sub Presentacion_Publicos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Presentacion_Revendedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Proveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: If Rubro.Text <> "" Then SendKeys "{TAB}"
End Sub

Private Sub Rubro_LostFocus()
    If Val(Rubro.Text) > 0 Then
        Qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & Trim$(Str$(Val(Rubro.Text)))
        Set Rs = Db.OpenRecordset(Qy)
        
        If Not Rs.EOF Then
            Rubro.Text = Trim$(Rs.Fields("Denominacion")) + Space$(30 - Len(Trim$(Rs.Fields("Denominacion")))) & " " & Trim$(Rs.Fields("Id_Rubro"))
        Else
            MsgBox "El Rubro es inexistente...", vbInformation, "Atenci�n.!"
            Rubro.Text = ""
            Rubro.SetFocus
        End If
    End If
End Sub

Private Sub Salir_Click()
    If Rubro.Text = "" And Articulo.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Rubro.Text = ""
    Articulo.Text = ""
    Descripcion.Text = ""
    Sistema_Medida.Text = ""
    
    Espesor.Text = ""
    Espesor.BackColor = 14737632
    Espesor.Enabled = False
    Ancho.Text = ""
    Ancho.BackColor = 14737632
    Ancho.Enabled = False
    Largo.Text = ""
    Largo.BackColor = 14737632
    Largo.Enabled = False
    
    Presentacion_Revendedor.Text = ""
    Precio_Ctdo_Rev.Text = ""
    Precio_Cta_Rev.Text = ""
    Margen_Util_Rev.Text = ""
    Presentacion_Publicos.Text = ""
    Precio_Ctdo_Pub.Text = ""
    Precio_Cta_Pub.Text = ""
    Margen_Util_Pub.Text = ""
    
    Precio_Compra.Text = ""
    Existencia.Text = ""
    Stock_Minimo.Text = ""
    Integra_Listado.Value = 0
    
    Marco_Recargos.Enabled = False
    Ancho_Menor.Text = ""
    Ancho_Menor.BackColor = 14737632
    Recargo_Ancho_Menor.Text = ""
    Recargo_Ancho_Menor.BackColor = 14737632
    Ancho_Mayor.Text = ""
    Ancho_Mayor.BackColor = 14737632
    Recargo_Ancho_Mayor.Text = ""
    Recargo_Ancho_Mayor.BackColor = 14737632
    Largo_Menor.Text = ""
    Largo_Menor.BackColor = 14737632
    Recargo_Largo_Menor.Text = ""
    Recargo_Largo_Menor.BackColor = 14737632
    Largo_Mayor.Text = ""
    Largo_Mayor.BackColor = 14737632
    Recargo_Largo_Mayor.Text = ""
    Recargo_Largo_Mayor.BackColor = 14737632
    
    Fecha_Alta.Text = ""
    Proveedor.Text = ""
    
    Cuenta_Compra.Text = ""
    Cuenta_Compra.Tag = ""
    Cuenta_Venta.Text = ""
    Cuenta_Venta.Tag = ""
        
    Marco_Datos.Enabled = False
    Grabar.Enabled = False
    Comentarios.Enabled = False
    Borrar.Enabled = False
    
    Rubro.SetFocus
End Sub

Private Sub Sistema_Medida_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Sistema_Medida_LostFocus
End Sub

Private Sub Sistema_Medida_LostFocus()
    Dim i As Integer
    
    If Sistema_Medida.ListIndex = 1 Then
        'Presentacion_Publicos.Clear
        Presentacion_Publicos.RemoveItem 1
        Presentacion_Publicos.RemoveItem 2
        Presentacion_Publicos.RemoveItem 6
        
        'Presentacion_Revendedor.Clear
        Presentacion_Revendedor.RemoveItem 1
        Presentacion_Revendedor.RemoveItem 2
        Presentacion_Revendedor.RemoveItem 6
        
        Marco_Recargos.Enabled = False
        
        Espesor.Text = ""
        Espesor.BackColor = 14737632
        Espesor.Enabled = False
        Ancho.Text = ""
        Ancho.BackColor = 14737632
        Ancho.Enabled = False
        Largo.Text = ""
        Largo.BackColor = 14737632
        Largo.Enabled = False
        
        Ancho_Menor.Text = ""
        Ancho_Menor.BackColor = 14737632
        Recargo_Ancho_Menor.Text = ""
        Recargo_Ancho_Menor.BackColor = 14737632
        Ancho_Mayor.Text = ""
        Ancho_Mayor.BackColor = 14737632
        Recargo_Ancho_Mayor.Text = ""
        Recargo_Ancho_Mayor.BackColor = 14737632
        Largo_Menor.Text = ""
        Largo_Menor.BackColor = 14737632
        Recargo_Largo_Menor.Text = ""
        Recargo_Largo_Menor.BackColor = 14737632
        Largo_Mayor.Text = ""
        Largo_Mayor.BackColor = 14737632
        Recargo_Largo_Mayor.Text = ""
        Recargo_Largo_Mayor.BackColor = 14737632

        Presentacion_Revendedor.SetFocus
    ElseIf Sistema_Medida.ListIndex = 0 Then
        Presentacion_Publicos.AddItem Articulo_Presentacion(1)
        Presentacion_Publicos.AddItem Articulo_Presentacion(1)
        Presentacion_Publicos.AddItem Articulo_Presentacion(1)
        
        'Presentacion_Revendedor.Clear
        Presentacion_Revendedor.AddItem Articulo_Presentacion(1)
        Presentacion_Revendedor.AddItem Articulo_Presentacion(2)
        Presentacion_Revendedor.AddItem Articulo_Presentacion(6)
        
        Marco_Recargos.Enabled = True
        
        Espesor.BackColor = vbWhite
        Espesor.Enabled = True
        Ancho.BackColor = vbWhite
        Ancho.Enabled = True
        Largo.BackColor = vbWhite
        Largo.Enabled = True
        
        Ancho_Menor.BackColor = vbWhite
        Recargo_Ancho_Menor.BackColor = vbWhite
        Ancho_Mayor.BackColor = vbWhite
        Recargo_Ancho_Mayor.BackColor = vbWhite
        Largo_Menor.BackColor = vbWhite
        Recargo_Largo_Menor.BackColor = vbWhite
        Largo_Mayor.BackColor = vbWhite
        Recargo_Largo_Mayor.BackColor = vbWhite
        
        Espesor.SetFocus
    End If
End Sub

Private Sub Stock_Minimo_GotFocus()
    Stock_Minimo.Text = Trim$(Stock_Minimo.Text)
    Stock_Minimo.SelStart = 0
    Stock_Minimo.SelText = ""
    Stock_Minimo.SelLength = Len(Stock_Minimo.Text)
End Sub

Private Sub Stock_Minimo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Stock_Minimo_LostFocus()
    Stock_Minimo.Text = Formateado(Str$(Val(Stock_Minimo.Text)), 2, 9, " ", False)
End Sub

Private Sub Ancho_Mayor_GotFocus()
    Ancho_Mayor.Text = Trim$(Ancho_Mayor.Text)
    Ancho_Mayor.SelStart = 0
    Ancho_Mayor.SelText = ""
    Ancho_Mayor.SelLength = Len(Ancho_Mayor.Text)
End Sub

Private Sub Ancho_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_Mayor_LostFocus()
    Ancho_Mayor.Text = Formateado(Str$(Val(Ancho_Mayor.Text)), 2, 8, " ", False)
End Sub

Private Sub Ancho_Menor_GotFocus()
    Ancho_Menor.Text = Trim$(Ancho_Menor.Text)
    Ancho_Menor.SelStart = 0
    Ancho_Menor.SelText = ""
    Ancho_Menor.SelLength = Len(Ancho_Menor.Text)
End Sub

Private Sub Ancho_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_Menor_LostFocus()
    Ancho_Menor.Text = Formateado(Str$(Val(Ancho_Menor.Text)), 2, 8, " ", False)
End Sub

Private Sub Largo_Menor_GotFocus()
    Largo_Menor.Text = Trim$(Largo_Menor.Text)
    Largo_Menor.SelStart = 0
    Largo_Menor.SelText = ""
    Largo_Menor.SelLength = Len(Largo_Menor.Text)
End Sub

Private Sub Largo_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_Menor_LostFocus()
    Largo_Menor.Text = Formateado(Str$(Val(Largo_Menor.Text)), 2, 8, " ", False)
End Sub

Private Sub Largo_Mayor_GotFocus()
    Largo_Mayor.Text = Trim$(Largo_Mayor.Text)
    Largo_Mayor.SelStart = 0
    Largo_Mayor.SelText = ""
    Largo_Mayor.SelLength = Len(Largo_Mayor.Text)
End Sub

Private Sub Largo_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_Mayor_LostFocus()
    Largo_Mayor.Text = Formateado(Str$(Val(Largo_Mayor.Text)), 2, 8, " ", False)
End Sub

Private Sub Recargo_Ancho_Mayor_GotFocus()
    Recargo_Ancho_Mayor.Text = Trim$(Recargo_Ancho_Mayor.Text)
    Recargo_Ancho_Mayor.SelStart = 0
    Recargo_Ancho_Mayor.SelText = ""
    Recargo_Ancho_Mayor.SelLength = Len(Recargo_Ancho_Mayor.Text)
End Sub

Private Sub Recargo_Ancho_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Ancho_Mayor_LostFocus()
    Recargo_Ancho_Mayor.Text = Formateado(Str$(Val(Recargo_Ancho_Mayor.Text)), 2, 6, " ", False)
End Sub

Private Sub Recargo_Ancho_Menor_GotFocus()
    Recargo_Ancho_Menor.Text = Trim$(Recargo_Ancho_Menor.Text)
    Recargo_Ancho_Menor.SelStart = 0
    Recargo_Ancho_Menor.SelText = ""
    Recargo_Ancho_Menor.SelLength = Len(Recargo_Ancho_Menor.Text)
End Sub

Private Sub Recargo_Ancho_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Ancho_Menor_LostFocus()
    Recargo_Ancho_Menor.Text = Formateado(Str$(Val(Recargo_Ancho_Menor.Text)), 2, 6, " ", False)
End Sub

Private Sub Recargo_Largo_Mayor_GotFocus()
    Recargo_Largo_Mayor.Text = Trim$(Recargo_Largo_Mayor.Text)
    Recargo_Largo_Mayor.SelStart = 0
    Recargo_Largo_Mayor.SelText = ""
    Recargo_Largo_Mayor.SelLength = Len(Recargo_Largo_Mayor.Text)
End Sub

Private Sub Recargo_Largo_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Largo_Mayor_LostFocus()
    Recargo_Largo_Mayor.Text = Formateado(Str$(Val(Recargo_Largo_Mayor.Text)), 2, 6, " ", False)
End Sub

Private Sub Recargo_Largo_Menor_GotFocus()
    Recargo_Largo_Menor.Text = Trim$(Recargo_Largo_Menor.Text)
    Recargo_Largo_Menor.SelStart = 0
    Recargo_Largo_Menor.SelText = ""
    Recargo_Largo_Menor.SelLength = Len(Recargo_Largo_Menor.Text)
End Sub

Private Sub Recargo_Largo_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Largo_Menor_LostFocus()
    Recargo_Largo_Menor.Text = Formateado(Str$(Val(Recargo_Largo_Menor.Text)), 2, 6, " ", False)
End Sub

Private Sub Articulos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo.SetFocus
End Sub

Private Sub Articulos_Encontrados_LostFocus()
    Rubro.Text = Mid$(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 32, 5)
    Rubro_LostFocus
    Articulo.Text = Mid$(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 38)
    If Val(Articulo.Text) > 0 Then
        Descripcion.SetFocus
    End If
    Articulos_Encontrados.Visible = False
End Sub
