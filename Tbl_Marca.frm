VERSION 5.00
Begin VB.Form Tbl_Marca 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualización de Marcas.-"
   ClientHeight    =   2415
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5535
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   2415
   ScaleWidth      =   5535
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Rubro 
      Height          =   1695
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   5535
      Begin VB.ComboBox Rubros_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   11
         Top             =   240
         Visible         =   0   'False
         Width           =   4095
      End
      Begin VB.CommandButton Buscar_Rubros 
         Caption         =   "&Marca:"
         Height          =   255
         Left            =   240
         TabIndex        =   7
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   5
         TabIndex        =   0
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Denominacion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   30
         TabIndex        =   1
         Top             =   600
         Width           =   4095
      End
      Begin VB.TextBox Fecha 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4080
         MaxLength       =   10
         TabIndex        =   6
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Denominación:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Alta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2400
         TabIndex        =   8
         Top             =   1320
         Width           =   1575
      End
   End
   Begin VB.Frame Botonera 
      Height          =   735
      Left            =   0
      TabIndex        =   10
      Top             =   1680
      Width           =   5535
      Begin VB.CommandButton Grabar 
         Caption         =   "&Grabar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   2
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Borrar 
         Caption         =   "&Borrar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   1200
         TabIndex        =   3
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Caption         =   "&Salir"
         Height          =   375
         Left            =   4440
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "Tbl_Marca"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Borrar_Click()
    Qy = "DELETE * FROM Marca WHERE Id_Marca = " & Trim$(Str$(Val(Rubro.Text)))
    Db.Execute (Qy)
    Salir_Click
End Sub

Private Sub Buscar_Rubros_Click()
    Rubros_Encontrados.Visible = True
    Rubros_Encontrados.Clear
    MousePointer = 11
    
    Qy = "SELECT * FROM Marca ORDER BY Denominacion"
    Set Rs = Db.OpenRecordset(Qy)
    
    While Not Rs.EOF
        Rubros_Encontrados.AddItem Trim$(Rs.Fields("Denominacion")) + Space$(30 - Len(Trim$(Rs.Fields("Denominacion")))) & " " & Trim$(Rs.Fields("Id_Marca"))
        Rs.MoveNext
    Wend
    
    MousePointer = 0
    Rubros_Encontrados.SetFocus
End Sub

Private Sub Denominacion_GotFocus()
    Denominacion.SelStart = 0
    Denominacion.SelText = ""
    Denominacion.SelLength = Len(Denominacion.Text)
End Sub

Private Sub Denominacion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Denominacion_LostFocus()
    Denominacion.Text = FiltroCaracter(Denominacion.Text)
    Denominacion.Text = UCase$(Denominacion.Text)
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Grabar_Click()
    If Val(Rubro.Text) > 0 And Trim$(Denominacion.Text) <> "" Then
        
        If Grabar.Tag = "Alta" Then
            
            Qy = "INSERT INTO Marca VALUES ("
            Qy = Qy & Trim$(Str$(Val(Rubro.Text)))
            Qy = Qy & ", '" & Trim$(Denominacion.Text) & "'"
            Qy = Qy & ", '" & Trim$(Fecha.Text) & "')"
            Db.Execute (Qy)
            
        Else
            
            Qy = "UPDATE Marca SET "
            Qy = Qy & "Denominacion = '" & Trim$(Denominacion.Text) & "' "
            Qy = Qy & "WHERE Id_Marca = " & Trim$(Str$(Val(Rubro.Text)))
            Db.Execute (Qy)
        
        End If
        
        Salir_Click
    Else
        Rubro.SetFocus
    End If
End Sub

Private Sub Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        If Val(Rubro.Text) = 0 Then
            Qy = "SELECT MAX(Id_Marca) FROM Marca"
            Set Rs = Db.OpenRecordset(Qy)
            
            Rubro.Text = 1
            If Not Rs.Fields(0) = "" Then Rubro.Text = Val(Rs.Fields(0) + 1)
        Else
            SendKeys "{TAB}"
        End If
    End If
End Sub

Private Sub Rubro_LostFocus()
    If Val(Rubro.Text) > 0 Then
        Leer_Rubro
    End If
End Sub

Private Sub Leer_Rubro()
    Qy = "SELECT * FROM Marca WHERE Id_Marca = " & Trim$(Str$(Val(Rubro.Text)))
    Set Rs = Db.OpenRecordset(Qy)
    
    If Rs.EOF Then
        If MsgBox("La Marca es inexistente, desea incorporarla ahora. ?", vbQuestion + vbYesNo, "Atención.!") = vbYes Then
            Grabar.Tag = "Alta"
            Fecha.Text = Format(Now, "dd/mm/yyyy")
            Grabar.Enabled = True
            Borrar.Enabled = False
            Denominacion.SetFocus
        Else
            Borrar_Campo
        End If
    Else
        Mostrar_Campo
        Grabar.Enabled = True
        Borrar.Enabled = True
        Denominacion.SetFocus
    End If
End Sub

Private Sub Mostrar_Campo()
    Denominacion.Text = Rs.Fields("Denominacion")
    Fecha.Text = Format(Rs.Fields("Fecha_Alta"), "dd/mm/yyyy")
End Sub

Private Sub Borrar_Campo()
    Rubro.Text = ""
    Denominacion.Text = ""
    Fecha.Text = ""
    
    Borrar.Enabled = False
    Grabar.Enabled = False
    
    Rubro.SetFocus
End Sub

Private Sub Salir_Click()
    If Rubro.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Rubros_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Rubro.SetFocus
End Sub

Private Sub Rubros_Encontrados_LostFocus()
    Rubro.Text = Mid$(Rubros_Encontrados.List(Rubros_Encontrados.ListIndex), 32)
    Leer_Rubro
    Rubros_Encontrados.Visible = False
End Sub

