VERSION 5.00
Begin VB.Form Ayuda_AcercaDe 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Acerca de Sistema."
   ClientHeight    =   4575
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6135
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4575
   ScaleWidth      =   6135
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco 
      Height          =   3855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6135
      Begin VB.Label Slogan 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   15.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   1215
         Left            =   120
         TabIndex        =   5
         Top             =   1440
         Width           =   5895
      End
      Begin VB.Label Empresa 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   20.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   975
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   5895
      End
      Begin VB.Label Label1 
         Alignment       =   2  'Center
         Caption         =   "Sistema de Administración Integral Ver. 1.0 --------------------------------------------------"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   375
         Left            =   1200
         TabIndex        =   3
         Top             =   3360
         Width           =   3735
      End
   End
   Begin VB.Frame Botonera 
      Height          =   735
      Left            =   0
      TabIndex        =   1
      Top             =   3840
      Width           =   6135
      Begin VB.CommandButton Aceptar 
         Cancel          =   -1  'True
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   375
         Left            =   4920
         TabIndex        =   2
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Ayuda_AcercaDe"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Aceptar_Click()
    Unload Me
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Acerca de..."
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 3
    Me.Left = (Screen.Width - Me.Width) / 2
    
    Empresa.Caption = Trim(cEmpresa)
    Slogan.Caption = Trim(cSlogan)
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub
