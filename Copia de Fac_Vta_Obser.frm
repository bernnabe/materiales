VERSION 5.00
Begin VB.Form Fac_Vta_Obser 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Observaciones.-"
   ClientHeight    =   3855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7095
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3855
   ScaleWidth      =   7095
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Comp 
      Caption         =   "Comprobantes Asociados"
      Height          =   1215
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7095
      Begin VB.TextBox Remito_Dos 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   20
         TabIndex        =   4
         Top             =   720
         Width           =   2535
      End
      Begin VB.TextBox Remito_Uno 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   20
         TabIndex        =   3
         Top             =   360
         Width           =   2535
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Remito:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Remito:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Obser 
      Caption         =   "Obervaciones"
      Height          =   1935
      Left            =   0
      TabIndex        =   1
      Top             =   1200
      Width           =   7095
      Begin VB.TextBox Linea_Cuatro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   14
         Top             =   1440
         Width           =   5895
      End
      Begin VB.TextBox Linea_Tres 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   13
         Top             =   1080
         Width           =   5895
      End
      Begin VB.TextBox Linea_Dos 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   12
         Top             =   720
         Width           =   5895
      End
      Begin VB.TextBox Linea_Uno 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   11
         Top             =   360
         Width           =   5895
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea 4:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1440
         Width           =   855
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea 3:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   1080
         Width           =   855
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea 2:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   720
         Width           =   855
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "L�nea 1:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   855
      End
   End
   Begin VB.Frame Botonera 
      Height          =   735
      Left            =   0
      TabIndex        =   2
      Top             =   3120
      Width           =   7095
      Begin VB.CommandButton Cancelar 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   5880
         TabIndex        =   16
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Aceptar 
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   4680
         TabIndex        =   15
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Fac_Vta_Obser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Grabar_Comprobante()
    Dim i                 As Integer
    Dim l                 As Integer
    Dim Rec_Rubro         As Integer
    Dim Rec_Articulo      As Integer
    Dim Rec_Descripcion   As String
    Dim Rec_Presentacion  As Integer
    Dim Rec_Cantidad      As Single
    Dim Rec_Espesor       As Single
    Dim Rec_Ancho         As Single
    Dim Rec_Largo         As Single
    Dim Rec_Precio        As Single
    Dim Rec_Impuesto      As Single
    Dim Rec_ImporCta      As Single
    Dim Rec_Existencia    As Single
    
    Dim Rec_Asiento       As Integer
    Dim Rec_Cuenta        As String
    Dim Alm_Cta           As String
    
    
    If Val(Fac_Vta.List1.ListCount) > 0 Then
        MousePointer = 11
        'Primero Imprimo la Factura y si no surge ning�n error la grabo
        
        If Trim$(UCase$(Fac_Vta.Cliente.Text)) = "M" Then
            Qy = "SELECT MAX(Id_Cliente) FROM Cliente"
            Set Rs = Db.OpenRecordset(Qy)
            
            Fac_Vta.Cliente.Text = 1
            If Not Rs.Fields(0) = "" Then Fac_Vta.Cliente.Text = Val(Rs.Fields(0)) + 1
            
            Qy = "INSERT INTO Cliente VALUES ('"
            Qy = Qy & Trim$(Str$(Val(Fac_Vta.Cliente.Text))) & "'"
            Qy = Qy & ", '" & Trim$(Fac_Vta.Nombre.Text) & "'"
            Qy = Qy & ", '" & Trim$(Fac_Vta.Direccion.Text) & "'"
            Qy = Qy & ", '0', '0', ' '"
            Qy = Qy & ", '" & Trim$(Str$(Val(Fac_Vta.Localidad.Text))) & "'"
            Qy = Qy & ", ' ', ' '"
            Qy = Qy & ", '" & Trim$(UCase$(Mid$(Fac_Vta.Condicion_Iva.Text, 1, 2))) & "'"
            Qy = Qy & ", '00'"
            Qy = Qy & ", '" & Trim$(Fac_Vta.Cuit.Text) & "'"
            Qy = Qy & ", '00', '00'"
            Qy = Qy & ", " & IIf(Trim$(Fac_Vta.Tipo_Cliente.Text) = "R", "1", "2")
            Qy = Qy & ", 0.00 "
            Qy = Qy & ", '" & Trim$(Fac_Vta.Fecha.Text) & "'"
            Qy = Qy & ", ' ')"
            Db.Execute (Qy)
                
            MsgBox "El Cliente ha sido registrado en la base de datos con el n�mero: " & Trim$(Str$(Val(Fac_Vta.Cliente.Text))), vbInformation, "Atenci�n.!"
        End If
    
        Qy = "SELECT MAX(Nro_Asiento) FROM Asiento "
        Qy = Qy & "WHERE Ejercicio = " & Trim$(Str$(Val(Mid$(Fac_Vta.Fecha.Text, 7, 4)))) & " "
        Qy = Qy & "AND Empresa = " & Trim$(Str$(Val(Id_Empresa)))
        Set Rs = Db.OpenRecordset(Qy)
        
        Rec_Asiento = 1
        If IsNull(Rs.Fields(0)) = False Then Rec_Asiento = Val(Rs.Fields(0)) + 1
        
        Qy = "INSERT INTO Factura_Venta VALUES("
        Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
        Qy = Qy & ", '" & Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) & "'"
        Qy = Qy & ", '" & Trim$(UCase$(Fac_Vta.Letra.Text)) & "'"
        Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Centro_Emisor.Text)))
        Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Numero.Text)))
        Qy = Qy & ", " & Mid$(Fac_Vta.Fecha.Text, 4, 2)
        Qy = Qy & ", " & Mid$(Fac_Vta.Fecha.Text, 7, 4)
        Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Plazo.Text)))
        Qy = Qy & ", '" & Trim$(Fac_Vta.Fecha.Text) & " " & Trim$(Hora_Fiscal) & "'"
        Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Cliente.Text)))
        Qy = Qy & ", " & IIf(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str$(Val(Fac_Vta.Neto_Gravado.Text)) * -1), (Str$(Val(Fac_Vta.Neto_Gravado.Text))))
        Qy = Qy & ", 0,0, " & IIf(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str$(Val(Fac_Vta.Exento.Text)) * -1), Trim$(Str$(Val(Fac_Vta.Exento.Text))))
        Qy = Qy & ", " & IIf(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str$(Val(Fac_Vta.Sub_Total.Text)) * -1), Str$(Val(Fac_Vta.Sub_Total.Text)))
        Qy = Qy & ", " & IIf(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", Str$(Val(Fac_Vta.Iva_Inscrip.Text)) * -1, Str$(Val(Fac_Vta.Iva_Inscrip.Text)))
        Qy = Qy & ", 0, " & IIf(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str$(Val(Fac_Vta.Retencion_Iva.Text)) * -1), Str$(Val(Fac_Vta.Retencion_Iva.Text)))
        Qy = Qy & ", " & IIf(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str$(Val(Fac_Vta.Retencion_IIBB.Text)) * -1), Str$(Val(Fac_Vta.Retencion_IIBB.Text)))
        Qy = Qy & ", " & IIf(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) = "NC", (Str$(Val(Fac_Vta.Total_Factura.Text)) * -1), (Str$(Val(Fac_Vta.Total_Factura.Text))))
        Qy = Qy & ", " & Trim$(Str$(Val(Rec_Asiento)))
        Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Fac_Vta.Fecha.Text, 7, 4)))) & ")"
        Db.Execute (Qy)
        
        'Grabo la Primera l�nea de la Contabilidad.-
        Qy = "INSERT INTO Asiento VALUES ("
        Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
        Qy = Qy & ", " & Trim$(Str$(Val(Rec_Asiento)))
        Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Fac_Vta.Fecha.Text, 7, 4))))
        Qy = Qy & ", '" & Trim$(Fac_Vta.Fecha.Text) & "'"
        Qy = Qy & ", 0"
        If Val(Fac_Vta.Plazo.Text) > 0 Then
            Qy = Qy & ", 1, 3, 2, 1, 0, 0"
        Else
            Qy = Qy & ", 1, 1, 1, 1, 0, 0"
        End If
        Qy = Qy & ", '" & Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) & " " & Trim$(Fac_Vta.Letra.Text) & " " & Formateado(Str$(Val(Fac_Vta.Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim$(Fac_Vta.Numero.Text) & " V'"
        Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Total_Factura.Text)))
        Qy = Qy & ", 0)"
        Db.Execute (Qy)

        For i = 0 To Fac_Vta.List1.ListCount - 1
            
            Rec_Rubro = Val(Mid$(Fac_Vta.List1.List(i), 1, 4))
            Rec_Articulo = Val(Mid$(Fac_Vta.List1.List(i), 6, 5))
            Rec_Descripcion = Trim$(Mid$(Fac_Vta.List1.List(i), 12, 30))
            Rec_Presentacion = Mid$(Fac_Vta.List1.List(i), 159, 1)
            Rec_Espesor = Mid$(Fac_Vta.List1.List(i), 119, 9)
            Rec_Ancho = Mid$(Fac_Vta.List1.List(i), 129, 9)
            Rec_Largo = Mid$(Fac_Vta.List1.List(i), 139, 9)
            Rec_Precio = Trim$(Mid$(Fac_Vta.List1.List(i), 73, 10))
            Rec_Impuesto = Val(Rec_Precio) / Val("1." & Val(Alicuota_Iva))
            Rec_Impuesto = (Val(Rec_Impuesto) * Val(Alicuota_Iva)) / 100
            Rec_Cantidad = Trim$(Mid$(Fac_Vta.List1.List(i), 84, 10))
            Rec_Existencia = Trim$(Mid$(Fac_Vta.List1.List(i), 98, 10))
            Rec_Cuenta = Mid$(Fac_Vta.List1.List(i), 172, 30)
            Rec_ImporCta = Val(Mid$(Fac_Vta.List1.List(i), 95, 10)) / Val("1." & Alicuota_Iva)
            
            'Si es una Nota de Cr�dito los importes se graban en Negativo
            Qy = "INSERT INTO Factura_Venta_Item VALUES ("
            Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
            Qy = Qy & ", '" & Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) & "'"
            Qy = Qy & ", '" & Trim$(UCase$(Fac_Vta.Letra.Text)) & "'"
            Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Centro_Emisor.Text)))
            Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Numero.Text)))
            Qy = Qy & ", " & Trim$(Str$(Val(i)))
            Qy = Qy & ", " & Trim$(Str$(Val(Rec_Rubro)))
            Qy = Qy & ", " & Trim$(Str$(Val(Rec_Articulo)))
            Qy = Qy & ", '" & Trim$(Rec_Descripcion) & "'"
            Qy = Qy & ", " & Trim$(Str$(Val(Rec_Presentacion)))
            Qy = Qy & ", " & IIf(Val(Rec_Espesor) > 0, Trim$(Str$(Val(Rec_Espesor))), 0)
            Qy = Qy & ", " & IIf(Val(Rec_Ancho) > 0, Trim$(Str$(Val(Rec_Ancho))), 0)
            Qy = Qy & ", " & IIf(Val(Rec_Largo) > 0, Trim$(Str$(Val(Rec_Largo))), 0)
            Qy = Qy & ", " & Trim$(Str$(Val(Rec_Cantidad)))
            Qy = Qy & ", " & Trim$(Str$(Val(Rec_Impuesto)))
            If (Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "NC" Then
                Qy = Qy & ", " & ((Val(Rec_Precio) * Val(Rec_Cantidad)) * -1) & ")"
            Else
                Qy = Qy & ", " & (Val(Rec_Precio) * Val(Rec_Cantidad)) & ")"
            End If
            Db.Execute (Qy)
            
            If Val(Rec_Articulo) > 0 Then
                Qy = "UPDATE Articulo SET "
                If Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "FC" Or Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "ND" Then
                    Qy = Qy & "Existencia = Existencia - " & Trim$(Str$(Val(Rec_Cantidad)))
                Else
                    Qy = Qy & "Existencia = Existencia + " & Trim$(Str$(Val(Rec_Cantidad)))
                End If
                Qy = Qy & " WHERE Id_Articulo = " & Trim$(Str$(Val(Rec_Articulo)))
                Db.Execute (Qy)
            End If
            
            If Trim$(Rec_Cuenta) <> "" Then
                Qy = "INSERT INTO Asiento VALUES ("
                Qy = Qy & Trim$(Str$(Val(Id_Empresa))) & " "
                Qy = Qy & ", " & Trim$(Str$(Val(Rec_Asiento)))
                Qy = Qy & ", '" & Trim$(Mid$(Fac_Vta.Fecha.Text, 7, 4)) & "'"
                Qy = Qy & ", '" & Trim$(Fac_Vta.Fecha.Text) & "'"
                Qy = Qy & ", " & Val(i) + 1
                        
                For l = 1 To Len(Rec_Cuenta)
                    If Mid$(Rec_Cuenta, l, 1) <> "." Then
                        Alm_Cta = Val(Alm_Cta) & Val(Mid$(Rec_Cuenta, l, 1))
                    Else
                        Qy = Qy & ", " & Trim$(Str$(Val(Alm_Cta))) & " "
                        Alm_Cta = ""
                    End If
                Next
                        
                Qy = Qy & ", " & Trim$(Str$(Val(Alm_Cta))) & " "
                Qy = Qy & ", '" & Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) & " " & Trim$(Fac_Vta.Letra.Text) & " " & Formateado(Str$(Val(Fac_Vta.Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim$(Fac_Vta.Numero.Text) & " V'"
                Qy = Qy & ", 0"
                Qy = Qy & ", " & Formateado(Str$(Val(Rec_ImporCta)), 2, 10, " ", False) & ")"
                Db.Execute (Qy)
            End If
        Next
        
        Qy = "INSERT INTO Asiento VALUES ("
        Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
        Qy = Qy & ", " & Trim$(Str$(Val(Rec_Asiento)))
        Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Fac_Vta.Fecha.Text, 7, 4))))
        Qy = Qy & ", '" & Trim$(Fac_Vta.Fecha.Text) & "'"
        Qy = Qy & ", " & (Val(i) + 1)
        Qy = Qy & ", 2, 4, 1, 1, 0, 0"
        Qy = Qy & ", '" & Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) & " " & Trim$(Fac_Vta.Letra.Text) & " " & Formateado(Str$(Val(Fac_Vta.Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim$(Fac_Vta.Numero.Text) & " V'"
        Qy = Qy & ", 0"
        Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Iva_Inscrip.Text))) & ")"
        Db.Execute (Qy)
       
        If Val(Fac_Vta.Plazo.Text) > 0 Then
            Qy = "INSERT INTO CtaCte_Cliente VALUES ("
            Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
            Qy = Qy & ", " & Trim$(Str$(Val(Fac_Vta.Cliente.Text)))
            Qy = Qy & ", '" & Trim$(Fac_Vta.Fecha.Text) & " " & Trim$(Hora_Fiscal) & "'"
            Qy = Qy & ", '" & Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2) & " " & Trim$(Fac_Vta.Letra.Text) & " " & Formateado(Str$(Val(Fac_Vta.Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim$(Fac_Vta.Numero.Text) & "'"
            Qy = Qy & ", '" & "OPERACIONES EN CTACTE" & "'"
            Qy = Qy & ", '" & Trim$(Fac_Vta.Vto.Text) & "'"
            If Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "FC" Or Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "ND" Then
                Qy = Qy & ", '" & Trim$(Str$(Val(Fac_Vta.Total_Factura.Text))) & "'"
                Qy = Qy & ", '0')"
            Else
                Qy = Qy & ", '0'"
                Qy = Qy & ", '" & Trim$(Str$(Val(Fac_Vta.Total_Factura.Text))) & "')"
            End If
            Db.Execute (Qy)
        End If
        
        If Not UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "ND" Then
            Qy = "UPDATE Empresa SET "
            If Trim$(UCase$(Fac_Vta.Letra.Text)) = "A" And UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "FC" Then
                Qy = Qy & "Ultima_FC_A = '" & Trim$(Fac_Vta.Numero.Text) & "' "
                'Qy = Qy & "Ultima_FC_A = '" & Trim$(Menu.HASAR1.UltimaFactura) & "'"
            ElseIf Trim$(UCase$(Fac_Vta.Letra.Text)) = "B" And UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "FC" Then
                Qy = Qy & "Ultima_FC_B = '" & Trim$(Fac_Vta.Numero.Text) & "' "
                'Qy = Qy & "Ultima_FC_B = '" & Trim$(Menu.HASAR1.UltimoTicket) & "'"
            ElseIf Trim$(UCase$(Fac_Vta.Letra.Text)) = "A" And UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "NC" Then
                Qy = Qy & "Ultima_NC_A = '" & Trim$(Fac_Vta.Numero.Text) & "' "
            ElseIf Trim$(UCase$(Fac_Vta.Letra.Text)) = "B" And UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2)) = "NC" Then
                Qy = Qy & "Ultima_NC_B = '" & Trim$(Fac_Vta.Numero.Text) & "' "
            End If
            Qy = Qy & "WHERE Id_Empresa = " & Trim$(Str$(Val(Id_Empresa))) & " "
            Db.Execute (Qy)
        End If
        
        MousePointer = 0
    End If
End Sub

Private Sub Aceptar_Click()
    Remito_Uno.Enabled = False
    Remito_Dos.Enabled = False
    
    Linea_Uno.Enabled = False
    Linea_Dos.Enabled = False
    Linea_Tres.Enabled = False
    Linea_Cuatro.Enabled = False
    
    Aceptar.Enabled = False
    Cancelar.Enabled = False
    
    'If Val(Id_Empresa) = 1 Then
    '    Imprimir_Hasar
    'ElseIf Val(Id_Empresa) = 2 Then
    '    Imprimir_Comun
    'End If
    Grabar_Comprobante
    
    Unload Me
End Sub

Private Sub Imprimir_Comun()
    'DESARROLLAR MODULO DE IMPRESI�N PARA IMPRESORAS COMUNES
End Sub

Private Sub Imprimir_Hasar()
    Dim i                    As Integer
    Dim Nombre_Cliente       As String
    Dim Direccion_Cliente    As String
    Dim Cuit_Cliente         As String
    Dim Articulo_Desc        As String
    Dim Articulo_Cant        As String
    Dim Articulo_Importe     As String
    
    Inicializar_Impresora
    
    Menu.HASAR1.Encabezado(11) = Trim$(Linea_Uno.Text)
    Menu.HASAR1.Encabezado(12) = Trim$(Linea_Dos.Text)
    Menu.HASAR1.Encabezado(13) = Trim$(Linea_Tres.Text)
    Menu.HASAR1.Encabezado(14) = Trim$(Linea_Cuatro.Text)

    Nombre_Cliente = Fac_Vta.Nombre.Text
    Direccion_Cliente = Fac_Vta.Direccion.Text
    Cuit_Cliente = Mid$(Fac_Vta.Cuit.Text, 1, 2) & Mid$(Fac_Vta.Cuit.Text, 4, 8) & Mid$(Fac_Vta.Cuit.Text, 13, 1)
    Cuit_Cliente = Val(Cuit_Cliente)

    'Inidico la condici�n de iva del cliente
    If Mid$(Fac_Vta.Condicion_Iva.Text, 1, 2) = "RI" Then
        Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, RESPONSABLE_INSCRIPTO, Direccion_Cliente
    ElseIf Mid$(Fac_Vta.Condicion_Iva.Text, 1, 2) = "CF" Then
        
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, CONSUMIDOR_FINAL
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, CONSUMIDOR_FINAL, Direccion_Cliente
        End If
    ElseIf Mid$(Fac_Vta.Condicion_Iva.Text, 1, 2) = "NI" Then
        
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, RESPONSABLE_NO_INSCRIPTO, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, RESPONSABLE_NO_INSCRIPTO, Direccion_Cliente
        End If
    ElseIf Mid$(Fac_Vta.Condicion_Iva.Text, 1, 2) = "MT" Then
        
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, MONOTRIBUTO, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, MONOTRIBUTO, Direccion_Cliente
        End If
    ElseIf Mid$(Fac_Vta.Condicion_Iva.Text, 1, 2) = "NC" Then
        
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, NO_CATEGORIZADO, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, NO_CATEGORIZADO, Direccion_Cliente
        End If
    ElseIf Mid$(Fac_Vta.Condicion_Iva.Text, 1, 2) = "ET" Then
        
        If Val(Fac_Vta.Cuit.Text) > 0 Then
            Menu.HASAR1.DatosCliente Nombre_Cliente, Cuit_Cliente, TIPO_CUIT, RESPONSABLE_EXENTO, Direccion_Cliente
        Else
            Menu.HASAR1.DatosCliente Nombre_Cliente, 0, TIPO_NINGUNO, RESPONSABLE_EXENTO, Direccion_Cliente
        End If
    End If
    
    'Indico el comprobante a imprimir
    If Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "FC" And Trim$(UCase$(Fac_Vta.Letra.Text)) = "A" Then
        Menu.HASAR1.AbrirComprobanteFiscal FACTURA_A
    ElseIf Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "FC" And Trim$(UCase$(Fac_Vta.Letra.Text)) = "B" Then
        Menu.HASAR1.AbrirComprobanteFiscal FACTURA_B
    ElseIf Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "ND" And Trim$(UCase$(Fac_Vta.Letra.Text)) = "A" Then
        Menu.HASAR1.AbrirComprobanteFiscal NOTA_DEBITO_A
    ElseIf Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "ND" And Trim$(UCase$(Fac_Vta.Letra.Text)) = "B" Then
        Menu.HASAR1.AbrirComprobanteFiscal NOTA_DEBITO_B
    ElseIf Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "NC" And Trim$(UCase$(Fac_Vta.Letra.Text)) = "A" Then
        Menu.HASAR1.AbrirComprobanteNoFiscalHomologado NOTA_CREDITO_A
    ElseIf Trim$(UCase$(Mid$(Fac_Vta.Tipo_Comp.Text, 1, 2))) = "NC" And Trim$(UCase$(Fac_Vta.Letra.Text)) = "B" Then
        Menu.HASAR1.AbrirComprobanteNoFiscalHomologado NOTA_CREDITO_B
    End If
    
    For i = 0 To Fac_Vta.List1.ListCount - 1
        Articulo_Desc = ""
    
        If Val(Mid$(Fac_Vta.List1.List(Fac_Vta.List1.ListIndex), 1, 4)) > 0 Then
            Articulo_Desc = Val(Mid$(Fac_Vta.List1.List(Fac_Vta.List1.ListIndex), 1, 4)) & "/" & Val(Mid$(Fac_Vta.List1.List(Fac_Vta.List1.ListIndex), 6, 5))
        End If
        
        Articulo_Desc = Trim$(Articulo_Desc) + Space$(50 - Len(Trim$(Articulo_Desc))) & " " & Trim$(Mid$(Fac_Vta.List1.List(i), 12, 50))
        
        Articulo_Importe = Trim$(Mid$(Fac_Vta.List1.List(i), 73, 10))
        Articulo_Cant = Trim$(Mid$(Fac_Vta.List1.List(i), 84, 10))
        
        Menu.HASAR1.ImprimirItem Articulo_Desc, Articulo_Cant, Articulo_Importe, Val(Alicuota_Iva), 0#
    Next
    
    Menu.HASAR1.Subtotal True
    
    If Val(Fac_Vta.Plazo.Text) = 0 Then
        Menu.HASAR1.ImprimirPago "EFECTIVO: ", Trim$(Str$(Val(Fac_Vta.Total_Factura.Text)))
    Else
        Menu.HASAR1.ImprimirPago "CTA. CTE.: ", Trim$(Str$(Val(Fac_Vta.Total_Factura.Text)))
    End If
    
    Menu.HASAR1.CerrarComprobanteFiscal
    Menu.HASAR1.Finalizar
End Sub

Private Sub Cancelar_Click()
    If Fac_Vta.Cliente.Text = "" Then
        Unload Fac_Vta
    Else
        Unload Me
    End If
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Observaciones y confirma de la emisi�n del comprobante.-"
End Sub

Private Sub Form_Load()
    Fac_Vta.Enabled = False
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    
    
    Linea_Uno.Text = "CLIENTE: " & Trim$(Fac_Vta.Cliente.Text)
    If Val(Fac_Vta.Plazo.Text) > 0 Then
        Linea_Dos.Text = "ESTA FACTURA VENCE EL D�A: " & Trim$(Fac_Vta.Vto.Text)
    Else
        Linea_Dos.Text = "ESTA FACTURA ES DE CONTADO."
    End If
    
    If Val(Fac_Vta.Total_Factura.Text) > 0 Then
        Linea_Tres.Text = "IMPORTE: " & Trim$(UCase$(NumerosALetras(Val(Fac_Vta.Total_Factura.Text), False, False)))
    End If
    
    Menu.Menu_Cliente.Enabled = False
    Menu.Menu_Articulo.Enabled = False
    Menu.Menu_Proveedor.Enabled = False
    Menu.Menu_Facturaci�n.Enabled = False
    Menu.menu_informa.Enabled = False
    Menu.Menu_Contan.Enabled = False
    Menu.Men_Tab_Aux.Enabled = False
    Menu.Ventanas.Enabled = False
    Menu.Salidas.Enabled = False
    Menu.Menu_Help.Enabled = False
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Fac_Vta.Grabar.Enabled = False
    Fac_Vta.Marco_Factura_Item.Enabled = False
    Fac_Vta.Marco_Factura.Enabled = True
    Fac_Vta.Tipo_Comp.ListIndex = 0
    Fac_Vta.Cliente.Text = ""
    Fac_Vta.Cliente.Tag = ""
    Fac_Vta.Vto.Text = ""
    Fac_Vta.Cuit.Text = ""
    Fac_Vta.Nombre.Text = ""
    Fac_Vta.Centro_Emisor.Text = ""
    Fac_Vta.List1.Clear
    Fac_Vta.Condicion_Iva.Text = ""
    Fac_Vta.Letra.Text = ""
    Fac_Vta.Numero.Text = ""
    Fac_Vta.Direccion.Text = ""
    Fac_Vta.Localidad.Text = ""
    Fac_Vta.Plazo.Text = ""
    Fac_Vta.Tipo_Cliente.Text = ""
    Fac_Vta.Vto.Text = ""
    
    Fac_Vta.Neto_Gravado.Text = ""
    Fac_Vta.Exento.Text = ""
    Fac_Vta.Iva_Inscrip.Text = ""
    Fac_Vta.Sub_Total.Text = ""
    Fac_Vta.Sobre_Tasa.Text = ""
    Fac_Vta.Retencion_Iva.Text = ""
    Fac_Vta.Retencion_IIBB.Text = ""
    Fac_Vta.Total_Factura.Text = ""
    
    Menu.Menu_Cliente.Enabled = True
    Menu.Menu_Articulo.Enabled = True
    Menu.Menu_Proveedor.Enabled = True
    Menu.Menu_Facturaci�n.Enabled = True
    Menu.menu_informa.Enabled = True
    Menu.Menu_Contan.Enabled = True
    Menu.Men_Tab_Aux.Enabled = True
    Menu.Ventanas.Enabled = True
    Menu.Salidas.Enabled = True
    Menu.Menu_Help.Enabled = True

    Fac_Vta.Enabled = True
    Fac_Vta.Cliente.SetFocus
End Sub

Private Sub Linea_Cuatro_GotFocus()
    Linea_Cuatro.Text = Trim$(Linea_Cuatro.Text)
    Linea_Cuatro.SelStart = 0
    Linea_Cuatro.SelText = ""
    Linea_Cuatro.SelLength = Len(Linea_Cuatro.Text)
End Sub

Private Sub Linea_Cuatro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Linea_Cuatro_LostFocus()
    Linea_Cuatro.Text = FiltroCaracter(Linea_Cuatro.Text)
End Sub

Private Sub Linea_Dos_GotFocus()
    Linea_Dos.Text = Trim$(Linea_Dos.Text)
    Linea_Dos.SelStart = 0
    Linea_Dos.SelText = ""
    Linea_Dos.SelLength = Len(Linea_Dos.Text)
End Sub

Private Sub Linea_Dos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Linea_Dos_LostFocus()
    Linea_Dos.Text = FiltroCaracter(Linea_Dos.Text)
End Sub

Private Sub Linea_Tres_GotFocus()
    Linea_Tres.Text = Trim$(Linea_Tres.Text)
    Linea_Tres.SelStart = 0
    Linea_Tres.SelText = ""
    Linea_Tres.SelLength = Len(Linea_Tres.Text)
End Sub

Private Sub Linea_Tres_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Linea_Tres_LostFocus()
    Linea_Tres.Text = FiltroCaracter(Linea_Tres.Text)
End Sub

Private Sub Linea_Uno_GotFocus()
    Linea_Uno.Text = Trim$(Linea_Uno.Text)
    Linea_Uno.SelStart = 0
    Linea_Uno.SelText = ""
    Linea_Uno.SelLength = Len(Linea_Uno.Text)
End Sub

Private Sub Linea_Uno_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Linea_Uno_LostFocus()
    Linea_Uno.Text = FiltroCaracter(Linea_Uno.Text)
End Sub

Private Sub Remito_Dos_GotFocus()
    Remito_Dos.Text = Trim$(Remito_Dos.Text)
    Remito_Dos.SelStart = 0
    Remito_Dos.SelText = ""
    Remito_Dos.SelLength = Len(Remito_Dos.Text)
End Sub

Private Sub Remito_Dos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Remito_Dos_LostFocus()
    Remito_Dos.Text = FiltroCaracter(Remito_Dos.Text)
End Sub

Private Sub Remito_Uno_GotFocus()
    Remito_Uno.Text = Trim$(Remito_Uno.Text)
    Remito_Uno.SelStart = 0
    Remito_Uno.SelText = ""
    Remito_Uno.SelLength = Len(Remito_Uno.Text)
End Sub

Private Sub Remito_Uno_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Remito_Uno_LostFocus()
    Remito_Uno.Text = FiltroCaracter(Remito_Uno.Text)
End Sub
