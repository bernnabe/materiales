VERSION 5.00
Begin VB.Form frmAlertas 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   645
   ClientLeft      =   45
   ClientTop       =   285
   ClientWidth     =   6465
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   645
   ScaleWidth      =   6465
   ShowInTaskbar   =   0   'False
   Begin VB.Timer Timer1 
      Interval        =   30000
      Left            =   75
      Top             =   150
   End
   Begin VB.Label lblMensaje 
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00800000&
      Height          =   690
      Left            =   75
      TabIndex        =   0
      Top             =   0
      Width           =   6315
   End
End
Attribute VB_Name = "frmAlertas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()

    Me.Top = (Screen.Height - Me.Height) / 1.21
    Me.Left = (Screen.Width - Me.Width) / 2

    Timer1.Interval = 1000
    
End Sub

Public Function MostrarMensaje(mensaje As String)
    lblMensaje.Caption = mensaje
End Function
    
Private Sub Timer1_Timer()
    Unload Me
End Sub
