VERSION 5.00
Begin VB.Form Dat_Art 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaci�n de Art�culos.-"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8535
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   8535
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Articulo 
      Height          =   975
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   8535
      Begin VB.ComboBox Tipo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         TabIndex        =   0
         Top             =   240
         Width           =   6615
      End
      Begin VB.ComboBox Articulos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         TabIndex        =   42
         Top             =   600
         Visible         =   0   'False
         Width           =   6615
      End
      Begin VB.TextBox Descripcion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   30
         TabIndex        =   2
         ToolTipText     =   "Descripci�n.-"
         Top             =   600
         Width           =   6615
      End
      Begin VB.TextBox Articulo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         MaxLength       =   5
         TabIndex        =   1
         Top             =   600
         Width           =   735
      End
      Begin VB.CommandButton Buscar_Articulos 
         Caption         =   "&Art�culo:"
         Height          =   255
         Left            =   120
         TabIndex        =   32
         ToolTipText     =   "Buscar art�culos existentes en la base de datos.-"
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   54
         Top             =   240
         Width           =   1575
      End
   End
   Begin VB.Frame Marco_Datos 
      Enabled         =   0   'False
      Height          =   5175
      Left            =   0
      TabIndex        =   34
      Top             =   960
      Width           =   8535
      Begin VB.TextBox MEMO 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4815
         Left            =   120
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   31
         Top             =   4800
         Visible         =   0   'False
         Width           =   8295
      End
      Begin VB.TextBox Medida_rev 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4080
         TabIndex        =   53
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Medida 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000000&
         Height          =   285
         Left            =   4080
         TabIndex        =   51
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Precio_Recargo_Pub_Cta 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2985
         TabIndex        =   62
         Top             =   2760
         Width           =   1095
      End
      Begin VB.TextBox Precio_Recargo_Pub_Ctdo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2985
         TabIndex        =   61
         Top             =   2400
         Width           =   1095
      End
      Begin VB.TextBox Precio_Recargo_Rev_Cta 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7305
         TabIndex        =   60
         Top             =   2040
         Width           =   1095
      End
      Begin VB.TextBox Precio_Recargo_Rev_Ctdo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2985
         TabIndex        =   59
         Top             =   2040
         Width           =   1095
      End
      Begin VB.TextBox Precio_ML_Cta 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7305
         MaxLength       =   8
         TabIndex        =   56
         Top             =   2760
         Width           =   1095
      End
      Begin VB.TextBox Precio_ML_Ctdo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7305
         MaxLength       =   8
         TabIndex        =   55
         Top             =   2400
         Width           =   1095
      End
      Begin VB.ComboBox Presentacion_Rev 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         TabIndex        =   5
         Top             =   600
         Width           =   2175
      End
      Begin VB.TextBox Precio_Pub_Cta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1785
         TabIndex        =   14
         Top             =   2760
         Width           =   1215
      End
      Begin VB.TextBox Precio_Rev_Ctdo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         TabIndex        =   11
         Top             =   2040
         Width           =   1215
      End
      Begin VB.TextBox Margen_Util_Rev 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7560
         MaxLength       =   6
         TabIndex        =   6
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox Precio_Rev_Cta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6120
         TabIndex        =   12
         Top             =   2040
         Width           =   1215
      End
      Begin VB.TextBox Precio_Pub_Ctdo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1785
         TabIndex        =   13
         Top             =   2400
         Width           =   1215
      End
      Begin VB.TextBox Ancho 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4440
         MaxLength       =   10
         TabIndex        =   8
         Top             =   1080
         Width           =   1215
      End
      Begin VB.TextBox Largo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7200
         MaxLength       =   10
         TabIndex        =   9
         Top             =   1080
         Width           =   1215
      End
      Begin VB.TextBox Espesor 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   10
         TabIndex        =   7
         Top             =   1080
         Width           =   1215
      End
      Begin VB.ComboBox Presentacion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1800
         TabIndex        =   3
         Top             =   240
         Width           =   2175
      End
      Begin VB.TextBox Stock_Min 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7065
         MaxLength       =   10
         TabIndex        =   16
         Top             =   3120
         Width           =   1335
      End
      Begin VB.ComboBox Proveedor 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1785
         TabIndex        =   18
         Top             =   3840
         Width           =   6615
      End
      Begin VB.TextBox Existencia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1785
         MaxLength       =   10
         TabIndex        =   15
         Top             =   3120
         Width           =   1335
      End
      Begin VB.TextBox Margen_Util_Pub 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7560
         MaxLength       =   6
         TabIndex        =   4
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Precio_Compra 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         MaxLength       =   10
         TabIndex        =   10
         Top             =   1440
         Width           =   1215
      End
      Begin VB.CheckBox Integra_Listado 
         Caption         =   "&Integra Listado"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1785
         TabIndex        =   17
         Top             =   3480
         Width           =   1455
      End
      Begin VB.Frame Marco_Recargos 
         Enabled         =   0   'False
         Height          =   1095
         Left            =   0
         TabIndex        =   67
         Top             =   4080
         Width           =   8535
         Begin VB.TextBox Ancho_Menor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1800
            TabIndex        =   19
            Top             =   240
            Width           =   1095
         End
         Begin VB.TextBox Largo_Menor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1800
            TabIndex        =   23
            Top             =   600
            Width           =   1095
         End
         Begin VB.TextBox Recargo_Ancho_Menor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3480
            TabIndex        =   20
            Top             =   240
            Width           =   855
         End
         Begin VB.TextBox Recargo_Largo_Menor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3480
            TabIndex        =   24
            Top             =   600
            Width           =   855
         End
         Begin VB.TextBox Ancho_Mayor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5760
            TabIndex        =   21
            Top             =   240
            Width           =   1095
         End
         Begin VB.TextBox Recargo_Ancho_Mayor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7560
            TabIndex        =   22
            Top             =   240
            Width           =   855
         End
         Begin VB.TextBox Largo_Mayor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   5760
            TabIndex        =   25
            Top             =   600
            Width           =   1095
         End
         Begin VB.TextBox Recargo_Largo_Mayor 
            BackColor       =   &H00E0E0E0&
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   7560
            TabIndex        =   26
            Top             =   600
            Width           =   855
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Caption         =   "% Rec.:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   3
            Left            =   6600
            TabIndex        =   75
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Caption         =   "% Rec.:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   1
            Left            =   6720
            TabIndex        =   74
            Top             =   600
            Width           =   855
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Caption         =   "% Rec.:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   2
            Left            =   2640
            TabIndex        =   73
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label19 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Ancho < de:"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   840
            TabIndex        =   72
            Top             =   240
            Width           =   870
         End
         Begin VB.Label Label20 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "Largo < de:"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   840
            TabIndex        =   71
            Top             =   600
            Width           =   810
         End
         Begin VB.Label Label21 
            Alignment       =   1  'Right Justify
            Caption         =   "Ancho > de:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4680
            TabIndex        =   70
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label22 
            Alignment       =   1  'Right Justify
            Caption         =   "Largo > de:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4680
            TabIndex        =   69
            Top             =   600
            Width           =   975
         End
         Begin VB.Label Label23 
            Alignment       =   2  'Center
            Caption         =   "% Rec.:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   0
            Left            =   2760
            TabIndex        =   68
            Top             =   600
            Width           =   855
         End
      End
      Begin VB.Label Label24 
         Alignment       =   1  'Right Justify
         Caption         =   "Precio de ML a Rev. a CtaCte:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   5025
         TabIndex        =   66
         Top             =   2760
         Width           =   2175
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "P2"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   6105
         TabIndex        =   65
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Caption         =   "Precio c/Rec."
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   7305
         TabIndex        =   64
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Caption         =   "Precio c/Rec."
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   2985
         TabIndex        =   63
         Top             =   1800
         Width           =   1095
      End
      Begin VB.Label Label24 
         Alignment       =   1  'Right Justify
         Caption         =   "Precio del ML al P�b. de Ctdo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   5025
         TabIndex        =   58
         Top             =   2400
         Width           =   2175
      End
      Begin VB.Label Label3 
         Alignment       =   2  'Center
         Caption         =   "P2"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   1785
         TabIndex        =   57
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Presentaci�n Rev.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   52
         Top             =   600
         Width           =   1575
      End
      Begin VB.Label Label16 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Precio Vta. Ctdo. Rev:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   90
         TabIndex        =   49
         Top             =   2040
         Width           =   1590
      End
      Begin VB.Label Label18 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "% Margen Util. Rev.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5520
         TabIndex        =   48
         Top             =   600
         Width           =   1935
      End
      Begin VB.Label Label17 
         Alignment       =   1  'Right Justify
         Caption         =   "Precio CtaCte. Rev.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4425
         TabIndex        =   47
         Top             =   2040
         Width           =   1575
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Precio Vta. Pub. Ctdo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   105
         TabIndex        =   46
         Top             =   2400
         Width           =   1575
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Caption         =   "Largo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5760
         TabIndex        =   45
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         Caption         =   "Ancho """
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3000
         TabIndex        =   44
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Espersor """
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   43
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Existencia:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   105
         TabIndex        =   41
         Top             =   3120
         Width           =   1575
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "% Margen Util. Pub.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5520
         TabIndex        =   40
         Top             =   240
         Width           =   1935
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Proveedor:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   3840
         Width           =   1575
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Stock M�nimo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5400
         TabIndex        =   38
         Top             =   3120
         Width           =   1575
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Precio de Compra:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   37
         Top             =   1440
         Width           =   1575
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Presentaci�n Pub:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   240
         Width           =   1575
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Precio P�b.CtaCte:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   -135
         TabIndex        =   50
         Top             =   2760
         Width           =   1815
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   35
      Top             =   6120
      Width           =   8535
      Begin VB.CommandButton Comentarios 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Dat_Art.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   30
         ToolTipText     =   "Mostrar el Campo Comentarios.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   6120
         Picture         =   "Dat_Art.frx":030A
         Style           =   1  'Graphical
         TabIndex        =   28
         ToolTipText     =   "Borrar el Art�culo.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   5160
         Picture         =   "Dat_Art.frx":0614
         Style           =   1  'Graphical
         TabIndex        =   27
         ToolTipText     =   "Grabar los Datos.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   7440
         Picture         =   "Dat_Art.frx":091E
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "Dat_Art"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Ancho_GotFocus()
    Ancho.Text = Trim$(Ancho.Text)
    Ancho.SelStart = 0
    Ancho.SelText = ""
    Ancho.SelLength = Len(Ancho.Text)
End Sub

Private Sub Ancho_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_LostFocus()
    Ancho.Text = Formateado(Str$(Val(Ancho.Text)), 2, 9, " ", False)
End Sub

Private Sub Ancho_Mayor_GotFocus()
    Ancho_Mayor.Text = Trim$(Ancho_Mayor.Text)
    Ancho_Mayor.SelStart = 0
    Ancho_Mayor.SelText = ""
    Ancho_Mayor.SelLength = Len(Ancho_Mayor.Text)
End Sub

Private Sub Ancho_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_Mayor_LostFocus()
    Ancho_Mayor.Text = Formateado(Str$(Val(Ancho_Mayor.Text)), 2, 8, " ", False)
    Aderir_Recargos
End Sub

Private Sub Ancho_Menor_GotFocus()
    Ancho_Menor.Text = Trim$(Ancho_Menor.Text)
    Ancho_Menor.SelStart = 0
    Ancho_Menor.SelText = ""
    Ancho_Menor.SelLength = Len(Ancho_Menor.Text)
End Sub

Private Sub Ancho_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_Menor_LostFocus()
    Ancho_Menor.Text = Formateado(Str$(Val(Ancho_Menor.Text)), 2, 8, " ", False)
    Aderir_Recargos
End Sub

Private Sub Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        If Val(Articulo.Text) = 0 And Trim$(Str$(Val(Mid$(Tipo.Text, 32, 5)))) > 0 Then
            Qy = "SELECT MAX(Id_Articulo) FROM Articulo WHERE Id_Rubro = " & Trim$(Str$(Val(Mid$(Tipo.Text, 32, 5))))
            Set Rs = Db.OpenRecordset(Qy)
        
            Articulo.Text = 1
            If Not Rs.Fields(0) = "" Then Articulo.Text = Val(Rs.Fields(0)) + 1
        ElseIf Val(Articulo.Text) > 0 Then
            SendKeys "{TAB}"
        End If
    End If
End Sub

Private Sub Articulo_LostFocus()
    If Val(Articulo.Text) > 0 Then
        Leer_Articulo
    End If
End Sub

Private Sub Leer_Articulo()
    If Val(Articulo.Text) > 0 And Trim$(Str$(Val(Mid$(Tipo.Text, 32, 5)))) > 0 Then
        Qy = "SELECT Articulo.*,Proveedor.* FROM Articulo,Proveedor WHERE Articulo.Id_Articulo = " & Trim$(Str$(Val(Articulo.Text))) & " "
        Qy = Qy & "AND Articulo.Id_Rubro = " & Trim$(Str$(Val(Mid$(Tipo.Text, 32, 5)))) & " "
        Set Rs = Db.OpenRecordset(Qy)
        
        'Borrar_Campo
        Comentarios.Enabled = True
        If Rs.EOF Then
            Grabar.Tag = "Alta"
            Grabar.Enabled = True
            Marco_Datos.Enabled = True
            Descripcion.SetFocus
        Else
            Grabar.Tag = "Modificaci�n"
            Grabar.Enabled = True
            Marco_Datos.Enabled = True
            Borrar.Enabled = True
            
            If Val(Permiso_Cons) = 1 Then
                Mostrar_Articulo
            Else
                MsgBox "Usted no est� autorizado para estos procesos.!", vbCritical, "Atenci�n.!"
                Borrar_Campo
                Tipo.Text = ""
                Articulo.Text = ""
                Tipo.SetFocus
            End If
        End If
    Else
        Articulo.Text = ""
        Tipo.SetFocus
    End If
End Sub

Private Sub Borrar_Campo()
    Espesor.Text = ""
    Ancho.Text = ""
    Largo.Text = ""
    Medida.Text = ""
    Medida_rev.Text = ""
    Margen_Util_Pub.Text = ""
    Margen_Util_Rev.Text = ""
    
    Integra_Listado.Value = 0
    Precio_Pub_Ctdo.Text = ""
    Precio_Pub_Cta.Text = ""
    Precio_Recargo_Rev_Ctdo.Text = ""
    Precio_Recargo_Rev_Cta.Text = ""
    Precio_Recargo_Pub_Ctdo.Text = ""
    Precio_Recargo_Pub_Cta.Text = ""
    Precio_Rev_Ctdo.Text = ""
    Precio_Rev_Cta.Text = ""
    Precio_ML_Ctdo.Text = ""
    Precio_ML_Cta.Text = ""
    
    Descripcion.Text = ""
    Grabar.Tag = ""
    Stock_Min.Text = ""
    Existencia.Text = ""
    MEMO.Text = ""
    Precio_Compra.Text = ""
    Margen_Util_Pub.Text = ""
    Margen_Util_Rev.Text = ""
    Presentacion.Text = ""
    Presentacion_Rev.Text = ""
    Tipo.Text = ""
    Proveedor.Text = ""
    Ancho_Menor.Text = ""
    Recargo_Ancho_Menor.Text = ""
    Largo_Menor.Text = ""
    Recargo_Largo_Menor.Text = ""
    Ancho_Mayor.Text = ""
    Recargo_Ancho_Mayor.Text = ""
    Largo_Mayor.Text = ""
    Recargo_Largo_Mayor.Text = ""
    
    Ancho_Menor.BackColor = 14737632
    Recargo_Ancho_Menor.BackColor = 14737632
    Largo_Menor.BackColor = 14737632
    Recargo_Largo_Menor.BackColor = 14737632
    Ancho_Mayor.BackColor = 14737632
    Recargo_Ancho_Mayor.BackColor = 14737632
    Largo_Mayor.BackColor = 14737632
    Recargo_Largo_Mayor.BackColor = 14737632
    
    Espesor.BackColor = 14737632
    Ancho.BackColor = 14737632
    Largo.BackColor = 14737632
    
    Espesor.Enabled = False
    Ancho.Enabled = False
    Largo.Enabled = False
    Grabar.Enabled = False
    Marco_Datos.Enabled = False
    Borrar.Enabled = False
    Comentarios.Enabled = False
End Sub

Private Sub Mostrar_Articulo()
    Tipo.Text = Trim$(Rs.Fields("Id_Rubro"))
    Descripcion.Text = Rs.Fields("Descripcion")
    Espesor.Text = Formateado(Str$(Val(Rs.Fields("Espesor"))), 2, 9, " ", False)
    Ancho.Text = Formateado(Str$(Val(Rs.Fields("Ancho"))), 2, 9, " ", False)
    Largo.Text = Formateado(Str$(Val(Rs.Fields("Largo"))), 2, 9, " ", False)
    Precio_Compra.Text = Formateado(Str$(Val(Rs.Fields("Precio_Compra"))), 2, 9, " ", False)
    Precio_Pub_Ctdo.Text = Formateado(Str$(Val(Rs.Fields("Precio_Ctdo_Pub"))), 2, 9, " ", False)
    Precio_Pub_Cta.Text = Formateado(Str$(Val(Rs.Fields("Precio_CtaCte_Pub"))), 2, 9, " ", False)
    Precio_Rev_Ctdo.Text = Formateado(Str$(Val(Rs.Fields("Precio_Ctdo_Rev"))), 2, 9, " ", False)
    Precio_Rev_Cta.Text = Formateado(Str$(Val(Rs.Fields("Precio_CtaCte_Rev"))), 2, 9, " ", False)
    Existencia.Text = Formateado(Str$(Val(Rs.Fields("Existencia"))), 2, 10, " ", False)
    Stock_Min.Text = Formateado(Str$(Val(Rs.Fields("Stock_Minimo"))), 2, 10, " ", False)
    
    Ancho_Menor.Text = Formateado(Str$(Val(Rs.Fields("Ancho_Menor"))), 2, 8, " ", False)
    Recargo_Ancho_Menor.Text = Formateado(Str$(Val(Rs.Fields("Rec_Ancho_Menor"))), 2, 6, " ", False)
    
    Largo_Menor.Text = Formateado(Str$(Val(Rs.Fields("Largo_Menor"))), 2, 8, " ", False)
    Recargo_Largo_Menor.Text = Formateado(Str$(Val(Rs.Fields("Rec_Largo_Menor"))), 2, 6, " ", False)
    
    Ancho_Mayor.Text = Formateado(Str$(Val(Rs.Fields("Ancho_Mayor"))), 2, 8, " ", False)
    Recargo_Ancho_Mayor.Text = Formateado(Str$(Val(Rs.Fields("Rec_Ancho_Mayor"))), 2, 6, " ", False)
    
    Largo_Mayor.Text = Formateado(Str$(Val(Rs.Fields("Largo_Mayor"))), 2, 8, " ", False)
    Recargo_Largo_Mayor.Text = Formateado(Str$(Val(Rs.Fields("Rec_Largo_Mayor"))), 2, 6, " ", False)
    
    Proveedor.Text = Val(Rs.Fields("Proveedor"))
    
    Integra_Listado.Value = Val(Rs.Fields("Integra_Listado"))
    
    MEMO.Text = Rs.Fields("Articulo.Observaciones")
    
    If Trim$(Str$(Val(Rs.Fields("Presentacion_Pub")))) = 1 Then
        Presentacion.ListIndex = 0
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Pub")))) = 2 Then
        Presentacion.ListIndex = 1
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Pub")))) = 3 Then
        Presentacion.ListIndex = 2
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Pub")))) = 4 Then
        Presentacion.ListIndex = 3
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Pub")))) = 5 Then
        Presentacion.ListIndex = 4
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Pub")))) = 6 Then
        Presentacion.ListIndex = 5
    End If
    
    If Trim$(Str$(Val(Rs.Fields("Presentacion_Rev")))) = 1 Then
        Presentacion_Rev.ListIndex = 0
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Rev")))) = 2 Then
        Presentacion_Rev.ListIndex = 1
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Rev")))) = 3 Then
        Presentacion_Rev.ListIndex = 2
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Rev")))) = 4 Then
        Presentacion_Rev.ListIndex = 3
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Rev")))) = 5 Then
        Presentacion_Rev.ListIndex = 4
    ElseIf Trim$(Str$(Val(Rs.Fields("Presentacion_Rev")))) = 6 Then
        Presentacion_Rev.ListIndex = 5
    End If
    
    Habilitar_Medidas
    Aderir_Recargos
    
    Precio_Pub_Ctdo_LostFocus
    Precio_Pub_Cta_LostFocus
    
    Margen_Util_Rev.Text = Formateado(Str$(Val(Rs.Fields("Margen_Rev"))), 2, 6, " ", False)
    Margen_Util_Pub.Text = Formateado(Str$(Val(Rs.Fields("Margen_Pub"))), 2, 6, " ", False)
    
    Calcular_Medida_Presentacion
    Tipo_LostFocus
    
    If Val(Proveedor.Text) > 0 Then
        Qy = "SELECT * FROM Proveedor WHERE Id_Proveedor = " & Trim$(Str$(Val(Proveedor.Text)))
        Set Rs = Db.OpenRecordset(Qy)
        
        If Not Rs.EOF Then
            Proveedor.Text = Trim$(Rs.Fields("Nombre")) + Space$(31 - Len(Trim$(Rs.Fields("Nombre")))) & Trim$(Rs.Fields("Id_Proveedor"))
        End If
    End If
End Sub

Private Sub Articulos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo.SetFocus
End Sub

Private Sub Articulos_Encontrados_LostFocus()
    Tipo.Text = Mid$(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 32, 5)
    Tipo_LostFocus
    Articulo.Text = Mid$(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 38)
    If Val(Articulo.Text) > 0 Then
        Descripcion.SetFocus
    End If
    Articulos_Encontrados.Visible = False
End Sub

Private Sub Borrar_Click()
    If Val(Permiso_Alta) = 1 Then
        If MsgBox("Esta seguro de borrar �ste art�culo. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            Qy = "DELETE * FROM Articulo WHERE Id_Articulo = " & Trim$(Str$(Val(Articulo.Text))) & " "
            Qy = Qy & "AND Id_Rubro = " & Trim$(Str$(Val(Mid$(Tipo.Text, 31, 5))))
            Db.Execute (Qy)
        End If
        
        Salir_Click
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Buscar_Articulos_Click()
    Articulos_Encontrados.Visible = True
    Articulo.Text = ""
    MousePointer = 11
    
    If Trim$(Str$(Val(Mid$(Tipo.Text, 31, 5)))) > 0 Then
        Qy = "SELECT * FROM Articulo WHERE Id_Rubro = " & Trim$(Str$(Val(Mid$(Tipo.Text, 31, 5)))) & " ORDER BY Descripcion"
    Else
        Qy = "SELECT * FROM Articulo ORDER BY Descripcion"
    End If
    Set Rs = Db.OpenRecordset(Qy)
        
    While Not Rs.EOF
        Articulos_Encontrados.AddItem Trim$(Rs.Fields("Descripcion")) + Space$(30 - Len(Trim$(Rs.Fields("Descripcion")))) & " " & Formateado(Str$(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & (Rs.Fields("Id_Articulo"))
        Rs.MoveNext
    Wend
    MousePointer = 0
    Articulos_Encontrados.SetFocus
End Sub

Private Sub Comentarios_Click()
    If MEMO.Visible = True Then
        Salir.Cancel = True
        Grabar.SetFocus
        MEMO.Visible = False
    Else
        Comentarios.Cancel = True
        MEMO.Visible = True
        MEMO.SetFocus
    End If
End Sub

Private Sub Descripcion_GotFocus()
    Descripcion.Text = Trim$(Descripcion.Text)
    Descripcion.SelStart = 0
    Descripcion.SelText = ""
    Descripcion.SelLength = Len(Descripcion.Text)
End Sub

Private Sub Descripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Descripcion_LostFocus()
    Descripcion.Text = UCase$(Descripcion.Text)
    Descripcion.Text = FiltroCaracter(Descripcion.Text)
End Sub

Private Sub Espesor_GotFocus()
    Espesor.Text = Trim$(Espesor.Text)
    Espesor.SelStart = 0
    Espesor.SelText = ""
    Espesor.SelLength = Len(Espesor.Text)
End Sub

Private Sub Espesor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Espesor_LostFocus()
    Espesor.Text = Formateado(Str$(Val(Espesor.Text)), 2, 9, " ", False)
End Sub

Private Sub Existencia_GotFocus()
    Existencia.Text = Trim$(Existencia.Text)
    Existencia.SelStart = 0
    Existencia.SelText = ""
    Existencia.SelLength = Len(Existencia.Text)
End Sub

Private Sub Existencia_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Existencia_LostFocus()
    Existencia.Text = Formateado(Str$(Val(Existencia.Text)), 2, 10, " ", False)
End Sub

Private Sub Form_Load()
    Dim i As Integer
    
    Me.Top = (Screen.Height - Me.Height) / 10
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    For i = 1 To 7
        Presentacion.AddItem Articulo_Presentacion(i)
        Presentacion_Rev.AddItem Articulo_Presentacion(i)
    Next
    
    Cargar_Datos
    
    Menu.Estado.Panels(2).Text = "Actualicaci�n de Stock.-"
End Sub

Private Sub Cargar_Datos()
    '�ste procedimiento cargar los datos necesarios para el alta de art�culos.-
    
    Qy = "SELECT * FROM Proveedor ORDER BY Nombre"
    Set Rs = Db.OpenRecordset(Qy)
    
    While Not Rs.EOF
        Proveedor.AddItem Trim$(Rs.Fields("Nombre")) + Space$(31 - Len(Trim$(Rs.Fields("Nombre")))) & Trim$(Rs.Fields("Id_Proveedor"))
        Rs.MoveNext
    Wend
    
    Qy = "SELECT * FROM Rubro ORDER BY Denominacion"
    Set Rs = Db.OpenRecordset(Qy)
        
    While Not Rs.EOF
        Tipo.AddItem Trim$(Rs.Fields("Denominacion")) + Space$(31 - Len(Trim$(Rs.Fields("Denominacion")))) & Trim$(Rs.Fields("Id_Rubro")) + Space$(5 - Len(Trim$(Rs.Fields("Id_Rubro")))) & " " & Formateado(Str$(Val(Rs.Fields("Margen_Util_Pub"))), 2, 10, " ", False) & " " & Formateado(Str$(Val(Rs.Fields("Margen_Util_Rev"))), 2, 10, " ", False)
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        
        If Val(Permiso_Alta) = 1 Then
            'Agrego el Art�culo a la Base de Datos.-
            
            Qy = "INSERT INTO Articulo VALUES ("
            Qy = Qy & Trim$(Str$(Val(Mid$(Tipo.Text, 31, 5)))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Articulo.Text))) & " "
            Qy = Qy & ", '" & Trim$(Descripcion.Text) & "'"
            Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Presentacion.Text, 1, 1)))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Presentacion_Rev.Text, 1, 1)))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Espesor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Ancho.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Largo.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Margen_Util_Pub.Text)))
            Qy = Qy & ", " & Trim$(Str$(Val(Margen_Util_Rev.Text)))
            Qy = Qy & ", " & Trim$(Str$(Val(Precio_Compra.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Precio_Pub_Ctdo.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Precio_Pub_Cta.Text))) & " "
            Qy = Qy & ", " & Trim(Str$(Val(Precio_Rev_Ctdo.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Precio_Rev_Cta.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Existencia.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Stock_Min.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Ancho_Menor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Recargo_Ancho_Menor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Largo_Menor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Recargo_Largo_Menor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Ancho_Mayor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Recargo_Ancho_Mayor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Largo_Mayor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Recargo_Largo_Mayor.Text))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Proveedor.Text, 31)))) & " "
            Qy = Qy & ", " & Trim$(Str$(Val(Integra_Listado.Value))) & " "
            Qy = Qy & ", '6.1.1.2.0.0'"
            Qy = Qy & ", '4.1.1.0.0.0'"
            Qy = Qy & ", '" & Trim$(Fecha_Fiscal) & "'"
            Qy = Qy & ", '" & Trim$(MEMO.Text) & "')"
            
            Db.Execute (Qy)
        Else
            MsgBox "Usted no est� autorizado para estos procesos, el art�culo no ser� guardado.", vbCritical, "Atenci�n.!"
        End If
    Else
        
        'Modifico los datos del Art�culo.-
        
        If Val(Permiso_Mod) = 1 Then
            Qy = "UPDATE Articulo SET "
            Qy = Qy & "Descripcion = '" & Trim$(Descripcion.Text) & "',"
            Qy = Qy & "Id_Rubro = " & Trim$(Str$(Val(Mid$(Tipo.Text, 31, 5)))) & ", "
            Qy = Qy & "Presentacion_Pub = " & Trim$(Str$(Val(Mid$(Presentacion.Text, 1, 1)))) & ", "
            Qy = Qy & "Presentacion_Rev = " & Trim$(Str$(Val(Mid$(Presentacion_Rev.Text, 1, 1)))) & ", "
            Qy = Qy & "Espesor = " & Trim$(Str$(Val(Espesor.Text))) & ", "
            Qy = Qy & "Ancho = " & Trim$(Str$(Val(Ancho.Text))) & ", "
            Qy = Qy & "Largo = " & Trim$(Str$(Val(Largo.Text))) & ", "
            Qy = Qy & "Margen_Pub = " & Trim$(Str$(Val(Margen_Util_Pub.Text))) & ","
            Qy = Qy & "Margen_Rev = " & Trim$(Str$(Val(Margen_Util_Rev.Text))) & ","
            Qy = Qy & "Precio_Compra = " & Trim$(Str$(Val(Precio_Compra.Text))) & ", "
            Qy = Qy & "Precio_Ctdo_Pub = " & Trim$(Str$(Val(Precio_Pub_Ctdo.Text))) & ", "
            Qy = Qy & "Precio_CtaCte_Pub = " & Trim$(Str$(Val(Precio_Pub_Cta.Text))) & ", "
            Qy = Qy & "Precio_Ctdo_Rev = " & Trim(Str$(Val(Precio_Rev_Ctdo.Text))) & ", "
            Qy = Qy & "Precio_Ctacte_Rev = " & Trim$(Str$(Val(Precio_Rev_Cta.Text))) & ", "
            Qy = Qy & "Existencia = " & Trim$(Str$(Val(Existencia.Text))) & ", "
            Qy = Qy & "Stock_Minimo = " & Trim$(Str$(Val(Stock_Min.Text))) & ", "
            Qy = Qy & "Ancho_Menor = " & Trim$(Str$(Val(Ancho_Menor.Text))) & ", "
            Qy = Qy & "Rec_Ancho_Menor = " & Trim$(Str$(Val(Recargo_Ancho_Menor.Text))) & ", "
            Qy = Qy & "Largo_Menor = " & Trim$(Str$(Val(Largo_Menor.Text))) & ", "
            Qy = Qy & "Rec_Largo_Menor = " & Trim$(Str$(Val(Recargo_Largo_Menor.Text))) & ", "
            Qy = Qy & "Ancho_Mayor = " & Trim$(Str$(Val(Ancho_Mayor.Text))) & ", "
            Qy = Qy & "Rec_Ancho_Mayor = " & Trim$(Str$(Val(Recargo_Ancho_Mayor.Text))) & ", "
            Qy = Qy & "Largo_Mayor = " & Trim$(Str$(Val(Largo_Mayor.Text))) & ", "
            Qy = Qy & "Rec_Largo_Mayor = " & Trim$(Str$(Val(Recargo_Largo_Mayor.Text))) & ", "
            Qy = Qy & "Proveedor = " & Trim$(Str$(Val(Mid$(Proveedor.Text, 31)))) & ", "
            Qy = Qy & "Integra_Listado = " & Trim$(Str$(Val(Integra_Listado.Value))) & ", "
            Qy = Qy & "Observaciones = '" & Trim$(MEMO.Text) & "' "
            Qy = Qy & "WHERE Id_Articulo = " & Trim$(Str$(Val(Articulo.Text))) & " AND Id_Rubro = " & Trim$(Str$(Val(Mid$(Tipo.Text, 32, 5))))
            Db.Execute (Qy)
        Else
            MsgBox "Usted no est� autorizado para estos procesos, no se guardar�n los cambios.", vbCritical, "Atenci�n.!"
        End If
    End If
    
    Salir_Click
End Sub

Private Sub Marca_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Integra_Listado_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_GotFocus()
    Largo.Text = Trim$(Largo.Text)
    Largo.SelStart = 0
    Largo.SelText = ""
    Largo.SelLength = Len(Largo.Text)
End Sub

Private Sub Largo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_LostFocus()
    Largo.Text = Formateado(Str$(Val(Largo.Text)), 2, 9, " ", False)
    
    If Val(Mid$(Presentacion.Text, 1, 1)) = 3 Or Val(Mid$(Presentacion.Text, 1, 1)) = 4 Or Val(Mid$(Presentacion.Text, 1, 1)) = 5 Then
        If Val(Len(Trim$(Descripcion.Text))) = 0 Then
            Descripcion.Text = Trim$(Mid$(Tipo.Text, 1, 30)) & " " & Trim$(Str$(Val(Espesor.Text))) & """"
        End If
    End If
End Sub

Private Sub Calcular_Medida_Presentacion()
    If Trim$(Str$(Val(Presentacion.List(Presentacion.ListIndex)))) = 3 Or Trim$(Str$(Val(Presentacion.List(Presentacion.ListIndex)))) = 4 Or Trim$(Str$(Val(Presentacion.List(Presentacion.ListIndex)))) = 5 Or Trim$(Str$(Val(Presentacion_Rev.List(Presentacion_Rev.ListIndex)))) = 3 Or Trim$(Str$(Val(Presentacion_Rev.List(Presentacion_Rev.ListIndex)))) = 4 Or Trim$(Str$(Val(Presentacion_Rev.List(Presentacion_Rev.ListIndex)))) = 5 Then
        If Val(Espesor.Text) = 0 Then
            Espesor.Text = ""
            Espesor.SetFocus
            
            Exit Sub
        End If
        
        If Val(Mid$(Presentacion.Text, 1, 1)) = 3 Then
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
        ElseIf Val(Mid$(Presentacion.Text, 1, 1)) = 4 Then
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
        ElseIf Val(Mid$(Presentacion.Text, 1, 1)) = 5 Then
            Medida.Text = Formateado(Str$(Val(Largo.Text)), 2, 9, " ", False)
        End If
        
        If Val(Mid$(Presentacion_Rev.Text, 1, 1)) = 3 Then
            Medida_rev.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
        ElseIf Val(Mid$(Presentacion_Rev.Text, 1, 1)) = 4 Then
            Medida_rev.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
        ElseIf Val(Mid$(Presentacion_Rev.Text, 1, 1)) = 5 Then
            Medida_rev.Text = Formateado(Str$(Val(Largo.Text)), 2, 9, " ", False)
        End If
    End If
End Sub

Private Sub Largo_Mayor_GotFocus()
    Largo_Mayor.Text = Trim$(Largo_Mayor.Text)
    Largo_Mayor.SelStart = 0
    Largo_Mayor.SelText = ""
    Largo_Mayor.SelLength = Len(Largo_Mayor.Text)
End Sub

Private Sub Largo_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_Mayor_LostFocus()
    Largo_Mayor.Text = Formateado(Str$(Val(Largo_Mayor.Text)), 2, 8, " ", False)
    Aderir_Recargos
End Sub

Private Sub Largo_Menor_GotFocus()
    Largo_Menor.Text = Trim$(Largo_Menor.Text)
    Largo_Menor.SelStart = 0
    Largo_Menor.SelText = ""
    Largo_Menor.SelLength = Len(Largo_Menor.Text)
End Sub

Private Sub Largo_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_Menor_LostFocus()
    Largo_Menor.Text = Formateado(Str$(Val(Largo_Menor.Text)), 2, 8, " ", False)
    Aderir_Recargos
End Sub

Private Sub Margen_Util_Pub_GotFocus()
    Margen_Util_Pub.Text = Trim$(Margen_Util_Pub.Text)
    Margen_Util_Pub.SelStart = 0
    Margen_Util_Pub.SelText = ""
    Margen_Util_Pub.SelLength = Len(Margen_Util_Pub.Text)
End Sub

Private Sub Margen_Util_Pub_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Margen_Util_Pub_LostFocus()
    Margen_Util_Pub.Text = Formateado(Str$(Val(Margen_Util_Pub.Text)), 2, 6, " ", False)
    
    Precio_Rev_Ctdo_GotFocus
    Precio_Pub_Ctdo_GotFocus
End Sub

Private Sub Margen_Util_Rev_GotFocus()
    Margen_Util_Rev.Text = Trim$(Margen_Util_Rev.Text)
    Margen_Util_Rev.SelStart = 0
    Margen_Util_Rev.SelText = ""
    Margen_Util_Rev.SelLength = Len(Margen_Util_Rev.Text)
End Sub

Private Sub Margen_Util_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Margen_Util_Rev_LostFocus()
    Margen_Util_Rev.Text = Formateado(Str$(Val(Margen_Util_Rev.Text)), 2, 6, " ", False)
    Precio_Rev_Ctdo_GotFocus
    Precio_Pub_Ctdo_GotFocus
End Sub

Private Sub MEMO_LostFocus()
    MEMO.Text = UCase$(MEMO.Text)
    MEMO.Text = FiltroCaracter(MEMO.Text)
End Sub

Private Sub Precio_Compra_GotFocus()
    Precio_Compra.Text = Trim$(Precio_Compra.Text)
    Precio_Compra.SelStart = 0
    Precio_Compra.SelText = ""
    Precio_Compra.SelLength = Len(Precio_Compra.Text)
End Sub

Private Sub Precio_Compra_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Compra_LostFocus()
    Precio_Compra.Text = Formateado(Str$(Val(Precio_Compra.Text)), 2, 9, " ", False)
    Precio_Rev_Ctdo_GotFocus
    Precio_Pub_Ctdo_GotFocus
End Sub

Private Sub Precio_Pub_Cta_GotFocus()
    Precio_Pub_Cta.Text = Precio_Pub_Ctdo.Text
    Precio_Pub_Cta.Text = Trim$(Precio_Pub_Cta.Text)
    Precio_Pub_Cta.SelStart = 0
    Precio_Pub_Cta.SelText = ""
    Precio_Pub_Cta.SelLength = Len(Precio_Pub_Cta.Text)
End Sub

Private Sub Precio_Pub_Cta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Pub_Cta_LostFocus()
    Precio_Pub_Cta.Text = Formateado(Str$(Val(Precio_Pub_Cta.Text)), 2, 9, " ", False)
    
    If Val(Mid$(Presentacion.Text, 1, 2)) = 5 And Val(Precio_Pub_Cta.Text) > 0 Then
        If Val(Precio_Recargo_Pub_Cta.Text) = 0 Then
            Precio_ML_Cta.Text = (((Val(Espesor.Text) * Val(Ancho.Text)) * 0.2734) * Val(Precio_Pub_Cta.Text)) * Val(Largo.Text)
        Else
            Precio_ML_Cta.Text = (((Val(Espesor.Text) * Val(Ancho.Text)) * 0.2734) * Val(Precio_Recargo_Pub_Cta.Text)) * Val(Largo.Text)
        End If
    End If
    
    Precio_ML_Cta.Text = Formateado(Str$(Val(Precio_ML_Cta.Text)), 3, 8, " ", False)
    
    Aderir_Recargos
End Sub

Private Sub Precio_Pub_Ctdo_GotFocus()
    If Val(Margen_Util_Pub.Text) > 0 And Val(Precio_Compra.Text) > 0 Then Precio_Pub_Ctdo.Text = Formateado(Str$(Val((((Val(Precio_Compra.Text) * Val(Margen_Util_Pub.Text)) / 100) + Val(Precio_Compra.Text)))), 2, 9, " ", False)
    Precio_Pub_Ctdo.Text = Trim$(Precio_Pub_Ctdo.Text)
    Precio_Pub_Ctdo.SelStart = 0
    Precio_Pub_Ctdo.SelText = ""
    Precio_Pub_Ctdo.SelLength = Len(Precio_Pub_Ctdo.Text)
End Sub

Private Sub Precio_Pub_Ctdo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Pub_Ctdo_LostFocus()
    Precio_Pub_Ctdo.Text = Formateado(Str$(Val(Precio_Pub_Ctdo.Text)), 2, 9, " ", False)
    
    If Val(Mid$(Presentacion.Text, 1, 2)) = 5 And Val(Precio_Pub_Ctdo.Text) > 0 Then
        If Val(Precio_Recargo_Pub_Ctdo.Text) = 0 Then
            Precio_ML_Ctdo.Text = (((Val(Espesor.Text) * Val(Ancho.Text)) * 0.2734) * Val(Precio_Pub_Ctdo.Text)) * Val(Largo.Text)
        Else
            Precio_ML_Ctdo.Text = (((Val(Espesor.Text) * Val(Ancho.Text)) * 0.2734) * Val(Precio_Recargo_Pub_Ctdo.Text)) * Val(Largo.Text)
        End If
    End If
    Precio_ML_Ctdo.Text = Formateado(Str$(Val(Precio_ML_Ctdo.Text)), 3, 8, " ", False)
    
    Aderir_Recargos
End Sub

Private Sub Precio_Rev_Ctdo_GotFocus()
    If Val(Margen_Util_Rev.Text) > 0 And Val(Precio_Compra.Text) > 0 Then Precio_Rev_Ctdo.Text = Formateado(Str$(Val((((Val(Precio_Compra.Text) * Val(Margen_Util_Rev.Text)) / 100) + Val(Precio_Compra.Text)))), 2, 9, " ", False)
    Precio_Rev_Ctdo.Text = Trim$(Precio_Rev_Ctdo.Text)
    Precio_Rev_Ctdo.SelStart = 0
    Precio_Rev_Ctdo.SelText = ""
    Precio_Rev_Ctdo.SelLength = Len(Precio_Rev_Ctdo.Text)
End Sub

Private Sub Precio_Rev_Ctdo__KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Rev_Ctdo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Rev_Ctdo_LostFocus()
    Precio_Rev_Ctdo.Text = Formateado(Str$(Val(Precio_Rev_Ctdo.Text)), 2, 9, " ", False)
    Aderir_Recargos
End Sub

Private Sub Precio_Rev_Cta_GotFocus()
    Precio_Rev_Cta.Text = Precio_Rev_Ctdo.Text
    Precio_Rev_Cta.Text = Trim$(Precio_Rev_Cta.Text)
    Precio_Rev_Cta.SelStart = 0
    Precio_Rev_Cta.SelText = ""
    Precio_Rev_Cta.SelLength = Len(Precio_Rev_Cta.Text)
End Sub

Private Sub Precio_Rev_Cta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Rev_Cta_LostFocus()
    Precio_Rev_Cta.Text = Formateado(Str$(Val(Precio_Rev_Cta.Text)), 2, 9, " ", False)
    Aderir_Recargos
End Sub

Private Sub Presentacion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Presentacion_LostFocus()
    Habilitar_Medidas
End Sub

Private Sub Habilitar_Medidas()
    If Val(Mid$(Presentacion.Text, 1, 1)) = 3 Or Val(Mid$(Presentacion.Text, 1, 1)) = 4 Or Val(Mid$(Presentacion.Text, 1, 1)) = 5 Or Val(Mid$(Presentacion_Rev.Text, 1, 1)) = 3 Or Val(Mid$(Presentacion_Rev.Text, 1, 1)) = 4 Or Val(Mid$(Presentacion_Rev.Text, 1, 1)) = 5 Then
        Espesor.Enabled = True
        Ancho.Enabled = True
        Largo.Enabled = True
        
        Ancho_Menor.BackColor = vbWhite
        Recargo_Ancho_Menor.BackColor = vbWhite
        Largo_Menor.BackColor = vbWhite
        Recargo_Largo_Menor.BackColor = vbWhite
        Ancho_Mayor.BackColor = vbWhite
        Recargo_Ancho_Mayor.BackColor = vbWhite
        Largo_Mayor.BackColor = vbWhite
        Recargo_Largo_Mayor.BackColor = vbWhite
        
        
        Espesor.BackColor = vbWhite
        Ancho.BackColor = vbWhite
        Largo.BackColor = vbWhite
        
        Marco_Recargos.Enabled = True
    Else
        Espesor.Text = ""
        Ancho.Text = ""
        Largo.Text = ""
        
        Espesor.Enabled = False
        Ancho.Enabled = False
        Largo.Enabled = False
        
        Ancho_Menor.BackColor = 14737632
        Recargo_Ancho_Menor.BackColor = 14737632
        Largo_Menor.BackColor = 14737632
        Recargo_Largo_Menor.BackColor = 14737632
        Ancho_Mayor.BackColor = 14737632
        Recargo_Ancho_Mayor.BackColor = 14737632
        Largo_Mayor.BackColor = 14737632
        Recargo_Largo_Mayor.BackColor = 14737632

        
        Espesor.BackColor = 14737632
        Ancho.BackColor = 14737632
        Largo.BackColor = 14737632
        
        Marco_Recargos.Enabled = False
    End If
End Sub

Private Sub Presentacion_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Habilitar_Medidas: SendKeys "{TAB}"
End Sub

Private Sub Proveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Ancho_Mayor_GotFocus()
    Recargo_Ancho_Mayor.Text = Trim$(Recargo_Ancho_Mayor.Text)
    Recargo_Ancho_Mayor.SelStart = 0
    Recargo_Ancho_Mayor.SelText = ""
    Recargo_Ancho_Mayor.SelLength = Len(Recargo_Ancho_Mayor.Text)
End Sub

Private Sub Recargo_Ancho_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Ancho_Mayor_LostFocus()
    Recargo_Ancho_Mayor.Text = Formateado(Str$(Val(Recargo_Ancho_Mayor.Text)), 2, 6, " ", False)
    Aderir_Recargos
End Sub

Private Sub Recargo_Ancho_Menor_GotFocus()
    Recargo_Ancho_Menor.Text = Trim$(Recargo_Ancho_Menor.Text)
    Recargo_Ancho_Menor.SelStart = 0
    Recargo_Ancho_Menor.SelText = ""
    Recargo_Ancho_Menor.SelLength = Len(Recargo_Ancho_Menor.Text)
End Sub

Private Sub Recargo_Ancho_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Ancho_Menor_LostFocus()
    Recargo_Ancho_Menor.Text = Formateado(Str$(Val(Recargo_Ancho_Menor.Text)), 2, 6, " ", False)
End Sub

Private Sub Recargo_Largo_Mayor_GotFocus()
    Recargo_Largo_Mayor.Text = Trim$(Recargo_Largo_Mayor.Text)
    Recargo_Largo_Mayor.SelStart = 0
    Recargo_Largo_Mayor.SelText = ""
    Recargo_Largo_Mayor.SelLength = Len(Recargo_Largo_Mayor.Text)
End Sub

Private Sub Recargo_Largo_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Largo_Mayor_LostFocus()
    Recargo_Largo_Mayor.Text = Formateado(Str$(Val(Recargo_Largo_Mayor.Text)), 2, 6, " ", False)
    Aderir_Recargos
End Sub

Private Sub Recargo_Largo_Menor_GotFocus()
    Recargo_Largo_Menor.Text = Trim$(Recargo_Largo_Menor.Text)
    Recargo_Largo_Menor.SelStart = 0
    Recargo_Largo_Menor.SelText = ""
    Recargo_Largo_Menor.SelLength = Len(Recargo_Largo_Menor.Text)
End Sub

Private Sub Recargo_Largo_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Largo_Menor_LostFocus()
    Recargo_Largo_Menor.Text = Formateado(Str$(Val(Recargo_Largo_Menor.Text)), 2, 6, " ", False)
    Aderir_Recargos
End Sub

Private Sub Salir_Click()
    If Articulo.Text = "" And Tipo.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
        Articulo.Text = ""
        Tipo.SetFocus
    End If
End Sub

Private Sub Stock_Min_GotFocus()
    Stock_Min.Text = Trim$(Stock_Min.Text)
    Stock_Min.SelStart = 0
    Stock_Min.SelText = ""
    Stock_Min.SelLength = Len(Stock_Min.Text)
End Sub

Private Sub Stock_Min_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Stock_Min_LostFocus()
    Stock_Min.Text = Formateado(Str$(Val(Stock_Min.Text)), 2, 10, " ", False)
End Sub

Private Sub Tipo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: If Trim$(Tipo.Text) <> "" Then SendKeys "{TAB}"
End Sub

Private Sub Tipo_LostFocus()
    If Val(Mid$(Tipo.Text, 1, 5)) > 0 Then
        Qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & Trim$(Str$(Val(Tipo.Text)))
        Set Rs = Db.OpenRecordset(Qy)
        
        If Not Rs.EOF Then
            Tipo.Text = Trim$(Rs.Fields("Denominacion")) + Space$(31 - Len(Trim$(Rs.Fields("Denominacion")))) & Trim$(Rs.Fields("Id_Rubro")) + Space$(5 - Len(Trim$(Rs.Fields("Id_Rubro")))) & " " & Formateado(Str$(Val(Rs.Fields("Margen_Util_Pub"))), 2, 9, " ", False) & " " & Formateado(Str$(Val(Rs.Fields("Margen_Util_Rev"))), 2, 9, " ", False)
        Else
            Tipo.Text = ""
        End If
    End If
    
    If Val(Margen_Util_Rev.Text) = 0 Then
        Margen_Util_Pub.Text = Formateado(Str$(Val(Mid$(Tipo.Text, 38, 9))), 2, 6, " ", False)
        Margen_Util_Rev.Text = Formateado(Str$(Val(Mid$(Tipo.Text, 49, 9))), 2, 6, " ", False)
    End If
End Sub

Private Sub Aderir_Recargos()
    Dim Recargo As Boolean
    
    Recargo = False
    
    If Trim$(Str$(Val(Ancho.Text))) < Trim$(Str$(Val(Ancho_Menor.Text))) Then
        Recargo = True
        Precio_Recargo_Rev_Ctdo.Text = ((Val(Precio_Rev_Ctdo.Text) * Val(Recargo_Ancho_Menor.Text)) / 100) + Val(Precio_Rev_Ctdo.Text)
        Precio_Recargo_Rev_Cta.Text = ((Val(Precio_Rev_Cta.Text) * Val(Recargo_Ancho_Menor.Text)) / 100) + Val(Precio_Rev_Cta.Text)
        Precio_Recargo_Pub_Ctdo.Text = ((Val(Precio_Pub_Ctdo.Text) * Val(Recargo_Ancho_Menor.Text)) / 100) + Val(Precio_Pub_Ctdo.Text)
        Precio_Recargo_Pub_Cta.Text = ((Val(Precio_Rev_Cta.Text) * Val(Recargo_Ancho_Menor.Text)) / 100) + Val(Precio_Pub_Cta.Text)
    ElseIf Trim$(Str$(Val(Ancho.Text))) > Trim$(Str$(Val(Ancho_Mayor))) Then
        If Recargo = False Then
            Recargo = True
            Precio_Recargo_Rev_Ctdo.Text = ((Val(Precio_Rev_Ctdo.Text) * Val(Recargo_Ancho_Mayor.Text)) / 100) + Val(Precio_Rev_Ctdo.Text)
            Precio_Recargo_Rev_Cta.Text = ((Val(Precio_Rev_Cta.Text) * Val(Recargo_Ancho_Mayor.Text)) / 100) + Val(Precio_Rev_Cta.Text)
            Precio_Recargo_Pub_Ctdo.Text = ((Val(Precio_Pub_Ctdo.Text) * Val(Recargo_Ancho_Mayor.Text)) / 100) + Val(Precio_Pub_Ctdo.Text)
            Precio_Recargo_Pub_Cta.Text = ((Val(Precio_Rev_Cta.Text) * Val(Recargo_Ancho_Mayor.Text)) / 100) + Val(Precio_Pub_Cta.Text)
        Else
            Precio_Recargo_Rev_Ctdo.Text = ((Val(Precio_Recargo_Rev_Ctdo.Text) * Val(Recargo_Ancho_Mayor.Text)) / 100) + Val(Precio_Recargo_Rev_Ctdo.Text)
            Precio_Recargo_Rev_Cta.Text = ((Val(Precio_Recargo_Rev_Ctdo.Text) * Val(Recargo_Ancho_Mayor.Text)) / 100) + Val(Precio_Recargo_Rev_Cta.Text)
            Precio_Recargo_Pub_Ctdo.Text = ((Val(Precio_Recargo_Rev_Ctdo.Text) * Val(Recargo_Ancho_Mayor.Text)) / 100) + Val(Precio_Recargo_Rev_Ctdo.Text)
            Precio_Recargo_Pub_Cta.Text = ((Val(Precio_Recargo_Rev_Ctdo.Text) * Val(Recargo_Ancho_Mayor.Text)) / 100) + Val(Precio_Recargo_Pub_Cta.Text)
        End If
    End If
    
    If Trim$(Str$(Val(Largo.Text))) < Trim$(Str$(Val(Largo_Menor.Text))) Then
        If Recargo = False Then
            Recargo = True
            Precio_Recargo_Rev_Ctdo.Text = ((Val(Precio_Rev_Ctdo.Text) * Val(Recargo_Largo_Menor.Text)) / 100) + Val(Precio_Rev_Ctdo.Text)
            Precio_Recargo_Rev_Cta.Text = ((Val(Precio_Rev_Cta.Text) * Val(Recargo_Largo_Menor.Text)) / 100) + Val(Precio_Rev_Cta.Text)
            Precio_Recargo_Pub_Ctdo.Text = ((Val(Precio_Pub_Ctdo.Text) * Val(Recargo_Largo_Menor.Text)) / 100) + Val(Precio_Pub_Ctdo.Text)
            Precio_Recargo_Pub_Cta.Text = ((Val(Precio_Pub_Cta.Text) * Val(Recargo_Largo_Menor.Text)) / 100) + Val(Precio_Pub_Cta.Text)
        Else
            Precio_Recargo_Rev_Ctdo.Text = ((Val(Precio_Recargo_Rev_Ctdo.Text) * Val(Recargo_Largo_Menor.Text)) / 100) + Val(Precio_Recargo_Rev_Ctdo.Text)
            Precio_Recargo_Rev_Cta.Text = ((Val(Precio_Recargo_Rev_Cta.Text) * Val(Recargo_Largo_Menor.Text)) / 100) + Val(Precio_Recargo_Rev_Cta.Text)
            Precio_Recargo_Pub_Ctdo.Text = ((Val(Precio_Recargo_Pub_Ctdo.Text) * Val(Recargo_Largo_Menor.Text)) / 100) + Val(Precio_Recargo_Pub_Ctdo.Text)
            Precio_Recargo_Pub_Cta.Text = ((Val(Precio_Recargo_Pub_Cta.Text) * Val(Recargo_Largo_Menor.Text)) / 100) + Val(Precio_Recargo_Pub_Cta.Text)
        End If
    ElseIf Trim$(Str$(Val(Largo.Text))) > Trim$(Str$(Val(Largo_Mayor.Text))) Then
        If Recargo = False Then
            Recargo = True
            Precio_Recargo_Rev_Ctdo.Text = ((Val(Precio_Rev_Ctdo.Text) * Val(Recargo_Largo_Mayor.Text)) / 100) + Val(Precio_Rev_Ctdo.Text)
            Precio_Recargo_Rev_Cta.Text = ((Val(Precio_Rev_Cta.Text) * Val(Recargo_Largo_Mayor.Text)) / 100) + Val(Precio_Rev_Cta.Text)
            Precio_Recargo_Pub_Ctdo.Text = ((Val(Precio_Pub_Ctdo.Text) * Val(Recargo_Largo_Mayor.Text)) / 100) + Val(Precio_Pub_Ctdo.Text)
            Precio_Recargo_Pub_Cta.Text = ((Val(Precio_Pub_Cta.Text) * Val(Recargo_Largo_Mayor.Text)) / 100) + Val(Precio_Pub_Cta.Text)
        Else
            Precio_Recargo_Rev_Ctdo.Text = ((Val(Precio_Recargo_Rev_Ctdo.Text) * Val(Recargo_Largo_Mayor.Text)) / 100) + Val(Precio_Recargo_Rev_Ctdo.Text)
            Precio_Recargo_Rev_Cta.Text = ((Val(Precio_Recargo_Rev_Cta.Text) * Val(Recargo_Largo_Mayor.Text)) / 100) + Val(Precio_Recargo_Rev_Cta.Text)
            Precio_Recargo_Pub_Ctdo.Text = ((Val(Precio_Recargo_Pub_Ctdo.Text) * Val(Recargo_Largo_Mayor.Text)) / 100) + Val(Precio_Recargo_Pub_Ctdo.Text)
            Precio_Recargo_Pub_Cta.Text = ((Val(Precio_Recargo_Pub_Cta.Text) * Val(Recargo_Largo_Mayor.Text)) / 100) + Val(Precio_Recargo_Pub_Cta.Text)
        End If
        
    End If
    
    If Recargo = False Then
        Precio_Recargo_Rev_Cta.Text = Val(Precio_Rev_Cta.Text)
        Precio_Recargo_Rev_Ctdo.Text = Val(Precio_Rev_Ctdo.Text)
        Precio_Recargo_Pub_Cta.Text = Val(Precio_Pub_Cta.Text)
        Precio_Recargo_Pub_Ctdo.Text = Val(Precio_Pub_Ctdo.Text)
    End If
        
    Precio_Recargo_Rev_Ctdo.Text = Formateado(Str$(Val(Precio_Recargo_Rev_Ctdo.Text)), 2, 8, " ", False)
    Precio_Recargo_Rev_Cta.Text = Formateado(Str$(Val(Precio_Recargo_Rev_Cta.Text)), 2, 8, " ", False)
    Precio_Recargo_Pub_Ctdo.Text = Formateado(Str$(Val(Precio_Recargo_Pub_Ctdo.Text)), 2, 8, " ", False)
    Precio_Recargo_Pub_Cta.Text = Formateado(Str$(Val(Precio_Recargo_Pub_Cta.Text)), 2, 8, " ", False)
End Sub
