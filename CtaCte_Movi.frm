VERSION 5.00
Begin VB.Form CtaCte_Movi 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Movimientos de Cuenta Corriente.-"
   ClientHeight    =   5295
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6960
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5295
   ScaleWidth      =   6960
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame2 
      Height          =   4335
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   6975
      Begin VB.OptionButton Proveedor 
         Caption         =   "Proveedor."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   960
         TabIndex        =   1
         Top             =   720
         Width           =   2175
      End
      Begin VB.OptionButton Cliente 
         Caption         =   "Cliente."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   960
         TabIndex        =   0
         Top             =   480
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.TextBox Vto 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5520
         TabIndex        =   7
         Top             =   2760
         Width           =   1335
      End
      Begin VB.TextBox Haber 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   10
         Top             =   3840
         Width           =   1335
      End
      Begin VB.TextBox Debe 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   9
         Top             =   3480
         Width           =   1335
      End
      Begin VB.TextBox Detalle 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   8
         Top             =   3120
         Width           =   4815
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   6
         Top             =   2760
         Width           =   1335
      End
      Begin VB.TextBox Saldo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   5
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Direccion 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   4
         Top             =   1680
         Width           =   4815
      End
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         TabIndex        =   16
         Top             =   1320
         Visible         =   0   'False
         Width           =   4815
      End
      Begin VB.TextBox Nombre 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         TabIndex        =   3
         Top             =   1320
         Width           =   4815
      End
      Begin VB.TextBox Cuenta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   2
         Top             =   1320
         Width           =   855
      End
      Begin VB.CommandButton Buscar_Cuentas 
         Caption         =   "&Cuenta:"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label8 
         Caption         =   "A Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Haber:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   3840
         Width           =   1815
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Debe:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   3480
         Width           =   1815
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Detalle:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   3120
         Width           =   1815
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Vencimiento:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3480
         TabIndex        =   20
         Top             =   2760
         Width           =   1935
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   2760
         Width           =   1815
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   2040
         Width           =   1815
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Direcci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   1680
         Width           =   1815
      End
   End
   Begin VB.Frame Frame1 
      Height          =   975
      Left            =   0
      TabIndex        =   11
      Top             =   4320
      Width           =   6975
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   5760
         Picture         =   "CtaCte_Movi.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   14
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "CtaCte_Movi.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "CtaCte_Movi"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Buscar_Cuentas_Click()
    Cuentas_Encontradas.Visible = True
    Cuentas_Encontradas.Clear
    MousePointer = 11
    qy = "SELECT * FROM " & IIf(Cliente.Value = True, "Cliente", "Proveedor") & " ORDER BY Nombre"
    AbreRs
    
    While Not Rs.EOF
        Cuentas_Encontradas.AddItem Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields(0))
        Rs.MoveNext
    Wend
    
    MousePointer = 0
    Cuentas_Encontradas.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_GotFocus()
    Cuenta.Text = Trim(Cuenta.Text)
    Cuenta.SelStart = 0
    Cuenta.SelText = ""
    Cuenta.SelLength = Len(Cuenta.Text)
End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_LostFocus()
    If Val(Cuenta.Text) > 0 Then
        Leer_Cuenta
    End If
End Sub

Private Sub Leer_Cuenta()
    If Val(Cuenta.Text) > 0 Then
        qy = "SELECT * FROM " & IIf(Cliente.Value = True, "Cliente", "Proveedor") & " WHERE " & IIf(Cliente.Value = True, "Id_Cliente", "Id_Proveedor") & " = " & Trim(Str(Val(Cuenta.Text)))
        AbreRs
        
        If Rs.EOF Then
            Borrar_Campo
            MsgBox "La Cuenta es inexistente...", vbCritical, "Atenci�n.!"
            Cuenta.SetFocus
        Else
            Nombre.Text = Trim(Rs.Fields("Nombre"))
            Direccion.Text = Trim(Rs.Fields("Domicilio")) & " " & Trim(Rs.Fields("Numero"))
            
            qy = "SELECT SUM(Debe - Haber) FROM " & IIf(Cliente.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " WHERE Id_Cuenta = " & Trim(Str(Val(Cuenta.Text))) & " "
            qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
            AbreRs
            
            If Not IsNull(Rs.Fields(0)) = True Then Saldo.Text = Val(Rs.Fields(0))
            
            Saldo.Text = Formateado(Str(Val(Saldo.Text)), 2, 10, " ", False)
        End If
    Else
        Cuenta.Text = ""
        Cuenta.SetFocus
    End If
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuenta.SetFocus
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Cuenta.Text = Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 31)
    Cuentas_Encontradas.Visible = False
    Cuenta_LostFocus
End Sub

Private Sub Debe_Change()
    If (Val(Debe.Text) > 0 Or Val(Haber.Text) > 0) And Val(Cuenta.Text) > 0 And Val(Fecha.Text) > 0 Then
        Grabar.Enabled = True
    Else
        Grabar.Enabled = False
    End If
End Sub

Private Sub Debe_GotFocus()
    Debe.Text = Trim(Debe.Text)
    Debe.SelStart = 0
    Debe.SelText = ""
    Debe.SelLength = Len(Debe.Text)
End Sub

Private Sub Debe_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Debe_LostFocus()
    Debe.Text = Formateado(Str(Val(Debe.Text)), 2, 10, " ", False)
End Sub

Private Sub Detalle_GotFocus()
    Detalle.SelStart = 0
    Detalle.SelText = ""
    Detalle.SelLength = Len(Detalle.Text)
End Sub

Private Sub Detalle_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Detalle_LostFocus()
    Detalle.Text = FiltroCaracter(Detalle.Text)
End Sub

Private Sub Fecha_GotFocus()
    If Val(Fecha.Text) = 0 Then Fecha.Text = Fecha_Fiscal
    
    Fecha.SelStart = 0
    Fecha.SelText = ""
    Fecha.SelLength = Len(Fecha.Text)
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 3.5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Menu.Estado.Panels(2).Text = "Actualizaci�n de Movimientos de Cta. Cte."
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Val(Debe.Text) > 0 Or Val(Haber.Text) > 0 And (Val(Fecha.Text) > 0) Then
        Dim Contador As Long
        
        qy = "SELECT MAX(SUBSTRING(Comprobante, 6, 10)) FROM " & IIf(Cliente.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " "
        qy = qy & "WHERE SUBSTRING(Comprobante, 1, 2) = 'AA' "
        qy = qy & "AND SUBSTRING(Comprobante, 4, 1) = 'X' "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        AbreRs
        
        Contador = 1
        If Not IsNull(Rs.Fields(0)) = True Then Contador = Val(Rs.Fields(0)) + 1
            
        qy = "INSERT INTO " & IIf(Cliente.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Cuenta.Text))) & " "
        qy = qy & ", '" & Trim(Fecha.Text) & " " & Trim(Hora_Fiscal) & "'"
        qy = qy & ", 'AA X " & Formateado(Str(Val(cCentro_Emisor)), 0, 4, "0", False) & "-" & Formateado(Str(Val(Contador)), 0, 10, "0", False) & "'"
        qy = qy & ", '" & IIf(Trim(Detalle.Text) = "", " ", Trim(Detalle.Text)) & "'"
        qy = qy & ", '" & IIf(Val(Vto.Text) = 0, Trim(Fecha.Text), Trim(Vto.Text)) & "'"
        qy = qy & ", " & Trim(Str(Val(Debe.Text)))
        qy = qy & ", " & Trim(Str(Val(Haber.Text))) & ")"
        Db.Execute (qy)
        
        Salir_Click
    Else
        MsgBox "Se han encontrado errores en los datos ingresados, verfique e ingrese nuevamente.", vbCritical, "Atenci�n.!"
        Fecha.SetFocus
    End If
End Sub

Private Sub Haber_Change()
    If (Val(Debe.Text) > 0 Or Val(Haber.Text) > 0) And Val(Cuenta.Text) > 0 And Val(Fecha.Text) > 0 Then
        Grabar.Enabled = True
    Else
        Grabar.Enabled = False
    End If
End Sub

Private Sub Haber_GotFocus()
    Haber.Text = Trim(Haber.Text)
    
    Haber.SelStart = 0
    Haber.SelText = ""
    Haber.SelLength = Len(Haber.Text)
End Sub

Private Sub Haber_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Haber_LostFocus()
    Haber.Text = Formateado(Str(Val(Haber.Text)), 2, 10, " ", False)
End Sub

Private Sub Proveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If Cliente.Value = True And Cuenta.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Cliente.Value = True
    Cuenta.Text = ""
    Nombre.Text = ""
    Direccion.Text = ""
    Saldo.Text = ""
    
    Fecha.Text = ""
    Vto.Text = ""
    Detalle.Text = ""
    Debe.Text = ""
    Haber.Text = ""
    
    Cliente.SetFocus
End Sub

Private Sub Vto_GotFocus()
    If Val(Fecha.Text) > 0 Then Vto.Text = DateAdd("d", 30, Fecha.Text)
    
    Vto.SelStart = 0
    Vto.SelText = ""
    Vto.SelLength = Len(Vto.Text)
End Sub

Private Sub Vto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Vto_LostFocus()
    Vto.Text = ValidarFecha(Vto.Text)
End Sub
