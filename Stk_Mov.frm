VERSION 5.00
Begin VB.Form Stk_Mov 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Movimientos de Artνculos.-"
   ClientHeight    =   6135
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9255
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6135
   ScaleWidth      =   9255
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Captura 
      Height          =   1935
      Left            =   1560
      TabIndex        =   16
      Top             =   1920
      Visible         =   0   'False
      Width           =   6255
      Begin VB.CommandButton Cancela 
         Caption         =   "C&ancela"
         Height          =   375
         Left            =   5040
         TabIndex        =   26
         Top             =   1440
         Width           =   975
      End
      Begin VB.CommandButton Confirma 
         Caption         =   "&Confirma"
         Height          =   375
         Left            =   3960
         TabIndex        =   25
         Top             =   1440
         Width           =   975
      End
      Begin VB.TextBox Ingreso 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         MaxLength       =   10
         TabIndex        =   24
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox Egreso 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         MaxLength       =   10
         TabIndex        =   23
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox Detalle 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         MaxLength       =   49
         TabIndex        =   22
         Top             =   600
         Width           =   5175
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         MaxLength       =   10
         TabIndex        =   21
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Ingreso:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   1320
         Width           =   735
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Egreso:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   960
         Width           =   735
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Detalle:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Marco_Articulo 
      Height          =   735
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   9255
      Begin VB.TextBox Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   4
         TabIndex        =   0
         Top             =   240
         Width           =   615
      End
      Begin VB.ComboBox Articulos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2760
         TabIndex        =   27
         Top             =   240
         Visible         =   0   'False
         Width           =   6375
      End
      Begin VB.TextBox Descripcion 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2760
         TabIndex        =   7
         Top             =   240
         Width           =   6375
      End
      Begin VB.TextBox Articulo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         MaxLength       =   5
         TabIndex        =   1
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton Buscar_Articulos 
         Caption         =   "&Artνculo:"
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Lista 
      Enabled         =   0   'False
      Height          =   4455
      Left            =   0
      TabIndex        =   4
      Top             =   720
      Width           =   9255
      Begin VB.CommandButton Command5 
         Caption         =   "Ingresos"
         Height          =   255
         Left            =   7800
         TabIndex        =   13
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Egresos"
         Height          =   255
         Left            =   6480
         TabIndex        =   12
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Detalle"
         Height          =   255
         Left            =   1320
         TabIndex        =   11
         Top             =   120
         Width           =   5175
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Fecha"
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   120
         Width           =   1215
      End
      Begin VB.ListBox List2 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   9
         Top             =   4080
         Width           =   9015
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3660
         Left            =   120
         TabIndex        =   8
         Top             =   360
         Width           =   9015
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   5
      Top             =   5160
      Width           =   9255
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   8040
         Picture         =   "Stk_Mov.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   6720
         Picture         =   "Stk_Mov.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Imprimir"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Ing 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Stk_Mov.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Ingresar Nuevos movimientos.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Stk_Mov"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Articulo_GotFocus()
    Articulo.SelStart = 0
    Articulo.SelText = ""
    Articulo.SelLength = Len(Articulo.Text)
End Sub

Private Sub Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Articulo_LostFocus()
    If Val(Articulo.Text) > 0 And Val(Rubro.Text) > 0 Then
        Leer_Articulo
    End If
End Sub

Private Sub Leer_Articulo()
    If Val(Articulo.Text) > 0 Then
        
        qy = "SELECT * FROM Articulo WHERE Articulo.Id_Articulo = " & Trim(Str(Val(Articulo.Text)))
        qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Rubro.Text)))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "El Artνculo es inexistente...", vbInformation, "Atenciσn.!"
            Borrar_Campo
        Else
            If Val(Permiso_Cons) = 1 Then
                Descripcion.Text = Rs.Fields("Descripcion")
                Cargar_Lista
            Else
                MsgBox "Usted no estα Autorizado para estos procesos.", vbCritical, "Atenciσn.!"
                Borrar_Campo
            End If
        End If
    Else
        Articulo.Text = ""
        Articulo.SetFocus
    End If
End Sub

Private Sub Cargar_Lista()
    Dim Total_Ing  As String
    Dim Total_Eg   As String

    qy = "SELECT * FROM Movimiento_Articulo "
    qy = qy & "WHERE Movimiento_Articulo.Articulo = " & Trim(Str(Val(Articulo.Text))) & " "
    qy = qy & "AND Rubro = " & Trim(Str(Val(Rubro.Text))) & " "
    qy = qy & "ORDER BY Fecha "
    AbreRs
    
    List1.Clear
    List2.Clear
    
    While Not Rs.EOF
        Txt = Format(Rs.Fields("Fecha"), "dd/mm/yyyy") & " "
        Txt = Txt & Trim(Rs.Fields("Detalle")) + Space(49 - Len(Trim(Rs.Fields("Detalle")))) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Egreso"))), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Ingreso"))), 2, 10, " ", False)
        Total_Ing = Val(Total_Ing) + Val(Rs.Fields("Ingreso"))
        Total_Eg = Val(Total_Eg) + Val(Rs.Fields("Egreso"))
        
        List1.AddItem Txt
        Rs.MoveNext
    Wend
    
    Txt = "TOTALES:" + Space(54) + Formateado(Str(Val(Total_Eg)), 2, 10, " ", False) & " " & Formateado(Str(Val(Total_Ing)), 2, 10, " ", False)
    List2.AddItem Txt
    
    Marco_Lista.Enabled = True
    Marco_Articulo.Enabled = False
    Ing.Enabled = True
    Imprimir.Enabled = True
    
    List1.SetFocus
End Sub

Private Sub Borrar_Campo()
    Articulo.Text = ""
    Rubro.Text = ""
    Descripcion.Text = ""
    List1.Clear
    List2.Clear
    Marco_Lista.Enabled = False
    Ing.Enabled = False
    Imprimir.Enabled = False
    
    Marco_Articulo.Enabled = True
    Rubro.SetFocus
End Sub

Private Sub Articulos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo.SetFocus
End Sub

Private Sub Articulos_Encontrados_LostFocus()
    Rubro.Text = Trim(Mid(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 32, 4))
    Articulo.Text = Mid(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 37)
    Articulos_Encontrados.Visible = False
    Leer_Articulo
End Sub

Private Sub Buscar_Articulos_Click()
    Articulos_Encontrados.Visible = True
    MousePointer = 11
    If Articulos_Encontrados.ListCount = 0 Then
        qy = "SELECT * FROM Articulo ORDER BY Descripcion"
        AbreRs
        
        While Not Rs.EOF
            Articulos_Encontrados.AddItem Trim(Rs.Fields("Descripcion")) + Space(30 - Len(Trim(Rs.Fields("Descripcion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 4, " ", False) & " " & Trim(Rs.Fields("Id_Articulo"))
            Rs.MoveNext
        Wend
    End If
    MousePointer = 0
    Articulos_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cancela_Click()
    If Detalle.Text = "" And Egreso.Text = "" And Ingreso.Text = "" Then
        Marco_Captura.Visible = False
        Marco_Lista.Enabled = True
        Botonera.Enabled = True
        Salir.Cancel = True
        
        List1.SetFocus
    Else
        Borrar_Campo_Captura
    End If
End Sub

Private Sub Confirma_Click()
    If Val(Egreso.Text) > 0 Or Val(Ingreso.Text) > 0 And Val(Fecha.Text) > 0 Then
        Dim Id_Mov As String
        qy = "SELECT MAX(Id_Movimiento) FROM Movimiento_Articulo"
        AbreRs
    
        Id_Mov = 1
        If Not Rs.Fields(0) = "" Then Id_Mov = Val(Rs.Fields(0)) + 1
    
        qy = "INSERT INTO Movimiento_Articulo VALUES('"
        qy = qy & Trim(Str(Val(Id_Mov))) & "' "
        qy = qy & ", " & Trim(Str(Val(Rubro.Text)))
        qy = qy & ", '" & Trim(Str(Val(Articulo.Text))) & "'"
        qy = qy & ", '" & Trim(Fecha.Text) & "'"
        qy = qy & ", '" & Trim(Detalle.Text) & "'"
        qy = qy & ", '" & Trim(Str(Val(Ingreso.Text))) & "'"
        qy = qy & ", '" & Trim(Str(Val(Egreso.Text))) & "')"
        Db.Execute (qy)
    
        If Val(Egreso.Text) > 0 Then
            qy = "UPDATE Articulo SET "
            qy = qy & "Existencia = Existencia - " & Val(Egreso.Text) & " "
            qy = qy & "WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " AND Id_Rubro = " & Trim(Str(Val(Rubro.Text)))
            Db.Execute (qy)
        ElseIf Val(Ingreso.Text) > 0 Then
            qy = "UPDATE Articulo SET "
            qy = qy & "Existencia = Existencia + " & Val(Ingreso.Text) & " "
            qy = qy & "WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " AND Id_Rubro = " & Trim(Str(Val(Rubro.Text)))
            Db.Execute (qy)
        End If
        
        Cargar_Lista
        Borrar_Campo_Captura
    Else
        Ingreso.SetFocus
    End If
End Sub

Private Sub Borrar_Campo_Captura()
    Fecha.Text = ""
    Detalle.Text = ""
    Ingreso.Text = ""
    Egreso.Text = ""
    
    Fecha.SetFocus
End Sub

Private Sub Detalle_GotFocus()
    Detalle.SelStart = 0
    Detalle.SelText = ""
    Detalle.SelLength = Len(Detalle.Text)
End Sub

Private Sub Detalle_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Detalle_LostFocus()
    Detalle.Text = FiltroCaracter(Detalle.Text)
    Detalle.Text = UCase(Detalle.Text)
End Sub

Private Sub Egreso_GotFocus()
    Egreso.Text = Trim(Egreso.Text)
    Egreso.SelStart = 0
    Egreso.SelText = ""
    Egreso.SelLength = Len(Egreso.Text)
End Sub

Private Sub Egreso_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Egreso_LostFocus()
    Egreso.Text = Formateado(Str(Val(Egreso.Text)), 2, 10, " ", False)
End Sub

Private Sub Fecha_GotFocus()
    If Fecha.Text = "" Then Fecha.Text = Format(Now, "dd/mm/yyyy")
    
    Fecha.SelStart = 0
    Fecha.SelText = ""
    Fecha.SelLength = Len(Fecha.Text)
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Registro de Movimientos de Artνculos.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Imprimir_Click()
    On Error GoTo Error_Impres
Inicia:

    Dim i As Long
    Dim l As Long
    
    If List1.ListCount > 0 Then
        
        Printer.Font = "Courier New"
        Printer.FontSize = 11
        
        i = 0
        l = 0
        
        Imprimir_Encabezado
        
        For i = 0 To List1.ListCount - 1
            If l <= 50 Then
                Printer.Print " " & Mid(List1.List(i), 1, 90)
                l = l + 1
            Else
                l = 0
                Printer.NewPage
                Imprimir_Encabezado
            End If
        Next
        
        Printer.Print " "
        Printer.Print " " & List2.List(0)
        Printer.EndDoc
    End If
    
    Salir.SetFocus
    Exit Sub
    
Error_Impres:
    If Err.Number = 482 Then
        If MsgBox("Error en la impresora, que desea hacer. ?", vbCritical + vbRetryCancel, "Atenciσn.!") = vbRetry Then
            Resume Inicia
        Else
            Salir.SetFocus
        End If
    End If
End Sub

Private Sub Imprimir_Encabezado()
    Imprimir.Tag = Int(List1.ListCount / 50) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                   Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                Fecha..: " & Format(Fecha_Fiscal, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                          Hora...: " & Format(Hora_Fiscal, "hh.mm"); ""
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " E-Mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print "                                   MOVIMIENTOS ARTΝCULOS"
    Printer.Print
    Printer.Print " ARTΝCULO: " & Formateado(Str(Val(Rubro.Text)), 0, 5, " ", False) & " " & Formateado(Str(Val(Articulo.Text)), 0, 5, " ", False) & " - " & Trim(Descripcion.Text)
    Printer.Print " "
    Printer.Print "       Fecha Detalle                                            Ingresos    Egresos"
    Printer.Print " "
End Sub

Private Sub Ing_Click()
    If Val(Permiso_Alta) = 1 Then
        Marco_Captura.Visible = True
        Marco_Lista.Enabled = False
        Botonera.Enabled = False
        Cancela.Cancel = True
        
        Fecha.SetFocus
    Else
        MsgBox "Usted no estα autorizado para estos procesos.", vbCritical, "Atenciσn.!"
    End If
End Sub

Private Sub Ingreso_GotFocus()
    Ingreso.Text = Trim(Ingreso.Text)
    Ingreso.SelStart = 0
    Ingreso.SelText = ""
    Ingreso.SelLength = Len(Ingreso.Text)
End Sub

Private Sub Ingreso_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ingreso_LostFocus()
    Ingreso.Text = Formateado(Str(Val(Ingreso.Text)), 2, 10, " ", False)
End Sub

Private Sub Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If Articulo.Text = "" And Rubro.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub
