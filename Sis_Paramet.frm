VERSION 5.00
Begin VB.Form Tbl_Paramet 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Parametrizaci�n del Sistema.-"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6735
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6735
   ScaleWidth      =   6735
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Datos 
      Height          =   5775
      Left            =   0
      TabIndex        =   16
      Top             =   0
      Width           =   6735
      Begin VB.TextBox Slogan 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   1
         Top             =   600
         Width           =   5055
      End
      Begin VB.TextBox Empresa 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   0
         Top             =   240
         Width           =   5055
      End
      Begin VB.TextBox Cta_Debito_STA 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   12
         Top             =   4200
         Width           =   5055
      End
      Begin VB.TextBox Puerto 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   1
         TabIndex        =   15
         Top             =   5400
         Width           =   375
      End
      Begin VB.TextBox Cta_Impuestos 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   14
         Top             =   4920
         Width           =   5055
      End
      Begin VB.TextBox Cta_Credito_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   13
         Top             =   4560
         Width           =   5055
      End
      Begin VB.TextBox Cta_Debito_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   11
         Top             =   3840
         Width           =   5055
      End
      Begin VB.TextBox CtaCte 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   10
         Top             =   3480
         Width           =   5055
      End
      Begin VB.TextBox Caja 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         TabIndex        =   9
         Top             =   3120
         Width           =   5055
      End
      Begin VB.TextBox Nc_B 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         MaxLength       =   10
         TabIndex        =   8
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Nc_A 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   10
         TabIndex        =   7
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Fc_B 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         MaxLength       =   10
         TabIndex        =   6
         Top             =   2280
         Width           =   1335
      End
      Begin VB.TextBox Fc_A 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   10
         TabIndex        =   5
         Top             =   2280
         Width           =   1335
      End
      Begin VB.TextBox Sta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   6
         TabIndex        =   4
         Top             =   1800
         Width           =   855
      End
      Begin VB.TextBox Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   6
         TabIndex        =   3
         Top             =   1440
         Width           =   855
      End
      Begin VB.TextBox Emisor 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   4
         TabIndex        =   2
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label16 
         Alignment       =   1  'Right Justify
         Caption         =   "Slogan:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre Empresa:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. D�bito STA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   33
         Top             =   4200
         Width           =   1215
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Caption         =   "Puerto Impresor:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   30
         Top             =   5400
         Width           =   1215
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Impuestos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   4920
         Width           =   1215
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Cr�dito IVA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   4560
         Width           =   1215
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. D�bito IVA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   27
         Top             =   3840
         Width           =   1215
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Cta.Cte.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   3480
         Width           =   1215
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Cta. Caja:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   25
         Top             =   3120
         Width           =   1215
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "�ltima N. Cred. 'A':"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   24
         Top             =   2640
         Width           =   1320
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "�ltima N. Cr�d. 'B':"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   23
         Top             =   2640
         Width           =   1335
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "�ltima FC 'B':"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   22
         Top             =   2280
         Width           =   1335
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "�ltima FC 'A':"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   21
         Top             =   2280
         Width           =   1215
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Alicuota STA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Alicuota IVA:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   19
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Centro Emisior:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   1080
         Width           =   1215
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   17
      Top             =   5760
      Width           =   6735
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   5520
         Picture         =   "Sis_Paramet.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   32
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Height          =   615
         Left            =   4200
         Picture         =   "Sis_Paramet.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   31
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Tbl_Paramet"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Caja_GotFocus()
    Caja.SelStart = 0
    Caja.SelText = ""
    Caja.SelLength = Len(Caja.Text)
End Sub

Private Sub Caja_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cta_Credito_Iva_GotFocus()
    Cta_Credito_Iva.SelStart = 0
    Cta_Credito_Iva.SelText = ""
    Cta_Credito_Iva.SelLength = Len(Cta_Credito_Iva.Text)
End Sub

Private Sub Cta_Credito_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cta_Debito_Iva_GotFocus()
    Cta_Debito_Iva.SelStart = 0
    Cta_Debito_Iva.SelText = ""
    Cta_Debito_Iva.SelLength = Len(Cta_Debito_Iva.Text)
End Sub

Private Sub Cta_Debito_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cta_Debito_STA_GotFocus()
    Cta_Debito_STA.SelStart = 0
    Cta_Debito_STA.SelText = ""
    Cta_Debito_STA.SelLength = Len(Cta_Debito_STA.Text)
End Sub

Private Sub Cta_Debito_STA_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cta_Impuestos_GotFocus()
    Cta_Impuestos.SelStart = 0
    Cta_Impuestos.SelText = ""
    Cta_Impuestos.SelLength = Len(Cta_Impuestos.Text)
End Sub

Private Sub Cta_Impuestos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub CtaCte_GotFocus()
    CtaCte.SelStart = 0
    CtaCte.SelText = ""
    CtaCte.SelLength = Len(CtaCte.Text)
End Sub

Private Sub CtaCte_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Emisor_GotFocus()
    Emisor.SelStart = 0
    Emisor.SelText = ""
    Emisor.SelLength = Len(Emisor.Text)
End Sub

Private Sub Emisor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Emisor_LostFocus()
    Emisor.Text = Formateado(Str$(Val(Emisor.Text)), 0, 4, "0", False)
End Sub

Private Sub Empresa_GotFocus()
    Empresa.Text = Trim$(Empresa.Text)
    Empresa.SelStart = 0
    Empresa.SelText = ""
    Empresa.SelLength = Len(Empresa.Text)
End Sub

Private Sub Empresa_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Empresa_LostFocus()
    Empresa.Text = FiltroCaracter(Empresa.Text)
End Sub

Private Sub Fc_A_GotFocus()
    Fc_A.SelStart = 0
    Fc_A.SelText = ""
    Fc_A.SelLength = Len(Fc_A.Text)
End Sub

Private Sub Fc_A_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fc_A_LostFocus()
    Fc_A.Text = Formateado(Str$(Val(Fc_A.Text)), 0, 10, "0", False)
End Sub

Private Sub Fc_B_GotFocus()
    Fc_B.SelStart = 0
    Fc_B.SelText = ""
    Fc_B.SelLength = Len(Fc_B.Text)
End Sub

Private Sub Fc_B_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fc_B_LostFocus()
    Fc_B.Text = Formateado(Str$(Val(Fc_B.Text)), 0, 10, "0", False)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Panel de Control.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Datos
End Sub

Private Sub Cargar_Datos()
    Qy = "SELECT * FROM Parametro"
    Set Rs = Db.OpenRecordset(Qy)
    
    Empresa.Text = Trim$(Rs.Fields("Empresa"))
    Slogan.Text = Trim$(Rs.Fields("Slogan"))
    Emisor.Text = Formateado(Str$(Val(Rs.Fields("Centro_Emisor"))), 0, 4, "0", False)
    Iva.Text = Formateado(Str$(Val(Rs.Fields("Alicuota_Iva"))), 2, 6, " ", False)
    Sta.Text = Formateado(Str$(Val(Rs.Fields("Alicuota_Sta"))), 2, 6, " ", False)
    Fc_A.Text = Formateado(Str$(Val(Rs.Fields("Ultima_FC_A"))), 0, 10, "0", False)
    Nc_A.Text = Formateado(Str$(Val(Rs.Fields("Ultima_NC_A"))), 0, 10, "0", False)
    Fc_B.Text = Formateado(Str$(Val(Rs.Fields("Ultima_FC_B"))), 0, 10, "0", False)
    Nc_B.Text = Formateado(Str$(Val(Rs.Fields("Ultima_NC_B"))), 0, 10, "0", False)
    Caja.Text = Rs.Fields("Cta_Caja")
    CtaCte.Text = Rs.Fields("Cta_CtaCte")
    Cta_Debito_Iva.Text = Rs.Fields("Cta_Debito_Iva")
    Cta_Debito_STA.Text = Rs.Fields("Cta_Debito_Sta")
    Cta_Credito_Iva.Text = Rs.Fields("Cta_Credito_Iva")
    Cta_Impuestos.Text = Rs.Fields("Cta_Impuestos")
    Puerto.Text = Rs.Fields("Puerto_Impresora")
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    Actualizar_NumComp
    Cargar_Datos
    
    Qy = "UPDATE Parametro SET "
    Qy = Qy & "Empresa = '" & Trim$(Empresa.Text) & "', "
    Qy = Qy & "Slogan = '" & Trim$(Slogan.Text) & "',"
    Qy = Qy & "Centro_Emisor = '" & Str$(Val(Emisor.Text)) & "',"
    Qy = Qy & "Alicuota_Iva = '" & Val(Iva.Text) & "',"
    Qy = Qy & "Alicuota_Sta = '" & Val(Sta.Text) & "',"
    Qy = Qy & "Ultima_Fc_A = '" & Val(Fc_A.Text) & "',"
    Qy = Qy & "Ultima_Fc_B = '" & Val(Fc_B.Text) & "',"
    Qy = Qy & "Ultima_Nc_A = '" & Val(Nc_A.Text) & "',"
    Qy = Qy & "Ultima_Nc_B = '" & Val(Nc_B.Text) & "',"
    Qy = Qy & "Cta_Caja = '" & Trim$(Caja.Text) & "',"
    Qy = Qy & "Cta_CtaCte = '" & Trim$(CtaCte.Text) & "',"
    Qy = Qy & "Cta_Debito_Iva = '" & Trim$(Cta_Debito_Iva.Text) & "',"
    Qy = Qy & "Cta_Debito_Sta = '" & Trim$(Cta_Debito_STA.Text) & "',"
    Qy = Qy & "Cta_Credito_Iva = '" & Trim$(Cta_Credito_Iva.Text) & "',"
    Qy = Qy & "Cta_Impuestos = '" & Trim$(Cta_Impuestos.Text) & "',"
    Qy = Qy & "Puerto_Impresora = '" & Trim$(Str$(Val(Puerto.Text))) & "' "
    Db.Execute (Qy)
    
    Cargar_Datos
    Parametrizar
    Empresa.SetFocus
End Sub

Private Sub Iva_GotFocus()
    Iva.Text = Trim$(Iva.Text)
    Iva.SelStart = 0
    Iva.SelText = ""
    Iva.SelLength = Len(Iva.Text)
End Sub

Private Sub Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Iva_LostFocus()
    Iva.Text = Formateado(Str$(Val(Iva.Text)), 2, 6, " ", False)
End Sub

Private Sub Nc_A_GotFocus()
    Nc_A.SelStart = 0
    Nc_A.SelText = ""
    Nc_A.SelLength = Len(Nc_A.Text)
End Sub

Private Sub Nc_A_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nc_A_LostFocus()
    Nc_A.Text = Formateado(Str$(Val(Nc_A.Text)), 0, 10, "0", False)
End Sub

Private Sub Nc_B_GotFocus()
    Nc_B.SelStart = 0
    Nc_B.SelText = ""
    Nc_B.SelLength = Len(Nc_B.Text)
End Sub

Private Sub Nc_B_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nc_B_LostFocus()
    Nc_B.Text = Formateado(Str$(Val(Nc_B.Text)), 0, 10, "0", False)
End Sub

Private Sub Puerto_GotFocus()
    Puerto.SelStart = 0
    Puerto.SelText = ""
    Puerto.SelLength = Len(Puerto.Text)
End Sub

Private Sub Puerto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Puerto_LostFocus()
    Puerto.Text = Formateado(Str$(Val(Puerto.Text)), 0, 1, "", False)
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub

Private Sub Slogan_GotFocus()
    Slogan.Text = Trim$(Slogan.Text)
    Slogan.SelStart = 0
    Slogan.SelText = ""
    Slogan.SelLength = Len(Slogan.Text)
End Sub

Private Sub Slogan_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Slogan_LostFocus()
    Slogan.Text = FiltroCaracter(Slogan.Text)
End Sub

Private Sub Sta_GotFocus()
    Sta.Text = Trim$(Sta.Text)
    Sta.SelStart = 0
    Sta.SelText = ""
    Sta.SelLength = Len(Sta.Text)
End Sub

Private Sub Sta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Sta_LostFocus()
    Sta.Text = Formateado(Str$(Val(Sta.Text)), 2, 6, " ", False)
End Sub
