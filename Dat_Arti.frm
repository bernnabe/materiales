VERSION 5.00
Begin VB.Form Dat_Arti 
   Caption         =   "Actualizaci�n de Art�culos"
   ClientHeight    =   7575
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   11880
   LinkTopic       =   "Form1"
   MDIChild        =   -1  'True
   ScaleHeight     =   7575
   ScaleWidth      =   11880
   WindowState     =   2  'Maximized
   Begin VB.TextBox Memo 
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4860
      Left            =   75
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   91
      Top             =   975
      Visible         =   0   'False
      Width           =   7770
   End
   Begin VB.Frame Marco_Articulo 
      Height          =   975
      Left            =   0
      TabIndex        =   78
      Top             =   0
      Width           =   7935
      Begin VB.TextBox Descripcion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2160
         MaxLength       =   30
         TabIndex        =   2
         ToolTipText     =   "Descripci�n del Art�culo.-"
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox Articulo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         MaxLength       =   5
         TabIndex        =   1
         ToolTipText     =   "Articulo"
         Top             =   600
         Width           =   855
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Art�culo:"
         Height          =   255
         Left            =   240
         TabIndex        =   80
         Top             =   600
         Width           =   855
      End
      Begin VB.TextBox Fecha 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6480
         MaxLength       =   10
         TabIndex        =   79
         Top             =   600
         Width           =   1335
      End
      Begin VB.ComboBox Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   0
         Top             =   240
         Width           =   5655
      End
      Begin VB.Label Label32 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   81
         Top             =   240
         Width           =   1815
      End
   End
   Begin VB.Frame Botonera 
      Height          =   1095
      Left            =   7920
      TabIndex        =   77
      Top             =   6480
      Width           =   3975
      Begin VB.CommandButton Observaciones 
         Enabled         =   0   'False
         Height          =   615
         Left            =   150
         Picture         =   "Dat_Arti.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   36
         ToolTipText     =   "Mostrar el Campo Comentarios.-"
         Top             =   375
         Width           =   915
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   3000
         Picture         =   "Dat_Arti.frx":5C12
         Style           =   1  'Graphical
         TabIndex        =   35
         ToolTipText     =   "Cancelar Salir.-"
         Top             =   375
         Width           =   915
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   2025
         Picture         =   "Dat_Arti.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   34
         ToolTipText     =   "Borrar el Art�culo de la Base de Datos siempre y cuando no tenga movimientos asociados.-"
         Top             =   375
         Width           =   915
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   1125
         Picture         =   "Dat_Arti.frx":12126
         Style           =   1  'Graphical
         TabIndex        =   33
         ToolTipText     =   "Grabar los Datos del Art�culo.-"
         Top             =   375
         Width           =   915
      End
   End
   Begin VB.Frame Frame1 
      Height          =   6855
      Left            =   0
      TabIndex        =   38
      Top             =   720
      Width           =   7935
      Begin VB.Frame Marco_Medidas 
         Enabled         =   0   'False
         Height          =   2535
         Left            =   0
         TabIndex        =   40
         Top             =   0
         Width           =   7935
         Begin VB.Frame Marco_Recargos 
            BorderStyle     =   0  'None
            Enabled         =   0   'False
            Height          =   735
            Left            =   120
            TabIndex        =   82
            Top             =   1680
            Width           =   7695
            Begin VB.TextBox Ancho_Menor 
               BackColor       =   &H00E0E0E0&
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1080
               TabIndex        =   8
               Top             =   0
               Width           =   1095
            End
            Begin VB.TextBox Largo_Menor 
               BackColor       =   &H00E0E0E0&
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   1080
               TabIndex        =   12
               Top             =   360
               Width           =   1095
            End
            Begin VB.TextBox Recargo_Ancho_Menor 
               BackColor       =   &H00E0E0E0&
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2880
               TabIndex        =   9
               Top             =   0
               Width           =   855
            End
            Begin VB.TextBox Recargo_Largo_Menor 
               BackColor       =   &H00E0E0E0&
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   2880
               TabIndex        =   13
               Top             =   360
               Width           =   855
            End
            Begin VB.TextBox Ancho_Mayor 
               BackColor       =   &H00E0E0E0&
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4920
               TabIndex        =   10
               Top             =   0
               Width           =   1095
            End
            Begin VB.TextBox Recargo_Ancho_Mayor 
               BackColor       =   &H00E0E0E0&
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6840
               TabIndex        =   11
               Top             =   0
               Width           =   855
            End
            Begin VB.TextBox Largo_Mayor 
               BackColor       =   &H00E0E0E0&
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   4920
               TabIndex        =   14
               Top             =   360
               Width           =   1095
            End
            Begin VB.TextBox Recargo_Largo_Mayor 
               BackColor       =   &H00E0E0E0&
               BeginProperty Font 
                  Name            =   "Courier"
                  Size            =   9.75
                  Charset         =   0
                  Weight          =   400
                  Underline       =   0   'False
                  Italic          =   0   'False
                  Strikethrough   =   0   'False
               EndProperty
               Height          =   285
               Left            =   6840
               TabIndex        =   15
               Top             =   360
               Width           =   855
            End
            Begin VB.Label Label23 
               Alignment       =   2  'Center
               Caption         =   "% Rec.:"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   3
               Left            =   5880
               TabIndex        =   90
               Top             =   0
               Width           =   1095
            End
            Begin VB.Label Label23 
               Alignment       =   2  'Center
               Caption         =   "% Rec.:"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   1
               Left            =   6000
               TabIndex        =   89
               Top             =   360
               Width           =   855
            End
            Begin VB.Label Label23 
               Alignment       =   2  'Center
               Caption         =   "% Rec.:"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   2
               Left            =   1920
               TabIndex        =   88
               Top             =   0
               Width           =   1095
            End
            Begin VB.Label Label19 
               Caption         =   "Ancho < de:"
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   120
               TabIndex        =   87
               Top             =   0
               Width           =   870
            End
            Begin VB.Label Label20 
               Alignment       =   1  'Right Justify
               Caption         =   "Largo < de:"
               ForeColor       =   &H00800000&
               Height          =   195
               Left            =   0
               TabIndex        =   86
               Top             =   360
               Width           =   930
            End
            Begin VB.Label Label21 
               Alignment       =   1  'Right Justify
               Caption         =   "Ancho > de:"
               ForeColor       =   &H00800000&
               Height          =   255
               Left            =   3840
               TabIndex        =   85
               Top             =   0
               Width           =   975
            End
            Begin VB.Label Label22 
               Alignment       =   1  'Right Justify
               Caption         =   "Largo > de:"
               ForeColor       =   &H00800000&
               Height          =   255
               Left            =   3840
               TabIndex        =   84
               Top             =   360
               Width           =   975
            End
            Begin VB.Label Label23 
               Alignment       =   2  'Center
               Caption         =   "% Rec.:"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   0
               Left            =   2040
               TabIndex        =   83
               Top             =   360
               Width           =   855
            End
         End
         Begin VB.ComboBox Presentacion_Rev 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1200
            TabIndex        =   3
            Top             =   480
            Width           =   2655
         End
         Begin VB.ComboBox Presentacion_Pub 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1200
            TabIndex        =   4
            Top             =   840
            Width           =   2655
         End
         Begin VB.TextBox Medida_Rev 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3960
            TabIndex        =   42
            Top             =   480
            Width           =   1215
         End
         Begin VB.TextBox Medida_Pub 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3960
            TabIndex        =   41
            Top             =   840
            Width           =   1215
         End
         Begin VB.TextBox Espesor 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            MaxLength       =   9
            TabIndex        =   5
            Top             =   1320
            Width           =   1215
         End
         Begin VB.TextBox Ancho 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3960
            MaxLength       =   9
            TabIndex        =   6
            Top             =   1320
            Width           =   1215
         End
         Begin VB.TextBox Largo 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6600
            MaxLength       =   9
            TabIndex        =   7
            Top             =   1320
            Width           =   1215
         End
         Begin VB.Label Label14 
            Alignment       =   1  'Right Justify
            Caption         =   "Largo (m):"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   5760
            TabIndex        =   57
            Top             =   1320
            Width           =   735
         End
         Begin VB.Label Label13 
            Alignment       =   1  'Right Justify
            Caption         =   "Ancho (""):"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   3000
            TabIndex        =   56
            Top             =   1320
            Width           =   855
         End
         Begin VB.Label Label12 
            Alignment       =   1  'Right Justify
            Caption         =   "Espesor(""):"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   240
            TabIndex        =   55
            Top             =   1320
            Width           =   855
         End
         Begin VB.Label Label4 
            Alignment       =   2  'Center
            Caption         =   "Medida:"
            ForeColor       =   &H00800000&
            Height          =   255
            Index           =   0
            Left            =   3960
            TabIndex        =   47
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label3 
            Alignment       =   1  'Right Justify
            Caption         =   "Publico:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   240
            TabIndex        =   46
            Top             =   840
            Width           =   855
         End
         Begin VB.Label Label2 
            Alignment       =   1  'Right Justify
            Caption         =   "Revendedor:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   45
            Top             =   480
            Width           =   975
         End
         Begin VB.Label Label1 
            Alignment       =   2  'Center
            Caption         =   "Presentaci�n:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1200
            TabIndex        =   44
            Top             =   240
            Width           =   2655
         End
      End
      Begin VB.Frame Marco_Precio 
         Enabled         =   0   'False
         Height          =   2655
         Left            =   0
         TabIndex        =   43
         Top             =   2520
         Width           =   7935
         Begin VB.CommandButton cmdVerHistorial 
            Caption         =   "Ver Historial de precios"
            Height          =   315
            Left            =   2475
            TabIndex        =   105
            Top             =   450
            Width           =   2040
         End
         Begin VB.ComboBox Alicuota 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1200
            TabIndex        =   25
            Top             =   2250
            Width           =   1935
         End
         Begin VB.TextBox Ret_IVA 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4440
            MaxLength       =   5
            TabIndex        =   27
            Top             =   2280
            Width           =   735
         End
         Begin VB.TextBox Ret_IB 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3375
            MaxLength       =   5
            TabIndex        =   26
            Top             =   2280
            Width           =   735
         End
         Begin VB.Frame Frame5 
            Height          =   2655
            Left            =   5400
            TabIndex        =   58
            Top             =   0
            Width           =   2535
            Begin VB.Frame Frame4 
               BorderStyle     =   0  'None
               Enabled         =   0   'False
               Height          =   735
               Left            =   480
               TabIndex        =   98
               Top             =   1680
               Width           =   1935
               Begin VB.TextBox Precio_Cta_Rev_P2 
                  BackColor       =   &H00E0E0E0&
                  BeginProperty Font 
                     Name            =   "Courier"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   0
                  TabIndex        =   101
                  Top             =   405
                  Width           =   975
               End
               Begin VB.TextBox Precio_Recargo_Rev_Cta 
                  BackColor       =   &H00E0E0E0&
                  BeginProperty Font 
                     Name            =   "Courier"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   0
                  TabIndex        =   99
                  Top             =   30
                  Width           =   975
               End
               Begin VB.TextBox Precio_Cta_Pub_P2 
                  BackColor       =   &H00E0E0E0&
                  BeginProperty Font 
                     Name            =   "Courier"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   960
                  TabIndex        =   102
                  Top             =   405
                  Width           =   975
               End
               Begin VB.TextBox Precio_Recargo_Pub_Cta 
                  BackColor       =   &H00E0E0E0&
                  BeginProperty Font 
                     Name            =   "Courier"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   960
                  TabIndex        =   100
                  Top             =   30
                  Width           =   975
               End
            End
            Begin VB.Frame Frame3 
               BorderStyle     =   0  'None
               Caption         =   "Frame3"
               Enabled         =   0   'False
               Height          =   720
               Left            =   495
               TabIndex        =   93
               Top             =   600
               Width           =   1935
               Begin VB.TextBox Precio_Ctdo_Rev_P2 
                  BackColor       =   &H00E0E0E0&
                  BeginProperty Font 
                     Name            =   "Courier"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   0
                  TabIndex        =   96
                  Top             =   360
                  Width           =   975
               End
               Begin VB.TextBox Precio_Recargo_Rev_Ctdo 
                  BackColor       =   &H00E0E0E0&
                  BeginProperty Font 
                     Name            =   "Courier"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   0
                  TabIndex        =   94
                  Top             =   0
                  Width           =   975
               End
               Begin VB.TextBox Precio_Ctdo_Pub_P2 
                  BackColor       =   &H00E0E0E0&
                  BeginProperty Font 
                     Name            =   "Courier"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   960
                  TabIndex        =   97
                  Top             =   360
                  Width           =   975
               End
               Begin VB.TextBox Precio_Recargo_Pub_Ctdo 
                  BackColor       =   &H00E0E0E0&
                  BeginProperty Font 
                     Name            =   "Courier"
                     Size            =   9.75
                     Charset         =   0
                     Weight          =   400
                     Underline       =   0   'False
                     Italic          =   0   'False
                     Strikethrough   =   0   'False
                  EndProperty
                  Height          =   285
                  Left            =   960
                  TabIndex        =   95
                  Top             =   0
                  Width           =   975
               End
            End
            Begin VB.CheckBox Mostrar_IVA 
               Alignment       =   1  'Right Justify
               ForeColor       =   &H000000FF&
               Height          =   195
               Left            =   480
               TabIndex        =   92
               Top             =   2400
               Width           =   1935
            End
            Begin VB.Label Label24 
               Alignment       =   2  'Center
               AutoSize        =   -1  'True
               BackStyle       =   0  'Transparent
               Caption         =   "Precios Finales de CtaCte"
               ForeColor       =   &H00FF0000&
               Height          =   195
               Index           =   1
               Left            =   495
               TabIndex        =   61
               Top             =   1290
               Width           =   1845
            End
            Begin VB.Label Label24 
               Alignment       =   2  'Center
               AutoSize        =   -1  'True
               Caption         =   "Precios Finales de Ctdo."
               ForeColor       =   &H00FF0000&
               Height          =   195
               Index           =   0
               Left            =   480
               TabIndex        =   62
               Top             =   120
               Width           =   1725
            End
            Begin VB.Label Label16 
               Caption         =   "P2:"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   1
               Left            =   105
               TabIndex        =   68
               Top             =   2115
               Width           =   615
            End
            Begin VB.Label Label15 
               Caption         =   "ML:"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   1
               Left            =   105
               TabIndex        =   67
               Top             =   1755
               Width           =   375
            End
            Begin VB.Label Label15 
               Caption         =   "ML:"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   0
               Left            =   135
               TabIndex        =   66
               Top             =   600
               Width           =   375
            End
            Begin VB.Label Label16 
               Caption         =   "P2:"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   0
               Left            =   135
               TabIndex        =   65
               Top             =   975
               Width           =   615
            End
            Begin VB.Label Label17 
               Alignment       =   2  'Center
               Caption         =   "Revendedor"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   0
               Left            =   480
               TabIndex        =   64
               Top             =   360
               Width           =   975
            End
            Begin VB.Label Label18 
               Alignment       =   2  'Center
               Caption         =   "P�blico"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   0
               Left            =   1440
               TabIndex        =   63
               Top             =   360
               Width           =   975
            End
            Begin VB.Label Label17 
               Alignment       =   2  'Center
               Caption         =   "Revendedor"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   1
               Left            =   495
               TabIndex        =   60
               Top             =   1500
               Width           =   975
            End
            Begin VB.Label Label18 
               Alignment       =   2  'Center
               Caption         =   "P�blico"
               ForeColor       =   &H00800000&
               Height          =   255
               Index           =   1
               Left            =   1440
               TabIndex        =   59
               Top             =   1500
               Width           =   975
            End
         End
         Begin VB.TextBox Precio_CtaCte_Rev 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4200
            MaxLength       =   8
            TabIndex        =   20
            Top             =   1320
            Width           =   1095
         End
         Begin VB.TextBox Precio_CtaCte_Pub 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   4200
            MaxLength       =   8
            TabIndex        =   24
            Top             =   1680
            Width           =   1095
         End
         Begin VB.TextBox Rec_CtaCte_Pub 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3360
            MaxLength       =   5
            TabIndex        =   23
            Top             =   1680
            Width           =   735
         End
         Begin VB.TextBox Precio_Ctdo_Pub 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            MaxLength       =   8
            TabIndex        =   22
            Top             =   1680
            Width           =   1095
         End
         Begin VB.TextBox Utilidad_Pub 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            MaxLength       =   5
            TabIndex        =   21
            Top             =   1680
            Width           =   735
         End
         Begin VB.TextBox Utilidad_Rev 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            MaxLength       =   5
            TabIndex        =   17
            Top             =   1320
            Width           =   735
         End
         Begin VB.TextBox Precio_Compra 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            MaxLength       =   9
            TabIndex        =   16
            Top             =   480
            Width           =   1215
         End
         Begin VB.TextBox Rec_CtaCte_Rev 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   3360
            MaxLength       =   5
            TabIndex        =   19
            Top             =   1320
            Width           =   735
         End
         Begin VB.TextBox Precio_Ctdo_Rev 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   2040
            MaxLength       =   8
            TabIndex        =   18
            Top             =   1320
            Width           =   1095
         End
         Begin VB.Label Label34 
            Alignment       =   1  'Right Justify
            Caption         =   "Impuestos:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   104
            Top             =   2280
            Width           =   975
         End
         Begin VB.Label Label33 
            Caption         =   "Alicuota IVA"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1200
            TabIndex        =   103
            Top             =   2040
            Width           =   1935
         End
         Begin VB.Label Label26 
            Alignment       =   1  'Right Justify
            Caption         =   "% Ret. I.V.A."
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   4200
            TabIndex        =   70
            Top             =   2040
            Width           =   1095
         End
         Begin VB.Label Label25 
            Alignment       =   1  'Right Justify
            BackStyle       =   0  'Transparent
            Caption         =   "% Ret. I.I.B.B."
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   3120
            TabIndex        =   69
            Top             =   2040
            Width           =   1095
         End
         Begin VB.Label Label11 
            Alignment       =   1  'Right Justify
            AutoSize        =   -1  'True
            Caption         =   "P�blico:"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   480
            TabIndex        =   54
            Top             =   1680
            Width           =   570
         End
         Begin VB.Label Label10 
            Alignment       =   2  'Center
            Caption         =   "% Utilidad:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1200
            TabIndex        =   53
            Top             =   1080
            Width           =   735
         End
         Begin VB.Label Label9 
            Alignment       =   2  'Center
            Caption         =   "Precio Cta. Cte.:"
            ForeColor       =   &H00800000&
            Height          =   375
            Left            =   4320
            TabIndex        =   52
            Top             =   840
            Width           =   855
         End
         Begin VB.Label Label8 
            Alignment       =   2  'Center
            Caption         =   "Precio de Compra:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   1080
            TabIndex        =   51
            Top             =   240
            Width           =   1455
         End
         Begin VB.Label Label7 
            Alignment       =   2  'Center
            Caption         =   "% Rec. Cta.Cte.:"
            ForeColor       =   &H00800000&
            Height          =   375
            Left            =   3240
            TabIndex        =   50
            Top             =   840
            Width           =   975
         End
         Begin VB.Label Label6 
            Alignment       =   2  'Center
            Caption         =   "Precio Contado:"
            ForeColor       =   &H00800000&
            Height          =   375
            Left            =   2160
            TabIndex        =   49
            Top             =   840
            Width           =   975
         End
         Begin VB.Label Label5 
            AutoSize        =   -1  'True
            Caption         =   "Revendedor:"
            ForeColor       =   &H00800000&
            Height          =   195
            Left            =   120
            TabIndex        =   48
            Top             =   1320
            Width           =   930
         End
      End
      Begin VB.Frame Marco_Cuentas 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1695
         Left            =   0
         TabIndex        =   71
         Top             =   5160
         Width           =   7935
         Begin VB.TextBox Stock_Minimo 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   6480
            MaxLength       =   10
            TabIndex        =   29
            Top             =   240
            Width           =   1335
         End
         Begin VB.TextBox Existencia 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1200
            MaxLength       =   10
            TabIndex        =   28
            Top             =   240
            Width           =   1335
         End
         Begin VB.ComboBox Proveedor 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1200
            TabIndex        =   30
            Top             =   600
            Width           =   6615
         End
         Begin VB.ComboBox Cuenta_Ventas 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1200
            TabIndex        =   32
            Top             =   1320
            Width           =   6615
         End
         Begin VB.ComboBox Cuenta_Compras 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   315
            Left            =   1200
            TabIndex        =   31
            Top             =   960
            Width           =   6615
         End
         Begin VB.Label Label31 
            Alignment       =   1  'Right Justify
            Caption         =   "Stock M�nimo:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   5280
            TabIndex        =   76
            Top             =   240
            Width           =   1095
         End
         Begin VB.Label Label30 
            Alignment       =   1  'Right Justify
            Caption         =   "Existencia:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   75
            Top             =   240
            Width           =   975
         End
         Begin VB.Label Label29 
            Alignment       =   1  'Right Justify
            Caption         =   "Proveedor:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   240
            TabIndex        =   74
            Top             =   600
            Width           =   855
         End
         Begin VB.Label Label28 
            Alignment       =   1  'Right Justify
            Caption         =   "Cta. Ventas:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   73
            Top             =   1320
            Width           =   975
         End
         Begin VB.Label Label27 
            Alignment       =   1  'Right Justify
            Caption         =   "Cta Compras:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   72
            Top             =   960
            Width           =   975
         End
      End
   End
   Begin VB.Frame Frame2 
      Height          =   6495
      Left            =   7920
      TabIndex        =   39
      Top             =   0
      Width           =   3975
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   6135
         Left            =   120
         TabIndex        =   37
         Top             =   240
         Width           =   3735
      End
   End
End
Attribute VB_Name = "Dat_Arti"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Alicuota_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_GotFocus()
    Ancho.Text = Trim(Ancho.Text)
    
    Ancho.SelStart = 0
    Ancho.SelText = ""
    Ancho.SelLength = Len(Ancho.Text)
End Sub

Private Sub Ancho_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_LostFocus()
    Ancho.Text = Formateado(Str(Val(Ancho.Text)), 2, 9, " ", False)
    Calcular_Medida_Presentacion
End Sub

Private Sub Ancho_Mayor_GotFocus()
    Ancho_Mayor.Text = Trim(Ancho_Mayor.Text)
    
    Ancho_Mayor.SelStart = 0
    Ancho_Mayor.SelText = ""
    Ancho_Mayor.SelLength = Len(Ancho_Mayor.Text)
End Sub

Private Sub Ancho_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_Mayor_LostFocus()
    Ancho_Mayor.Text = Formateado(Str(Val(Ancho_Mayor.Text)), 2, 8, " ", False)
    Aderir_Recargos
End Sub

Private Sub Ancho_Menor_GotFocus()
    Ancho_Menor.Text = Trim(Ancho_Menor.Text)
    
    Ancho_Menor.SelStart = 0
    Ancho_Menor.SelText = ""
    Ancho_Menor.SelLength = Len(Ancho_Menor.Text)
End Sub

Private Sub Ancho_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_Menor_LostFocus()
    Ancho_Menor.Text = Formateado(Str(Val(Ancho_Menor.Text)), 2, 8, " ", False)
    Aderir_Recargos
End Sub

Private Sub Articulo_GotFocus()
    Articulo.Text = Trim(Articulo.Text)
    
    Articulo.SelStart = 0
    Articulo.SelLength = Len(Articulo.Text)
End Sub

Private Sub Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        If Val(Articulo.Text) > 0 And Val(Mid(Rubro.Text, 31, 5)) > 0 Then
            Leer_Articulo
        ElseIf Val(Mid(Rubro.Text, 31, 5)) > 0 Then
            qy = "SELECT MAX(Id_Articulo) FROM Articulo WHERE Id_Rubro = " & Val(Mid(Rubro.Text, 31, 5))
            AbreRs
        
            Articulo.Text = 1
            If Not IsNull(Rs.Fields(0)) = True Then Articulo.Text = Val(Rs.Fields(0)) + 1
        
            Leer_Articulo
        Else
            Rubro.SetFocus
        End If
    End If
End Sub

Private Sub Leer_Articulo()
    If Val(Articulo.Text) > 0 And Val(Mid(Rubro.Text, 31, 5)) > 0 Then
        qy = "SELECT * FROM Articulo WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " "
        qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Mid(Rubro.Text, 31, 5))))
        AbreRs
        
        If Rs.EOF Then
            If MsgBox("El Art�culo es inexistente, desea incorporarlo ahora. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
                Grabar.Tag = "Alta"
                
                Habilitar_Campos
                Borrar.Enabled = False
                Descripcion.SetFocus
            Else
                Salir_Click
            End If
        Else
            If Val(Permiso_Cons) = 1 Then
                Mostrar_Articulo
                
                Habilitar_Campos
                Grabar.Tag = "Modificaci�n"
                Existencia.Enabled = False
                Descripcion.SetFocus
            Else
                MsgBox "Usted no est� autorizado para estos procesos.!", vbCritical, "Atenci�n.!"
                Borrar_Campo
                Rubro.SetFocus
            End If
        End If
    Else
        Articulo.Text = ""
        Articulo.SetFocus
    End If
End Sub

Private Sub Habilitar_Campos()
    Marco_Medidas.Enabled = True
    Marco_Precio.Enabled = True
    Marco_Cuentas.Enabled = True
    Grabar.Enabled = True
    Borrar.Enabled = True
    Observaciones.Enabled = True
End Sub

Private Sub Mostrar_Articulo()
    Articulo.Enabled = False
    Rubro.Enabled = False
    
    Descripcion.Text = Trim(Rs.Fields("Descripcion"))
    Espesor.Text = Formateado(Str(Val(Rs.Fields("Espesor"))), 2, 9, " ", False)
    Ancho.Text = Formateado(Str(Val(Rs.Fields("Ancho"))), 2, 9, " ", False)
    Largo.Text = Formateado(Str(Val(Rs.Fields("Largo"))), 2, 9, " ", False)
    
    Precio_Compra.Text = Formateado(Str(Val(Rs.Fields("Precio_Compra"))), 3, 9, " ", False)
    Rec_CtaCte_Pub.Text = Formateado(Str(Val(Rs.Fields("Rec_CtaCte_Pub"))), 2, 5, " ", False)
    Rec_CtaCte_Rev.Text = Formateado(Str(Val(Rs.Fields("Rec_CtaCte_Rev"))), 2, 5, " ", False)
    Utilidad_Rev.Text = Formateado(Str(Val(Rs.Fields("Margen_Rev"))), 2, 5, " ", False)
    Utilidad_Pub.Text = Formateado(Str(Val(Rs.Fields("Margen_Pub"))), 2, 5, " ", False)
    
    'Precio_Ctdo_Pub.Text = Formateado(str(Val(Rs.Fields("Precio_Ctdo_Pub"))), 3, 8, " ", False)
    'Precio_CtaCte_Pub.Text = Formateado(str(Val(Rs.Fields("Precio_CtaCte_Pub"))), 3, 8, " ", False)
    'Precio_Ctdo_Rev.Text = Formateado(str(Val(Rs.Fields("Precio_Ctdo_Rev"))), 3, 8, " ", False)
    'Precio_CtaCte_Rev.Text = Formateado(str(Val(Rs.Fields("Precio_CtaCte_Rev"))), 3, 8, " ", False)
    
    Existencia.Text = Formateado(Str(Val(Rs.Fields("Existencia"))), 2, 10, " ", False)
    Stock_Minimo.Text = Formateado(Str(Val(Rs.Fields("Stock_Minimo"))), 2, 10, " ", False)
    
    Ancho_Menor.Text = Formateado(Str(Val(Rs.Fields("Ancho_Menor"))), 2, 8, " ", False)
    Recargo_Ancho_Menor.Text = Formateado(Str(Val(Rs.Fields("Rec_Ancho_Menor"))), 2, 6, " ", False)
    
    Largo_Menor.Text = Formateado(Str(Val(Rs.Fields("Largo_Menor"))), 2, 8, " ", False)
    Recargo_Largo_Menor.Text = Formateado(Str(Val(Rs.Fields("Rec_Largo_Menor"))), 2, 6, " ", False)
    
    Ancho_Mayor.Text = Formateado(Str(Val(Rs.Fields("Ancho_Mayor"))), 2, 8, " ", False)
    Recargo_Ancho_Mayor.Text = Formateado(Str(Val(Rs.Fields("Rec_Ancho_Mayor"))), 2, 6, " ", False)
    
    Largo_Mayor.Text = Formateado(Str(Val(Rs.Fields("Largo_Mayor"))), 2, 8, " ", False)
    Recargo_Largo_Mayor.Text = Formateado(Str(Val(Rs.Fields("Rec_Largo_Mayor"))), 2, 6, " ", False)
    
    Ret_Iva.Text = Formateado(Str(Val(Rs.Fields("Ret_IVA"))), 2, 5, " ", False)
    Ret_IB.Text = Formateado(Str(Val(Rs.Fields("Ret_IIBB"))), 2, 5, " ", False)
    
    Proveedor.Text = Val(Rs.Fields("Proveedor"))
    
    Memo.Text = Rs.Fields("Observaciones")
    
    If Trim(Str(Val(Rs.Fields("Presentacion_Pub")))) = 1 Then
        Presentacion_Pub.ListIndex = 0
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Pub")))) = 2 Then
        Presentacion_Pub.ListIndex = 1
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Pub")))) = 3 Then
        Presentacion_Pub.ListIndex = 2
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Pub")))) = 4 Then
        Presentacion_Pub.ListIndex = 3
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Pub")))) = 5 Then
        Presentacion_Pub.ListIndex = 4
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Pub")))) = 6 Then
        Presentacion_Pub.ListIndex = 5
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Pub")))) = 7 Then
        Presentacion_Pub.ListIndex = 6
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Pub")))) = 8 Then
        Presentacion_Pub.ListIndex = 7
    End If
    
    If Trim(Str(Val(Rs.Fields("Presentacion_Rev")))) = 1 Then
        Presentacion_Rev.ListIndex = 0
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Rev")))) = 2 Then
        Presentacion_Rev.ListIndex = 1
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Rev")))) = 3 Then
        Presentacion_Rev.ListIndex = 2
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Rev")))) = 4 Then
        Presentacion_Rev.ListIndex = 3
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Rev")))) = 5 Then
        Presentacion_Rev.ListIndex = 4
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Rev")))) = 6 Then
        Presentacion_Rev.ListIndex = 5
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Rev")))) = 7 Then
        Presentacion_Rev.ListIndex = 6
    ElseIf Trim(Str(Val(Rs.Fields("Presentacion_Rev")))) = 8 Then
        Presentacion_Rev.ListIndex = 7
    End If
    
    Cuenta_Compras.Text = Trim(Rs.Fields("Cuenta_Compra"))
    Cuenta_Ventas.Text = Trim(Rs.Fields("Cuenta_Venta"))
    
    Alicuota.Text = Rs.Fields("Alicuota_iva")
    
    Identificar_Cuenta 'Buscar La Cuenta la base de datos
    
    Habilitar_Medidas
    Aderir_Recargos
    
    Calcular_Medida_Presentacion
    Rubro_LostFocus
    
    If Val(Proveedor.Text) > 0 Then
        qy = "SELECT * FROM Proveedor WHERE Id_Proveedor = " & Trim(Str(Val(Proveedor.Text)))
        AbreRs
        
        If Not Rs.EOF Then
            Proveedor.Text = Trim(Rs.Fields("Nombre")) + Space(31 - Len(Trim(Rs.Fields("Nombre")))) & Trim(Rs.Fields("Id_Proveedor"))
        End If
    End If
End Sub

Private Sub Identificar_Cuenta()
    Dim l As Long
    
    For l = 0 To Cuenta_Compras.ListCount - 1
        If Trim(Cuenta_Compras.Text) = Trim(Mid(Cuenta_Compras.List(l), 51)) Then
            Cuenta_Compras.Text = Cuenta_Compras.List(l)
            Exit For
        End If
    Next
    
    For l = 0 To Cuenta_Ventas.ListCount - 1
        If Trim(Cuenta_Ventas.Text) = Trim(Mid(Cuenta_Ventas.List(l), 51)) Then
            Cuenta_Ventas.Text = Cuenta_Ventas.List(l)
            Exit For
        End If
    Next
End Sub

Private Sub Borrar_Click()
    If Val(Permiso_Alta) = 1 Then
        If MsgBox("Esta seguro de borrar �ste art�culo. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            qy = "DELETE FROM Articulo WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " "
            qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Mid(Rubro.Text, 31, 5))))
            Db.Execute (qy)
        End If
        
        Salir_Click
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Check1_Click()
    
End Sub

Private Sub cmdVerHistorial_Click()
    
    If Val(Articulo.Text) > 0 And Val(Mid(Rubro.Text, 31, 5)) > 0 Then
    
        Load frmHistorialPrecios
        
        frmHistorialPrecios.Show
        
        frmHistorialPrecios.CargarDatos Val(Articulo.Text), Val(Mid(Rubro.Text, 31, 5))
        
    End If
        
End Sub

Private Sub Cuenta_Compras_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cuenta_Compras_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_Ventas_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cuenta_Ventas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Descripcion_GotFocus()
    Descripcion.Text = Trim(Descripcion.Text)
    
    Descripcion.SelStart = 0
    Descripcion.SelLength = Len(Descripcion.Text)
End Sub

Private Sub Descripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Descripcion_LostFocus()
    Descripcion.Text = FiltroCaracter(Descripcion.Text)
End Sub

Private Sub Espesor_GotFocus()
    Espesor.Text = Trim(Espesor.Text)
    
    Espesor.SelStart = 0
    Espesor.SelLength = Len(Espesor.Text)
End Sub

Private Sub Espesor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Espesor_LostFocus()
    Espesor.Text = Formateado(Str(Val(Espesor.Text)), 2, 9, " ", False)
    Calcular_Medida_Presentacion
End Sub

Private Sub Existencia_GotFocus()
    Existencia.Text = Trim(Existencia.Text)
    
    Existencia.SelStart = 0
    Existencia.SelLength = Len(Existencia.Text)
End Sub

Private Sub Existencia_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Existencia_LostFocus()
    Existencia.Text = Formateado(Str(Val(Existencia.Text)), 2, 10, " ", False)
End Sub

Private Sub Form_Activate()
    Dat_Arti.WindowState = 2
    Menu.Estado.Panels(2).Text = "Actualiazaci�n del Registro de Art�culos.-"
End Sub

Private Sub Form_Deactivate()
    Dat_Arti.WindowState = 1
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    For i = 1 To 8
        Presentacion_Pub.AddItem Articulo_Presentacion(i)
        Presentacion_Rev.AddItem Articulo_Presentacion(i)
    Next
    
    Cargar_Datos
    
    Mostrar_IVA.Caption = "Precios con IVA (" & Trim(Alicuota_Iva) & "%)"
End Sub

Private Sub Cargar_Datos()
    qy = "SELECT * FROM Rubro ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Rubro.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 4, " ", False) & " " & Formateado(Str(Val(Rs.Fields("Margen_Util_Pub_Ctdo"))), 2, 9, " ", False) & " " & Formateado(Str(Val(Rs.Fields("Margen_Util_Rev_Ctdo"))), 2, 9, " ", False) & " " & Formateado(Str(Val(Rs.Fields("Margen_Util_Pub_Cta"))), 2, 9, " ", False) & " " & Formateado(Str(Val(Rs.Fields("Margen_Util_Rev_Cta"))), 2, 9, " ", False)
        
        Rs.MoveNext
    Wend
    
    qy = "SELECT * FROM Cuenta WHERE Recibe_Asiento = -1 "
    qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Cuenta_Compras.AddItem Trim(Rs.Fields("Denominacion")) + Space(50 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6"))
        Cuenta_Ventas.AddItem Trim(Rs.Fields("Denominacion")) + Space(50 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6"))
        Rs.MoveNext
    Wend
        
    qy = "SELECT * FROM Proveedor ORDER BY Nombre"
    AbreRs
    
    While Not Rs.EOF
        Proveedor.AddItem Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields("Id_Proveedor"))
        Rs.MoveNext
    Wend
    
    qy = "Select * From Articulo_Alicuota Order By Denominacion"
    AbreRs

    While Not Rs.EOF

        Alicuota.AddItem Trim(Rs.Fields("Alicuota"))
        Rs.MoveNext

    Wend
    
End Sub

Private Sub Cargar_Articulo()
    If Val(Mid(Rubro.Text, 31, 5)) > 0 Then
        qy = "SELECT * FROM Articulo WHERE Id_Rubro =  " & Val(Mid(Rubro.Text, 31, 5)) & " ORDER BY Descripcion"
        AbreRs
        
        List1.Clear
        
        While Not Rs.EOF
            List1.AddItem Trim(Rs.Fields("Descripcion")) + Space(30 - Len(Trim(Rs.Fields("Descripcion")))) & " " & Trim(Rs.Fields("Id_Articulo"))
            Rs.MoveNext
        Wend
    Else
        List1.Clear
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        
        If Val(Permiso_Alta) = 1 Then
            'Agrego el Art�culo a la Base de Datos.-
            
            qy = "INSERT INTO Articulo VALUES ("
            qy = qy & Trim(Str(Val(Mid(Rubro.Text, 31, 5)))) & " "
            qy = qy & ", " & Trim(Str(Val(Articulo.Text))) & " "
            qy = qy & ", '" & Trim(Descripcion.Text) & "'"
            qy = qy & ", " & Trim(Str(Val(Mid(Presentacion_Pub.Text, 1, 1)))) & " "
            qy = qy & ", " & Trim(Str(Val(Mid(Presentacion_Rev.Text, 1, 1)))) & " "
            qy = qy & ", " & Trim(Str(Val(Espesor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Ancho.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Largo.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Utilidad_Pub.Text)))
            qy = qy & ", " & Trim(Str(Val(Utilidad_Rev.Text)))
            qy = qy & ", " & Trim(Str(Val(Rec_CtaCte_Pub.Text)))
            qy = qy & ", " & Trim(Str(Val(Rec_CtaCte_Rev.Text)))
            qy = qy & ", " & Trim(Str(Val(Precio_Compra.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Ret_IB.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Ret_Iva.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Existencia.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Stock_Minimo.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Ancho_Menor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Recargo_Ancho_Menor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Largo_Menor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Recargo_Largo_Menor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Ancho_Mayor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Recargo_Ancho_Mayor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Largo_Mayor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Recargo_Largo_Mayor.Text))) & " "
            qy = qy & ", " & Trim(Str(Val(Mid(Proveedor.Text, 31)))) & " "
            qy = qy & ", -1 "
            qy = qy & ", '" & IIf(Trim(Mid(Cuenta_Compras.Text, 51)) <> "", Trim(Mid(Cuenta_Compras.Text, 51)), "1.5.1.1.0.0") & "'"
            qy = qy & ", '" & IIf(Trim(Mid(Cuenta_Ventas.Text, 51)) <> "", Trim(Mid(Cuenta_Ventas.Text, 51)), "4.1.1.0.0.0") & "'"
            qy = qy & ", '" & Trim(Fecha_Fiscal) & "'"
            qy = qy & ", '" & Trim(Memo.Text) & "'"
            qy = qy & ", " & IIf(Val(Alicuota.Text) = 0, Alicuota_Iva, Trim(Alicuota.Text)) & ")"
            Db.Execute (qy)
            
        Else
            MsgBox "Usted no est� autorizado para estos procesos, el art�culo no ser� guardado.", vbCritical, "Atenci�n.!"
        End If
    Else
        
        'Modifico los datos del Art�culo.-
        
        If Val(Permiso_Mod) = 1 Then
        
            Dim rstDatos As New ADODB.Recordset
            
            qy = "Select Precio_Compra From Articulo Where Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " "
            qy = qy & " And Id_Rubro = " & Trim(Str(Val(Mid(Rubro.Text, 31, 5))))
            rstDatos.Open qy, Db
            
            If Not rstDatos.EOF Then
            
                If (Val(rstDatos("Precio_Compra")) <> Val(Precio_Compra.Text)) Then
            
                    qy = "INSERT INTO Articulo_Historial_Precio VALUES ("
                    qy = qy & Trim(Str(Val(Mid(Rubro.Text, 31, 5)))) & " "
                    qy = qy & ", " & Trim(Str(Val(Articulo.Text))) & " "
                    qy = qy & ", GetDate() "
                    qy = qy & ", " & Trim(Val(rstDatos("Precio_Compra"))) & " "
                    qy = qy & ", " & Trim(Val(Precio_Compra.Text)) & " "
                    qy = qy & ", '" & Trim(Numero_Usuario) & "') "
                    
                    Db.Execute qy
                
                End If
            
            End If
        
            qy = "UPDATE Articulo SET "
            qy = qy & "Descripcion = '" & Trim(Descripcion.Text) & "',"
            qy = qy & "Id_Rubro = " & Trim(Str(Val(Mid(Rubro.Text, 31, 5)))) & ", "
            qy = qy & "Presentacion_Pub = " & Trim(Str(Val(Mid(Presentacion_Pub.Text, 1, 1)))) & ", "
            qy = qy & "Presentacion_Rev = " & Trim(Str(Val(Mid(Presentacion_Rev.Text, 1, 1)))) & ", "
            qy = qy & "Espesor = " & Trim(Str(Val(Espesor.Text))) & ", "
            qy = qy & "Ancho = " & Trim(Str(Val(Ancho.Text))) & ", "
            qy = qy & "Largo = " & Trim(Str(Val(Largo.Text))) & ", "
            qy = qy & "Margen_Pub = " & Trim(Str(Val(Utilidad_Pub.Text))) & ","
            qy = qy & "Margen_Rev = " & Trim(Str(Val(Utilidad_Rev.Text))) & ","
            qy = qy & "Rec_CtaCte_Pub = " & Trim(Str(Val(Rec_CtaCte_Pub.Text))) & ", "
            qy = qy & "Rec_CtaCte_Rev = " & Trim(Str(Val(Rec_CtaCte_Rev.Text))) & ", "
            qy = qy & "Precio_Compra = " & Trim(Str(Val(Precio_Compra.Text))) & ", "
            qy = qy & "Ret_IIBB = " & Trim(Str(Val(Ret_IB.Text))) & ", "
            qy = qy & "Ret_Iva = " & Trim(Str(Val(Ret_Iva.Text))) & ", "
            qy = qy & "Existencia = " & Trim(Str(Val(Existencia.Text))) & ", "
            qy = qy & "Stock_Minimo = " & Trim(Str(Val(Stock_Minimo.Text))) & ", "
            qy = qy & "Ancho_Menor = " & Trim(Str(Val(Ancho_Menor.Text))) & ", "
            qy = qy & "Rec_Ancho_Menor = " & Trim(Str(Val(Recargo_Ancho_Menor.Text))) & ", "
            qy = qy & "Largo_Menor = " & Trim(Str(Val(Largo_Menor.Text))) & ", "
            qy = qy & "Rec_Largo_Menor = " & Trim(Str(Val(Recargo_Largo_Menor.Text))) & ", "
            qy = qy & "Ancho_Mayor = " & Trim(Str(Val(Ancho_Mayor.Text))) & ", "
            qy = qy & "Rec_Ancho_Mayor = " & Trim(Str(Val(Recargo_Ancho_Mayor.Text))) & ", "
            qy = qy & "Largo_Mayor = " & Trim(Str(Val(Largo_Mayor.Text))) & ", "
            qy = qy & "Rec_Largo_Mayor = " & Trim(Str(Val(Recargo_Largo_Mayor.Text))) & ", "
            qy = qy & "Proveedor = " & Trim(Str(Val(Mid(Proveedor.Text, 31)))) & ", "
            qy = qy & "Cuenta_Compra = '" & IIf(Trim(Mid(Cuenta_Compras.Text, 51)) <> "", Trim(Mid(Cuenta_Compras.Text, 51)), "6.1.1.2.0.0") & "', "
            qy = qy & "Cuenta_Venta = '" & IIf(Trim(Mid(Cuenta_Ventas.Text, 51)) <> "", Trim(Mid(Cuenta_Ventas.Text, 51)), "4.1.1.0.0.0") & "', "
            qy = qy & "Alicuota_iva = " & IIf(Val(Alicuota.Text) = 0, Alicuota_Iva, Val(Alicuota.Text)) & ", "
            qy = qy & "Observaciones = '" & Trim(Memo.Text) & "' "
            qy = qy & "WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " AND Id_Rubro = " & Trim(Str(Val(Mid(Rubro.Text, 31, 5))))
            Db.Execute (qy)
            
        Else
            MsgBox "Usted no est� autorizado para estos procesos, no se guardar�n los cambios.", vbCritical, "Atenci�n.!"
        End If
    End If
    
    Salir_Click
End Sub

Private Sub Largo_GotFocus()
    Largo.Text = Trim(Largo.Text)
    
    Largo.SelStart = 0
    Largo.SelLength = Len(Largo.Text)
End Sub

Private Sub Largo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_LostFocus()
    Largo.Text = Formateado(Str(Val(Largo.Text)), 2, 9, " ", False)
    
    If Val(Mid(Presentacion_Pub.Text, 1, 1)) = 3 Or Val(Mid(Presentacion_Pub.Text, 1, 1)) = 4 Or Val(Mid(Presentacion_Pub.Text, 1, 1)) = 5 Then
        If Val(Len(Trim(Descripcion.Text))) = 0 Then
            Descripcion.Text = Trim(Mid(Rubro.Text, 1, 30)) & " " & Trim(Str(Val(Espesor.Text))) & """"
        End If
    End If
    
    Calcular_Medida_Presentacion
End Sub

Private Sub List1_Click()
    If Val(Mid(List1.List(List1.ListIndex), 32, 5)) > 0 Then
        Articulo.Text = Mid(List1.List(List1.ListIndex), 32, 5)
        Leer_Articulo
    End If
End Sub

Private Sub Mostrar_IVA_Click()
    If Mostrar_IVA.Value = 1 Then 'habilitado
        Precio_Recargo_Rev_Ctdo.Text = ((Val(Precio_Recargo_Rev_Ctdo.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Recargo_Rev_Ctdo.Text)
        Precio_Recargo_Rev_Cta.Text = ((Val(Precio_Recargo_Rev_Cta.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Recargo_Rev_Cta.Text)
        Precio_Recargo_Pub_Ctdo.Text = ((Val(Precio_Recargo_Pub_Ctdo.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Recargo_Pub_Ctdo.Text)
        Precio_Recargo_Pub_Cta.Text = ((Val(Precio_Recargo_Pub_Cta.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Recargo_Pub_Cta.Text)
        Precio_Ctdo_Rev_P2.Text = ((Val(Precio_Ctdo_Rev_P2.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Ctdo_Rev_P2.Text)
        Precio_Ctdo_Pub_P2.Text = ((Val(Precio_Ctdo_Pub_P2.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Ctdo_Pub_P2.Text)
        Precio_Cta_Rev_P2.Text = ((Val(Precio_Cta_Rev_P2.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Cta_Rev_P2.Text)
        Precio_Cta_Pub_P2.Text = ((Val(Precio_Cta_Pub_P2.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Cta_Pub_P2.Text)
    Else
        Aderir_Recargos
    End If
    
    Precio_Recargo_Rev_Ctdo.Text = Formateado(Str(Val(Precio_Recargo_Rev_Ctdo.Text)), 2, 7, " ", False)
    Precio_Recargo_Rev_Cta.Text = Formateado(Str(Val(Precio_Recargo_Rev_Cta.Text)), 2, 7, " ", False)
    Precio_Recargo_Pub_Ctdo.Text = Formateado(Str(Val(Precio_Recargo_Pub_Ctdo.Text)), 2, 7, " ", False)
    Precio_Recargo_Pub_Cta.Text = Formateado(Str(Val(Precio_Recargo_Pub_Cta.Text)), 2, 7, " ", False)
    Precio_Ctdo_Rev_P2.Text = Formateado(Str(Val(Precio_Ctdo_Rev_P2.Text)), 2, 7, " ", False)
    Precio_Ctdo_Pub_P2.Text = Formateado(Str(Val(Precio_Ctdo_Pub_P2.Text)), 2, 7, " ", False)
    Precio_Cta_Rev_P2.Text = Formateado(Str(Val(Precio_Cta_Rev_P2.Text)), 2, 7, " ", False)
    Precio_Cta_Pub_P2.Text = Formateado(Str(Val(Precio_Cta_Pub_P2.Text)), 2, 7, " ", False)
End Sub

Private Sub Observaciones_Click()
    If Memo.Visible = False Then
        Memo.Visible = True
        Observaciones.Cancel = True
        
        Memo.SetFocus
    Else
        Memo.Visible = False
        Salir.Cancel = True
    End If
End Sub

Private Sub Precio_Compra_GotFocus()
    Precio_Compra.Text = Trim(Precio_Compra.Text)
    
    Precio_Compra.SelStart = 0
    Precio_Compra.SelLength = Len(Precio_Compra.Text)
End Sub

Private Sub Precio_Compra_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Compra_LostFocus()
    Precio_Compra.Text = Formateado(Str(Val(Precio_Compra.Text)), 3, 9, " ", False)

    Utilidad_Rev_GotFocus
    Utilidad_Pub_GotFocus
    Rec_CtaCte_Rev_GotFocus
    Rec_CtaCte_Pub_GotFocus
    
    Utilidad_Rev_LostFocus
    Utilidad_Pub_LostFocus
    Rec_CtaCte_Rev_LostFocus
    Rec_CtaCte_Pub_LostFocus
End Sub

Private Sub Presentacion_Pub_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Presentacion_Pub_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Presentacion_Pub_LostFocus()
    Habilitar_Medidas
End Sub

Private Sub Presentacion_Rev_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Presentacion_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Presentacion_Rev_LostFocus()
    Habilitar_Medidas
End Sub

Private Sub Proveedor_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Proveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rec_CtaCte_Pub_GotFocus()
    If Val(Rec_CtaCte_Pub.Text) = 0 Then Rec_CtaCte_Pub.Text = Trim(Mid(Rubro.Text, 67, 9))
    Rec_CtaCte_Pub.Text = Trim(Rec_CtaCte_Pub.Text)
    
    Rec_CtaCte_Pub.SelStart = 0
    Rec_CtaCte_Pub.SelLength = Len(Rec_CtaCte_Pub.Text)
End Sub

Private Sub Rec_CtaCte_Pub_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rec_CtaCte_Pub_LostFocus()
    Rec_CtaCte_Pub.Text = Formateado(Str(Val(Rec_CtaCte_Pub.Text)), 2, 5, " ", False)
    Aderir_Recargos
End Sub

Private Sub Rec_CtaCte_Rev_GotFocus()
    If Val(Rec_CtaCte_Rev.Text) = 0 Then Rec_CtaCte_Rev.Text = Trim(Mid(Rubro.Text, 58, 9))
    '"Margen_Util_Pub_Ctdo" 9 "Margen_Util_Rev_Ctdo" 9 "Margen_Util_Pub_Cta" 9 "Margen_Util_Rev_Cta" 9
    Rec_CtaCte_Rev.Text = Trim(Rec_CtaCte_Rev.Text)
    
    Rec_CtaCte_Rev.SelStart = 0
    Rec_CtaCte_Rev.SelLength = Len(Rec_CtaCte_Rev.Text)
End Sub

Private Sub Rec_CtaCte_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rec_CtaCte_Rev_LostFocus()
    Rec_CtaCte_Rev.Text = Formateado(Str(Val(Rec_CtaCte_Rev.Text)), 2, 5, " ", False)
    
    Aderir_Recargos
End Sub

Private Sub Ret_IB_GotFocus()
    Ret_IB.Text = Trim(Ret_IB.Text)
    
    Ret_IB.SelStart = 0
    Ret_IB.SelLength = Len(Ret_IB.Text)
End Sub

Private Sub Ret_IB_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ret_IB_LostFocus()
    Ret_IB.Text = Formateado(Str(Val(Ret_IB.Text)), 2, 5, " ", False)
End Sub

Private Sub Ret_Iva_GotFocus()
    Ret_Iva.Text = Trim(Ret_Iva.Text)
    
    Ret_Iva.SelStart = 0
    Ret_Iva.SelLength = Len(Ret_Iva.Text)
End Sub

Private Sub Ret_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ret_Iva_LostFocus()
    Ret_Iva.Text = Formateado(Str(Val(Ret_Iva.Text)), 2, 5, " ", False)
End Sub

Private Sub Rubro_Click()
    If Val(Mid(Rubro.List(Rubro.ListIndex), 30, 5)) > 0 Then
        Cargar_Articulo
    Else
        List1.Clear
    End If
End Sub

Private Sub Rubro_GotFocus()
    Dat_Arti.Refresh
    'Rubro.Text = ""
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: If Trim(Rubro.Text) <> "" Then SendKeys "{TAB}"
End Sub

Private Sub Rubro_LostFocus()
    If Val(Mid(Rubro.Text, 1, 5)) > 0 Then
        qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & Trim(Str(Val(Rubro.Text)))
        AbreRs
        
        If Not Rs.EOF Then
            Rubro.Text = Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Str(Val(Rs.Fields("Id_Rubro")))) & " " & Formateado(Str(Val(Rs.Fields("Margen_Util_Pub_Ctdo"))), 2, 9, " ", False) & " " & Formateado(Str(Val(Rs.Fields("Margen_Util_Rev_Ctdo"))), 2, 9, " ", False) & " " & Formateado(Str(Val(Rs.Fields("Margen_Util_Pub_Cta"))), 2, 9, " ", False) & " " & Formateado(Str(Val(Rs.Fields("Margen_Util_Rev_Cta"))), 2, 9, " ", False)
            Cargar_Articulo
        Else
            Rubro.Text = ""
        End If
    End If
End Sub

Private Sub Salir_Click()
    If Articulo.Text = "" Then
        If Rubro.Text = "" Then
            Unload Me
        Else
            Rubro.Text = ""
            List1.Clear
            Rubro.SetFocus
        End If
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Alicuota.ListIndex = -1
    List1.Clear
    Cargar_Articulo
    Articulo.Text = ""
    'Rubro.Text = ""
    Descripcion.Text = ""
    Fecha.Text = ""
    Presentacion_Pub.Text = ""
    Presentacion_Rev.Text = ""
    Medida_Rev.Text = ""
    Medida_Pub.Text = ""
    Stock_Minimo.Text = ""
    Existencia.Text = ""
    Proveedor.Text = ""
    Cuenta_Compras.Text = ""
    Cuenta_Ventas.Text = ""
    Memo.Text = ""
    Mostrar_IVA.Value = 0
    
    Precio_Compra.Text = ""
    Precio_Ctdo_Pub.Text = ""
    Precio_Ctdo_Rev.Text = ""
    Precio_CtaCte_Pub.Text = ""
    Precio_CtaCte_Rev.Text = ""
    Utilidad_Pub.Text = ""
    Utilidad_Rev.Text = ""
    Precio_Ctdo_Pub.Text = ""
    Precio_Ctdo_Rev.Text = ""
    Rec_CtaCte_Pub.Text = ""
    Rec_CtaCte_Rev.Text = ""
    Precio_CtaCte_Pub.Text = ""
    Precio_CtaCte_Rev.Text = ""
    Precio_Recargo_Rev_Ctdo.Text = ""
    Precio_Recargo_Rev_Cta.Text = ""
    Precio_Recargo_Pub_Ctdo.Text = ""
    Precio_Recargo_Pub_Cta.Text = ""
    Precio_Ctdo_Rev_P2.Text = ""
    Precio_Ctdo_Pub_P2.Text = ""
    Precio_Cta_Rev_P2.Text = ""
    Precio_Cta_Pub_P2.Text = ""
    Ret_IB.Text = ""
    Ret_Iva.Text = ""
    
    Espesor.Text = ""
    Espesor.BackColor = 14737632
    Ancho.Text = ""
    Ancho.BackColor = 14737632
    Largo.Text = ""
    Largo.BackColor = 14737632
    
    Ancho_Menor.Text = ""
    Recargo_Ancho_Menor.Text = ""
    Largo_Menor.Text = ""
    Recargo_Largo_Menor.Text = ""
    Ancho_Mayor.Text = ""
    Recargo_Ancho_Mayor.Text = ""
    Largo_Mayor.Text = ""
    Recargo_Largo_Mayor.Text = ""
    
    Ancho_Menor.BackColor = 14737632
    Recargo_Ancho_Menor.BackColor = 14737632
    Largo_Menor.BackColor = 14737632
    Recargo_Largo_Menor.BackColor = 14737632
    Ancho_Mayor.BackColor = 14737632
    Recargo_Ancho_Mayor.BackColor = 14737632
    Largo_Mayor.BackColor = 14737632
    Recargo_Largo_Mayor.BackColor = 14737632
    
    Memo.Visible = False
    Marco_Articulo.Enabled = True
    Marco_Medidas.Enabled = False
    Marco_Precio.Enabled = False
    Marco_Cuentas.Enabled = False
    Marco_Recargos.Enabled = False
    
    Grabar.Enabled = False
    Borrar.Enabled = False
    Observaciones.Enabled = False
    Articulo.Enabled = True
    Rubro.Enabled = True
    Salir.Cancel = True
    
    'Rubro.SetFocus
    Articulo.SetFocus
End Sub

Private Sub Habilitar_Medidas()
    If Val(Mid(Presentacion_Pub.Text, 1, 1)) = 3 Or Val(Mid(Presentacion_Pub.Text, 1, 1)) = 4 Or Val(Mid(Presentacion_Pub.Text, 1, 1)) = 5 Or Val(Mid(Presentacion_Rev.Text, 1, 1)) = 3 Or Val(Mid(Presentacion_Rev.Text, 1, 1)) = 4 Or Val(Mid(Presentacion_Rev.Text, 1, 1)) = 5 Or Val(Mid(Presentacion_Rev.Text, 1, 1)) = 8 Or Val(Mid(Presentacion_Pub.Text, 1, 1)) = 8 Then
        If Val(Mid(Presentacion_Rev.Text, 1, 1)) = 8 Or Val(Mid(Presentacion_Pub.Text, 1, 1)) = 8 Then
            Espesor.Enabled = False
            Espesor.Text = ""
            Espesor.BackColor = 14737632
            
            Ancho.Enabled = True
            Largo.Enabled = True
            
            Ancho.BackColor = vbWhite
            Largo.BackColor = vbWhite
        Else
        
            Espesor.Enabled = True
            Ancho.Enabled = True
            Largo.Enabled = True
            
            Ancho_Menor.BackColor = vbWhite
            Recargo_Ancho_Menor.BackColor = vbWhite
            Largo_Menor.BackColor = vbWhite
            Recargo_Largo_Menor.BackColor = vbWhite
            Ancho_Mayor.BackColor = vbWhite
            Recargo_Ancho_Mayor.BackColor = vbWhite
            Largo_Mayor.BackColor = vbWhite
            Recargo_Largo_Mayor.BackColor = vbWhite
            
            
            Espesor.BackColor = vbWhite
            Ancho.BackColor = vbWhite
            Largo.BackColor = vbWhite
            
            Marco_Recargos.Enabled = True
        End If
    Else
        Espesor.Text = ""
        Ancho.Text = ""
        Largo.Text = ""
        
        Ancho_Menor.Text = ""
        Recargo_Ancho_Menor.Text = ""
        Largo_Menor.Text = ""
        Recargo_Largo_Menor.Text = ""
        Ancho_Mayor.Text = ""
        Recargo_Ancho_Mayor.Text = ""
        Largo_Mayor.Text = ""
        Recargo_Largo_Mayor.Text = ""
        
        Espesor.Enabled = False
        Ancho.Enabled = False
        Largo.Enabled = False
        
        Ancho_Menor.BackColor = 14737632
        Recargo_Ancho_Menor.BackColor = 14737632
        Largo_Menor.BackColor = 14737632
        Recargo_Largo_Menor.BackColor = 14737632
        Ancho_Mayor.BackColor = 14737632
        Recargo_Ancho_Mayor.BackColor = 14737632
        Largo_Mayor.BackColor = 14737632
        Recargo_Largo_Mayor.BackColor = 14737632

        
        Espesor.BackColor = 14737632
        Ancho.BackColor = 14737632
        Largo.BackColor = 14737632
        
        Marco_Recargos.Enabled = False
       
    End If
End Sub

Private Sub Aderir_Recargos()
    Dim Recargo  As Boolean
    Dim sMedida  As Single
    
    Recargo = False
    
    'ACTUALIZO LOS PRECIOS
       'CTDO
    Precio_Ctdo_Rev.Text = Formateado(Str(Val(Precio_Compra.Text)), 2, 8, " ", False)
    If Val(Utilidad_Rev.Text) > 0 And Val(Precio_Compra.Text) > 0 Then Precio_Ctdo_Rev.Text = Formateado(Str(Val(Val(Precio_Compra.Text) * Val(Utilidad_Rev.Text) / 100) + Val(Precio_Compra.Text)), 2, 8, " ", False)
    
    Precio_Ctdo_Pub.Text = Formateado(Str(Val(Precio_Compra.Text)), 2, 8, " ", False)
    If Val(Utilidad_Pub.Text) > 0 And Val(Precio_Compra.Text) > 0 Then Precio_Ctdo_Pub.Text = Formateado(Str(Val((((Val(Precio_Compra.Text) * Val(Utilidad_Pub.Text)) / 100) + Val(Precio_Compra.Text)))), 2, 8, " ", False)
       'CTA CTE
    Precio_CtaCte_Rev.Text = Formateado(Str(Val(Precio_Ctdo_Rev.Text)), 2, 8, " ", False)
    If Val(Rec_CtaCte_Rev.Text) > 0 And Val(Precio_Ctdo_Rev.Text) > 0 Then Precio_CtaCte_Rev.Text = Formateado(Str(Val((((Val(Precio_Ctdo_Rev.Text) * Val(Rec_CtaCte_Rev.Text)) / 100) + Val(Precio_Ctdo_Rev.Text)))), 2, 8, " ", False)
    
    Precio_CtaCte_Pub.Text = Formateado(Str(Val(Precio_Ctdo_Pub.Text)), 2, 8, " ", False)
    If Val(Rec_CtaCte_Pub.Text) > 0 And Val(Precio_Ctdo_Pub.Text) > 0 Then Precio_CtaCte_Pub.Text = Formateado(Str(Val((((Val(Precio_Ctdo_Pub.Text) * Val(Rec_CtaCte_Pub.Text)) / 100) + Val(Precio_Ctdo_Pub.Text)))), 2, 8, " ", False)
    
    If Recargo = False Then
        Precio_Recargo_Rev_Cta.Text = Val(Precio_CtaCte_Rev.Text)
        Precio_Recargo_Rev_Ctdo.Text = Val(Precio_Ctdo_Rev.Text)
        Precio_Recargo_Pub_Cta.Text = Val(Precio_CtaCte_Pub.Text)
        Precio_Recargo_Pub_Ctdo.Text = Val(Precio_Ctdo_Pub.Text)
        Precio_Ctdo_Rev_P2.Text = Val(Precio_Ctdo_Rev.Text)
        Precio_Cta_Rev_P2.Text = Val(Precio_CtaCte_Rev.Text)
        Precio_Ctdo_Pub_P2.Text = Val(Precio_Ctdo_Pub.Text)
        Precio_Cta_Pub_P2.Text = Val(Precio_CtaCte_Pub.Text)
    End If
        
    'P�BLICOS
    If Val(Precio_Ctdo_Pub.Text) > 0 Then
        If (Val(Mid(Presentacion_Pub.Text, 1, 2)) = 5 Or Val(Mid(Presentacion_Pub.Text, 1, 2)) = 3) Then
            Precio_Recargo_Pub_Ctdo.Text = ((((Val(Espesor.Text) * Val(Ancho.Text)) * 0.2734)) * Val(Largo)) * Val(Precio_Recargo_Pub_Ctdo.Text)
            Precio_Recargo_Pub_Cta.Text = ((((Val(Espesor.Text) * Val(Ancho.Text)) * 0.2734)) * Val(Largo)) * Val(Precio_Recargo_Pub_Cta.Text)
        ElseIf (Val(Mid(Presentacion_Pub.Text, 1, 2)) = 4) Then
             Precio_Recargo_Pub_Ctdo.Text = (CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)) * Val(Precio_Recargo_Pub_Ctdo)
             Precio_Recargo_Pub_Cta.Text = (CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)) * Val(Precio_Recargo_Pub_Cta)
        End If
    End If
    Precio_Recargo_Pub_Ctdo.Text = Formateado(Str(Val(Precio_Recargo_Pub_Ctdo.Text)), 2, 7, " ", False)
    Precio_Recargo_Pub_Cta.Text = Formateado(Str(Val(Precio_Recargo_Pub_Cta.Text)), 2, 7, " ", False)
   '
    'REVENDEDORES
    If Val(Precio_Ctdo_Rev.Text) > 0 Then 'ML
        If (Val(Presentacion_Rev.Text) = 5 Or Val(Presentacion_Rev.Text) = 3) Then
            Precio_Recargo_Rev_Ctdo.Text = ((((Espesor.Text) * Val(Ancho.Text)) * 0.2734) * Val(Precio_Recargo_Rev_Ctdo)) * Val(Largo.Text)
            Precio_Recargo_Rev_Cta.Text = ((((Espesor.Text) * Val(Ancho.Text)) * 0.2734) * Val(Precio_Recargo_Rev_Cta)) * Val(Largo.Text)
        ElseIf (Val(Presentacion_Rev.Text) = 4) Then
             Precio_Recargo_Rev_Ctdo.Text = (CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)) * Val(Precio_Recargo_Rev_Ctdo)
             Precio_Recargo_Rev_Cta.Text = (CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)) * Val(Precio_Recargo_Rev_Cta)
        End If
    End If
    
    Precio_Recargo_Rev_Ctdo.Text = Formateado(Str(Val(Precio_Recargo_Rev_Ctdo.Text)), 2, 7, " ", False)
    Precio_Recargo_Rev_Cta.Text = Formateado(Str(Val(Precio_Recargo_Rev_Cta.Text)), 2, 7, " ", False)
    
    Precio_Ctdo_Rev_P2.Text = Formateado(Str(Val(Precio_Ctdo_Rev_P2.Text)), 2, 7, " ", False)
    Precio_Ctdo_Pub_P2.Text = Formateado(Str(Val(Precio_Ctdo_Pub_P2.Text)), 2, 7, " ", False)
    Precio_Cta_Rev_P2.Text = Formateado(Str(Val(Precio_Cta_Rev_P2.Text)), 2, 7, " ", False)
    Precio_Cta_Pub_P2.Text = Formateado(Str(Val(Precio_Cta_Pub_P2.Text)), 2, 7, " ", False)
End Sub

Private Sub Calcular_Medida_Presentacion()
    If Trim(Str(Val(Presentacion_Pub.List(Presentacion_Pub.ListIndex)))) = 3 Or Trim(Str(Val(Presentacion_Pub.List(Presentacion_Pub.ListIndex)))) = 4 Or Trim(Str(Val(Presentacion_Pub.List(Presentacion_Pub.ListIndex)))) = 5 Or Trim(Str(Val(Presentacion_Pub.List(Presentacion_Pub.ListIndex)))) = 8 Or Trim(Str(Val(Presentacion_Rev.List(Presentacion_Rev.ListIndex)))) = 3 Or Trim(Str(Val(Presentacion_Rev.List(Presentacion_Rev.ListIndex)))) = 4 Or Trim(Str(Val(Presentacion_Rev.List(Presentacion_Rev.ListIndex)))) = 5 Or Trim(Str(Val(Presentacion_Rev.List(Presentacion_Rev.ListIndex)))) = 8 Then
        If Val(Espesor.Text) = 0 And Espesor.Enabled = True Then
            Espesor.Text = ""
            Espesor.SetFocus
            
            Exit Sub
        End If
        
        If Val(Mid(Presentacion_Pub.Text, 1, 1)) = 3 Then
            Medida_Pub.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
        ElseIf Val(Mid(Presentacion_Pub.Text, 1, 1)) = 4 Then
            Medida_Pub.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
        ElseIf Val(Mid(Presentacion_Pub.Text, 1, 1)) = 5 Then
            Medida_Pub.Text = Formateado(Str(Val(Largo.Text)), 2, 9, " ", False)
        ElseIf Val(Mid(Presentacion_Pub.Text, 1, 1)) = 8 Then
            Medida_Pub.Text = Formateado(Str(Val((Val(Ancho.Text)) * Val(Largo.Text))), 2, 9, " ", False)
        End If
        
        If Val(Mid(Presentacion_Rev.Text, 1, 1)) = 3 Then
            Medida_Rev.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
        ElseIf Val(Mid(Presentacion_Rev.Text, 1, 1)) = 4 Then
            Medida_Rev.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
        ElseIf Val(Mid(Presentacion_Rev.Text, 1, 1)) = 5 Then
            Medida_Rev.Text = Formateado(Str(Val(Largo.Text)), 2, 9, " ", False)
        ElseIf Val(Mid(Presentacion_Rev.Text, 1, 1)) = 8 Then
            Medida_Rev.Text = Formateado(Str(Val((Val(Ancho.Text)) * Val(Largo.Text))), 2, 9, " ", False)
        End If
    End If
End Sub

Private Sub Recargo_Ancho_Mayor_GotFocus()
    Recargo_Ancho_Mayor.Text = Trim(Recargo_Ancho_Mayor.Text)
    Recargo_Ancho_Mayor.SelStart = 0
    Recargo_Ancho_Mayor.SelText = ""
    Recargo_Ancho_Mayor.SelLength = Len(Recargo_Ancho_Mayor.Text)
End Sub

Private Sub Recargo_Ancho_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Ancho_Mayor_LostFocus()
    Recargo_Ancho_Mayor.Text = Formateado(Str(Val(Recargo_Ancho_Mayor.Text)), 2, 6, " ", False)
    Aderir_Recargos
End Sub

Private Sub Recargo_Ancho_Menor_GotFocus()
    Recargo_Ancho_Menor.Text = Trim(Recargo_Ancho_Menor.Text)
    Recargo_Ancho_Menor.SelStart = 0
    Recargo_Ancho_Menor.SelText = ""
    Recargo_Ancho_Menor.SelLength = Len(Recargo_Ancho_Menor.Text)
End Sub

Private Sub Recargo_Ancho_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Ancho_Menor_LostFocus()
    Recargo_Ancho_Menor.Text = Formateado(Str(Val(Recargo_Ancho_Menor.Text)), 2, 6, " ", False)
    Aderir_Recargos
End Sub

Private Sub Recargo_Largo_Mayor_GotFocus()
    Recargo_Largo_Mayor.Text = Trim(Recargo_Largo_Mayor.Text)
    Recargo_Largo_Mayor.SelStart = 0
    Recargo_Largo_Mayor.SelText = ""
    Recargo_Largo_Mayor.SelLength = Len(Recargo_Largo_Mayor.Text)
End Sub

Private Sub Recargo_Largo_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Largo_Mayor_LostFocus()
    Recargo_Largo_Mayor.Text = Formateado(Str(Val(Recargo_Largo_Mayor.Text)), 2, 6, " ", False)
    Aderir_Recargos
End Sub

Private Sub Recargo_Largo_Menor_GotFocus()
    Recargo_Largo_Menor.Text = Trim(Recargo_Largo_Menor.Text)
    Recargo_Largo_Menor.SelStart = 0
    Recargo_Largo_Menor.SelText = ""
    Recargo_Largo_Menor.SelLength = Len(Recargo_Largo_Menor.Text)
End Sub

Private Sub Recargo_Largo_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Recargo_Largo_Menor_LostFocus()
    Recargo_Largo_Menor.Text = Formateado(Str(Val(Recargo_Largo_Menor.Text)), 2, 6, " ", False)
    Aderir_Recargos
End Sub

Private Sub Largo_Mayor_GotFocus()
    Largo_Mayor.Text = Trim(Largo_Mayor.Text)
    Largo_Mayor.SelStart = 0
    Largo_Mayor.SelText = ""
    Largo_Mayor.SelLength = Len(Largo_Mayor.Text)
End Sub

Private Sub Largo_Mayor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_Mayor_LostFocus()
    Largo_Mayor.Text = Formateado(Str(Val(Largo_Mayor.Text)), 2, 8, " ", False)
    Aderir_Recargos
End Sub

Private Sub Largo_Menor_GotFocus()
    Largo_Menor.Text = Trim(Largo_Menor.Text)
    Largo_Menor.SelStart = 0
    Largo_Menor.SelText = ""
    Largo_Menor.SelLength = Len(Largo_Menor.Text)
End Sub

Private Sub Largo_Menor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_Menor_LostFocus()
    Largo_Menor.Text = Formateado(Str(Val(Largo_Menor.Text)), 2, 8, " ", False)
    Aderir_Recargos
End Sub

Private Sub Stock_Minimo_GotFocus()
    Stock_Minimo.Text = Trim(Stock_Minimo.Text)
    
    Stock_Minimo.SelStart = 0
    Stock_Minimo.SelLength = Len(Stock_Minimo.Text)
End Sub

Private Sub Stock_Minimo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Stock_Minimo_LostFocus()
    Stock_Minimo.Text = Formateado(Str(Val(Stock_Minimo.Text)), 2, 10, " ", False)
End Sub

Private Sub Utilidad_Rev_GotFocus()
    If Val(Utilidad_Rev.Text) = 0 Then Utilidad_Rev.Text = Formateado(Str(Val(Mid(Rubro.Text, 48, 9))), 2, 5, " ", False)
    Utilidad_Rev.Text = Trim(Utilidad_Rev.Text)
    
    Utilidad_Rev.SelStart = 0
    Utilidad_Rev.SelLength = Len(Utilidad_Rev.Text)
End Sub

Private Sub Utilidad_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Utilidad_Rev_LostFocus()
    Utilidad_Rev.Text = Formateado(Str(Val(Utilidad_Rev.Text)), 2, 5, " ", False)
    
    Aderir_Recargos
End Sub

Private Sub Utilidad_Pub_GotFocus()
    If Val(Utilidad_Pub.Text) = 0 Then Utilidad_Pub.Text = Formateado(Str(Val(Mid(Rubro.Text, 38, 9))), 2, 5, " ", False)
    Utilidad_Pub.Text = Trim(Utilidad_Pub.Text)
    
    Utilidad_Pub.SelStart = 0
    Utilidad_Pub.SelLength = Len(Utilidad_Pub.Text)
End Sub

Private Sub Utilidad_Pub_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Utilidad_Pub_LostFocus()
    Utilidad_Pub.Text = Formateado(Str(Val(Utilidad_Pub.Text)), 2, 5, " ", False)
    
    Aderir_Recargos
End Sub

Public Function CargarDatos(idArticulo As Long, idRubro As Long)
    
    Rubro.Text = idRubro
    Articulo.Text = idArticulo
    
    Rubro_LostFocus
    
    Leer_Articulo

End Function
