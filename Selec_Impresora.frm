VERSION 5.00
Begin VB.Form Selec_Impresora 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Seleccione el Dispositivo por el Cual desea Imprimir.-"
   ClientHeight    =   3135
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3135
   ScaleWidth      =   5415
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Device 
      Height          =   2175
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5415
      Begin VB.ComboBox Impresoras_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   2
         Top             =   480
         Width           =   5175
      End
      Begin VB.Label Label1 
         Caption         =   "Impresora:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   5175
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   1
      Top             =   2160
      Width           =   5415
      Begin VB.CommandButton Salir 
         Height          =   615
         Left            =   4320
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "Selec_Impresora"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Impresoras_Encontradas.Text = Printer.DeviceName
End Sub
