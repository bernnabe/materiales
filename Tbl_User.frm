VERSION 5.00
Begin VB.Form Tbl_User 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Administrador de Usuarios.-"
   ClientHeight    =   4590
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5775
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4590
   ScaleWidth      =   5775
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Datos 
      Height          =   3615
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5775
      Begin VB.TextBox Clave_Anterior 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1320
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   6
         Top             =   1680
         Width           =   4335
      End
      Begin VB.CheckBox Nivel_4 
         Caption         =   "Administraci�n del Sistema"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3480
         TabIndex        =   12
         Top             =   3240
         Width           =   2175
      End
      Begin VB.CheckBox Nivel_3 
         Caption         =   "Modificaciones"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3480
         TabIndex        =   11
         Top             =   3000
         Width           =   1455
      End
      Begin VB.CheckBox Nivel_2 
         Caption         =   "Consultas"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1320
         TabIndex        =   10
         Top             =   3240
         Width           =   1095
      End
      Begin VB.CheckBox Nivel_1 
         Caption         =   "Alta - Bajas"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1320
         TabIndex        =   9
         Top             =   3000
         Width           =   1095
      End
      Begin VB.TextBox Nueva_Clave 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1320
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   7
         Top             =   2040
         Width           =   4335
      End
      Begin VB.TextBox Repetir_Clave 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1320
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   8
         Top             =   2400
         Width           =   4335
      End
      Begin VB.TextBox Clave 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   1320
         MaxLength       =   30
         PasswordChar    =   "*"
         TabIndex        =   5
         Top             =   1200
         Width           =   4335
      End
      Begin VB.TextBox Nombre 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   30
         TabIndex        =   4
         Top             =   600
         Width           =   4335
      End
      Begin VB.CommandButton Buscar_Usuario 
         Caption         =   "Usuario:"
         Height          =   255
         Left            =   240
         TabIndex        =   3
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Usuario 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   5
         TabIndex        =   2
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Clave Anterior:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Permisos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   3000
         Width           =   1095
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Nueva Clave:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Repetir Clave:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   2400
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Clave:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   1200
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   600
         Width           =   1095
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   1
      Top             =   3600
      Width           =   5775
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   4680
         Picture         =   "Tbl_User.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   1080
         Picture         =   "Tbl_User.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Tbl_User.frx":C514
         Style           =   1  'Graphical
         TabIndex        =   17
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "Tbl_User"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Borrar_Click()
    If MsgBox("Est� seguro de borrar �ste usuario. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
        qy = "DELETE FROM Usuario WHERE Id_Usuario = " & Trim(Str(Val(Usuario.Text)))
        Db.Execute (qy)
    End If
    
    Salir_Click
End Sub

Private Sub Clave_Anterior_GotFocus()
    Clave_Anterior.SelStart = 0
    Clave_Anterior.SelText = ""
    Clave_Anterior.SelLength = Len(Clave_Anterior.Text)
End Sub

Private Sub Clave_Anterior_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Clave_GotFocus()
    Clave.Text = Trim(Clave.Text)
    Clave.SelStart = 0
    Clave.SelText = ""
    Clave.SelLength = Len(Clave.Text)
End Sub

Private Sub Clave_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 3
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    'Qy = "SELECT * FROM Usuario WHERE Id_Usuario = " & trim(str(Val(Numero_Usuario)))
    'AbreRs
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Clave.Text = Repetir_Clave.Text Then
        If Grabar.Tag = "Alta" Then
            
            qy = "INSERT INTO Usuario VALUES ("
            qy = qy & Trim(Str(Val(Usuario.Text)))
            qy = qy & ", '" & Trim(Nombre.Text) & "'"
            qy = qy & ", '" & Clave.Text & "'"
            qy = qy & ", " & Val(Nivel_1.Value)
            qy = qy & ", " & Val(Nivel_2.Value)
            qy = qy & ", " & Val(Nivel_3.Value)
            qy = qy & ", " & Val(Nivel_4.Value) & ")"
            Db.Execute (qy)
        Else
            qy = "UPDATE Usuario SET "
            qy = qy & "Nombre = '" & Trim(Nombre.Text) & "', "
            If Nueva_Clave.Text <> "" And Trim(UCase(Nombre.Text)) <> "Administrador" Then
                qy = qy & "Clave = '" & Nueva_Clave.Text & "', "
            Else
                qy = qy & "Clave = '" & Clave.Text & "', "
            End If
            qy = qy & "Alta_Baja = " & Val(Nivel_1.Value) & ", "
            qy = qy & "Consulta = " & Val(Nivel_2.Value) & ", "
            qy = qy & "Modificacion = " & Val(Nivel_3.Value) & ", "
            qy = qy & "Administracion = " & Val(Nivel_4.Value) & " "
            qy = qy & "WHERE Id_Usuario = " & Trim(Str(Val(Usuario.Text)))
            Db.Execute (qy)
        End If
        
        Salir_Click
    Else
        MsgBox "La Clave ingresada contiene errores, verfique.!", vbCritical, "Atenci�n.!"
        Clave.SetFocus
    End If
End Sub

Private Sub Nivel_1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nivel_2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nivel_3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nivel_4_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_GotFocus()
    Nombre.Text = Trim(Nombre.Text)
    Nombre.SelStart = 0
    Nombre.SelText = ""
    Nombre.SelLength = Len(Nombre.Text)
End Sub

Private Sub Nombre_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_LostFocus()
    Nombre.Text = FiltroCaracter(Nombre.Text)
End Sub

Private Sub Nueva_Clave_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Repetir_Clave_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If Usuario.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Usuario_GotFocus()
    Usuario.Text = Trim(Usuario.Text)
    Usuario.SelStart = 0
    Usuario.SelText = ""
    Usuario.SelLength = Len(Usuario.Text)
End Sub

Private Sub Usuario_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: If Val(Usuario.Text) > 0 Then SendKeys "{TAB}"
End Sub

Private Sub Usuario_LostFocus()
    If Val(Usuario.Text) > 0 Then
        Leer_Usuario
    End If
End Sub

Private Sub Leer_Usuario()
    If Val(Usuario.Text) > 0 Then
        qy = "SELECT * FROM Usuario WHERE Id_Usuario = " & Trim(Str(Val(Usuario.Text)))
        AbreRs
        
        If Rs.EOF Then
            If MsgBox("El Usuario es Inexistente, lo incorpora ahora. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
                Grabar.Tag = "Alta"
                Clave.Enabled = True
                Repetir_Clave.Enabled = True
                Clave_Anterior.Enabled = False
                Grabar.Enabled = True
                
                Nombre.SetFocus
            Else
                Borrar_Campo
            End If
        Else
            Mostrar_Usuario
            If Val(Usuario.Text) = 1 And Trim(UCase(Nombre.Text)) = "ADMINISTRADOR" Then
                Nombre.Enabled = False
                Borrar.Enabled = False
                Nivel_1.Enabled = False
                Nivel_2.Enabled = False
                Nivel_3.Enabled = False
                Nivel_4.Enabled = False
                MsgBox "El Usuario 'Administrador' es un usuario de Sistema, por lo tanto las modificaciones est�n restringidas.-", vbInformation, "Atenci�n.!"
            Else
                Nombre.Enabled = True
                Borrar.Enabled = True
                Nivel_1.Enabled = True
                Nivel_2.Enabled = True
                Nivel_3.Enabled = True
                Nivel_4.Enabled = True
            End If
            
            Grabar.Enabled = True
            Nueva_Clave.Enabled = True
            If Nombre.Enabled = True Then
                Nombre.SetFocus
            Else
                'NADA
            End If
        End If
    Else
        Usuario.Text = ""
        Usuario.SetFocus
    End If
End Sub

Private Sub Mostrar_Usuario()
    Dim i As Long
    Nombre.Text = Trim(Rs.Fields("Nombre"))
    Clave.Text = Trim(Rs.Fields("Clave"))
    Repetir_Clave.Text = Clave.Text
    
    Nivel_1.Value = Val(Rs.Fields("Alta_Baja"))
    Nivel_2.Value = Val(Rs.Fields("Consulta"))
    Nivel_3.Value = Val(Rs.Fields("Modificacion"))
    Nivel_4.Value = Val(Rs.Fields("Administracion"))
End Sub

Private Sub Borrar_Campo()
    Usuario.Text = ""
    Nombre.Text = ""
    Nombre.Enabled = True
    Clave.Text = ""
    Clave_Anterior.Text = ""
    Repetir_Clave.Text = ""
    Nueva_Clave.Text = ""
    
    Grabar.Enabled = False
    Borrar.Enabled = False
    Nivel_1.Enabled = True
    Nivel_2.Enabled = True
    Nivel_3.Enabled = True
    Nivel_4.Enabled = True
    Clave.Enabled = False
    Repetir_Clave.Enabled = False
    Nueva_Clave.Enabled = False
    
    Nivel_1.Value = 0
    Nivel_2.Value = 0
    Nivel_3.Value = 0
    Nivel_4.Value = 0
    
    Usuario.SetFocus
End Sub
