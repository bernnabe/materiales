VERSION 5.00
Begin VB.Form Ayuda_Indice 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "�nidice de Ayuda.-"
   ClientHeight    =   6390
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6390
   ScaleWidth      =   8415
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Busqueda 
      Height          =   3375
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   8415
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Caption         =   "&Salir"
         Height          =   375
         Left            =   6720
         TabIndex        =   8
         Top             =   1560
         Width           =   1575
      End
      Begin VB.CommandButton Cancelar 
         Caption         =   "&Cancelar"
         Height          =   375
         Left            =   6720
         TabIndex        =   7
         Top             =   960
         Width           =   1575
      End
      Begin VB.CommandButton Command1 
         Caption         =   "&Aceptar"
         Height          =   375
         Left            =   6720
         TabIndex        =   6
         Top             =   360
         Width           =   1575
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1230
         Left            =   120
         TabIndex        =   4
         Top             =   2040
         Width           =   8175
      End
      Begin VB.ComboBox Modulo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         TabIndex        =   3
         Top             =   1560
         Width           =   6375
      End
      Begin VB.ComboBox Temas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   120
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   960
         Width           =   6375
      End
      Begin VB.Label Label2 
         Caption         =   "Seleccione el M�dulo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1320
         Width           =   4815
      End
      Begin VB.Label Label1 
         Caption         =   "Seleccione el Tema a Buscar:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   720
         Width           =   4815
      End
   End
   Begin VB.Frame Encontrado 
      Height          =   3015
      Left            =   0
      TabIndex        =   1
      Top             =   3360
      Width           =   8415
      Begin VB.TextBox Texto 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2655
         Left            =   120
         Locked          =   -1  'True
         MultiLine       =   -1  'True
         ScrollBars      =   2  'Vertical
         TabIndex        =   5
         Top             =   240
         Width           =   8175
      End
   End
End
Attribute VB_Name = "Ayuda_Indice"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Cancelar_Click()
    List1.Clear
    Modulo.Text = ""
    Temas.ListIndex = 0
    Texto.Text = ""
    Temas.SetFocus
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    
    qy = "SELECT * FROM Ayuda_Tema ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Temas.AddItem Formateado(Str(Val(Rs.Fields("Id_Tema"))), 0, 5, " ", False) & " - " & Trim(Rs.Fields("Denominacion"))
        Rs.MoveNext
    Wend
End Sub

Private Sub List1_DblClick()
    Mostrar_Ayuda
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Mostrar_Ayuda
End Sub

Private Sub Mostrar_Ayuda()
    If Val(Mid(List1.List(List1.ListIndex), 1, 5)) > 0 Then
        qy = "SELECT * FROM Ayuda_Accion WHERE Id_Tema = " & Trim(Str(Val(Temas.Text))) & " "
        qy = qy & "AND Id_Modulo = " & Trim(Str(Val(Modulo.Text))) & " "
        qy = qy & "AND Id_Accion = " & Trim(Str(Val(Mid(List1.List(List1.ListIndex), 1, 5))))
        AbreRs
        
        If Not Rs.EOF Then
            If Not IsNull("Texto_Ayuda") = True Then Texto.Text = Rs.Fields("Texto_Ayuda")
        Else
            Texto.Text = "AYUDA NO DISPONIBLE..."
        End If
    End If
End Sub

Private Sub Modulo_Click()
    Modulo_LostFocus
End Sub

Private Sub Modulo_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
    Texto.Text = ""
End Sub

Private Sub Modulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Modulo_LostFocus()
    List1.Clear
    
    If Val(Modulo.Text) > 0 And Val(Temas.Text) > 0 Then
        qy = "SELECT * FROM Ayuda_Accion WHERE Id_Tema = " & Trim(Str(Val(Mid(Temas.Text, 1, 5)))) & " "
        qy = qy & "AND Id_Modulo = " & Trim(Str(Val(Mid(Modulo.Text, 1, 5))))
        AbreRs
        
        While Not Rs.EOF
            List1.AddItem Formateado(Str(Val(Rs.Fields("Id_Accion"))), 0, 5, " - ", False) & " - " & Trim(Rs.Fields("Denominacion"))
            Rs.MoveNext
        Wend
    End If
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub

Private Sub Temas_GotFocus()
    Ayuda_Indice.Refresh
    If Desplegar_Combos = True Then SendKeys "{F4}"
    Texto.Text = ""
End Sub

Private Sub Temas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Temas_LostFocus()
    Modulo.Clear
    If Val(Temas.Text) > 0 Then
        qy = "SELECT * FROM Ayuda_Modulo WHERE Id_Tema = " & Trim(Str(Val(Mid(Temas.Text, 1, 5)))) & " "
        qy = qy & "ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Modulo.AddItem Formateado(Str(Val(Rs.Fields("Id_Modulo"))), 0, 5, " ", False) & " - " & Trim(Rs.Fields("Denominacion"))
            Rs.MoveNext
        Wend
    End If
End Sub

