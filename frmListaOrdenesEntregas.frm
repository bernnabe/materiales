VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form frmListaOrdenesEntregas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listado de ordenes de Entrega"
   ClientHeight    =   7410
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10635
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7410
   ScaleWidth      =   10635
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton btnImprimir 
      Height          =   615
      Left            =   75
      Picture         =   "frmListaOrdenesEntregas.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   17
      ToolTipText     =   "Imprimir el Plan de Cuentas.-"
      Top             =   6675
      Width           =   1095
   End
   Begin VB.CommandButton btnCancelar 
      Cancel          =   -1  'True
      Height          =   615
      Left            =   9450
      Picture         =   "frmListaOrdenesEntregas.frx":5C12
      Style           =   1  'Graphical
      TabIndex        =   16
      ToolTipText     =   "Salir del m�dulo o cancelar.-"
      Top             =   6675
      Width           =   1140
   End
   Begin VB.Frame Frame1 
      Height          =   1665
      Left            =   75
      TabIndex        =   8
      Top             =   0
      Width           =   10515
      Begin VB.CommandButton btnBuscar 
         Height          =   615
         Left            =   9225
         Picture         =   "frmListaOrdenesEntregas.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   15
         Top             =   225
         Width           =   1095
      End
      Begin MSComCtl2.DTPicker dtDesde 
         Height          =   315
         Left            =   150
         TabIndex        =   0
         Top             =   450
         Width           =   1440
         _ExtentX        =   2540
         _ExtentY        =   556
         _Version        =   393216
         Format          =   16646145
         CurrentDate     =   40712
      End
      Begin VB.OptionButton optPresupuestadas 
         Caption         =   "Presupuestadas"
         Height          =   240
         Left            =   4125
         TabIndex        =   6
         Top             =   1125
         Width           =   1740
      End
      Begin VB.OptionButton optFacturadas 
         Caption         =   "Facturadas"
         Height          =   240
         Left            =   4125
         TabIndex        =   5
         Top             =   900
         Width           =   1740
      End
      Begin VB.OptionButton optPendientes 
         Caption         =   "Pendientes"
         Height          =   240
         Left            =   4125
         TabIndex        =   4
         Top             =   675
         Width           =   1440
      End
      Begin VB.OptionButton optTodas 
         Caption         =   "Todas"
         Height          =   240
         Left            =   4125
         TabIndex        =   3
         Top             =   450
         Value           =   -1  'True
         Width           =   840
      End
      Begin VB.ComboBox cboClientes 
         Height          =   315
         Left            =   150
         Style           =   2  'Dropdown List
         TabIndex        =   2
         Top             =   1050
         Width           =   3840
      End
      Begin MSComCtl2.DTPicker dtHasta 
         Height          =   315
         Left            =   2625
         TabIndex        =   1
         Top             =   450
         Width           =   1365
         _ExtentX        =   2408
         _ExtentY        =   556
         _Version        =   393216
         Format          =   16646145
         CurrentDate     =   40712
      End
      Begin VB.Label Label2 
         Caption         =   "Hasta"
         Height          =   240
         Index           =   1
         Left            =   2625
         TabIndex        =   12
         Top             =   225
         Width           =   1065
      End
      Begin VB.Label Label2 
         Caption         =   "Desde"
         Height          =   240
         Index           =   0
         Left            =   150
         TabIndex        =   11
         Top             =   225
         Width           =   1065
      End
      Begin VB.Label Label1 
         Caption         =   "Cliente"
         Height          =   240
         Index           =   0
         Left            =   150
         TabIndex        =   10
         Top             =   825
         Width           =   915
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4965
      Left            =   75
      TabIndex        =   9
      Top             =   1650
      Width           =   10515
      Begin VB.TextBox txtSumaValores 
         Alignment       =   1  'Right Justify
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8925
         Locked          =   -1  'True
         TabIndex        =   13
         Text            =   "0.00"
         Top             =   4575
         Width           =   1515
      End
      Begin MSComctlLib.ListView lvwLista 
         Height          =   4305
         Left            =   75
         TabIndex        =   7
         Top             =   240
         Width           =   10365
         _ExtentX        =   18283
         _ExtentY        =   7594
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         AllowReorder    =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.Label Label3 
         Caption         =   "Suma de valores:"
         Height          =   240
         Left            =   7500
         TabIndex        =   14
         Top             =   4650
         Width           =   1365
      End
   End
End
Attribute VB_Name = "frmListaOrdenesEntregas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const mconstColFecha = 1
Private Const mconstColCliente = 2
Private Const mconstColCuit = 3
Private Const mconstColLineas = 4
Private Const mconstColImporte = 5
Private Const mconstColEstado = 6

Private Sub CrearLista()

    Dim ctmX As ColumnHeader
    
    lvwLista.ColumnHeaders.Clear
    
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "N�mero", 1000)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Fecha", 1200)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Cliente", 2900)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Cuit", 1300)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Lineas", 500)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Importe", 1200, 1)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Estado", 1900)
    
    Set ctmX = Nothing
    
End Sub

Private Sub btnBuscar_Click()

    MousePointer = 11
    
    CargarDatos
    
    MousePointer = 0

End Sub

Private Sub btnCancelar_Click()
    
    If (lvwLista.ListItems.Count = 0) Then
    
        Unload Me
    
    Else
    
        LimpiarCampos
    
    End If

End Sub

Private Sub LimpiarCampos()

    optTodas.Value = True
    lvwLista.ListItems.Clear
    txtSumaValores.Text = Empty
    
    optTodas.SetFocus
    
End Sub

Private Sub btnImprimir_Click()
    MsgBox "Funcion no disponible momentaneamente...", vbCritical, "Atenci�n"
End Sub

Private Sub Form_Load()

    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2

    dtDesde.Value = GetDateFromServer()
    dtHasta.Value = GetDateFromServer()
    CrearLista
    CargarCombos

End Sub

Private Sub CargarDatos()

    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String
    Dim Item As ListItem
    Dim keyLista As String
    
    strSql = "Select oe.Id_Nro_ORden_Entrega, oe.Fecha, oe.Cliente, oe.Total_Factura, oe.Estado, c.Nombre, c.Nro as Cuit, c.Id_Cliente "
    strSql = strSql & "From Orden_Entrega oe "
    strSql = strSql & "Inner join Cliente c On oe.Cliente = c.Id_Cliente "
    strSql = strSql & "Where Fecha BetWeen '" & Format(dtDesde.Value, "dd/MM/yyyy 00:00") & "' And '" & Format(dtHasta.Value, "dd/MM/yyyy 23:59") & "' "
    If (cboClientes.ListIndex <> -1) Then
        strSql = strSql & "And c.Id_Cliente = " & cboClientes.ItemData(cboClientes.ListIndex)
    End If
    If optFacturadas.Value Then
        strSql = strSql & "And oe.Estado = " & ComprobanteEstados.Facturado
    ElseIf optPendientes.Value Then
        strSql = strSql & "And oe.Estado = " & ComprobanteEstados.Pendiente
    ElseIf optPresupuestadas.Value Then
        strSql = strSql & "And oe.Estado = " & ComprobanteEstados.Presupuestado
    End If
    strSql = strSql & " AND Empresa = " & Id_Empresa & " "
    strSql = strSql & "Group by oe.Id_Nro_ORden_Entrega , oe.Fecha, oe.Cliente, oe.Total_Factura, oe.Estado, C.Nombre, c.Nro, c.Id_Cliente "
    strSql = strSql & "Order by Fecha Desc "
    
    rstDatos.Open strSql, Db
    
    lvwLista.ListItems.Clear
    
    If Not (rstDatos.EOF) Then
    
        txtSumaValores.Text = Format(ObtenerSumaValores(rstDatos), "0.00")
        
        While Not rstDatos.EOF
        
            keyLista = "OE" & rstDatos("Id_Nro_Orden_Entrega")
            
            Set Item = lvwLista.ListItems.Add(, keyLista, rstDatos("Id_Nro_Orden_Entrega"))
             
            With Item
            
                .SubItems(mconstColFecha) = Format(rstDatos("Fecha"), "dd/MM/yyyy")
                .SubItems(mconstColCliente) = rstDatos("Id_Cliente") & " - " & rstDatos("Nombre")
                .SubItems(mconstColCuit) = rstDatos("Cuit")
                .SubItems(mconstColLineas) = "1"
                .SubItems(mconstColImporte) = Format(rstDatos("Total_Factura"), "0.00")
                .SubItems(mconstColEstado) = ComprobanteEstados.GetDescripcionEstadoByCodigo(CInt(rstDatos("Estado")))
            
            End With
        
            Select Case rstDatos("Estado")
            
                Case ComprobanteEstados.Pendiente
                    Item.ForeColor = vbRed
                    Item.ListSubItems.Item(1).ForeColor = vbRed
                    Item.ListSubItems.Item(2).ForeColor = vbRed
                    Item.ListSubItems.Item(3).ForeColor = vbRed
                    Item.ListSubItems.Item(4).ForeColor = vbRed
                    Item.ListSubItems.Item(5).ForeColor = vbRed
                    Item.ListSubItems.Item(6).ForeColor = vbRed
                Case ComprobanteEstados.Facturado
                    Item.ForeColor = vbBlue
                    Item.ListSubItems.Item(1).ForeColor = vbBlue
                    Item.ListSubItems.Item(2).ForeColor = vbBlue
                    Item.ListSubItems.Item(3).ForeColor = vbBlue
                    Item.ListSubItems.Item(4).ForeColor = vbBlue
                    Item.ListSubItems.Item(5).ForeColor = vbBlue
                    Item.ListSubItems.Item(6).ForeColor = vbBlue
                Case ComprobanteEstados.Presupuestado
                    Item.ForeColor = vbGreen
                    Item.ListSubItems.Item(1).ForeColor = vbGreen
                    Item.ListSubItems.Item(2).ForeColor = vbGreen
                    Item.ListSubItems.Item(3).ForeColor = vbGreen
                    Item.ListSubItems.Item(4).ForeColor = vbGreen
                    Item.ListSubItems.Item(5).ForeColor = vbGreen
                    Item.ListSubItems.Item(6).ForeColor = vbGreen
                    
            End Select
        
            rstDatos.MoveNext
        
        Wend
        
    End If
    
    Set rstDatos = Nothing
    
End Sub

Private Function ObtenerSumaValores(ByVal rstDatos As ADODB.Recordset) As Double

    Dim dblResult As Double

    While Not rstDatos.EOF
    
        dblResult = dblResult + rstDatos("Total_factura")
        rstDatos.MoveNext
    
    Wend
    
    rstDatos.MoveFirst
    ObtenerSumaValores = dblResult

End Function

Private Sub CargarCombos()

    Dim rstClientes As New ADODB.Recordset
    Dim objCliente As New clsCliente
    
    Set rstClientes = objCliente.GetAll()
    
    While Not rstClientes.EOF
    
        cboClientes.AddItem rstClientes("Nombre")
        cboClientes.ItemData(cboClientes.NewIndex) = rstClientes("Id_Cliente")
        
        rstClientes.MoveNext
    
    Wend
    
    Set rstClientes = Nothing
    Set rstClientes = Nothing
    
End Sub

