VERSION 5.00
Begin VB.Form Dat_Cheques 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaci�n de Cheques.-"
   ClientHeight    =   4455
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5775
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4455
   ScaleWidth      =   5775
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Comprobante 
      Caption         =   "Comprobante Asociado:"
      Enabled         =   0   'False
      Height          =   855
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   5775
      Begin VB.TextBox Tipo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3600
         MaxLength       =   2
         TabIndex        =   4
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox Letra 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3960
         MaxLength       =   1
         TabIndex        =   5
         Top             =   360
         Width           =   375
      End
      Begin VB.TextBox Nro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         MaxLength       =   10
         TabIndex        =   3
         Top             =   360
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Comprobante:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   6
         Top             =   360
         Width           =   3255
      End
   End
   Begin VB.Frame Marco_Cheque 
      Height          =   2655
      Left            =   0
      TabIndex        =   1
      Top             =   840
      Width           =   5775
      Begin VB.CheckBox Nuestra_Firma 
         Caption         =   "&Cheque de nuestra firma."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1320
         TabIndex        =   15
         Top             =   1320
         Width           =   4335
      End
      Begin VB.ComboBox Cheques_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   28
         Top             =   240
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.CommandButton Buscar_Cheques 
         Caption         =   "Nro. Cheque:"
         Height          =   255
         Left            =   120
         TabIndex        =   27
         Top             =   240
         Width           =   1095
      End
      Begin VB.TextBox Importe 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         MaxLength       =   10
         TabIndex        =   22
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Situacion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   1
         TabIndex        =   17
         Top             =   2040
         Width           =   495
      End
      Begin VB.ComboBox Banco 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Dat_Cheques.frx":0000
         Left            =   1320
         List            =   "Dat_Cheques.frx":0002
         TabIndex        =   16
         Top             =   1680
         Width           =   4335
      End
      Begin VB.TextBox Titular 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   30
         TabIndex        =   14
         Top             =   960
         Width           =   4335
      End
      Begin VB.TextBox Fecha_Cobrar 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         TabIndex        =   13
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Dias_Cobro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   3
         TabIndex        =   11
         Top             =   600
         Width           =   495
      End
      Begin VB.TextBox Fecha_Emision 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         TabIndex        =   9
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Cheque 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   7
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3000
         TabIndex        =   23
         Top             =   2040
         Width           =   1215
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Situaci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   2040
         Width           =   1095
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Banco:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   1680
         Width           =   1095
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Titular:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha a Cobrar:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3000
         TabIndex        =   18
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "D�as al Cobro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Emisi�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2880
         TabIndex        =   10
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Nro.Cheque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   0
         TabIndex        =   8
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   2
      Top             =   3480
      Width           =   5775
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   4800
         Picture         =   "Dat_Cheques.frx":0004
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Cancelar Salir.-"
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton Campo_Memo 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Dat_Cheques.frx":628E
         Style           =   1  'Graphical
         TabIndex        =   26
         ToolTipText     =   "Mostrar Campo Comentarios.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   3480
         Picture         =   "Dat_Cheques.frx":BEA0
         Style           =   1  'Graphical
         TabIndex        =   25
         ToolTipText     =   "Borrar el Cheque de la Cartera.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   2520
         Picture         =   "Dat_Cheques.frx":1212A
         Style           =   1  'Graphical
         TabIndex        =   24
         ToolTipText     =   "Grabar los datos del Cheque.-"
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "Dat_Cheques"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Banco_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Banco_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Buscar_Cheques_Click()
    Cheques_Encontrados.Visible = True
    Cheques_Encontrados.Clear
    MousePointer = 11
    qy = "SELECT * FROM Cheque WHERE Tipo_Cuenta <> 'P' ORDER BY Titular"
    AbreRs
    
    While Not Rs.EOF
        Cheques_Encontrados.AddItem Trim(Mid(Rs.Fields("Titular"), 1, 20)) + Space(30 - Len(Trim(Mid(Rs.Fields("Titular"), 1, 20)))) & " " & Format(Rs.Fields("Fecha_Cobrar"), "dd/mm/yy") & " " & Trim(Rs.Fields("Id_Cheque"))
        Rs.MoveNext
    Wend
    MousePointer = 0
    Cheques_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cheque_GotFocus()
    Cheque.Text = Trim(Cheque.Text)
    Cheque.SelStart = 0
    Cheque.SelText = ""
    Cheque.SelLength = Len(Cheque.Text)
End Sub

Private Sub Cheque_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cheque_LostFocus
End Sub

Private Sub Cheque_LostFocus()
    Cheque.Text = Formateado(Str(Val(Cheque.Text)), 0, 10, " ", False)
    
    If Val(Cheque.Text) > 0 Then
        Leer_Cheque
    End If
End Sub

Private Sub Leer_Cheque()
    If Val(Cheque.Text) > 0 Then
        qy = "SELECT * FROM Cheque WHERE Id_Cheque = " & Trim(Str(Val(Cheque.Text))) & " "
        'Qy = Qy & "AND Tipo_Cuenta = '" & IIf(Fac_Recibo.Clientes.Value = True, "C", "P") & "' "
        AbreRs
        
        If Not Rs.EOF Then
            Nuestra_Firma.Enabled = False
            If Fac_Recibo.Clientes = False Then
                Fecha_Emision.Text = Rs.Fields("Fecha_Emision")
                Dias_Cobro.Text = Rs.Fields("Dias_Cobro")
                Fecha_Cobrar.Text = DateAdd("d", Val(Dias_Cobro.Text), Fecha_Emision.Text)
                Titular.Text = Rs.Fields("Titular")
                Banco.Text = Rs.Fields("Banco")
                Situacion.Text = Rs.Fields("Situacion")
                Importe.Text = Formateado(Str(Val(Rs.Fields("Importe"))), 2, 10, " ", False)
                
                Grabar.Tag = "Pago"
                
                If Val(Rs.Fields("Disponible")) = 2 Then
                    MsgBox "El Cheque ha sido utilizado ya para realizar otro pago"
                    Borrar_Campo
                Else
                    Fecha_Emision.SetFocus
                End If
            Else
                MsgBox "El Cheque ya existe en la base de datos.-", vbInformation, "Atenci�n.!"
                Borrar_Campo
            End If
        Else
            If Fac_Recibo.Proveedores.Value = True Then Nuestra_Firma.Enabled = True
            Fecha_Emision.SetFocus
        End If
    Else
        Cheque.Text = ""
        Cheque.SetFocus
    End If
End Sub

Private Sub Borrar_Campo()
    Cheque.Text = ""
    Titular.Text = ""
    Dias_Cobro.Text = ""
    Banco.Text = ""
    Situacion.Text = ""
    Importe.Text = ""
    Fecha_Cobrar.Text = ""
    Fecha_Emision.Text = ""
    Nuestra_Firma.Value = 0
    
    Cheque.SetFocus
End Sub

Private Sub Cheques_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cheque.SetFocus
End Sub

Private Sub Cheques_Encontrados_LostFocus()
    Cheque.Text = Mid(Cheques_Encontrados.List(Cheques_Encontrados.ListIndex), 41)
    Cheques_Encontrados.Visible = False
    Leer_Cheque
End Sub

Private Sub Dias_Cobro_GotFocus()
    Dias_Cobro.Text = Trim(Dias_Cobro.Text)
    Dias_Cobro.SelStart = 0
    Dias_Cobro.SelText = ""
    Dias_Cobro.SelLength = Len(Dias_Cobro.Text)
End Sub

Private Sub Dias_Cobro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Dias_Cobro_LostFocus()
    Dias_Cobro.Text = Formateado(Str(Val(Dias_Cobro.Text)), 0, 3, " ", False)
    If Val(Dias_Cobro.Text) > 0 Then Fecha_Cobrar.Text = DateAdd("d", Val(Dias_Cobro.Text), Fecha_Emision.Text)
End Sub

Private Sub Fecha_Emision_GotFocus()
    Fecha_Emision.Text = Fecha_Fiscal
    
    Fecha_Emision.Text = Trim(Fecha_Emision.Text)
    Fecha_Emision.SelStart = 0
    Fecha_Emision.SelText = ""
    Fecha_Emision.SelLength = Len(Fecha_Emision.Text)
End Sub

Private Sub Fecha_Emision_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_Emision_LostFocus()
    Fecha_Emision.Text = ValidarFecha(Fecha_Emision.Text)
    
    If Fecha_Emision.Text = "error" Then
        MsgBox "Error de Fechas, verifique e ingrese nuevamemte.", vbInformation, "Atenci�n.!"
        Fecha_Emision.Text = ""
        Fecha_Emision.SetFocus
    End If
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualiazaci�n de Cheques.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    qy = "SELECT * FROM Banco ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Banco.AddItem Trim(Mid(Rs.Fields("Denominacion"), 1, 30)) + Space(30 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 30)))) & " " & Trim(Rs.Fields("Id_Banco"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Fac_Recibo.Enabled = True
    Fac_Recibo.ZOrder
End Sub

Private Sub Grabar_Click()
    If Not Grabar.Tag = "Pago" Then
        qy = "INSERT INTO Cheque VALUES("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Cheque.Text)))
        qy = qy & ", " & Trim(Str(Val(Mid(Banco, 32))))
        qy = qy & ", '" & Trim(Tipo.Text) & " " & Trim(Letra.Text) & " " & Formateado(Str(Val(cCentro_Emisor)), 0, 4, "0", False) & "-" & Formateado(Str(Val(Nro.Text)), 0, 10, "0", False) & "'"
        qy = qy & ", '" & IIf(Fac_Recibo.Clientes.Value = True, "C", "P") & "'"
        qy = qy & ", '" & Trim(Titular.Text) & "'"
        qy = qy & ", " & Trim(Str(Val(Nuestra_Firma.Value)))
        qy = qy & ", '" & Trim(Fecha_Emision.Text) & "'"
        qy = qy & ", '" & Trim(Dias_Cobro.Text) & "'"
        qy = qy & ", '" & Trim(Fecha_Cobrar.Text) & "'"
        qy = qy & ", " & Trim(Str(Val(Situacion.Text)))
        qy = qy & ", " & Trim(Str(Val(Importe.Text))) & ", 1"
        qy = qy & ", '-')"
        Db.Execute (qy)
        
        MsgBox "El Cheque n�mero: " & Trim(Str(Val(Cheque.Text))) & " ha sido grabado respaldando el comprobante: " & Trim(Tipo.Text) & "-" & Trim(Letra.Text) & "-" & Trim(Nro.Text), vbInformation, "Cartera de Cheques."
    Else
        
        qy = "UPDATE Cheque SET "
        qy = qy & "Haber = Haber + " & Trim(Str(Val(Importe.Text))) & " "
        qy = qy & "WHERE Id_Cheque = " & Trim(Str(Val(Cheque.Text))) & " "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        Db.Execute (qy)
        
        MsgBox "Se ha realizado el pago con el cheque.", vbInformation, "Atenci�n.!"
    End If
    
    Unload Me
End Sub

Private Sub Importe_Change()
    If Val(Importe.Text) > 0 And Val(Dias_Cobro.Text) > 0 And Trim(Titular.Text) <> "" And Trim(Banco.Text) <> "" And Val(Cheque.Text) > 0 And Val(Fecha_Emision.Text) > 0 Then
        Grabar.Enabled = True
        Campo_Memo.Enabled = True
    Else
        Grabar.Enabled = False
        Campo_Memo.Enabled = False
    End If
End Sub

Private Sub Importe_GotFocus()
    Importe.Text = Trim(Importe.Text)
    Importe.SelStart = 0
    Importe.SelText = ""
    Importe.SelLength = Len(Importe.Text)
End Sub

Private Sub Importe_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Importe_LostFocus()
    Importe.Text = Formateado(Str(Val(Importe.Text)), 2, 10, " ", False)
End Sub

Private Sub Nuestra_Firma_Click()
    If Nuestra_Firma.Value = 0 Then
        Titular.Enabled = True
        Titular.Text = ""
    Else
        Titular.Enabled = False
        Titular.Text = "MATERIALES 2000 S.A."
    End If
End Sub

Private Sub Nuestra_Firma_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If Cheque.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Situacion_GotFocus()
    Situacion.SelStart = 0
    Situacion.SelText = ""
    Situacion.SelLength = 1
End Sub

Private Sub Situacion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Situacion_LostFocus()
    Situacion.Text = Formateado(Str(Val(Situacion.Text)), 0, 1, " ", False)
End Sub

Private Sub Titular_GotFocus()
    Titular.Text = Trim(Titular.Text)
    Titular.SelStart = 0
    Titular.SelText = ""
    Titular.SelLength = Len(Titular.Text)
End Sub

Private Sub Titular_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Titular_LostFocus()
    Titular.Text = UCase(Titular.Text)
End Sub
