VERSION 5.00
Begin VB.Form Lst_Cli_Saldos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listado de Saldos.-"
   ClientHeight    =   6855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6855
   ScaleWidth      =   11415
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Cliente 
      Height          =   975
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   11415
      Begin VB.CheckBox Saldo_Cero 
         Caption         =   "&Mostrar Saldos en cero."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9240
         TabIndex        =   3
         Top             =   600
         Width           =   2055
      End
      Begin VB.OptionButton Option3 
         Caption         =   "Sσlo Cuentas Deudoras."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   2295
      End
      Begin VB.OptionButton Option2 
         Caption         =   "Sσlo Cuentas Acreedoras."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   2295
      End
      Begin VB.OptionButton Todas_Ctas 
         Caption         =   "&Todas las Cuentas."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Value           =   -1  'True
         Width           =   1695
      End
   End
   Begin VB.Frame Lista 
      Height          =   4935
      Left            =   0
      TabIndex        =   9
      Top             =   960
      Width           =   11415
      Begin VB.TextBox Total 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9840
         MaxLength       =   11
         TabIndex        =   18
         Top             =   4560
         Width           =   1455
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4110
         Left            =   120
         TabIndex        =   5
         Top             =   360
         Width           =   11175
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Saldo"
         Height          =   255
         Left            =   9960
         TabIndex        =   15
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Haber"
         Height          =   255
         Left            =   8760
         TabIndex        =   14
         Top             =   120
         Width           =   1215
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Debe"
         Height          =   255
         Left            =   7560
         TabIndex        =   13
         Top             =   120
         Width           =   1215
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Ult. Mov."
         Height          =   255
         Index           =   0
         Left            =   6720
         TabIndex        =   12
         Top             =   120
         Width           =   855
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Direcciσn"
         Height          =   255
         Index           =   1
         Left            =   3720
         TabIndex        =   16
         Top             =   120
         Width           =   3015
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Nombre"
         Height          =   255
         Left            =   840
         TabIndex        =   17
         Top             =   120
         Width           =   2895
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Cuenta"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   120
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Total:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8400
         TabIndex        =   19
         Top             =   4560
         Width           =   1335
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   10
      Top             =   5880
      Width           =   11415
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8760
         Picture         =   "Lst_Cli_Saldos.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   20
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10200
         Picture         =   "Lst_Cli_Saldos.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   7680
         Picture         =   "Lst_Cli_Saldos.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Imprimir la lista de Cuentas.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Comenzar 
         Height          =   615
         Left            =   120
         Picture         =   "Lst_Cli_Saldos.frx":C80E
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Confirma la Carga de Cuentas.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Lst_Cli_Saldos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Comenzar_Click()
    Dim sTotal As Single
    
    MousePointer = 11
    List1.Clear
    sTotal = 0
    
    qy = "SELECT CtaCte_Cliente.Id_Cuenta,Cliente.Id_Cliente,Cliente.Nombre,Cliente.Domicilio,Cliente.Numero, MAX(Fecha) AS Ult_Mov, SUM(Debe) AS Total_Debe, SUM(Haber) AS Total_Haber,  SUM(Debe - Haber) AS Saldo FROM Cliente,CtaCte_Cliente "
    qy = qy & "WHERE Cliente.Id_Cliente = CtaCte_Cliente.Id_Cuenta "
    qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "GROUP BY CtaCte_Cliente.Id_Cuenta,Cliente.Id_Cliente,Cliente.Nombre,Cliente.Domicilio,Cliente.Numero "
    qy = qy & "ORDER BY Cliente.Nombre"
    AbreRs
    
    While Not Rs.EOF
        Txt = Formateado(Str(Val(Rs.Fields("Id_Cuenta"))), 0, 5, " ", False) & " "
        Txt = Txt & Trim(Mid(Rs.Fields("Nombre"), 1, 27)) + Space(27 - Len(Trim(Mid(Rs.Fields("Nombre"), 1, 27)))) & " "
        Txt = Txt & Trim(Mid(Rs.Fields("Domicilio"), 1, 23)) & " " & Trim(Mid(Rs.Fields("Numero"), 1, 4)) + Space(27 - Len(Trim(Mid(Rs.Fields("Domicilio"), 1, 23)) & " " & Trim(Mid(Rs.Fields("Numero"), 1, 4)))) & " "
        Txt = Txt & Format(Rs.Fields("Ult_Mov"), "dd/mm/yy") & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Total_Debe"))), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Total_Haber"))), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Saldo"))), 2, 10, " ", False)
        
        If Todas_Ctas.Value = True Then 'Totas las cuentas
            If Saldo_Cero.Value = 1 Then  'Mostrar Saldos en Cero
                sTotal = Val(sTotal) + Val(Rs.Fields("Saldo"))
                List1.AddItem Txt
                
            Else
                If Not Formateado(Str(Val(Rs.Fields("Saldo"))), 2, 10, " ", False) = 0 Then 'No Mostrar Saldos en Cero
                    sTotal = Val(sTotal) + Val(Rs.Fields("Saldo"))
                    List1.AddItem Txt
                End If
                
            End If
            
        ElseIf Option2.Value = True Then 'Cuentas Acreedoras
            If Formateado(Str(Val(Rs.Fields("Saldo"))), 2, 10, " ", False) < 0 Then
                sTotal = Val(sTotal) + Val(Rs.Fields("Saldo"))
                List1.AddItem Txt
            End If
        ElseIf Option3.Value = True Then 'Cuentas Deudoras
            
            If Formateado(Str(Val(Rs.Fields("Saldo"))), 2, 10, " ", False) > 0 Then
                sTotal = Val(sTotal) + Val(Rs.Fields("Saldo"))
                List1.AddItem Txt
            End If
        End If
        
        Rs.MoveNext
    Wend
    
    Total.Text = Formateado(Str(Val(sTotal)), 2, 11, " ", True)
    
    MousePointer = 0
    List1.SetFocus
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Listado de Saldos de los Clientes.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 7
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Option2_Click()
    Saldo_Cero.Enabled = False
End Sub

Private Sub Option2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Option3_Click()
    Saldo_Cero.Enabled = False
End Sub

Private Sub Option3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Saldo_Cero_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If List1.ListCount = 0 And Todas_Ctas.Visible = True And Saldo_Cero.Value = 0 Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Total.Text = ""
    List1.Clear
    Saldo_Cero.Value = 0
    Todas_Ctas.Value = True
    
    Todas_Ctas.SetFocus
End Sub

Private Sub Todas_Ctas_Click()
    Saldo_Cero.Enabled = True
End Sub

Private Sub Todas_Ctas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
    
    If List1.ListCount > 0 Then
        
        l = 0
        i = 0
        
        Printer.Font = "Courier New"
        Printer.FontSize = 9
        
        Imprimir_Encabezado
        
        For i = 0 To List1.ListCount - 1
            If l <= 67 Then
                Printer.Print " " & Mid(List1.List(i), 1, 170)
                l = l + 1
            Else
                Printer.Print " "
                Printer.NewPage
                Imprimir_Encabezado
                l = 0
            End If
        Next
        
        Printer.Print " "
        Printer.Print "                                                                                      TOTAL: " & Total.Text
        Printer.EndDoc
    End If
    
    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Imprimir.Tag = Int(List1.ListCount / 67) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                    Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                  Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                            Hora...: " & Format(Time, "hh.mm")
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " E-Mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print "                                        LISTA DE SALDOS DE CLIENTES"
    Printer.Print
    Printer.Print " "
    Printer.Print "  Cta. Nombre                      Direcciσn                    Ult.Mov      Debe       Haber      Saldo"
    Printer.Print " "
End Sub
