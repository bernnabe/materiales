VERSION 5.00
Begin VB.Form Fac_Recibo_Cheque 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Ingreso de Valores"
   ClientHeight    =   3420
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7035
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3420
   ScaleWidth      =   7035
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox Cheques_Encontrados 
      BeginProperty Font 
         Name            =   "Courier New"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   2430
      Left            =   150
      Style           =   1  'Simple Combo
      TabIndex        =   17
      Top             =   225
      Visible         =   0   'False
      Width           =   6765
   End
   Begin VB.CommandButton Confirma 
      Caption         =   "&Confirmar"
      Enabled         =   0   'False
      Height          =   390
      Left            =   4350
      TabIndex        =   7
      Top             =   2925
      Width           =   1290
   End
   Begin VB.CommandButton Termina 
      Cancel          =   -1  'True
      Caption         =   "C&ancelar"
      Height          =   390
      Left            =   5700
      TabIndex        =   8
      Top             =   2925
      Width           =   1215
   End
   Begin VB.Frame Frame1 
      Height          =   2790
      Left            =   75
      TabIndex        =   9
      Top             =   0
      Width           =   6915
      Begin VB.CommandButton Buscar_Cheque 
         Caption         =   "Ch&eque:"
         Height          =   270
         Left            =   300
         TabIndex        =   10
         Top             =   1050
         Width           =   930
      End
      Begin VB.TextBox NumeroCheque 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1350
         MaxLength       =   8
         TabIndex        =   2
         Top             =   1050
         Width           =   1365
      End
      Begin VB.ComboBox Banco 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1350
         TabIndex        =   0
         Top             =   300
         Width           =   4695
      End
      Begin VB.TextBox Titular 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1350
         MaxLength       =   30
         TabIndex        =   3
         Top             =   1425
         Width           =   4665
      End
      Begin VB.TextBox Emision 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1350
         MaxLength       =   10
         TabIndex        =   4
         Top             =   1800
         Width           =   1365
      End
      Begin VB.TextBox Cobranza 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4650
         MaxLength       =   10
         TabIndex        =   5
         Top             =   1875
         Width           =   1365
      End
      Begin VB.TextBox Importe_Cheque 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1350
         MaxLength       =   10
         TabIndex        =   6
         Top             =   2175
         Width           =   1365
      End
      Begin VB.CheckBox Nuestro 
         Caption         =   "&De nuestra firma"
         ForeColor       =   &H00800000&
         Height          =   330
         Left            =   1350
         TabIndex        =   1
         Top             =   675
         Width           =   1815
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Cheque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   150
         TabIndex        =   18
         Top             =   1125
         Width           =   1095
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Cobro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   3225
         TabIndex        =   16
         Top             =   1875
         Width           =   1320
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Banco:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   150
         TabIndex        =   14
         Top             =   300
         Width           =   1095
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Titular:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   150
         TabIndex        =   13
         Top             =   1455
         Width           =   1095
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha emisi�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   150
         TabIndex        =   12
         Top             =   1815
         Width           =   1095
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   150
         TabIndex        =   11
         Top             =   2175
         Width           =   1095
      End
   End
   Begin VB.Label Label12 
      Alignment       =   1  'Right Justify
      Caption         =   "Fecha emisi�n:"
      ForeColor       =   &H00800000&
      Height          =   255
      Index           =   1
      Left            =   75
      TabIndex        =   15
      Top             =   0
      Width           =   1095
   End
End
Attribute VB_Name = "Fac_Recibo_Cheque"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mvarCheque As New ReciboCheque
Private mvarChequesActuales As New Collection
Private mvarCargaConfirmada As Boolean
Private mvarOrigenPago As String

Private objOrigenPago As New OrigenPago

Public Property Get ChequeCargado() As ReciboCheque
    Set ChequeCargado = mvarCheque
End Property

Public Property Let ChequeCargado(vdata As ReciboCheque)
    Set mvarCheque = vdata
End Property

Public Property Get ChequesActuales() As Collection
    Set ChequesActuales = mvarChequesActuales
End Property

Public Property Let ChequesActuales(vdata As Collection)
    Set mvarChequesActuales = vdata
End Property

Public Property Get CargaConfirmada() As Boolean
    CargaConfirmada = mvarCargaConfirmada
End Property

Public Property Let CargaConfirmada(vdata As Boolean)
    mvarCargaConfirmada = vdata
End Property

Public Property Get OrigenPagoCheque() As String
    OrigenPagoCheque = mvarOrigenPago
End Property

Public Property Let OrigenPagoCheque(vdata As String)
    mvarOrigenPago = vdata
End Property

Private Sub Buscar_Cheque_Click()
    MousePointer = 11
    Cheques_Encontrados.Clear
    Cheques_Encontrados.Visible = True
    
    qy = "SELECT Cheque.*, Banco.* FROM Cheque, Banco WHERE Empresa = " & Trim(Val(Id_Empresa)) & " "
    qy = qy & "AND Tipo_Cuenta = 'C' "
    qy = qy & "AND Disponible = 1 "
    qy = qy & "AND Banco.Id_Banco = Cheque.Banco "
    If Val(Mid(Banco.Text, 31)) > 0 Then qy = qy & "AND Cheque.Banco = " & Val(Mid(Banco.Text, 31)) & " "
    qy = qy & "ORDER BY Titular, Fecha_Cobrar "
    AbreRs
    
    While Not Rs.EOF
        Txt = Formateado(Str(Val(Rs.Fields("Id_Cheque"))), 0, 8, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Banco"))), 0, 4, " ", False) & "-"
        Txt = Txt & Trim(Mid(Rs.Fields("Denominacion"), 1, 20)) + Space(20 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 20)))) & " "
        Txt = Txt & Trim(Mid(Rs.Fields("Titular"), 1, 20)) + Space(20 - Len(Trim(Mid(Rs.Fields("Titular"), 1, 20)))) & " "
        Txt = Txt & Trim(Format(Rs.Fields("Fecha_Cobrar"), "dd/mm/yy")) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Importe"))), 2, 10, " ", False)
        
        Cheques_Encontrados.AddItem Txt
        Rs.MoveNext
    Wend
    
    MousePointer = 0
    Cheques_Encontrados.SetFocus
End Sub

Private Sub Mostrar_Cheque()
    Nuestro.Value = 0
    Nuestro.Enabled = False

    Banco.Text = Rs.Fields("Banco")
    Titular.Text = Rs.Fields("Titular")
    Emision.Text = Rs.Fields("Fecha_Emision")
    Cobranza.Text = Rs.Fields("Fecha_Cobrar")
    Importe_Cheque.Text = Formateado(Str(Val(Rs.Fields("Importe"))), 2, 10, " ", False)
    
    Leer_Banco
End Sub

Private Sub Cheques_Encontrados_KeyPress(KeyAscii As Integer)
    If Cheques_Encontrados.Text <> "" Then
        NumeroCheque.Text = Trim(Mid(Cheques_Encontrados.Text, 1, 8))
        Banco.Text = Trim(Mid(Cheques_Encontrados.Text, 10, 4))
        Leer_Banco
        Cheque_LostFocus
        
        Termina_Click
    End If
End Sub

Private Sub Cobranza_GotFocus()
    Cobranza.SelLength = Len(Cobranza.Text)
End Sub

Private Sub Cobranza_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cobranza_LostFocus()
    Cobranza.Text = ValidarFecha(Cobranza.Text)
End Sub

Private Sub Confirma_Click()

    If Validar() Then
    
        CargarDatosCheque
        CargaConfirmada = True
        
        Unload Me
        
    End If
    
End Sub

Private Function Validar() As Boolean

    Dim objCheque As ReciboCheque
    
    Validar = True
    
    For Each objCheque In ChequesActuales
    
        If objCheque.NroCheque = Val(NumeroCheque.Text) And objCheque.Banco = Val(Mid(Banco.Text, 31)) Then
        
            MsgBox "El cheque que est� intentando ingresar ya ha sido cargado", vbInformation, "Atenci�n!"
            Validar = False
        
        End If
    
    Next

End Function

Private Function CargarDatosCheque()

    Dim objCheque As New ReciboCheque
    
    objCheque.NroCheque = NumeroCheque.Text
    objCheque.FechaCobranza = Cobranza.Text
    objCheque.FechaEmision = Emision.Text
    objCheque.Importe = CDbl(Importe_Cheque.Text)
    objCheque.Titular = Titular.Text
    objCheque.Banco = Val(Mid(Banco.Text, 31))
    objCheque.BancoNombre = Trim(Mid(Banco.Text, 1, 30))
    objCheque.NuestraFirma = Nuestro.Value
    
    ChequeCargado = objCheque
    
End Function

Private Sub Emision_GotFocus()
    Emision.SelLength = Len(Emision.Text)
End Sub

Private Sub Emision_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Emision_LostFocus()
    Emision.Text = ValidarFecha(Emision.Text)
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 2
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos

    Cargar_Bancos
    
    SetearModoPorOrigen
End Sub

Private Sub SetearModoPorOrigen()

    If OrigenPagoCheque = objOrigenPago.Cliente Then
        
        Buscar_Cheque.Enabled = False
        Nuestro.Enabled = False
        
    Else
    
        Buscar_Cheque.Enabled = True
        Nuestro.Enabled = True
        
    End If
        
End Sub

Private Sub Importe_Cheque_Change()
    If Val(Importe_Cheque.Text) > 0 And Val(NumeroCheque.Text) > 0 And Val(Emision.Text) > 0 And Val(Cobranza.Text) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Importe_Cheque_GotFocus()
    Importe_Cheque.SelLength = Len(Importe_Cheque.Text)
End Sub

Private Sub Importe_Cheque_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Importe_Cheque_LostFocus()
    Importe_Cheque.Text = Formateado(Str(Val(Importe_Cheque.Text)), 2, 10, " ", False)
End Sub

Private Sub NumeroCheque_GotFocus()
    NumeroCheque.Text = Trim(NumeroCheque.Text)
    NumeroCheque.SelLength = Len(NumeroCheque.Text)
End Sub

Private Sub NumeroCheque_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cheque_LostFocus()
    NumeroCheque.Text = Formateado(Str(Val(NumeroCheque.Text)), 0, 8, " ", False)
    
    If Val(NumeroCheque.Text) > 0 And Val(Mid(Banco.Text, 31)) > 0 And Val(Nuestro.Value) = 0 And mvarOrigenPago = objOrigenPago.Proveedor Then
        Leer_Cheque
    End If
End Sub

Private Sub Leer_Cheque()
    If Val(NumeroCheque.Text) > 0 Then
        If Val(Mid(Banco.Text, 31)) > 0 Then
            qy = "SELECT * FROM Cheque WHERE Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
            qy = qy & "AND Tipo_Cuenta = 'C' "
            qy = qy & "AND Banco = " & Trim(Str(Val(Mid(Banco.Text, 31))))
            qy = qy & "AND Id_Cheque = " & Trim(Str(Val(NumeroCheque.Text)))
            AbreRs
            
            If Rs.EOF Then
                MsgBox "El Cheque es inexistente, verifique e ingrese nuevamente.", vbInformation, "Atenci�n.!"
                NumeroCheque.Text = ""
                Banco.Text = ""
                
                Banco.SetFocus
            Else
                If Val(Rs.Fields("Disponible")) = 1 Then
                    Mostrar_Cheque
                Else
                    MsgBox "El Cheque no se encuentra disponible, verifique e ingrese nuevamente.", vbInformation, "Atenci�n.!"
                    Banco.Text = ""
                    NumeroCheque.Text = ""
                    
                    Banco.SetFocus
                End If
            End If
        Else
            MsgBox "Selecione un banco.", vbInformation
            Banco.Text = ""
            Banco.SetFocus
        End If
    End If
End Sub

Private Sub Nuestro_Click()
    If Nuestro.Value <> 0 Then
        Buscar_Cheque.Enabled = False
    Else
        Buscar_Cheque.Enabled = True
    End If
End Sub

Private Sub Nuestro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Banco_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Banco_LostFocus()
    If Val(Banco.Text) > 0 Then
        Leer_Banco
    End If
End Sub

Private Sub Leer_Banco()
    If Val(Banco.Text) > 0 Then
        qy = "SELECT * FROM Banco WHERE Id_Banco = " & Trim(Str(Val(Banco.Text)))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "El C�digo de Banco que ha ingresado es inexistente, verifique e ingrese nuevamente.", vbInformation, "Atenci�n.!"
            Banco.Text = ""
            Banco.SetFocus
        Else
            Banco.Text = Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Banco"))
        End If
    End If
End Sub

Private Sub Cargar_Bancos()
    qy = "SELECT * FROM Banco ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Banco.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Banco"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Termina_Click()
    If Cheques_Encontrados.Visible Then
        Cheques_Encontrados.Visible = False
        NumeroCheque.SetFocus
    Else
        CargaConfirmada = False
        ChequeCargado = Nothing
        Unload Me
    End If
End Sub

Private Sub Titular_GotFocus()
    Titular.Text = Trim(Titular.Text)
    Titular.SelLength = Len(Titular.Text)
End Sub

Private Sub Titular_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub
    
Private Sub Titular_LostFocus()
    Titular.Text = FiltroCaracter(Titular.Text)
End Sub
