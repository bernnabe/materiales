VERSION 5.00
Begin VB.Form Inf_ResCtaCli 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Resumen de Cuenta Corriente.-"
   ClientHeight    =   6855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10815
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6855
   ScaleWidth      =   10815
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Cuenta 
      Height          =   1095
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   10815
      Begin VB.ComboBox Mes_Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7320
         TabIndex        =   3
         Top             =   600
         Width           =   2175
      End
      Begin VB.TextBox A�o_Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10080
         MaxLength       =   4
         TabIndex        =   4
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox A�o 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10080
         MaxLength       =   4
         TabIndex        =   2
         Top             =   240
         Width           =   615
      End
      Begin VB.ComboBox Mes 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7320
         TabIndex        =   1
         Top             =   240
         Width           =   2175
      End
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2040
         TabIndex        =   15
         Top             =   240
         Visible         =   0   'False
         Width           =   4215
      End
      Begin VB.TextBox Nombre 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2040
         MaxLength       =   30
         TabIndex        =   14
         Top             =   240
         Width           =   4215
      End
      Begin VB.TextBox Cuenta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton Buscar_Cuenta 
         Caption         =   "&Cuenta:"
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "A�o:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9120
         TabIndex        =   26
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta mes:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6360
         TabIndex        =   25
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde mes:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6360
         TabIndex        =   24
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "A�o:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9120
         TabIndex        =   23
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Resumen 
      Enabled         =   0   'False
      Height          =   4815
      Left            =   0
      TabIndex        =   10
      Top             =   1080
      Width           =   10815
      Begin VB.ListBox List2 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         TabIndex        =   12
         Top             =   4440
         Width           =   10575
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4110
         Left            =   120
         TabIndex        =   6
         ToolTipText     =   "Haciendo un Doble Click sobre un Item de la lista puede ver el contenido del comprobante.-"
         Top             =   360
         Width           =   10575
      End
      Begin VB.CommandButton Command7 
         Caption         =   "Saldo"
         Height          =   255
         Left            =   9240
         TabIndex        =   21
         Top             =   120
         Width           =   1455
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Haber"
         Height          =   255
         Left            =   8160
         TabIndex        =   20
         Top             =   120
         Width           =   1095
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Debe"
         Height          =   255
         Left            =   6960
         TabIndex        =   19
         Top             =   120
         Width           =   1215
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Vto."
         Height          =   255
         Left            =   6000
         TabIndex        =   18
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Detalle"
         Height          =   255
         Left            =   3240
         TabIndex        =   17
         Top             =   120
         Width           =   2775
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Comprobante"
         Height          =   255
         Left            =   1080
         TabIndex        =   16
         Top             =   120
         Width           =   2175
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Fecha"
         Height          =   255
         Left            =   120
         TabIndex        =   22
         Top             =   120
         Width           =   975
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   11
      Top             =   5880
      Width           =   10815
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   9600
         Picture         =   "Inf_ResCtaCli.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   8160
         Picture         =   "Inf_ResCtaCli.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Imprimir el Resumen de Cuenta.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Inf_ResCtaCli.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Confirma la Carga del Resumen de la Cuenta.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Inf_ResCtaCli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Pagina As Long

Private Sub A�o_GotFocus()
    A�o.Text = Trim(A�o.Text)
    A�o.SelStart = 0
    A�o.SelLength = Len(A�o.Text)
End Sub

Private Sub A�o_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub A�o_LostFocus()
    A�o.Text = Val(A�o.Text)
    
    If Val(A�o.Text) > 0 And Len(A�o.Text) < 4 Then
        MsgBox "Ingrese el a�o con cuatro d�gitos.", vbCritical, "Atenci�n.!"
        A�o.Text = ""
        A�o.SetFocus
    End If
End Sub

Private Sub A�o_Hasta_GotFocus()
    A�o_hasta.Text = Trim(A�o_hasta.Text)
    A�o_hasta.SelStart = 0
    A�o_hasta.SelLength = Len(A�o_hasta.Text)
End Sub

Private Sub A�o_Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub A�o_Hasta_LostFocus()
    A�o_hasta.Text = Val(A�o_hasta.Text)
    
    If Val(A�o_hasta.Text) > 0 And Len(A�o_hasta.Text) < 4 Then
        MsgBox "Ingrese el A�o_Hasta con cuatro d�gitos.", vbCritical, "Atenci�n.!"
        A�o_hasta.Text = ""
        A�o_hasta.SetFocus
    End If
End Sub


Private Sub Buscar_Cuenta_Click()
    Cuentas_Encontradas.Visible = True
    MousePointer = 11
    If Cuentas_Encontradas.ListCount = 0 Then
        qy = "SELECT Cliente.Nombre, Cliente.Id_Cliente, CtaCte_Cliente.Id_Cuenta FROM CtaCte_Cliente, Cliente "
        qy = qy & "WHERE Id_Cliente = Id_Cuenta "
        qy = qy & "GROUP BY Cliente.Nombre, Cliente.Id_Cliente, CtaCte_Cliente.Id_Cuenta "
        qy = qy & "ORDER BY Cliente.Nombre"
        AbreRs
        
        While Not Rs.EOF
            Cuentas_Encontradas.AddItem Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields("Id_Cuenta"))
            Rs.MoveNext
        Wend
    End If
    MousePointer = 0
    Cuentas_Encontradas.SetFocus
End Sub

Private Sub Confirma_Click()
    Dim Total_Debe    As Double
    Dim Total_Haber   As Double
    
    Dim Saldo_Inicial As Double
    
    Dim MesDesde As Long
    Dim A�oDesde As Long
    Dim MesHasta As Long
    Dim A�oHasta As Long
    
    Confirma.Tag = 0
    Total_Debe = 0
    Total_Haber = 0
    
    MesDesde = Val(Mid(Mes.Text, 1, 2))
    A�oDesde = Val(A�o.Text)
    MesHasta = Val(Mid(Mes_Hasta.Text, 1, 2))
    A�oHasta = Val(A�o_hasta.Text)
    
    If Val(Cuenta.Text) > 0 Then
        List1.Clear
        List2.Clear
                
        If MesDesde > 0 And A�oDesde > 0 Then
        
            Dim FechaInicial As Date
            Dim FechaFinal As Date
            
            FechaInicial = CDate("01/" & MesDesde & "/" & A�oDesde)
            
            If (MesHasta < 12) Then
                FechaFinal = DateAdd("d", -1, CDate("01/" & MesHasta + 1 & "/" & A�oHasta))
            Else
                FechaFinal = CDate("31/" & MesHasta & "/" & A�oHasta)
            End If
        
            qy = "SELECT SUM(DEBE - HABER) FROM CtaCte_Cliente "
            qy = qy & "WHERE Id_Cuenta = " & Val(Cuenta.Text) & " "
            qy = qy & "AND Id_Empresa = " & Val(Id_Empresa) & " "
            qy = qy & "AND Fecha < '" & FechaInicial & "' "
            
            AbreRs
            
            If Not IsNull(Rs.Fields(0)) = True Then
                Saldo_Inicial = Formateado(Str(Val(Rs.Fields(0))), 2, 0, "", False)
                If Val(Saldo_Inicial) > 0 Then
                    Total_Debe = Saldo_Inicial
                ElseIf Val(Saldo_Inicial) < 0 Then
                    Total_Haber = Saldo_Inicial
                End If
            End If
            
            List1.AddItem Space(30) & "SALDO INICIAL                                            " & Formateado(Str(Val(Saldo_Inicial)), 2, 10, " ", False)
            'List1.AddItem
            List1.AddItem Space(30) & "��������������������������������������������������������������������"
            List1.AddItem ""
        End If

        
        qy = "SELECT CtaCte_Cliente.Id_Cuenta, CtaCte_Cliente.Comprobante,CtaCte_Cliente.Detalle,CtaCte_Cliente.Vencimiento,CtaCte_Cliente.Debe,CtaCte_Cliente.Haber,CtaCte_Cliente.Fecha FROM CtaCte_Cliente WHERE Id_Cuenta = " & Trim(Str(Val(Cuenta.Text))) & " "
        
        If Val(Mes.Text) > 0 And Val(A�o.Text) > 0 Then
            
            qy = qy & "AND Fecha BETWEEN '" & FechaInicial & "' AND '" & FechaFinal & "'"
            
        End If
        qy = qy & "AND Id_Empresa = " & Val(Id_Empresa) & " "
        qy = qy & "GROUP BY CtaCte_Cliente.Id_Cuenta,CtaCte_Cliente.Comprobante,CtaCte_Cliente.Detalle,CtaCte_Cliente.Vencimiento,CtaCte_Cliente.Debe,CtaCte_Cliente.Haber,CtaCte_Cliente.Fecha "
        qy = qy & "ORDER BY Fecha ASC"
        AbreRs
        
        If Val(Saldo_Inicial) > 0 Then
            Total_Debe = Saldo_Inicial
        ElseIf Val(Saldo_Inicial) < 0 Then
            Total_Haber = Val(Saldo_Inicial) * -1
        End If
        
        While Not Rs.EOF
            Txt = Trim(Format(Rs.Fields("Fecha"), "dd/mm/yy")) & " "
            Txt = Txt & Trim(Rs.Fields("Comprobante")) + Space(20 - Len(Trim(Rs.Fields("Comprobante")))) & " "
            Txt = Txt & Trim(Mid(Rs.Fields("Detalle"), 1, 25)) + Space(25 - Len(Trim(Mid(Rs.Fields("Detalle"), 1, 25)))) & " "
            If Not IsNull(Rs.Fields("Vencimiento")) = True Then
                Txt = Txt & Trim(Format(Rs.Fields("Vencimiento"), "dd/mm/yy")) & " "
            Else
                Txt = Txt & Space(9)
            End If
            Txt = Txt & IIf(Val(Rs.Fields("Debe")) <> 0, Formateado(Str(Val(Rs.Fields("Debe"))), 2, 10, " ", False), Space(10)) & " "
            Txt = Txt & IIf(Val(Rs.Fields("Haber")) <> 0, Formateado(Str(Val(Rs.Fields("Haber"))), 2, 10, " ", False), Space(10)) & " "
                
            Total_Debe = Val(Total_Debe) + Val(Rs.Fields("Debe"))
            Total_Haber = Val(Total_Haber) + Val(Rs.Fields("Haber"))

                
            Txt = Txt & Formateado(Str(Val(Total_Debe - Total_Haber)), 2, 10, " ", False)
                
            List1.AddItem Txt
            Rs.MoveNext
        Wend
        
        Confirma.Tag = Val(Total_Debe) - Val(Total_Haber)
        List2.AddItem "SALDO DE LA CUENTA:" & Space(66) & Formateado(Str(Val(Confirma.Tag)), 2, 11, " ", True)
        
        Marco_Resumen.Enabled = True
        Marco_Cuenta.Enabled = False
        Imprimir.Enabled = True
        List1.SetFocus
    Else
        Cuenta.Text = ""
        Cuenta.SetFocus
    End If
End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_LostFocus()
    If Val(Cuenta.Text) > 0 Then
        Leer_Cuenta
    End If
End Sub

Private Sub Leer_Cuenta()
    qy = "SELECT * FROM Cliente "
    qy = qy & "WHERE Id_Cliente = " & Trim(Str(Val(Cuenta.Text)))
    AbreRs
    
    If Rs.EOF Then
        MsgBox "Cuenta Corriente Inexistente...", vbInformation, "Atenci�n.!"
        Cuenta.Text = ""
        Nombre.Text = ""
        Cuenta.SetFocus
    Else
        Nombre.Text = Rs.Fields("Nombre")
        
        'No Traigo los movimientos porque puede no ser la cuenta correcta
        'Leer_Movimientos
    End If
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuenta.SetFocus
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Cuenta.Text = Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 32)
    Cuentas_Encontradas.Visible = False
    Cuenta.SetFocus
    Cuenta_LostFocus
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Menu.Estado.Panels(2).Text = "Resumen de Cuenta Corriente.-"
    
    For i = 1 To 12
        Mes.AddItem Mes_Letra(i)
        
        Mes_Hasta.AddItem Mes_Letra(i)
    Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
        
    If List1.ListCount > 0 Then
            
        i = 0
        l = 0
        Pagina = 1
            
        Close #1
        Open "LPT1:" For Output As #1
            
        Imprimir_Encabezado
        
        If Val(Mid(List1.List(0), 1, 10)) = 0 Then
            Print #1, Trim(List1.List(0))
            Print #1,
            i = 3
        End If
            
        For i = 0 To List1.ListCount - 1
            If l <= 28 Then
                Print #1, Mid(List1.List(i), 1, 8) & " " & Mid(List1.List(i), 10, 20) & " " & Mid(List1.List(i), 66, 10) & " " & Mid(List1.List(i), 77, 10) & " " & Mid(List1.List(i), 88, 10)
                l = l + 1
            Else
                Pagina = Pagina + 1
                Imprimir_Encabezado
                l = 0
            End If
        Next
            
        Print #1, "-------------------------------------------------------------------------------"
        Print #1, "SALDO DE LA CUENTA:                                " & Formateado(Str(Val((Mid(List2.List(0), 50)))), 2, 11, " ", True)
        Print #1, Chr$(12);
        Pagina = 0
        Close #1
    End If
        
    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Imprimir.Tag = Int(List1.ListCount / 53) + 1
    
    Print #1, " " & Trim(UCase(cEmpresa)) & "                                       P�gina.: " & Val(Pagina) & "/" & Imprimir.Tag
    Print #1, "                                                    Fecha..: " & Format(Fecha_Fiscal, "dd/mm/yyyy")
    Print #1,
    Print #1, "                                 RESUMEN DE CUENTA"
    Print #1,
    Print #1, "CUENTA Nro.: "; Formateado(Str(Val(Cuenta.Text)), 0, 5, " ", False); " - "; Trim(Nombre.Text)
    Print #1, "-------------------------------------------------------------------------------"
    Print #1, " Fecha   Comprobante                Debe      Haber      Saldo"
    Print #1, "-------------------------------------------------------------------------------"
End Sub

Private Sub List1_DblClick()
    If Trim(Mid(List1.List(List1.ListIndex), 7, 2)) = "FC" Or Trim(Mid(List1.List(List1.ListIndex), 7, 2)) = "NC" Or Trim(Mid(List1.List(List1.ListIndex), 7, 2)) = "ND" Then
        Detalle_Factura.Comprobante.Text = Trim(Mid(List1.List(List1.ListIndex), 7, 2)) & " "
        Detalle_Factura.Comprobante.Text = Detalle_Factura.Comprobante.Text & Trim(Mid(List1.List(List1.ListIndex), 10, 1)) & " "
        Detalle_Factura.Comprobante.Text = Detalle_Factura.Comprobante.Text & Trim(Mid(List1.List(List1.ListIndex), 12, 15)) & " "
        
        If Val(Mid(List1.List(List1.ListIndex), 63, 10)) > 0 Then
            Detalle_Factura.Importe.Text = Mid(List1.List(List1.ListIndex), 63, 10) & " "
        Else
            Detalle_Factura.Importe.Text = Mid(List1.List(List1.ListIndex), 74, 10) & " "
        End If
        
        Detalle_Factura.Codigo.Text = Cuenta.Text
        Detalle_Factura.Codigo.Tag = "Cliente"
        Detalle_Factura.Nombre.Text = Nombre.Text
    End If
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: List1_DblClick
End Sub

Private Sub Mes_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Mes_LostFocus()
    Mes.Text = Mes.List(Val(Mes.Text) - 1)
End Sub

Private Sub Mes_Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Mes_Hasta_LostFocus()
    Mes_Hasta.Text = Mes_Hasta.List(Val(Mes_Hasta.Text) - 1)
End Sub

Private Sub Salir_Click()
    If Cuenta.Text = "" And List1.ListCount = 0 And List2.ListCount = 0 Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Cuenta.Text = ""
    Nombre.Text = ""
    Mes.Text = ""
    A�o.Text = ""
    A�o_hasta.Text = ""
    Mes_Hasta.Text = ""
    List1.Clear
    List2.Clear
    Marco_Resumen.Enabled = False
    Marco_Cuenta.Enabled = True
    Imprimir.Enabled = False
    
    Cuenta.SetFocus
End Sub

