VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Act_PreciosArt 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualización de Precios.-"
   ClientHeight    =   5535
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7935
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   5535
   ScaleWidth      =   7935
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Global 
      Height          =   3975
      Left            =   225
      TabIndex        =   27
      Top             =   525
      Visible         =   0   'False
      Width           =   7455
      Begin VB.TextBox Porcentaje_Global 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   6
         TabIndex        =   16
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Porcentaje de Remarque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   28
         Top             =   360
         Width           =   1935
      End
   End
   Begin VB.Frame Marco_Individual 
      Height          =   3975
      Left            =   225
      TabIndex        =   24
      Top             =   525
      Visible         =   0   'False
      Width           =   7455
      Begin VB.OptionButton Option3 
         Caption         =   "Actualización de Precios de Contado.-"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2280
         TabIndex        =   14
         Top             =   1440
         Width           =   4695
      End
      Begin VB.OptionButton Act_Precio_Cta 
         Caption         =   "Actualización a Precios de Cuentas Corrientes.-"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2280
         TabIndex        =   13
         Top             =   1200
         Width           =   4575
      End
      Begin VB.ComboBox Individual_Categoria 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   12
         Top             =   720
         Width           =   4695
      End
      Begin VB.TextBox Porcentaje_Individual 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         TabIndex        =   15
         Top             =   1800
         Width           =   855
      End
      Begin VB.ComboBox Articulo_Individual 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   11
         Top             =   360
         Width           =   4695
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo de Cliente:"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   600
         TabIndex        =   30
         Top             =   720
         Width           =   1575
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Porcentaje de Remarque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   26
         Top             =   1800
         Width           =   1935
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Artículo a Actualizar:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   600
         TabIndex        =   25
         Top             =   360
         Width           =   1575
      End
   End
   Begin VB.Frame Marco_Rubro 
      Height          =   3975
      Left            =   240
      TabIndex        =   3
      Top             =   480
      Width           =   7455
      Begin VB.OptionButton Option2 
         Caption         =   "Actualización de Precios de Contado"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2160
         TabIndex        =   9
         Top             =   2760
         Width           =   4215
      End
      Begin VB.OptionButton Precio_Cta 
         Caption         =   "Actualización a Precios de Cuentas Corrientes.-"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2160
         TabIndex        =   8
         Top             =   2520
         Width           =   4095
      End
      Begin VB.ComboBox Rubro_Categoria 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   7
         Top             =   2040
         Width           =   5055
      End
      Begin VB.TextBox Porcentaje_Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2160
         MaxLength       =   6
         TabIndex        =   10
         Top             =   3240
         Width           =   855
      End
      Begin VB.ComboBox Hasta_Articulo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   6
         Top             =   1560
         Width           =   5055
      End
      Begin VB.ComboBox Desde_Articulo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   5
         Top             =   1200
         Width           =   5055
      End
      Begin VB.ComboBox Hasta_Rubro 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   4
         Top             =   720
         Visible         =   0   'False
         Width           =   5055
      End
      Begin VB.ComboBox Desde_Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   1
         Top             =   360
         Width           =   5055
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo de Clientes:"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   29
         Top             =   2040
         Width           =   1815
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Porcentaje de Remarque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   3240
         Width           =   1935
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta el Artículo:"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde el Artículo:"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   1200
         Width           =   1935
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta el Rubro:"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   720
         Visible         =   0   'False
         Width           =   1935
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   360
         Width           =   1935
      End
   End
   Begin MSComctlLib.TabStrip Fichero 
      Height          =   4455
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   7695
      _ExtentX        =   13573
      _ExtentY        =   7858
      TabWidthStyle   =   2
      MultiRow        =   -1  'True
      _Version        =   393216
      BeginProperty Tabs {1EFB6598-857C-11D1-B16A-00C0F0283628} 
         NumTabs         =   3
         BeginProperty Tab1 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Rubros"
            Object.ToolTipText     =   "Actualización de Precios por Rangos de Rubros y artículos.-"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab2 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Individual"
            Object.ToolTipText     =   "Actualización de Precios de Forma Individual.-"
            ImageVarType    =   2
         EndProperty
         BeginProperty Tab3 {1EFB659A-857C-11D1-B16A-00C0F0283628} 
            Caption         =   "&Global"
            Object.ToolTipText     =   "Actualización Global de Precios.-"
            ImageVarType    =   2
         EndProperty
      EndProperty
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   2
      Top             =   4560
      Width           =   7935
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   6720
         Picture         =   "Act_Art.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   23
         ToolTipText     =   "Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Act_Art.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   22
         ToolTipText     =   "Grabar los Datos.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Act_PreciosArt"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Act_Precio_Cta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Articulo_Individual_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Articulo_Individual_LostFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Desde_Articulo_GotFocus()
    If Val(Mid(Desde_Rubro.Text, 32, 5)) > 0 And Val(Mid(Hasta_Rubro.Text, 32, 5)) > 0 Then
        qy = "SELECT * FROM Articulo "
        qy = qy & "WHERE Id_Rubro BETWEEN " & Trim(Str(Val(Mid(Desde_Rubro.Text, 32, 5)))) & " AND " & Trim(Str(Val(Mid(Hasta_Rubro.Text, 32, 5)))) & " "
        qy = qy & "ORDER BY Descripcion"
        AbreRs
        
        While Not Rs.EOF
            Desde_Articulo.AddItem Trim(Rs.Fields("Descripcion")) + Space(30 - Len(Trim(Rs.Fields("Descripcion")))) & " " & Trim(Rs.Fields("Id_Articulo"))
            Hasta_Articulo.AddItem Trim(Rs.Fields("Descripcion")) + Space(30 - Len(Trim(Rs.Fields("Descripcion")))) & " " & Trim(Rs.Fields("Id_Articulo"))
            Rs.MoveNext
        Wend
        
        If Desplegar_Combos = True Then SendKeys "{F4}"
    Else
        Desde_Articulo.Clear
        Hasta_Articulo.Clear
    End If
End Sub

Private Sub Desde_Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_Rubro_GotFocus()
    Act_PreciosArt.Refresh
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Desde_Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_Rubro_LostFocus()
    If Val(Desde_Rubro.Text) > 0 Then
        Desde_Rubro = Leer_Rubro(Val(Desde_Rubro.Text))
    ElseIf Trim(Desde_Rubro.Text) = "*" Then
        Desde_Rubro.Text = "RUBRO INICIAL                  0"
    End If
End Sub

Function Leer_Rubro(Id_Rubro As String)
    qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & Trim(Str(Val(Id_Rubro)))
    AbreRs
    
    If Rs.EOF Then
        MsgBox "El Rubro es inexistente...", vbInformation, "Atención.!"
        Leer_Rubro = ""
    Else
        Leer_Rubro = Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Rubro"))
    End If
End Function

Private Sub Fichero_Click()
    If Fichero.SelectedItem.Index = 1 Then
        Marco_Rubro.Enabled = True
        Marco_Rubro.Visible = True
        Desde_Rubro.SetFocus
        Marco_Individual.Enabled = False
        Marco_Individual.Visible = False
        Marco_Global.Enabled = False
        Marco_Global.Visible = False
    ElseIf Fichero.SelectedItem.Index = 2 Then
        Marco_Individual.Enabled = True
        Marco_Individual.Visible = True
        Articulo_Individual.SetFocus
        Marco_Rubro.Enabled = False
        Marco_Rubro.Visible = False
        Marco_Global.Enabled = False
        Marco_Global.Visible = False
    ElseIf Fichero.SelectedItem.Index = 3 Then
        Marco_Global.Enabled = True
        Marco_Global.Visible = True
        Porcentaje_Global.SetFocus
        Marco_Rubro.Enabled = False
        Marco_Rubro.Visible = False
        Marco_Individual.Enabled = False
        Marco_Individual.Visible = False
    End If
    
    Grabar.Enabled = False
End Sub

Private Sub Form_Activate()
    Fichero_Click
    Menu.Estado.Panels(2).Text = "Actualización de Precios.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 3.5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Parametros
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    On Error GoTo Errores
    
    Db.BeginTrans
    Dim strCalculo As String
    
    If Marco_Rubro.Enabled = True Then
        
        strCalculo = IIf(Val(Porcentaje_Rubro.Text) > 0, "(((Precio_Compra) * " & Val(Porcentaje_Rubro.Text) & ") /100) + Precio_Compra", "Precio_Compra / (((" & Val(Porcentaje_Rubro.Text) & " / 100) - 1) * -1) ")
        
        qy = "Insert Into Articulo_Historial_Precio " & _
                    "(Id_Rubro, Id_Articulo, Fecha, ValorAnterior, ValorActual, Usuario) " & _
                "SELECT " & _
                    "Id_Rubro, " & _
                    "Id_Articulo, " & _
                    "GetDate(), " & _
                    "Precio_Compra, " & _
                    strCalculo & ", " & _
                    Numero_Usuario & " " & _
                "FROM Articulo " & _
                "WHERE Id_Rubro = " & Trim(Str(Val(Mid(Desde_Rubro.Text, 32, 5))))
        Db.Execute (qy)
        
        qy = "UPDATE Articulo SET "
        If Val(Porcentaje_Rubro.Text) > 0 Then 'Remarque
            qy = qy & "Precio_Compra = (((Precio_Compra) * " & Val(Porcentaje_Rubro.Text) & ") /100) + Precio_Compra "
        Else 'Descuento
            qy = qy & "Precio_Compra = Precio_Compra / (((" & Val(Porcentaje_Rubro.Text) & " / 100) - 1) * -1) "
        End If
        qy = qy & "WHERE Id_Rubro = " & Trim(Str(Val(Mid(Desde_Rubro.Text, 32, 5))))
        Db.Execute (qy)
        
    ElseIf Marco_Individual.Enabled = True Then

        strCalculo = IIf(Val(Porcentaje_Individual.Text) > 0, "(((Precio_Compra) * " & Val(Porcentaje_Individual.Text) & ") /100) + Precio_Compra", "Precio_Compra / (((" & Val(Porcentaje_Individual.Text) & " / 100) - 1) * -1) ")
        
        qy = "Insert Into Articulo_Historial_Precio " & _
                    "(Id_Rubro, Id_Articulo, Fecha, ValorAnterior, ValorActual, Usuario) " & _
                "SELECT " & _
                    "Id_Rubro, " & _
                    "Id_Articulo, " & _
                    "GetDate(), " & _
                    "Precio_Compra, " & _
                    strCalculo & ", " & _
                    Numero_Usuario & " " & _
                "FROM Articulo " & _
                "WHERE Id_Rubro = " & Trim(Str(Val(Mid(Articulo_Individual.Text, 31, 5)))) & " AND Id_Articulo = " & Trim(Str(Val(Mid(Articulo_Individual.Text, 36, 5)))) & " "
        Db.Execute (qy)
        
        qy = "UPDATE Articulo SET "
        If Val(Porcentaje_Individual.Text) > 0 Then 'Remarque
            qy = qy & "Precio_Compra = (((Precio_Compra) * " & Val(Porcentaje_Individual.Text) & ") /100) + Precio_Compra "
        Else 'Descuento
            qy = qy & "Precio_Compra = Precio_Compra / (((" & Val(Porcentaje_Individual.Text) & " / 100) - 1) * -1) "
        End If
        qy = qy & "WHERE Id_Rubro = " & Trim(Str(Val(Mid(Articulo_Individual.Text, 31, 5)))) & " AND Id_Articulo = " & Trim(Str(Val(Mid(Articulo_Individual.Text, 36, 5)))) & " "
        Db.Execute (qy)
        
    Else
    
        strCalculo = IIf(Val(Porcentaje_Global.Text) > 0, "(((Precio_Compra) * " & Val(Porcentaje_Global.Text) & ") /100) + Precio_Compra", "Precio_Compra / (((" & Val(Porcentaje_Global.Text) & " / 100) - 1) * -1) ")
        
        qy = "Insert Into Articulo_Historial_Precio " & _
                    "(Id_Rubro, Id_Articulo, Fecha, ValorAnterior, ValorActual, Usuario) " & _
                "SELECT " & _
                    "Id_Rubro, " & _
                    "Id_Articulo, " & _
                    "GetDate(), " & _
                    "Precio_Compra, " & _
                    strCalculo & ", " & _
                    Numero_Usuario & " " & _
                "FROM Articulo "
        Db.Execute (qy)

        'Remarque Global
        qy = "UPDATE Articulo SET "
        If Val(Porcentaje_Global.Text) > 0 Then
            qy = qy & "Precio_Compra = (((Precio_Compra) * " & Val(Porcentaje_Global.Text) & ") /100) + Precio_Compra "
        ElseIf Val(Porcentaje_Global.Text) < 0 Then
            qy = qy & "Precio_Compra = Precio_Compra / (((" & Val(Porcentaje_Global.Text) & " / 100) - 1) * -1), "
        End If
        Db.Execute (qy)
        
    End If
    
    Db.CommitTrans
    
    Desde_Rubro.Text = ""
    Desde_Articulo.Text = ""
    Hasta_Rubro.Text = ""
    Hasta_Articulo.Text = ""
    Porcentaje_Rubro.Text = ""
    Precio_Cta.Value = True
    Rubro_Categoria.Text = ""
    
    Articulo_Individual.Text = ""
    Porcentaje_Individual.Text = ""
    Act_Precio_Cta.Value = True
    Individual_Categoria.Text = ""
    
    Porcentaje_Global.Text = ""
    
    Fichero.SelectedItem.Selected = True
    
    MsgBox "La actualización se ha realizado correctamente.", vbInformation, "Actualización de precios.-"
    
    Exit Sub
    
Errores:
    
    Db.RollbackTrans
    
    MsgBox "Han ocurrido errores intentando actualizar los precios de los articulos. Descripcion: " + Err.Description, vbCritical, "Actualización de precios.-"

End Sub

Private Sub Hasta_Articulo_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Hasta_Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Rubro_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Hasta_Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Rubro_LostFocus()
    If Val(Hasta_Rubro.Text) > 0 Then
        Hasta_Rubro = Leer_Rubro(Val(Hasta_Rubro.Text))
    ElseIf Trim(Hasta_Rubro.Text) = "*" Then
        Hasta_Rubro.Text = "RUBRO FINAL                    0"
    End If
End Sub

Private Sub Individual_Categoria_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Individual_Categoria_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Option2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Option3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Porcentaje_Global_Change()
    If Val(Porcentaje_Global.Text) <> 0 And Marco_Global.Enabled = True Then
        Grabar.Enabled = True
    Else
        Grabar.Enabled = False
    End If
End Sub

Private Sub Porcentaje_Global_GotFocus()
    Porcentaje_Global.Text = Trim(Porcentaje_Global.Text)
    
    Porcentaje_Global.SelStart = 0
    Porcentaje_Global.SelText = ""
    Porcentaje_Global.SelLength = Len(Porcentaje_Global.Text)
End Sub

Private Sub Porcentaje_Global_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Porcentaje_Global_LostFocus()
    Porcentaje_Global.Text = Formateado(Str(Val(Porcentaje_Global.Text)), 2, 6, " ", False)
End Sub

Private Sub Porcentaje_Individual_Change()
    If Val(Porcentaje_Individual.Text) <> 0 And Val(Mid(Articulo_Individual.Text, 32, 4)) > 0 Then
        Grabar.Enabled = True
    Else
        Grabar.Enabled = False
    End If
End Sub

Private Sub Porcentaje_Individual_GotFocus()
    Porcentaje_Individual.Text = Trim(Porcentaje_Individual.Text)
    
    Porcentaje_Individual.SelStart = 0
    Porcentaje_Individual.SelText = ""
    Porcentaje_Individual.SelLength = Len(Porcentaje_Individual.Text)
End Sub

Private Sub Porcentaje_Individual_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Porcentaje_Individual_LostFocus()
    Porcentaje_Individual.Text = Formateado(Str(Val(Porcentaje_Individual.Text)), 2, 6, " ", False)
End Sub

Private Sub Porcentaje_Rubro_Change()
    If Val(Porcentaje_Rubro.Text) <> 0 And Val(Mid(Desde_Rubro.Text, 32, 5)) > 0 Then
        Grabar.Enabled = True
    Else
        Grabar.Enabled = False
    End If
End Sub

Private Sub Porcentaje_Rubro_GotFocus()
    Porcentaje_Rubro.Text = Trim(Porcentaje_Rubro.Text)
    
    Porcentaje_Rubro.SelStart = 0
    Porcentaje_Rubro.SelText = ""
    Porcentaje_Rubro.SelLength = Len(Porcentaje_Rubro.Text)
End Sub

Private Sub Porcentaje_Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Porcentaje_Rubro_LostFocus()
    Porcentaje_Rubro.Text = Formateado(Str(Val(Porcentaje_Rubro.Text)), 2, 6, " ", False)
End Sub

Private Sub Precio_Cta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rubro_Categoria_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Rubro_Categoria_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub

Private Sub Cargar_Parametros()
    Dim i As Long
    
    For i = 1 To 2
        Rubro_Categoria.AddItem Cliente_Categ(i)
        Individual_Categoria.AddItem Cliente_Categ(i)
    Next
    
    qy = "SELECT * FROM Rubro ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Desde_Rubro.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Rubro"))
        Hasta_Rubro.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Rubro"))
        
        Rs.MoveNext
    Wend
    
    qy = "SELECT * FROM Articulo ORDER BY Descripcion"
    AbreRs
    
    While Not Rs.EOF
        Articulo_Individual.AddItem Trim(Rs.Fields("Descripcion")) + Space(30 - Len(Trim(Rs.Fields("Descripcion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 4, " ", False) & " " & Trim(Rs.Fields("Id_Articulo"))
        Rs.MoveNext
    Wend
End Sub
