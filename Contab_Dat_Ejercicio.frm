VERSION 5.00
Begin VB.Form Contab_Dat_Ejercicio 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaci�n de Ejercicios.-"
   ClientHeight    =   4695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4695
   ScaleWidth      =   6015
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Datos 
      Height          =   2415
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   6015
      Begin VB.TextBox Periodo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         TabIndex        =   1
         Top             =   1200
         Width           =   615
      End
      Begin VB.TextBox Cierre 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         TabIndex        =   3
         Top             =   1920
         Width           =   1335
      End
      Begin VB.TextBox Apertura 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         TabIndex        =   2
         Top             =   1560
         Width           =   1335
      End
      Begin VB.TextBox Codigo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1680
         TabIndex        =   0
         Top             =   360
         Width           =   615
      End
      Begin VB.CommandButton Command4 
         Caption         =   "C�digo:"
         Height          =   255
         Left            =   720
         TabIndex        =   9
         Top             =   360
         Width           =   855
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de cierre:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1920
         Width           =   1455
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de apertura:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   1560
         Width           =   1455
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Per�odo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   1200
         Width           =   1455
      End
   End
   Begin VB.Frame Marco_Estado 
      Caption         =   "Estado del Ejercicio"
      Enabled         =   0   'False
      Height          =   1335
      Left            =   0
      TabIndex        =   13
      Top             =   2400
      Width           =   6015
      Begin VB.Frame Frame2 
         Height          =   1095
         Left            =   3120
         TabIndex        =   17
         Top             =   240
         Width           =   2775
         Begin VB.TextBox Desde 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1320
            TabIndex        =   19
            Top             =   360
            Width           =   1335
         End
         Begin VB.CheckBox Cierre_Parcial 
            Caption         =   "Cierres Parciales"
            Height          =   255
            Left            =   120
            TabIndex        =   22
            Top             =   0
            Width           =   1575
         End
         Begin VB.TextBox Hasta 
            BackColor       =   &H00E0E0E0&
            Enabled         =   0   'False
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   285
            Left            =   1320
            TabIndex        =   18
            Top             =   720
            Width           =   1335
         End
         Begin VB.Label Label5 
            Alignment       =   1  'Right Justify
            Caption         =   "Hasta:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   720
            Width           =   1095
         End
         Begin VB.Label Label4 
            Alignment       =   1  'Right Justify
            Caption         =   "Desde:"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   360
            Width           =   1095
         End
      End
      Begin VB.OptionButton No_Abierto 
         Caption         =   "No abierto"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   720
         TabIndex        =   16
         Top             =   840
         Width           =   1335
      End
      Begin VB.OptionButton Cerrado 
         Caption         =   "Cerrado"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   720
         TabIndex        =   15
         Top             =   600
         Width           =   1335
      End
      Begin VB.OptionButton Abierto 
         Caption         =   "Abierto"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   720
         TabIndex        =   14
         Top             =   360
         Value           =   -1  'True
         Width           =   1335
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   8
      Top             =   3720
      Width           =   6015
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   1080
         Picture         =   "Contab_Dat_Ejercicio.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   4920
         Picture         =   "Contab_Dat_Ejercicio.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Contab_Dat_Ejercicio.frx":C514
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Grabar los Datos.-"
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "Contab_Dat_Ejercicio"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Abierto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Apertura_GotFocus()
    Apertura.SelStart = 0
    Apertura.SelLength = Len(Apertura.Text)
End Sub

Private Sub Apertura_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Apertura_LostFocus()
    Apertura.Text = ValidarFecha(Apertura.Text)
End Sub

Private Sub Cerrado_Click()
    If Cerrado.Enabled = True Then Cierre_Parcial.Value = 0
End Sub

Private Sub Cerrado_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cierre_Click()
    If Cerrado.Enabled = True Then Cierre_Parcial.Value = 0
End Sub

Private Sub Cierre_GotFocus()
    If Val(Apertura.Text) > 0 Then Cierre.Text = DateAdd("d", 364, Apertura.Text)
    
    Cierre.Text = Trim(Cierre.Text)
    Cierre.SelStart = 0
    Cierre.SelLength = Len(Cierre.Text)
End Sub

Private Sub Cierre_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cierre_LostFocus()
    Cierre.Text = ValidarFecha(Cierre.Text)
End Sub

Private Sub Cierre_Parcial_Click()
    If Cierre_Parcial.Value = 1 Then
        If Not Abierto.Value = True Then
            MsgBox "S�lo puede hacer cierres parciales a un ejercicio abierto!.", vbCritical, "Atenci�n.!"
            Cierre_Parcial.Value = 0
            Abierto.SetFocus
        Else
            Desde.Enabled = True
            Hasta.Enabled = True
            Desde.BackColor = vbWhite
            Hasta.BackColor = vbWhite
            
            Desde.SetFocus
        End If
    Else
        Desde.Enabled = False
        Hasta.Enabled = False
        Desde.BackColor = &HE0E0E0
        Hasta.BackColor = &HE0E0E0
    End If
End Sub

Private Sub Codigo_GotFocus()
    Codigo.Text = Trim(Codigo.Text)
    Codigo.SelStart = 0
    Codigo.SelLength = Len(Codigo.Text)
End Sub

Private Sub Codigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Codigo_LostFocus()
    If Val(Codigo.Text) > 0 Then
        Leer_Codigo
    End If
End Sub

Private Sub Leer_Codigo()
    If Val(Codigo.Text) > 0 Then
        qy = "SELECT * FROM Ejercicio WHERE Id_Ejercicio = " & Trim(Str(Val(Codigo.Text)))
        AbreRs
        
        If Rs.EOF Then
            If MsgBox("El Ejercicio es inexistente desea darlo de alta?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
                Marco_Estado.Enabled = False
                Habilitar_Campos
            Else
                Salir_Click
            End If
        Else
            Marco_Estado.Enabled = True
            Mostrar_Ejercicio
        End If
    Else
        Codigo.Text = ""
        Codigo.SetFocus
    End If
End Sub

Private Sub Habilitar_Campos()
    Grabar.Tag = "Alta"
    Grabar.Enabled = True
    Periodo.Text = ""
    Apertura.Text = ""
    Cierre.Text = ""
    
    Periodo.SetFocus
End Sub

Private Sub Mostrar_Ejercicio()
    Grabar.Tag = "NO"
    Borrar.Enabled = True
    
    
    Periodo.Text = Rs.Fields("Periodo")
    Apertura.Text = Rs.Fields("Apertura")
    Cierre.Text = Rs.Fields("Cierre")
    
    Periodo.SetFocus
End Sub

Private Sub Combo1_Change()

End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    
    Abrir_Base_Datos
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        MsgBox "ATENCI�N: Se est� por crear un nuevo ciclo contable, esto puede demorar unos minutos. Aguarde por favor.", vbInformation, "ATENCI�N.!"
        
        MousePointer = 11
        
        qy = "INSERT INTO Ejercicio VALUES ("
        qy = qy & Val(Id_Empresa)
        qy = qy & ", " & Val(Codigo.Text)
        qy = qy & ", " & Trim(Str(Val(Periodo.Text)))
        qy = qy & ", '" & Trim(Apertura.Text) & "'"
        qy = qy & ", '" & Trim(Cierre.Text) & "'"
        qy = qy & ", 0, '" & Trim(Apertura.Text) & "', '" & Trim(Cierre.Text) & "')"
        Db.Execute (qy)
                
        'Traigo todas las cuentas que van a ser insertadas en "Saldo_Inicial"
        qy = "SELECT Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6 "
        qy = qy & "FROM Cuenta "
        qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "GROUP BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6 "
        qy = qy & "ORDER BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6 "
        AbreRs
        
        While Not Rs.EOF
            'Inserto las cuentas encontradas
            qy = "INSERT INTO Saldo_Inicial VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_1"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_2"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_3"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_4"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_5"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_6"))))
            qy = qy & ", " & Val(Codigo.Text)
            qy = qy & ", 0.00)"
            Db.Execute (qy)
            
            Rs.MoveNext
        Wend
        
        qy = "SELECT Asiento_Item.Id_Asiento, Asiento_Item.Nivel_1, Asiento_Item.Nivel_2, Asiento_Item.Nivel_3, Asiento_Item.Nivel_4, Asiento_Item.Nivel_5, Asiento_Item.Nivel_6, SUM(Debe) AS Debitos, SUM(Haber) AS Creditos "
        qy = qy & "FROM Asiento_Item "
        qy = qy & "WHERE Asiento_Item.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "GROUP BY Asiento_Item.Id_Asiento, Asiento_Item.Nivel_1, Asiento_Item.Nivel_2, Asiento_Item.Nivel_3, Asiento_Item.Nivel_4, Asiento_Item.Nivel_5, Asiento_Item.Nivel_6"
        AbreRs
        
        While Not Rs.EOF
            qy = "UPDATE Saldo_Inicial SET "
            qy = qy & "Saldo_Inicial = Saldo_Inicial + " & Formateado(Str(Val(Val(Rs.Fields("Debitos")) - Val(Rs.Fields("Creditos")))), 2, 12, " ", False) & " "
            qy = qy & "WHERE Id_Nivel_1 = " & Trim(Str(Val(Rs.Fields("Nivel_1")))) & " "
            qy = qy & "AND Id_Nivel_2 = " & Trim(Str(Val(Rs.Fields("Nivel_2")))) & " "
            qy = qy & "AND Id_Nivel_3 = " & Trim(Str(Val(Rs.Fields("Nivel_3")))) & " "
            qy = qy & "AND Id_Nivel_4 = " & Trim(Str(Val(Rs.Fields("Nivel_4")))) & " "
            qy = qy & "AND Id_Nivel_5 = " & Trim(Str(Val(Rs.Fields("Nivel_5")))) & " "
            qy = qy & "AND Id_Nivel_6 = " & Trim(Str(Val(Rs.Fields("Nivel_6")))) & " "
            qy = qy & "AND Ejercicio = " & Val(Codigo.Text) & " "
            qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
            Db.Execute (qy)
            
            Rs.MoveNext
        Wend
        
        MousePointer = 0
        
        MsgBox "El Nuevo Ciclo contable ya ha sido creado.!", vbInformation, "Atenci�n.!"
    End If
    
    Salir_Click
End Sub

Private Sub Option1_Click()

End Sub

Private Sub No_Abierto_Click()
    If No_Abierto.Enabled = True Then Cierre_Parcial.Value = 0
End Sub

Private Sub No_Abierto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Periodo_GotFocus()
    Periodo.Text = Trim(Periodo.Text)
    Periodo.SelStart = 0
    Periodo.SelLength = Len(Periodo.Text)
End Sub

Private Sub Periodo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Periodo_LostFocus()
    Periodo.Text = Val(Periodo.Text)
End Sub

Private Sub Salir_Click()
    If Codigo.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Marco_Estado.Enabled = False
    Borrar.Enabled = False
    Grabar.Enabled = False
    
    Periodo.Text = ""
    Codigo.Text = ""
    Apertura.Text = ""
    Cierre.Text = ""
    Grabar.Tag = ""
    
    Codigo.SetFocus
End Sub
