VERSION 5.00
Begin VB.Form Fac_Vta 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Emisi�n de Comprobantes.-"
   ClientHeight    =   7680
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7680
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Captura 
      Height          =   3165
      Left            =   1950
      TabIndex        =   48
      Top             =   2250
      Visible         =   0   'False
      Width           =   7815
      Begin VB.ComboBox Alicuota 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1650
         TabIndex        =   84
         Top             =   2025
         Visible         =   0   'False
         Width           =   690
      End
      Begin VB.TextBox Id_Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   49
         Top             =   240
         Width           =   4335
      End
      Begin VB.TextBox Articulo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   5
         TabIndex        =   51
         Top             =   600
         Width           =   735
      End
      Begin VB.CommandButton Buscar_Cuentas 
         Caption         =   "&Cuenta:"
         Height          =   255
         Left            =   1440
         TabIndex        =   82
         Top             =   1320
         Width           =   855
      End
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2400
         TabIndex        =   81
         Top             =   1320
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.TextBox Cuenta 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   30
         TabIndex        =   55
         Top             =   1320
         Width           =   4335
      End
      Begin VB.TextBox Rec_Desc 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   10
         TabIndex        =   61
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Final_Unitario 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   57
         Top             =   2640
         Width           =   1335
      End
      Begin VB.ComboBox Articulos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2400
         TabIndex        =   76
         Top             =   600
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.TextBox Largo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         MaxLength       =   9
         TabIndex        =   60
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Ancho 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3840
         MaxLength       =   10
         TabIndex        =   59
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Espesor 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   9
         TabIndex        =   58
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Medida 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   10
         TabIndex        =   53
         Top             =   960
         Width           =   1215
      End
      Begin VB.CommandButton Termina 
         Caption         =   "&Termina"
         Height          =   375
         Left            =   6840
         TabIndex        =   66
         ToolTipText     =   "Terminar o cancelar.-"
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton Borrar 
         Caption         =   "&Borra"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6840
         TabIndex        =   65
         ToolTipText     =   "Descargar el art�culo de la factura.-"
         Top             =   2280
         Width           =   855
      End
      Begin VB.CommandButton Confirma 
         Caption         =   "&Confirma"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6840
         TabIndex        =   64
         ToolTipText     =   "Cargar el Art�culo de la factura.-"
         Top             =   1920
         Width           =   855
      End
      Begin VB.TextBox Total 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   63
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Cantidad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3840
         MaxLength       =   10
         TabIndex        =   62
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Precio_Venta 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   10
         TabIndex        =   56
         Top             =   2040
         Width           =   1335
      End
      Begin VB.ComboBox Presentacion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4800
         TabIndex        =   54
         Top             =   960
         Width           =   1935
      End
      Begin VB.TextBox Descripcion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   30
         TabIndex        =   52
         Top             =   600
         Width           =   4335
      End
      Begin VB.CommandButton Buscar_Articulos 
         Caption         =   "&Art�culo:"
         Height          =   255
         Left            =   600
         TabIndex        =   50
         TabStop         =   0   'False
         ToolTipText     =   "Buscar Art�culos en la Base de datos.-"
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label31 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   83
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label19 
         Alignment       =   1  'Right Justify
         Caption         =   "Presentaci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   80
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label29 
         Alignment       =   1  'Right Justify
         Caption         =   "A Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1440
         TabIndex        =   79
         Top             =   1320
         Width           =   855
      End
      Begin VB.Label Label24 
         Alignment       =   2  'Center
         Caption         =   "% Rec +/ Desc -"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2400
         TabIndex        =   78
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label Label30 
         Alignment       =   2  'Center
         Caption         =   "P/Final Unitario"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   77
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label Label28 
         Alignment       =   2  'Center
         Caption         =   "Largo M"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5160
         TabIndex        =   74
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label27 
         Alignment       =   2  'Center
         Caption         =   "Ancho """
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   73
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label26 
         Alignment       =   2  'Center
         Caption         =   "Espesor """
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2400
         TabIndex        =   72
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label23 
         Alignment       =   1  'Right Justify
         Caption         =   "Medida:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   70
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label22 
         Alignment       =   2  'Center
         Caption         =   "Total"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5280
         TabIndex        =   69
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label Label21 
         Alignment       =   2  'Center
         Caption         =   "Cantidad a Retirar"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   68
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label Label20 
         Alignment       =   2  'Center
         Caption         =   "Precio Vta. Unit."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   67
         Top             =   1800
         Width           =   1335
      End
   End
   Begin VB.Frame Marco_Factura 
      Height          =   1770
      Left            =   75
      TabIndex        =   17
      Top             =   0
      Width           =   11535
      Begin VB.ComboBox Clientes_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         TabIndex        =   75
         Top             =   600
         Visible         =   0   'False
         Width           =   3975
      End
      Begin VB.TextBox Tipo_Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10920
         MaxLength       =   1
         TabIndex        =   11
         Top             =   960
         Width           =   495
      End
      Begin VB.TextBox Vto 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10080
         MaxLength       =   10
         TabIndex        =   13
         Top             =   1320
         Width           =   1335
      End
      Begin VB.TextBox Plazo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8175
         MaxLength       =   4
         TabIndex        =   12
         ToolTipText     =   "Cantidad de d�as al Vencimiento del documento (ciendo cero, el documento es de contado).-"
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox Cuit 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8175
         MaxLength       =   13
         TabIndex        =   10
         Top             =   975
         Width           =   1695
      End
      Begin VB.ComboBox Condicion_Iva 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8175
         TabIndex        =   9
         Top             =   600
         Width           =   3240
      End
      Begin VB.TextBox Letra 
         Alignment       =   2  'Center
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8175
         MaxLength       =   1
         TabIndex        =   6
         ToolTipText     =   "Letra"
         Top             =   240
         Width           =   375
      End
      Begin VB.ComboBox Localidad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         TabIndex        =   5
         Top             =   1350
         Width           =   3990
      End
      Begin VB.TextBox Direccion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         MaxLength       =   30
         TabIndex        =   4
         Top             =   975
         Width           =   3990
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4575
         MaxLength       =   10
         TabIndex        =   1
         ToolTipText     =   "Fecha de Emisi�n.-"
         Top             =   225
         Width           =   1365
      End
      Begin VB.TextBox Nombre 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         MaxLength       =   30
         TabIndex        =   3
         Top             =   600
         Width           =   3990
      End
      Begin VB.TextBox Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1050
         MaxLength       =   5
         TabIndex        =   2
         ToolTipText     =   "Ingrese el C�digo del Cliente o ""M"" para ingresarlo manualmente y posteriormente registrarlo en la Base de Datos.-"
         Top             =   600
         Width           =   765
      End
      Begin VB.CommandButton Buscar_Clientes 
         Caption         =   "&Cliente:"
         Height          =   315
         Left            =   150
         TabIndex        =   22
         TabStop         =   0   'False
         ToolTipText     =   "Buscar Clientes en la base de datos.-"
         Top             =   600
         Width           =   840
      End
      Begin VB.ComboBox Tipo_Comp 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   2775
      End
      Begin VB.TextBox Centro_Emisor 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8550
         TabIndex        =   7
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Numero 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9150
         MaxLength       =   10
         TabIndex        =   8
         ToolTipText     =   "N�mero.-"
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label25 
         Caption         =   "(P/R):"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   10320
         TabIndex        =   71
         Top             =   960
         Width           =   495
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Vto:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9360
         TabIndex        =   31
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Plazo:"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   6375
         TabIndex        =   30
         Top             =   1350
         Width           =   1740
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "C.U.I.T.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6375
         TabIndex        =   29
         Top             =   975
         Width           =   1695
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Condici�n de Iva:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6375
         TabIndex        =   28
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Comprobante:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6375
         TabIndex        =   27
         Top             =   300
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Localidad:"
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   75
         TabIndex        =   25
         Top             =   1350
         Width           =   1740
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Direcci�n:"
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   75
         TabIndex        =   24
         Top             =   975
         Width           =   1740
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   23
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Factura_Item 
      Enabled         =   0   'False
      Height          =   4305
      Left            =   75
      TabIndex        =   18
      Top             =   1725
      Width           =   11535
      Begin VB.CommandButton cmdLimpiarLista 
         Caption         =   "Limpiar Lista"
         Height          =   315
         Left            =   3225
         TabIndex        =   87
         Top             =   3900
         Width           =   1515
      End
      Begin VB.CommandButton Agregar 
         Caption         =   "Agregar art�oculos"
         Height          =   315
         Left            =   75
         TabIndex        =   86
         ToolTipText     =   "Comenzar a agregar art�culos a la factura.-"
         Top             =   3900
         Width           =   1545
      End
      Begin VB.CommandButton Facturar_Presupuesto 
         Caption         =   "&Importar art�culos"
         Height          =   315
         Left            =   1650
         TabIndex        =   85
         TabStop         =   0   'False
         ToolTipText     =   "Facturar Presupuestos emitidos.-"
         Top             =   3900
         Width           =   1515
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3435
         Left            =   75
         TabIndex        =   14
         ToolTipText     =   "Haciendo un doble click sobre un item puede borrarlo o modificarlo.-"
         Top             =   375
         Width           =   11340
      End
      Begin VB.Label Label10 
         Caption         =   $"Fac_Vta.frx":0000
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   120
         Width           =   11295
      End
   End
   Begin VB.Frame Marco_Totales 
      Enabled         =   0   'False
      Height          =   735
      Left            =   75
      TabIndex        =   19
      Top             =   6000
      Width           =   11535
      Begin VB.TextBox Total_Factura 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9960
         MaxLength       =   10
         TabIndex        =   39
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox Neto_Gravado 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   9
         TabIndex        =   26
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Exento 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   9
         TabIndex        =   33
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Sub_Total 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         MaxLength       =   9
         TabIndex        =   34
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Iva_Inscrip 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         MaxLength       =   9
         TabIndex        =   35
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Sobre_Tasa 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4920
         MaxLength       =   9
         TabIndex        =   36
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Retencion_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6120
         MaxLength       =   9
         TabIndex        =   37
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Retencion_IIBB 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7320
         MaxLength       =   9
         TabIndex        =   38
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "TOTAL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9960
         TabIndex        =   47
         Top             =   120
         Width           =   1455
      End
      Begin VB.Label Label17 
         Alignment       =   2  'Center
         Caption         =   "Retenc. IIBB"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7320
         TabIndex        =   46
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label16 
         Alignment       =   2  'Center
         Caption         =   "Retenc. IVA"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6120
         TabIndex        =   45
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label15 
         Alignment       =   2  'Center
         Caption         =   "IVA 10.5%"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4920
         TabIndex        =   44
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "IVA 21%"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   43
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label13 
         Alignment       =   2  'Center
         Caption         =   "Sub Total"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2520
         TabIndex        =   42
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label12 
         Alignment       =   2  'Center
         Caption         =   "Exento"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1320
         TabIndex        =   41
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label11 
         Alignment       =   2  'Center
         Caption         =   "Neto Gravado"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   120
         Width           =   1215
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   75
      TabIndex        =   20
      Top             =   6675
      Width           =   11535
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10275
         Picture         =   "Fac_Vta.frx":00CB
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Salir del m�dulo o cancelar.-"
         Top             =   225
         Width           =   1140
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   75
         Picture         =   "Fac_Vta.frx":6355
         Style           =   1  'Graphical
         TabIndex        =   15
         ToolTipText     =   "Grabar e imprimir la factura.-"
         Top             =   225
         Width           =   1140
      End
   End
End
Attribute VB_Name = "Fac_Vta"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mvarOrigenDatosImportados As TiposComprobantes
Private mvarDatosImportados As Boolean
Private mvarCodigoDatosImportados As String

Public Property Get OrigenDatosImportados() As TiposComprobantes
    OrigenDatosImportados = mvarOrigenDatosImportados
End Property

Public Property Let OrigenDatosImportados(vdata As TiposComprobantes)
    mvarOrigenDatosImportados = vdata
End Property

Public Property Get DatosImportados() As Boolean
    DatosImportados = mvarDatosImportados
End Property

Public Property Let DatosImportados(vdata As Boolean)
    mvarDatosImportados = vdata
End Property

Public Property Get CodigoDatosImportados() As String
    CodigoDatosImportados = mvarCodigoDatosImportados
End Property

Public Property Let CodigoDatosImportados(vdata As String)
    mvarCodigoDatosImportados = vdata
End Property

Private Function CargarArticulosImportados(rstDatos As ADODB.Recordset, ActualizarPrecios As Boolean)

    Dim i As Long
    Dim objArticulo As New clsArticulo
    
    If Not IsNull(rstDatos) And Not rstDatos.EOF Then
        
        While Not rstDatos.EOF
        
            Txt = Formateado(Str(Val(rstDatos("Rubro"))), 0, 4, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(rstDatos("Articulo"))), 0, 5, " ", False) & " "
            If Val(rstDatos("Presentacion")) = 3 Or Val(rstDatos("Presentacion")) = 4 Or Val(rstDatos("Presentacion")) = 5 Then
                Txt = Txt & Trim(Mid(Trim(Mid(rstDatos("Descripcion"), 1, 28)) & "x(" & Trim(Val(rstDatos("Ancho"))) & "x" & Trim(Val(rstDatos("Largo"))) & ")", 1, 37)) + Space(37 - Len(Trim(Mid(Trim(Mid(rstDatos("Descripcion"), 1, 28)) & "x(" & Trim(Val(rstDatos("Ancho"))) & "x" & Trim(Val(rstDatos("Largo")) & ")"), 1, 37)))) & " "
            Else
                Txt = Txt & Trim(rstDatos("Descripcion")) + Space(37 - Len(Trim(rstDatos("Descripcion")))) & " "
            End If
            
            Txt = Txt & IIf(Val(Medida.Text) > 0, Formateado(Str(Val(Medida.Text)), 2, 7, " ", False) & " ", Space(8))
            
            If Val(rstDatos("Presentacion")) = 0 Then
                Txt = Txt & Space(3) & " "
            ElseIf Val(rstDatos("Presentacion")) = 1 Then
                Txt = Txt & "Us." & " "
            ElseIf Val(rstDatos("Presentacion")) = 2 Then
                Txt = Txt & "Lts" & " "
            ElseIf Val(rstDatos("Presentacion")) = 3 Then
                Txt = Txt & "P2 " & " "
            ElseIf Val(rstDatos("Presentacion")) = 4 Then
                Txt = Txt & "M2 " & " "
            ElseIf Val(rstDatos("Presentacion")) = 5 Then
                Txt = Txt & "Ml " & " "
            ElseIf Val(rstDatos("Presentacion")) = 6 Then
                Txt = Txt & "Ml " & " "
            ElseIf Val(rstDatos("Presentacion")) = 7 Then
                Txt = Txt & "Kgs" & " "
            ElseIf Val(rstDatos("Presentacion")) = 8 Then
                Txt = Txt & "AxL" & " "
            End If
            
            Dim dblPrecioVenta As Double
             
            If (rstDatos("Rubro") > 0 And rstDatos("Articulo") > 0) And ActualizarPrecios Then
                dblPrecioVenta = objArticulo.GetPrecioVentaPorTipoCliente(rstDatos("Rubro"), rstDatos("Articulo"), Tipo_Cliente.Text, Plazo.Text)
            Else
                If (rstDatos("Precio_Vta") > 0) Then
                    dblPrecioVenta = rstDatos("Precio_Vta")
                Else
                    dblPrecioVenta = (rstDatos("Importe_Total") / rstDatos("Cantidad")) - rstDatos("Impuestos")
                End If
            End If
             
            If ActualizarPrecios Then
                
                Dim dblImporteUnitario As Double
                
                dblImporteUnitario = objArticulo.GetPrecioArticulo(rstDatos("Rubro"), rstDatos("Articulo"), Tipo_Cliente.Text, Plazo.Text, rstDatos("Espesor"), rstDatos("Ancho"), rstDatos("Largo"))
                
                If Not Letra.Text = "A" Then
                    dblImporteUnitario = ((dblImporteUnitario * Alicuota_Iva) / 100) + dblImporteUnitario
                End If
                
                Txt = Txt & Formateado(Str(Val(dblImporteUnitario)), 2, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(dblImporteUnitario)), 4, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(rstDatos("Cantidad"))), 2, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(dblImporteUnitario) * rstDatos("Cantidad")), 2, 10, " ", False) & "   "
            
            Else
                            
                Txt = Txt & Formateado(Str(Val(rstDatos("Importe_Total") / rstDatos("Cantidad"))), 2, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(rstDatos("Importe_Total") / rstDatos("Cantidad"))), 4, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(rstDatos("Cantidad"))), 2, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(rstDatos("Importe_Total"))), 2, 10, " ", False) & "   "
                
            End If
            
            Txt = Txt & Formateado(Str(Val(0)), 2, 10, " ", False) & " " 'Existencia
            Txt = Txt & Formateado(Str(Val(rstDatos("Espesor"))), 2, 9, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(rstDatos("Ancho"))), 2, 9, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(rstDatos("Largo"))), 2, 9, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(dblPrecioVenta)), 4, 9, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(rstDatos("Presentacion"))), 0, 1, " ", False)
            
            List1.AddItem Txt
            
            rstDatos.MoveNext
        Wend
    End If

End Function

Private Sub Agregar_Click()
    If Trim(Fecha.Text) <> "" And Trim(Mid(Condicion_Iva.Text, 1, 2)) <> "" And Cliente.Text <> "" And (UCase(Mid(Tipo_Comp.Text, 1, 2)) = "FC" Or UCase(Mid(Tipo_Comp.Text, 1, 2)) = "NC" Or UCase(Mid(Tipo_Comp.Text, 1, 2)) = "ND") And (Trim(Tipo_Cliente.Text) = "P" Or Trim(Tipo_Cliente.Text) = "R") Then
        
        If ExisteFacturaAImprimir() And Id_Empresa = 2 Then
            
            MsgBox "La factura ya ha sido impresa, verifique e ingrese nuevamente", vbCritical, "Atenci�n"
            
            Numero.SetFocus
            Exit Sub
            
        End If
        
        If Val(Cuit.Text) = 0 And Trim(UCase(Letra.Text)) = "A" Then
            MsgBox "El Cliente no tiene Cargado su n�mero de CUIT"
            Salir_Click
            
            Exit Sub
        End If
        
        Marco_Captura.Visible = True
        Marco_Captura.Refresh
        
        Marco_Captura.Enabled = True
        Marco_Factura_Item.Enabled = False
        
        Cliente.Enabled = False
        Tipo_Cliente.Enabled = False
        
        Botonera.Enabled = False
        Termina.Cancel = True
        Confirma.Visible = True
        
        AbrirBusquedaArticulos

    Else
        Tipo_Comp.SetFocus
    End If
    
End Sub

Private Function ExisteFacturaAImprimir() As Boolean
    Dim rstDatos As New ADODB.Recordset
        
    qy = "SELECT * FROM Factura_Venta WHERE Id_Tipo_Factura = '" & Trim(Mid(Tipo_Comp.Text, 1, 2)) & "' AND Id_Letra_Factura = '" & Trim(Letra.Text) & "' AND Id_Centro_Emisor = " & Trim(Str(Val(Centro_Emisor.Text))) & " AND Id_Nro_Factura = " & Trim(Str(Val(Numero.Text))) & " "
    qy = qy & "AND Empresa = 2"
    rstDatos.Open qy, Db
    
    If rstDatos.EOF And rstDatos.BOF Then
        ExisteFacturaAImprimir = False
    Else
        ExisteFacturaAImprimir = True
    End If

End Function

Private Sub Cargar_Numero_Comprobante()
    Dim rstDatos As New ADODB.Recordset
    Centro_Emisor.Text = Formateado(Str(Val(cCentro_Emisor)), 0, 4, "0", False)
    
    If UCase(Mid(Condicion_Iva.Text, 1, 2)) = "RI" Then
        Letra.Text = "A"
        If UCase(Mid(Tipo_Comp.Text, 1, 2)) = "FC" Then
            qy = "SELECT MAX(Ultima_FC_A) FROM Empresa WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        ElseIf UCase(Mid(Tipo_Comp.Text, 1, 2)) = "NC" Then
            qy = "SELECT MAX(Ultima_NC_A) FROM Empresa WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        ElseIf UCase(Mid(Tipo_Comp.Text, 1, 2)) = "ND" Then
            qy = "SELECT MAX(Id_Nro_Factura) FROM Factura_Venta WHERE Id_Tipo_Factura = 'ND' AND Id_Letra_Factura = 'B' AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        End If
    Else
        Letra.Text = "B"
        If UCase(Mid(Tipo_Comp.Text, 1, 2)) = "FC" Then
            qy = "SELECT MAX(Ultima_FC_B) FROM Empresa WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        ElseIf UCase(Mid(Tipo_Comp.Text, 1, 2)) = "NC" Then
            qy = "SELECT MAX(Ultima_NC_B) FROM Empresa WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        ElseIf UCase(Mid(Tipo_Comp.Text, 1, 2)) = "ND" Then
            qy = "SELECT MAX(Id_Nro_Factura) FROM Factura_Venta WHERE Id_Tipo_Factura = 'ND' AND Id_Letra_Factura = 'B' AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        End If
    End If
    rstDatos.Open qy, Db
    
    Numero.Text = 1
    If Not rstDatos(0) = "" Then Numero.Text = rstDatos(0) + 1
    Numero.Text = Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
    
    Set rstDatos = Nothing
End Sub

Private Sub Ancho_GotFocus()
    Ancho.Text = Trim(Ancho.Text)
    Ancho.SelStart = 0
    Ancho.SelText = ""
    Ancho.SelLength = Len(Ancho.Text)
End Sub

Private Sub Ancho_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_LostFocus()
    Ancho.Text = Formateado(Str(Val(Ancho.Text)), 2, 9, " ", False)
'    Precio_Venta.Text = Aderir_Recargos(Ancho.Text, "")
    Cantidad_LostFocus
End Sub

Private Sub Articulo_GotFocus()
    Articulo.Text = Trim(Articulo.Text)
    Articulo.SelStart = 0
    Articulo.SelText = ""
    Articulo.SelLength = Len(Articulo.Text)
End Sub

Private Sub Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo_LostFocus
End Sub

Private Sub Articulo_LostFocus()
    If Val(Articulo.Text) > 0 And Val(Mid(Id_Rubro.Text, 31)) > 0 Then
        Leer_Articulo
    ElseIf Trim(UCase(Id_Rubro.Text)) = "M" Then
        Descripcion.Enabled = True
        Presentacion.Enabled = True
        Cuenta.Enabled = True
        Rec_Desc.Enabled = True
        Cantidad.Enabled = True
        Total.Enabled = True
        
        Descripcion.SetFocus
    End If
End Sub

Private Sub Leer_Articulo()
    
    If Val(Articulo.Text) > 0 And Val(Mid(Id_Rubro.Text, 31)) > 0 Then
        qy = "SELECT * FROM Articulo WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text)))
        qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Mid(Id_Rubro.Text, 31))))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "El Art�culo es inexistente...", vbInformation, "Atenci�n.!"
            Id_Rubro.Text = ""
            Articulo.Text = ""
            Id_Rubro.SetFocus
        Else
            Habilitar_Campos
            Mostrar_Articulo
            BuscarAlertas Rs
        End If
    Else
        Articulo.Text = ""
        Id_Rubro.SetFocus
    End If
End Sub

Private Sub Habilitar_Campos()
    If Val(Mid(Presentacion.Text, 1, 1)) = 3 Or Val(Mid(Presentacion.Text, 1, 1)) = 4 Then
        Espesor.Enabled = True
        Largo.Enabled = True
    End If
    Rec_Desc.Enabled = True
    Cantidad.Enabled = True
End Sub

Private Sub Mostrar_Articulo()
    Descripcion.Text = Rs.Fields("Descripcion")
    
    If Trim(UCase(Tipo_Cliente.Text)) = "P" Then
        Presentacion.ListIndex = Val(Rs.Fields("Presentacion_Pub"))
        If Val(Plazo.Text) = 0 Then
            Precio_Venta.Text = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Pub"))) / 100) + Val(Rs.Fields("Precio_Compra"))
        Else
            Precio_Venta.Text = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Pub"))) / 100) + Val(Rs.Fields("Precio_Compra"))
            Precio_Venta.Text = ((Val(Precio_Venta.Text) * Val(Rs.Fields("Rec_CtaCte_Pub"))) / 100) + Val(Precio_Venta)
        End If
    Else
        Presentacion.ListIndex = Val(Rs.Fields("Presentacion_Rev"))
        
        If Val(Plazo.Text) = 0 Then
            Precio_Venta.Text = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Rev"))) / 100) + Val(Rs.Fields("Precio_Compra"))
        Else
            Precio_Venta.Text = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Rev"))) / 100) + Val(Rs.Fields("Precio_Compra"))
            Precio_Venta.Text = ((Val(Precio_Venta.Text) * Val(Rs.Fields("Rec_CtaCte_Rev"))) / 100) + Val(Precio_Venta)
        End If
    End If
    'Saco el IVA
    'Precio_Venta.Text = ((Val(Precio_Venta.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Venta.Text)
    Precio_Venta.Text = Formateado(Str(Val(Precio_Venta.Text)), 4, 10, " ", False)
    
    Precio_Venta.Tag = Val(Precio_Venta.Text)
    Cantidad.Tag = Rs.Fields("Existencia")
    
    If Val(Espesor.Text) = 0 And Val(Ancho.Text) = 0 And Val(Largo.Text) = 0 Then
        Espesor.Text = Formateado(Str(Val(Rs.Fields("Espesor"))), 2, 9, " ", False)
        Ancho.Text = Formateado(Str(Val(Rs.Fields("Ancho"))), 2, 9, " ", False)
        Largo.Text = Formateado(Str(Val(Rs.Fields("Largo"))), 2, 9, " ", False)
    End If
    
    Cuenta.Text = Trim(Rs.Fields("Cuenta_Venta"))
    
    Presentacion_LostFocus 'Porque habilito los campos de medidas
    Mostrar_Medidas
    
    If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Or Presentacion.ListIndex = 8 Then
        Ancho.SetFocus
    Else
        
        'Precio_Venta.Enabled = True
        'Cantidad.SetFocus
        Rec_Desc.SetFocus
    End If
End Sub

Private Sub Mostrar_Medidas()
    If Presentacion.ListIndex = 4 Then
        Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
    Else
        If Presentacion.ListIndex = 5 Then
            Medida.Text = Largo.Text
        Else
            If Presentacion.ListIndex = 8 Then
                Medida.Text = Formateado(Str(Val(Ancho.Text * Largo.Text)), 2, 9, " ", False)
            Else
                Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
            End If
        End If
    End If
End Sub

Private Sub Articulos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo.SetFocus
End Sub

Private Sub Articulos_Encontrados_LostFocus()
    Id_Rubro.Text = Trim(Mid(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 32, 4))
    Articulo.Text = Trim(Mid(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 37))
    If Val(Articulo.Text) > 0 Then
        Leer_Rubro
        Leer_Articulo
    End If
    Articulos_Encontrados.Visible = False
End Sub

Private Sub Borrar_Click()
    If Trim(Id_Rubro.Text) <> "" Then
        If MsgBox("Desea Remover el Art�culo de la Factura ?.", vbYesNo + vbInformation, "Atenci�n.!") = vbYes Then
            List1.RemoveItem Confirma.Tag
            List1.Refresh
        End If
        
        Calcular_Total_Factura
        Termina_Click
    End If
End Sub

Private Sub Buscar_Articulos_Click()
    AbrirBusquedaArticulos
End Sub

Private Sub AbrirBusquedaArticulos()
    Load Fac_Vta_Bus
    Fac_Vta_Bus.Show vbModal
    
    If Fac_Vta_Bus.ArticuloSeleccionado > 0 And Fac_Vta_Bus.RubroSeleccionado > 0 Then
    
        Articulo.Text = Fac_Vta_Bus.ArticuloSeleccionado
        Id_Rubro.Text = Fac_Vta_Bus.RubroSeleccionado
        
        Fac_Vta.Id_Rubro.SetFocus
        SendKeys "{enter}"
        SendKeys "{tab}"
        SendKeys "{enter}"

    Else
        Id_Rubro.SetFocus
    End If
End Sub

Private Sub Buscar_Clientes_Click()
    Clientes_Encontrados.Visible = True
    Dim rstDatos As New ADODB.Recordset
        
    If Clientes_Encontrados.ListCount = 0 Then
        MousePointer = 11
        qy = "SELECT * FROM Cliente ORDER BY Nombre"
        rstDatos.Open qy, Db
        
        While Not rstDatos.EOF
            Clientes_Encontrados.AddItem Trim(rstDatos("Nombre")) + Space(30 - Len(Trim(rstDatos("Nombre")))) & " " & Trim(rstDatos("Id_Cliente"))
            rstDatos.MoveNext
        Wend
        
        MousePointer = 0
    End If
    Clientes_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Buscar_Cuentas_Click()
    Cuentas_Encontradas.Clear
    Cuentas_Encontradas.Visible = True
    
    qy = "SELECT * FROM Cuenta "
    qy = qy & "WHERE Recibe_Asiento = -1 "
    qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Cuentas_Encontradas.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6"))
        Rs.MoveNext
    Wend
    
    Cuentas_Encontradas.SetFocus
End Sub

Private Sub Cantidad_GotFocus()
    Cantidad.Text = Trim(Cantidad.Text)
    Cantidad.SelStart = 0
    Cantidad.SelText = ""
    Cantidad.SelLength = Len(Cantidad.Text)
End Sub

Private Sub Cantidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cantidad_LostFocus()
    If Val(Cantidad.Text) <= 999 Then
        If Val(Largo.Text) > 0 Then Largo_LostFocus
        
        Cantidad.Text = Formateado(Str(Val(Cantidad.Text)), 2, 10, " ", False)
        If Val(Cantidad.Text) > 0 Then
            Largo_LostFocus
            
            Medida.Text = Formateado(Str(Val(Val(Medida.Text) * Val(Cantidad.Text))), 2, 9, " ", False)
        End If
        
        Rec_Desc_GotFocus
        Rec_Desc_LostFocus
        
        Total_GotFocus
        Total_LostFocus
        
        If Confirma.Enabled = True Then Confirma.SetFocus
    Else
        MsgBox "La cantidad tiene que ser menor o igual a 999", vbCritical, "Atenci�n.!"
        Cantidad.Text = ""
        Cantidad.SetFocus
    End If
End Sub

Private Sub Cliente_GotFocus()
    Nombre.Enabled = False
    Direccion.Enabled = False
    Localidad.Enabled = False
    Condicion_Iva.Enabled = False
    Cuit.Enabled = False
    
    Cliente.Text = Trim(Cliente.Text)
    Cliente.SelStart = 0
    Cliente.SelText = ""
    Cliente.SelLength = Len(Cliente.Text)
End Sub

Private Sub Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: If Val(Cliente.Text) > 0 Or Trim(UCase(Cliente.Text)) = "M" Then SendKeys "{TAB}"
End Sub

Private Sub Cliente_LostFocus()
    If Val(Cliente.Text) > 0 Then
        Leer_Cliente
    ElseIf Trim(UCase(Cliente.Text)) = "M" Then
        Habilitar_Campo_Cliente
        Nombre.SetFocus
    End If
    
    Cargar_Numero_Comprobante
    
    If Id_Empresa = 2 Then
        Numero.Enabled = True
    End If
    
End Sub

Private Sub Habilitar_Campo_Cliente()
    Nombre.Enabled = True
    Direccion.Enabled = True
    Localidad.Enabled = True
    Condicion_Iva.Enabled = True
    Cuit.Enabled = True
End Sub

Private Sub Leer_Cliente()
    If Val(Cliente.Text) > 0 Then
        qy = "SELECT * FROM Cliente WHERE Id_Cliente = " & Trim(Str(Val(Cliente.Text)))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "El Cliente es inexistente...", vbInformation, "Atenci�n.!"
            Cliente.Text = ""
            Cliente.SetFocus
        Else
            Mostrar_Campo_Cliente
        End If
    Else
        Cliente.Text = ""
        Cliente.SetFocus
    End If
End Sub

Private Sub Mostrar_Campo_Cliente()
    Nombre.Text = Trim(Rs.Fields("Nombre"))
    Direccion.Text = Rs.Fields("Domicilio")
    Localidad.Text = Rs.Fields("Codigo_Postal")
    Condicion_Iva.Text = Rs.Fields("Condicion_Iva")
    Cuit.Text = Rs.Fields("Nro")
    
    If UCase(Condicion_Iva.Text) = "CF" Then
        Condicion_Iva.ListIndex = 0
    ElseIf UCase(Condicion_Iva.Text) = "RI" Then
        Condicion_Iva.ListIndex = 1
    ElseIf UCase(Condicion_Iva.Text) = "NI" Then
        Condicion_Iva.ListIndex = 2
    ElseIf UCase(Condicion_Iva.Text) = "MT" Then
        Condicion_Iva.ListIndex = 3
    ElseIf UCase(Condicion_Iva.Text) = "ET" Then
        Condicion_Iva.ListIndex = 4
    ElseIf UCase(Condicion_Iva.Text) = "NC" Then
        Condicion_Iva.ListIndex = 5
    End If
    
    If Val(Rs.Fields("Categoria")) = 1 Then
        Tipo_Cliente.Text = "R"
    ElseIf Val(Rs.Fields("Categoria")) = 2 Then
        Tipo_Cliente.Text = "P"
    End If
    
    'Calculo los Saldos
    If Val(Rs.Fields("Estado")) < 2 Then
        Cliente.Tag = Rs.Fields("Saldo_Autorizado")
        qy = "SELECT SUM(Debe - Haber) FROM CtaCte_Cliente WHERE Id_Cuenta = " & Trim(Str(Val(Cliente.Text)))
        AbreRs
        
        If Not Rs.Fields(0) = "" Then
            If Val(Rs.Fields(0)) >= Val(Cliente.Tag) Then
                MsgBox "El Cliente ah exedido su cr�dito, por lo tanto no puede tener movimientos en Cta. Cte.", vbInformation, "Atenci�n.!"
                Plazo.Text = "C"
                Plazo_LostFocus
                Plazo.Enabled = False
            Else
                Plazo.Enabled = True
            End If
        End If
    Else
        Cliente.Tag = "CUENTA_SUSPENDIDA"
    End If
    
    'Busco la localidad
    If Val(Localidad.Text) > 0 Then
        qy = "SELECT * FROM Localidad WHERE Id_Localidad = " & Trim(Str(Val(Localidad.Text)))
        AbreRs
        
        If Not Rs.EOF Then
            Localidad.Text = Trim(Rs.Fields("Id_Localidad")) + Space(5 - Len(Trim(Rs.Fields("Id_LocalidaD")))) & " " & Trim(Rs.Fields("Localidad"))
        End If
    End If
End Sub

Private Sub Clientes_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Clientes_Encontrados_LostFocus
End Sub

Private Sub Clientes_Encontrados_LostFocus()
    Cliente.Text = Mid(Clientes_Encontrados.List(Clientes_Encontrados.ListIndex), 32)
    Clientes_Encontrados.Visible = False
    If Val(Cliente.Text) > 0 Then
        
        Leer_Cliente
        Cargar_Numero_Comprobante
        
        If Id_Empresa = 2 Then
            Numero.SetFocus
        Else
            Tipo_Cliente.SetFocus
        End If
        
    Else
        Cliente.SetFocus
    End If
End Sub

Private Sub cmdLimpiarLista_Click()
    
    List1.Clear
    Calcular_Total_Factura

End Sub

Private Sub Condicion_Iva_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Condicion_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    If List1.ListCount < 30 Then
        Dim Unidad As String
        
        
        Txt = Formateado(Str(Val(Mid(Id_Rubro.Text, 31))), 0, 4, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Articulo.Text)), 0, 5, " ", False) & " "
        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
            Txt = Txt & Trim(Mid(Trim(Mid(Descripcion.Text, 1, 28)) & "x" & Trim(Val(Ancho.Text)) & "x" & Trim(Val(Largo.Text)), 1, 37)) + Space(37 - Len(Trim(Mid(Trim(Mid(Descripcion.Text, 1, 28)) & "x" & Trim(Val(Ancho.Text)) & "x" & Trim(Val(Largo.Text)), 1, 37)))) & " "
        Else
            Txt = Txt & Trim(Descripcion.Text) + Space(37 - Len(Trim(Descripcion.Text))) & " "
        End If
        Txt = Txt & IIf(Val(Medida.Text) > 0, Formateado(Str(Val(Medida.Text)), 2, 7, " ", False) & " ", Space(8))
        
        If Val(Mid(Presentacion.Text, 1, 1)) = 0 Then
            Txt = Txt & Space(3) & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 1 Then
            Txt = Txt & "Us." & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 2 Then
            Txt = Txt & "Lts" & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 3 Then
            Txt = Txt & "P2 " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 4 Then
            Txt = Txt & "M2 " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 5 Then
            Txt = Txt & "Ml " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 6 Then
            Txt = Txt & "Ml " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 7 Then
            Txt = Txt & "Kgs" & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 8 Then
            Txt = Txt & "AxL" & " "
        End If
        
        If UCase(Trim(Letra.Text)) = "B" Then
            Final_Unitario.Text = (((Val(Final_Unitario.Text) * Val(Alicuota_Iva)) / 100) + Val(Final_Unitario.Text))
            'Aderir_Recargos
            Total.Text = ((Val(Cantidad.Text) * Val(Final_Unitario.Text)))
        End If
            
        
        Txt = Txt & Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Cantidad.Text)), 4, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Total.Text)), 2, 10, " ", False) & "   "  '3 espacios porque empieza la carga de datos que no se ve.-
            
        Txt = Txt & Formateado(Str(Val(Cantidad.Tag)), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Espesor.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Ancho.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Largo.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Precio_Venta.Text)), 4, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Mid(Presentacion.Text, 1, 2))), 0, 1, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rec_Desc.Text)), 2, 10, " ", False) & " "
        Txt = Txt & Trim(Cuenta.Text) + Space(30 - Len(Trim(Cuenta.Text))) & " "
        Txt = Txt & Formateado(Str(Val(Total.Text) / Val("1." & Val(Alicuota_Iva))), 2, 10, " ", False)
        
        If Confirma.Tag = "" Then
            List1.AddItem Txt
        Else
            List1.RemoveItem Val(Confirma.Tag)
            List1.Refresh
            List1.AddItem Txt, List1.Tag
        End If
        
        Calcular_Total_Factura
    Else
        MsgBox "El M�ximo permitido son 30 art�culos por Factura.-", vbCritical, "Atenci�n.!"
    End If
    
    Termina_Click
End Sub

Private Sub Calcular_Total_Factura()
    Dim i As Long
    
    i = 0
    Neto_Gravado.Text = 0
    Exento.Text = 0
    Sobre_Tasa.Text = 0
    Retencion_Iva.Text = 0
    Retencion_IIBB.Text = 0
    Iva_Inscrip.Text = 0
    Neto_Gravado.Text = 0
    Sub_Total.Text = 0
    Total_Factura.Text = ""
    
    For i = 0 To List1.ListCount - 1
        If Trim(UCase(Letra.Text)) = "A" Then
            Total_Factura.Tag = Val(Mid(List1.List(i), 73, 10))
            Total_Factura.Tag = ((Val(Total_Factura.Tag) * Val(Alicuota_Iva)) / 100) + Val(Total_Factura.Tag)
            Total_Factura.Tag = Val(Total_Factura.Tag) * Val(Mid(List1.List(i), 84, 10))
            
            Total_Factura.Text = Val(Total_Factura.Text) + Val(Total_Factura.Tag)
        Else
            Total_Factura.Tag = Val(Mid(List1.List(i), 95, 10)) / Val(Mid(List1.List(i), 84, 10))
            Total_Factura.Tag = Val(Total_Factura.Tag) / Val("1." & Alicuota_Iva)
            
            Total_Factura.Tag = ((Val(Total_Factura.Tag) * Val(Alicuota_Iva)) / 100) + Val(Total_Factura.Tag)
            Total_Factura.Tag = Val(Total_Factura.Tag) * Val(Mid(List1.List(i), 84, 10))
            
            Total_Factura.Text = Val(Total_Factura.Text) + Val(Total_Factura.Tag)
        End If
    Next
    
    Neto_Gravado.Text = Formateado(Str(Val(Val(Total_Factura.Text) / Val("1." & Alicuota_Iva))), 2, 9, " ", False)
    
    Iva_Inscrip.Text = (Val(Total_Factura.Text) - Val(Neto_Gravado.Text))
    Iva_Inscrip.Text = Formateado(Str(Val(Iva_Inscrip.Text)), 2, 9, " ", False)
    
    Sub_Total.Text = Val(Neto_Gravado.Text) + Val(Exento.Text)
    Sub_Total.Text = Formateado(Str(Val(Sub_Total.Text)), 2, 9, " ", False)
    
    Exento.Text = Formateado(Str(Val(Exento.Text)), 2, 9, " ", False)
    Sobre_Tasa.Text = Formateado(Str(Val(Sobre_Tasa.Text)), 2, 9, " ", False)
    Retencion_Iva.Text = Formateado(Str(Val(Retencion_Iva.Text)), 2, 9, " ", False)
    Retencion_IIBB.Text = Formateado(Str(Val(Retencion_IIBB.Text)), 2, 9, " ", False)
    
    Total_Factura.Text = Val(Sub_Total.Text) + Val(Iva_Inscrip.Text) + Val(Sobre_Tasa.Text) + Val(Retencion_Iva.Text) + Val(Retencion_IIBB.Text)
    Total_Factura.Text = Formateado(Str(Val(Total_Factura.Text)), 2, 10, " ", False)
End Sub

Private Sub Cuenta_GotFocus()
    Cuenta.Text = Trim(Cuenta.Text)
    Cuenta.SelStart = 0
    Cuenta.SelText = ""
    Cuenta.SelLength = Len(Cuenta.Text)
End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_LostFocus()
    If Val(Cuenta.Text) = 0 Then Cuenta.Text = "4.1.1.0.0.0"
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuentas_Encontradas_LostFocus
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Cuenta.Text = Trim(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 31))
    If Precio_Venta.Enabled = True Then
        Precio_Venta.SetFocus
    Else
        Largo.SetFocus
    End If
    Cuentas_Encontradas.Visible = False
End Sub

Private Sub Cuit_GotFocus()
    Cuit.Text = Trim(Cuit.Text)
    Cuit.SelStart = 0
    Cuit.SelText = ""
    Cuit.SelLength = Len(Cuit.Text)
End Sub

Private Sub Cuit_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuit_LostFocus()
    Cuit.Text = ValidarCuit(Cuit.Text)
    
    If Cuit.Text = "error" Then
        MsgBox "El Cuit Ingresado no es correcto, verifique e ingrese nuevamente.", vbInformation, "Atenci�n.!"
        Cuit.Text = ""
        Cuit.SetFocus
    End If
End Sub

Private Sub Descripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Descripcion_LostFocus()
    Descripcion.Text = FiltroCaracter(Descripcion.Text)
    Descripcion.Text = UCase(Descripcion.Text)
End Sub

Private Sub Direccion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Direccion_LostFocus()
    Direccion.Text = FiltroCaracter(Direccion.Text)
    Direccion.Text = UCase(Direccion.Text)
End Sub

Private Sub Espesor_GotFocus()
    Espesor.Text = Trim(Espesor.Text)
    Espesor.SelStart = 0
    Espesor.SelText = ""
    Espesor.SelLength = Len(Espesor.Text)
End Sub

Private Sub Espesor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Espesor_LostFocus()
    Espesor.Text = Formateado(Str(Val(Espesor.Text)), 2, 9, " ", False)
End Sub

Private Sub Facturar_Presupuesto_Click()
    
    frmBuscadorComprobantes.TipoComprobante = FacturaVenta
    frmBuscadorComprobantes.ClienteId = Val(Cliente.Text)
    frmBuscadorComprobantes.Show vbModal
    
    If (frmBuscadorComprobantes.Confirma) Then
        
        DatosImportados = True
        OrigenDatosImportados = frmBuscadorComprobantes.TipoComprobanteSeleccionado
        CodigoDatosImportados = frmBuscadorComprobantes.NumerosComprobantesSeleccionados
        
        CargarArticulosImportados frmBuscadorComprobantes.ResultItems, frmBuscadorComprobantes.ActualizarPrecios
        
        Numero.Tag = "Alta"
        
        Calcular_Total_Factura
        Agregar_Click
    
    End If

End Sub

Private Sub Fecha_GotFocus()
    Fecha.Text = Fecha_Fiscal
    Fecha.SelStart = 0
    Fecha.SelText = ""
    Fecha.SelLength = 10
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
    
    If Trim(Fecha.Text) = "error" Then
        MsgBox "La Fecha ingresada es incorrecta, ingrese nuevamente.", vbInformation, "Atenci�n.!"
        Fecha.SetFocus
    End If
End Sub

Private Sub Final_Unitario_GotFocus()
    Final_Unitario.Text = Trim(Final_Unitario.Text)
    Final_Unitario.SelStart = 0
    Final_Unitario.SelText = ""
    Final_Unitario.SelLength = Len(Final_Unitario.Text)
End Sub

Private Sub Final_Unitario_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Final_Unitario_LostFocus()
    Precio_Venta_LostFocus

    Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
    Final_Unitario.Enabled = False
    Final_Unitario.BackColor = 14737632
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Emisi�n de Comprobantes de Venta.-"
End Sub

Private Sub Form_Load()
    Dim i As Long

    Me.Top = (Screen.Height - Me.Height) / 18
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Tipo_Comp.AddItem "FC - FACTURA        "
    Tipo_Comp.AddItem "ND - NOTA DE D�BITO "
    Tipo_Comp.AddItem "NC - NOTA DE CR�DITO"
    Tipo_Comp.ListIndex = 0
    
    For i = 1 To 6
        Condicion_Iva.AddItem Tipo_Iva(i)
    Next
    
    i = 0
    
    For i = 0 To 8
        Presentacion.AddItem Articulo_Presentacion(i)
    Next
    
    Cargar_Combos
End Sub

Private Sub Cargar_Combos()

    qy = "Select * From Articulo_Alicuota Where Alicuota = 21 And Alicuota = 10.5 Order By Denominacion"
    AbreRs

    While Not Rs.EOF

        Alicuota.AddItem Trim(Rs.Fields("Alicuota"))
        Rs.MoveNext

    Wend

End Sub

Private Sub Cargar_Rubro()

    Dim rstDatos As New ADODB.Recordset

    qy = "SELECT * FROM Rubro ORDER BY Denominacion"
    rstDatos.Open qy, Db
    
    While Not rstDatos.EOF
        Id_Rubro.AddItem Trim(Mid(rstDatos("Denominacion"), 1, 30)) + Space(30 - Len(Trim(Mid(rstDatos("Denominacion"), 1, 30)))) & " " & Trim(rstDatos("Id_Rubro"))
        rstDatos.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
    Unload frmAlertas
End Sub

Private Sub Grabar_Click()
    
    If Val(List1.ListCount) > 0 Then
        
        Load Fac_Vta_Obser
        Fac_Vta_Obser.Show vbModal
        
        If (Fac_Vta_Obser.OperacionConfirmada) Then
        
            Borrar_Campo_Importe
            Borrar_Campo_Captura
            Borrar_Campo
        
        End If
        
        
    Else
    
        Salir_Click
    
    End If
    
End Sub

Private Sub Id_Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        
        If Trim(UCase(Id_Rubro.Text)) = "M" Then
            Articulo_LostFocus
        ElseIf Val(Id_Rubro.Text) > 0 Or Val(Mid(Id_Rubro.Text, 31)) > 0 Then
            
            If Val(Mid(Id_Rubro.Text, 31)) > 0 Or Val(Id_Rubro.Text) > 0 Then
                qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & IIf(Val(Id_Rubro.Text) > 0, Val(Id_Rubro.Text), Trim(Str(Val(Mid(Id_Rubro.Text, 31)))))
                AbreRs
                
                If Rs.EOF Then
                    MsgBox "El Rubro especificado es inexistente...", vbCritical, "Atenci�n.!"
                    Id_Rubro.Text = ""
                    Id_Rubro.SetFocus
                    
                Else
                    Id_Rubro.Text = Trim(Mid(Rs.Fields("Denominacion"), 1, 30)) + Space(30 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 30)))) & " " & Trim(Rs.Fields("Id_Rubro"))
                    
                End If
            Else
                SendKeys "{tab}"
            End If
        End If
    End If
End Sub

Private Sub Id_Rubro_LostFocus()
    Id_Rubro.Text = UCase(Id_Rubro.Text)
End Sub

Private Sub Largo_GotFocus()
    Largo.Text = Trim(Largo.Text)
    Largo.SelStart = 0
    Largo.SelText = ""
    Largo.SelLength = Len(Largo.Text)
End Sub

Private Sub Largo_KeyPress(KeyAscii As Integer)
     If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_LostFocus()
    Largo.Text = Formateado(Str(Val(Largo.Text)), 2, 9, " ", False)
    
    If Val(Largo.Text) > 0 Then
        Mostrar_Medidas
        If Val(Mid(Presentacion.Text, 1, 1)) = 3 Then 'P2
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 4 Then 'M2
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 5 Then 'ML
            Medida.Text = Largo.Text & " " & Trim(Mid(Presentacion.Text, 6))
            Final_Unitario.Text = (((Val(Espesor.Text) * Val(Ancho.Text)) * 0.2734) * Val(Largo)) * Val(Precio_Venta.Text)
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 8 Then 'ML
            Medida.Text = Formateado(Str(Val(Largo.Text * Ancho.Text)), 2, 9, " ", False)
        End If
    End If
    
    If Val(Mid(Presentacion.Text, 1, 1)) = 3 Or Val(Mid(Presentacion.Text, 1, 1)) = 4 Or Val(Mid(Presentacion.Text, 1, 1)) = 8 Then
        Final_Unitario.Text = Val(Precio_Venta.Text) * Val(Medida.Text)
    End If
    Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
    Final_Unitario.Tag = Val(Final_Unitario.Text)
    
    '/// ATENCI�N SE REALIZA UN REMARQUE EN LOS ART�CULOS EN ESTE PROCEDIMIENTO!!
    Aderir_Recargos
        
    Final_Unitario.Tag = Val(Final_Unitario.Text)
    Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
End Sub

Private Sub List1_DblClick()
    
    If DatosImportados Then
    
        Confirma.Visible = False
        
    End If
    
    If Trim(Mid(List1.List(List1.ListIndex), 1, 5)) <> "" Then
    
        Agregar_Click

        If Trim(Str(Val(Mid(List1.List(List1.ListIndex), 1, 5)))) > 0 Then
            Id_Rubro.Text = Mid(List1.List(List1.ListIndex), 1, 4)
            Articulo.Text = Mid(List1.List(List1.ListIndex), 6, 5)
            Leer_Rubro
            Leer_Articulo
        Else
            Id_Rubro.Text = "M"
            Descripcion.Text = Trim(Mid(List1.List(List1.ListIndex), 12, 31))
            Presentacion.ListIndex = Str(Val(Mid(List1.List(List1.ListIndex), 150, 1)))
            Descripcion.Enabled = True
            Descripcion.SetFocus
        End If
        
        Rec_Desc.Text = Mid(List1.List(List1.ListIndex), 161, 10)
        Cuenta.Text = Mid(List1.List(List1.ListIndex), 172, 30)
        Precio_Venta.Text = Mid(List1.List(List1.ListIndex), 149, 9)
        Cantidad.Text = Mid(List1.List(List1.ListIndex), 84, 10)
        Espesor.Text = Mid(List1.List(List1.ListIndex), 119, 9)
        Ancho.Text = Mid(List1.List(List1.ListIndex), 129, 9)
        Largo.Text = Mid(List1.List(List1.ListIndex), 139, 9)
        List1.Tag = Val(List1.ListIndex)
        
        If Largo.Enabled = True Then
            'Largo_LostFocus
        Else
            Precio_Venta_LostFocus
        End If
        
        Rec_Desc_LostFocus
        Cantidad_LostFocus
        Total_GotFocus
        Total_LostFocus
        
        Borrar.Enabled = True
        Confirma.Tag = List1.ListIndex
    End If
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        List1_DblClick
    End If
End Sub

Private Sub Localidad_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Localidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_LostFocus()
    Nombre.Text = FiltroCaracter(Nombre.Text)
    Nombre.Text = UCase(Nombre.Text)
End Sub

Private Sub Numero_GotFocus()
    Numero.SelStart = 0
    Numero.SelLength = Len(Numero.Text)
End Sub

Private Sub Numero_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Plazo_GotFocus()
    If Trim(Plazo.Text) = "" Or Val(Plazo.Text) = 0 Then Plazo.Text = "C"
    Plazo.Text = Trim(Plazo.Text)
    Plazo.SelStart = 0
    Plazo.SelText = ""
    Plazo.SelLength = Len(Plazo.Text)
End Sub

Private Sub Plazo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Plazo_LostFocus()
    'Plazo.Text = Val(Plazo.Text)
    
    If Cliente.Tag = "CUENTA_SUSPENDIDA" And Val(Plazo.Text) > 0 Then
        MsgBox "El Cliente tiene su CUENTA SUSPENDIDA, para poder hacer una operaci�n en cuenta corriente debe habilitar la misma.", vbInformation, "Atenci�n.!"
        Plazo.Text = ""
        Plazo.SetFocus
    Else
        
        If Val(Plazo.Text) > 0 Then
            Vto.Text = DateAdd("d", Plazo.Text, Fecha.Text)
            Agregar_Click
        ElseIf Trim(UCase(Plazo.Text)) = "C" Then
            Plazo.Text = 0
            Vto.Text = Fecha.Text
            Agregar_Click
        Else
            MsgBox "Debe ingresar 'C' para especificar que la " & Trim(Mid(Tipo_Comp.Text, 5)) & " es de contado, o bien, el plazo en d�as al vencimiento.-", vbInformation, "Atenci�n.!"
            Plazo.Text = ""
            Plazo.SetFocus
        End If

    End If
End Sub

Private Sub Presentacion_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Presentacion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Presentacion_LostFocus()
    If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Or Presentacion.ListIndex = 8 Then
        Precio_Venta.BackColor = 14737632
        Precio_Venta.Enabled = False
        Largo.Enabled = True
        Largo.BackColor = vbWhite
        Ancho.Enabled = True
        Ancho.BackColor = vbWhite
        'Precio_Unidad.Enabled = True
        If Trim(UCase(Id_Rubro.Text)) = "M" Then
            Precio_Venta.Enabled = True
            Precio_Venta.BackColor = vbWhite
            If Presentacion.ListIndex <> 8 Then
                Espesor.BackColor = vbWhite
                Espesor.Enabled = True
            End If
            If Trim(Cuenta.Text) = "" And Cuenta.Enabled = True Then
                Cuenta.SetFocus
            Else
                'Espesor.SetFocus
                Precio_Venta.SetFocus
            End If
        End If
    Else
        Precio_Venta.BackColor = vbWhite
        Precio_Venta.Enabled = True
        Largo.Enabled = False
        Largo.BackColor = 14737632
        Ancho.BackColor = 14737632
        Ancho.Enabled = False
        Espesor.Enabled = False
        Espesor.BackColor = 14737632
        'Precio_Unidad.Enabled = False
    End If
End Sub

Private Sub Rec_Desc_GotFocus()
    'If Val(Rec_Desc.Text) > 0 Then
        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
            Largo_LostFocus
        Else
            Precio_Venta_LostFocus
        End If
    'End If
    Rec_Desc.Text = Trim(Rec_Desc.Text)
    Rec_Desc.SelStart = 0
    Rec_Desc.SelText = ""
    Rec_Desc.SelLength = Len(Rec_Desc.Text)
End Sub

Private Sub Rec_Desc_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rec_Desc_LostFocus()
    Dim Valor As Single
    
    If UCase(Mid(Condicion_Iva.Text, 1, 2)) = "RI" Then
        Valor = Val(Final_Unitario.Tag) / Val("1." & Val(Alicuota_Iva))
    Else
        Valor = Val(Final_Unitario.Tag)
    End If
    
    If Val(Rec_Desc.Text) <> 0 Then
        If Val(Rec_Desc.Text) > 0 Then
            Final_Unitario.Text = ((Val(Valor) * Val(Rec_Desc.Text)) / 100) + Val(Valor)
        ElseIf Val(Rec_Desc.Text) < 0 Then
            Final_Unitario.Text = Val(Valor) - ((Val(Valor) * (Val(Rec_Desc.Text) * -1)) / 100)
        End If
        
        If UCase(Mid(Condicion_Iva.Text, 1, 2)) = "RI" Then Final_Unitario.Text = ((Val(Final_Unitario.Text) * Val(Alicuota_Iva)) / 100) + Val(Final_Unitario.Text)
        Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
    ElseIf Val(Rec_Desc.Text) = 0 Then
        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
            Largo_LostFocus
        Else
            Precio_Venta_LostFocus
        End If
    End If
    
    Rec_Desc.Text = Formateado(Str(Val(Rec_Desc.Text)), 2, 10, " ", False)
End Sub

Private Sub Salir_Click()
    If Cliente.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Public Sub Borrar_Campo()
    
    Unload Fac_Vta_Bus
    
    DatosImportados = False
    CodigoDatosImportados = Empty
        
    Grabar.Enabled = False
    Marco_Factura_Item.Enabled = False
    
    Cliente.Enabled = True
    Tipo_Cliente.Enabled = True

    Tipo_Comp.ListIndex = 0
    Cliente.Text = ""
    Cliente.Tag = ""
    Vto.Text = ""
    Cuit.Text = ""
    Nombre.Text = ""
    List1.Clear
    Condicion_Iva.Text = ""
    Letra.Text = ""
    Centro_Emisor.Text = ""
    Numero.Text = ""
    Direccion.Text = ""
    Localidad.Text = ""
    Plazo.Text = ""
    Tipo_Cliente.Text = ""
    Vto.Text = ""
    
    Borrar_Campo_Importe
    Cliente.SetFocus
    
End Sub

Private Sub Borrar_Campo_Importe()
    Neto_Gravado.Text = ""
    Exento.Text = ""
    Iva_Inscrip.Text = ""
    Sub_Total.Text = ""
    Sobre_Tasa.Text = ""
    Retencion_Iva.Text = ""
    Retencion_IIBB.Text = ""
    Total_Factura.Text = ""
End Sub

Private Sub Termina_Click()
    If Articulo.Text = "" And Id_Rubro.Text = "" Then
        Marco_Factura_Item.Enabled = True
        Marco_Captura.Visible = False
        Botonera.Enabled = True
        Salir.Cancel = True
        
        Grabar.Enabled = True
        Grabar.SetFocus
    Else
        Borrar_Campo_Captura
        AbrirBusquedaArticulos
    End If
End Sub

Private Sub Borrar_Campo_Captura()
    Unload frmAlertas
    
    List1.Tag = ""
    Id_Rubro.Text = ""
    Articulo.Text = ""
    Descripcion.Text = ""
    Presentacion.Text = ""
    Cuenta.Text = ""
    Final_Unitario.Text = ""
    Final_Unitario.Tag = ""
    Precio_Venta.Text = ""
    Precio_Venta.BackColor = 14737632
    Espesor.BackColor = 14737632
    Ancho.BackColor = 14737632
    Largo.BackColor = 14737632
    Precio_Venta.BackColor = 14737632
    Precio_Venta.Tag = ""
    Rec_Desc.Text = ""
    Confirma.Tag = ""
    Cantidad.Text = ""
    Cantidad.Tag = ""
    Total.Text = ""
    
    Espesor.Text = ""
    Largo.Text = ""
    Ancho.Text = ""
    Medida.Text = ""
    
    Espesor.Enabled = False
    Largo.Enabled = False
    Rec_Desc.Enabled = False
    Cantidad.Enabled = False
    Borrar.Enabled = False
End Sub

Private Sub Tipo_Cliente_GotFocus()
    Tipo_Cliente.SelStart = 0
    Tipo_Cliente.SelText = ""
    Tipo_Cliente.SelLength = Len(Tipo_Cliente.Text)
End Sub

Private Sub Tipo_Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Tipo_Cliente_LostFocus()
    Tipo_Cliente.Text = UCase(Tipo_Cliente.Text)
    If Not (Trim(UCase(Tipo_Cliente.Text)) = "P" Or Trim(UCase(Tipo_Cliente.Text)) = "R") Then Tipo_Cliente.Text = "error"
End Sub

Private Sub Tipo_Comp_GotFocus()
    Fac_Vta.Refresh
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Tipo_Comp_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Total_Change()
    If Val(Total.Text) > 0 And Val(Cantidad.Text) > 0 And (Trim(UCase(Id_Rubro.Text)) = "M" Or (Val(Articulo.Text) > 0 And Val(Mid(Id_Rubro.Text, 31)) > 0)) Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Total_GotFocus()
    Total.Text = Val(Final_Unitario.Text) * Val(Cantidad.Text)
    
    Total.SelStart = 0
    Total.SelText = ""
    Total.SelLength = Len(Total.Text)
End Sub

Private Sub Total_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Total_LostFocus()
    Total.Text = Formateado(Str(Val(Total.Text)), 2, 10, " ", False)
End Sub

Private Sub Vto_GotFocus()
    Vto.SelStart = 0
    Vto.SelText = ""
    Vto.SelLength = Len(Vto.Text)
End Sub

Private Sub Vto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Vto_LostFocus()
    Vto.Text = ValidarFecha(Vto.Text)
End Sub

Private Sub Aderir_Recargos()
    Dim Recargo  As Boolean
    Recargo = False
    Dim Precio As Single
    
    qy = "SELECT * FROM Articulo WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " "
    qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Mid(Id_Rubro.Text, 31))))
    AbreRs
    
    If Not Rs.EOF Then
                
        Precio = Val(Final_Unitario.Text)
        
        If Val(Ancho.Text) < Val(Rs.Fields("Ancho_Menor")) Then
            Precio = ((Val(Precio) * Val(Rs.Fields("Rec_Ancho_Menor"))) / 100) + Val(Precio)
        End If
        If Val(Ancho.Text) > Val(Rs.Fields("Ancho_Mayor")) Then
            Precio = ((Val(Precio) * Val(Rs.Fields("Rec_Ancho_Mayor"))) / 100) + Val(Precio)
        End If
        If Val(Largo.Text) < Val(Rs.Fields("Largo_Menor")) Then
            Precio = ((Val(Precio) * Val(Rs.Fields("Rec_Largo_Menor"))) / 100) + Val(Precio)
        End If
        If Val(Largo.Text) > Val(Rs.Fields("Largo_Mayor")) Then
            Precio = ((Val(Precio) * Val(Rs.Fields("Rec_Largo_Mayor"))) / 100) + Val(Precio)
        End If
        
        Final_Unitario.Tag = Val(Precio)
        Final_Unitario.Text = Val(Final_Unitario.Tag)
        Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
        
    End If ' Termino el Procedimiento.-
End Sub

Private Sub Precio_Venta_GotFocus()
    Precio_Venta.Text = Trim(Precio_Venta.Text)
    Precio_Venta.SelStart = 0
    Precio_Venta.SelText = ""
    Precio_Venta.SelLength = Len(Precio_Venta.Text)
End Sub

Private Sub Precio_Venta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Venta_LostFocus()
    Precio_Venta.Text = Formateado(Str(Val(Precio_Venta.Text)), 4, 10, " ", False)
    
    If Val(Precio_Venta.Text) > 0 Then
        If Largo.Enabled = True Then
            Largo_LostFocus
        Else
            'Final_Unitario.Text = ((Val(Precio_Venta.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Venta.Text)
            Final_Unitario.Text = Val(Precio_Venta.Text)
            Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
        End If
    ElseIf Val(Precio_Venta.Text) = 0 Then
        If (Val(Id_Rubro.Text) > 0 Or Trim(UCase(Id_Rubro.Text)) = "M") Then
            Precio_Venta.Text = ""
            
            Final_Unitario.Enabled = True
            Final_Unitario.BackColor = vbWhite
            Final_Unitario.Text = ""
            Final_Unitario.SetFocus
        End If
    End If
    
    Aderir_Recargos
End Sub

Private Sub Leer_Rubro()
    qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & Trim(Str(Val(Id_Rubro.Text)))
    AbreRs
                
    If Rs.EOF Then
        MsgBox "El Rubro especificado es inexistente...", vbCritical, "Atenci�n.!"
        Id_Rubro.Text = ""
        Id_Rubro.SetFocus
    Else
        'Id_Rubro.Text = Formateado(str(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & trim(Rs.Fields("Denominacion"))
        Id_Rubro.Text = Trim(Mid(Rs.Fields("Denominacion"), 1, 30)) + Space(30 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 30)))) & " " & Trim(Rs.Fields("Id_Rubro"))
    End If
End Sub

Private Function BuscarAlertas(rstArticulo As ADODB.Recordset)

    Dim strMensaje As String
    Dim rstDatos As New ADODB.Recordset
    
    If (Val(Rs("Existencia")) <= Val(Rs("Stock_Minimo"))) Then
        strMensaje = "El Art�culo seleccionado tiene una existencia por debajo del pto. de reposici�n" + Chr(13)
    End If
        
    qy = "Select Max(Fecha) As UltimaActualizacion From Articulo_Historial_Precio Where Id_Articulo = " & rstArticulo("Id_Articulo") & " "
    qy = qy + " And Id_Rubro = " & rstArticulo("Id_Rubro")
    rstDatos.Open qy, Db
    
    If Not rstDatos.EOF Then
        If Not rstDatos("UltimaActualizacion") = Empty Then
            If (DateDiff("d", Format(rstDatos("UltimaActualizacion"), "dd/MM/yyyy"), CDate(Fecha_Fiscal))) >= 30 Then
                strMensaje = strMensaje + "Verifique el precio del art�culo seleccionado"
            End If
        End If
    Else
        strMensaje = strMensaje + "El precio de este art�culo nunca ha sido actualizado"
    End If
    
    If (strMensaje <> Empty) Then
    
        Load frmAlertas
        frmAlertas.Show
        frmAlertas.MostrarMensaje strMensaje
        
        Me.SetFocus
        
    End If

End Function
