VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Lst_Gral_Art 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listado General de Artνculos.-"
   ClientHeight    =   6615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11415
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6615
   ScaleWidth      =   11415
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Consulta 
      Height          =   1095
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   11415
      Begin VB.ComboBox Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7200
         TabIndex        =   3
         Top             =   240
         Width           =   4095
      End
      Begin VB.OptionButton Orden_Rubro 
         Caption         =   "Ordenado por Rubros.-"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   2655
      End
      Begin VB.OptionButton Orden_Alfa 
         Caption         =   "Por σrden alfabιtico.-"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   2295
      End
      Begin VB.OptionButton Orden_Numer 
         Caption         =   "Por σrden numιrico.-"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Value           =   -1  'True
         Width           =   2295
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4080
         TabIndex        =   10
         Top             =   240
         Width           =   3015
      End
   End
   Begin VB.Frame Marco_Lista 
      Height          =   4575
      Left            =   0
      TabIndex        =   8
      Top             =   1080
      Width           =   11415
      Begin VB.Frame Marco_Progreso 
         Height          =   1575
         Left            =   2760
         TabIndex        =   22
         Top             =   1680
         Visible         =   0   'False
         Width           =   5895
         Begin MSComctlLib.ProgressBar Prog 
            Height          =   255
            Left            =   120
            TabIndex        =   23
            Top             =   1200
            Width           =   5655
            _ExtentX        =   9975
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
         End
         Begin VB.Label Label3 
            Caption         =   "Aguarde un momento..."
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   120
            Width           =   5655
         End
         Begin VB.Label Label2 
            Caption         =   "0%                                                   Progreso                                               100%"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   24
            Top             =   960
            Width           =   5655
         End
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4110
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   11175
      End
      Begin VB.Frame Barra_titulo 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   120
         Width           =   11175
         Begin VB.CommandButton Command8 
            Caption         =   "Final Pub."
            Height          =   255
            Left            =   9840
            TabIndex        =   20
            Top             =   0
            Width           =   1335
         End
         Begin VB.CommandButton Command6 
            Caption         =   "Precio Pub."
            Height          =   255
            Left            =   8760
            TabIndex        =   18
            Top             =   0
            Width           =   1095
         End
         Begin VB.CommandButton Command5 
            Caption         =   "% pub."
            Height          =   255
            Left            =   8160
            TabIndex        =   17
            Top             =   0
            Width           =   615
         End
         Begin VB.CommandButton Command7 
            Caption         =   "Final Rev."
            Height          =   255
            Left            =   7080
            TabIndex        =   19
            Top             =   0
            Width           =   1095
         End
         Begin VB.CommandButton Command4 
            Caption         =   "Precio Rev."
            Height          =   255
            Left            =   6000
            TabIndex        =   16
            Top             =   0
            Width           =   1095
         End
         Begin VB.CommandButton Command3 
            Caption         =   "% rev."
            Height          =   255
            Left            =   5400
            TabIndex        =   15
            Top             =   0
            Width           =   615
         End
         Begin VB.CommandButton Command9 
            Caption         =   "P. Compra"
            Height          =   255
            Left            =   4320
            TabIndex        =   21
            Top             =   0
            Width           =   1095
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Descripciσn"
            Height          =   255
            Left            =   1080
            TabIndex        =   14
            Top             =   0
            Width           =   3255
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Cσdigo"
            Height          =   255
            Left            =   0
            TabIndex        =   13
            Top             =   0
            Width           =   1095
         End
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   9
      Top             =   5640
      Width           =   11415
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8760
         Picture         =   "Lst_Gral_Art.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   26
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10200
         Picture         =   "Lst_Gral_Art.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprime 
         Height          =   615
         Left            =   7680
         Picture         =   "Lst_Gral_Art.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Lst_Gral_Art.frx":C80E
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Lst_Gral_Art"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Combo1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    Dim Sum As Single
    Dim rRubro As Long
    Dim Precio As Single
    
    List1.Clear
    Prog.Value = 0
    MousePointer = 11
    
    qy = "SELECT Rubro.*,Articulo.* FROM Rubro,Articulo WHERE Rubro.Id_Rubro = Articulo.Id_Rubro "
    If Val(Mid(Rubro.Text, 31)) > 0 Then qy = qy & "AND Rubro.Id_Rubro = " & Trim(Str(Val(Mid(Rubro.Text, 31)))) & " "
    If Orden_Numer.Value = True Then
        qy = qy & "ORDER BY Rubro.Id_Rubro, Id_Articulo, Articulo.Descripcion "
    ElseIf Orden_Alfa.Value = True Then
        qy = qy & "ORDER BY Rubro.Denominacion, Rubro.Id_Rubro, Articulo.Descripcion "
    ElseIf Orden_Rubro.Value Then
        qy = qy & "ORDER BY Rubro.Id_Rubro, Rubro.Denominacion, Articulo.Descripcion "
    End If
    AbreRs
    
    Marco_Progreso.Visible = True
    Marco_Progreso.Refresh
    
    If Not Rs.EOF Then
        Sum = Val(100 / Rs.RecordCount)
        rRubro = Val(Rs.Fields("Id_Rubro"))
        List1.AddItem Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 9, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
        List1.AddItem "――――――――――――――――――――――――――――――――――――――――"
    End If
    
    While Not Rs.EOF
        If Val(rRubro) = Val(Rs.Fields("Id_Rubro")) Then
            Txt = Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 4, " ", False)
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Articulo"))), 0, 5, " ", False) & " "
            Txt = Txt & Trim(Rs.Fields("Descripcion")) + Space(30 - Len(Trim(Rs.Fields("Descripcion")))) & " "
            Txt = Txt & Formateado(Str(Rs.Fields("Precio_Compra")), 3, 9, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Margen_Rev"))), 2, 5, " ", False) & " "
            Precio = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Rev"))) / 100) + Val(Rs.Fields("Precio_Compra"))
            Txt = Txt & Formateado(Str(Val(Precio)), 3, 9, " ", False) & " "
            Precio = ((Val(Precio) * Val(Alicuota_Iva)) / 100) + Val(Precio)
            Txt = Txt & Formateado(Str(Val(Precio)), 3, 9, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Margen_Pub"))), 2, 5, " ", False) & " "
            Precio = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Pub"))) / 100) + Val(Rs.Fields("Precio_Compra"))
            Txt = Txt & Formateado(Str(Val(Precio)), 3, 9, " ", False) & " "
            Precio = ((Val(Precio) * Val(Alicuota_Iva)) / 100) + Val(Precio)
            Txt = Txt & Formateado(Str(Val(Precio)), 3, 9, " ", False) & " "
            
            If (Val(Sum) + Val(Prog.Value)) < 100 Then Prog.Value = Val(Prog.Value) + Val(Sum)
            
            List1.AddItem Txt
            Rs.MoveNext
        Else
            List1.AddItem ""
            rRubro = Val(Rs.Fields("Id_Rubro"))
            Txt = Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 9, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
            List1.AddItem Txt
            List1.AddItem "――――――――――――――――――――――――――――――――――――――――"
        End If
    Wend
    Prog.Value = 100
    Marco_Progreso.Visible = False
    MousePointer = 0
    
    List1.SetFocus
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4.5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Rubro
End Sub

Private Sub Cargar_Rubro()
    qy = "SELECT * FROM Rubro ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Rubro.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Rubro"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Imprime_Click()
    Dim i As Long
    Dim l As Long
    
    If List1.ListCount > 0 Then
        
        i = 0
        l = 0
        Printer.Font = "Courier New"
        Printer.FontSize = 9
        
        Imprimir_Encabezado
        
        For i = 0 To List1.ListCount - 1
            
            If l <= 64 Then
                Printer.Print " " & Mid(List1.List(i), 1, 170)
                l = l + 1
            Else
                Printer.Print " "
                Printer.Print " Los Precios de lista incluyen la alνcuota de IVA (" & Formateado(Str(Val(Alicuota_Iva)), 2, 0, "", False) & "%) "
                
                l = 0
                Printer.NewPage
                Imprimir_Encabezado
            End If
        Next
        
        Printer.Print " "
        Printer.Print " Los Precios de lista incluyen la alνcuota de IVA (" & Formateado(Str(Val(Alicuota_Iva)), 2, 0, "", False) & "%) "
        Printer.EndDoc
    End If
    
    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Dim Titulo As String
    
    Titulo = "                        LISTA GENERAL DE ARTΜCULOS "
    If Orden_Numer.Value = True Then
        Titulo = Titulo & "POR ORDEN NUMΙRICO."
    ElseIf Orden_Alfa.Value = True Then
        Titulo = Titulo & "POR ORDEN ALFABΙTICO."
    ElseIf Orden_Rubro.Value Then
        Titulo = Titulo & "ORDENADO POR RUBROS."
    End If
    
    Imprime.Tag = Int(List1.ListCount / 64) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                     Pαgina.: " & Printer.Page & "/" & Imprime.Tag
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                   Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                             Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " e-mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print Titulo
    Printer.Print
    Printer.Print " "
    Printer.Font = "MS Sans Serif"
    Printer.FontSize = 8.5
    Printer.Print "      Cσd.           Descripciσn                                                         P. Compra   %Rev.      Precio Rev.        Final Rev.   %Pub.      Precio Pub.        Final Pub."
    Printer.Font = "Courier New"
    Printer.FontSize = 9
    Printer.Print " "
End Sub

Private Sub Orden_Alfa_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Orden_Numer_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Orden_Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rubro_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If List1.ListCount = 0 Then
        Unload Me
    Else
        List1.Clear
        Orden_Numer.Value = True
        Rubro.Text = ""
        
        Orden_Numer.SetFocus
    End If
End Sub
