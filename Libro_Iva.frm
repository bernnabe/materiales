VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form Libro_Iva 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Libros de IVA.-"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11295
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   11295
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Periodo 
      Height          =   855
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11295
      Begin VB.OptionButton Ventas 
         Caption         =   "&Ventas"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   10200
         TabIndex        =   3
         Top             =   480
         Width           =   975
      End
      Begin VB.OptionButton Compras 
         Caption         =   "&Compras"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   10200
         TabIndex        =   2
         Top             =   240
         Value           =   -1  'True
         Width           =   975
      End
      Begin VB.TextBox Aρo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4440
         MaxLength       =   4
         TabIndex        =   1
         Top             =   240
         Width           =   615
      End
      Begin VB.ComboBox Mes 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   2535
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Aρo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   13
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Perνodo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Libro 
      Enabled         =   0   'False
      Height          =   5295
      Left            =   0
      TabIndex        =   10
      Top             =   840
      Width           =   11295
      Begin MSFlexGridLib.MSFlexGrid Lista 
         Height          =   4935
         Left            =   120
         TabIndex        =   14
         Top             =   240
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   8705
         _Version        =   393216
         Cols            =   12
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         FillStyle       =   1
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   11
      Top             =   6120
      Width           =   11295
      Begin VB.CommandButton Posicion 
         Height          =   615
         Left            =   8520
         Picture         =   "Libro_Iva.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Mostrar Posiciσn Frente al IVA.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Exporta 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7560
         Picture         =   "Libro_Iva.frx":5602
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10080
         Picture         =   "Libro_Iva.frx":5F74
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Cancelar - Salir"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   6600
         Picture         =   "Libro_Iva.frx":C1FE
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Imprimir Libro de IVA.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Libro_Iva.frx":11E10
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Confirma la Carga del Libro IVA.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Libro_Iva"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Aρo_GotFocus()
    Aρo.Text = Trim(Aρo.Text)
    Aρo.SelStart = 0
    Aρo.SelText = ""
    Aρo.SelLength = Len(Aρo.Text)
End Sub

Private Sub Aρo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Aρo_LostFocus()
    If Val(Aρo.Text) = 0 Then Aρo.Text = Val(Mid(Fecha_Fiscal, 7, 4))
End Sub

Private Sub Compras_Click()
    Armar_Lista
End Sub

Private Sub Compras_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    Dim Gravado   As Single
    Dim Exento    As Single
    Dim Iva       As Single
    Dim iva_Ni    As Single
    Dim Perc      As Single
    Dim Ret_Iva   As Single
    Dim Ret_IIBB  As Single
    Dim Total     As Single
    Dim i         As Long
    
    i = 0
    
    Lista.Clear
    Armar_Lista
    Marco_Libro.Enabled = True
    
    qy = "SELECT " & IIf(Compras.Value = True, "Proveedor.*,Factura_Compra.*", "Cliente.*,Factura_Venta.*") & " FROM "
    qy = qy & IIf(Compras.Value = True, "Proveedor,Factura_Compra", "Cliente,Factura_Venta") & " WHERE Mes_Libro = " & Val(Mid(Mes.Text, 1, 2)) & " "
    If Compras.Value = True Then
        qy = qy & "AND Factura_Compra.Proveedor = Proveedor.Id_Proveedor "
    Else
        qy = qy & "AND Factura_Venta.Cliente = Cliente.Id_Cliente "
    End If
    qy = qy & "AND Aρo_Libro = " & Val(Aρo.Text) & " "
    qy = qy & "AND Empresa = " & Val(Id_Empresa)
    qy = qy & "ORDER BY Fecha, Id_Tipo_Factura, Id_Letra_Factura, Id_Centro_Emisor, Id_Nro_Factura"
    AbreRs
    Lista.Rows = 2
    
    For i = 1 To Rs.RecordCount
        Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & Chr(9)
        Txt = Txt & Trim(Rs.Fields("Id_Tipo_Factura")) & " " & Trim(Rs.Fields("Id_Letra_Factura")) & " " & Formateado(Str(Val(Rs.Fields("Id_Centro_Emisor"))), 0, 4, "0", False) & "-" & Formateado(Str(Val(Rs.Fields("Id_Nro_Factura"))), 0, 10, "0", False) + Space(20 - Len(Trim(Rs.Fields("Id_Tipo_Factura")) & " " & Trim(Rs.Fields("Id_Letra_Factura")) & " " & Formateado(Str(Val(Rs.Fields("Id_Centro_Emisor"))), 0, 4, "0", False) & "-" & Formateado(Str(Val(Rs.Fields("Id_Nro_Factura"))), 0, 10, "0", False))) & Chr(9)
        
        If Val(Rs.Fields("Total_Factura")) = 0 Then
            
            Txt = Txt & "COMPROBANTE ANULADO" + Space(30 - Len("COMPROBANTE ANULADO")) & Chr(9)
            Txt = Txt & Space(2) & Chr(9)
            Txt = Txt & Space(13) & Chr(9)
        
        Else
            
            Txt = Txt & Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & Chr(9)
            Txt = Txt & Trim(Rs.Fields("Condicion_IVA")) + Space(2 - Len(Trim(Rs.Fields("Condicion_Iva")))) & Chr(9)
            Txt = Txt & Trim(Rs.Fields("Nro")) + Space(13 - Len(Trim(Rs.Fields("Nro")))) & Chr(9)
        
        End If
        
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Gravado"))), 2, 10, " ", False) & Chr$(9): Gravado = Val(Gravado) + Val(Rs.Fields("Gravado"))
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Exento"))), 2, 10, " ", False) & Chr$(9): Exento = Val(Exento) + Val(Rs.Fields("Exento"))
        Txt = Txt & Formateado(Str(Val(Rs.Fields("SubTotal"))), 2, 10, " ", False) & Chr(9): Perc = Val(Perc) + Val(Rs.Fields("SubTotal"))
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Iva"))), 2, 10, " ", False) & Chr$(9): Iva = Val(Iva) + Val(Rs.Fields("Iva"))
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Sta"))), 2, 10, " ", False) & Chr$(9): iva_Ni = Val(iva_Ni) + Val(Rs.Fields("Sta"))
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Ret_Iva"))), 2, 10, " ", False) & Chr$(9): Ret_Iva = Val(Ret_Iva) + Val(Rs.Fields("Ret_Iva"))
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Ret_IIBB"))), 2, 10, " ", False) & Chr$(9): Ret_IIBB = Val(Ret_IIBB) + Val(Rs.Fields("Ret_IIBB"))
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Total_Factura"))), 2, 10, " ", False): Total = Val(Total) + Val(Rs.Fields("Total_Factura"))
        
        Lista.AddItem Txt, i
        Rs.MoveNext
    Next
    
    Lista.AddItem Chr(9) & Chr(9) & Chr(9) & Chr(9) & "         TOTALES:" & Chr(9) & Formateado(Str(Val(Gravado)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Exento)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Perc)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Iva)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(iva_Ni)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Ret_Iva)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Ret_IIBB)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Total)), 2, 10, " ", True), i + 1
    
    Lista.SetFocus
    Imprimir.Enabled = True
    Exporta.Enabled = True
End Sub

Private Sub Exporta_Click()
    If MsgBox("Confirma la tranferencia de los datos a una hoja de cαlculo ?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        
        Dim i As Long
        Dim l As Long
        Dim ObjExcel As Excel.Application
        
        MousePointer = 11
        Lista.Col = 0
        Lista.Row = 0
        
        On Error GoTo ErrorExcel
        
        Set ObjExcel = New Excel.Application
        ObjExcel.SheetsInNewWorkbook = 3
        ObjExcel.Workbooks.Add
    
        With ObjExcel.ActiveSheet
            
            .Cells(3, 1) = "Fecha"
            .Cells(3, 2) = "Comprobante"
            .Cells(3, 3) = "Nombre"
            .Cells(3, 4) = "Iva"
            .Cells(3, 5) = "CUIT"
            .Cells(3, 6) = "Gravado"
            .Cells(3, 7) = "Exento"
            .Cells(3, 8) = "Sub Total"
            .Cells(3, 9) = "Iva Inscripto"
            .Cells(3, 10) = "Iva NI"
            .Cells(3, 11) = "Ret. Iva"
            .Cells(3, 12) = "Ret. IIBB"
            .Cells(3, 13) = "Total"
            
            .Range(.Cells(3, 1), .Cells(3, 13)).Font.Bold = True
            
            .Columns("D").HorizontalAlignment = xlHAlignRight
            .Columns("A").ColumnWidth = 10
            .Columns("B").ColumnWidth = 22
            .Columns("C").ColumnWidth = 32
            .Columns("D").ColumnWidth = 4
            .Columns("E").ColumnWidth = 15
            .Columns("F").ColumnWidth = 12
            .Columns("G").ColumnWidth = 12
            .Columns("H").ColumnWidth = 12
            .Columns("I").ColumnWidth = 12
            .Columns("J").ColumnWidth = 12
            .Columns("K").ColumnWidth = 12
            .Columns("L").ColumnWidth = 12
            .Columns("M").ColumnWidth = 12
        
        End With
        
        ObjExcel.ActiveSheet.Cells(1, 1) = "LIBRO DE IVA " & IIf(Compras.Value = True, "COMPRAS", "VENTAS")
        ObjExcel.ActiveSheet.Range(ObjExcel.ActiveSheet.Cells(1, 1), ObjExcel.ActiveSheet.Cells(1, 10)).HorizontalAlignment = xlHAlignCenterAcrossSelection
        ObjExcel.ActiveSheet.Cells(2, 1) = "PERIODO " & Trim(Mid(Mes.Text, 5)) & " " & Trim(Str(Val(Aρo.Text)))
        ObjExcel.ActiveSheet.Range(ObjExcel.ActiveSheet.Cells(2, 1), ObjExcel.ActiveSheet.Cells(2, 10)).HorizontalAlignment = xlHAlignCenterAcrossSelection
            
        Dim P, T  As Variant
             
        For P = 1 To 10
            ObjExcel.ActiveSheet.Range(ObjExcel.ActiveSheet.Cells(3, 9), ObjExcel.ActiveSheet.Cells(3, P)).HorizontalAlignment = xlHAlignCenterAcrossSelection
        Next P
        
        For T = 1 To 2
            With ObjExcel.ActiveSheet.Cells(T, 1).Font
                If T = 1 Then
                    .Color = vbBlue
                Else
                    .Color = vbRed
                End If
                
                .Size = 12
                .Bold = True
            End With
        Next T
        
        If Lista.Rows = 0 Then
            
            ObjExcel.ActiveSheet.Cells(5, 3) = "NOTA: No Existen Registros"
        
        Else
            
            i = 0
            For i = 1 To Lista.Rows - 1
                Lista.Row = i
                
                For l = 0 To 12
                    Lista.Col = l
                    ObjExcel.ActiveSheet.Cells(i + 4, l + 1) = Trim(Lista.Text)
                Next
            Next
        
        End If
       
        ObjExcel.Visible = True
        
        Set ObjExcel = Nothing
        
        MousePointer = 0
    Else
        Salir.SetFocus
    End If
    
    Exit Sub
    
ErrorExcel:
    MsgBox ("Ha ocurrido un error de conexiσn con Excel."), vbCritical, "Error al conectar con Excel"
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Libros de IVA.-"
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 12
    Me.Left = (Screen.Width - Me.Width) / 2
    
    For i = 1 To 12
        Mes.AddItem Mes_Letra(i)
    Next
    
    Armar_Lista
End Sub

Private Sub Armar_Lista()
    Lista.Clear
    
    Lista.Cols = 13
    Lista.ColWidth(0) = 900
    Lista.ColWidth(1) = 1900
    Lista.ColWidth(2) = 3700
    Lista.ColWidth(3) = 450
    Lista.ColAlignment(4) = 6
    Lista.ColWidth(4) = 1200
    Lista.ColAlignment(5) = 6
    Lista.ColWidth(5) = 900
    Lista.ColAlignment(6) = 6
    Lista.ColWidth(6) = 900
    Lista.ColAlignment(7) = 6
    Lista.ColWidth(7) = 900
    Lista.ColAlignment(8) = 6
    Lista.ColWidth(8) = 900
    Lista.ColAlignment(9) = 6
    Lista.ColWidth(9) = 900
    Lista.ColAlignment(10) = 6
    Lista.ColWidth(10) = 900
    Lista.ColAlignment(11) = 6
    Lista.ColWidth(11) = 900
    Lista.ColAlignment(12) = 6
    Lista.ColWidth(12) = 900
    
    Lista.Rows = 1
    Lista.Col = 0
    Lista.Row = 0
    
    Lista.Text = "Fecha"
    Lista.Col = 1
    Lista.Text = "Comprobante"
    Lista.Col = 2
    Lista.Text = IIf(Ventas.Value = True, "Cliente", "Proveedor")
    Lista.Col = 3
    Lista.Text = "Iva"
    Lista.Col = 4
    Lista.Text = "Cuit"
    Lista.Col = 5
    Lista.Text = "Gravado"
    Lista.Col = 6
    Lista.Text = "Exento"
    Lista.Col = 7
    Lista.Text = "Sub Total"
    Lista.Col = 8
    Lista.Text = "IVA Inscrip."
    Lista.Col = 9
    Lista.Text = "IVA no Insc."
    Lista.Col = 10
    Lista.Text = "Ret. Iva"
    Lista.Col = 11
    Lista.Text = "Ret. IIBB"
    Lista.Col = 12
    Lista.Text = "TOTAL"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
    Dim C As Long

    If Lista.Rows > 0 Then

        i = 0
        l = 0
        C = 0
        Lista.Col = 0
        Lista.Row = 0

        Printer.Font = "Courier New"
        Printer.FontSize = 5.5

        Imprimir_Encabezado
        Txt = ""

        While i <= Lista.Rows - 1
        
            Lista.Row = i
            Txt = ""
            
            If l <= 110 Then
                If l > 1 Then
                    For C = 0 To Lista.Cols - 1
                        Lista.Col = C
                        
                        Txt = Txt & Lista.Text
                        Txt = Txt & " "
                        
                    Next
                    
                    Printer.Print " " & Txt
                    i = i + 1
                End If
                    
                l = l + 1
            Else
                Printer.Print " "
                Printer.NewPage
                Imprimir_Encabezado
                l = 0
                
                i = i - 1
                Lista.Row = i
            End If
            
        Wend
        
        Txt = ""
        Lista.Row = Lista.Rows - 1
        For i = 5 To Lista.Cols - 1
            Lista.Col = i
            
            Txt = Txt & Lista.Text
            Txt = Txt & " "
        Next
        
        Printer.Print " "
        Printer.Print Space(70) & "TOTALES: " & Txt
        Printer.Print " "
        Printer.Print " "
        
        Printer.EndDoc
    End If

    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Imprimir.Tag = Int(Lista.Rows / 110) + 1
    Printer.Print " " & Trim(UCase(cEmpresa)) + Space(30 - Len(Trim(cEmpresa))) & "                                                                                                                       Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print "                                                                                                                                                      Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print "                                                                                                                                                      Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.FontBold = True
    Printer.Print "                                                          LIBRO IVA " & IIf(Compras.Value = True, "COMPRAS", "VENTAS") & " - PERIODO "; Mid(Mes.Text, 1, 2); " ("; Trim(Mid(Mes.Text, 6)); ") AΡO "; Trim(Aρo.Text)
    Printer.FontBold = False
    Printer.Print
    Printer.Print " "
    Printer.Print "    Fecha Comprobante          Nombre                         Iva C.U.I.T.        Gravado     Exento  Sub Total        Iva     Iva NI    Ret.Iva   Ret.IIBB      Total"
    Printer.Print " "
End Sub

Private Sub MostrarDetalleFactura()

    Detalle_Factura.Show
    
    If Ventas.Value Then
        
        Detalle_Factura.Codigo.Tag = "Cliente"
        
    Else
        
        Detalle_Factura.Codigo.Tag = "Proveedor"
        
    End If
    
    Detalle_Factura.Comprobante.Text = Lista.TextMatrix(Lista.RowSel, 1)
    Detalle_Factura.Nombre.Text = Lista.TextMatrix(Lista.RowSel, 2)
    Detalle_Factura.Importe.Text = Lista.TextMatrix(Lista.RowSel, 12)
    
    Detalle_Factura.Confirma_Click
    
End Sub

Private Sub Lista_DblClick()
    
    If Lista.RowSel > 0 Then
        
        MostrarDetalleFactura
    
    End If

End Sub

Private Sub Mes_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Mes_LostFocus()
    If Val(Mes.Text) = 0 Then
        Mes.ListIndex = Val(Mid(Fecha_Fiscal, 4, 2)) - 1
    Else
        Mes.ListIndex = Val(Mes.Text) - 1
    End If
End Sub

Private Sub Posicion_Click()
    Load Libro_Iva_Posicion
    Libro_Iva_Posicion.Show
    
    Libro_Iva_Posicion.Salir.SetFocus
End Sub

Private Sub Salir_Click()
    If Aρo.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Lista.Clear
    Armar_Lista
    Exporta.Enabled = False
    'List2.Clear
    Mes.Text = ""
    Aρo.Text = ""
    Compras.Value = True
    Marco_Libro.Enabled = False
    Imprimir.Enabled = False
    
    Mes.SetFocus
End Sub

Private Sub Ventas_Click()
    If Compras.Value = True Then
        Armar_Lista
    Else
        Armar_Lista
    End If
End Sub

Private Sub Ventas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub
