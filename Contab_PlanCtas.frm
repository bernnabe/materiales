VERSION 5.00
Object = "{FE0065C0-1B7B-11CF-9D53-00AA003C9CB6}#1.1#0"; "COMCT232.OCX"
Begin VB.Form Contab_PlanCtas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaci�n del Plan de Cuentas.-"
   ClientHeight    =   6015
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   8895
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6015
   ScaleWidth      =   8895
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Plan 
      Height          =   5055
      Left            =   0
      TabIndex        =   13
      Top             =   0
      Width           =   8895
      Begin ComCtl2.UpDown UpDown 
         Height          =   615
         Left            =   5880
         TabIndex        =   30
         Top             =   240
         Width           =   240
         _ExtentX        =   450
         _ExtentY        =   1085
         _Version        =   327681
         Enabled         =   -1  'True
      End
      Begin VB.Frame Marco_Periodo_Saldo 
         Enabled         =   0   'False
         Height          =   3615
         Left            =   120
         TabIndex        =   23
         Top             =   1320
         Width           =   8655
         Begin VB.ListBox List1 
            BeginProperty Font 
               Name            =   "Courier"
               Size            =   9.75
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   2985
            Left            =   120
            TabIndex        =   24
            Top             =   480
            Width           =   8415
         End
         Begin VB.CommandButton Command9 
            Caption         =   "Saldo Ejercicio"
            Height          =   255
            Left            =   6840
            TabIndex        =   25
            Top             =   240
            Width           =   1695
         End
         Begin VB.CommandButton Command8 
            Caption         =   "Saldo Per�odo"
            Height          =   255
            Left            =   5160
            TabIndex        =   26
            Top             =   240
            Width           =   1695
         End
         Begin VB.CommandButton Command7 
            Caption         =   "Cr�ditos"
            Height          =   255
            Left            =   3480
            TabIndex        =   27
            Top             =   240
            Width           =   1695
         End
         Begin VB.CommandButton Command6 
            Caption         =   "D�bitos"
            Height          =   255
            Left            =   1800
            TabIndex        =   28
            Top             =   240
            Width           =   1695
         End
         Begin VB.CommandButton Command10 
            Caption         =   "Per�odo"
            Height          =   255
            Left            =   120
            TabIndex        =   29
            Top             =   240
            Width           =   1695
         End
      End
      Begin VB.CheckBox Recibe 
         Caption         =   "Recibe Asientos"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1560
         TabIndex        =   7
         Top             =   960
         Width           =   4215
      End
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   22
         Top             =   600
         Visible         =   0   'False
         Width           =   4215
      End
      Begin VB.TextBox Saldo_Inicial 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7440
         MaxLength       =   10
         TabIndex        =   9
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox Ejercicio 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7440
         MaxLength       =   4
         TabIndex        =   8
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox Fecha_Alta 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7440
         TabIndex        =   12
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Nombre 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   30
         TabIndex        =   6
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox Id_Nivel_1 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   5
         TabIndex        =   0
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_2 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   5
         TabIndex        =   1
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_3 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3000
         MaxLength       =   5
         TabIndex        =   2
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_4 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         MaxLength       =   5
         TabIndex        =   3
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_5 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4440
         MaxLength       =   5
         TabIndex        =   4
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_6 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5160
         MaxLength       =   5
         TabIndex        =   5
         Top             =   240
         Width           =   615
      End
      Begin VB.CommandButton Buscar_Cuentas 
         Caption         =   "&Nombre:"
         Height          =   255
         Left            =   600
         TabIndex        =   11
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label17 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo Inicial:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5880
         TabIndex        =   18
         Top             =   960
         Width           =   1455
      End
      Begin VB.Label Label16 
         Alignment       =   1  'Right Justify
         Caption         =   "Ejercicio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5880
         TabIndex        =   17
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Alta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5880
         TabIndex        =   16
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   240
         Width           =   1215
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   14
      Top             =   5040
      Width           =   8895
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   7680
         Picture         =   "Contab_PlanCtas.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   6360
         Picture         =   "Contab_PlanCtas.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Listar Plan de Cuentas.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   5280
         Picture         =   "Contab_PlanCtas.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "Borrar.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Contab_PlanCtas.frx":12126
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Grabar.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Contab_PlanCtas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Rs_Cta As Recordset

Private Sub Borrar_Click()
    If MsgBox("Est� seguro de borrar esta cuenta. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
        qy = "DELETE FROM Cuenta WHERE "
        qy = qy & "Cuenta.Id_Nivel_1 = " & Trim(Str(Val(Id_Nivel_1.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_2 = " & Trim(Str(Val(Id_Nivel_2.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_3 = " & Trim(Str(Val(Id_Nivel_3.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_4 = " & Trim(Str(Val(Id_Nivel_4.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_5 = " & Trim(Str(Val(Id_Nivel_5.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_6 = " & Trim(Str(Val(Id_Nivel_6.Text))) & " "
        qy = qy & "AND Cuenta.Empresa = " & Trim(Str(Val(Id_Empresa)))
        Db.Execute (qy)
        
        qy = "DELETE FROM Saldo_Inicial WHERE "
        qy = qy & "Saldo_Inicial.Id_Nivel_1 = " & Trim(Str(Val(Id_Nivel_1.Text))) & " "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_2 = " & Trim(Str(Val(Id_Nivel_2.Text))) & " "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_3 = " & Trim(Str(Val(Id_Nivel_3.Text))) & " "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_4 = " & Trim(Str(Val(Id_Nivel_4.Text))) & " "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_5 = " & Trim(Str(Val(Id_Nivel_5.Text))) & " "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_6 = " & Trim(Str(Val(Id_Nivel_6.Text))) & " "
        qy = qy & "AND Saldo_Inicial.Empresa = " & Trim(Str(Val(Id_Empresa)))
        Db.Execute (qy)
        
        Salir_Click
    End If
End Sub

Private Sub Buscar_Cuentas_Click()
    Cuentas_Encontradas.Visible = True
    MousePointer = 11
    If Cuentas_Encontradas.ListCount = 0 Then
        qy = "SELECT * FROM Cuenta "
        qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Cuentas_Encontradas.AddItem Trim(Rs.Fields("Denominacion")) + Space(35 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Nivel_1"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_2"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_3"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_4"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_5"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_6"))), 0, 4, " ", False)
            
            Rs.MoveNext
        Wend
    End If
    
    MousePointer = 0
    Cuentas_Encontradas.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Id_Nivel_1.Text = Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 37, 4))
    Id_Nivel_2.Text = Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 42, 4))
    Id_Nivel_3.Text = Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 47, 4))
    Id_Nivel_4.Text = Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 52, 4))
    Id_Nivel_5.Text = Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 57, 4))
    Id_Nivel_6.Text = Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 62, 4))
    
    Cuentas_Encontradas.Visible = False
    Leer_Cuenta
End Sub

Private Sub Leer_Cuenta()
    If Val(Ejercicio.Text) = 0 Then Ejercicio.Text = Mid(Fecha_Fiscal, 7, 4)
    
    If Val(Id_Nivel_1.Text) > 0 Then
        qy = "SELECT Cuenta.*,Saldo_Inicial.* FROM Cuenta,Saldo_Inicial WHERE "
        qy = qy & "Cuenta.Id_Nivel_1 = " & Trim(Str(Val(Id_Nivel_1.Text))) & " "
        qy = qy & "AND Saldo_Inicial.Ejercicio = " & Trim(Str(Val(Ejercicio.Text) - 1))
        qy = qy & "AND Saldo_Inicial.Id_Nivel_1 = Cuenta.Id_Nivel_1 "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_2 = Cuenta.Id_Nivel_2 "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_3 = Cuenta.Id_Nivel_3 "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_4 = Cuenta.Id_Nivel_4 "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_5 = Cuenta.Id_Nivel_5 "
        qy = qy & "AND Saldo_Inicial.Id_Nivel_6 = Cuenta.Id_Nivel_6 "
        qy = qy & "AND Saldo_Inicial.Id_Empresa = Cuenta.Id_Empresa "
        qy = qy & "AND Cuenta.Id_Nivel_2 = " & Trim(Str(Val(Id_Nivel_2.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_3 = " & Trim(Str(Val(Id_Nivel_3.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_4 = " & Trim(Str(Val(Id_Nivel_4.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_5 = " & Trim(Str(Val(Id_Nivel_5.Text))) & " "
        qy = qy & "AND Cuenta.Id_Nivel_6 = " & Trim(Str(Val(Id_Nivel_6.Text)))
        qy = qy & "AND Cuenta.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        AbreRs

        If Rs.EOF Then
            
            'MsgBox "La Cuenta es inexistente...", vbInformation, "Atenci�n.!"
            'Borrar_Campo
            Nombre.Text = "REGISTRO NO ENCONTRADO"
            SendKeys "{TAB}"
        Else
            Nombre.Text = Rs.Fields("Denominacion")
            Fecha_Alta.Text = Rs.Fields("Fecha_Alta")
            Recibe.Value = IIf(Rs.Fields("Recibe_Asiento") = "Falso", 0, 1)
            Saldo_Inicial.Text = Formateado(Str(Val(Rs.Fields("Saldo_Inicial"))), 2, 10, " ", False)
            Borrar.Enabled = True
            Cargar_Movimientos
        End If
    Else
        Id_Nivel_1.Text = ""
        Id_Nivel_1.SetFocus
    End If
End Sub

Private Sub Cargar_Movimientos()
    List1.Clear
    Dim i                     As Long
    Dim Saldo_Periodo(1 To 12) As String
    Dim Saldo_Ejercicio       As String
    
    MousePointer = 11
    Saldo_Ejercicio = 0
    'Saldo_Periodo = 0
    
    For i = 1 To IIf(Val(Ejercicio.Text) = Val(Mid(Fecha_Fiscal, 7, 4)), Val(Mid(Fecha_Fiscal, 4, 2)), 12)
        'If i = 1 Then
        '    Saldo_Periodo(i) = Val(Saldo_Inicial.Text)
        'End If
            
        qy = "SELECT SUM(Debe), SUM (Haber), SUM (Debe - Haber) AS Saldo_Periodo FROM Asiento WHERE Ejercicio = " & Trim(Str(Val(Ejercicio.Text)))
        qy = qy & "AND MID(Fecha, 4, 2) = " & Val(i) & " "
        qy = qy & "AND Nivel_1 = " & Trim(Str(Val(Id_Nivel_1.Text))) & " "
        If Val(Id_Nivel_2.Text) > 0 Then qy = qy & "AND Nivel_2 = " & Trim(Str(Val(Id_Nivel_2.Text))) & " "
        If Val(Id_Nivel_3.Text) > 0 Then qy = qy & "AND Nivel_3 = " & Trim(Str(Val(Id_Nivel_3.Text))) & " "
        If Val(Id_Nivel_4.Text) > 0 Then qy = qy & "AND Nivel_4 = " & Trim(Str(Val(Id_Nivel_4.Text))) & " "
        If Val(Id_Nivel_5.Text) > 0 Then qy = qy & "AND Nivel_5 = " & Trim(Str(Val(Id_Nivel_5.Text))) & " "
        If Val(Id_Nivel_6.Text) > 0 Then qy = qy & "AND Nivel_6 = " & Trim(Str(Val(Id_Nivel_6.Text))) & " "
        qy = qy & "AND Asiento.Empresa = " & Trim(Str(Val(Id_Empresa)))
        AbreRs
        
        Txt = Trim(Mid(Mes_Letra(i), 6, 9)) + Space(14 - Len(Trim(Mid(Mes_Letra(i), 6, 9))))
        
        If Not Rs.EOF Then
            If IsNull(Rs.Fields("Saldo_Periodo")) = False Then
                Saldo_Periodo(i) = Val(Saldo_Periodo(i)) + Val(Rs.Fields("Saldo_Periodo"))
            End If
            
            If IsNull(Rs.Fields(0)) = False Then
                Txt = Txt & Formateado(Str(Val(Rs.Fields(0))), 2, 13, " ", True) & " "
            Else
                Txt = Txt & Formateado(Str(Val(0)), 2, 13, " ", True) & " "
            End If
            
            If IsNull(Rs.Fields(1)) = False Then
                Txt = Txt & Formateado(Str(Val(Rs.Fields(1))), 2, 13, " ", True) & " "
            Else
                Txt = Txt & Formateado(Str(Val(0)), 2, 13, " ", True) & " "
            End If
            
            If IsNull(Rs.Fields("Saldo_Periodo")) = False Then
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Saldo_Periodo"))), 2, 13, " ", True) & " "
            Else
                Txt = Txt & Formateado(Str(Val(0)), 2, 13, " ", True) & " "
            End If
            
            If IsNull(Rs.Fields("Saldo_Periodo")) = False Then
                Saldo_Ejercicio = Val(Saldo_Periodo(i)) + Val(Saldo_Ejercicio)
            Else
                Saldo_Ejercicio = Val(Saldo_Ejercicio)
            End If
        Else
            Txt = Txt & Formateado(Str(Val(0)), 2, 13, " ", True) & " "
            Txt = Txt & Formateado(Str(Val(0)), 2, 13, " ", True) & " "
            Txt = Txt & Formateado(Str(Val(0)), 2, 13, " ", True) & " "
        End If
        Txt = Txt & Formateado(Str(Val(Saldo_Ejercicio)), 2, 13, " ", True)
        
        List1.AddItem Txt
    Next
    
    MousePointer = 0
    Id_Nivel_1_GotFocus
End Sub

Private Sub Borrar_Campo()
    Id_Nivel_1.Text = ""
    Id_Nivel_2.Text = ""
    Id_Nivel_3.Text = ""
    Id_Nivel_4.Text = ""
    Id_Nivel_5.Text = ""
    Id_Nivel_6.Text = ""
    Nombre.Text = ""
    Saldo_Inicial.Text = ""
    Fecha_Alta.Text = ""
    Ejercicio.Text = ""
    Recibe.Value = 0
    Borrar.Enabled = False
    List1.Clear
    
    Id_Nivel_1.SetFocus
End Sub

Private Sub Ejercicio_GotFocus()
    Ejercicio.Text = Trim(Ejercicio.Text)
    Ejercicio.SelStart = 0
    Ejercicio.SelText = ""
    Ejercicio.SelLength = Len(Ejercicio.Text)
End Sub

Private Sub Ejercicio_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ejercicio_LostFocus()
    Ejercicio.Text = Val(Ejercicio.Text)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualiazaci�n del Plan de Cuentas.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Plan_Cuenta
End Sub

Private Sub Cargar_Plan_Cuenta()
    If Val(Ejercicio.Text) = 0 Then Ejercicio.Text = Mid(Fecha_Fiscal, 7, 4)
    'Qy = "SELECT Cuenta.*,Saldo_Inicial.* FROM Cuenta,Saldo_Inicial WHERE Saldo_Inicial.Id_Nivel_1 = Cuenta.Id_Nivel_1 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_2 = Cuenta.Id_Nivel_2 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_3 = Cuenta.Id_Nivel_3 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_4 = Cuenta.Id_Nivel_4 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_5 = Cuenta.Id_Nivel_5 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_6 = Cuenta.Id_Nivel_6 "
    'Qy = Qy & "AND Saldo_Inicial.Empresa = Cuenta.Empresa "
    'Qy = Qy & "AND Cuenta.Empresa = " & trim(str(Val(Id_Empresa))) & " "
    'Qy = Qy & "ORDER BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6"
    
    qy = "SELECT * FROM Cuenta, Saldo_Inicial WHERE Cuenta.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    'Qy = Qy & "AND Saldo_Inicial.Ejercicio = " & trim(str(Val(Ejercicio.Text) - 1))
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_1 = Cuenta.Id_Nivel_1 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_2 = Cuenta.Id_Nivel_2 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_3 = Cuenta.Id_Nivel_3 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_4 = Cuenta.Id_Nivel_4 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_5 = Cuenta.Id_Nivel_5 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Nivel_6 = Cuenta.Id_Nivel_6 "
    'Qy = Qy & "AND Saldo_Inicial.Id_Empresa = Cuenta.Id_Empresa "
    'Qy = Qy & "ORDER BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6"
    'Set Rs_Cta = Db.OpenRecordset(Qy)
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuentas_Encontradas_LostFocus
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Trim(Nombre.Text) <> "" Then
        qy = "INSERT INTO Cuenta VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_1.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_2.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_3.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_4.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_5.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_6.Text)))
        qy = qy & ", '" & Trim(Nombre.Text) & "'"
        qy = qy & ", '" & IIf(Val(Fecha_Alta.Text) > 0, Trim(Fecha_Alta.Text), Fecha_Fiscal) & "'"
        qy = qy & ", " & IIf(Recibe.Value = 0, 0, 1) & ")"
        Db.Execute (qy)
        
        qy = "INSERT INTO Saldo_Inicial VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_1.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_2.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_3.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_4.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_5.Text)))
        qy = qy & ", " & Trim(Str(Val(Id_Nivel_6.Text)))
        qy = qy & ", " & Trim(Str(Val(Ejercicio.Text)))
        qy = qy & ", " & Trim(Str(Val(Saldo_Inicial.Text))) & ")"
        Db.Execute (qy)
        
    End If
    
    Salir_Click
End Sub

Private Sub Id_Nivel_1_GotFocus()
    Id_Nivel_1.Text = Trim(Id_Nivel_1.Text)
    Id_Nivel_1.SelStart = 0
    Id_Nivel_1.SelText = ""
    Id_Nivel_1.SelLength = Len(Id_Nivel_1.Text)
End Sub

Private Sub Id_Nivel_1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Leer_Cuenta
End Sub

Private Sub Id_Nivel_1_LostFocus()
    'Leer_Cuenta
End Sub

Private Sub Id_Nivel_2_GotFocus()
    Id_Nivel_2.Text = Trim(Id_Nivel_2.Text)
    Id_Nivel_2.SelStart = 0
    Id_Nivel_2.SelText = ""
    Id_Nivel_2.SelLength = Len(Id_Nivel_2.Text)
End Sub

Private Sub Id_Nivel_2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Id_Nivel_2_LostFocus()
    Id_Nivel_2.Text = Val(Id_Nivel_2.Text)
    
    Leer_Cuenta
End Sub

Private Sub Id_Nivel_3_GotFocus()
    Id_Nivel_3.Text = Trim(Id_Nivel_3.Text)
    Id_Nivel_3.SelStart = 0
    Id_Nivel_3.SelText = ""
    Id_Nivel_3.SelLength = Len(Id_Nivel_3.Text)
End Sub

Private Sub Id_Nivel_3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Id_Nivel_3_LostFocus()
    Id_Nivel_3.Text = Val(Id_Nivel_3.Text)
    
    Leer_Cuenta
End Sub

Private Sub Id_Nivel_4_GotFocus()
    Id_Nivel_4.Text = Trim(Id_Nivel_4.Text)
    Id_Nivel_4.SelStart = 0
    Id_Nivel_4.SelText = ""
    Id_Nivel_4.SelLength = Len(Id_Nivel_4.Text)
End Sub

Private Sub Id_Nivel_4_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Id_Nivel_4_LostFocus()
    Id_Nivel_4.Text = Val(Id_Nivel_4.Text)
    
    Leer_Cuenta
End Sub

Private Sub Id_Nivel_5_GotFocus()
    Id_Nivel_5.Text = Trim(Id_Nivel_5.Text)
    Id_Nivel_5.SelStart = 0
    Id_Nivel_5.SelText = ""
    Id_Nivel_5.SelLength = Len(Id_Nivel_5.Text)
End Sub

Private Sub Id_Nivel_5_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Id_Nivel_5_LostFocus()
    Id_Nivel_5.Text = Val(Id_Nivel_5.Text)
    
    Leer_Cuenta
End Sub

Private Sub Id_Nivel_6_GotFocus()
    Id_Nivel_6.Text = Trim(Id_Nivel_6.Text)
    Id_Nivel_6.SelStart = 0
    Id_Nivel_6.SelText = ""
    Id_Nivel_6.SelLength = Len(Id_Nivel_6.Text)
End Sub

Private Sub Id_Nivel_6_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Id_Nivel_6_LostFocus()
    Id_Nivel_6.Text = Val(Id_Nivel_6.Text)
    
    Leer_Cuenta
    
    If Trim(UCase(Nombre.Text)) = "REGISTRO NO ENCONTRADO" Then
        If MsgBox("Desea ingresar una nueva cuenta al plan de cuenta con el c�digo ingresado. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            Nombre_GotFocus
            Nombre.Enabled = True
            Recibe.Enabled = True
            Ejercicio.Enabled = True
            Saldo_Inicial.Enabled = True
            Saldo_Inicial.Text = ""
            Grabar.Enabled = True
            
            Fecha_Alta.Text = Fecha_Fiscal
            Ejercicio.Text = Val(Mid(Fecha_Fiscal, 7, 4)) - 1
            
            Nombre.SetFocus
        Else
            Id_Nivel_1.SetFocus
        End If
    End If
End Sub

Private Sub Imprimir_Click()
    MousePointer = 11
    Contab_LstPlanCtas.Show
    MousePointer = 0
End Sub

Private Sub Nombre_GotFocus()
    If Trim(UCase(Nombre.Text)) = "REGISTRO NO ENCONTRADO" Then
        Nombre.Text = "INGRESE EL NOMBRE DE LA CUENTA"
    End If
    
    Nombre.SelStart = 0
    Nombre.SelLength = Len(Nombre.Text)
End Sub

Private Sub Nombre_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_LostFocus()
    Nombre.Text = FiltroCaracter(Nombre.Text)
End Sub

Private Sub Recibe_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Saldo_Inicial_GotFocus()
    Saldo_Inicial.Text = Trim(Saldo_Inicial.Text)
    
    Saldo_Inicial.SelStart = 0
    Saldo_Inicial.SelLength = Len(Saldo_Inicial.Text)
End Sub

Private Sub Saldo_Inicial_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Saldo_Inicial_LostFocus()
    Saldo_Inicial.Text = Formateado(Str(Val(Saldo_Inicial.Text)), 2, 10, " ", False)
End Sub

Private Sub Salir_Click()
    If Id_Nivel_1.Text = "" And Id_Nivel_2.Text = "" And Id_Nivel_3.Text = "" And Id_Nivel_4.Text = "" And Id_Nivel_5.Text = "" And Id_Nivel_6.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub UpDown_DownClick()
    If Not Rs_Cta.EOF Then
        If Not Val(Id_Nivel_1.Text) = 0 Then
            Rs_Cta.MoveNext
        End If
        Leer_Cuenta
        Mostrar_Cuenta
    Else
        'Rs.MoveFirst
        Leer_Cuenta
        Mostrar_Cuenta
    End If
End Sub

Private Sub Mostrar_Cuenta()
    On Error GoTo Fin
    
Inicio:
    Id_Nivel_1.Text = Val(Rs_Cta.Fields("Cuenta.Id_Nivel_1"))
    Id_Nivel_2.Text = Val(Rs_Cta.Fields("Cuenta.Id_Nivel_2"))
    Id_Nivel_3.Text = Val(Rs_Cta.Fields("Cuenta.Id_Nivel_3"))
    Id_Nivel_4.Text = Val(Rs_Cta.Fields("Cuenta.Id_Nivel_4"))
    Id_Nivel_5.Text = Val(Rs_Cta.Fields("Cuenta.Id_Nivel_5"))
    Id_Nivel_6.Text = Val(Rs_Cta.Fields("Cuenta.Id_Nivel_6"))
    Nombre.Text = Rs_Cta.Fields("Denominacion")
    Saldo_Inicial.Text = Formateado(Str(Val(Rs_Cta.Fields("Saldo_Inicial"))), 2, 10, " ", False)
    
    Cargar_Movimientos
    
    Exit Sub
Fin:
    If Err.Number = 3021 Then
        'Rs.MoveFirst
'        Resume Inicio
        Exit Sub
    End If
End Sub

Private Sub UpDown_UpClick()
    If Not Rs_Cta.BOF Then
        If Val(Id_Nivel_1.Text) >= 1 Then 'And Val(Id_Nivel_2.Text) <> 0 Then
            Rs_Cta.MovePrevious
        End If
        Leer_Cuenta
        Mostrar_Cuenta
    End If
End Sub
