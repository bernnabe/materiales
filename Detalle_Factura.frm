VERSION 5.00
Begin VB.Form Detalle_Factura 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consultas a detalle de Facturas.-"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10695
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6735
   ScaleWidth      =   10695
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Fc 
      Enabled         =   0   'False
      Height          =   975
      Left            =   0
      TabIndex        =   3
      Top             =   0
      Width           =   10695
      Begin VB.TextBox Importe 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9240
         MaxLength       =   10
         TabIndex        =   12
         Text            =   "0123456789"
         Top             =   480
         Width           =   1335
      End
      Begin VB.TextBox Codigo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3600
         MaxLength       =   5
         TabIndex        =   8
         Text            =   "12345"
         Top             =   480
         Width           =   735
      End
      Begin VB.TextBox Nombre 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         MaxLength       =   30
         TabIndex        =   7
         Text            =   "XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
         Top             =   480
         Width           =   3735
      End
      Begin VB.TextBox Comprobante 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   20
         TabIndex        =   6
         Text            =   "XX A 0000-0000000000"
         Top             =   480
         Width           =   2535
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe Total:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8040
         TabIndex        =   13
         Top             =   480
         Width           =   1095
      End
      Begin VB.Label Label4 
         Alignment       =   2  'Center
         Caption         =   "Nombre"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4320
         TabIndex        =   11
         Top             =   240
         Width           =   3735
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2280
         TabIndex        =   10
         Top             =   480
         Width           =   1215
      End
      Begin VB.Label Label1 
         Caption         =   "Tipo  Letra            N�mero "
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   240
         Width           =   1935
      End
   End
   Begin VB.Frame Marco_Item 
      Height          =   4815
      Left            =   0
      TabIndex        =   4
      Top             =   960
      Width           =   10695
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4335
         Left            =   120
         TabIndex        =   1
         Top             =   360
         Width           =   10455
      End
      Begin VB.Label Label10 
         Caption         =   $"Detalle_Factura.frx":0000
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   120
         Width           =   10335
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   5
      Top             =   5760
      Width           =   10695
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   9480
         Picture         =   "Detalle_Factura.frx":00BC
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Detalle_Factura.frx":6346
         Style           =   1  'Graphical
         TabIndex        =   0
         ToolTipText     =   "Confirma la Carga del Detalle de la Factura.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Detalle_Factura"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Comprobante_LostFocus()
    Confirma_Click
End Sub

Public Sub Confirma_Click()
    List1.Clear
    
    Qy = "SELECT * FROM " & IIf(Trim(Codigo.Tag) = "Cliente", "Factura_Venta_Item", "Factura_Compra_Item") & " "
    Qy = Qy & "WHERE Id_Tipo_Factura = '" & Trim(Mid(Comprobante.Text, 1, 2)) & "' "
    Qy = Qy & "AND Id_Letra_Factura = '" & Trim(Mid(Comprobante.Text, 4, 1)) & "' "
    Qy = Qy & "AND Id_Nro_Factura = " & Trim(Str(Val(Mid(Comprobante.Text, 11, 10)))) & " "
    Qy = Qy & "AND Id_Centro_Emisor = " & Trim(Str(Val(Mid(Comprobante.Text, 6, 4)))) & " "
    Qy = Qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    'Qy = Qy & "AND " & IIf(trim(Codigo.Tag) = "Cliente", "Cliente", "Proveedor") & " "
    Qy = Qy & "ORDER BY Linea "
    AbreRs
    
    While Not Rs.EOF
        Txt = Formateado(Str(Val(Rs.Fields("Rubro"))), 0, 4, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Articulo"))), 0, 5, " ", False) & " "
        Txt = Txt & Trim(Rs.Fields("Descripcion")) + Space(38 - Len(Trim(Rs.Fields("Descripcion")))) & " "
        
        If Val(Rs.Fields("Presentacion")) = 3 Then 'P2
            Txt = Txt & Formateado(Str(Val(CalcMed(Rs.Fields("Espesor"), Rs.Fields("Ancho"), Rs.Fields("Largo"), False))), 2, 6, " ", False) & " "
        ElseIf Val(Rs.Fields("Presentacion")) = 4 Then 'M2
            Txt = Txt & Formateado(Str(Val(CalcMed(Rs.Fields("Espesor"), Rs.Fields("Ancho"), Rs.Fields("Largo"), True))), 2, 6, " ", False) & " "
        ElseIf Val(Rs.Fields("Presentacion")) = 5 Then 'Ml
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Largo"))), 2, 6, " ", False) & " "
        Else
            Txt = Txt & "       "
        End If
        
        If Val(Rs.Fields("Presentacion")) = 0 Then
            Txt = Txt & Space(5) & " "
        ElseIf Val(Rs.Fields("Presentacion")) = 1 Then
            Txt = Txt & Trim("Unds.") + Space(5 - Len(Trim("Unds."))) & " "
        ElseIf Val(Rs.Fields("Presentacion")) = 2 Then
            Txt = Txt & Trim("Lts.") + Space(5 - Len(Trim("Lts."))) & " "
        ElseIf Val(Rs.Fields("Presentacion")) = 3 Then
            Txt = Txt & Trim("P2") + Space(5 - Len(Trim("P2"))) & " "
        ElseIf Val(Rs.Fields("Presentacion")) = 4 Then
            Txt = Txt & Trim("M2") + Space(5 - Len(Trim("M2"))) & " "
        ElseIf Val(Rs.Fields("Presentacion")) = 5 Then
            Txt = Txt & Trim("Ml") + Space(5 - Len(Trim("Ml"))) & " "
        ElseIf Val(Rs.Fields("Presentacion")) = 6 Then
            Txt = Txt & Trim("Kgs.") + Space(5 - Len(Trim("Kgs."))) & " "
        End If

        Txt = Txt & Formateado(Str(Val(Rs.Fields("Importe_Total")) / Val(Rs.Fields("Cantidad"))), 4, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Cantidad"))), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Importe_Total"))), 2, 10, " ", False) & " "
        
        List1.AddItem Txt
        Rs.MoveNext
    Wend
    
    List1.SetFocus
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Art�culos correspondientes a la factura seleccionada.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub
