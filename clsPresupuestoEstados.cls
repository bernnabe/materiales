VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsPresupuestoEstados"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private Const constPendiente = 0
Private Const constFacturado = 1
Private Const constPresupuestado = 2
Private Const constCerrado = 3

Public Property Get Pendiente() As Integer

    Pendiente = constPendiente

End Property

Public Property Get Facturado() As Integer

    Facturado = constFacturado

End Property

Public Property Get Presupuestado() As Integer
    
    Presupuestado = constPresupuestado

End Property

Public Property Get Cerrado() As Integer
    
    Cerrado = constCerrado

End Property


Public Function GetDescripcionEstadoByCodigo(codigoEstado As Integer) As String

    Dim strResultado As String
    
    Select Case codigoEstado
        Case constPendiente
            strResultado = "Pendiente"
        Case constFacturado
            strResultado = "Facturado"
        Case constPresupuestado
            strResultado = "Presupuestado"
        Case constCerrado
            strResultado = "Cerrado"
    End Select
    
    GetDescripcionEstadoByCodigo = strResultado

End Function
