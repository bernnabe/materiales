VERSION 5.00
Begin VB.Form SubDiario_Pagos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listado de Pagos.-"
   ClientHeight    =   6660
   ClientLeft      =   45
   ClientTop       =   225
   ClientWidth     =   11430
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6660
   ScaleWidth      =   11430
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Consulta 
      Height          =   1095
      Left            =   75
      TabIndex        =   9
      Top             =   0
      Width           =   11310
      Begin VB.ComboBox Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6735
         TabIndex        =   4
         Top             =   600
         Width           =   4455
      End
      Begin VB.TextBox Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9855
         MaxLength       =   10
         TabIndex        =   3
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Desde 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6735
         MaxLength       =   10
         TabIndex        =   2
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton Cobranza 
         Caption         =   "&Cobranzas Emitidas"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   1935
      End
      Begin VB.OptionButton Pagos 
         Caption         =   "&Pagos Realizado"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Value           =   -1  'True
         Width           =   1935
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5175
         TabIndex        =   20
         Top             =   600
         Width           =   1455
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8655
         TabIndex        =   13
         Top             =   240
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5535
         TabIndex        =   12
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Marco_Lista 
      Height          =   4575
      Left            =   75
      TabIndex        =   10
      Top             =   1050
      Width           =   11310
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4110
         Left            =   75
         TabIndex        =   6
         Top             =   375
         Width           =   11070
      End
      Begin VB.CommandButton Importe 
         Caption         =   "Importe"
         Height          =   255
         Index           =   0
         Left            =   8760
         TabIndex        =   19
         Top             =   120
         Width           =   2355
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Direcci�n"
         Height          =   255
         Left            =   5760
         TabIndex        =   18
         Top             =   120
         Width           =   3015
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Nombre"
         Height          =   255
         Left            =   2520
         TabIndex        =   17
         Top             =   120
         Width           =   3255
      End
      Begin VB.CommandButton Command2 
         Caption         =   "N�mero"
         Height          =   255
         Left            =   1320
         TabIndex        =   16
         Top             =   120
         Width           =   1215
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Fecha"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label3 
         Caption         =   "Fecha           N�mero Nombre                        Direcci�n                   Importe"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   120
         Width           =   9255
      End
   End
   Begin VB.Frame Frame3 
      Height          =   975
      Left            =   75
      TabIndex        =   11
      Top             =   5640
      Width           =   11310
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8625
         Picture         =   "SubDiario_Pagos.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   21
         Top             =   225
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10125
         Picture         =   "SubDiario_Pagos.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   8
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprime 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7500
         Picture         =   "SubDiario_Pagos.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   225
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "SubDiario_Pagos.frx":C80E
         Style           =   1  'Graphical
         TabIndex        =   5
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "SubDiario_Pagos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Cliente_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cliente_LostFocus()
    If Val(Cliente.Text) > 0 Then
        Leer_Cliente
    End If
End Sub

Private Sub Leer_Cliente()
    If Val(Cliente.Text) > 0 Then
        qy = "SELECT Nombre,Id_Cliente FROM Cliente WHERE Id_Cliente = " & Trim(Str(Val(Cliente.Text)))
        AbreRs
        
        If Not Rs.EOF Then
            Cliente.Text = Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields("Id_Cliente"))
        Else
            MsgBox "El Cliente es inexistente...", vbCritical, "Atenci�n.!"
            Cliente.Text = ""
            Cliente.SetFocus
        End If
    Else
        Cliente.Text = ""
        Cliente.SetFocus
    End If
End Sub

Private Sub Cobranza_Click()
    Cargar_Cliente
End Sub

Private Sub Cobranza_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    If Val(Desde.Text) > 0 And Val(Hasta.Text) > 0 Then
        qy = "SELECT Recibo.*," & IIf(Pagos.Value = True, "Proveedor.*", "Cliente.*") & " FROM Recibo," & IIf(Pagos.Value = True, "Proveedor", "Cliente") & " WHERE Tipo_Cuenta = '" & IIf(Pagos.Value = True, "P", "C") & "' "
        If Pagos.Value = True Then
            qy = qy & "AND Cuenta = Proveedor.Id_Proveedor "
        Else
            qy = qy & "AND Cuenta = Cliente.Id_Cliente "
        End If
        qy = qy & "AND (Fecha BETWEEN CONVERT(DATETIME, '" & Format(Desde.Text, "YYYY-MM-DD") & " 00:00:00', 102) AND CONVERT(DATETIME, '" & Format(Hasta.Text, "YYYY-MM-DD") & " 23:59:59.99', 102))"
        'Si Hay una Cuenta seleccionada
        If Val(Mid(Cliente.Text, 31)) > 0 Then qy = qy & "AND Cuenta = " & Trim(Str(Val(Mid(Cliente.Text, 31)))) & " "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Fecha"
        AbreRs
        
        MousePointer = 11
        
        While Not Rs.EOF
            Txt = Format(Rs.Fields("Fecha"), "dd/mm/yyyy") & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Recibo"))), 0, 10, " ", False) & " "
            Txt = Txt & Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " "
            Txt = Txt & Trim(Mid((Rs.Fields("Domicilio")) & Rs.Fields("Numero"), 1, 28)) + Space(28 - Len(Trim(Mid((Rs.Fields("Domicilio")) & Rs.Fields("Numero"), 1, 28)))) & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Ret_Gcias"))), 2, 10, " ", False)
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Importe"))), 2, 10, " ", False)
            
            List1.AddItem Txt
            Rs.MoveNext
        Wend
        
        MousePointer = 0
        Imprime.Enabled = True
        List1.SetFocus
    Else
        MsgBox "Error al ingresar las fechas, verifique e ingrese nuevamente.", vbCritical, "Atenci�n.!"
        Desde.Text = ""
        Hasta.Text = ""
        Desde.SetFocus
    End If
End Sub

Private Sub Desde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_LostFocus()
    Desde.Text = ValidarFecha(Desde.Text)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Sub Diario de Pagos.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4.5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Cliente
End Sub

Private Sub Cargar_Cliente()
    Cliente.Clear
    
    If Pagos.Value = False Then
        qy = "SELECT Nombre,Id_Cliente FROM Cliente ORDER BY Nombre"
        AbreRs
    Else
        qy = "SELECT Nombre,Id_Proveedor FROM Proveedor ORDER BY Nombre"
        AbreRs
    End If
    
    While Not Rs.EOF
        Cliente.AddItem Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields(1))
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_LostFocus()
    Hasta.Text = ValidarFecha(Hasta.Text)
End Sub

Private Sub Pagos_Click()
    Cargar_Cliente
End Sub

Private Sub Pagos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If Desde.Text = "" And Hasta.Text = "" And List1.ListCount = 0 And Pagos.Value = True Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Desde.Text = ""
    Hasta.Text = ""
    Cliente.Text = ""
    List1.Clear
    Pagos.Value = True
    
    Pagos.SetFocus
End Sub
