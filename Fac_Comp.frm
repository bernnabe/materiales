VERSION 5.00
Begin VB.Form Fac_Compras 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Facturaci�n Compras.-"
   ClientHeight    =   7215
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11535
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7215
   ScaleWidth      =   11535
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Captura 
      Height          =   2895
      Left            =   1920
      TabIndex        =   50
      Top             =   2280
      Visible         =   0   'False
      Width           =   7815
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2400
         TabIndex        =   55
         Top             =   960
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.CommandButton Buscar_Cuenta 
         Caption         =   "&Cuenta:"
         Height          =   255
         Left            =   1440
         TabIndex        =   82
         Top             =   960
         Width           =   855
      End
      Begin VB.CommandButton Buscar_Articulos 
         Caption         =   "&Art�culo:"
         Height          =   255
         Left            =   120
         TabIndex        =   69
         TabStop         =   0   'False
         ToolTipText     =   "Buscar Art�culos en la Base de datos.-"
         Top             =   240
         Width           =   735
      End
      Begin VB.ComboBox Presentacion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   4800
         TabIndex        =   54
         Top             =   600
         Width           =   1935
      End
      Begin VB.TextBox Precio_Venta 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   10
         TabIndex        =   58
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox Cantidad 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         MaxLength       =   10
         TabIndex        =   65
         Top             =   2400
         Width           =   1335
      End
      Begin VB.TextBox Total 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5160
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   66
         Top             =   2400
         Width           =   1335
      End
      Begin VB.TextBox Medida 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   10
         TabIndex        =   62
         Top             =   600
         Width           =   1215
      End
      Begin VB.TextBox Espesor 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   9
         TabIndex        =   60
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox Ancho 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         MaxLength       =   10
         TabIndex        =   61
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox Largo 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5160
         MaxLength       =   9
         TabIndex        =   63
         Top             =   1800
         Width           =   1335
      End
      Begin VB.ComboBox Articulos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2400
         TabIndex        =   56
         Top             =   240
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.TextBox Id_Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         MaxLength       =   4
         TabIndex        =   51
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Final_Unitario 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   59
         Top             =   2400
         Width           =   1335
      End
      Begin VB.TextBox Rec_Desc 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   10
         TabIndex        =   64
         Top             =   2400
         Width           =   1335
      End
      Begin VB.TextBox Cuenta 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   30
         TabIndex        =   57
         Top             =   960
         Width           =   4335
      End
      Begin VB.TextBox Articulo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   5
         TabIndex        =   52
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox Descripcion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   30
         TabIndex        =   53
         Top             =   240
         Width           =   4335
      End
      Begin VB.CommandButton Termina 
         Caption         =   "&Termina"
         Height          =   375
         Left            =   6840
         TabIndex        =   70
         ToolTipText     =   "Terminar o cancelar.-"
         Top             =   2400
         Width           =   855
      End
      Begin VB.CommandButton Borrar 
         Caption         =   "&Borra"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6840
         TabIndex        =   68
         ToolTipText     =   "Descargar el art�culo de la factura.-"
         Top             =   2040
         Width           =   855
      End
      Begin VB.CommandButton Confirma 
         Caption         =   "&Confirma"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6840
         TabIndex        =   67
         ToolTipText     =   "Cargar el Art�culo de la factura.-"
         Top             =   1680
         Width           =   855
      End
      Begin VB.Label Label20 
         Alignment       =   2  'Center
         Caption         =   "Precio Vta. Unit."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   81
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label21 
         Alignment       =   2  'Center
         Caption         =   "Cantidad a Retirar"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   80
         Top             =   2160
         Width           =   1335
      End
      Begin VB.Label Label22 
         Alignment       =   2  'Center
         Caption         =   "Total"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5160
         TabIndex        =   79
         Top             =   2160
         Width           =   1335
      End
      Begin VB.Label Label23 
         Alignment       =   1  'Right Justify
         Caption         =   "Medida:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   78
         Top             =   600
         Width           =   2055
      End
      Begin VB.Label Label26 
         Alignment       =   2  'Center
         Caption         =   "Espesor "":"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2280
         TabIndex        =   77
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label27 
         Alignment       =   2  'Center
         Caption         =   "Ancho """
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   76
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label28 
         Alignment       =   2  'Center
         Caption         =   "Largo M"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5160
         TabIndex        =   75
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label30 
         Alignment       =   2  'Center
         Caption         =   "P/Final Unitario"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   74
         Top             =   2160
         Width           =   1335
      End
      Begin VB.Label Label24 
         Alignment       =   2  'Center
         Caption         =   "% Rec +/ Desc -"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2280
         TabIndex        =   73
         Top             =   2160
         Width           =   1335
      End
      Begin VB.Label Label29 
         Alignment       =   1  'Right Justify
         Caption         =   "A Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1440
         TabIndex        =   72
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label19 
         Alignment       =   1  'Right Justify
         Caption         =   "Presentaci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   71
         Top             =   600
         Width           =   975
      End
   End
   Begin VB.Frame Marco_Factura 
      Height          =   1695
      Left            =   0
      TabIndex        =   18
      Top             =   0
      Width           =   11535
      Begin VB.ComboBox Tipo_Comp 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   240
         Width           =   2655
      End
      Begin VB.CommandButton Buscar_Clientes 
         Caption         =   "&Proveedor:"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         TabStop         =   0   'False
         ToolTipText     =   "Mostrar Lista de Proveedores.-"
         Top             =   600
         Width           =   975
      End
      Begin VB.TextBox Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   5
         TabIndex        =   2
         ToolTipText     =   "Ingrese el C�digo del Cliente o ""M"" para ingresarlo manualmente y posteriormente registrarlo en la Base de Datos.-"
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4800
         MaxLength       =   10
         TabIndex        =   1
         ToolTipText     =   "Fecha de Emisi�n.-"
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Direccion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2160
         MaxLength       =   30
         TabIndex        =   4
         Top             =   960
         Width           =   3975
      End
      Begin VB.ComboBox Localidad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   5
         Top             =   1320
         Width           =   3975
      End
      Begin VB.TextBox Letra 
         Alignment       =   2  'Center
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8520
         MaxLength       =   1
         TabIndex        =   6
         ToolTipText     =   "Letra"
         Top             =   240
         Width           =   375
      End
      Begin VB.ComboBox Condicion_Iva 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8520
         TabIndex        =   9
         Top             =   600
         Width           =   2895
      End
      Begin VB.TextBox Cuit 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8520
         MaxLength       =   13
         TabIndex        =   10
         Top             =   960
         Width           =   2895
      End
      Begin VB.TextBox Plazo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8520
         MaxLength       =   4
         TabIndex        =   11
         ToolTipText     =   "Cantidad de d�as al Vencimiento del documento (ciendo cero, el documento es de contado).-"
         Top             =   1320
         Width           =   735
      End
      Begin VB.TextBox Vto 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10080
         MaxLength       =   10
         TabIndex        =   12
         Top             =   1320
         Width           =   1335
      End
      Begin VB.ComboBox Clientes_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2160
         TabIndex        =   19
         Top             =   600
         Visible         =   0   'False
         Width           =   3975
      End
      Begin VB.TextBox Nombre 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2160
         MaxLength       =   30
         TabIndex        =   3
         Top             =   600
         Width           =   3975
      End
      Begin VB.TextBox Centro_Emisor 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8880
         TabIndex        =   7
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Numero 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9480
         MaxLength       =   10
         TabIndex        =   8
         ToolTipText     =   "N�mero.-"
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   29
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4080
         TabIndex        =   28
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Direcci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   27
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Localidad:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   26
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Comprobante:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6720
         TabIndex        =   25
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Condici�n de Iva:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6720
         TabIndex        =   24
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "C.U.I.T.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6720
         TabIndex        =   23
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Plazo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6720
         TabIndex        =   22
         Top             =   1320
         Width           =   1695
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Vto:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9360
         TabIndex        =   21
         Top             =   1320
         Width           =   615
      End
   End
   Begin VB.Frame Marco_Factura_Item 
      Enabled         =   0   'False
      Height          =   3855
      Left            =   0
      TabIndex        =   30
      Top             =   1680
      Width           =   11535
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3435
         Left            =   120
         TabIndex        =   15
         ToolTipText     =   "Haciendo un doble click sobre un item puede borrarlo o modificarlo.-"
         Top             =   360
         Width           =   11295
      End
      Begin VB.Label Label10 
         Caption         =   $"Fac_Comp.frx":0000
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   120
         Width           =   11295
      End
   End
   Begin VB.Frame Marco_Totales 
      BackColor       =   &H8000000A&
      Enabled         =   0   'False
      Height          =   735
      Left            =   0
      TabIndex        =   32
      Top             =   5520
      Width           =   11535
      Begin VB.TextBox Neto_Gravado 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   9
         TabIndex        =   34
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Total_Factura 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9960
         MaxLength       =   10
         TabIndex        =   33
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox Exento 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   9
         TabIndex        =   35
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Sub_Total 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         MaxLength       =   9
         TabIndex        =   36
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Iva_Inscrip 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         MaxLength       =   9
         TabIndex        =   37
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Sobre_Tasa 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4920
         MaxLength       =   9
         TabIndex        =   38
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Retencion_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6120
         MaxLength       =   9
         TabIndex        =   39
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Retencion_IIBB 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7320
         MaxLength       =   9
         TabIndex        =   40
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label11 
         Alignment       =   2  'Center
         Caption         =   "Neto Gravado"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   48
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label12 
         Alignment       =   2  'Center
         Caption         =   "Exento"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1320
         TabIndex        =   47
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label13 
         Alignment       =   2  'Center
         Caption         =   "Sub Total"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2520
         TabIndex        =   46
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "IVA Resp. Ins."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   45
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label15 
         Alignment       =   2  'Center
         Caption         =   "Sobre Tasa"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4920
         TabIndex        =   44
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label16 
         Alignment       =   2  'Center
         Caption         =   "Retenc. IVA"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6120
         TabIndex        =   43
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label17 
         Alignment       =   2  'Center
         Caption         =   "Retenc. IIBB"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7320
         TabIndex        =   42
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "TOTAL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9960
         TabIndex        =   41
         Top             =   120
         Width           =   1455
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   49
      Top             =   6240
      Width           =   11535
      Begin VB.CommandButton Cargar_Importes 
         Caption         =   "Cargar Importes"
         Height          =   615
         Left            =   1440
         TabIndex        =   14
         Top             =   240
         Width           =   1455
      End
      Begin VB.CommandButton Agregar 
         Height          =   615
         Left            =   120
         Picture         =   "Fac_Comp.frx":00CB
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Comenzar a agregar art�culos a la factura.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   9000
         Picture         =   "Fac_Comp.frx":6355
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Grabar e imprimir la factura.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10320
         Picture         =   "Fac_Comp.frx":665F
         Style           =   1  'Graphical
         TabIndex        =   17
         ToolTipText     =   "Salir del m�dulo o cancelar.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Fac_Compras"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Agregar_Click()
    If Trim(Fecha.Text) <> "" And Trim(Mid(Condicion_Iva.Text, 1, 2)) <> "" And Cliente.Text <> "" And (UCase(Mid(Tipo_Comp.Text, 1, 2)) = "FC" Or UCase(Mid(Tipo_Comp.Text, 1, 2)) = "NC" Or UCase(Mid(Tipo_Comp.Text, 1, 2)) = "ND") Then
        If Trim(Letra.Text) <> "" And Val(Numero.Text) > 0 And Val(Centro_Emisor.Text) > 0 Then
            qy = "SELECT * FROM Factura_Compra WHERE Id_Tipo_Factura = '" & Trim(Mid(Tipo_Comp.Text, 1, 2)) & "' AND Id_Letra_Factura = '" & Trim(Letra.Text) & "' AND Id_Centro_Emisor = " & Trim(Str(Val(Centro_Emisor.Text))) & " AND Id_Nro_Factura = " & Trim(Str(Val(Numero.Text))) & " "
            qy = qy & "AND Proveedor = " & Trim(Cliente.Text)
            AbreRs
            
            If Not Rs.EOF Then
                MsgBox "La Factura ya ha sido ingresada.!", vbCritical, "Atenci�n.!"
                Salir_Click
            Else
                Marco_Captura.Visible = True
                Marco_Captura.Enabled = True
                Marco_Factura_Item.Enabled = False
                Marco_Factura.Enabled = False
                Botonera.Enabled = False
                Termina.Cancel = True
                
                Id_Rubro.SetFocus
            End If
        Else
            MsgBox "Error al ingresar los datos del comprobante, verifique e ingrese nuevamente.-", vbCritical, "Atenci�n.!"
            Letra.Text = ""
            Centro_Emisor.Text = ""
            Numero.Text = ""
            
            Letra.SetFocus
        End If
    Else
        Tipo_Comp.SetFocus
    End If
End Sub

Private Sub Ancho_GotFocus()
    Ancho.Text = Trim(Ancho.Text)
    Ancho.SelStart = 0
    Ancho.SelText = ""
    Ancho.SelLength = Len(Ancho.Text)
End Sub

Private Sub Ancho_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_LostFocus()
    Ancho.Text = Formateado(Str(Val(Ancho.Text)), 2, 9, " ", False)
End Sub

Private Sub Articulo_GotFocus()
    Articulo.Text = Trim(Articulo.Text)
    Articulo.SelStart = 0
    Articulo.SelText = ""
    Articulo.SelLength = Len(Articulo.Text)
End Sub

Private Sub Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo_LostFocus
End Sub

Private Sub Articulo_LostFocus()
    If Val(Articulo.Text) > 0 And Val(Id_Rubro.Text) > 0 Then
        Leer_Articulo
    ElseIf Trim(UCase(Id_Rubro.Text)) = "M" Then
        Descripcion.Enabled = True
        Presentacion.Enabled = True
        Cuenta.Enabled = True
        
        Descripcion.SetFocus
    End If
End Sub

Private Sub Leer_Articulo()
    If Val(Articulo.Text) > 0 And Val(Id_Rubro.Text) > 0 Then
        
        qy = "SELECT * FROM Articulo WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text)))
        qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Id_Rubro.Text)))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "El Art�culo es inexistente...", vbInformation, "Atenci�n.!"
            Id_Rubro.Text = ""
            Articulo.Text = ""
            Id_Rubro.SetFocus
        Else
            Mostrar_Articulo
        End If
    Else
        Articulo.Text = ""
        Id_Rubro.SetFocus
    End If
End Sub

Private Sub Mostrar_Articulo()
    Descripcion.Text = Rs.Fields("Descripcion")
    
    Presentacion.ListIndex = Val(Rs.Fields("Presentacion_Rev"))
    Precio_Venta.Text = Formateado(Str(Val(Rs.Fields("Precio_Compra"))), 2, 10, " ", False)
    Precio_Venta.Tag = Val(Precio_Venta.Text)
    Cantidad.Tag = Rs.Fields("Existencia")
    
    If Val(Espesor.Text) = 0 And Val(Ancho.Text) = 0 And Val(Largo.Text) = 0 Then
        Espesor.Text = Formateado(Str(Val(Rs.Fields("Espesor"))), 2, 9, " ", False)
        Ancho.Text = Formateado(Str(Val(Rs.Fields("Ancho"))), 2, 9, " ", False)
        Largo.Text = Formateado(Str(Val(Rs.Fields("Largo"))), 2, 9, " ", False)
    End If
    
    Cuenta.Text = Trim(Rs.Fields("Cuenta_Compra"))
    
    Presentacion_LostFocus 'Porque habilito los campos de medidas
    Mostrar_Medidas
    
    Precio_Venta.Enabled = True
    Precio_Venta.BackColor = vbWhite
    Precio_Venta.SetFocus
    
End Sub

Private Sub Mostrar_Medidas()
    If Presentacion.ListIndex = 4 Then
        Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
    Else
        If Presentacion.ListIndex = 5 Then
            Medida.Text = Largo.Text
        Else
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
        End If
    End If
End Sub

Private Sub Articulos_Encontrados_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Articulos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo.SetFocus
End Sub

Private Sub Articulos_Encontrados_LostFocus()
    Id_Rubro.Text = Trim(Mid(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 32, 4))
    Articulo.Text = Trim(Mid(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 37))
    If Val(Articulo.Text) > 0 Then
        Leer_Articulo
    End If
    Articulos_Encontrados.Visible = False
End Sub

Private Sub Borrar_Click()
    If Trim(Id_Rubro.Text) <> "" Then
        If MsgBox("Desea Remover el Art�culo de la Factura ?.", vbYesNo + vbInformation, "Atenci�n.!") = vbYes Then
            List1.RemoveItem Confirma.Tag
            List1.Refresh
        End If
        
        Calcular_Total_Factura
        Termina_Click
    End If
End Sub

Private Sub Buscar_Articulos_Click()
    Articulos_Encontrados.Visible = True
    If Articulos_Encontrados.ListCount = 0 Then
        MousePointer = 11
        qy = "SELECT * FROM Articulo ORDER BY Descripcion"
        AbreRs
        
        While Not Rs.EOF
            Articulos_Encontrados.AddItem Trim(Rs.Fields("Descripcion")) + Space(30 - Len(Trim(Rs.Fields("Descripcion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 4, " ", False) & " " & (Rs.Fields("Id_Articulo"))
            Rs.MoveNext
        Wend
        MousePointer = 0
    End If
    Articulos_Encontrados.SetFocus
End Sub

Private Sub Buscar_Clientes_Click()
    Clientes_Encontrados.Visible = True
    If Clientes_Encontrados.ListCount = 0 Then
        MousePointer = 11
        qy = "SELECT * FROM Proveedor ORDER BY Nombre"
        AbreRs
        
        While Not Rs.EOF
            Clientes_Encontrados.AddItem Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields("Id_Proveedor"))
            Rs.MoveNext
        Wend
        MousePointer = 0
    End If
    Clientes_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Buscar_Cuenta_Click()
    Cuentas_Encontradas.Clear
    Cuentas_Encontradas.Visible = True
    
    qy = "SELECT * FROM Cuenta WHERE Recibe_Asiento = -1 "
    qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Cuentas_Encontradas.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6"))
        Rs.MoveNext
    Wend
    
    Cuentas_Encontradas.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cantidad_GotFocus()
    Cantidad.Text = Trim(Cantidad.Text)
    Cantidad.SelStart = 0
    Cantidad.SelText = ""
    Cantidad.SelLength = Len(Cantidad.Text)
End Sub

Private Sub Cantidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cantidad_LostFocus()
    Cantidad.Text = Formateado(Str(Val(Cantidad.Text)), 2, 10, " ", False)
    If Val(Cantidad.Text) > 0 Then
        Medida.Text = Val(Medida.Text) * Val(Cantidad.Text)
        Medida.Text = Formateado(Str(Val(Medida.Text)), 2, 9, " ", False)
    End If
End Sub

Private Sub Cargar_Importes_Click()
    If Trim(Fecha.Text) <> "" And Trim(Mid(Condicion_Iva.Text, 1, 2)) <> "" And Cliente.Text <> "" And (UCase(Mid(Tipo_Comp.Text, 1, 2)) = "FC" Or UCase(Mid(Tipo_Comp.Text, 1, 2)) = "NC" Or UCase(Mid(Tipo_Comp.Text, 1, 2)) = "ND") Then
        If Trim(Letra.Text) <> "" And Val(Numero.Text) > 0 And Val(Centro_Emisor.Text) > 0 Then
            qy = "SELECT * FROM Factura_Compra WHERE Id_Tipo_Factura = '" & Trim(Mid(Tipo_Comp.Text, 1, 2)) & "' AND Id_Letra_Factura = '" & Trim(Letra.Text) & "' AND Id_Centro_Emisor = " & Trim(Str(Val(Centro_Emisor.Text))) & " AND Id_Nro_Factura = " & Trim(Str(Val(Numero.Text))) & " "
            qy = qy & "AND Proveedor = " & Trim(Cliente.Text)
            AbreRs
            
            If Not Rs.EOF Then
                MsgBox "La Factura ya ha sido ingresada.!", vbCritical, "Atenci�n.!"
                Salir_Click
            Else
                List1.Clear
                Neto_Gravado.Text = ""
                Exento.Text = ""
                Sub_Total.Text = ""
                Iva_Inscrip.Text = ""
                Sobre_Tasa.Text = ""
                Retencion_Iva.Text = ""
                Retencion_IIBB.Text = ""
                Total_Factura.Text = ""
            
                Marco_Totales.Enabled = True
                Marco_Factura.Enabled = False
                List1.BackColor = &H8000000F
                Agregar.Enabled = False
                Neto_Gravado.SetFocus
            End If
        Else
            MsgBox "Error al ingresar los datos del comprobante, verifique e ingrese nuevamente.-", vbCritical, "Atenci�n.!"
            Letra.Text = ""
            Centro_Emisor.Text = ""
            Numero.Text = ""
            
            Letra.SetFocus
        End If
    Else
        Tipo_Comp.SetFocus
    End If
End Sub

Private Sub Centro_Emisor_GotFocus()
    Centro_Emisor.Text = Trim(Centro_Emisor.Text)
    Centro_Emisor.SelStart = 0
    Centro_Emisor.SelText = ""
    Centro_Emisor.SelLength = Len(Centro_Emisor.Text)
End Sub

Private Sub Centro_Emisor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Centro_Emisor_LostFocus()
    Centro_Emisor.Text = Formateado(Str(Val(Centro_Emisor.Text)), 0, 4, "0", False)
End Sub

Private Sub Cliente_GotFocus()
    Nombre.Enabled = False
    Direccion.Enabled = False
    Localidad.Enabled = False
    Condicion_Iva.Enabled = False
    Cuit.Enabled = False
    
    Cliente.Text = Trim(Cliente.Text)
    Cliente.SelStart = 0
    Cliente.SelText = ""
    Cliente.SelLength = Len(Cliente.Text)
End Sub

Private Sub Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: If Val(Cliente.Text) > 0 Or Trim(UCase(Cliente.Text)) = "M" Then SendKeys "{TAB}"
End Sub

Private Sub Cliente_LostFocus()
    If Val(Cliente.Text) > 0 Then
        Leer_Cliente
    ElseIf Trim(UCase(Cliente.Text)) = "M" Then
        Habilitar_Campo_Cliente
        Nombre.SetFocus
    End If
End Sub

Private Sub Habilitar_Campo_Cliente()
    Nombre.Enabled = True
    Direccion.Enabled = True
    Localidad.Enabled = True
    Condicion_Iva.Enabled = True
    Cuit.Enabled = True
End Sub

Private Sub Leer_Cliente()
    If Val(Cliente.Text) > 0 Then
        qy = "SELECT * FROM Proveedor WHERE Id_Proveedor = " & Trim(Str(Val(Cliente.Text)))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "El Proveedor es inexistente...", vbInformation, "Atenci�n.!"
            Cliente.Text = ""
            Cliente.SetFocus
        Else
            Mostrar_Campo_Cliente
        End If
    Else
        Cliente.Text = ""
        Cliente.SetFocus
    End If
End Sub

Private Sub Mostrar_Campo_Cliente()
    Nombre.Text = Trim(Rs.Fields("Nombre"))
    Direccion.Text = Rs.Fields("Domicilio")
    Localidad.Text = Rs.Fields("Codigo_Postal")
    Condicion_Iva.Text = Rs.Fields("Condicion_Iva")
    Cuit.Text = Rs.Fields("Nro")
    'ret_iibb
    
    If UCase(Condicion_Iva.Text) = "CF" Then
        Condicion_Iva.ListIndex = 0
    ElseIf UCase(Condicion_Iva.Text) = "RI" Then
        Condicion_Iva.ListIndex = 1
    ElseIf UCase(Condicion_Iva.Text) = "NI" Then
        Condicion_Iva.ListIndex = 2
    ElseIf UCase(Condicion_Iva.Text) = "MT" Then
        Condicion_Iva.ListIndex = 3
    ElseIf UCase(Condicion_Iva.Text) = "ET" Then
        Condicion_Iva.ListIndex = 4
    ElseIf UCase(Condicion_Iva.Text) = "NC" Then
        Condicion_Iva.ListIndex = 5
    End If
    
    'Busco la localidad
    If Val(Localidad.Text) > 0 Then
        qy = "SELECT * FROM Localidad WHERE Id_Localidad = " & Trim(Str(Val(Localidad.Text)))
        AbreRs
        
        If Not Rs.EOF Then
            Localidad.Text = Trim(Rs.Fields("Id_LocalidaD")) + Space(5 - Len(Trim(Rs.Fields("Id_LocalidaD")))) & " " & Trim(Rs.Fields("Localidad"))
        End If
    End If
    
    Letra.SetFocus
End Sub

Private Sub Clientes_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Clientes_Encontrados_LostFocus
End Sub

Private Sub Clientes_Encontrados_LostFocus()
    Cliente.Text = Mid(Clientes_Encontrados.List(Clientes_Encontrados.ListIndex), 32)
    Clientes_Encontrados.Visible = False
    If Val(Cliente.Text) > 0 Then
        Leer_Cliente
    Else
        Cliente.SetFocus
    End If
End Sub

Private Sub Condicion_Iva_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Condicion_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    Dim Unidad As String
    
    If Trim(Cuenta.Text) <> "" Then
        Txt = Formateado(Str(Val(Id_Rubro.Text)), 0, 4, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Articulo.Text)), 0, 5, " ", False) & " "
        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
            Txt = Txt & Trim(Mid(Trim(Mid(Descripcion.Text, 1, 28)) & "x" & Trim(Val(Ancho.Text)) & "x" & Trim(Val(Largo.Text)), 1, 38)) + Space(38 - Len(Trim(Mid(Trim(Mid(Descripcion.Text, 1, 28)) & "x" & Trim(Val(Ancho.Text)) & "x" & Trim(Val(Largo.Text)), 1, 38)))) & " "
        Else
            Txt = Txt & Trim(Descripcion.Text) + Space(38 - Len(Trim(Descripcion.Text))) & " "
        End If
        Txt = Txt & IIf(Val(Medida.Text) > 0, Formateado(Str(Val(Medida.Text)), 2, 7, " ", False) & " ", Space(8))
        
        If Val(Mid(Presentacion.Text, 1, 1)) = 0 Then
            Txt = Txt & Space(4) & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 1 Then
            Txt = Txt & "Us. " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 2 Then
            Txt = Txt & "Lts." & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 3 Then
            Txt = Txt & "P2  " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 4 Then
            Txt = Txt & "M2  " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 5 Then
            Txt = Txt & "Ml  " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 6 Then
            Txt = Txt & "Ml" & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 7 Then
            Txt = Txt & "Kgs." & " "
        End If
        
        If Val(Medida.Text) > 0 Then
            Unidad = (Val(Ancho.Text) * 0.2734) * Val(Precio_Venta.Text)
            Unidad = (Val(Unidad) * Val(Alicuota_Iva) / 100) + Val(Unidad)
            
            If Val(Mid(Presentacion.Text, 1, 1)) = 3 Or Val(Mid(Presentacion.Text, 1, 1)) = 4 Then
                Unidad = Val(Precio_Venta.Text)
                Unidad = (Val(Unidad) * Val(Alicuota_Iva) / 100) + Val(Unidad)
                Txt = Txt & Formateado(Str(Val(Precio_Venta.Text)), 4, 8, " ", False) & " "
            ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 5 Then
                Txt = Txt & Formateado(Str(Val(Unidad)), 4, 8, " ", False) & " "
            End If
    
        Else
            Txt = Txt & Formateado(Str(Val(Final_Unitario.Text)), 4, 8, " ", False) & " "
        End If
        
        Txt = Txt & Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Cantidad.Text)), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Total.Text)), 2, 10, " ", False) & "   " '3 espacios porque empieza la carga de datos que no se ve.-
        
        Txt = Txt & Formateado(Str(Val(Cantidad.Tag)), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Espesor.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Ancho.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Largo.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Precio_Venta.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Mid(Presentacion.Text, 1, 2))), 0, 1, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rec_Desc.Text)), 2, 10, " ", False) & " "
        Txt = Txt & Trim(Cuenta.Text) + Space(30 - Len(Trim(Cuenta.Text))) & " "
        
        If Confirma.Tag = "" Then
            List1.AddItem Txt
        Else
            List1.RemoveItem Val(Confirma.Tag)
            List1.Refresh
            List1.AddItem Txt
        End If
        
        Calcular_Total_Factura
        Termina_Click
    Else
        MsgBox "El Movimiento debe estar imputado a alguna cuenta.-", vbInformation, "Atenci�n.!"
        Cuenta.Text = ""
        Cuenta.SetFocus
    End If
End Sub

Private Sub Calcular_Total_Factura()
    Dim i As Long
    
    i = 0
    Neto_Gravado.Text = 0
    Exento.Text = 0
    Sobre_Tasa.Text = 0
    Retencion_Iva.Text = 0
    Retencion_IIBB.Text = 0
    Iva_Inscrip.Text = 0
    Neto_Gravado.Text = 0
    Sub_Total.Text = 0
    
    For i = 0 To List1.ListCount - 1
        Neto_Gravado.Text = Formateado(Str(Val(Neto_Gravado.Text) + Val(Mid(List1.List(i), 95, 10) / Val("1." & Alicuota_Iva))), 2, 0, "", False)
        Iva_Inscrip.Text = (Val(Neto_Gravado.Text) * Val(Alicuota_Iva)) / 100
    Next

    Sub_Total.Text = Neto_Gravado.Text
    
    Neto_Gravado.Text = Formateado(Str(Val(Neto_Gravado.Text)), 2, 9, " ", False)
    Sub_Total.Text = Formateado(Str(Val(Sub_Total.Text)), 2, 9, " ", False)
    Iva_Inscrip.Text = Formateado(Str(Val(Iva_Inscrip.Text)), 2, 9, " ", False)
    
    Exento.Text = Formateado(Str(Val(Exento.Text)), 2, 9, " ", False)
    Sobre_Tasa.Text = Formateado(Str(Val(Sobre_Tasa.Text)), 2, 9, " ", False)
    Retencion_Iva.Text = Formateado(Str(Val(Retencion_Iva.Text)), 2, 9, " ", False)
    Retencion_IIBB.Text = Formateado(Str(Val(Retencion_IIBB.Text)), 2, 9, " ", False)
    
    Total_Factura.Text = Val(Sub_Total.Text) + Val(Iva_Inscrip.Text)
    Total_Factura.Text = Formateado(Str(Val(Total_Factura.Text)), 2, 10, " ", False)
End Sub

Private Sub Cuenta_GotFocus()
    Cuenta.Text = Trim(Cuenta.Text)
    Cuenta.SelStart = 0
    Cuenta.SelText = ""
    Cuenta.SelLength = Len(Cuenta.Text)
End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_LostFocus()
    If Val(Cuenta.Text) = 0 Then Cuenta.Text = "6.1.1.2.0.0"
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuentas_Encontradas_LostFocus
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Cuenta.Text = Trim(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 31))
    'Precio_Venta.SetFocus
    Cuentas_Encontradas.Visible = False
End Sub

Private Sub Cuit_GotFocus()
    Cuit.Text = Trim(Cuit.Text)
    Cuit.SelStart = 0
    Cuit.SelText = ""
    Cuit.SelLength = Len(Cuit.Text)
End Sub

Private Sub Cuit_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuit_LostFocus()
    Cuit.Text = ValidarCuit(Cuit.Text)
    
    If Cuit.Text = "error" Then
        MsgBox "El Cuit Ingresado no es correcto, verifique e ingrese nuevamente.", vbInformation, "Atenci�n.!"
        Cuit.Text = ""
        Cuit.SetFocus
    End If
End Sub

Private Sub Descripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Descripcion_LostFocus()
    Descripcion.Text = FiltroCaracter(Descripcion.Text)
    Descripcion.Text = UCase(Descripcion.Text)
End Sub

Private Sub Direccion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Direccion_LostFocus()
    Direccion.Text = FiltroCaracter(Direccion.Text)
    Direccion.Text = UCase(Direccion.Text)
End Sub

Private Sub Espesor_GotFocus()
    Espesor.Text = Trim(Espesor.Text)
    Espesor.SelStart = 0
    Espesor.SelText = ""
    Espesor.SelLength = Len(Espesor.Text)
End Sub

Private Sub Espesor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Espesor_LostFocus()
    Espesor.Text = Formateado(Str(Val(Espesor.Text)), 2, 9, " ", False)
End Sub

Private Sub Exento_GotFocus()
    Exento.Text = Trim(Exento.Text)
    Exento.SelStart = 0
    Exento.SelLength = Len(Exento.Text)
End Sub

Private Sub Exento_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Exento_LostFocus()
    Exento.Text = Formateado(Str(Val(Exento.Text)), 2, 9, " ", False)
    
End Sub

Private Sub Fecha_GotFocus()
    If Val(Fecha.Text) = 0 Then Fecha.Text = Fecha_Fiscal
    Fecha.SelStart = 0
    Fecha.SelText = ""
    Fecha.SelLength = 10
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
    
    If Trim(Fecha.Text) = "error" Then
        MsgBox "La Fecha ingresada es incorrecta, ingrese nuevamente.", vbInformation, "Atenci�n.!"
        Fecha.SetFocus
    End If
End Sub

Private Sub Final_Unitario_GotFocus()
    Final_Unitario.Text = Trim(Final_Unitario.Text)
    Final_Unitario.SelStart = 0
    Final_Unitario.SelText = ""
    Final_Unitario.SelLength = Len(Final_Unitario.Text)
End Sub

Private Sub Final_Unitario_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Final_Unitario_LostFocus()
    Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 2, 10, " ", False)
    Final_Unitario.Enabled = False
    Final_Unitario.BackColor = 14737632
    
    Precio_Venta.Text = Val(Final_Unitario.Text) / Val("1." & Alicuota_Iva)
    Precio_Venta_LostFocus
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Emisi�n de Comprobantes de Venta.-"
End Sub

Private Sub Form_Load()
    Dim i As Long

    Me.Top = (Screen.Height - Me.Height) / 18
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Tipo_Comp.AddItem "FC - FACTURA        "
    Tipo_Comp.AddItem "ND - NOTA DE D�BITO "
    Tipo_Comp.AddItem "NC - NOTA DE CR�DITO"
    Tipo_Comp.ListIndex = 0
    
    For i = 1 To 6
        Condicion_Iva.AddItem Tipo_Iva(i)
    Next
    
    i = 0
    
    For i = 0 To 8
        Presentacion.AddItem Articulo_Presentacion(i)
    Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    Dim i                 As Long
    Dim l                 As Long
    
    Dim Rec_Rubro         As Long
    Dim Rec_Articulo      As Long
    Dim Rec_Descripcion   As String
    Dim Rec_Presentacion  As Long
    Dim Rec_Cantidad      As Single
    Dim Rec_Espesor       As Single
    Dim Rec_Ancho         As Single
    Dim Rec_Largo         As Single
    Dim Rec_Precio        As Single
    Dim Rec_Impuesto      As Single
    Dim Rec_Existencia    As Single
    
    Dim Rec_Asiento       As Long
    Dim Rec_Cuenta        As String
    Dim Alm_Cta           As String
    Dim Rec_ImporCta      As Single
    
    
    'If Val(List1.ListCount) > 0 Then
    MousePointer = 11
    
    qy = "SELECT MAX(Nro_Asiento) FROM Asiento WHERE Ejercicio = " & Trim(Str(Val(Mid(Fecha.Text, 7, 4)))) & " "
    qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    'AbreRs
        
    'Rec_Asiento = 1
    'If IsNull(Rs.Fields(0)) = False Then Rec_Asiento = Val(Rs.Fields(0)) + 1
        
    qy = "INSERT INTO Factura_Compra VALUES("
    qy = qy & Trim(Str(Val(Id_Empresa)))
    qy = qy & ", '" & Trim(UCase(Mid(Tipo_Comp.Text, 1, 2))) & "'"
    qy = qy & ", '" & Trim(UCase(Letra.Text)) & "'"
    qy = qy & ", " & Trim(Str(Val(Centro_Emisor)))
    qy = qy & ", " & Trim(Str(Val(Numero.Text)))
    qy = qy & ", " & Val(Mid(Fecha_Fiscal, 4, 2))
    qy = qy & ", " & Mid(Fecha_Fiscal, 7, 4)
    qy = qy & ", " & Trim(Str(Val(Plazo.Text)))
    qy = qy & ", '" & Trim(Fecha.Text) & " " & Trim(Hora_Fiscal) & "'"
    qy = qy & ", " & Trim(Str(Val(Cliente.Text)))
    qy = qy & ", " & Trim(Str(Val(Neto_Gravado.Text)))
    qy = qy & ", 0, 0, " & Trim(Str(Val(Exento.Text)))
    qy = qy & ", " & Trim(Str(Val(Sub_Total.Text)))
    qy = qy & ", " & Trim(Str(Val(Iva_Inscrip.Text)))
    qy = qy & ", 0, " & Trim(Str(Val(Retencion_Iva.Text)))
    qy = qy & ", " & Trim(Str(Val(Retencion_IIBB.Text)))
    qy = qy & ", " & Trim(Str(Val(Total_Factura.Text)))
    qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
    qy = qy & ", " & Trim(Str(Val(Mid(Fecha_Fiscal, 7, 4)))) & ")"
    Db.Execute (qy)
        
    If Val(List1.ListCount) > 0 Then
        For i = 0 To List1.ListCount - 1
            
            Rec_Rubro = Val(Mid(List1.List(i), 1, 4))
            Rec_Articulo = Val(Mid(List1.List(i), 6, 5))
            Rec_Descripcion = Trim(Mid(List1.List(i), 12, 30))
            'Rec_Presentacion = mid(List1.List(i), 159, 1)
            Rec_Presentacion = 3
            Rec_Espesor = Mid(List1.List(i), 119, 9)
            Rec_Ancho = Mid(List1.List(i), 129, 9)
            Rec_Largo = Mid(List1.List(i), 139, 9)
            Rec_Precio = Trim(Mid(List1.List(i), 73, 10))
            Rec_Impuesto = Val(Rec_Precio) / Val("1." & Val(Alicuota_Iva))
            Rec_Impuesto = (Val(Rec_Impuesto) * Val(Alicuota_Iva)) / 100
            Rec_Cantidad = Trim(Mid(List1.List(i), 84, 10))
            Rec_Existencia = Trim(Mid(List1.List(i), 98, 10))
            Rec_Cuenta = Trim(Mid(List1.List(i), 172, 30))
            Rec_ImporCta = Val(Mid(List1.List(i), 95, 10)) / Val("1." & Alicuota_Iva)
            
            qy = "INSERT INTO Factura_Compra_Item VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", '" & Trim(UCase(Mid(Tipo_Comp.Text, 1, 2))) & "'"
            qy = qy & ", '" & Trim(UCase(Letra.Text)) & "'"
            qy = qy & ", " & Trim(Str(Val(Centro_Emisor)))
            qy = qy & ", " & Trim(Str(Val(Numero.Text)))
            qy = qy & ", " & Trim(Str(Val(i)))
            qy = qy & ", " & Trim(Str(Val(Rec_Rubro)))
            qy = qy & ", " & Trim(Str(Val(Rec_Articulo)))
            qy = qy & ", '" & Trim(Rec_Descripcion) & "'"
            qy = qy & ", " & Trim(Str(Val(Rec_Presentacion)))
            qy = qy & ", " & IIf(Val(Rec_Espesor) > 0, Trim(Str(Val(Rec_Espesor))), 0)
            qy = qy & ", " & IIf(Val(Rec_Ancho) > 0, Trim(Str(Val(Rec_Ancho))), 0)
            qy = qy & ", " & IIf(Val(Rec_Largo) > 0, Trim(Str(Val(Rec_Largo))), 0)
            qy = qy & ", " & Trim(Str(Val(Rec_Cantidad)))
            qy = qy & ", " & Trim(Str(Val(Rec_Impuesto)))
            qy = qy & ", " & Val(Rec_Precio) & ")"
            Db.Execute (qy)
            
            If Val(Rec_Articulo) > 0 Then
                qy = "UPDATE Articulo SET "
                If Trim(UCase(Mid(Tipo_Comp.Text, 1, 2))) = "FC" Or Trim(UCase(Mid(Tipo_Comp.Text, 1, 2))) = "ND" Then
                    qy = qy & "Existencia = Existencia + " & Trim(Str(Val(Rec_Cantidad)))
                Else
                    qy = qy & "Existencia = Existencia - " & Trim(Str(Val(Rec_Cantidad)))
                End If
                qy = qy & ", Precio_Compra = " & Trim(Str(Val(Rec_Precio) - Val(Rec_Impuesto))) & " "
                qy = qy & "WHERE Id_Articulo = " & Trim(Str(Val(Rec_Articulo))) & " "
                qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Rec_Rubro)))
                Db.Execute (qy)
            End If
        Next
        
    End If
    
    i = 0
    
    If Not Marco_Totales.Enabled = True Then
        For i = 0 To List1.ListCount - 1
            Alm_Cta = ""
            Rec_Cuenta = ""
            Rec_ImporCta = 0
            
            Rec_Cuenta = Trim(Mid(List1.List(i), 172, 30))
            Rec_ImporCta = Val(Mid(List1.List(i), 95, 10)) / Val("1." & Alicuota_Iva)
    
            If Trim(Rec_Cuenta) <> "" Then
                Alm_Cta = ""
                l = 0
                        
                qy = "INSERT INTO Asiento VALUES ("
                qy = qy & Trim(Str(Val(Id_Empresa))) & " "
                qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
                qy = qy & ", '" & Trim(Mid(Fecha.Text, 7, 4)) & "'"
                qy = qy & ", '" & Trim(Fecha.Text) & "'"
                qy = qy & ", " & Val(i) + 1
                    
                For l = 1 To Len(Rec_Cuenta)
                    If Mid(Rec_Cuenta, l, 1) <> "." Then
                        Alm_Cta = Val(Alm_Cta) & Val(Mid(Rec_Cuenta, l, 1))
                    Else
                        qy = qy & ", " & Trim(Str(Val(Alm_Cta))) & " "
                        Alm_Cta = ""
                    End If
                Next
                    
                qy = qy & ", " & Trim(Str(Val(Alm_Cta))) & " "
                qy = qy & ", '" & Mid(Tipo_Comp.Text, 1, 2) & " " & Trim(Letra.Text) & " " & Formateado(Str(Val(Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Numero.Text) & " C'"
                qy = qy & ", " & Formateado(Str(Val(Rec_ImporCta)), 2, 10, " ", False)
                qy = qy & ", 0)"
                'Db.Execute (Qy)
            End If
        Next
    Else
        qy = "INSERT INTO Asiento VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
        qy = qy & ", '" & Trim(Mid(Fecha.Text, 7, 4)) & "'"
        qy = qy & ", '" & Trim(Fecha.Text) & "'"
        qy = qy & ", " & Val(i)
        qy = qy & ", 6, 1, 1, 2, 0, 0"
        'qy = Qy & ", ''"
        qy = qy & ", '" & Mid(Tipo_Comp.Text, 1, 2) & " " & Trim(Letra.Text) & " " & Formateado(Str(Val(Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Numero.Text) & " C'"
        qy = qy & ", " & Formateado(Str(Val(Sub_Total.Text) + Val(Sobre_Tasa.Text) + Val(Retencion_Iva.Text) + Val(Retencion_IIBB.Text)), 2, 10, " ", False)
        qy = qy & ", 0)"
        'Db.Execute (Qy)
    End If
        
    If Val(Iva_Inscrip.Text) > 0 Then
        qy = "INSERT INTO Asiento VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
        qy = qy & ", " & Trim(Str(Val(Mid(Fecha.Text, 7, 4))))
        qy = qy & ", '" & Trim(Fecha.Text) & "'"
        qy = qy & ", " & (Val(i) + 1)
        qy = qy & ", 1, 4, 5, 1, 0, 0"
        qy = qy & ", '" & Mid(Tipo_Comp.Text, 1, 2) & " " & Trim(Letra.Text) & " " & Formateado(Str(Val(Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Numero.Text) & " C'"
        qy = qy & ", " & Trim(Str(Val(Iva_Inscrip.Text)))
        qy = qy & ", 0)"
        'Db.Execute (Qy)
    End If
        
    qy = "INSERT INTO Asiento VALUES ("
    qy = qy & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
    qy = qy & ", " & Trim(Str(Val(Mid(Fecha.Text, 7, 4))))
    qy = qy & ", '" & Trim(Fecha.Text) & "'"
    qy = qy & ", " & (Val(i) + 2)
    If Val(Plazo.Text) > 0 Then
        qy = qy & ", 2, 1, 2, 1, 0, 0"
    Else
        qy = qy & ", 1, 1, 1, 1, 0, 0"
    End If
    qy = qy & ", '" & Mid(Tipo_Comp.Text, 1, 2) & " " & Trim(Letra.Text) & " " & Formateado(Str(Val(Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Numero.Text) & " C'"
    qy = qy & ", 0"
    qy = qy & ", " & Trim(Str(Val(Total_Factura.Text))) & ")"
    'Db.Execute (Qy)
       
    If Val(Plazo.Text) > 0 Then
        qy = "INSERT INTO CtaCte_Proveedor VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Cliente.Text)))
        qy = qy & ", '" & Trim(Fecha.Text) & " " & Trim(Hora_Fiscal) & "'"
        qy = qy & ", '" & Mid(Tipo_Comp.Text, 1, 2) & " " & Trim(Letra.Text) & " " & Formateado(Str(Val(Centro_Emisor.Text)), 0, 4, "0", False) & "-" & Trim(Numero.Text) & "'"
        qy = qy & ", '" & "COMPRAS EN CTACTE" & "'"
        qy = qy & ", '" & Trim(Vto.Text) & "'"
        If Trim(UCase(Mid(Tipo_Comp.Text, 1, 2))) = "FC" Or Trim(UCase(Mid(Tipo_Comp.Text, 1, 2))) = "ND" Then
            qy = qy & ", '" & Trim(Str(Val(Total_Factura.Text))) & "'"
            qy = qy & ", '0')"
        Else
            qy = qy & ", '0'"
            qy = qy & ", '" & Trim(Str(Val(Total_Factura.Text))) & "')"
        End If
        Db.Execute (qy)
    End If
    
    MousePointer = 0
    Salir_Click
End Sub

Private Sub Id_Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        
        If Trim(UCase(Id_Rubro.Text)) = "M" Then
            Articulo_LostFocus
        ElseIf Val(Id_Rubro.Text) > 0 Then
            SendKeys "{tab}"
        End If
    End If
End Sub

Private Sub Id_Rubro_LostFocus()
    Id_Rubro.Text = UCase(Id_Rubro.Text)
End Sub

Private Sub Iva_Inscrip_GotFocus()
    Iva_Inscrip.Text = ((Val(Neto_Gravado.Text) * Val(Alicuota_Iva)) / 100)

    Iva_Inscrip.Text = Trim(Iva_Inscrip.Text)
    Iva_Inscrip.SelStart = 0
    Iva_Inscrip.SelLength = Len(Iva_Inscrip.Text)
End Sub

Private Sub Iva_Inscrip_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Iva_Inscrip_LostFocus()
    Iva_Inscrip.Text = Formateado(Str(Val(Iva_Inscrip.Text)), 2, 9, " ", False)
End Sub

Private Sub Largo_GotFocus()
    Largo.Text = Trim(Largo.Text)
    Largo.SelStart = 0
    Largo.SelText = ""
    Largo.SelLength = Len(Largo.Text)
End Sub

Private Sub Largo_KeyPress(KeyAscii As Integer)
     If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_LostFocus()
    Largo.Text = Formateado(Str(Val(Largo.Text)), 2, 9, " ", False)
    
    If Val(Largo.Text) > 0 Then
        Mostrar_Medidas
        If Val(Mid(Presentacion.Text, 1, 1)) = 3 Then
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 4 Then
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 5 Then
            Medida.Text = Largo.Text & " " & Trim(Mid(Presentacion.Text, 6))
            Final_Unitario.Text = Formateado(Str(Val((Val((Ancho.Text) * 0.2734) * Val(Precio_Venta.Text)) * Val(Largo.Text))), 4, 10, " ", False)
        End If
    End If
    
    If Val(Mid(Presentacion.Text, 1, 1)) = 3 Or Val(Mid(Presentacion.Text, 1, 1)) = 4 Then
        Final_Unitario.Text = Val(Precio_Venta.Text) * Val(Medida.Text)
    End If
    
    '/// ATENCI�N SE REALIZA UN REMARQUE EN LOS ART�CULOS EN ESTE PROCEDIMIENTO!!
    Final_Unitario.Tag = Val(Final_Unitario.Text)
    Aderir_Recargos
    Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 2, 10, " ", False)
    '----------------------------------------------------------------------------
    
    Final_Unitario.Text = Formateado(Str(((Val(Final_Unitario.Text) * Val(Alicuota_Iva)) / 100) + Val(Final_Unitario.Text)), 4, 10, " ", False)
End Sub

Private Sub Letra_GotFocus()
    Letra.Text = Trim(Letra.Text)
    Letra.SelStart = 0
    Letra.SelText = ""
    Letra.SelLength = 1
End Sub

Private Sub Letra_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Letra_LostFocus()
    Letra.Text = UCase(Letra.Text)

    If Trim(UCase(Letra.Text)) = "A" Or Trim(UCase(Letra.Text)) = "B" Or Trim(UCase(Letra.Text)) = "C" Then
        'Esta bien
    Else
        Letra.Text = ""
        Letra.SetFocus
    End If
End Sub

Private Sub List1_DblClick()
    If Trim(Mid(List1.List(List1.ListIndex), 1, 5)) <> "" Then
        Agregar_Click
        
        If Trim(Str(Val(Mid(List1.List(List1.ListIndex), 1, 5)))) > 0 Then
            Id_Rubro.Text = Mid(List1.List(List1.ListIndex), 1, 4)
            Articulo.Text = Mid(List1.List(List1.ListIndex), 6, 5)
            Leer_Articulo
        Else
            Id_Rubro.Text = "M"
            Descripcion.Text = Trim(Mid(List1.List(List1.ListIndex), 12, 31))
            Presentacion.ListIndex = Str(Val(Mid(List1.List(List1.ListIndex), 150, 1)))
            Descripcion.SetFocus
        End If
        
        Rec_Desc.Text = Mid(List1.List(List1.ListIndex), 161, 10)
        Cuenta.Text = Mid(List1.List(List1.ListIndex), 172, 30)
        Precio_Venta.Text = Mid(List1.List(List1.ListIndex), 149, 10)
        Cantidad.Text = Mid(List1.List(List1.ListIndex), 84, 10)
        Espesor.Text = Mid(List1.List(List1.ListIndex), 119, 9)
        Ancho.Text = Mid(List1.List(List1.ListIndex), 129, 9)
        Largo.Text = Mid(List1.List(List1.ListIndex), 139, 9)
        
        If Largo.Enabled = True Then
            Largo_LostFocus
        Else
            Precio_Venta_LostFocus
        End If
        
        Rec_Desc_LostFocus
        Cantidad_LostFocus
        Total_GotFocus
        Total_LostFocus
        
        Borrar.Enabled = True
        Confirma.Tag = List1.ListIndex
    End If
End Sub

Private Sub Localidad_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Localidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Neto_Gravado_GotFocus()
    Neto_Gravado.Text = Trim(Neto_Gravado.Text)
    Neto_Gravado.SelStart = 0
    Neto_Gravado.SelLength = Len(Neto_Gravado.Text)
End Sub

Private Sub Neto_Gravado_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Neto_Gravado_LostFocus()
    Neto_Gravado.Text = Formateado(Str(Val(Neto_Gravado.Text)), 2, 9, " ", False)
End Sub

Private Sub Nombre_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_LostFocus()
    Nombre.Text = FiltroCaracter(Nombre.Text)
    Nombre.Text = UCase(Nombre.Text)
End Sub

Private Sub Numero_GotFocus()
    Numero.Text = Trim(Numero.Text)
    Numero.SelStart = 0
    Numero.SelText = ""
    Numero.SelLength = Len(Numero.Text)
End Sub

Private Sub Numero_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Numero_LostFocus()
    Numero.Text = Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
End Sub

Private Sub Plazo_GotFocus()
    If Trim(Plazo.Text) = "" Or Val(Plazo.Text) = 0 Then Plazo.Text = "C"
    Plazo.Text = Trim(Plazo.Text)
    Plazo.SelStart = 0
    Plazo.SelText = ""
    Plazo.SelLength = Len(Plazo.Text)
End Sub

Private Sub Plazo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Plazo_LostFocus()
    Plazo.Text = Val(Plazo.Text)
    
    If Val(Plazo.Text) > 0 Then
        Vto.Text = DateAdd("d", Val(Plazo.Text), Fecha.Text)
    Else
        Vto.Text = Fecha.Text
    End If
End Sub

Private Sub Presentacion_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Presentacion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Presentacion_LostFocus()
    If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
        Precio_Venta.BackColor = 14737632
        Precio_Venta.Enabled = False
        Largo.Enabled = True
        Largo.BackColor = vbWhite
        Ancho.Enabled = True
        Ancho.BackColor = vbWhite
        'Precio_Unidad.Enabled = True
        If Trim(UCase(Id_Rubro.Text)) = "M" Then
            Precio_Venta.Enabled = True
            Precio_Venta.BackColor = vbWhite
            Espesor.BackColor = vbWhite
            Espesor.Enabled = True
            If Trim(Cuenta.Text) = "" And Cuenta.Enabled = True Then
                Cuenta.SetFocus
            Else
                'Espesor.SetFocus
                Precio_Venta.SetFocus
            End If
        End If
    Else
        Precio_Venta.BackColor = vbWhite
        Precio_Venta.Enabled = True
        Largo.Enabled = False
        Largo.BackColor = 14737632
        Ancho.BackColor = 14737632
        Ancho.Enabled = False
        Espesor.Enabled = False
        Espesor.BackColor = 14737632
        'Precio_Unidad.Enabled = False
    End If
End Sub

Private Sub Rec_Desc_GotFocus()
    If Val(Rec_Desc.Text) > 0 Then
        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
            Largo_LostFocus
        Else
            Precio_Venta_LostFocus
        End If
    End If
    
    Rec_Desc.Text = Trim(Rec_Desc.Text)
    Rec_Desc.SelStart = 0
    Rec_Desc.SelText = ""
    Rec_Desc.SelLength = Len(Rec_Desc.Text)
End Sub

Private Sub Rec_Desc_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rec_Desc_LostFocus()
    If Val(Rec_Desc.Text) <> 0 Then
        'Final_Unitario.Text = ((Val(Final_Unitario.Tag) * Val(Alicuota_Iva)) / 100) + Val(Final_Unitario.Tag)
        Final_Unitario.Text = ((Val(Final_Unitario.Text) * Val(Rec_Desc.Text)) / 100) + Val(Final_Unitario.Text)
        Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
    ElseIf Val(Rec_Desc.Text) = 0 Then
        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
            Largo_LostFocus
        Else
            Precio_Venta_LostFocus
        End If
    End If
    
    Rec_Desc.Text = Formateado(Str(Val(Rec_Desc.Text)), 2, 10, " ", False)
End Sub

Private Sub Retencion_IIBB_GotFocus()
    Retencion_IIBB.Text = Trim(Retencion_IIBB.Text)
    Retencion_IIBB.SelStart = 0
    Retencion_IIBB.SelLength = Len(Retencion_IIBB.Text)
End Sub

Private Sub Retencion_IIBB_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Retencion_IIBB_LostFocus()
    Retencion_IIBB.Text = Formateado(Str(Val(Retencion_IIBB.Text)), 2, 9, " ", False)
    
    Total_Factura.Text = Val(Sub_Total.Text) + Val(Iva_Inscrip.Text) + Val(Sobre_Tasa.Text) + Val(Retencion_Iva.Text) + Val(Retencion_IIBB.Text)
    Total_Factura.Text = Formateado(Str(Val(Total_Factura.Text)), 2, 10, " ", False)
    
    If Val(Total_Factura.Text) > 0 Then
        Grabar.Enabled = True
        Grabar.SetFocus
    End If
End Sub

Private Sub Retencion_Iva_GotFocus()
    Retencion_Iva.Text = Trim(Retencion_Iva.Text)
    Retencion_Iva.SelStart = 0
    Retencion_Iva.SelLength = Len(Retencion_Iva.Text)
End Sub

Private Sub Retencion_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Retencion_Iva_LostFocus()
    Retencion_Iva.Text = Formateado(Str(Val(Retencion_Iva.Text)), 2, 9, " ", False)
End Sub

Private Sub Salir_Click()
    If Cliente.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Grabar.Enabled = False
    Marco_Totales.Enabled = False
    Marco_Factura_Item.Enabled = False
    Marco_Factura.Enabled = True
    Tipo_Comp.ListIndex = 0
    List1.BackColor = vbWhite
    Cliente.Text = ""
    Cliente.Tag = ""
    Vto.Text = ""
    Cuit.Text = ""
    Nombre.Text = ""
    List1.Clear
    Condicion_Iva.Text = ""
    Letra.Text = ""
    Centro_Emisor.Text = ""
    Numero.Text = ""
    Direccion.Text = ""
    Localidad.Text = ""
    Plazo.Text = ""
    Vto.Text = ""
    Agregar.Enabled = True
    
    Borrar_Campo_Importe
    Cliente.SetFocus
End Sub

Private Sub Borrar_Campo_Importe()
    Neto_Gravado.Text = ""
    Exento.Text = ""
    Iva_Inscrip.Text = ""
    Sub_Total.Text = ""
    Sobre_Tasa.Text = ""
    Retencion_Iva.Text = ""
    Retencion_IIBB.Text = ""
    Total_Factura.Text = ""
End Sub

Private Sub Sobre_Tasa_GotFocus()
    Sobre_Tasa.Text = Trim(Sobre_Tasa.Text)
    Sobre_Tasa.SelStart = 0
    Sobre_Tasa.SelLength = Len(Sobre_Tasa.Text)
End Sub

Private Sub Sobre_Tasa_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Sobre_Tasa_LostFocus()
    Sobre_Tasa.Text = Formateado(Str(Val(Sobre_Tasa.Text)), 2, 9, " ", False)
End Sub

Private Sub Sub_Total_GotFocus()
    Sub_Total.Text = Val(Neto_Gravado.Text) + Val(Exento.Text)

    Sub_Total.Text = Trim(Sub_Total.Text)
    Sub_Total.SelStart = 0
    Sub_Total.SelLength = Len(Sub_Total.Text)
End Sub

Private Sub Sub_Total_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Sub_Total_LostFocus()
    Sub_Total.Text = Formateado(Str(Val(Sub_Total.Text)), 2, 9, " ", False)
End Sub

Private Sub Termina_Click()
    If Articulo.Text = "" And Id_Rubro.Text = "" Then
        Marco_Factura_Item.Enabled = True
        Marco_Captura.Visible = False
        Botonera.Enabled = True
        Salir.Cancel = True
        
        Grabar.Enabled = True
        Grabar.SetFocus
    Else
        Borrar_Campo_Captura
    End If
End Sub

Private Sub Borrar_Campo_Captura()
    Id_Rubro.Text = ""
    Articulo.Text = ""
    Descripcion.Text = ""
    Presentacion.Text = ""
    Cuenta.Text = ""
    Final_Unitario.Text = ""
    Precio_Venta.Text = ""
    Precio_Venta.BackColor = 14737632
    Espesor.BackColor = 14737632
    Ancho.BackColor = 14737632
    Largo.BackColor = 14737632
    Precio_Venta.BackColor = 14737632
    Precio_Venta.Tag = ""
    Rec_Desc.Text = ""
    Confirma.Tag = ""
    Cantidad.Text = ""
    Cantidad.Tag = ""
    Total.Text = ""
    Borrar.Enabled = False
    
    Espesor.Text = ""
    Largo.Text = ""
    Ancho.Text = ""
    Medida.Text = ""
    
    Id_Rubro.SetFocus
End Sub

Private Sub Tipo_Comp_GotFocus()
    Fac_Compras.Refresh
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Tipo_Comp_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Total_Change()
    If Val(Total.Text) > 0 And Val(Cantidad.Text) > 0 And (Trim(UCase(Id_Rubro.Text)) = "M" Or (Val(Articulo.Text) > 0 And Val(Id_Rubro.Text) > 0)) Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Total_GotFocus()
    Total.Text = Val(Final_Unitario.Text) * Val(Cantidad.Text)
    Total.SelStart = 0
    Total.SelText = ""
    Total.SelLength = Len(Total.Text)
End Sub

Private Sub Total_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Total_LostFocus()
    Total.Text = Formateado(Str(Val(Total.Text)), 2, 10, " ", False)
End Sub

Private Sub Vto_GotFocus()
    Vto.SelStart = 0
    Vto.SelText = ""
    Vto.SelLength = Len(Vto.Text)
End Sub

Private Sub Vto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Vto_LostFocus()
    Vto.Text = ValidarFecha(Vto.Text)
End Sub

Private Sub Aderir_Recargos()
    Dim Recargo  As Boolean
    
    Recargo = False
    
    qy = "SELECT * FROM Articulo WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " "
    qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Id_Rubro.Text)))
    AbreRs
    
    If Not Rs.EOF Then
    
        If Not Val(Rs.Fields("Ancho")) = Val(Ancho.Text) Then
            If Val(Ancho.Text) < Val(Rs.Fields("Ancho_Menor")) Then
                Recargo = True
                Final_Unitario.Text = (Val(Final_Unitario.Tag) * Val(Rs.Fields("Rec_Ancho_Menor"))) / 100 + Val(Final_Unitario.Tag)
            ElseIf Val(Ancho.Text) > Val(Rs.Fields("Ancho_Mayor")) Then
                If Recargo = False Then
                    Recargo = True
                    Final_Unitario.Text = (Val(Final_Unitario.Tag) * Val(Rs.Fields("Rec_Ancho_Mayor"))) / 100 + Val(Final_Unitario.Tag)
                Else
                    Final_Unitario.Text = (Val(Final_Unitario.Text) * Val(Rs.Fields("Rec_Ancho_Mayor"))) / 100 + Val(Final_Unitario.Text)
                End If
            End If
        End If
        
        If Not Val(Rs.Fields("Largo")) = Val(Largo.Text) Then
            If Val(Largo.Text) < Val(Rs.Fields("Largo_Menor")) Then
                If Recargo = False Then
                    Recargo = True
                    Final_Unitario.Text = (Val(Final_Unitario.Tag) * Val(Rs.Fields("Rec_Largo_Menor"))) / 100 + Val(Final_Unitario.Tag)
                Else
                    Final_Unitario.Text = (Val(Final_Unitario.Text) * Val(Rs.Fields("Rec_Largo_Menor"))) / 100 + Val(Final_Unitario.Text)
                End If
            ElseIf Val(Largo.Text) > Val(Rs.Fields("Largo_Mayor")) Then
                If Recargo = False Then
                    Recargo = True
                    Final_Unitario.Text = (Val(Final_Unitario.Tag) * Val(Rs.Fields("Rec_Largo_Mayor"))) / 100 + Val(Final_Unitario.Tag)
                Else
                    Final_Unitario.Text = (Val(Final_Unitario.Text) * Val(Rs.Fields("Rec_Largo_Mayor"))) / 100 + Val(Final_Unitario.Text)
                End If
            End If
        End If
    
    End If ' Termino el Procedimiento.-
End Sub

Private Sub Precio_Venta_GotFocus()
    Precio_Venta.Text = Trim(Precio_Venta.Text)
    Precio_Venta.SelStart = 0
    Precio_Venta.SelText = ""
    Precio_Venta.SelLength = Len(Precio_Venta.Text)
End Sub

Private Sub Precio_Venta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Venta_LostFocus()
    Precio_Venta.Text = Formateado(Str(Val(Precio_Venta.Text)), 4, 10, " ", False)
    
    If Val(Precio_Venta.Text) > 0 Then
        If Largo.Enabled = True Then
            Largo_LostFocus
        Else
            Final_Unitario.Text = ((Val(Precio_Venta.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Venta)
            Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
        End If
    ElseIf Val(Precio_Venta.Text) = 0 And (Val(Id_Rubro.Text) > 0 Or Trim(UCase(Id_Rubro.Text)) = "M") Then
        Precio_Venta.Text = ""
        
        Final_Unitario.Enabled = True
        Final_Unitario.BackColor = vbWhite
        Final_Unitario.Text = ""
        Final_Unitario.SetFocus
    End If
End Sub
