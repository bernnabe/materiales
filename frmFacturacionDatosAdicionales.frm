VERSION 5.00
Begin VB.Form frmFacturacionDatosAdicionales 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Datos adicionales"
   ClientHeight    =   1200
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6075
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   1200
   ScaleWidth      =   6075
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   1140
      Left            =   75
      TabIndex        =   0
      Top             =   0
      Width           =   5940
      Begin VB.CommandButton cmdCancelar 
         Caption         =   "Cancelar"
         Height          =   390
         Left            =   4725
         TabIndex        =   4
         Top             =   600
         Width           =   1065
      End
      Begin VB.CommandButton btnAceptar 
         Caption         =   "Aceptar"
         Height          =   390
         Left            =   4725
         TabIndex        =   2
         Top             =   225
         Width           =   1065
      End
      Begin VB.ComboBox cboEstados 
         Height          =   315
         Left            =   150
         Style           =   2  'Dropdown List
         TabIndex        =   1
         Top             =   525
         Width           =   3165
      End
      Begin VB.Label Label1 
         Caption         =   "Estado del comprobante"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   150
         TabIndex        =   3
         Top             =   300
         Width           =   1740
      End
   End
End
Attribute VB_Name = "frmFacturacionDatosAdicionales"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mvarOperacionConfirmada As Boolean
Private mvarEstadoSeleccionado As Integer

Public Property Get OperacionConfirmada() As Boolean
    OperacionConfirmada = mvarOperacionConfirmada
End Property

Private Property Let OperacionConfirmada(vdata As Boolean)
    mvarOperacionConfirmada = vdata
End Property

Public Property Get EstadoSeleccionado() As Integer
    EstadoSeleccionado = mvarEstadoSeleccionado
End Property

Private Property Let EstadoSeleccionado(vdata As Integer)
    mvarEstadoSeleccionado = vdata
End Property

Private Sub btnAceptar_Click()

    If Validar() Then
    
        OperacionConfirmada = True
        EstadoSeleccionado = cboEstados.ItemData(cboEstados.ListIndex)
        Unload Me
        
    End If
    
End Sub

Private Function Validar() As Boolean
    
    Dim blnValidar As Boolean
    
    blnValidar = True
    
    If (cboEstados.ListIndex = -1) Then
        
        MsgBox "Debe seleccionar un estado", vbCritical, "Atenci�n"
        cboEstados.SetFocus
        blnValidar = False
    
    End If
    
    Validar = blnValidar

End Function

Private Sub cmdCancelar_Click()
    
    OperacionConfirmada = False
    Unload Me
    
End Sub


Private Sub Form_Load()
    
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    
    CargarCombos
    
End Sub

Private Sub CargarCombos()
    
    'Que pasa si el presupuesto esta facturado y esta siendo modificado.
    'Mostrando el estado pendiente se reabrir�a el presupuesto.
    
    Dim objPresupuestoEstados As New clsPresupuestoEstados
    
    cboEstados.AddItem objPresupuestoEstados.GetDescripcionEstadoByCodigo(objPresupuestoEstados.Pendiente)
    cboEstados.ItemData(cboEstados.NewIndex) = objPresupuestoEstados.Pendiente
    
    cboEstados.AddItem objPresupuestoEstados.GetDescripcionEstadoByCodigo(objPresupuestoEstados.Cerrado)
    cboEstados.ItemData(cboEstados.NewIndex) = objPresupuestoEstados.Cerrado
    
    Set objPresupuestoEstados = Nothing

End Sub
