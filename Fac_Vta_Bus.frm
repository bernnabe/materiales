VERSION 5.00
Begin VB.Form Fac_Vta_Bus 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar artículos."
   ClientHeight    =   6885
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9060
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   ScaleHeight     =   6885
   ScaleWidth      =   9060
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame4 
      Height          =   840
      Left            =   75
      TabIndex        =   12
      Top             =   0
      Width           =   8940
      Begin VB.TextBox Campo 
         BackColor       =   &H00FFFFC0&
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   75
         TabIndex        =   0
         Top             =   300
         Width           =   7440
      End
      Begin VB.CommandButton Buscar 
         Caption         =   "&Buscar"
         Height          =   315
         Left            =   7725
         TabIndex        =   1
         Top             =   300
         Width           =   1140
      End
   End
   Begin VB.Frame Frame1 
      Height          =   5190
      Left            =   75
      TabIndex        =   5
      Top             =   825
      Width           =   8940
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4560
         Left            =   75
         TabIndex        =   2
         Top             =   525
         Width           =   8790
      End
      Begin VB.Frame Frame3 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   615
         Left            =   75
         TabIndex        =   7
         Top             =   150
         Width           =   8775
         Begin VB.CommandButton Command3 
            Caption         =   "Existencia"
            Height          =   315
            Index           =   1
            Left            =   7275
            TabIndex        =   11
            Top             =   75
            Width           =   1515
         End
         Begin VB.CommandButton Command3 
            Caption         =   "Articulo"
            Height          =   315
            Index           =   0
            Left            =   6525
            TabIndex        =   10
            Top             =   75
            Width           =   765
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Rubro"
            Height          =   315
            Left            =   5775
            TabIndex        =   9
            Top             =   75
            Width           =   765
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Descripción"
            Height          =   315
            Left            =   0
            TabIndex        =   8
            Top             =   75
            Width           =   5790
         End
      End
   End
   Begin VB.Frame Frame2 
      Height          =   855
      Left            =   75
      TabIndex        =   6
      Top             =   6000
      Width           =   8940
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Caption         =   "&Salir"
         Height          =   495
         Left            =   7650
         TabIndex        =   4
         Top             =   225
         Width           =   1215
      End
      Begin VB.CommandButton Confirma 
         Caption         =   "&Confirma"
         Height          =   495
         Left            =   120
         TabIndex        =   3
         Top             =   240
         Width           =   1215
      End
   End
End
Attribute VB_Name = "Fac_Vta_Bus"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim k As Long

Private mlngArticulo As Long
Private mlngRubro As Long

Public Property Get ArticuloSeleccionado() As Long
    ArticuloSeleccionado = mlngArticulo
End Property

Public Property Let ArticuloSeleccionado(vdata As Long)
    mlngArticulo = vdata
End Property

Public Property Get RubroSeleccionado() As Long
    RubroSeleccionado = mlngRubro
End Property

Public Property Let RubroSeleccionado(vdata As Long)
    mlngRubro = vdata
End Property

Private Sub Buscar_Click()
    qy = "SELECT * FROM Articulo WHERE Descripcion LIKE '" & Trim(Campo.Text) & "%'"
    AbreRs
    
    List1.Clear
    Cargar_Resultado
End Sub

Private Sub Cargar_Resultado()
    k = 0
    
    While Not Rs.EOF And k <= 50
        Txt = Trim(Mid(Rs.Fields("Descripcion"), 1, 35)) + Space(55 - Len(Trim(Mid(Rs.Fields("Descripcion"), 1, 35)))) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Articulo"))), 0, 5, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Existencia"))), 2, 10, " ", False) & " "
        
        List1.AddItem Txt
        k = k + 1
        Rs.MoveNext
    Wend
    
    Buscar.Enabled = False
    
End Sub

Private Sub Campo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0
        Buscar_Click
        List1.SetFocus
    End If
End Sub

Private Sub Campo_LostFocus()
    Campo.Text = FiltroCaracter(Campo.Text)
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    
    Abrir_Base_Datos
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cargar_Codigo
End Sub

Private Sub Cargar_Codigo()
    If Val(Mid(List1.List(List1.ListIndex), 57, 5)) > 0 Then
        RubroSeleccionado = Val(Mid(List1.List(List1.ListIndex), 57, 5))
        ArticuloSeleccionado = Val(Mid(List1.List(List1.ListIndex), 65, 5))
        
        Me.Visible = False
    End If
End Sub

Private Sub List1_KeyUp(KeyCode As Integer, Shift As Integer)
    If (KeyCode = 34 Or KeyCode = 40) And Val(List1.ListIndex + 1) = List1.ListCount Then
        Cargar_Resultado
    End If
End Sub

Private Sub Salir_Click()
    If Campo.Text = "" And List1.ListCount = 0 Then
        ArticuloSeleccionado = 0
        RubroSeleccionado = 0
    
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    ArticuloSeleccionado = 0
    RubroSeleccionado = 0
    List1.Clear
    Buscar.Enabled = True
    Campo.Text = ""
    k = 0
    
    Campo.SetFocus
End Sub
