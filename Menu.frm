VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{D9AF33E0-7C55-11D5-9151-0000E856BC17}#1.0#0"; "fiscal010724.ocx"
Begin VB.MDIForm Menu 
   BackColor       =   &H00808000&
   Caption         =   "Sistema de Gesti�n Integral.-"
   ClientHeight    =   8040
   ClientLeft      =   165
   ClientTop       =   855
   ClientWidth     =   12165
   LinkTopic       =   "MDIForm1"
   StartUpPosition =   3  'Windows Default
   WindowState     =   2  'Maximized
   Begin VB.Timer Timer2 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   0
      Top             =   480
   End
   Begin VB.Timer Timer1 
      Enabled         =   0   'False
      Interval        =   100
      Left            =   480
      Top             =   0
   End
   Begin MSComctlLib.StatusBar Estado 
      Align           =   2  'Align Bottom
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   7785
      Width           =   12165
      _ExtentX        =   21458
      _ExtentY        =   450
      _Version        =   393216
      BeginProperty Panels {8E3867A5-8586-11D1-B16A-00C0F0283628} 
         NumPanels       =   4
         BeginProperty Panel1 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Bevel           =   2
            Object.Width           =   6085
            Text            =   "-"
            TextSave        =   "-"
         EndProperty
         BeginProperty Panel2 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            AutoSize        =   1
            Bevel           =   2
            Object.Width           =   6085
            Text            =   "Libre..."
            TextSave        =   "Libre..."
         EndProperty
         BeginProperty Panel3 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            AutoSize        =   1
            Bevel           =   2
            Object.Width           =   6085
         EndProperty
         BeginProperty Panel4 {8E3867AB-8586-11D1-B16A-00C0F0283628} 
            Alignment       =   1
            Bevel           =   2
         EndProperty
      EndProperty
   End
   Begin FiscalPrinterLibCtl.HASAR HASAR1 
      Left            =   0
      OleObjectBlob   =   "Menu.frx":0000
      Top             =   0
   End
   Begin VB.Menu Menu_Articulo 
      Caption         =   "&Art�culos"
      Enabled         =   0   'False
      Begin VB.Menu Art_Act 
         Caption         =   "&Actualizaci�n de Art�culos"
      End
      Begin VB.Menu Art_Act_Precio 
         Caption         =   "Actualizaci�n de &Precios"
      End
      Begin VB.Menu Mov_Stk_art 
         Caption         =   "Movimiento de &Stock"
      End
      Begin VB.Menu LsT_Precios 
         Caption         =   "&Lista de Precios"
      End
      Begin VB.Menu LSt_Existencias 
         Caption         =   "Listado de Existencias"
      End
      Begin VB.Menu art_lst_act_precios 
         Caption         =   "Listado de Actualizaci�n de precios"
      End
      Begin VB.Menu Art_Lst_Gral 
         Caption         =   "&Listado General de Art�culos"
      End
      Begin VB.Menu mnuStkEst 
         Caption         =   "Estad�stica de Mvtos de stock"
      End
   End
   Begin VB.Menu Menu_Cliente 
      Caption         =   "&Clientes"
      Enabled         =   0   'False
      Begin VB.Menu aCT_CLIE 
         Caption         =   "Actualizaci�n de Clientes"
      End
      Begin VB.Menu Menu_Cli_Movi 
         Caption         =   "&Movimientos de Cta. Cte"
      End
      Begin VB.Menu Rs_Cta 
         Caption         =   "Resumen de Cta.Cte"
      End
      Begin VB.Menu ctas_ctes_menus 
         Caption         =   "Listado de Saldos"
      End
      Begin VB.Menu Listao_Cliente 
         Caption         =   "Listado de &Clientes"
      End
   End
   Begin VB.Menu Menu_Proveedor 
      Caption         =   "&Proveedores"
      Enabled         =   0   'False
      Begin VB.Menu Act_Prov 
         Caption         =   "Act. de Proveedores"
      End
      Begin VB.Menu Menu_Pro_Movi 
         Caption         =   "Movimientos de Cta. Cte"
      End
      Begin VB.Menu Rs_CtaCte_PRov 
         Caption         =   "&Resumen de Cta.Cte"
      End
      Begin VB.Menu Lst_Saldos_prov 
         Caption         =   "Listado de Saldos"
      End
      Begin VB.Menu lst_prov 
         Caption         =   "Listado de Proveedores"
      End
   End
   Begin VB.Menu Menu_Facturaci�n 
      Caption         =   "&Facturaci�n"
      Begin VB.Menu Vta_Cli_Fac 
         Caption         =   "&Ventas"
      End
      Begin VB.Menu Compr_Prov_Fac 
         Caption         =   "&Compras"
      End
      Begin VB.Menu mnu_fac_sep_rc 
         Caption         =   "-"
      End
      Begin VB.Menu Rc_Pago 
         Caption         =   "&Recibo de Pago"
      End
      Begin VB.Menu Sub_Menu_pagos 
         Caption         =   "Listado de Recibos"
      End
      Begin VB.Menu seporden 
         Caption         =   "-"
      End
      Begin VB.Menu Orde_Pago_Fac 
         Caption         =   "&Orden de Pago"
      End
      Begin VB.Menu Lst_Ord_Menu_Emi 
         Caption         =   "Listado de �rdenes de Pago"
      End
      Begin VB.Menu sep_fac_fac 
         Caption         =   "-"
      End
      Begin VB.Menu Fac_Prsu 
         Caption         =   "&Presupuestos"
      End
      Begin VB.Menu lst_resu_emi_menu 
         Caption         =   "Listado de Presupuestos"
      End
      Begin VB.Menu sep_Fac_presu 
         Caption         =   "-"
      End
      Begin VB.Menu Menu_Np 
         Caption         =   "&Nota de Pedido"
         Enabled         =   0   'False
      End
      Begin VB.Menu lst_np_menu 
         Caption         =   "&Pedidos Emitidos"
         Enabled         =   0   'False
      End
      Begin VB.Menu sep_fac_orden 
         Caption         =   "-"
      End
      Begin VB.Menu fac_orden_entrega 
         Caption         =   "&Orden de Entrega"
      End
      Begin VB.Menu mnuOrdenDevo 
         Caption         =   "Nota de Devoluci�n"
      End
      Begin VB.Menu mnuLstOrdenes 
         Caption         =   "Listado de Ordenes"
      End
      Begin VB.Menu s10 
         Caption         =   "-"
      End
      Begin VB.Menu inf_x 
         Caption         =   "Informe "" X """
      End
      Begin VB.Menu Cierre_z 
         Caption         =   "Cierre Jornal "" Z """
      End
   End
   Begin VB.Menu Menu_Contan 
      Caption         =   "&Contabilidad"
      Enabled         =   0   'False
      Begin VB.Menu Plan_Cta 
         Caption         =   "&Plan de Cuentas.-"
      End
      Begin VB.Menu s4 
         Caption         =   "-"
      End
      Begin VB.Menu act_asientos 
         Caption         =   "&Registrar en el Libro Diario.-"
      End
      Begin VB.Menu book_day 
         Caption         =   "&Libro Diario.-"
      End
      Begin VB.Menu book_big 
         Caption         =   "&Libro Mayor.-"
      End
      Begin VB.Menu Bal_Sum_sal 
         Caption         =   "Balance de Sumas y Saldos.-"
      End
      Begin VB.Menu s341 
         Caption         =   "-"
      End
      Begin VB.Menu Fac_Sub_Vtas 
         Caption         =   "Sub &Diario Ventas y Compras.-"
      End
      Begin VB.Menu sc4 
         Caption         =   "-"
      End
      Begin VB.Menu Apetu_eje 
         Caption         =   "Actualizaci�n de &Ejercicios.-"
      End
   End
   Begin VB.Menu Menu_Banco 
      Caption         =   "&Banco"
      Enabled         =   0   'False
      Begin VB.Menu Menu_Bco_Lbro 
         Caption         =   "&Libro Banco.-"
      End
      Begin VB.Menu SEP_BANCO 
         Caption         =   "-"
      End
      Begin VB.Menu menu_che_cart 
         Caption         =   "&Cartera de cheques.-"
      End
      Begin VB.Menu act_Cheques 
         Caption         =   "Actualizaci�n de Cheques.-"
      End
      Begin VB.Menu sebac2 
         Caption         =   "-"
      End
      Begin VB.Menu Aux_ent_ban 
         Caption         =   "&Actualizaci�n de Entidades Bancarias.-"
      End
      Begin VB.Menu act_cta_bcas 
         Caption         =   "&Actualizaci�n de Cuentas bancarias.-"
      End
   End
   Begin VB.Menu menu_informa 
      Caption         =   "&Informes"
      Enabled         =   0   'False
      Begin VB.Menu Libro_Iva_Meni 
         Caption         =   "&Libros de I.V.A.-"
      End
   End
   Begin VB.Menu Men_Tab_Aux 
      Caption         =   "&Auxiliares"
      Enabled         =   0   'False
      Begin VB.Menu Menu_DB 
         Caption         =   "&Base de Datos.-"
      End
      Begin VB.Menu sep12 
         Caption         =   "-"
      End
      Begin VB.Menu Menu_Loc 
         Caption         =   "&Localidades.-"
      End
      Begin VB.Menu Rub_men 
         Caption         =   "&Rubros.-"
         Enabled         =   0   'False
         Visible         =   0   'False
      End
      Begin VB.Menu Menu_Usuarios 
         Caption         =   "&Usuarios.-"
      End
      Begin VB.Menu Saldos_Iniciales_Act 
         Caption         =   "&Saldos Iniciales.-"
      End
      Begin VB.Menu s6 
         Caption         =   "-"
      End
      Begin VB.Menu parametrizacion 
         Caption         =   "&Panel de Control.-"
      End
   End
   Begin VB.Menu Ventanas 
      Caption         =   "&Ventana"
      Enabled         =   0   'False
      WindowList      =   -1  'True
   End
   Begin VB.Menu Menu_Help 
      Caption         =   "&Ayuda"
      Enabled         =   0   'False
      Begin VB.Menu Menu_Help_Index 
         Caption         =   "�ndice.-"
      End
      Begin VB.Menu s7 
         Caption         =   "-"
      End
      Begin VB.Menu Menu_Help_Abaut_of 
         Caption         =   "Acerca de..."
      End
   End
   Begin VB.Menu Salidas 
      Caption         =   "&Salida"
      Enabled         =   0   'False
      Begin VB.Menu salid_rap 
         Caption         =   "Salida R�pida.-"
         Shortcut        =   {F9}
      End
   End
End
Attribute VB_Name = "Menu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'SISTEMA DE GESTI�N INTEGRAL VER 1.0
'FECHA DE INICIO: 05/2001
'MODELO IMPRESORA: P320/F HASAR

Option Explicit
Dim Fe            As String
Dim Cad           As String
Dim Carac         As String

Private Sub act_asientos_Click()
    Load Contab_Asientos
    Contab_Asientos.Show
End Sub

Private Sub aCT_CLIE_Click()
    Load Dat_Cli
    Dat_Cli.Show
End Sub

Private Sub act_cta_bcas_Click()
    Dat_Ctabcas.Show
End Sub

Private Sub Act_Prov_Click()
    Load Dat_Pro
    Dat_Pro.Show
End Sub

Private Sub Apetu_eje_Click()
    If Permiso_Alta = 1 Then
        Load Contab_Dat_Ejercicio
        Contab_Dat_Ejercicio.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.-", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Art_Act_Click()
    MousePointer = 11
    Load Dat_Arti
    Dat_Arti.Show
'
    'Dat_Art.Show
    MousePointer = 0
End Sub

Private Sub Art_Act_Precio_Click()
    If Val(Permiso_Mod) = 0 Then
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    Else
        Load Act_PreciosArt
        Act_PreciosArt.Show
    End If
End Sub

Private Sub art_lst_act_precios_Click()
    
    If Val(Permiso_Cons) = 1 Then
        Load frmListaActualizacionPrecios
        frmListaActualizacionPrecios.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If

End Sub

Private Sub Art_Lst_Gral_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Lst_Gral_Art
        Lst_Gral_Art.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Aux_ent_ban_Click()
    Tbl_Banco.Show
End Sub

Private Sub Bal_Sum_sal_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Contab_BalanceGral
        Contab_BalanceGral.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub book_big_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Contab_LibroMayor
        Contab_LibroMayor.Show
    Else
        MsgBox "Usted no esta autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub book_day_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Contab_LibroDiario
        Contab_LibroDiario.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Cierre_z_Click()
    If MsgBox("Desea imprimir el cierre del d�a 'Z' ahora. ?", vbQuestion + vbYesNo, "ATENCI�N.!") = vbYes Then
        MousePointer = 11
        
        Inicializar_Impresora
        
        Menu.HASAR1.Encabezado(11) = ""
        Menu.HASAR1.Encabezado(12) = ""
        Menu.HASAR1.Encabezado(13) = ""
        Menu.HASAR1.Encabezado(14) = ""
        
        Menu.HASAR1.ReporteZ
        
        Menu.HASAR1.Finalizar
        
        MousePointer = 0
    End If
End Sub

Private Sub Compr_Prov_Fac_Click()
    If Val(Permiso_Alta) = 1 Then
        Load Fac_Compras
        Fac_Compras.Show
        'Fac_Comp.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub ctas_ctes_menus_Click()
    Load Lst_Cli_Saldos
    Lst_Cli_Saldos.Show
End Sub

Private Sub Estado_PanelDblClick(ByVal Panel As MSComctlLib.Panel)
    If MsgBox("Desea cerrar esta seci�n y entrar con otro nombre de usuario o empresa. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
        Inicial.Show
    End If
End Sub

Private Sub fac_orden_entrega_Click()
    
    Load Fac_Orden
    Fac_Orden.Show

End Sub

Private Sub Fac_Prsu_Click()
    If Val(Permiso_Alta) = 1 Then
        Load Fac_Presu
        Fac_Presu.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Fac_Sub_Vtas_Click()
    Load SubDiario_ComVent
    SubDiario_ComVent.Show
End Sub

Private Sub inf_x_Click()
    If MsgBox("Desea imprimir un informe parcial 'X' ahora. ?", vbQuestion + vbYesNo, "ATENCI�N.!") = vbYes Then
        MousePointer = 11
        
        Inicializar_Impresora
        
        Menu.HASAR1.Encabezado(11) = ""
        Menu.HASAR1.Encabezado(12) = ""
        Menu.HASAR1.Encabezado(13) = ""
        Menu.HASAR1.Encabezado(14) = ""
        
        Menu.HASAR1.ReporteX
        
        Menu.HASAR1.Finalizar
        
        MousePointer = 0
    End If
End Sub

Private Sub Libro_Iva_Meni_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Libro_Iva
        Libro_Iva.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Listao_Cliente_Click()
    Load Lst_Cli
    Lst_Cli.Show
End Sub
    
Private Sub LSt_Existencias_Click()
    Load Lst_Stk
    Lst_Stk.Show
End Sub

Private Sub Lst_Ord_Menu_Emi_Click()
    If Val(Permiso_Cons) = 1 Then
        frmLstOrdenesPago.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Lst_Precios_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Lst_Prec
        Lst_Prec.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Lst_Prov_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Lst_Pro
        Lst_Pro.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub lst_resu_emi_menu_Click()
    If Val(Permiso_Cons) = 1 Then
        frmListaPresupuestos.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Lst_Saldos_prov_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Lst_Pro_Saldos
        Lst_Pro_Saldos.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub MDIForm_Load()

    Hora_Fiscal = Format(Time, "hh:mm:ss")
    Fecha_Fiscal = Format(GetDateFromServer(), "dd/mm/yyyy")
    
    Fe = Format(Fecha_Fiscal, "Long Date")
    Cad = Len(Fe)
    Carac = 0
    
    If (RunTimeEnviropment = Development) Then
        Menu.Estado.Panels(1).Text = "Versi�n: " & App.Major & "." & App.Minor & "." & App.Revision & " en Development\" & DbServerName
    Else
        Menu.Estado.Panels(1).Text = "Versi�n: " & App.Major & "." & App.Minor & "." & App.Revision & " en " & DbServerName
    End If
End Sub

Private Sub MDIForm_QueryUnload(Cancel As Integer, UnloadMode As Integer)
    Cancel = 1
    Menu.Estado.Panels(2).Text = "Confirma la Salida del Sistema. ?"
    If MsgBox("Esto finalizar� la ejecuci�n del sistema, desea continuar. ?", vbQuestion + vbYesNo, "Salida del Sistema.") = vbYes Then
        Cancel = 0
    Else
        Menu.Estado.Panels(2).Text = "Libre..."
    End If
End Sub

Private Sub MDIForm_Unload(Cancel As Integer)
    Db.Close
End Sub

Private Sub Menu_Bco_Lbro_Click()
    Libro_Banco.Show
End Sub

Private Sub menu_che_cart_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Inf_CartCheques
        Inf_CartCheques.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Menu_Cli_Movi_Click()
    Load CtaCte_Movi
    CtaCte_Movi.Show
End Sub

Private Sub Menu_DB_Click()
    Load Mant_BD
    Mant_BD.Show
End Sub

Private Sub Menu_Help_Abaut_of_Click()
    Load Ayuda_AcercaDe
    Ayuda_AcercaDe.Show
End Sub

Private Sub Menu_Help_Index_Click()
    Load Ayuda_Indice
    Ayuda_Indice.Show
End Sub

Private Sub Menu_Loc_Click()
    Load Tbl_Localidad
    Tbl_Localidad.Show
End Sub

Private Sub Menu_Pro_Movi_Click()
    Load CtaCte_Movi
    CtaCte_Movi.Show
    
    CtaCte_Movi.Proveedor.Value = True
    CtaCte_Movi.Proveedor.SetFocus
End Sub

Private Sub Menu_Usuarios_Click()
    If Val(Permiso_Admin) = 1 Then
        Load Tbl_User
        Tbl_User.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub mnuLstOrdenes_Click()
    frmListaOrdenesEntregas.Show
End Sub

Private Sub mnuOrdenDevo_Click()
    Fac_Orden_Devolucion.Show
End Sub

Private Sub mnuStkEst_Click()
    Art_Estadistica.Show
End Sub

Private Sub Mov_Stk_art_Click()
    Load Stk_Mov
    Stk_Mov.Show
End Sub

Private Sub Orde_Pago_Fac_Click()
    If Val(Permiso_Alta) = 1 Then
        Load Fac_OP
        Fac_OP.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub parametrizacion_Click()
    If Val(Permiso_Admin) = 1 Then
        'Load Tbl_Paramet
        'Tbl_Paramet.Show
        
        Tbl_PanelCtrol.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Plan_Cta_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Contab_PlanCtas
        Contab_PlanCtas.Show
    End If
End Sub

Private Sub Rc_Pago_Click()
    If Val(Permiso_Alta) = 1 Then
        Load Fac_Recibo
        Fac_Recibo.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Rs_Cta_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Inf_ResCtaCli
        Inf_ResCtaCli.Show
    Else
        MsgBox "Usted no est� autorizado parar estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Rs_CtaCte_PRov_Click()
    If Val(Permiso_Cons) = 1 Then
        Load Inf_ResCtaPro
        Inf_ResCtaPro.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Rub_men_Click()
    Load Tbl_Rubro
    Tbl_Rubro.Show
End Sub

Private Sub Saldos_Iniciales_Act_Click()
    Load Tbl_SldInic
    Tbl_SldInic.Show
End Sub

Private Sub salid_rap_Click()
    End
End Sub

Private Sub Sub_Menu_pagos_Click()
    If Val(Permiso_Cons) = 1 Then
        frmLstRecibos.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub

Private Sub Timer1_Timer()
    If Timer1.Interval = 60000 Then
        Estado.Panels(3).Text = ""
    End If
    
    Timer1.Interval = 150
    
    If Not Val(Carac) = Val(Cad) Then
        Carac = Carac + 1
        Estado.Panels(3).Text = Estado.Panels(3).Text & Mid(Fe, Carac, 1)
    Else
        Timer1.Interval = 60000
        Carac = 0
    End If
End Sub

Private Sub Timer2_Timer()
    Hora_Fiscal = Format(Time, "hh:mm:ss")
    Fecha_Fiscal = Format(Now, "dd/mm/yyyy")
End Sub

Private Sub Vta_Cli_Fac_Click()
    If Val(Permiso_Alta) = 1 Then
        Load Fac_Vta
        Fac_Vta.Show
    Else
        MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
    End If
End Sub
