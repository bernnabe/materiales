Attribute VB_Name = "Enums"
Option Explicit

Public Enum SystemEnviropment
    Development
    Testing
    Integration
    PreProdution
    Prodution
End Enum

Public Enum ModoIngresoDeCapturaArticulos
    BusquedaPorCodigo
    Manual
End Enum

Public Enum CapturaArticulosAccion
    Agregar
    Borrar
    Editar
    Visualizar
End Enum

Public Enum TiposCliente
    Publico
    Revendedor
End Enum

Public Enum CondicionesVenta
    Contado
    CuentaCorriente
End Enum

Public Enum TiposComprobantes
    FacturaVenta
    FacturaCompra
    NotaDebito
    NotaCredito
    NotaPedido
    OrdenEntrega
    OrdenDevolucion
    Presupuesto
    Recibo
    OrdenPago
End Enum

Public Enum ArticuloPresentaciones
    NoEspecificado = 0
    Unidades = 1
    Litros = 2
    PieCuadrado = 3
    MetroCuadrado = 4
    MetroLinealDeMadera = 5
    MetroLineal = 6
    Kilogramo = 7
    MetroCuadradoAxL = 8
End Enum
