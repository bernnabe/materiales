VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Lst_Cli 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listado de Clientes.-"
   ClientHeight    =   6855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11535
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6855
   ScaleWidth      =   11535
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1095
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   11535
      Begin VB.TextBox Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   10560
         TabIndex        =   1
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Desde 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   5
         TabIndex        =   0
         Top             =   240
         Width           =   855
      End
      Begin VB.ComboBox Tipo_Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1560
         TabIndex        =   2
         Top             =   600
         Width           =   9855
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Tipo de Cliente:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Hasta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9600
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         BackStyle       =   0  'Transparent
         Caption         =   "Desde:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   600
         TabIndex        =   10
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4815
      Left            =   0
      TabIndex        =   8
      Top             =   1080
      Width           =   11535
      Begin VB.Frame Marco_Prog 
         Height          =   1695
         Left            =   2760
         TabIndex        =   18
         Top             =   1560
         Visible         =   0   'False
         Width           =   6015
         Begin MSComctlLib.ProgressBar Prog 
            Height          =   255
            Left            =   120
            TabIndex        =   19
            Top             =   1320
            Width           =   5775
            _ExtentX        =   10186
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
         End
         Begin VB.Label Label5 
            Caption         =   "Aguarde un Momento..."
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   21
            Top             =   120
            Width           =   1815
         End
         Begin VB.Label Label4 
            Caption         =   "0%                                                    Progreso                                                100%"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   20
            Top             =   1080
            Width           =   5775
         End
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4335
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   11295
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Cond.Iva. CUIT"
         Height          =   255
         Left            =   9360
         TabIndex        =   17
         Top             =   120
         Width           =   2055
      End
      Begin VB.CommandButton Command5 
         Caption         =   "Localidad"
         Height          =   255
         Left            =   7320
         TabIndex        =   16
         Top             =   120
         Width           =   2055
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Direcciσn"
         Height          =   255
         Left            =   4080
         TabIndex        =   15
         Top             =   120
         Width           =   3255
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Nombre"
         Height          =   255
         Left            =   720
         TabIndex        =   14
         Top             =   120
         Width           =   3375
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Cσd."
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   120
         Width           =   615
      End
   End
   Begin VB.Frame Frame3 
      Height          =   975
      Left            =   0
      TabIndex        =   9
      Top             =   5880
      Width           =   11535
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8880
         Picture         =   "Lst_Cli.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10320
         Picture         =   "Lst_Cli.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7800
         Picture         =   "Lst_Cli.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Imprimir lista de Clientes.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Comenzar 
         Height          =   615
         Left            =   120
         Picture         =   "Lst_Cli.frx":C80E
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Confirma la Carga del Listado.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Lst_Cli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Order      As String

Private Sub Comenzar_Click()
    Dim Sum As Single
    
    List1.Clear
    MousePointer = 11
    
    qy = "SELECT Cliente.*,Localidad.* FROM Cliente,Localidad WHERE Id_Cliente = Id_Cliente "
    If Val(Desde.Text) > 0 And Val(Hasta.Text) > 0 Then qy = qy & " AND Id_Cliente BETWEEN " & Val(Desde.Text) & " AND " & Val(Hasta.Text) & " "
    If Trim(Mid(Tipo_Cliente.Text, 1, 2)) <> "" Then qy = qy & " AND Condicion_Iva = '" & Trim(Mid(Tipo_Cliente.Text, 1, 2)) & "' "
    qy = qy & "AND Cliente.Codigo_Postal = Localidad.Id_Localidad "
    qy = qy & "ORDER BY " & Trim(Order)
    AbreRs
    
    Sum = 0
    Prog.Value = 0
    If Not Rs.EOF And Val(Rs.RecordCount) > 0 Then Sum = 100 / Val(Rs.RecordCount)
    
    Marco_Prog.Visible = True
    Marco_Prog.Refresh
    
    While Not Rs.EOF
        Txt = Formateado(Str(Val(Rs.Fields("Id_Cliente"))), 0, 5, " ", False) & " "
        Txt = Txt & Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " "
        Txt = Txt & Trim(Rs.Fields("Domicilio") & " " & IIf(Val(Rs.Fields("Numero")) > 0, Trim(Rs.Fields("Numero")), "")) + Space(30 - Len(Trim(Rs.Fields("Domicilio") & " " & IIf(Val(Rs.Fields("Numero")) > 0, Trim(Rs.Fields("Numero")), "")))) & " "
        Txt = Txt & Trim(Mid(Rs.Fields("Localidad"), 1, 18)) + Space(18 - Len(Trim(Mid(Rs.Fields("Localidad"), 1, 18)))) & " "
        Txt = Txt & Trim(Rs.Fields("Condicion_Iva")) + Space(2 - Len(Trim(Rs.Fields("Condicion_Iva")))) & " "
        Txt = Txt & Trim(Rs.Fields("Nro"))
        
        If (Val(Prog.Value) + Val(Sum)) < 100 Then Prog.Value = Val(Sum) + Val(Prog.Value)
        
        List1.AddItem Txt
        Rs.MoveNext
    Wend
    
    Prog.Value = 100
    Marco_Prog.Visible = False
    
    Imprimir.Enabled = True
    MousePointer = 0
    List1.SetFocus
End Sub

Private Sub Command1_Click()
    Order = "Id_Cliente"
    Comenzar_Click
End Sub

Private Sub Command2_Click()
    Order = "Nombre"
    Comenzar_Click
End Sub

Private Sub Command3_Click()
    Order = "Domicilio"
    Comenzar_Click
End Sub

Private Sub Command4_Click()
    Order = "Condicion_Iva"
    Comenzar_Click
End Sub

Private Sub Desde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Listado inverso de Clientes.-"
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 7
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Order = "Nombre"
    
    For i = 1 To 6
        Tipo_Cliente.AddItem Tipo_Iva(i)
    Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
    
    i = 0
    l = 0
    
    If List1.ListCount > 0 Then
        
        Printer.Font = "Courier New"
        Printer.FontSize = 9
        
        Imprimir_Encabezado
        
        For i = 0 To List1.ListCount - 1
            
            If l <= 70 Then
                Printer.Print " " & Mid(List1.List(i), 1, 170)
                l = l + 1
            Else
                Printer.Print " "
                
                l = 0
                Printer.NewPage
                Imprimir_Encabezado
            End If
        Next
        
        Printer.Print " "
        Printer.EndDoc
    End If
    
    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Dim Titulo As String
    Titulo = "                                  LISTADO DE CLIENTES POR ORDEN ALFABΙTICO."
    
    Imprimir.Tag = Int(List1.ListCount / 70) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                    Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                   Fecha..: " & Format(Fecha_Fiscal, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                             Hora...: " & Format(Hora_Fiscal, "hh.mm")
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " E-Mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print Titulo
    Printer.Print
    Printer.Print " "
    Printer.Print "  Cσd. Nombre                         Domicilio                      Localidad          CI C.U.I.T."
    Printer.Print " "
End Sub

Private Sub Salir_Click()
    If Desde.Text = "" And Hasta.Text = "" And List1.ListCount = 0 And Tipo_Cliente.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Desde.Text = ""
    Hasta.Text = ""
    Tipo_Cliente.Text = ""
    List1.Clear
    Imprimir.Enabled = False
    Order = "Nombre"
    
    Desde.SetFocus
End Sub

Private Sub Tipo_Cliente_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Tipo_Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub
