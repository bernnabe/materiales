VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "ReciboCheque"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Private mintNroCheque As Long
Private mstrTitular As String
Private mbitNuestraFirma As Boolean
Private mdatFechaEmision As Date
Private mdatFechaCobranza As Date
Private mdblImporte As Double
Private mintBanco As Long
Private mstrBancoNombre As String

Public Property Get NroCheque() As Long
    NroCheque = mintNroCheque
End Property

Public Property Let NroCheque(vData As Long)
    mintNroCheque = vData
End Property

Public Property Get Titular() As String
    Titular = mstrTitular
End Property

Public Property Let Titular(vData As String)
    mstrTitular = vData
End Property

Public Property Get NuestraFirma() As Boolean
    NuestraFirma = mbitNuestraFirma
End Property

Public Property Let NuestraFirma(vData As Boolean)
    mbitNuestraFirma = vData
End Property

Public Property Get FechaEmision() As Date
    FechaEmision = mdatFechaEmision
End Property

Public Property Let FechaEmision(vData As Date)
    mdatFechaEmision = vData
End Property

Public Property Get FechaCobranza() As Date
    FechaCobranza = mdatFechaCobranza
End Property

Public Property Let FechaCobranza(vData As Date)
     mdatFechaCobranza = vData
End Property

Public Property Get Importe() As Double
    Importe = mdblImporte
End Property

Public Property Let Importe(vData As Double)
    mdblImporte = vData
End Property

Public Property Get Banco() As Long
    Banco = mintBanco
End Property

Public Property Let Banco(vData As Long)
    mintBanco = vData
End Property

Public Property Get BancoNombre() As String
    BancoNombre = mstrBancoNombre
End Property

Public Property Let BancoNombre(vData As String)
    mstrBancoNombre = vData
End Property
