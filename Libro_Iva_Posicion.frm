VERSION 5.00
Begin VB.Form Libro_Iva_Posicion 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Posici�n Frente al IVA.-"
   ClientHeight    =   3735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5775
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3735
   ScaleWidth      =   5775
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   2775
      Left            =   0
      TabIndex        =   2
      Top             =   0
      Width           =   5775
      Begin VB.TextBox Dif 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         MaxLength       =   13
         TabIndex        =   7
         Top             =   2040
         Width           =   1695
      End
      Begin VB.ComboBox Periodo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   0
         Top             =   240
         Width           =   4215
      End
      Begin VB.TextBox Credito 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         TabIndex        =   6
         Top             =   1440
         Width           =   1695
      End
      Begin VB.TextBox Debito 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         TabIndex        =   5
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "SALDO:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   10
         Top             =   2040
         Width           =   3375
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Total Cr�dito Fiscal I.V.A. del Per�odo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   9
         Top             =   1440
         Width           =   3375
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Total D�bito Fiscal I.V.A. del Per�do:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   1080
         Width           =   3495
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Per�odo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   0
      TabIndex        =   3
      Top             =   2760
      Width           =   5775
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   4560
         Picture         =   "Libro_Iva_Posicion.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Salir.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Libro_Iva_Posicion"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    
    For i = 1 To 12
        Periodo.AddItem Mes_Letra(i)
    Next
    
    Periodo.ListIndex = Val(Mid(Fecha_Fiscal, 4, 2)) - 1
End Sub

Private Sub Periodo_Click()
    Periodo_LostFocus
End Sub

Private Sub Periodo_GotFocus()
    Debito.Text = Formateado(Str(Val(0)), 2, 13, " ", True)
    Credito.Text = Formateado(Str(Val(0)), 2, 13, " ", True)
    Dif.Text = Formateado(Str(Val(0)), 2, 13, " ", False)
End Sub

Private Sub Periodo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Periodo_LostFocus()
    If Val(Periodo.Text) > 0 Then
        qy = "SELECT SUM(Iva) FROM Factura_Venta WHERE Mes_Libro = " & Trim(Val(Mid(Periodo.Text, 1, 2))) & " "
        qy = qy & "AND A�o_Libro = " & Trim(Str(Val(Libro_Iva.A�o.Text))) & " "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        AbreRs
        
        If IsNull(Rs.Fields(0)) = False Then
            Debito.Text = Rs.Fields(0)
        Else
            Debito = 0
        End If
        
        qy = "SELECT SUM(Iva) FROM Factura_Compra WHERE Mes_Libro = " & Trim(Val(Mid(Periodo.Text, 1, 2))) & " "
        qy = qy & "AND A�o_Libro = " & Trim(Str(Val(Libro_Iva.A�o.Text))) & " "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        AbreRs
        
        If IsNull(Rs.Fields(0)) = False Then
            Credito.Text = Trim(Str(Val(Rs.Fields(0))))
        Else
            Credito = 0
        End If
        
        Dif.Text = Val(Debito.Text) - Val(Credito.Text)
    End If
    
    Debito.Text = Formateado(Str(Val(Debito.Text)), 2, 13, " ", True)
    Credito.Text = Formateado(Str(Val(Credito.Text)), 2, 13, " ", True)
    Dif.Text = Formateado(Str(Val(Dif.Text)), 2, 13, " ", False)
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub
