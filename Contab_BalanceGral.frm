VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form Contab_BalanceGral 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Balance de Sumas y Saldos.-"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   11655
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Cuenta 
      Height          =   1095
      Left            =   0
      TabIndex        =   12
      Top             =   0
      Width           =   11655
      Begin VB.ComboBox Desde_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   33
         Top             =   240
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.ComboBox Hasta_Encontrada 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   32
         Top             =   600
         Visible         =   0   'False
         Width           =   7095
      End
      Begin VB.CommandButton Busca_Desde 
         Caption         =   "&Desde Cta.:"
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Busca_Hasta 
         Caption         =   "&Hasta Cta.:"
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   600
         Width           =   1095
      End
      Begin VB.ComboBox Periodo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9240
         TabIndex        =   8
         Top             =   600
         Width           =   2295
      End
      Begin VB.ComboBox Ejercicio 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   9240
         TabIndex        =   7
         Top             =   240
         Width           =   2295
      End
      Begin VB.TextBox Desde_Nivel_1 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   0
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_2 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   19
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_3 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   18
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_4 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   17
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_5 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         TabIndex        =   16
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_6 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         TabIndex        =   15
         Top             =   240
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_1 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   1
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_2 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   2
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_3 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   3
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_4 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   4
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_5 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         TabIndex        =   5
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_6 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         TabIndex        =   6
         Top             =   600
         Width           =   615
      End
      Begin VB.TextBox Nombre_Desde 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4920
         TabIndex        =   14
         Top             =   240
         Width           =   3495
      End
      Begin VB.TextBox Nombre_Hasta 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4920
         MaxLength       =   30
         TabIndex        =   13
         Top             =   600
         Width           =   3495
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Per�odo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8400
         TabIndex        =   29
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Ejercicio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8280
         TabIndex        =   28
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde Cta.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   21
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta Cta.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   20
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Balance 
      Enabled         =   0   'False
      Height          =   5055
      Left            =   0
      TabIndex        =   22
      Top             =   1080
      Width           =   11655
      Begin VB.Frame Marco_Progreso 
         Height          =   1695
         Left            =   2880
         TabIndex        =   24
         Top             =   1680
         Visible         =   0   'False
         Width           =   6135
         Begin MSComctlLib.ProgressBar Progreso 
            Height          =   255
            Left            =   120
            TabIndex        =   25
            Top             =   1320
            Width           =   5895
            _ExtentX        =   10398
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
         End
         Begin VB.Label Label4 
            Caption         =   "Cargando Cuentas y Resultados, Aguarde un momento por favor..."
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   27
            Top             =   240
            Width           =   5895
         End
         Begin VB.Label Label3 
            Caption         =   "0%                                                      Progreso                                                 100%"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   26
            Top             =   1080
            Width           =   5895
         End
      End
      Begin MSFlexGridLib.MSFlexGrid Lista 
         Height          =   4695
         Left            =   120
         TabIndex        =   34
         Top             =   240
         Width           =   11415
         _ExtentX        =   20135
         _ExtentY        =   8281
         _Version        =   393216
         Cols            =   6
         FixedCols       =   0
         AllowBigSelection=   0   'False
         FocusRect       =   0
         HighLight       =   2
         FillStyle       =   1
         ScrollBars      =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   23
      Top             =   6120
      Width           =   11655
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8760
         Picture         =   "Contab_BalanceGral.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   35
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10440
         Picture         =   "Contab_BalanceGral.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   7680
         Picture         =   "Contab_BalanceGral.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Imprimir el Balance.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Contab_BalanceGral.frx":C80E
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Confirma la Carga del Balance.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Contab_BalanceGral"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Busca_Desde_Click()
    Desde_Encontradas.Visible = True
    MousePointer = 11
    If Desde_Encontradas.ListCount = 0 Then
        qy = "SELECT * FROM Cuenta "
        qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Desde_Encontradas.AddItem Trim(Rs.Fields("Denominacion")) + Space(35 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Nivel_1"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_2"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_3"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_4"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_5"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_6"))), 0, 4, " ", False)
            
            Rs.MoveNext
        Wend
    End If
    
    MousePointer = 0
    Desde_Encontradas.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Busca_Hasta_Click()
    Hasta_Encontrada.Visible = True
    MousePointer = 11
    If Hasta_Encontrada.ListCount = 0 Then
        qy = "SELECT * FROM Cuenta "
        qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Hasta_Encontrada.AddItem Trim(Rs.Fields("Denominacion")) + Space(35 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Nivel_1"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_2"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_3"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_4"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_5"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_6"))), 0, 4, " ", False)
            
            Rs.MoveNext
        Wend
    End If
    
    MousePointer = 0
    Hasta_Encontrada.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Confirma_Click()
    Dim i                As Long
    Dim Sum              As String
    
    Dim Saldo_Ejercicio  As String
    Dim Saldo_Periodo    As String
    
    Dim Total_Cred       As Single
    Dim Total_Debi       As Single
    
    Dim Debitos          As Single
    Dim Creditos         As Single
    
    Dim Total_Per        As Single
    Dim Total_Ejer       As Single
    
    Dim Rec_Saldos       As New ADODB.Recordset
    
    MousePointer = 11
    Armar_Lista
    'List2.Clear
    Marco_Progreso.Visible = True
    Marco_Progreso.Refresh
    Progreso.Value = 0
    Sum = 0
    
    'Qy = "SELECT Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Cuenta.Denominacion, Saldo_Inicial.Saldo_Inicial, SUM(Debe) AS Debitos, SUM(Haber) AS Creditos "
    qy = "SELECT Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Cuenta.Denominacion, Saldo_Inicial.Saldo_Inicial, SUM(Debe) AS Debitos, SUM(Haber) AS Creditos "
    qy = qy & "FROM Asiento, Asiento_Item, Cuenta, Saldo_Inicial "
    qy = qy & "WHERE Asiento_Item.Nivel_1 = Cuenta.Id_Nivel_1 "
    qy = qy & "AND Cuenta.Id_Nivel_1 = Saldo_Inicial.Id_Nivel_1 "
    qy = qy & "AND Asiento_Item.Nivel_2 = Cuenta.Id_Nivel_2 "
    qy = qy & "AND Cuenta.Id_Nivel_2 = Saldo_Inicial.Id_Nivel_2 "
    qy = qy & "AND Asiento_Item.Nivel_3 = Cuenta.Id_Nivel_3 "
    qy = qy & "AND Cuenta.Id_Nivel_3 = Saldo_Inicial.Id_Nivel_3 "
    qy = qy & "AND Asiento_Item.Nivel_4 = Cuenta.Id_Nivel_4 "
    qy = qy & "AND Cuenta.Id_Nivel_4 = Saldo_Inicial.Id_Nivel_4 "
    qy = qy & "AND Asiento_Item.Nivel_5 = Cuenta.Id_Nivel_5 "
    qy = qy & "AND Cuenta.Id_Nivel_5 = Saldo_Inicial.Id_Nivel_5 "
    qy = qy & "AND Asiento_Item.Nivel_6 = Cuenta.Id_Nivel_6 "
    qy = qy & "AND Cuenta.Id_Nivel_6 = Saldo_Inicial.Id_Nivel_6 "
    If Trim(Desde_Nivel_1.Text) <> "*" Then
        qy = qy & "AND Cuenta.Id_Nivel_1 BETWEEN " & Val(Desde_Nivel_1.Text) & " AND " & Val(Hasta_Nivel_1.Text) & " "
        If Val(Hasta_Nivel_2.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_2 = " & Trim(Str(Val(Hasta_Nivel_2.Text))) & " "
        If Val(Hasta_Nivel_3.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_3 = " & Trim(Str(Val(Hasta_Nivel_3.Text))) & " "
        If Val(Hasta_Nivel_4.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_4 = " & Trim(Str(Val(Hasta_Nivel_4.Text))) & " "
        If Val(Hasta_Nivel_5.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_5 = " & Trim(Str(Val(Hasta_Nivel_5.Text))) & " "
        If Val(Hasta_Nivel_6.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_6 = " & Trim(Str(Val(Hasta_Nivel_6.Text))) & " "
    End If
    qy = qy & "AND Asiento.Id_Asiento = Asiento_Item.Id_Asiento "
    'If Val(Periodo.Text) > 0 Then Qy = Qy & "AND MID(Fecha, 4, 2) = " & trim(str(Val(Periodo.Text))) & " "
    qy = qy & "AND Saldo_Inicial.Ejercicio = " & Val(Mid(Ejercicio.Text, 1, 4)) & " "
    qy = qy & "AND Cuenta.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "AND Cuenta.Id_Empresa = Asiento_Item.Id_Empresa "
    qy = qy & "AND Asiento_Item.Id_Empresa = Saldo_Inicial.Id_Empresa "
    qy = qy & "GROUP BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Cuenta.Denominacion, Saldo_Inicial.Saldo_Inicial "
    qy = qy & "ORDER BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Cuenta.Denominacion"
    AbreRs
    
    If Not Rs.RecordCount = 0 Then Sum = 100 / Val(Rs.RecordCount)
    
    While Not Rs.EOF
        Saldo_Ejercicio = 0
        Saldo_Periodo = 0
        Total_Debi = 0
        Total_Cred = 0
        
        Txt = Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")) & Chr(9)
        Txt = Txt & Trim(Rs.Fields("Denominacion")) & Chr(9)
        
        For i = 1 To Trim(Str(Val(Mid(Periodo.Text, 1, 2))))
            Saldo_Periodo = 0
            Total_Debi = 0
            Total_Cred = 0
            
            If (Rec_Saldos.State = 1) Then
                Rec_Saldos.Close
            End If
            
            qy = "SELECT SUM(Debe) AS Total_Debitos, SUM(Haber) AS Total_Creditos, SUM(Debe - Haber) As Saldo_Periodo FROM Asiento, Asiento_Item "
            qy = qy & "WHERE MONTH(Fecha) = " & Val(i) & " "
            qy = qy & "AND Asiento.Id_Asiento = Asiento_Item.Id_Asiento "
            qy = qy & "AND Asiento_Item.Id_Ejercicio = " & Trim(Str(Val(Mid(Ejercicio.Text, 1, 4)))) & " "
            qy = qy & "AND Nivel_1 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_1")))) & " "
            qy = qy & "AND Nivel_2 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_2")))) & " "
            qy = qy & "AND Nivel_3 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_3")))) & " "
            qy = qy & "AND Nivel_4 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_4")))) & " "
            qy = qy & "AND Nivel_5 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_5")))) & " "
            qy = qy & "AND Nivel_6 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_6")))) & " "
            qy = qy & "AND Asiento.Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
            
            Rec_Saldos.Open qy, Db
            
            If IsNull(Rec_Saldos.Fields("Total_Debitos")) = False Then Total_Debi = Rec_Saldos.Fields("Total_Debitos")
            If IsNull(Rec_Saldos.Fields("Total_Creditos")) = False Then Total_Cred = Rec_Saldos.Fields("Total_Creditos")
            Saldo_Periodo = Val(Total_Debi) - Val(Total_Cred)
            
            Saldo_Ejercicio = Val(Saldo_Ejercicio) + Val(Saldo_Periodo)
        Next
        
        Saldo_Ejercicio = Val(Saldo_Ejercicio) + Val(Rs.Fields("Saldo_Inicial"))
        Debitos = Val(Debitos) + Val(Total_Debi)
        Creditos = Val(Creditos) + Val(Total_Cred)
        Total_Per = Val(Total_Per) + Val(Saldo_Periodo)
        Total_Ejer = Val(Total_Ejer) + Val(Saldo_Ejercicio)
        
        Txt = Txt & Formateado(Str(Val(Total_Debi)), 2, 13, " ", True) & Chr(9)
        Txt = Txt & Formateado(Str(Val(Total_Cred)), 2, 13, " ", True) & Chr(9)
        
        If Val(Saldo_Periodo) < 0 Then
            Saldo_Periodo = Formateado(Str(Val(Saldo_Periodo) * -1), 2, 14, " ", True)
            Txt = Txt & Space(14 - Len("(" & Trim(Saldo_Periodo) & ")")) & "(" & Trim(Saldo_Periodo) & ")" & Chr(9)
        Else
            Txt = Txt & Formateado(Str(Val(Saldo_Periodo)), 2, 13, " ", True) & Chr(9)
        End If
        
        If Val(Saldo_Ejercicio) < 0 Then
            Saldo_Ejercicio = Formateado(Str(Val(Saldo_Ejercicio) * -1), 2, 14, " ", True)
            Txt = Txt & Space(14 - Len("(" & Trim(Saldo_Ejercicio) & ")")) & "(" & Trim(Saldo_Ejercicio) & ")"
        Else
            Txt = Txt & Formateado(Str(Val(Saldo_Ejercicio)), 2, 13, " ", True)
        End If
        
        Lista.AddItem Txt
        
        If Not Val(Progreso.Value) + Val(Sum) > 100 Then
            Progreso.Value = Val(Progreso.Value) + Val(Sum)
        Else
            Progreso.Value = 100
        End If
        
        Rs.MoveNext
    Wend
    
    Lista.AddItem ""
    Lista.AddItem Chr(9) & "TOTALES: " & Chr(9) & Formateado(Str(Val(Debitos)), 2, 14, " ", True) & Chr(9) & Formateado(Str(Val(Creditos)), 2, 14, " ", True) & Chr(9) & Formateado(Str(Val(Total_Per)), 2, 14, " ", True) & Chr(9) & Formateado(Str(Val(Total_Ejer)), 2, 14, " ", True)
    
    'List2.AddItem "TOTALES: " & space(38) & Formateado(str(Val(Debitos)), 2, 14, " ", True) & " " & Formateado(str(Val(Creditos)), 2, 14, " ", True) & " " & Formateado(str(Val(Total_Per)), 2, 14, " ", True) & " " & Formateado(str(Val(Total_Ejer)), 2, 14, " ", True)
    
    Marco_Progreso.Visible = False
    MousePointer = 0
    Marco_Balance.Enabled = True
    If Imprimir.Enabled = True Then
        Imprimir.SetFocus
    Else
        Salir.SetFocus
    End If
End Sub

Private Sub Desde_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Desde_Encontradas_LostFocus
End Sub

Private Sub Desde_Encontradas_LostFocus()
    Desde_Nivel_1.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 37, 4))
    Desde_Nivel_2.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 42, 4))
    Desde_Nivel_3.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 47, 4))
    Desde_Nivel_4.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 52, 4))
    Desde_Nivel_5.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 57, 4))
    Desde_Nivel_6.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 62, 4))
    
    Desde_Encontradas.Visible = False
    Leer_Cuenta_Inicial
End Sub

Private Sub Desde_Nivel_1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_Nivel_1_LostFocus()
    If Val(Desde_Nivel_1.Text) > 0 Then
        Leer_Cuenta_Inicial
    ElseIf Desde_Nivel_1.Text = "*" Then
        Nombre_Desde.Text = "CUENTA INICIAL"
    End If
End Sub

Private Sub Leer_Cuenta_Inicial()
    If Val(Desde_Nivel_1.Text) > 0 Then
        
        qy = "SELECT * FROM Cuenta WHERE "
        qy = qy & "Id_Nivel_1 = " & Trim(Str(Val(Desde_Nivel_1.Text))) & " "
        qy = qy & "AND Id_Nivel_2 = " & Trim(Str(Val(Desde_Nivel_2.Text))) & " "
        qy = qy & "AND Id_Nivel_3 = " & Trim(Str(Val(Desde_Nivel_3.Text))) & " "
        qy = qy & "AND Id_Nivel_4 = " & Trim(Str(Val(Desde_Nivel_4.Text))) & " "
        qy = qy & "AND Id_Nivel_5 = " & Trim(Str(Val(Desde_Nivel_5.Text))) & " "
        qy = qy & "AND Id_Nivel_6 = " & Trim(Str(Val(Desde_Nivel_6.Text))) & " "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        AbreRs
        
        If Not Rs.EOF Then
            Nombre_Desde.Text = Rs.Fields("Denominacion")
            Hasta_Nivel_1.SetFocus
        Else
            MsgBox "La cuenta es inexistente...", vbInformation, "Atenci�n.!"
            Borrar_Campo
            Desde_Nivel_1.SetFocus
        End If
    Else
        Desde_Nivel_1.SetFocus
    End If
End Sub

Private Sub Borrar_Campo()
    Armar_Lista
    Periodo.Text = ""
    Ejercicio.Text = ""
    Desde_Nivel_1.Text = ""
    Desde_Nivel_2.Text = ""
    Desde_Nivel_3.Text = ""
    Desde_Nivel_4.Text = ""
    Desde_Nivel_5.Text = ""
    Desde_Nivel_6.Text = ""
    Nombre_Desde.Text = ""
    Hasta_Nivel_1.Text = ""
    Hasta_Nivel_2.Text = ""
    Hasta_Nivel_3.Text = ""
    Hasta_Nivel_4.Text = ""
    Hasta_Nivel_5.Text = ""
    Hasta_Nivel_6.Text = ""
    Nombre_Hasta.Text = ""
    Marco_Balance.Enabled = False
    Imprimir.Enabled = False
    
    Desde_Nivel_1.SetFocus
End Sub

Private Sub Ejercicio_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Ejercicio_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Balance de Sumas y Saldos.-"
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 9
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Armar_Lista
    
    Cargar_Ejercicio
    
    For i = 1 To 12
        Periodo.AddItem Mes_Letra(i)
    Next
End Sub

Private Sub Cargar_Ejercicio()
    Ejercicio.Clear
    
    qy = "SELECT * FROM Ejercicio WHERE Id_Empresa = " & Val(Id_Empresa) & " "
    qy = qy & "ORDER BY Periodo DESC"
    AbreRs
    
    While Not Rs.EOF
        Ejercicio.AddItem Trim(Str(Val(Rs.Fields("Id_Ejercicio")))) + Space(4 - Len(Trim(Str(Val(Rs.Fields("Id_Ejercicio")))))) & " " & Rs.Fields("Periodo")
        Rs.MoveNext
    Wend
End Sub

Private Sub Armar_Lista()
    Lista.Clear
    
    Lista.Cols = 6
    Lista.ColWidth(0) = 2000
    Lista.ColWidth(1) = 4200
    Lista.ColWidth(2) = 1200
    Lista.ColWidth(3) = 1200
    Lista.ColWidth(4) = 1200
    Lista.ColWidth(5) = 1200
    Lista.ColAlignment(2) = 6
    Lista.ColAlignment(3) = 6
    Lista.ColAlignment(4) = 6
    Lista.ColAlignment(5) = 6
    
    Lista.Rows = 1
    Lista.Col = 0
    Lista.Row = 0
    
    Lista.Text = "C�digo"
    Lista.Col = 1
    Lista.Text = "Cuenta"
    Lista.Col = 2
    Lista.Text = "D�bitos"
    Lista.Col = 3
    Lista.Text = "Cr�ditos"
    Lista.Col = 4
    Lista.Text = "Saldo Per�odo"
    Lista.Col = 5
    Lista.Text = "Saldo Ejercicio"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Hasta_Encontrada_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Hasta_Encontrada_LostFocus
End Sub

Private Sub Hasta_Encontrada_LostFocus()
    Hasta_Nivel_1.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 37, 4))
    Hasta_Nivel_2.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 42, 4))
    Hasta_Nivel_3.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 47, 4))
    Hasta_Nivel_4.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 52, 4))
    Hasta_Nivel_5.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 57, 4))
    Hasta_Nivel_6.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 62, 4))
    
    Hasta_Encontrada.Visible = False
    
    Hasta_Nivel_6_LostFocus
    Ejercicio.SetFocus
End Sub

Private Sub Hasta_Nivel_1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_1_LostFocus()
    If Hasta_Nivel_1.Text = "*" Then
        Nombre_Hasta.Text = "CUENTA FINAL"
        Ejercicio.SetFocus
    End If
End Sub

Private Sub Hasta_Nivel_2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_4_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_5_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Periodo_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
    
    If Val(Periodo.Text) = 0 Then Periodo.Text = Mes_Letra(Val(Mid(Fecha_Fiscal, 4, 2)))
    
    Periodo.SelStart = 0
    Periodo.SelText = ""
    Periodo.SelLength = Len(Periodo.Text)
End Sub

Private Sub Periodo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_6_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_6_LostFocus()
    If Val(Hasta_Nivel_1.Text) > 0 Then
        Leer_Cuenta_Final
    End If
End Sub

Private Sub Leer_Cuenta_Final()
    If Val(Hasta_Nivel_1.Text) > 0 Then
        
        qy = "SELECT * FROM Cuenta WHERE "
        qy = qy & "Id_Nivel_1 = " & Trim(Str(Val(Hasta_Nivel_1.Text))) & " "
        qy = qy & "AND Id_Nivel_2 = " & Trim(Str(Val(Hasta_Nivel_2.Text))) & " "
        qy = qy & "AND Id_Nivel_3 = " & Trim(Str(Val(Hasta_Nivel_3.Text))) & " "
        qy = qy & "AND Id_Nivel_4 = " & Trim(Str(Val(Hasta_Nivel_4.Text))) & " "
        qy = qy & "AND Id_Nivel_5 = " & Trim(Str(Val(Hasta_Nivel_5.Text))) & " "
        qy = qy & "AND Id_Nivel_6 = " & Trim(Str(Val(Hasta_Nivel_6.Text))) & " "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        AbreRs
        
        If Not Rs.EOF Then
            Nombre_Hasta.Text = Rs.Fields("Denominacion")
            Ejercicio.SetFocus
        Else
            MsgBox "La cuenta es inexistente...", vbInformation, "Atenci�n.!"
            Borrar_Campo
        End If
    Else
        Hasta_Nivel_1.SetFocus
    End If
End Sub

Private Sub Salir_Click()
    If Desde_Nivel_1.Text = "" And Hasta_Nivel_1.Text = "" And Periodo.Text = "" And Ejercicio.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub
