VERSION 5.00
Begin VB.Form Lst_Presu 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Lista de Presupuestos Emitidos.-"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11295
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   7095
   ScaleWidth      =   11295
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Periodo 
      Height          =   975
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   11295
      Begin VB.TextBox Desde 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9840
         MaxLength       =   10
         TabIndex        =   0
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9840
         MaxLength       =   10
         TabIndex        =   1
         Top             =   600
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8880
         TabIndex        =   8
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8880
         TabIndex        =   7
         Top             =   600
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Libro 
      Enabled         =   0   'False
      Height          =   5175
      Left            =   0
      TabIndex        =   9
      Top             =   960
      Width           =   11295
      Begin VB.ListBox List2 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   240
         Left            =   120
         TabIndex        =   10
         Top             =   4800
         Width           =   11055
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   6.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4380
         Left            =   120
         TabIndex        =   3
         ToolTipText     =   "Haciendo un Doble Click sobre un Item de la lista puede ver el contenido del comprobante.-"
         Top             =   360
         Width           =   11055
      End
      Begin VB.Label Encabezado_Ventas 
         Caption         =   $"Lst_Presu.frx":0000
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   120
         Width           =   10815
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   12
      Top             =   6120
      Width           =   11295
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8640
         Picture         =   "Lst_Presu.frx":00BE
         Style           =   1  'Graphical
         TabIndex        =   13
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10080
         Picture         =   "Lst_Presu.frx":0A30
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Cancelar - Salir"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7560
         Picture         =   "Lst_Presu.frx":6CBA
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Imprimir Libro de IVA.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Lst_Presu.frx":C8CC
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Confirma la Carga del Sub Diario.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Lst_Presu"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Confirma_Click()
    If Val(Desde.Text) > 0 And Val(Hasta.Text) > 0 Then
        
        Dim Gravado   As String
        Dim Exento    As String
        Dim Iva       As String
        Dim Perc      As String
        Dim Ret_Iva   As String
        Dim Total     As String
        
        List1.Clear
        List2.Clear
        Marco_Libro.Enabled = True
        
        qy = "SELECT * FROM Cliente, Presupuesto "
        qy = qy & "WHERE (Fecha BETWEEN CONVERT(DATETIME, '" & Format(Desde.Text, "YYYY-MM-DD") & " 00:00:00', 102) AND CONVERT(DATETIME, '" & Format(Hasta.Text, "YYYY-MM-DD") & " 23:59:59.99', 102))"
        qy = qy & "AND Presupuesto.Cliente = Cliente.Id_Cliente "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Fecha"
        AbreRs
        
        
        While Not Rs.EOF
            Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Nro_Presupuesto"))), 0, 10, "0", False) + Space(20 - Len(Formateado(Str(Val(Rs.Fields("Id_Nro_Presupuesto"))), 0, 10, "0", False))) & " "
            Txt = Txt & Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " "
            Txt = Txt & Trim(Rs.Fields("Condicion_IVA")) + Space(2 - Len(Trim(Rs.Fields("Condicion_Iva")))) & " "
            Txt = Txt & Trim(Rs.Fields("Nro")) + Space(13 - Len(Trim(Rs.Fields("Nro")))) & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Gravado"))), 2, 10, " ", False) & " ": Gravado = Val(Gravado) + Val(Rs.Fields("Gravado"))
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Exento"))), 2, 10, " ", False) & " ": Exento = Val(Exento) + Val(Rs.Fields("Exento"))
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Iva"))), 2, 10, " ", False) & " ": Iva = Val(Iva) + Val(Rs.Fields("Iva"))
            Txt = Txt & Formateado(Str(Val(0)), 2, 10, " ", False) & " ": Perc = 0
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Ret_Iva"))), 2, 10, " ", False) & " ": Ret_Iva = Val(Ret_Iva) + Val(Rs.Fields("Ret_Iva"))
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Total_Factura"))), 2, 10, " ", False): Total = Val(Total) + Val(Rs.Fields("Total_Factura"))
            
            List1.AddItem Txt
            Rs.MoveNext
        Wend
        
        Imprimir.Enabled = True
        List2.AddItem "TOTALES: " & Space(69) & Formateado(Str(Val(Gravado)), 2, 10, " ", False) & " " & Formateado(Str(Val(Exento)), 2, 10, " ", False) & " " & Formateado(Str(Val(Iva)), 2, 10, " ", False) & " " & Formateado(Str(Val(Perc)), 2, 10, " ", False) & " " & Formateado(Str(Val(Ret_Iva)), 2, 10, " ", False) & " " & Formateado(Str(Val(Total)), 2, 10, " ", False)
        List1.SetFocus
    Else
        MsgBox "Error en las fechas ingresadas, verifique e ingrese nuevamente.", vbInformation, "Atenciσn.!"
        Desde.Text = ""
        Hasta.Text = ""
        Desde.SetFocus
    End If
End Sub

Private Sub Desde_GotFocus()
    If Desde.Text = "error" Or Trim(Desde.Text) = "" Then Desde.Text = Fecha_Fiscal
    Desde.Text = Trim(Desde.Text)
    Desde.SelStart = 0
    Desde.SelText = ""
    Desde.SelLength = Len(Desde.Text)
End Sub

Private Sub Desde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_LostFocus()
    Desde.Text = ValidarFecha(Desde.Text)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Listado de Presupuestos.-"
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 12
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Hasta_GotFocus()
    If Hasta.Text = "error" Or Trim(Hasta.Text) = "" Then Hasta.Text = Fecha_Fiscal
    Hasta.Text = Trim(Hasta.Text)
    Hasta.SelStart = 0
    Hasta.SelText = ""
    Hasta.SelLength = Len(Hasta.Text)
End Sub

Private Sub Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_LostFocus()
    Hasta.Text = ValidarFecha(Hasta.Text)
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
    
    If List1.ListCount > 0 Then
        
        i = 0
        l = i
        
        Printer.Font = "Courier New"
        Printer.FontSize = 7
        
        Imprimir_Encabezado
        
        For i = 0 To List1.ListCount - 1
            If l <= 74 Then
                Printer.Print " " & Mid(List1.List(i), 1, 250)
                l = l + 1
            Else
                Printer.NewPage
                Imprimir_Encabezado
                l = 0
            End If
        Next
        
        Printer.Print " "
        Printer.Print " "; List2.List(0)
        Printer.EndDoc
    End If
    
    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Imprimir.Tag = Int(List1.ListCount / 74) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                                                      Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                                                     Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                                                               Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " E-Mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print "                                                   LISTADO DE PRESUPUESTOS"
    Printer.Print
    Printer.Print "Desde:" & Trim(Desde.Text)
    Printer.Print "Hasta:" & Trim(Hasta.Text)
    Printer.Print " "
    Printer.Font = "MS Sans Serif"
    Printer.FontSize = 8.5
    Printer.Print "      Fecha  Comprobante      Nombre                                       Iva               Cuit      Gravado        Exento            Iva     Perc.NC       Ret.Iva         Total"
    Printer.Font = "Courier New"
    Printer.FontSize = 7
    Printer.Print " "
End Sub

Private Sub List1_DblClick()
'    If trim(mid(List1.List(List1.ListIndex), 10, 2)) = "FC" Or trim(mid(List1.List(List1.ListIndex), 10, 2)) = "NC" Or trim(mid(List1.List(List1.ListIndex), 10, 2)) = "ND" Then
'        Detalle_Factura.Comprobante.Text = trim(mid(List1.List(List1.ListIndex), 10, 2)) & " "
'        Detalle_Factura.Comprobante.Text = Detalle_Factura.Comprobante.Text & trim(mid(List1.List(List1.ListIndex), 13, 1)) & " "
'        Detalle_Factura.Comprobante.Text = Detalle_Factura.Comprobante.Text & trim(mid(List1.List(List1.ListIndex), 15, 15)) & " "
'
'        Detalle_Factura.Importe.Text = mid(List1.List(List1.ListIndex), 134, 10) & " "
'        Detalle_Factura.Codigo.Text = "00000"
'        Detalle_Factura.Codigo.Tag = IIf(Compras.Value = True, "Proveedor", "Cliente")
'        Detalle_Factura.Nombre.Text = mid(List1.List(List1.ListIndex), 31, 25)
'    End If
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: List1_DblClick
End Sub

Private Sub Salir_Click()
    If Desde.Text = "" And Hasta.Text = "" And List1.ListCount = 0 Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Desde.SetFocus

    Desde.Text = ""
    Hasta.Text = ""
    List1.Clear
    List2.Clear
    Marco_Libro.Enabled = False
    Imprimir.Enabled = False
End Sub
