VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsArticulo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
Option Explicit

Public Function GetPrecioVentaPorTipoCliente(idRubro As Long, idArticulo As Long, tipoCliente As String, Plazo As Integer) As Double
    
    Dim dblPrecioVenta As Double
    Dim strSql As String
    Dim rstDatos As New ADODB.Recordset
    
    strSql = "SELECT * FROM Articulo WHERE Id_Articulo = " & idArticulo & " "
    strSql = strSql & "AND Id_Rubro = " & idRubro
    rstDatos.Open strSql, Db
    
    If tipoCliente = "P" Then
        If Val(Plazo) = 0 Then
            dblPrecioVenta = ((Val(rstDatos("Precio_Compra")) * Val(rstDatos("Margen_Pub"))) / 100) + Val(rstDatos("Precio_Compra"))
        Else
            dblPrecioVenta = ((Val(rstDatos("Precio_Compra")) * Val(rstDatos("Margen_Pub"))) / 100) + Val(rstDatos("Precio_Compra"))
            dblPrecioVenta = ((Val(dblPrecioVenta) * Val(rstDatos("Rec_CtaCte_Pub"))) / 100) + Val(dblPrecioVenta)
        End If
    Else
        If Val(Plazo) = 0 Then
            dblPrecioVenta = ((Val(rstDatos("Precio_Compra")) * Val(rstDatos("Margen_Rev"))) / 100) + Val(rstDatos("Precio_Compra"))
        Else
            dblPrecioVenta = ((Val(rstDatos("Precio_Compra")) * Val(rstDatos("Margen_Rev"))) / 100) + Val(rstDatos("Precio_Compra"))
            dblPrecioVenta = ((Val(dblPrecioVenta) * Val(rstDatos("Rec_CtaCte_Rev"))) / 100) + Val(dblPrecioVenta)
        End If
        
    End If
    
    GetPrecioVentaPorTipoCliente = dblPrecioVenta

End Function

'/// Summary
'    Esta funci�n devuelve el precio neto del art�culo
'///
Public Function GetPrecioArticulo(idRubro As Long, idArticulo As Long, tipoCliente As String, Plazo As Integer, Espesor As Single, Ancho As Single, Largo As Single) As Double

    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String

    Dim dblPrecioVenta As Double
    Dim dblPrecioFinalUnitario As Double
    Dim dblMedida As Double
    Dim intPresentacion As Integer
    
    strSql = "SELECT * FROM Articulo WHERE Id_Articulo = " & idArticulo & " "
    strSql = strSql & "AND Id_Rubro = " & idRubro
    rstDatos.Open strSql, Db
    
    If Not (rstDatos.EOF) Then
    
        '*********
        'Obtengo precio venta segun el tipo de cliente y condicion de venta
        '*********
    
        If tipoCliente = "P" Then
            intPresentacion = rstDatos("Presentacion_Pub")
            If Val(Plazo) = 0 Then
                dblPrecioVenta = ((Val(rstDatos("Precio_Compra")) * Val(rstDatos("Margen_Pub"))) / 100) + Val(rstDatos("Precio_Compra"))
            Else
                dblPrecioVenta = ((Val(rstDatos("Precio_Compra")) * Val(rstDatos("Margen_Pub"))) / 100) + Val(rstDatos("Precio_Compra"))
                dblPrecioVenta = ((Val(dblPrecioVenta) * Val(rstDatos("Rec_CtaCte_Pub"))) / 100) + Val(dblPrecioVenta)
            End If
        Else
            intPresentacion = rstDatos("Presentacion_Rev")
            If Val(Plazo) = 0 Then
                dblPrecioVenta = ((Val(rstDatos("Precio_Compra")) * Val(rstDatos("Margen_Rev"))) / 100) + Val(rstDatos("Precio_Compra"))
            Else
                dblPrecioVenta = ((Val(rstDatos("Precio_Compra")) * Val(rstDatos("Margen_Rev"))) / 100) + Val(rstDatos("Precio_Compra"))
                dblPrecioVenta = ((Val(dblPrecioVenta) * Val(rstDatos("Rec_CtaCte_Rev"))) / 100) + Val(dblPrecioVenta)
            End If
            
        End If
        
        '************************
        'Calculo medidas y precio
        '************************
        
        dblPrecioFinalUnitario = dblPrecioVenta
        
        If (intPresentacion = 3 Or intPresentacion = 4 Or intPresentacion = 5 Or intPresentacion = 8) Then
        
            If intPresentacion = 3 Then 'P2
                dblMedida = CalcMed(Espesor, Ancho, Largo, False)
            ElseIf intPresentacion = 4 Then 'M2
                dblMedida = CalcMed(Espesor, Ancho, Largo, True)
            ElseIf intPresentacion = 5 Then 'ML
                dblMedida = Largo
                dblPrecioFinalUnitario = (((Espesor * Ancho) * 0.2734) * Val(Largo)) * Val(dblPrecioVenta)
            ElseIf intPresentacion = 8 Then 'ML
                dblMedida = Largo * Ancho
            End If
            
            If intPresentacion = 3 Or intPresentacion = 4 Or intPresentacion = 8 Then
                dblPrecioFinalUnitario = dblPrecioVenta * dblMedida
            End If
            
            '****************
            'Adherir recargos
            '****************
            
            If Ancho < Val(rstDatos("Ancho_Menor")) Then
                dblPrecioFinalUnitario = ((Val(dblPrecioFinalUnitario) * Val(rstDatos("Rec_Ancho_Menor"))) / 100) + Val(dblPrecioFinalUnitario)
            End If
            If Ancho > Val(rstDatos("Ancho_Mayor")) Then
                dblPrecioFinalUnitario = ((Val(dblPrecioFinalUnitario) * Val(rstDatos("Rec_Ancho_Mayor"))) / 100) + Val(dblPrecioFinalUnitario)
            End If
            If Largo < Val(rstDatos("Largo_Menor")) Then
                dblPrecioFinalUnitario = ((Val(dblPrecioFinalUnitario) * Val(rstDatos("Rec_Largo_Menor"))) / 100) + Val(dblPrecioFinalUnitario)
            End If
            If Largo > Val(rstDatos("Largo_Mayor")) Then
                dblPrecioFinalUnitario = ((Val(dblPrecioFinalUnitario) * Val(rstDatos("Rec_Largo_Mayor"))) / 100) + Val(dblPrecioFinalUnitario)
            End If
            
        End If
        
    End If
    
    GetPrecioArticulo = dblPrecioFinalUnitario
    
    Set rstDatos = Nothing

End Function

