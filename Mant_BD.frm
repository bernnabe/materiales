VERSION 5.00
Begin VB.Form Mant_BD 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Mantenimiento de Base de Datos Automatizado.-"
   ClientHeight    =   4695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7215
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4695
   ScaleWidth      =   7215
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco 
      Height          =   3735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   7215
      Begin VB.Timer Timer2 
         Enabled         =   0   'False
         Interval        =   1
         Left            =   480
         Top             =   120
      End
      Begin VB.Timer Timer1 
         Enabled         =   0   'False
         Interval        =   600
         Left            =   0
         Top             =   120
      End
      Begin VB.Label Modo 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2640
         TabIndex        =   21
         Top             =   2880
         Width           =   4455
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "La B.D. est� abierta en modo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   20
         Top             =   2880
         Width           =   2295
      End
      Begin VB.Label Dire 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2640
         TabIndex        =   19
         Top             =   2520
         Width           =   4455
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Directorio de Trabajo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   18
         Top             =   2520
         Width           =   2295
      End
      Begin VB.Label Creada 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2640
         TabIndex        =   17
         Top             =   1800
         Width           =   2535
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Creaci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   1800
         Width           =   2295
      End
      Begin VB.Label Nombre 
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2640
         TabIndex        =   15
         Top             =   360
         Width           =   3735
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "Nombre de la Base de Datos:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   450
         TabIndex        =   14
         Top             =   360
         Width           =   2085
      End
      Begin VB.Label Mbytes 
         AutoSize        =   -1  'True
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2640
         TabIndex        =   13
         Top             =   1440
         Width           =   45
      End
      Begin VB.Label Kbytes 
         AutoSize        =   -1  'True
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2640
         TabIndex        =   12
         Top             =   1080
         Width           =   45
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Bytes:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1560
         TabIndex        =   11
         Top             =   1440
         Width           =   975
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "KBytes:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   960
         TabIndex        =   10
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label Copiando 
         Alignment       =   2  'Center
         Caption         =   "( Copiando... )"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H000000FF&
         Height          =   255
         Left            =   120
         TabIndex        =   9
         Top             =   3360
         Visible         =   0   'False
         Width           =   6975
      End
      Begin VB.Label Fecha 
         AutoSize        =   -1  'True
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2640
         TabIndex        =   5
         Top             =   2160
         Width           =   375
      End
      Begin VB.Label Tama�o 
         AutoSize        =   -1  'True
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   2640
         TabIndex        =   4
         Top             =   720
         Width           =   45
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de �ltima Modificaci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   3
         Top             =   2160
         Width           =   2415
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Tama�o en M.Bytes:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   2415
      End
   End
   Begin VB.Frame Frame2 
      Height          =   975
      Left            =   0
      TabIndex        =   1
      Top             =   3720
      Width           =   7215
      Begin VB.CommandButton Compactar 
         Height          =   615
         Left            =   1200
         Picture         =   "Mant_BD.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Compactar la Base de Datos.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Copia 
         Height          =   615
         Left            =   120
         Picture         =   "Mant_BD.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Hacer Copia de Seguridad a la Base de Datos.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   6120
         Picture         =   "Mant_BD.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Salir.-"
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "Mant_BD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Compactar_Click()
    Modo.Caption = "Exclusivo"
    Modo.Refresh
    
    On Error GoTo Errores
    Dim Retval
    Dim Objeto, Renom As Object
    
    'Timer2.Enabled = True


    'Prog.Visible = True
    'Prog.Refresh
    Marco.Refresh
    MousePointer = 11
    
    Set Objeto = CreateObject("Scripting.FileSystemObject")
    Objeto.copyfile "c:\Sistemas\Integral\Materiales\Materiales.mdb", "c:\Sistemas\Integral\Materiales\Materiales_BC.Mdb"
    
    'Db.Close
    '.CompactDatabase "c:\Sistemas\Integral\Materiales\Materiales.Mdb", "c:\Sistemas\Integral\Materiales\Materiales_Compact.mdb", , , ";pwd=uvst8189ghbc0176"
    
    Kill ("c:\Sistemas\Integral\Materiales\Materiales.Mdb")
    
    Objeto.Movefile "c:\Sistemas\Integral\Materiales\Materiales_Compact.mdb", "c:\Sistemas\Integral\Materiales\Materiales.mdb"
    Objeto.DeleteFile "c:\Sistemas\Integral\Materiales\Materiales_BC.mdb", True
    
    Abrir_Base_Datos
    Mostrar_Tama�o ("c:\Sistemas\Integral\Materiales")
    
    MousePointer = 0
    
    MsgBox "La Base de Datos se ha compactado con �xito...", vbInformation, "Atenci�n.!"
    Salir.SetFocus
    Exit Sub
    
Errores:
    If Err.Number = 3356 Then
        MsgBox "Todos los Usuario de su Red deben tener el sistema cerrado para que la operaci�n pueda concretarse.", vbInformation, "Atenci�n.!"
        Timer2.Enabled = False
        'Prog.Visible = False
    ElseIf Err.Number > 0 Then
        MsgBox "Se han producido Errores, y se ha creado una copia de seguridad de la Base de Datos con el nombre 'C:\Sistemas\Integral\Materiales\Materiales_BC.mdb' si el sistema no funciona restaure esta copia con el nombre original", vbCritical, "Atenci�n.!"
    End If
    
    MousePointer = 0
End Sub

Private Sub Copia_Click()
    On Error GoTo Sale_Mal
    
    Dim Origen, Destino
    
    
    Copiando.Visible = True
    Timer1.Enabled = True
    Copiando.Refresh

    If MsgBox("Inserte un Disco en la diquetera de 3 y 1/2 'A:' formateado y precione aceptar cuando est� listo.-", vbOKCancel + vbExclamation, "Atenci�n.!") = vbOK Then
        MousePointer = 11
        Db.Close
    
        Origen = "c:\Sistemas\Integral\Materiales\Materiales.Mdb" 'Colocar la ruta de acceso a la Base de Datos"
        Destino = "A:\Materiales.Mdb"
    
        FileCopy Origen, Destino
    
        MousePointer = 0
        
        Abrir_Base_Datos
        MsgBox "La Base de Datos se ha copiado con �xito.", vbInformation, "Atenci�n.!"
    End If
    
    Timer1.Enabled = False
    Copiando.Visible = False
    
    Exit Sub
    
Sale_Mal:
    If MsgBox("Han Ocurrido errores mientras se trataba de iniciar la copia, que desea hacer. ?", vbQuestion + vbRetryCancel, "Atenci�n.!") = vbRetry Then
        Resume
    Else
        Timer1.Enabled = False
        Copiando.Visible = False
        MousePointer = 0
    End If
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Estado y Mantenimiento de la Base de Datos del Sistema.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 3
    Me.Left = (Screen.Width - Me.Width) / 2
    
    Mostrar_Tama�o ("c:\Sistemas\Integral\Materiales\")
End Sub

Private Sub Mostrar_Tama�o(Ruta)
    Dim Objeto, Archivo, S, Mb
    Set Objeto = CreateObject("Scripting.FileSystemObject")
    Set Archivo = Objeto.GetFolder(Ruta)
    
    Mb = Formateado(Str(Val(Archivo.Size / 1048576)), 2, 0, "", True) & " MBytes"
    Kbytes = Formateado(Str(Val(Archivo.Size / 1024)), 2, 0, "", True) & " KBytes"
    Mbytes = Formateado(Str(Val(Archivo.Size)), 2, 0, "", True) & " Bytes"
    
    Tama�o.Caption = Mb
    
    'If Db.Mode = adModeShareExclusive Then
    '    Modo.Caption = "Exlusivo."
    'Else
        Modo.Caption = "Compartido."
    'End If
    
    Fecha.Caption = Archivo.DateLastModified
    Creada.Caption = Archivo.DateCreated
    Nombre.Caption = Archivo.Name
    Dire.Caption = CurDir
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub

Private Sub Timer1_Timer()
    If Copiando.Visible = False Then
        Copiando.Visible = True
        Copiando.Refresh
    Else
        Copiando.Visible = False
        Copiando.Refresh
    End If
End Sub

'Private Sub Timer2_Timer()
'    If Prog.Value < 100 And Prog.Visible = True Then
'        Prog.Value = Prog.Value + 2
'        Prog.Refresh
'    Else
'        Prog.Value = 0
'        Prog.Visible = False
'        Timer2.Enabled = False
'    End If
'
'    Mostrar_Tama�o ("c:\Sistemas\Integral\Materiales\")
'End Sub
