VERSION 5.00
Begin VB.Form Inicial 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Iniciar Sesi�n.-"
   ClientHeight    =   1695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5895
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   Moveable        =   0   'False
   ScaleHeight     =   1695
   ScaleWidth      =   5895
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Clave 
      Height          =   1695
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   5895
      Begin VB.ComboBox Empresa 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   4
         Top             =   240
         Width           =   3855
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   4920
         Picture         =   "Inicial.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Salir del Sistema.-"
         Top             =   840
         Width           =   855
      End
      Begin VB.CommandButton Confirma 
         Enabled         =   0   'False
         Height          =   615
         Left            =   4920
         Picture         =   "Inicial.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Abrir el Sistema.-"
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Clave 
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         IMEMode         =   3  'DISABLE
         Left            =   960
         PasswordChar    =   "*"
         TabIndex        =   1
         Top             =   1200
         Width           =   3855
      End
      Begin VB.ComboBox Usuario 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   960
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   840
         Width           =   3855
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Empresa:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Clave:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   1200
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Usuario:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   6
         Top             =   840
         Width           =   735
      End
   End
End
Attribute VB_Name = "Inicial"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Clave_Change()
    If Clave.Text = "" Then
        Confirma.Enabled = False
    Else
        Confirma.Enabled = True
    End If
End Sub

Private Sub Clave_GotFocus()
    Clave.SelStart = 0
    Clave.SelText = ""
    Clave.SelLength = Len(Clave.Text)
End Sub

Private Sub Clave_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    If Val(Mid(Usuario.Text, 1, 5)) > 0 And Trim(Clave.Text) <> "" Then
        qy = "SELECT * FROM Usuario WHERE Id_Usuario = " & Trim(Str(Val(Mid(Usuario.Text, 1, 5)))) & " "
        qy = qy & "AND Clave = '" & Clave.Text & "'"
        AbreRs
        
        If Rs.EOF Then
            MsgBox "Los Datos ingresados son incorrectos.-", vbInformation, "Atenci�n.!"
            Usuario.ListIndex = 0
            Clave.Text = ""
            Usuario.SetFocus
        Else
            MousePointer = 11
            Menu.Estado.Panels(4).Text = Trim(Mid(Usuario.Text, 9))
            Numero_Usuario = Val(Rs.Fields("Id_Usuario"))
            Permiso_Mod = Val(Rs.Fields("Modificacion"))
            Permiso_Alta = Val(Rs.Fields("Alta_Baja"))
            Permiso_Admin = Val(Rs.Fields("Administracion"))
            Permiso_Cons = Val(Rs.Fields("Consulta"))
            Puerto_MPto = Trim(Rs.Fields("Puerto_MPto"))
            
            Menu.Menu_Cliente.Enabled = True
            Menu.Menu_Banco.Enabled = True
            Menu.Menu_Articulo.Enabled = True
            Menu.Menu_Proveedor.Enabled = True
            Menu.Menu_Banco.Enabled = True
            Menu.Menu_Facturaci�n.Enabled = True
            Menu.menu_informa.Enabled = True
            Menu.Menu_Contan.Enabled = True
            Menu.Men_Tab_Aux.Enabled = True
            Menu.Ventanas.Enabled = True
            Menu.Salidas.Enabled = True
            Menu.Menu_Help.Enabled = True
            
            qy = "SELECT * FROM Empresa WHERE Id_Empresa = " & Trim(Str(Val(Empresa.List(Empresa.ListIndex))))
            AbreRs
            
            Id_Empresa = Trim(Rs.Fields("Id_Empresa"))
            Parametrizar
            
            MousePointer = 0
            Unload Me
            
            SendKeys "{F10}"
            SendKeys "{ENTER}"
        End If
    End If
End Sub

Private Sub Empresa_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Empresa_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cerrar()

    Id_Empresa = 0
    
    Menu.Menu_Cliente.Enabled = False
    Menu.Menu_Articulo.Enabled = False
    Menu.Menu_Proveedor.Enabled = False
    Menu.Menu_Facturaci�n.Enabled = False
    Menu.menu_informa.Enabled = False
    Menu.Menu_Contan.Enabled = False
    Menu.Men_Tab_Aux.Enabled = False
    Menu.Ventanas.Enabled = False
    Menu.Salidas.Enabled = False
    Menu.Menu_Banco.Enabled = False
    Menu.Menu_Help.Enabled = False
    
    Menu.Caption = "Sistema de Gesti�n Integral.-"
    
    Unload Msg_Empresa
    
End Sub

Private Sub Form_Load()
    Puerto_MPto = ""
    CheckRunTimeEnviropment
    Cerrar
    
    Me.Top = (Screen.Height - Me.Height) / 3
    Me.Left = (Screen.Width - Me.Width) / 2
    
    Menu.Estado.Panels(2).Text = "Identificaci�n de Usuarios.-"
    Abrir_Base_Datos
    
    CargarCombos
    
    Usuario.ListIndex = 0
    Empresa.ListIndex = 0
    
    If (RunTimeEnviropment = Development) Then
        
        Clave.Text = "1"
        Confirma.TabIndex = 0
        
    End If
    
End Sub

Private Sub CheckRunTimeEnviropment()

    If Not (RunTimeEnviropment = Prodution) Then
        
        If MsgBox("Se ha seleccionado un ambiente no productivo, si esto no ha sido seleccionado por usted contactese con el administrador." + Chr(10) + "Este ambiente no trabaja con datos verdaderos y puede no cumplir con todas las funcionalidades" + Chr(10) + "Desea continuar ?", vbYesNo + vbCritical, "ATENCI�N!!!") = vbNo Then
            
            End
            
        End If
    
    End If

End Sub
Private Sub CargarCombos()
    qy = "SELECT * FROM Usuario ORDER BY Nombre"
    AbreRs
    
    While Not Rs.EOF
        Usuario.AddItem Formateado(Str(Val(Rs.Fields("Id_Usuario"))), 0, 5, " ", False) & " - " & Trim(Rs.Fields("Nombre"))
        Rs.MoveNext
    Wend
    
    qy = "SELECT * FROM Empresa ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Empresa.AddItem Formateado(Str(Val(Rs.Fields("Id_Empresa"))), 0, 5, " ", False) & " - " & Trim(Rs.Fields("Denominacion"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Salir_Click()
    End
End Sub

Private Sub Usuario_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Usuario_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub
