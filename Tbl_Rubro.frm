VERSION 5.00
Begin VB.Form Tbl_Rubro 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Registro de Rubros de Artνculos.-"
   ClientHeight    =   3495
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6495
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   3495
   ScaleWidth      =   6495
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Datos 
      Height          =   2535
      Left            =   0
      TabIndex        =   10
      Top             =   0
      Width           =   6495
      Begin VB.TextBox Util_Pub_Cta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   5
         TabIndex        =   4
         Top             =   1560
         Width           =   735
      End
      Begin VB.TextBox Util_Rev_Cta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5640
         MaxLength       =   5
         TabIndex        =   5
         Top             =   1560
         Width           =   735
      End
      Begin VB.TextBox Util_Rev 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5640
         MaxLength       =   5
         TabIndex        =   3
         Top             =   1200
         Width           =   735
      End
      Begin VB.TextBox Fecha 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5040
         MaxLength       =   10
         TabIndex        =   16
         Top             =   2160
         Width           =   1335
      End
      Begin VB.TextBox Utilidad 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   5
         TabIndex        =   2
         Top             =   1200
         Width           =   735
      End
      Begin VB.ComboBox Tipos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   14
         Top             =   240
         Visible         =   0   'False
         Width           =   4095
      End
      Begin VB.TextBox Descripcion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   1
         Top             =   840
         Width           =   4095
      End
      Begin VB.TextBox Codigo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   4
         TabIndex        =   0
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton Buscar_Tipos 
         Caption         =   "&Cσdigo:"
         Height          =   255
         Left            =   1080
         TabIndex        =   12
         ToolTipText     =   "Buscar Rubros existentes en la base de datos.-"
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "% Margen Util. Rev. Cta. Cte.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3360
         TabIndex        =   20
         Top             =   1560
         Width           =   2175
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         AutoSize        =   -1  'True
         Caption         =   "% Margen Util. Pub. Cta. Cte.:"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   75
         TabIndex        =   19
         Top             =   1560
         Width           =   2100
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "% Margen Util. Rev. Ctdo.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3600
         TabIndex        =   18
         Top             =   1200
         Width           =   1935
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de Alta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   17
         Top             =   2160
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "% Margen Util. Pub. Ctdo.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   1200
         Width           =   1935
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Descripciσn:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   720
         TabIndex        =   13
         Top             =   840
         Width           =   1455
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   11
      Top             =   2520
      Width           =   6495
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   3960
         Picture         =   "Tbl_Rubro.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Imprimir Lista.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   5280
         Picture         =   "Tbl_Rubro.frx":5C12
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   1200
         Picture         =   "Tbl_Rubro.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Borrar.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Tbl_Rubro.frx":12126
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Grabar.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Tbl_Rubro"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Cuenta_Paginas As Single

Private Sub Borrar_Click()
    If MsgBox("Desea Borrar ιste registro. ?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        qy = "DELETE FROM Rubro WHERE Id_Rubro = " & Trim(Str(Val(Codigo.Text)))
        Db.Execute (qy)
        
        Salir_Click
    Else
        Salir.SetFocus
    End If
End Sub

Private Sub Buscar_Tipos_Click()
    Tipos_Encontrados.Visible = True
    If Tipos_Encontrados.ListCount = 0 Then
        MousePointer = 11
        
        qy = "SELECT * FROM Rubro ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Tipos_Encontrados.AddItem Trim(Rs.Fields("Denominacion")) + Space(31 - Len(Trim(Rs.Fields("Denominacion")))) + Trim(Rs.Fields("Id_Rubro"))
            Rs.MoveNext
        Wend
        MousePointer = 0
    End If
    Tipos_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Codigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Codigo_LostFocus
End Sub

Private Sub Codigo_LostFocus()
    If Val(Codigo.Text) > 0 Then
        Leer_Codigo
    Else
        qy = "SELECT MAX(Id_Rubro) FROM Rubro"
        AbreRs
        
        Codigo.Text = 1
        If Not Rs.Fields(0) = "" Then Codigo.Text = Val(Rs.Fields(0)) + 1
    End If
End Sub

Private Sub Leer_Codigo()
    If Val(Codigo.Text) > 0 Then
        qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & Trim(Str(Val(Codigo.Text)))
        AbreRs
        
        If Rs.EOF Then
            Grabar.Tag = "Alta"
            Grabar.Enabled = True
            Fecha.Text = Format(Fecha_Fiscal, "dd/mm/yyyy")
            
            Descripcion.SetFocus
        Else
            Grabar.Tag = "Modificaciσn"
            Mostrar_Tipo
        End If
    Else
        Codigo.Text = ""
        Codigo.SetFocus
    End If
End Sub

Private Sub Borrar_Campo()
    Descripcion.Text = ""
    Util_Rev.Text = ""
    Utilidad.Text = ""
    Util_Pub_Cta.Text = ""
    Util_Rev_Cta.Text = ""
    Fecha.Text = ""
    Codigo.Text = ""
    
    Grabar.Enabled = False
    Borrar.Enabled = False
End Sub

Private Sub Mostrar_Tipo()
    Descripcion.Text = Rs.Fields("Denominacion")
    Fecha.Text = Rs.Fields("Fecha_Alta")
    Utilidad.Text = Formateado(Str(Val(Rs.Fields("Margen_Util_Pub_Ctdo"))), 2, 5, " ", False)
    Util_Rev.Text = Formateado(Str(Val(Rs.Fields("Margen_Util_Rev_Ctdo"))), 2, 5, " ", False)
    Util_Rev_Cta.Text = Formateado(Str(Val(Rs.Fields("Margen_Util_Rev_Cta"))), 2, 5, " ", False)
    Util_Pub_Cta.Text = Formateado(Str(Val(Rs.Fields("Margen_Util_Pub_Cta"))), 2, 5, " ", False)
    
    Grabar.Enabled = True
    Borrar.Enabled = True
    
    Descripcion.SetFocus
End Sub

Private Sub Descripcion_GotFocus()
    Descripcion.Text = Trim(Descripcion.Text)
    
    Descripcion.SelStart = 0
    Descripcion.SelText = ""
    Descripcion.SelLength = Len(Descripcion.Text)
End Sub

Private Sub Descripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub Descripcion_LostFocus()
    Descripcion.Text = UCase(Descripcion.Text)
    Descripcion.Text = FiltroCaracter(Descripcion.Text)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualizaciσn del Registro de Rubros.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 3
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        
        qy = "INSERT INTO Rubro VALUES ('"
        qy = qy & Trim(Str(Val(Codigo.Text))) & "'"
        qy = qy & ", '" & Trim(Descripcion.Text) & "'"
        qy = qy & ", " & Trim(Str(Val(Utilidad.Text))) & " "
        qy = qy & ", " & Trim(Str(Val(Util_Pub_Cta.Text))) & " "
        qy = qy & ", " & Trim(Str(Val(Util_Rev.Text))) & " "
        qy = qy & ", " & Trim(Str(Val(Util_Rev_Cta.Text))) & " "
        qy = qy & ", '" & Trim(Fecha.Text) & "')"
        Db.Execute (qy)
    Else
        
        qy = "UPDATE Rubro SET "
        qy = qy & "Denominacion = '" & Trim(Descripcion.Text) & "', "
        qy = qy & "Margen_Util_Pub_Ctdo = " & Trim(Str(Val(Utilidad.Text))) & ", "
        qy = qy & "Margen_Util_Pub_Cta = " & Trim(Str(Val(Util_Pub_Cta.Text))) & ", "
        qy = qy & "Margen_Util_Rev_Ctdo = " & Trim(Str(Val(Util_Rev.Text))) & ", "
        qy = qy & "Margen_Util_Rev_Cta = " & Trim(Str(Val(Util_Rev_Cta.Text))) & " "
        qy = qy & "WHERE Id_Rubro = " & Trim(Str(Val(Codigo.Text)))
        Db.Execute (qy)
        
        qy = "UPDATE Articulo SET "
        qy = qy & "Margen_Pub = " & Trim(Str(Val(Utilidad.Text))) & ", "
        qy = qy & "Margen_Rev = " & Trim(Str(Val(Util_Rev.Text))) & ", "
        qy = qy & "Rec_CtaCte_Pub = " & Trim(Str(Val(Util_Pub_Cta.Text))) & ", "
        qy = qy & "Rec_CtaCte_Rev = " & Trim(Str(Val(Util_Rev_Cta.Text))) & " "
        qy = qy & "WHERE Id_Rubro = " & Trim(Str(Val(Codigo.Text))) & " "
        'Qy = Qy & "AND " Seguir !!!!!
        Db.Execute (qy)
    End If
    
    Salir_Click
End Sub

Private Sub Salir_Click()
    If Val(Codigo.Text) = 0 Then
        Unload Me
    Else
        
        Borrar_Campo
        Codigo.SetFocus
    End If
End Sub

Private Sub Tipos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Codigo.SetFocus
End Sub

Private Sub Tipos_Encontrados_LostFocus()
    Codigo.Text = Mid(Tipos_Encontrados.List(Tipos_Encontrados.ListIndex), 32)
    Tipos_Encontrados.Visible = False
End Sub

Private Sub Util_Pub_Cta_GotFocus()
    Util_Pub_Cta.SelStart = 0
    Util_Pub_Cta.SelLength = Len(Util_Pub_Cta.Text)
End Sub

Private Sub Util_Pub_Cta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Util_Pub_Cta_LostFocus()
    Util_Pub_Cta.Text = Formateado(Str(Val(Util_Pub_Cta.Text)), 2, 5, " ", False)
End Sub

Private Sub Util_Rev_Cta_GotFocus()
    Util_Rev_Cta.SelStart = 0
    Util_Rev_Cta.SelLength = Len(Util_Rev_Cta.Text)
End Sub

Private Sub Util_Rev_Cta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Util_Rev_Cta_LostFocus()
    Util_Rev_Cta.Text = Formateado(Str(Val(Util_Rev_Cta.Text)), 2, 5, " ", False)
End Sub

Private Sub Util_Rev_GotFocus()
    Util_Rev.Text = Trim(Util_Rev.Text)
    Util_Rev.SelStart = 0
    Util_Rev.SelText = ""
    Util_Rev.SelLength = Len(Util_Rev.Text)
End Sub

Private Sub Util_Rev_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Util_Rev_LostFocus()
    Util_Rev.Text = Formateado(Str(Val(Util_Rev.Text)), 2, 5, " ", False)
End Sub

Private Sub Utilidad_GotFocus()
    Utilidad.Text = Trim(Utilidad.Text)
    Utilidad.SelStart = 0
    Utilidad.SelText = ""
    Utilidad.SelLength = Len(Utilidad.Text)
End Sub

Private Sub Utilidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Utilidad_LostFocus()
    Utilidad.Text = Formateado(Str(Val(Utilidad.Text)), 2, 5, " ", False)
End Sub

Private Sub Imprimir_Click()
    If MsgBox("Confirma la impresiσn del listado de Rubros. ?.", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        Dim l As Long
        
        qy = "SELECT * FROM Rubro ORDER BY Denominacion"
        AbreRs
        
        If Rs.RecordCount > 0 Then
            l = 0
            
            Printer.Font = "Courier New"
            Printer.FontSize = 9
            
            Imprimir_Encabezado
            
            While Not Rs.EOF
                
                If l <= 64 Then
                    Printer.Print "  " & Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
                    l = l + 1
                Else
                    Printer.Print " "
                    
                    l = 0
                    Printer.NewPage
                    Imprimir_Encabezado
                End If
                
                Rs.MoveNext
            Wend
            
            Printer.Print " "
            Printer.EndDoc
        End If
        
        Salir.SetFocus
    Else
        Codigo.SetFocus
    End If
End Sub

Private Sub Imprimir_Encabezado()
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                      Pαgina.: " & Printer.Page
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                   Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                             Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " e-mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print "                                    LISTA DE RUBROS POR ORDEN ALFABΙTICO"
    Printer.Print
    Printer.Print " "
    Printer.Print "   Cσd. Denominaciσn "
    Printer.Print " "
End Sub
