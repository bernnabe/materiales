VERSION 5.00
Begin VB.Form Fac_Recibo_1 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Emisiσn de Recibos"
   ClientHeight    =   7590
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9495
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7590
   ScaleWidth      =   9495
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Importe_Recibo 
      Enabled         =   0   'False
      Height          =   2535
      Left            =   0
      TabIndex        =   20
      Top             =   4080
      Width           =   9495
      Begin VB.TextBox Text3 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8040
         MaxLength       =   10
         TabIndex        =   32
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox Text2 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8040
         MaxLength       =   10
         TabIndex        =   31
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Text1 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8040
         MaxLength       =   10
         TabIndex        =   30
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Importe 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8040
         MaxLength       =   10
         TabIndex        =   23
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox Importe_Letras 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   22
         Top             =   2160
         Width           =   8295
      End
      Begin VB.TextBox Ret_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   21
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe abonado con Targetas de Crιdito:"
         Height          =   255
         Left            =   4920
         TabIndex        =   35
         Top             =   960
         Width           =   3015
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe abonado en Cheques:"
         Height          =   255
         Left            =   4920
         TabIndex        =   34
         Top             =   600
         Width           =   3015
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe abonado en Efectivo:"
         Height          =   255
         Left            =   4920
         TabIndex        =   33
         Top             =   240
         Width           =   3015
      End
      Begin VB.Label Label6 
         Alignment       =   2  'Center
         Caption         =   "TOTAL:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8040
         TabIndex        =   26
         Top             =   1560
         Width           =   1335
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Son Pesos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   2160
         Width           =   855
      End
      Begin VB.Label Label10 
         Alignment       =   2  'Center
         Caption         =   "Ret. Iva:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1080
         TabIndex        =   24
         Top             =   1560
         Width           =   1335
      End
   End
   Begin VB.Frame Marco_Recibo 
      Height          =   735
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   9495
      Begin VB.TextBox Numero 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   2
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8040
         MaxLength       =   10
         TabIndex        =   1
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Nϊmero:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   4
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7200
         TabIndex        =   3
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Marco_Cuenta 
      Height          =   1335
      Left            =   0
      TabIndex        =   5
      Top             =   720
      Width           =   9495
      Begin VB.OptionButton Clientes 
         Caption         =   "Clientes"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   600
         TabIndex        =   13
         Top             =   600
         Value           =   -1  'True
         Width           =   1335
      End
      Begin VB.OptionButton Proveedores 
         Caption         =   "Proveedores"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   600
         TabIndex        =   12
         Top             =   840
         Width           =   1335
      End
      Begin VB.CommandButton Buscar_Cuenta 
         Caption         =   "&Cuenta:"
         Height          =   255
         Left            =   3000
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Cuenta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4080
         MaxLength       =   5
         TabIndex        =   10
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox Domicilio 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5040
         MaxLength       =   30
         TabIndex        =   8
         Top             =   600
         Width           =   4335
      End
      Begin VB.TextBox Saldo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5040
         MaxLength       =   10
         TabIndex        =   7
         Top             =   960
         Width           =   1335
      End
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5040
         TabIndex        =   6
         Top             =   240
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.TextBox Nombre 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5040
         MaxLength       =   30
         TabIndex        =   9
         Top             =   240
         Width           =   4335
      End
      Begin VB.Label Label3 
         Caption         =   "Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Domicilio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3240
         TabIndex        =   15
         Top             =   600
         Width           =   1695
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3240
         TabIndex        =   14
         Top             =   960
         Width           =   1695
      End
   End
   Begin VB.Frame Marco_Facturas 
      Enabled         =   0   'False
      Height          =   2055
      Left            =   0
      TabIndex        =   17
      Top             =   2040
      Width           =   9495
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1590
         Left            =   120
         Style           =   1  'Checkbox
         TabIndex        =   18
         Top             =   360
         Width           =   9255
      End
      Begin VB.Label Label9 
         Caption         =   $"Fac_Recibo_1.frx":0000
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   19
         Top             =   120
         Width           =   9255
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   27
      Top             =   6600
      Width           =   9495
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Fac_Recibo_1.frx":0095
         Style           =   1  'Graphical
         TabIndex        =   29
         ToolTipText     =   "Grabar los Datos del Recibo.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   8280
         Picture         =   "Fac_Recibo_1.frx":039F
         Style           =   1  'Graphical
         TabIndex        =   28
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Fac_Recibo_1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Buscar_Cuenta_Click()
    MousePointer = 11
    Cuentas_Encontradas.Visible = True
    Cuentas_Encontradas.Clear
    Qy = "SELECT * FROM " & IIf(Clientes.Value = True, "Cliente", "Proveedor") & " ORDER BY Nombre "
    Set Rs = Db.OpenRecordset(Qy)
    
    While Not Rs.EOF
        Cuentas_Encontradas.AddItem Trim$(Rs.Fields("Nombre")) + Space$(30 - Len(Trim$(Rs.Fields("Nombre")))) & " " & Trim$(Rs.Fields(0))
        Rs.MoveNext
    Wend
    MousePointer = 0
    Cuentas_Encontradas.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Clientes_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_GotFocus()
    Cuenta.Text = Trim$(Cuenta.Text)
    Cuenta.SelStart = 0
    Cuenta.SelText = ""
    Cuenta.SelLength = Len(Cuenta.Text)
End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_LostFocus()
    If Val(Cuenta.Text) > 0 Then
        Leer_Cuenta
    End If
End Sub

Private Sub Leer_Cuenta()
    If Val(Cuenta.Text) > 0 Then
        Qy = "SELECT " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Cuenta,Cliente.Id_Cliente,Cliente.Nombre,Cliente.Domicilio", "CtaCte_Proveedor.Id_Cuenta,Proveedor.Id_Proveedor,Proveedor.Nombre,Proveedor.Domicilio") & ", SUM(Debe - Haber) AS Saldo_Cuenta FROM " & IIf(Clientes.Value = True, "CtaCte_Cliente,Cliente", "CtaCte_Proveedor,Proveedor") & " WHERE Id_Cuenta = " & Trim$(Str$(Val(Cuenta.Text))) & " "
        Qy = Qy & "AND " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Cuenta = Cliente.Id_Cliente", "CtaCte_Proveedor.Id_Cuenta = Proveedor.Id_Proveedor") & " "
        Qy = Qy & "AND Id_Empresa = " & Trim$(Str$(Val(Id_Empresa))) & " "
        Qy = Qy & "GROUP BY " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Cuenta,Cliente.Id_Cliente,Cliente.Nombre,Cliente.Domicilio", "CtaCte_Proveedor.Id_Cuenta,Proveedor.Id_Proveedor,Proveedor.Nombre,Proveedor.Domicilio")
        Set Rs = Db.OpenRecordset(Qy)
        
        If Rs.EOF Then
            MsgBox "La Cuenta es inexistente...", vbInformation, "Atenciσn.!"
            Borrar_Campo_Cuenta
            Cuenta.SetFocus
        Else
            Nombre.Text = Rs.Fields("Nombre")
            Domicilio.Text = Rs.Fields("Domicilio")
            Saldo.Text = Formateado(Str$(Val(Rs.Fields("Saldo_Cuenta"))), 2, 10, " ", False)
            
            Mostrar_Facturas_Pendientes
            
            Marco_Facturas.Enabled = True
            Marco_Importe_Recibo.Enabled = True
            Importe.SetFocus
        End If
    Else
        Cuenta.Text = ""
        Cuenta.SetFocus
    End If
End Sub

Private Sub Mostrar_Facturas_Pendientes()
    Dim Haberes      As Single
    Dim Pagado       As Single
    Dim Pagos        As Single
    Dim Deuda        As Single
    Dim Adeuda       As Single
    Dim Acumulado    As Single
    
    List1.Clear
        
    Qy = "SELECT SUM(Haber) FROM " & IIf(Clientes.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " WHERE Id_Cuenta = " & Trim$(Str$(Val(Cuenta.Text))) & " "
    Qy = Qy & "AND Id_Empresa = " & Trim$(Str$(Val(Id_Empresa)))
    Set Rs = Db.OpenRecordset(Qy)
    
    If Not Rs.Fields(0) = "" Then Pagos = Val(Rs.Fields(0))
    
    Qy = "SELECT * FROM " & IIf(Clientes.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " WHERE Id_Cuenta = " & Trim$(Str$(Val(Cuenta.Text))) & " "
    Qy = Qy & "AND Id_Empresa = " & Trim$(Str$(Val(Id_Empresa))) & " "
    Qy = Qy & "ORDER BY Comprobante, Fecha ASC"
    Set Rs = Db.OpenRecordset(Qy)
    
    If Val(Saldo.Text) > 0 Then
        
        While Not Rs.EOF
            Pagos = Val(Pagos) + Formateado(Str$(Val(Rs.Fields("Haber"))), 2, 0, "", False)
            Deuda = Formateado(Str$(Val(Rs.Fields("Debe"))), 2, 0, "", False)
            
            If Val(Pagos) > 0 Then
                    
                Adeuda = Val(Deuda) - Val(Pagos)
                Adeuda = Formateado(Str$(Val(Adeuda)), 2, 10, " ", False)
                
                Pagado = Val(Deuda) - Val(Adeuda)
                Pagado = Formateado(Str$(Val(Pagado)), 2, 10, " ", False)
                
                If Val(Deuda) < Val(Pagos) Then
                    Pagos = Val(Pagos) - Val(Deuda)
                Else
                    Pagos = Val(Pagos) - Val(Pagado)
                End If
                Pagos = Formateado(Str$(Val(Pagos)), 2, 10, " ", False)
                
            Else
                Pagado = 0
                Adeuda = Val(Deuda)
            End If
                                       
            Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & " "
            Txt = Txt & Trim$(Rs.Fields("Comprobante")) & " "
            Txt = Txt & Format(Rs.Fields("Vencimiento"), "dd/mm/yy") & " "
            Txt = Txt & Formateado(Str$(Val(DateDiff("d", Date, Rs.Fields("Vencimiento")))), 0, 3, " ", False) & " "
            Txt = Txt & Formateado(Str$(Val(Rs.Fields("Debe"))), 2, 10, " ", False)
            Txt = Txt & Formateado(Str$(Val(Pagado)), 2, 10, " ", False) & " "
            Txt = Txt & Formateado(Str$(Val(Adeuda)), 2, 10, " ", False) & " "
            Txt = Txt & Formateado(Str$(Val(Acumulado)), 2, 10, " ", False) & " "
            
            If Trim$(UCase$(Mid$(Rs.Fields("Comprobante"), 1, 2))) = "FC" Or Trim$(UCase$(Mid$(Rs.Fields("Comprobante"), 1, 2))) = "ND" Or Trim$(UCase$(Mid$(Rs.Fields("Comprobante"), 1, 2))) = "OP" Then
                If Val(Rs.Fields("debe")) = Val(Pagado) Then Pagado = 0
                
                If Val(Adeuda) > 0 Then
                    
                    List1.AddItem Txt
                End If
            End If
            
            Rs.MoveNext
        Wend
    Else
        MsgBox "El Cliente no tiene facturas pendientes.-", vbInformation, "Atenciσn.!"
    End If
End Sub

Private Sub Borrar_Campo_Cuenta()
    Nombre.Text = ""
    Cuenta.Text = ""
    Domicilio.Text = ""
    Saldo.Text = ""
    Marco_Importe_Recibo.Enabled = False
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuenta.SetFocus
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Cuenta.Text = Mid$(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 32)
    Cuentas_Encontradas.Visible = False
    Leer_Cuenta
End Sub

Private Sub Fecha_GotFocus()
    Numero_GotFocus
    If Fecha.Text = "" Then Fecha.Text = Fecha_Fiscal
    Fecha.SelStart = 0
    Fecha.SelText = ""
    Fecha.SelLength = Len(Fecha.Text)
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
    
    If Fecha.Text = "error" Then
        MsgBox "Error ingresando las fechas, verifique e ingrese nuevamente.", vbInformation, "Atenciσn.!"
        Fecha.Text = ""
        Fecha.SetFocus
    End If
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 7
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Forma_Pago.AddItem "01 - EFECTIVO"
    Forma_Pago.AddItem "02 - CHEQUE  "
    Forma_Pago.AddItem "03 - TARGETA DE CRΙDITO"
    
    Menu.Estado.Panels(2).Text = "Pagos en Cuenta Corriente.-"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Forma_Pago_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Forma_Pago_KeyPress(KeyAscii As Integer)
    If Val(Importe.Text) > 0 And Val(Fecha.Text) And Val(Forma_Pago.Text) > 0 Then
        Grabar.Enabled = True
    Else
        Grabar.Enabled = False
    End If
    
    
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Forma_Pago_LostFocus()
    If Val(Mid$(Forma_Pago.Text, 1, 2)) = 2 And Val(Importe.Text) > 0 Then
        Abrir_Cartera_Cheque
    End If
End Sub

Private Sub Abrir_Cartera_Cheque()
    Fac_Recibo.Enabled = False
    Load Dat_Cheques
    Dat_Cheques.Show
    
    Dat_Cheques.Tipo.Text = "RC"
    Dat_Cheques.Letra.Text = "X"
    Dat_Cheques.Nro.Text = Numero.Text
    
    If Fac_Recibo.Clientes.Value = True Then
        Dat_Cheques.Nuestra_Firma.Value = 0
        Dat_Cheques.Nuestra_Firma.Enabled = False
    End If
End Sub

Private Sub Grabar_Click()
    Dim Rec_Asiento As String
    
    Qy = "INSERT INTO Recibo VALUES ("
    Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
    Qy = Qy & ", " & Trim$(Str$(Val(Numero.Text)))
    Qy = Qy & ", '" & Trim$(Fecha.Text) & " " & Trim$(Hora_Fiscal) & "'"
    Qy = Qy & ", '" & IIf(Clientes.Value = True, "C", "P") & "'"
    Qy = Qy & ", " & Trim$(Str$(Val(Cuenta.Text)))
    Qy = Qy & ", " & Trim$(Str$(Val(Ret_Iva.Text)))
    Qy = Qy & ", " & Trim$(Str$(Val(Importe.Text))) & ")"
    Db.Execute (Qy)
    
    Qy = "INSERT INTO " & IIf(Clientes.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " VALUES ("
    Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
    Qy = Qy & ", " & Trim$(Str$(Val(Cuenta.Text)))
    Qy = Qy & ", '" & Trim$(Fecha.Text) & " " & Trim$(Hora_Fiscal) & "'"
    Qy = Qy & ", 'RC X " & Trim$(Numero.Text) & "'"
    Qy = Qy & ", 'PAGO A CUENTA CORRIENTE'"
    Qy = Qy & ", '" & Trim$(Fecha.Text) & "'"
    Qy = Qy & ", 0, " & Trim$(Str$(Val(Importe.Text))) & ")"
    Db.Execute (Qy)
    
    Qy = "SELECT MAX(Nro_Asiento) FROM Asiento WHERE Ejercicio = " & Trim$(Str$(Val(Mid$(Fecha.Text, 7, 4)))) & " "
    Qy = Qy & "AND Empresa = " & Trim$(Str$(Val(Id_Empresa)))
    Set Rs = Db.OpenRecordset(Qy)
        
    Rec_Asiento = 1
    If Not Rs.Fields(0) = "" Then Rec_Asiento = Val(Rs.Fields(0)) + 1
        
    Qy = "INSERT INTO Asiento VALUES ("
    Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
    Qy = Qy & ", " & Trim$(Str$(Val(Rec_Asiento)))
    Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Fecha.Text, 7, 4))))
    Qy = Qy & ", '" & Trim$(Fecha.Text) & "'"
    Qy = Qy & ", 1"
    If Val(Forma_Pago.Text) = 1 Then
        Qy = Qy & ", 1, 1, 1, 1, 0, 0"
    Else
        If Val(Forma_Pago.Text) = 2 Then
            If Clientes.Value = False Then
                Qy = Qy & ", 1, 1, 2, 2, 0, 0"
            Else
                Qy = Qy & ", 1, 3, 2, 2, 0, 0"
            End If
        End If
    End If
    Qy = Qy & ", 'RC X " & Trim$(Numero.Text) & " " & IIf(Clientes.Value = True, "CLIENTE", "PROVEEDOR") & "'"
    Qy = Qy & ", " & Trim$(Str$(Val(Importe.Text)))
    Qy = Qy & ", 0)"
    Db.Execute (Qy)
    
    Qy = "INSERT INTO Asiento VALUES ("
    Qy = Qy & Trim$(Str$(Val(Id_Empresa)))
    Qy = Qy & ", " & Trim$(Str$(Val(Rec_Asiento)))
    Qy = Qy & ", " & Trim$(Str$(Val(Mid$(Fecha.Text, 7, 4))))
    Qy = Qy & ", '" & Trim$(Fecha.Text) & "'"
    Qy = Qy & ", 2"
    If Clientes.Value = True Then
        Qy = Qy & ", 1, 3, 2, 1, 0, 0"
    Else
        Qy = Qy & ", 2, 1, 2, 1, 0, 0"
    End If
    Qy = Qy & ", 'RC X " & Trim$(Numero.Text) & " " & IIf(Clientes.Value = True, "CLIENTE", "PROVEEDOR") & "'"
    Qy = Qy & ", 0"
    Qy = Qy & ", " & Trim$(Str$(Val(Importe.Text))) & ")"
    Db.Execute (Qy)
    
    If MsgBox("Imprime el comprobante de esta operaciσn. ?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        Imprimir
    End If
    
    Salir_Click
End Sub

Private Sub Imprimir()
    Printer.Font = "Courier New"
    Printer.FontSize = 11
    
    Printer.Print "     ++"
    Printer.Print "     |FECHA: 00/00/0000                             NΪMERO: 000000000|"
    Printer.Print "     |                                                               |"
    Printer.Print "     ++"
    Printer.Print "     |CUENTA: 00000 - XXXXXXXXXXXXXXXXXXXXXXXXXXXXXX SALDO 0000000.00|"
    Printer.Print "     ++"
    
    Printer.EndDoc
End Sub

Private Sub Importe_Change()
    If Val(Importe.Text) > 0 And Val(Fecha.Text) And Val(Forma_Pago.Text) > 0 Then
        Grabar.Enabled = True
    Else
        Grabar.Enabled = False
    End If
End Sub

Private Sub Importe_GotFocus()
    Importe.Text = Trim$(Importe.Text)
    Importe.SelStart = 0
    Importe.SelText = ""
    Importe.SelLength = Len(Importe.Text)
End Sub

Private Sub Importe_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Importe_LostFocus()
    Importe.Text = Formateado(Str$(Val(Importe.Text)), 2, 10, " ", False)
    Importe_Letras.Text = NumerosALetras(Str$(Val(Importe.Text)), False, False)
End Sub

Private Sub Numero_GotFocus()
    Qy = "SELECT MAX(Id_Recibo) FROM Recibo"
    Set Rs = Db.OpenRecordset(Qy)
    
    Numero.Text = 1
    If Not Rs.Fields(0) = "" Then Numero.Text = Val(Rs.Fields(0)) + 1
    
    Numero.Text = Formateado(Str$(Val(Numero.Text)), 0, 10, "0", False)
    
    Numero.SelStart = 0
    Numero.SelText = ""
    Numero.SelLength = Len(Numero.Text)
End Sub

Private Sub Numero_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Numero_LostFocus()
    Numero.Text = Formateado(Str$(Val(Numero.Text)), 0, 10, "0", False)
End Sub

Private Sub Proveedores_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ret_Iva_GotFocus()
    Ret_Iva.Text = Trim$(Ret_Iva.Text)
    Ret_Iva.SelStart = 0
    Ret_Iva.SelText = ""
    Ret_Iva.SelLength = Len(Ret_Iva.Text)
End Sub

Private Sub Ret_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ret_Iva_LostFocus()
    Ret_Iva.Text = Formateado(Str$(Val(Ret_Iva.Text)), 2, 10, " ", False)
End Sub

Private Sub Salir_Click()
    If Cuenta.Text = "" And Fecha.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Numero.Text = ""
    Fecha.Text = ""
    Clientes.Value = True
    Borrar_Campo_Cuenta
    List1.Clear
    Importe.Text = ""
    Forma_Pago.Text = ""
    Importe_Letras.Text = ""
    Ret_Iva.Text = ""
    
    Marco_Importe_Recibo.Enabled = False
    Marco_Facturas.Enabled = False
    
    Fecha.SetFocus
End Sub
