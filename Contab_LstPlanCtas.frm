VERSION 5.00
Begin VB.Form Contab_LstPlanCtas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Plan de Cuentas.-"
   ClientHeight    =   6870
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6870
   ScaleWidth      =   7575
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Cabeza 
      Enabled         =   0   'False
      Height          =   855
      Left            =   0
      TabIndex        =   4
      Top             =   0
      Width           =   7575
      Begin VB.TextBox Empresa 
         Alignment       =   2  'Center
         BackColor       =   &H00FFFFC0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   375
         Left            =   120
         TabIndex        =   7
         Top             =   240
         Width           =   7335
      End
   End
   Begin VB.Frame Marco_Lista 
      Height          =   5055
      Left            =   0
      TabIndex        =   5
      Top             =   840
      Width           =   7575
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4560
         Left            =   120
         TabIndex        =   3
         Top             =   360
         Width           =   7335
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   120
         Width           =   7335
         Begin VB.CommandButton Command2 
            Caption         =   "Cσdigo / Denominaciσn"
            Height          =   255
            Left            =   0
            TabIndex        =   9
            Top             =   0
            Width           =   7335
         End
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   6
      Top             =   5880
      Width           =   7575
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   4920
         Picture         =   "Contab_LstPlanCtas.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   10
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirmar 
         Height          =   615
         Left            =   120
         Picture         =   "Contab_LstPlanCtas.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Confirma la Carga de Lista de Proveedores.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   6360
         Picture         =   "Contab_LstPlanCtas.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   1
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   3840
         Picture         =   "Contab_LstPlanCtas.frx":CE86
         Style           =   1  'Graphical
         TabIndex        =   0
         ToolTipText     =   "Imprimir el Plan de Cuentas.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Contab_LstPlanCtas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Confirmar_Click()
    List1.Clear
        
    qy = "SELECT * FROM Cuenta WHERE Cuenta.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "ORDER BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6"
    AbreRs
    
    While Not Rs.EOF
        Txt = Val(Rs.Fields("Id_Nivel_1")) & "."
        If Val(Rs.Fields("Id_Nivel_2")) = 0 Then Txt = Txt & "  "
        
        Txt = Txt & IIf(Val(Rs.Fields("Id_Nivel_2")) > 0, Val(Rs.Fields("Id_Nivel_2")) & ".", " ")
        Txt = Txt & IIf(Val(Rs.Fields("Id_Nivel_3")) > 0, Val(Rs.Fields("Id_Nivel_3")) & ".", " ")
        Txt = Txt & IIf(Val(Rs.Fields("Id_Nivel_4")) > 0, Val(Rs.Fields("Id_Nivel_4")) & ".", " ")
        Txt = Txt & IIf(Val(Rs.Fields("Id_Nivel_5")) > 0, Val(Rs.Fields("Id_Nivel_5")) & ".", " ")
        Txt = Txt & IIf(Val(Rs.Fields("Id_Nivel_6")) > 0, Val(Rs.Fields("Id_Nivel_6")) & "  ", " ")
        
        If Val(Rs.Fields("Id_Nivel_2")) > 0 Then Txt = Txt & Space(6)
        If Val(Rs.Fields("Id_Nivel_3")) > 0 Then Txt = Txt & Space(6)
        If Val(Rs.Fields("Id_Nivel_4")) > 0 Then Txt = Txt & Space(6)
        If Val(Rs.Fields("Id_Nivel_5")) > 0 Then Txt = Txt & Space(6)
        If Val(Rs.Fields("Id_Nivel_6")) > 0 Then Txt = Txt & Space(6)
        
        Txt = Txt & Trim(Rs.Fields("denominacion"))
        
        List1.AddItem Txt
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Confirmar_Click
    
    Empresa.Text = UCase(cEmpresa)
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
    
    If List1.ListCount > 0 Then
        Printer.Font = "Courier New"
        Printer.FontSize = 9
        
        i = 0
        l = 0
        
        Imprimir_Encabezado
        
        For i = 0 To List1.ListCount - 1
            If l <= 65 Then
                Printer.Print " " & List1.List(i)
                l = l + 1
            Else
                Printer.NewPage
                Imprimir_Encabezado
                l = 0
            End If
        Next
        
        Printer.Print " "
        Printer.EndDoc
    End If
    
    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Dim Titulo As String
    Titulo = "                                                    PLAN DE CUENTAS"
    
    Imprimir.Tag = Int(List1.ListCount / 65) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                      Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                   Fecha..: " & Format(Fecha_Fiscal, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                             Hora...: " & Format(Hora_Fiscal, "hh.mm")
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " E-Mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print Titulo
    Printer.Print
    Printer.Print " "
    Printer.Print " CΣDIGO                DENOMINACIΣN                                      "
    Printer.Print " "
End Sub

Private Sub Salir_Click()
    Unload Me
End Sub
