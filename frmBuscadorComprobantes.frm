VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmBuscadorComprobantes 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Buscar comprobantes"
   ClientHeight    =   6510
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   8175
   ControlBox      =   0   'False
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   6510
   ScaleWidth      =   8175
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Frame Frame1 
      Height          =   1140
      Left            =   75
      TabIndex        =   0
      Top             =   0
      Width           =   8040
      Begin VB.CommandButton btnBuscar 
         Default         =   -1  'True
         Height          =   540
         Left            =   6750
         Picture         =   "frmBuscadorComprobantes.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   225
         Width           =   1140
      End
      Begin VB.CheckBox chkSoloPendientes 
         Caption         =   "S�lo comprobantes pendientes"
         Height          =   240
         Left            =   2775
         TabIndex        =   10
         Top             =   750
         Value           =   1  'Checked
         Width           =   2640
      End
      Begin VB.ComboBox cboClientes 
         Height          =   315
         Left            =   2775
         Style           =   2  'Dropdown List
         TabIndex        =   8
         Top             =   375
         Width           =   3690
      End
      Begin VB.OptionButton optOrdenesDevolucion 
         Caption         =   "Devoluciones"
         Enabled         =   0   'False
         Height          =   240
         Left            =   75
         TabIndex        =   5
         Top             =   825
         Width           =   2415
      End
      Begin VB.OptionButton optOrdenesEntrega 
         Caption         =   "Ordenes de entrega"
         Height          =   240
         Left            =   75
         TabIndex        =   4
         Top             =   525
         Width           =   1890
      End
      Begin VB.OptionButton optPresupuestos 
         Caption         =   "Presupuestos"
         Height          =   240
         Left            =   75
         TabIndex        =   3
         Top             =   225
         Value           =   -1  'True
         Width           =   1890
      End
      Begin VB.Label Label1 
         Caption         =   "Cliente"
         Height          =   240
         Left            =   2775
         TabIndex        =   9
         Top             =   150
         Width           =   915
      End
   End
   Begin VB.Frame fraLista 
      Height          =   3990
      Left            =   75
      TabIndex        =   1
      Top             =   1125
      Width           =   8040
      Begin VB.CommandButton cmdBorrarTodos 
         Caption         =   "Borrar Seleccion"
         Height          =   315
         Left            =   1575
         TabIndex        =   13
         Top             =   3600
         Width           =   1440
      End
      Begin VB.CommandButton cmdSeleccionarTodos 
         Caption         =   "Seleccionar todos"
         Height          =   315
         Left            =   75
         TabIndex        =   12
         Top             =   3600
         Width           =   1440
      End
      Begin MSComctlLib.ListView lvwLista 
         Height          =   3315
         Left            =   75
         TabIndex        =   11
         Top             =   225
         Width           =   7815
         _ExtentX        =   13785
         _ExtentY        =   5847
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         Checkboxes      =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
   Begin VB.Frame Frame2 
      Height          =   540
      Left            =   75
      TabIndex        =   14
      Top             =   5100
      Width           =   8040
      Begin VB.CheckBox chkActualizarPrecios 
         Caption         =   "Actualizar el precio de los art�culos al precio m�s actual."
         Height          =   315
         Left            =   75
         TabIndex        =   15
         Top             =   150
         Width           =   5115
      End
   End
   Begin VB.Frame Frame3 
      Height          =   840
      Left            =   75
      TabIndex        =   2
      Top             =   5625
      Width           =   8040
      Begin VB.CommandButton btnCancelar 
         Cancel          =   -1  'True
         Caption         =   "Cancelar"
         Height          =   465
         Left            =   6675
         TabIndex        =   7
         Top             =   225
         Width           =   1215
      End
      Begin VB.CommandButton btnConfirma 
         Caption         =   "Confirma"
         Height          =   465
         Left            =   75
         TabIndex        =   6
         Top             =   225
         Width           =   1215
      End
   End
End
Attribute VB_Name = "frmBuscadorComprobantes"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mvarClienteId As Long
Private mvarTipoComprobante As TiposComprobantes
Private mvarConfirma As Boolean
Private mvarResultItems As New ADODB.Recordset

Private Const mconstListColFecha = 1
Private Const mconstListColTotal = 2
Private Const mconstListColEstado = 3

Private mvarNumerosComprobantesSeleccionados As String
Private mvarTipoComprobanteSeleccionado As TiposComprobantes
Private mvarActualizarPrecios As Boolean

Public Property Get TipoComprobanteSeleccionado() As TiposComprobantes
    TipoComprobanteSeleccionado = mvarTipoComprobanteSeleccionado
End Property

Public Property Let TipoComprobanteSeleccionado(vdata As TiposComprobantes)
    mvarTipoComprobanteSeleccionado = vdata
End Property

Public Property Get ActualizarPrecios() As Boolean
    ActualizarPrecios = mvarActualizarPrecios
End Property

Public Property Let ActualizarPrecios(vdata As Boolean)
    mvarActualizarPrecios = vdata
End Property

Public Property Get NumerosComprobantesSeleccionados() As String
    NumerosComprobantesSeleccionados = mvarNumerosComprobantesSeleccionados
End Property

Public Property Let NumerosComprobantesSeleccionados(vdata As String)
    mvarNumerosComprobantesSeleccionados = vdata
End Property

Public Property Get ResultItems() As ADODB.Recordset
    Set ResultItems = mvarResultItems
End Property

Public Property Let ResultItems(vdata As ADODB.Recordset)
    Set mvarResultItems = vdata
End Property

Public Property Get Confirma() As Boolean
    Confirma = mvarConfirma
End Property

Public Property Let Confirma(vdata As Boolean)
    mvarConfirma = vdata
End Property

Public Property Get ClienteId() As Long
    ClienteId = mvarClienteId
End Property

Public Property Let ClienteId(vdata As Long)
    mvarClienteId = vdata
End Property

Public Property Get TipoComprobante() As TiposComprobantes
    TipoComprobante = mvarTipoComprobante
End Property

Public Property Let TipoComprobante(vdata As TiposComprobantes)
    mvarTipoComprobante = vdata
End Property

Private Sub btnCancelar_Click()

    If (lvwLista.ListItems.Count = 0) Then
        Unload Me
    Else
        Call CambiarEstadoPanelBusqueda(True)
        LimiparControles
    End If

End Sub


Private Sub btnBuscar_Click()

    MousePointer = 11

    If (Validar()) Then
    
        CargarDatos
        Call CambiarEstadoPanelBusqueda(False)
        lvwLista.SetFocus
        
    End If
    
    MousePointer = 0

End Sub

Private Function CambiarEstadoPanelBusqueda(Estado As Boolean)
    
    optOrdenesEntrega.Enabled = Estado
    optPresupuestos.Enabled = Estado
    cboClientes.Enabled = Estado
    chkSoloPendientes.Enabled = Estado

End Function

Private Sub btnConfirma_Click()

    If (ValidarSeleccion()) Then
    
        SetearDatos
        ConfirmarOperacion
        Unload Me
        
    End If

End Sub

Private Sub SetearDatos()
    
    ActualizarPrecios = CBool(chkActualizarPrecios.Value)

    'Seteo tipo de comprobante
    If optOrdenesDevolucion.Value Then TipoComprobanteSeleccionado = OrdenDevolucion
    If optOrdenesEntrega.Value Then TipoComprobanteSeleccionado = OrdenEntrega
    If optPresupuestos.Value Then TipoComprobanteSeleccionado = Presupuesto
    
End Sub

Private Function ValidarSeleccion() As Boolean
    
    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String
    Dim objCliente As New clsCliente
    
    ValidarSeleccion = True
    
    Dim Item As ListItem
    Dim blnSeleccionEncontrada As Boolean
    
    blnSeleccionEncontrada = False
    
    For Each Item In lvwLista.ListItems
        
        If Item.Checked Then
            blnSeleccionEncontrada = True
            Exit For
        End If
    
    Next
    
    If Not blnSeleccionEncontrada Then
        
        MsgBox "Para confirmar la importaci�n debe seleccionar por lo menos un registro", vbCritical, "Atenci�n!"
        ValidarSeleccion = False
        
        lvwLista.SetFocus
        
        Exit Function
        
    End If
    
    If (cboClientes.ListIndex = -1) Then
    
        MsgBox "Debe haber un cliente seleccionado.", vbCritical, "Atenci�n!"
        ValidarSeleccion = False
        
        CambiarEstadoPanelBusqueda True
        LimiparControles
        cboClientes.SetFocus
        
        Exit Function
    
    End If
    
    If (ClienteId <> cboClientes.ItemData(cboClientes.ListIndex)) Then
    
        If (objCliente.GetById(ClienteId).CondicionIva <> objCliente.GetById(cboClientes.ItemData(cboClientes.ListIndex)).CondicionIva) Then
        
            MsgBox "Deben coincidir las condiciones de iva de los clientes seleccionados .", vbCritical, "Atenci�n!"
            ValidarSeleccion = False
            
            CambiarEstadoPanelBusqueda True
            LimiparControles
            cboClientes.SetFocus
                
        End If
    
    End If
    
    Set rstDatos = Nothing
    Set objCliente = Nothing

End Function

Private Sub cmdBorrarTodos_Click()

    Call CambiarSeleccion(False)

End Sub

Private Sub cmdSeleccionarTodos_Click()

    Call CambiarSeleccion(True)

End Sub

Private Function CambiarSeleccion(Seleccionado As Boolean)

    Dim Item As ListItem

    If Not lvwLista.ListItems Is Nothing Then

        For Each Item In lvwLista.ListItems
        
            Item.Checked = Seleccionado
            
        Next
    
    End If

End Function

Private Sub Form_Load()
    
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    
    CrearLista
    CargarCombos
    SetParametrosPorDefecto
    
End Sub

Private Function Validar() As Boolean

    Validar = True

    If (cboClientes.ListIndex = -1) Then
    
        MsgBox "Debe seleccionar un cliente", vbInformation, "Atenci�n"
        cboClientes.SetFocus
        Validar = False
    
    End If

End Function

Private Sub SetParametrosPorDefecto()

    Select Case TipoComprobante
        Case FacturaVenta
            optPresupuestos.Enabled = True
            optOrdenesEntrega.Enabled = True
            fraLista.Caption = "Lista de Comprobantes"
            optPresupuestos.Value = True
        Case Presupuesto
            optPresupuestos.Enabled = False
            optOrdenesEntrega.Value = True
            fraLista.Caption = "Lista de Presupuestos"
        Case OrdenEntrega
            optOrdenesEntrega.Enabled = False
            optPresupuestos.Value = True
            fraLista.Caption = "Lista de Ordenes de Entrega"
        Case OrdenDevolucion
            optOrdenesDevolucion.Value = True
            fraLista.Caption = "Lista de Ordenes de devoluciones"
    End Select
    
    If (Me.ClienteId > 0) Then
    
        cboClientes.ListIndex = BusPosEnCombo(cboClientes, Me.ClienteId)
        
    End If

End Sub

Private Sub CargarCombos()

    Dim rstClientes As New ADODB.Recordset
    Dim objCliente As New clsCliente
    
    Set rstClientes = objCliente.GetAll()
    
    While Not rstClientes.EOF
    
        cboClientes.AddItem rstClientes("Nombre")
        cboClientes.ItemData(cboClientes.NewIndex) = rstClientes("Id_Cliente")
        
        rstClientes.MoveNext
    
    Wend
    
    Set rstClientes = Nothing
    Set rstClientes = Nothing
    
End Sub

Private Sub CargarDatos()
    
    lvwLista.ListItems.Clear
    Dim intClienteId As Long
    
    intClienteId = 0
    
    If (cboClientes.ListIndex <> -1) Then
    
        intClienteId = cboClientes.ItemData(cboClientes.ListIndex)
        
    End If
    
    If (optPresupuestos.Value) Then
    
        GetPresupuestos intClienteId, CBool(chkSoloPendientes.Value)
        
    End If
    
    If (optOrdenesEntrega.Value) Then
    
        GetOrdenesEntrega intClienteId, CBool(chkSoloPendientes.Value)
    
    End If
    
    If (optOrdenesDevolucion.Value) Then
    
        GetOrdenesDevoluciones intClienteId, CBool(chkSoloPendientes.Value)
    
    End If
    
End Sub

Private Sub CrearLista()

    Dim ctmX As ColumnHeader
    
    lvwLista.ColumnHeaders.Clear
    
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "N�mero", 1500)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Fecha", 1500, 1)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Total", 1500, 1)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Estado", 1000)
    
    Set ctmX = Nothing
    
End Sub

Private Function GetPresupuestos(ClienteId As Long, Pendientes As Boolean)

    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String
    Dim objPresupuestoEstado As New clsPresupuestoEstados
    Dim strEstado As String
    Dim Item As ListItem
    
    strSql = "Select p.*, c.Nombre From Presupuesto p " & _
                "Inner Join Cliente c " & _
                    " On p.Cliente = c.Id_Cliente " & _
                "Where p.Cliente = " & CStr(ClienteId) & " " & _
                "And p.Estado = " & IIf(Pendientes, ComprobanteEstados.Pendiente, ComprobanteEstados.Facturado) & " " & _
                "And Empresa = " & CStr(Id_Empresa) & " " & _
                "Order by p.Fecha Desc"

    rstDatos.Open strSql, Db
    
    If Not (rstDatos.EOF) Then
    
        While Not rstDatos.EOF
        
            Set Item = lvwLista.ListItems.Add(, "K" & rstDatos("ID_Nro_Presupuesto"), rstDatos("Id_Nro_Presupuesto"))
            
            strEstado = ComprobanteEstados.GetDescripcionEstadoByCodigo(CInt(rstDatos("Estado")))
             
            With Item
            
                .SubItems(mconstListColFecha) = Format(rstDatos("Fecha"), "dd/MM/yyyy")
                .SubItems(mconstListColTotal) = Format(rstDatos("Total_Factura"), "0.00")
                .SubItems(mconstListColEstado) = strEstado
            
            End With
            
            rstDatos.MoveNext
        Wend
    
    End If
    
    Set rstDatos = Nothing

End Function

Private Function GetPresupuestoItem(Numeros As String) As ADODB.Recordset

    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String

    strSql = "Select * From Presupuesto_Item Where Id_Nro_Presupuesto IN (" & CStr(Numeros) & ") "

    rstDatos.Open strSql, Db

    Set GetPresupuestoItem = rstDatos

    Set rstDatos = Nothing

End Function

Private Function GetOrdenesEntrega(ClienteId As Long, Pendientes As Boolean)

    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String
    Dim objPresupuestoEstado As New clsPresupuestoEstados
    Dim strEstado As String
    Dim Item As ListItem
    
    strSql = "Select p.*, c.Nombre From Orden_Entrega p " & _
                "Inner Join Cliente c " & _
                    " On p.Cliente = c.Id_Cliente " & _
                "Where p.Cliente = " & CStr(ClienteId) & " " & _
                "And p.Estado = " & IIf(Pendientes, ComprobanteEstados.Pendiente, ComprobanteEstados.Facturado) & " " & _
                "And Empresa = " & CStr(Id_Empresa) & " " & _
                "Order by p.Fecha Desc"

    rstDatos.Open strSql, Db
    
    If Not (rstDatos.EOF) Then
    
        While Not rstDatos.EOF
        
            Set Item = lvwLista.ListItems.Add(, "K" & rstDatos("Id_Nro_Orden_Entrega"), rstDatos("Id_Nro_Orden_Entrega"))
            
            strEstado = ComprobanteEstados.GetDescripcionEstadoByCodigo(CInt(rstDatos("Estado")))
             
            With Item
            
                .SubItems(mconstListColFecha) = Format(rstDatos("Fecha"), "dd/MM/yyyy")
                .SubItems(mconstListColTotal) = Format(rstDatos("Total_Factura"), "0.00")
                .SubItems(mconstListColEstado) = strEstado
            
            End With
            
            rstDatos.MoveNext
        Wend
    
    End If
    
    Set rstDatos = Nothing

End Function

Private Function GetOrdenEntregaItem(Numeros As String) As ADODB.Recordset

    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String

    strSql = "Select * From Orden_Entrega_Item Where Id_Nro_Orden_Entrega IN (" & CStr(Numeros) & ") "

    rstDatos.Open strSql, Db

    Set GetOrdenEntregaItem = rstDatos

    Set rstDatos = Nothing

End Function


Private Function GetOrdenesDevoluciones(ClienteId As Long, Pendientes As Boolean)

    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String
    Dim objPresupuestoEstado As New clsPresupuestoEstados
    Dim strEstado As String
    Dim Item As ListItem
    
    strSql = "Select p.*, c.Nombre From Devolucion p " & _
                "Inner Join Cliente c " & _
                    " On p.Cliente = c.Id_Cliente " & _
                "Where p.Cliente = " & CStr(ClienteId) & " " & _
                "And Empresa = " & CStr(Id_Empresa) & " " & _
                "Order by p.Fecha Desc"
                '"And Facturado = " & IIf(Pendientes, "0", "1") & " " & _

    rstDatos.Open strSql, Db
    
    If Not (rstDatos.EOF) Then
    
       While Not rstDatos.EOF
        
            Set Item = lvwLista.ListItems.Add(, "K" & rstDatos("Id_Nro_Devolucion"), rstDatos("Id_Nro_Devolucion"))
            
            strEstado = ""
             
            With Item
            
                .SubItems(mconstListColFecha) = Format(rstDatos("Fecha"), "dd/MM/yyyy")
                .SubItems(mconstListColTotal) = Format(rstDatos("Total_Factura"), "0.00")
                .SubItems(mconstListColEstado) = strEstado
            
            End With
            
            rstDatos.MoveNext
        Wend
    
    End If
    
    Set rstDatos = Nothing

End Function

Private Function GetOrdenDevolucionItem(Numeros As String) As ADODB.Recordset

    Dim rstDatos As New ADODB.Recordset
    Dim strSql As String
    
    strSql = "Select * From Orden_Entrega_Item Where Id_Nro_Orden_Devolucion IN (" & CStr(Numeros) & ") "
    
    rstDatos.Open strSql, Db
    
    Set GetOrdenDevolucionItem = rstDatos
    
    Set rstDatos = Nothing
    
End Function


Private Sub LimiparControles()

    lvwLista.ListItems.Clear
    cboClientes.ListIndex = BusPosEnCombo(cboClientes, ClienteId)
    chkSoloPendientes.Value = Checked
    
    cboClientes.SetFocus

End Sub

Private Sub ConfirmarOperacion()

    Dim rstDatos As ADODB.Recordset
    Dim Item As ListItem
    Dim strNumeros As String
    
    Confirma = False
    
    For Each Item In lvwLista.ListItems
    
        If (Item.Checked) Then
        
            strNumeros = strNumeros & Replace(Item.Key, "K", "") & ","
        
        End If
    
    Next
    
    If Len(strNumeros) > 0 Then
    
        'Le borro la ultima coma
        strNumeros = Mid(strNumeros, 1, Len(strNumeros) - 1)
        
        If (optPresupuestos.Value) Then
        
            Set rstDatos = GetPresupuestoItem(strNumeros)
            
        End If
        
        If (optOrdenesEntrega.Value) Then
        
            Set rstDatos = GetOrdenEntregaItem(strNumeros)
        
        End If
        
        If (optOrdenesDevolucion.Value) Then
        
            Set rstDatos = GetOrdenDevolucionItem(strNumeros)
        
        End If
        
        If Not (rstDatos.EOF) Then
            Confirma = True
        End If
        
    End If
    
    Me.ResultItems = rstDatos
    Me.NumerosComprobantesSeleccionados = strNumeros
    
    Set rstDatos = Nothing
    
End Sub

