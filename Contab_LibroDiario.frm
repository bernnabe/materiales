VERSION 5.00
Begin VB.Form Contab_LibroDiario 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consultas al Libro Diario.-"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10335
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6735
   ScaleWidth      =   10335
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Fechas 
      Height          =   975
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   10335
      Begin VB.ComboBox Ejercicio 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   2895
      End
      Begin VB.TextBox Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8880
         MaxLength       =   10
         TabIndex        =   2
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Desde 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8880
         MaxLength       =   10
         TabIndex        =   1
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7560
         TabIndex        =   12
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7560
         TabIndex        =   11
         Top             =   240
         Width           =   1215
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Ejercicio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   10
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Cuentas 
      Enabled         =   0   'False
      Height          =   4815
      Left            =   0
      TabIndex        =   8
      Top             =   960
      Width           =   10335
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4335
         Left            =   120
         TabIndex        =   4
         Top             =   360
         Width           =   10095
      End
      Begin VB.Frame Frame1 
         Caption         =   "Frame1"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   120
         Width           =   10095
         Begin VB.CommandButton Command6 
            Caption         =   "Crιditos"
            Height          =   255
            Left            =   8280
            TabIndex        =   14
            Top             =   0
            Width           =   1815
         End
         Begin VB.CommandButton Command5 
            Caption         =   "Dιbitos"
            Height          =   255
            Left            =   6840
            TabIndex        =   15
            Top             =   0
            Width           =   1455
         End
         Begin VB.CommandButton Command4 
            Caption         =   "Denominaciσn"
            Height          =   255
            Left            =   2880
            TabIndex        =   16
            Top             =   0
            Width           =   3975
         End
         Begin VB.CommandButton Command3 
            Caption         =   "Cσd. Cuenta"
            Height          =   255
            Left            =   0
            TabIndex        =   17
            Top             =   0
            Width           =   2895
         End
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   9
      Top             =   5760
      Width           =   10335
      Begin VB.CommandButton Excel 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7680
         Picture         =   "Contab_LibroDiario.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   18
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   9120
         Picture         =   "Contab_LibroDiario.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   6600
         Picture         =   "Contab_LibroDiario.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Imprimir el Libro Diario.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Contab_LibroDiario.frx":C80E
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Confirma la Consulta al Libro Diario.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Contab_LibroDiario"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Confirma_Click()
    Dim Asiento As String
    
    If Val(Ejercicio.Text) > 0 And Val(Desde.Text) > 0 And Val(Hasta.Text) > 0 Then
        List1.Clear
        MousePointer = 11
        
        qy = "SELECT Asiento.Id_Asiento, Asiento.Fecha, Asiento.Detalle, Asiento_Item.Id_Linea, Asiento_Item.Id_Asiento, Asiento_Item.Nivel_1,Asiento_Item.Nivel_2,Asiento_Item.Nivel_3,Asiento_Item.Nivel_4, Asiento_Item.Nivel_5, Asiento_Item.Nivel_6, Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Cuenta.Denominacion, SUM(Debe) AS Total_Debe, SUM(Haber) AS Total_Haber "
        qy = qy & "FROM Asiento, Asiento_Item, Cuenta "
        qy = qy & "WHERE Asiento_Item.Nivel_1 = Cuenta.Id_Nivel_1 "
        qy = qy & "AND Asiento_Item.Nivel_2 = Cuenta.Id_Nivel_2 "
        qy = qy & "AND Asiento_Item.Nivel_3 = Cuenta.Id_Nivel_3 "
        qy = qy & "AND Asiento_Item.Nivel_4 = Cuenta.Id_Nivel_4 "
        qy = qy & "AND Asiento_Item.Nivel_5 = Cuenta.Id_Nivel_5 "
        qy = qy & "AND Asiento_Item.Nivel_6 = Cuenta.Id_Nivel_6 "
        qy = qy & "AND Asiento.Id_Asiento = Asiento_Item.Id_Asiento "
        qy = qy & "AND Asiento.Id_Ejercicio = " & Trim(Str(Val(Mid(Ejercicio.Text, 1, 4)))) & " "
        qy = qy & "AND Asiento.Id_Ejercicio = Asiento_Item.Id_Ejercicio "
        qy = qy & "AND Cuenta.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "AND Asiento.Id_Empresa = Asiento_Item.Id_Empresa "
        qy = qy & "AND Asiento_Item.Id_Empresa = Cuenta.Id_Empresa "
        qy = qy & "AND (Asiento.Fecha BETWEEN #" & Trim(Format(Desde.Text, "mm-dd-yy")) & "# AND #" & Trim(Format(Hasta.Text, "mm-dd-yy")) & "#) "
        qy = qy & "GROUP BY Asiento.Id_Asiento, Asiento.Fecha, Asiento.Detalle, Asiento_Item.Id_Linea, Asiento_Item.Id_Asiento, Asiento_Item.Nivel_1,Asiento_Item.Nivel_2,Asiento_Item.Nivel_3,Asiento_Item.Nivel_4, Asiento_Item.Nivel_5, Asiento_Item.Nivel_6, Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Cuenta.Denominacion "
        qy = qy & "ORDER BY Fecha, Asiento.Id_Asiento, Asiento_Item.Id_Linea "
        AbreRs
        
        If Not Rs.EOF Then
            Asiento = Val(Rs.Fields("Asiento.Id_Asiento"))
            List1.AddItem "ASIENTO: " & Formateado(Str(Val(Rs.Fields("Asiento.id_Asiento"))), 0, 10, " ", False) & "                                            FECHA DEL ASIENTO: " & Trim(Rs.Fields("Fecha"))
            List1.AddItem "CONCEPTO: " & Trim(Rs.Fields("Detalle"))
            List1.AddItem "................................................................................................"
        End If
        
        While Not Rs.EOF
            
            If Val(Rs.Fields("Asiento.Id_Asiento")) = Val(Asiento) Then
                Txt = Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")) + Space(26 - Len(Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")))) & " "
                Txt = Txt & Trim(Mid(Rs.Fields("Denominacion"), 1, 37)) + Space(37 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 37)))) & " "
                If Val(Rs.Fields("Total_Debe")) > 0 Then
                    Txt = Txt & Formateado(Str(Val(Rs.Fields("Total_Debe"))), 2, 13, " ", True) & " "
                Else
                    Txt = Txt & "              " & Formateado(Str(Val(Rs.Fields("Total_Haber"))), 2, 13, " ", True)
                End If
                
                List1.AddItem Txt
                Rs.MoveNext
            Else
                Asiento = Val(Rs.Fields("Asiento.Id_Asiento"))
                List1.AddItem ""
                List1.AddItem ""
                List1.AddItem "ASIENTO: " & Formateado(Str(Val(Rs.Fields("Asiento.id_Asiento"))), 0, 10, " ", False) & "                                            FECHA DEL ASIENTO: " & Trim(Rs.Fields("Fecha"))
                List1.AddItem "CONCEPTO: " & Trim(Rs.Fields("Detalle"))
                List1.AddItem "................................................................................................"
            End If
        Wend
        
        If List1.ListCount > 0 Then List1.AddItem ""
        MousePointer = 0
        
        Marco_Cuentas.Enabled = True
        If List1.ListCount > 0 Then Imprimir.Enabled = True
        List1.SetFocus
    Else
        Ejercicio.SetFocus
    End If
End Sub

Private Sub Desde_GotFocus()
    If Desde.Text = "" Then Desde.Text = Fecha_Fiscal
    Desde.SelStart = 0
    Desde.SelText = ""
    Desde.SelLength = 10
End Sub

Private Sub Desde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_LostFocus()
    Desde.Text = ValidarFecha(Desde.Text)
End Sub

Private Sub Ejercicio_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Ejercicio_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Libro Diario.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    qy = "SELECT * FROM Ejercicio WHERE Id_Empresa = " & Val(Id_Empresa) & " "
    qy = qy & "ORDER BY Periodo DESC"
    AbreRs
    
    While Not Rs.EOF
        Ejercicio.AddItem Trim(Str(Val(Rs.Fields("Id_Ejercicio")))) + Space(4 - Len(Trim(Str(Val(Rs.Fields("Id_Ejercicio")))))) & " " & Rs.Fields("Periodo") & " " & Trim(Rs.Fields("Cierre"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Hasta_GotFocus()
    If Hasta.Text = "" Then Hasta.Text = Fecha_Fiscal
    Hasta.SelStart = 0
    Hasta.SelText = ""
    Hasta.SelLength = 10
End Sub

Private Sub Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_LostFocus()
    Hasta.Text = ValidarFecha(Hasta.Text)
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
    
    If List1.ListCount > 0 Then
        i = 0
        l = 0
        
        Printer.Font = "Courier New"
        Printer.FontSize = 9
        
        Imprimir_Encabezado
        
        For i = 0 To List1.ListCount - 1
            If l <= 67 Then
                Printer.Print " " & Mid(List1.List(i), 1, 170)
                l = l + 1
            Else
                Printer.NewPage
                Imprimir_Encabezado
                l = 0
            End If
        Next
        
        Printer.Print " "
        Printer.EndDoc
    End If
    
    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Imprimir.Tag = Int(List1.ListCount / 67) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                   Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print "                                                                                  Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print "                                                                                  Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print
    Printer.Print
    Printer.Print
    Printer.Print "                                         LIBRO DIARIO (EJERCICIO: "; Trim(Ejercicio.Text); ")"
    Printer.Print
    Printer.Print " DESDE: "; Trim(Desde.Text)
    Printer.Print " HASTA: "; Trim(Hasta.Text)
    Printer.Print " "
    Printer.Print "     Cσd. Cuenta             Denominaciσn                                   Debe         Haber"
    Printer.Print " "
End Sub

Private Sub Salir_Click()
    If Ejercicio.Text = "" And Desde.Text = "" And Hasta.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    List1.Clear
    Desde.Text = ""
    Hasta.Text = ""
    Ejercicio.Text = ""
    
    Marco_Cuentas.Enabled = False
    Imprimir.Enabled = False
    
    Ejercicio.SetFocus
End Sub
