VERSION 5.00
Begin VB.Form Dat_Ctabcas 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaciσn de Cuentas Bancarias.-"
   ClientHeight    =   4095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6510
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4095
   ScaleWidth      =   6510
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   11
      Top             =   3120
      Width           =   6495
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   3960
         Picture         =   "Dat_Ctabcas.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Imprimir Lista.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   5280
         Picture         =   "Dat_Ctabcas.frx":5C12
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   1200
         Picture         =   "Dat_Ctabcas.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Borrar.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Dat_Ctabcas.frx":12126
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Grabar.-"
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Marco_Datos 
      Height          =   3135
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   6495
      Begin VB.TextBox Titular 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         MaxLength       =   30
         TabIndex        =   1
         Top             =   1080
         Width           =   4455
      End
      Begin VB.ComboBox Banco 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   2
         Top             =   1440
         Width           =   4455
      End
      Begin VB.ComboBox Tipos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   8
         Top             =   240
         Visible         =   0   'False
         Width           =   4455
      End
      Begin VB.TextBox Codigo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         MaxLength       =   10
         TabIndex        =   0
         Top             =   240
         Width           =   1335
      End
      Begin VB.CommandButton Buscar_Tipos 
         Caption         =   "&Numero de Cta:"
         Height          =   255
         Left            =   240
         TabIndex        =   9
         ToolTipText     =   "Buscar Registros existentes en la base de datos.-"
         Top             =   240
         Width           =   1455
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Titular:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   12
         Top             =   1080
         Width           =   1575
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Banco:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   360
         TabIndex        =   10
         Top             =   1440
         Width           =   1455
      End
   End
End
Attribute VB_Name = "Dat_Ctabcas"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Cuenta_Paginas As Single

Private Sub Banco_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Borrar_Click()
    If MsgBox("Desea Borrar ιste registro. ?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        qy = "DELETE FROM Cuentas_Banco WHERE Id_Cuenta = '" & Trim(Codigo.Text) & "' "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        
        Db.Execute (qy)
        
        Salir_Click
    Else
        Salir.SetFocus
    End If
End Sub

Private Sub Buscar_Tipos_Click()
    Tipos_Encontrados.Visible = True
    If Tipos_Encontrados.ListCount = 0 Then
        MousePointer = 11
        
        qy = "SELECT * FROM Cuentas_Banco, Banco "
        qy = qy & "WHERE Banco = Id_Banco "
        qy = qy & "ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Tipos_Encontrados.AddItem Trim(Rs.Fields("Id_Cuenta")) + Space(10 - Len(Trim(Rs.Fields("Id_Cuenta")))) & " " & Trim(Rs.Fields("Denominacion"))
            Rs.MoveNext
        Wend
        MousePointer = 0
    End If
    Tipos_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Codigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        If Trim(Codigo.Text) <> "" Then
            qy = "SELECT * FROM Cuentas_Banco, Banco WHERE Id_Cuenta = '" & Trim(Codigo.Text) & "' "
            qy = qy & "AND Banco = Id_Banco "
            qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
            AbreRs
            
            If Rs.EOF Then
                If MsgBox("El Registro es inexistente, desea darlo de alta.?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
                    Grabar.Tag = "Alta"
                    Grabar.Enabled = True
                    
                    Titular.SetFocus
                Else
                    Salir_Click
                End If
            Else
                Grabar.Tag = "Modificaciσn"
                Mostrar_Tipo
            End If
        Else
            Codigo.Text = ""
            Codigo.SetFocus
        End If
    End If
End Sub

Private Sub Borrar_Campo()
    Titular.Text = ""
    Codigo.Text = ""
    Banco.Text = ""
    
    Grabar.Enabled = False
    Borrar.Enabled = False
End Sub

Private Sub Mostrar_Tipo()
    Titular.Text = Rs.Fields("Titular")
    Banco.Text = Rs.Fields("Denominacion") + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Banco"))
    
    Grabar.Enabled = True
    Borrar.Enabled = True
    
    Banco.SetFocus
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualizaciσn de Cuentas Bancarias.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 3
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Bancos
End Sub

Private Sub Cargar_Bancos()
    qy = "SELECT * FROM Banco ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Banco.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Banco"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        
        qy = "INSERT INTO Cuentas_Banco VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", '" & Trim(Codigo.Text) & "'"
        qy = qy & ", '" & Trim(Titular.Text) & "'"
        qy = qy & ", " & Trim(Str(Val(Mid(Banco.Text, 31))))
        qy = qy & ", '')"
        Db.Execute (qy)
    Else
        
        qy = "UPDATE Cuentas_Banco SET "
        qy = qy & "Titular = '" & Trim(Titular.Text) & "', "
        qy = qy & "Banco = '" & Trim(Str(Val(Mid(Banco.Text, 31)))) & "' "
        qy = qy & "WHERE Id_Cuenta = '" & Trim(Codigo.Text) & "' "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        Db.Execute (qy)
    End If
    
    Salir_Click
End Sub

Private Sub Salir_Click()
    If Trim(Codigo.Text) = "" Then
        Unload Me
    Else
        
        Borrar_Campo
        Codigo.SetFocus
    End If
End Sub

Private Sub Tipos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Codigo.SetFocus
End Sub

Private Sub Tipos_Encontrados_LostFocus()
    Codigo.Text = Mid(Tipos_Encontrados.List(Tipos_Encontrados.ListIndex), 1, 10)
    Tipos_Encontrados.Visible = False
End Sub

Private Sub Imprimir_Click()
    If MsgBox("Confirma la impresiσn del listado de Rubros. ?.", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        Dim l As Long
        
        qy = "SELECT * FROM Cuentas_Banco, Banco "
        qy = qy & "WHERE Banco = Id_Banco "
        qy = qy & "ORDER BY Denominacion"
        AbreRs
        
        If Rs.RecordCount > 0 Then
            l = 0
            
            Printer.Font = "Courier New"
            Printer.FontSize = 9
            
            Imprimir_Encabezado
            
            While Not Rs.EOF
                
                If l <= 64 Then
                    Printer.Print "  " & Trim(Rs.Fields("Id_Cuenta")) + Space(10 - Len(Trim(Rs.Fields("Id_Cuenta")))) & " " & Trim(Rs.Fields("Denominacion"))
                    l = l + 1
                Else
                    Printer.Print " "
                    
                    l = 0
                    Printer.NewPage
                    Imprimir_Encabezado
                End If
                
                Rs.MoveNext
            Wend
            
            Printer.Print " "
            Printer.EndDoc
        End If
        
        Salir.SetFocus
    Else
        Codigo.SetFocus
    End If
End Sub

Private Sub Imprimir_Encabezado()
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                      Pαgina.: " & Printer.Page
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                   Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                             Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " e-mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print "                                LISTA DE CUENTAS BANCARIAS POR ORDEN ALFABΙTICO"
    Printer.Print
    Printer.Print " "
    Printer.Print "   Cσd. Denominaciσn "
    Printer.Print " "
End Sub

Private Sub Titular_GotFocus()
    Titular.SelStart = 0
    Titular.SelLength = Len(Titular.Text)
End Sub

Private Sub Titular_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Titular_LostFocus()
    Titular.Text = FiltroCaracter(Titular.Text)
End Sub
