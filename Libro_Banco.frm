VERSION 5.00
Begin VB.Form Libro_Banco 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Libro Banco.-"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9735
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   6735
   ScaleWidth      =   9735
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Captura 
      Height          =   2775
      Left            =   1800
      TabIndex        =   30
      Top             =   2040
      Visible         =   0   'False
      Width           =   6255
      Begin VB.CommandButton Terminar 
         Caption         =   "&Terminar"
         Height          =   375
         Left            =   5160
         TabIndex        =   12
         Top             =   2280
         Width           =   975
      End
      Begin VB.CommandButton Borrar 
         Caption         =   "&Borrar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   11
         Top             =   2280
         Width           =   975
      End
      Begin VB.CommandButton Confirma 
         Caption         =   "&Confirma"
         Enabled         =   0   'False
         Height          =   375
         Left            =   4200
         TabIndex        =   10
         Top             =   2280
         Width           =   975
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Cheque:"
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   960
         Width           =   735
      End
      Begin VB.ComboBox Cheque 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   960
         TabIndex        =   7
         Top             =   960
         Width           =   5175
      End
      Begin VB.TextBox Haber 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4800
         TabIndex        =   9
         Top             =   1800
         Width           =   1335
      End
      Begin VB.TextBox Debe 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4800
         TabIndex        =   8
         Top             =   1440
         Width           =   1335
      End
      Begin VB.TextBox Concepto 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         TabIndex        =   6
         Top             =   600
         Width           =   5175
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         TabIndex        =   5
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Cr�dito:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3960
         TabIndex        =   34
         Top             =   1800
         Width           =   735
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "D�bito:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3960
         TabIndex        =   33
         Top             =   1440
         Width           =   735
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Concepto:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   600
         Width           =   735
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Marco_Filtro 
      Height          =   1095
      Left            =   0
      TabIndex        =   15
      Top             =   0
      Width           =   9735
      Begin VB.ComboBox Cuenta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   2
         Top             =   600
         Width           =   8535
      End
      Begin VB.TextBox Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8280
         MaxLength       =   10
         TabIndex        =   1
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Desde 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   0
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7560
         TabIndex        =   19
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Lista 
      Enabled         =   0   'False
      Height          =   4695
      Left            =   0
      TabIndex        =   16
      Top             =   1080
      Width           =   9735
      Begin VB.Frame Barra_Titulo 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   120
         Width           =   9495
         Begin VB.CommandButton Command5 
            Caption         =   "Saldo"
            Height          =   255
            Left            =   7920
            TabIndex        =   29
            Top             =   0
            Width           =   1575
         End
         Begin VB.CommandButton Command4 
            Caption         =   "Cr�ditos"
            Height          =   255
            Left            =   6600
            TabIndex        =   28
            Top             =   0
            Width           =   1335
         End
         Begin VB.CommandButton Command3 
            Caption         =   "D�bitos"
            Height          =   255
            Left            =   5280
            TabIndex        =   27
            Top             =   0
            Width           =   1335
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Concepto"
            Height          =   255
            Left            =   1320
            TabIndex        =   26
            Top             =   0
            Width           =   3975
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Fecha"
            Height          =   255
            Left            =   0
            TabIndex        =   25
            Top             =   0
            Width           =   1335
         End
      End
      Begin VB.TextBox Saldo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7680
         MaxLength       =   15
         TabIndex        =   21
         Top             =   4320
         Width           =   1935
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3900
         Left            =   120
         TabIndex        =   20
         ToolTipText     =   "Haciendo un doble click en alg�n registo, puede modificarlo.-"
         Top             =   360
         Width           =   9495
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo de la Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5640
         TabIndex        =   22
         Top             =   4320
         Width           =   1935
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   17
      Top             =   5760
      Width           =   9735
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   8520
         Picture         =   "Libro_Banco.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   14
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   2760
         Picture         =   "Libro_Banco.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Imprimir Lista.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Nuevo_Mov 
         Height          =   615
         Left            =   1680
         Picture         =   "Libro_Banco.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Insertar NUEVO Movimiento.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Actualizar 
         Height          =   615
         Left            =   120
         Picture         =   "Libro_Banco.frx":11AAE
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Actualizar Libro Banco"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Libro_Banco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Actualizar_Click()
    Dim Debe    As Single
    Dim Haber   As Single
    Dim sSaldo  As Single
    
    List1.Clear
    
    If Trim(Cuenta.Text) <> "" Then
        qy = "SELECT Libro_Banco.*, Cuentas_Banco.* FROM Libro_Banco, Cuentas_Banco "
        qy = qy & "WHERE Libro_Banco.Cuenta_Banco = Cuentas_Banco.Id_Cuenta "
        'If Val(Desde.Text) > 0 And Val(Hasta.Text) > 0 Then Qy = Qy & "AND Fecha BETWEEN " & Format(Desde.Text, "mm/dd/yyyy") & " AND " & Format(Hasta.Text, "mm/dd/yyyy") & " "
        If Trim(Cuenta.Text) <> "" Then qy = qy & "AND Cuenta_Banco = '" & Trim(Mid(Cuenta.Text, 1, 10)) & "' "
        qy = qy & "AND Libro_Banco.Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Fecha " 'DESC"
        AbreRs
        
        While Not Rs.EOF
            Txt = Format(Rs.Fields("Fecha"), "dd/mm/yyyy") & " "
            Txt = Txt & Trim(Mid(Rs.Fields("Concepto"), 1, 32)) + Space(32 - Len(Trim(Mid(Rs.Fields("Concepto"), 1, 32)))) & " "
            If Val(Rs.Fields("Debe")) > 0 Then
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Debe"))), 2, 10, " ", False) & " "
            Else
                Txt = Txt & Space(10) & " "
            End If
            If Val(Rs.Fields("Haber")) > 0 Then
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Haber"))), 2, 10, " ", False) & " "
            Else
                Txt = Txt & Space(10) & " "
            End If
            
            Debe = Val(Debe) + Val(Rs.Fields("Debe"))
            Haber = Val(Haber) + Val(Rs.Fields("Haber"))
            sSaldo = Val(sSaldo) + (Val(Rs.Fields("Debe")) - Val(Rs.Fields("Haber")))
            
            Txt = Txt & Formateado(Str(Val(sSaldo)), 2, 10, " ", False) & " "
            Txt = Txt & "  " & Val(Rs.Fields("Id_Movimiento"))
            
            List1.AddItem Txt
            Rs.MoveNext
        Wend
        
        Marco_Lista.Enabled = True
        Saldo.Text = Formateado(Str(Val(Debe - Haber)), 2, 15, " ", True)
        
        Cargar_Cheque_Dispon
    Else
        MsgBox "Seleccione una cuenta bancaria.", vbCritical, "Atenci�n.!"
        Cuenta.SetFocus
    End If
End Sub

Private Sub Borrar_Click()
    If Val(Confirma.Tag) > 0 Then
        If MsgBox("Desea borrar �ste registro ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            qy = "DELETE FROM Libro_Banco "
            qy = qy & "WHERE Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
            qy = qy & "AND Id_Movimiento = " & Trim(Str(Val(Confirma.Tag)))
            Db.Execute (qy)
            
            If Trim(UCase(Mid(Concepto.Text, 1, 7))) = "CHEQUE:" Then
                If Val(Mid(Concepto.Text, 8, 10)) > 0 Then
                    qy = "UPDATE Cheque SET "
                    qy = qy & "Disponible = 1 "
                    qy = qy & "WHERE Id_Cheque = " & Val(Mid(Concepto.Text, 8, 10)) & " "
                    qy = qy & "AND Empresa = " & Val(Id_Empresa)
                    Db.Execute (qy)
                End If
            End If
            
            Actualizar_Click
        End If
        
        Terminar_Click
    End If
End Sub

Private Sub Cheque_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cheque_LostFocus()
    If Val(Cheque.Text) > 0 Then Concepto.Text = "CHEQUE: " & Trim(Val(Mid(Cheque.Text, 1, 10))) + Space(10 - Len(Trim(Val(Mid(Cheque.Text, 1, 10))))) & " " & Mid(Cheque.Text, 33, 20)
End Sub

Private Sub Concepto_GotFocus()
    Concepto.SelStart = 0
    Concepto.SelLength = Len(Concepto.Text)
End Sub

Private Sub Concepto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Concepto_LostFocus()
    Concepto.Text = FiltroCaracter(Concepto.Text)
End Sub

Private Sub Confirma_Click()
    Cheque_LostFocus
    
    If Val(Fecha.Text) > 0 Then
        If Val(Confirma.Tag) = 0 Then
            qy = "SELECT MAX(Id_Movimiento) FROM Libro_Banco WHERE Empresa = " & Trim(Str(Val(Id_Empresa)))
            AbreRs
            If Not IsNull(Rs.Fields(0)) = True Then
                Confirma.Tag = Val(Rs.Fields(0)) + 1
            Else
                Confirma.Tag = 1
            End If
            
            qy = "INSERT INTO Libro_Banco VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Str(Val(Confirma.Tag))
            qy = qy & ", '" & Trim(Mid(Cuenta.Text, 1, 10)) & "'"
            qy = qy & ", '" & Trim(Fecha.Text) & "'"
            qy = qy & ", '" & Trim(Concepto.Text) & "'"
            qy = qy & ", " & Trim(Str(Val(Debe.Text)))
            qy = qy & ", " & Trim(Str(Val(Haber.Text)))
            qy = qy & ", '')"
            Db.Execute (qy)
            
            If Val(Cheque.Text) > 0 Then
                qy = "UPDATE Cheque SET "
                qy = qy & "Disponible = '2' "
                qy = qy & "WHERE Cheque.Banco = " & Trim(Str(Val(Mid(Cheque.Text, 54))))
                qy = qy & "AND Cheque.Id_Cheque = " & Trim(Str(Val(Mid(Cheque.Text, 1, 10)))) & " "
                qy = qy & "AND Cheque.Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
                Db.Execute (qy)
            End If
        Else
        
            qy = "UPDATE Libro_Banco SET "
            qy = qy & "Fecha = '" & Trim(Fecha.Text) & "', "
            qy = qy & "Concepto = '" & Trim(Concepto.Text) & "', "
            qy = qy & "Debe = " & Trim(Str(Val(Debe.Text))) & ", "
            qy = qy & "Haber = " & Trim(Str(Val(Haber.Text))) & " "
            qy = qy & "WHERE Id_Movimiento = " & Trim(Str(Val(Confirma.Tag))) & " "
            qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
            Db.Execute (qy)
            
        End If
        Actualizar_Click
            
        Terminar_Click
    Else
        MsgBox "La fecha ha sido mal ingresada, verifique.!", vbCritical, "Atenci�n.!"
        Fecha.Text = ""
        Fecha.SetFocus
    End If
End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_LostFocus()
    Actualizar_Click
End Sub

Private Sub Debe_Change()
    If Val(Debe.Text) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Debe_GotFocus()
    If Val(Cheque.Text) > 0 And Val(Debe.Text) = 0 And Val(Haber.Text) = 0 Then
        Debe.Text = Mid(Cheque.Text, 22, 10)
    End If
    Debe.Text = Trim(Debe.Text)
    Debe.SelStart = 0
    Debe.SelLength = Len(Debe.Text)
End Sub

Private Sub Debe_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Debe_LostFocus()
    Debe.Text = Formateado(Str(Val(Debe.Text)), 2, 10, " ", False)
End Sub

Private Sub Desde_GotFocus()
    If Val(Desde.Text) = 0 Then Desde.Text = Fecha_Fiscal
    Desde.Text = Trim(Desde.Text)
    Desde.SelStart = 0
    Desde.SelLength = Len(Desde.Text)
End Sub

Private Sub Desde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_LostFocus()
    Desde.Text = ValidarFecha(Desde.Text)
End Sub

Private Sub Fecha_GotFocus()
    'If Val(Fecha.Text) = 0 Then Fecha.Text = Fecha_Fiscal
    Fecha.SelStart = 0
    Fecha.SelLength = Len(Fecha.Text)
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
    'If Fecha.Text = "error" Then
    '    MsgBox "Error al ingresar la fecha, verifique e ingrese nuevamente.-", vbCritical, "Atenci�n.!"
    '    Fecha.Text = ""
    '    'Fecha.SetFocus
    'End If
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 5
    Me.Left = (Screen.Width - Me.Width) / 2
    
    Cargar_Cuentas
End Sub

Private Sub Cargar_Cuentas()
    qy = "SELECT Cuentas_Banco.*, Banco.* FROM Cuentas_Banco, Banco "
    qy = qy & "WHERE Cuentas_Banco.Banco = Banco.Id_Banco "
    qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
    AbreRs
    
    While Not Rs.EOF
        Cuenta.AddItem Trim(Rs.Fields("Id_Cuenta")) + Space(10 - Len(Trim(Rs.Fields("Id_Cuenta")))) & " " & Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Titular"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Haber_Change()
    If Val(Haber.Text) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Haber_GotFocus()
    If Val(Cheque.Text) > 0 And Val(Haber.Text) = 0 And Val(Debe.Text) = 0 Then
        Haber.Text = Mid(Cheque.Text, 22, 10)
    End If
    
    Haber.Text = Trim(Haber.Text)
    Haber.SelStart = 0
    Haber.SelLength = Len(Haber.Text)
End Sub

Private Sub Haber_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Haber_LostFocus()
    Haber.Text = Formateado(Str(Val(Haber.Text)), 2, 10, " ", False)
End Sub

Private Sub Hasta_GotFocus()
    If Val(Hasta.Text) = 0 And Val(Desde.Text) > 0 Then Hasta.Text = DateAdd("d", 30, Desde.Text)
    Hasta.SelStart = 0
    Hasta.SelLength = Len(Hasta.Text)
End Sub

Private Sub Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_LostFocus()
    Hasta.Text = ValidarFecha(Hasta.Text)
End Sub

Private Sub List1_DblClick()
    If Val(Mid(List1.List(List1.ListIndex), 79)) > 0 Then
        Nuevo_Mov_Click
        
        Fecha.Text = Mid(List1.List(List1.ListIndex), 1, 10)
        Concepto.Text = Mid(List1.List(List1.ListIndex), 12, 32)
        Debe.Text = Mid(List1.List(List1.ListIndex), 45, 10)
        Haber.Text = Mid(List1.List(List1.ListIndex), 56, 10)
        
        Confirma.Tag = Val(Mid(List1.List(List1.ListIndex), 79))
        List1.Tag = List1.ListIndex
        Borrar.Enabled = True
    End If
End Sub

Private Sub Nuevo_Mov_Click()
    If Trim(Cuenta.Text) <> "" Then
        Marco_Filtro.Enabled = False
        Marco_Lista.Enabled = False
        Botonera.Enabled = False
        Marco_Captura.Visible = True
        Terminar.Cancel = True
        
        Cargar_Cheque_Dispon
        Borrar_Campo_Captura
    Else
        MsgBox "Seleccione una cuenta bancaria.", vbCritical, "Atenci�n.!"
        Cuenta.SetFocus
    End If
End Sub

Private Sub Salir_Click()
    If Desde.Text = "" And Hasta.Text = "" And Cuenta.Text = "" And List1.ListCount = 0 Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Desde.Text = ""
    Hasta.Text = ""
    List1.Clear
    Cuenta.Text = ""
    Marco_Lista.Enabled = False
    
    Desde.SetFocus
End Sub

Private Sub Terminar_Click()
    If Fecha.Text = "" Then
        Marco_Filtro.Enabled = True
        Marco_Captura.Visible = False
        Marco_Lista.Enabled = True
        Botonera.Enabled = True
        Salir.Cancel = True
        
        Actualizar_Click
        List1.SetFocus
    Else
        Borrar_Campo_Captura
    End If
End Sub

Private Sub Borrar_Campo_Captura()
    Confirma.Tag = ""
    Fecha.Text = ""
    Concepto.Text = ""
    Cheque.Text = ""
    Debe.Text = ""
    Haber.Text = ""
    Confirma.Enabled = False
    Borrar.Enabled = False
    
    Fecha.SetFocus
End Sub

Private Sub Cargar_Cheque_Dispon()
    Cheque.Clear
    
    qy = "SELECT Cheque.*, Banco.* FROM Cheque, Banco WHERE Disponible = 1 "
    qy = qy & "AND Banco.Id_Banco = Cheque.Banco "
    qy = qy & "AND Cheque.Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "ORDER BY Fecha_Cobrar "
    AbreRs
    
    While Not Rs.EOF
        Cheque.AddItem Trim(Rs.Fields("Id_Cheque")) + Space(10 - Len(Trim(Rs.Fields("Id_Cheque")))) & " " & Trim(Format(Rs.Fields("Fecha_Cobrar"), "dd/mm/yy")) & " $" & Formateado(Str(Val(Rs.Fields("Importe"))), 2, 10, " ", False) & " " & Trim(Mid(Rs.Fields("Denominacion"), 1, 20)) + Space(20 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 20)))) & " " & Val(Rs.Fields("Id_Banco"))
        Rs.MoveNext
    Wend
End Sub
