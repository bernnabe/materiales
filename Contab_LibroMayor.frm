VERSION 5.00
Begin VB.Form Contab_LibroMayor 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consultas al Libro Mayor.-"
   ClientHeight    =   7215
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11295
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7215
   ScaleWidth      =   11295
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Cuenta 
      Height          =   1455
      Left            =   0
      TabIndex        =   14
      Top             =   0
      Width           =   11295
      Begin VB.ComboBox Ejercicio 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         ItemData        =   "Contab_LibroMayor.frx":0000
         Left            =   1320
         List            =   "Contab_LibroMayor.frx":0002
         TabIndex        =   0
         Top             =   240
         Width           =   9855
      End
      Begin VB.ComboBox Hasta_Encontrada 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   27
         Top             =   1080
         Visible         =   0   'False
         Width           =   8415
      End
      Begin VB.ComboBox Desde_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         TabIndex        =   26
         Top             =   720
         Visible         =   0   'False
         Width           =   8415
      End
      Begin VB.CommandButton Busca_Hasta 
         Caption         =   "&Hasta Cta.:"
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   1080
         Width           =   1095
      End
      Begin VB.CommandButton Busca_Desde 
         Caption         =   "&Desde Cta.:"
         Height          =   255
         Left            =   120
         TabIndex        =   24
         Top             =   720
         Width           =   1095
      End
      Begin VB.TextBox Fecha_Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9840
         MaxLength       =   10
         TabIndex        =   9
         ToolTipText     =   "Fecha Final.-"
         Top             =   1080
         Width           =   1335
      End
      Begin VB.TextBox Fecha_Desde 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9840
         MaxLength       =   10
         TabIndex        =   8
         ToolTipText     =   "Fecha Inicial.-"
         Top             =   720
         Width           =   1335
      End
      Begin VB.TextBox Nombre_Hasta 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5040
         MaxLength       =   30
         TabIndex        =   23
         Top             =   1080
         Width           =   4695
      End
      Begin VB.TextBox Nombre_Desde 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5040
         TabIndex        =   22
         Top             =   720
         Width           =   4695
      End
      Begin VB.TextBox Hasta_Nivel_1 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   2
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_1 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   1
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_2 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   17
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_3 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   18
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_4 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   19
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_5 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         TabIndex        =   20
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox Desde_Nivel_6 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         TabIndex        =   21
         Top             =   720
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_2 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   3
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_3 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   4
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_4 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3120
         TabIndex        =   5
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_5 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         TabIndex        =   6
         Top             =   1080
         Width           =   615
      End
      Begin VB.TextBox Hasta_Nivel_6 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4320
         TabIndex        =   7
         Top             =   1080
         Width           =   615
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Ejercicio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Marco_Mayor 
      Enabled         =   0   'False
      Height          =   4815
      Left            =   0
      TabIndex        =   15
      Top             =   1440
      Width           =   11295
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4335
         Left            =   120
         TabIndex        =   11
         Top             =   360
         Width           =   11055
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   120
         Width           =   11055
         Begin VB.CommandButton Command7 
            Caption         =   "Saldo"
            Height          =   255
            Left            =   8760
            TabIndex        =   30
            Top             =   0
            Width           =   2295
         End
         Begin VB.CommandButton Command6 
            Caption         =   "Cr馘itos"
            Height          =   255
            Left            =   7440
            TabIndex        =   31
            Top             =   0
            Width           =   1335
         End
         Begin VB.CommandButton Command5 
            Caption         =   "D饕itos"
            Height          =   255
            Left            =   6120
            TabIndex        =   32
            Top             =   0
            Width           =   1335
         End
         Begin VB.CommandButton Detalle 
            Caption         =   "Detalle"
            Height          =   255
            Left            =   1560
            TabIndex        =   35
            Top             =   0
            Width           =   4575
         End
         Begin VB.CommandButton Command2 
            Caption         =   "Asient."
            Height          =   255
            Left            =   960
            TabIndex        =   33
            Top             =   0
            Width           =   615
         End
         Begin VB.CommandButton Command1 
            Caption         =   "Fecha"
            Height          =   255
            Left            =   0
            TabIndex        =   34
            Top             =   0
            Width           =   975
         End
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   16
      Top             =   6240
      Width           =   11295
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8640
         Picture         =   "Contab_LibroMayor.frx":0004
         Style           =   1  'Graphical
         TabIndex        =   36
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10080
         Picture         =   "Contab_LibroMayor.frx":0976
         Style           =   1  'Graphical
         TabIndex        =   13
         ToolTipText     =   "Cancelar Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7560
         Picture         =   "Contab_LibroMayor.frx":6C00
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Imprimir el Libro Mayor.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Contab_LibroMayor.frx":C812
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Confirma la Carga y consulta al Libro Mayor.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Contab_LibroMayor"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Inicial_Debe  As String
Dim Inicial_Haber As String
Dim Saldo_Cuentas As String
Dim Cuenta        As String

Private Sub Busca_Desde_Click()
    Desde_Encontradas.Visible = True
    MousePointer = 11
    If Desde_Encontradas.ListCount = 0 Then
        qy = "SELECT * FROM Cuenta "
        qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Desde_Encontradas.AddItem Trim(Rs.Fields("Denominacion")) + Space(35 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Nivel_1"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_2"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_3"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_4"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_5"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_6"))), 0, 4, " ", False)
            
            Rs.MoveNext
        Wend
    End If
    
    MousePointer = 0
    Desde_Encontradas.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Busca_Hasta_Click()
    Hasta_Encontrada.Visible = True
    MousePointer = 11
    If Hasta_Encontrada.ListCount = 0 Then
        qy = "SELECT * FROM Cuenta "
        qy = qy & "WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Hasta_Encontrada.AddItem Trim(Rs.Fields("Denominacion")) + Space(35 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Nivel_1"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_2"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_3"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_4"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_5"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_6"))), 0, 4, " ", False)
            
            Rs.MoveNext
        Wend
    End If
    
    MousePointer = 0
    Hasta_Encontrada.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Confirma_Click()
    Dim Asiento As Long
    
        MousePointer = 11
        List1.Clear
        Inicial_Debe = 0
        Inicial_Haber = 0
        
        qy = "SELECT Asiento.Id_Asiento, Asiento.Detalle, Asiento.Fecha, Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Cuenta.Denominacion, Saldo_Inicial.Saldo_Inicial, SUM(Debe) AS Debitos, SUM(Haber) AS Creditos, SUM (Saldo_Inicial.Saldo_Inicial) AS Saldo_Grabado "
        qy = qy & "FROM Asiento, Cuenta, Saldo_Inicial, Asiento_Item "
        qy = qy & "WHERE Asiento_Item.Nivel_1 = Cuenta.Id_Nivel_1 "
        qy = qy & "AND Asiento_Item.Nivel_1 = Saldo_Inicial.Id_Nivel_1 "
        qy = qy & "AND Asiento_Item.Nivel_2 = Cuenta.Id_Nivel_2 "
        qy = qy & "AND Asiento_Item.Nivel_2 = Saldo_Inicial.Id_Nivel_2 "
        qy = qy & "AND Asiento_Item.Nivel_3 = Cuenta.Id_Nivel_3 "
        qy = qy & "AND Asiento_Item.Nivel_3 = Saldo_Inicial.Id_Nivel_3 "
        qy = qy & "AND Asiento_Item.Nivel_4 = Cuenta.Id_Nivel_4 "
        qy = qy & "AND Asiento_Item.Nivel_4 = Saldo_Inicial.Id_Nivel_4 "
        qy = qy & "AND Asiento_Item.Nivel_5 = Cuenta.Id_Nivel_5 "
        qy = qy & "AND Cuenta.Id_Nivel_5 = Saldo_Inicial.Id_Nivel_5 "
        qy = qy & "AND Asiento_Item.Nivel_6 = Cuenta.Id_Nivel_6 "
        qy = qy & "AND Cuenta.Id_Nivel_6 = Saldo_Inicial.Id_Nivel_6 "
        qy = qy & "AND Asiento.Id_Asiento = Asiento_Item.Id_Asiento "
        qy = qy & "AND (Cuenta.Id_Nivel_1 BETWEEN " & Val(Desde_Nivel_1.Text) & " AND " & Val(Hasta_Nivel_1.Text) & ") "
        If Val(Hasta_Nivel_2.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_2 = " & Trim(Str(Val(Hasta_Nivel_2.Text))) & " "
        If Val(Hasta_Nivel_3.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_3 = " & Trim(Str(Val(Hasta_Nivel_3.Text))) & " "
        If Val(Hasta_Nivel_4.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_4 = " & Trim(Str(Val(Hasta_Nivel_4.Text))) & " "
        If Val(Hasta_Nivel_5.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_5 = " & Trim(Str(Val(Hasta_Nivel_5.Text))) & " "
        If Val(Hasta_Nivel_6.Text) > 0 Then qy = qy & "AND Cuenta.Id_Nivel_6 = " & Trim(Str(Val(Hasta_Nivel_6.Text))) & " "
        qy = qy & "AND (Fecha BETWEEN #" & Trim(Format(Fecha_Desde.Text, "mm-dd-yy")) & "# AND #" & Trim(Format(Fecha_Hasta.Text, "mm-dd-yy")) & "#) "
        qy = qy & "AND Cuenta.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "AND Cuenta.Id_Empresa = Asiento.Id_Empresa "
        qy = qy & "AND Asiento.Id_Empresa = Saldo_Inicial.Id_Empresa "
        qy = qy & "AND Asiento.Id_Empresa = Asiento_Item.Id_Empresa "
        qy = qy & "AND Asiento.Id_Ejercicio = Asiento_Item.Id_Ejercicio "
        qy = qy & "AND Saldo_Inicial.Ejercicio = Asiento.Id_Ejercicio "
        qy = qy & "AND Asiento.Id_Ejercicio = " & Val(Mid(Ejercicio.Text, 1, 4)) & " "
        qy = qy & "GROUP BY Asiento.Id_Asiento, Asiento.Detalle, Asiento.Fecha, Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Cuenta.Denominacion, Saldo_Inicial.Saldo_Inicial "
        qy = qy & "ORDER BY Cuenta.Denominacion, Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, Fecha"
        
        
        AbreRs
        
        If Not Rs.EOF Then
            Cuenta = Val(Rs.Fields("Id_Nivel_1") & Rs.Fields("Id_Nivel_2") & Rs.Fields("Id_Nivel_3") & Rs.Fields("Id_Nivel_4") & Rs.Fields("Id_Nivel_5") & Rs.Fields("Id_Nivel_6"))
            List1.AddItem "CUENTA: " & Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")) + Space(23 - Len(Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")))) & " " & Trim(Mid(Rs.Fields("Denominacion"), 1, 25))
            List1.AddItem "SALDO INICIAL: " & Formateado(Str(Val(Rs.Fields("Saldo_Grabado"))), 2, 15, " ", True)
            List1.AddItem "....................................................................................................."
            
            If Val(Rs.Fields("Saldo_Grabado")) < 0 Then
                Inicial_Haber = Val(Rs.Fields("Saldo_Grabado"))
            ElseIf Val(Rs.Fields("Saldo_Grabado")) > 0 Then
                Inicial_Debe = Val(Rs.Fields("Saldo_Grabado"))
            End If
        End If
        
        While Not Rs.EOF
            If Val(Rs.Fields("Id_Nivel_1") & Rs.Fields("Id_Nivel_2") & Rs.Fields("Id_Nivel_3") & Rs.Fields("Id_Nivel_4") & Rs.Fields("Id_Nivel_5") & Rs.Fields("Id_Nivel_6")) = Val(Cuenta) Then
                    
                    Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & " "
                    Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Asiento"))), 0, 5, " ", False) & " "
                    Txt = Txt & Trim(Rs.Fields("Detalle")) + Space(42 - Len(Trim(Rs.Fields("Detalle")))) & " "
                    Txt = Txt & IIf(Val(Rs.Fields("Debitos")) > 0, Formateado(Str(Val(Rs.Fields("Debitos"))), 2, 12, " ", True), "            ") & " "
                    Txt = Txt & IIf(Val(Rs.Fields("Creditos")) > 0, Formateado(Str(Val(Rs.Fields("Creditos"))), 2, 12, " ", True), "            ") & " "
                    
                    Inicial_Debe = Val(Inicial_Debe) + Val(Rs.Fields("Debitos"))
                    Inicial_Haber = Val(Inicial_Haber) + Val(Rs.Fields("Creditos"))
                    
                    If Val(Inicial_Debe - Inicial_Haber) < 0 Then
                        'Dim O As Long
        
                        Confirma.Tag = (Val(Inicial_Debe - Inicial_Haber)) * -1
                        Confirma.Tag = Formateado(Str(Val(Confirma.Tag)), 2, 0, " ", True)
                        Confirma.Tag = Trim(Confirma.Tag)
                        Txt = Txt & Space(17 - Len("(" & Confirma.Tag & ")")) & "(" & Trim(Confirma.Tag) & ")"
                        
                    Else
                        Txt = Txt & Formateado(Str(Val(Inicial_Debe - Inicial_Haber)), 2, 16, " ", True) & " "
                    End If
                        
                    
                    List1.AddItem Txt
                    Rs.MoveNext
            Else
                List1.AddItem "覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧�"
                List1.AddItem "TOTALES: " & Space(49) & Formateado(Str(Val(Inicial_Debe)), 2, 12, " ", True) & " " & Formateado(Str(Val(Inicial_Haber)), 2, 12, " ", True)
                List1.AddItem "覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧�"
                List1.AddItem "覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧�"
                List1.AddItem "CUENTA: " & Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")) + Space(23 - Len(Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")))) & " " & Trim(Mid(Rs.Fields("Denominacion"), 1, 25))
                List1.AddItem "SALDO INICIAL: " & Formateado(Str(Val(Rs.Fields("Saldo_Grabado"))), 2, 15, " ", True)
                List1.AddItem "....................................................................................................."
                
                Cuenta = Val(Rs.Fields("Id_Nivel_1") & Rs.Fields("Id_Nivel_2") & Rs.Fields("Id_Nivel_3") & Rs.Fields("Id_Nivel_4") & Rs.Fields("Id_Nivel_5") & Rs.Fields("Id_Nivel_6"))
                
                Inicial_Debe = 0
                Inicial_Haber = 0
                
                If Val(Rs.Fields("Saldo_Grabado")) < 0 Then
                    Inicial_Haber = Val(Rs.Fields("Saldo_Grabado"))
                ElseIf Val(Rs.Fields("Saldo_Grabado")) > 0 Then
                    Inicial_Debe = Val(Rs.Fields("Saldo_Grabado"))
                End If
                
            End If
        Wend
        
        List1.AddItem "覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧�"
        
        MousePointer = 0
        Marco_Mayor.Enabled = True
        If List1.ListCount > 0 Then Imprimir.Enabled = True
        List1.SetFocus
End Sub

Private Sub Leer_Saldo_Inicial()
'    If Not Rs.EOF Then
        Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & " S. I. " & Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")) + Space(16 - Len(Trim(Rs.Fields("Id_Nivel_1")) & "." & Trim(Rs.Fields("Id_Nivel_2")) & "." & Trim(Rs.Fields("Id_Nivel_3")) & "." & Trim(Rs.Fields("Id_Nivel_4")) & "." & Trim(Rs.Fields("Id_Nivel_5")) & "." & Trim(Rs.Fields("Id_Nivel_6")))) & " " & Trim(Mid(Rs.Fields("Denominacion"), 1, 25)) + Space(25 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 25)))) & " "

        If Rs.Fields("Saldo_Inicial") < 0 Then

            Inicial_Haber = Val(Rs.Fields("Saldo_Inicial")) * -1

            Txt = Txt & Formateado(Str(Val(0)), 2, 12, " ", True) & " " & Formateado(Str(Val(Inicial_Haber)), 2, 12, " ", True) & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Saldo_Inicial"))), 2, 13, " ", True)
            ElseIf Val(Rs.Fields("Saldo_Inicial")) > 0 Then

            Inicial_Debe = Val(Rs.Fields("Saldo_Inicial"))

            Txt = Txt & Formateado(Str(Val(Inicial_Debe)), 2, 12, " ", True) & " " & Formateado(Str(Val(0)), 2, 12, " ", True) & " "
            Txt = Txt & Formateado(Str(Val(Inicial_Debe)), 2, 13, " ", True)
        ElseIf Val(Rs.Fields("Saldo_Inicial")) = 0 Then
            Inicial_Debe = 0
            Inicial_Haber = 0

            Txt = Txt & Formateado(Str(Val(0)), 2, 12, " ", True) & " " & Formateado(Str(Val(0)), 2, 12, " ", True) & " "
            Txt = Txt & Formateado(Str(Val(0)), 2, 13, " ", True)
        End If

        Cuenta = Val(Rs.Fields("Id_Nivel_1") & Rs.Fields("Id_Nivel_2") & Rs.Fields("Id_Nivel_3") & Rs.Fields("Id_Nivel_4") & Rs.Fields("Id_Nivel_5") & Rs.Fields("Id_Nivel_6"))

        List1.AddItem Txt
    'End If
End Sub

Private Sub Desde_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Desde_Encontradas_LostFocus
End Sub

Private Sub Desde_Encontradas_LostFocus()
    Desde_Nivel_1.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 37, 4))
    Desde_Nivel_2.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 42, 4))
    Desde_Nivel_3.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 47, 4))
    Desde_Nivel_4.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 52, 4))
    Desde_Nivel_5.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 57, 4))
    Desde_Nivel_6.Text = Val(Mid(Desde_Encontradas.List(Desde_Encontradas.ListIndex), 62, 4))
    
    Desde_Encontradas.Visible = False
    Leer_Cuenta_Inicial
End Sub

Private Sub Desde_Nivel_1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Desde_Nivel_1_LostFocus
End Sub

Private Sub Desde_Nivel_1_LostFocus()
    If Val(Desde_Nivel_1.Text) > 0 Then
        Leer_Cuenta_Inicial
    End If
End Sub

Private Sub Leer_Cuenta_Inicial()
    If Val(Desde_Nivel_1.Text) > 0 Then
        
        qy = "SELECT * FROM Cuenta WHERE "
        qy = qy & "Id_Nivel_1 = " & Trim(Str(Val(Desde_Nivel_1.Text))) & " "
        qy = qy & "AND Id_Nivel_2 = " & Trim(Str(Val(Desde_Nivel_2.Text))) & " "
        qy = qy & "AND Id_Nivel_3 = " & Trim(Str(Val(Desde_Nivel_3.Text))) & " "
        qy = qy & "AND Id_Nivel_4 = " & Trim(Str(Val(Desde_Nivel_4.Text))) & " "
        qy = qy & "AND Id_Nivel_5 = " & Trim(Str(Val(Desde_Nivel_5.Text))) & " "
        qy = qy & "AND Id_Nivel_6 = " & Trim(Str(Val(Desde_Nivel_6.Text))) & " "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        AbreRs
        
        If Not Rs.EOF Then
            Nombre_Desde.Text = Rs.Fields("Denominacion")
            Hasta_Nivel_1.SetFocus
        Else
            MsgBox "La cuenta es inexistente...", vbInformation, "Atenci�n.!"
            Borrar_Campo
            Desde_Nivel_1.SetFocus
        End If
    Else
        Desde_Nivel_1.SetFocus
    End If
End Sub

Private Sub Borrar_Campo()
    Ejercicio.Text = ""
    Desde_Nivel_1.Text = ""
    Desde_Nivel_2.Text = ""
    Desde_Nivel_3.Text = ""
    Desde_Nivel_4.Text = ""
    Desde_Nivel_5.Text = ""
    Desde_Nivel_6.Text = ""
    Nombre_Desde.Text = ""
    Fecha_Desde.Text = ""
    Hasta_Nivel_1.Text = ""
    Hasta_Nivel_2.Text = ""
    Hasta_Nivel_3.Text = ""
    Hasta_Nivel_4.Text = ""
    Hasta_Nivel_5.Text = ""
    Hasta_Nivel_6.Text = ""
    Nombre_Hasta.Text = ""
    Fecha_Hasta.Text = ""
    
    List1.Clear
    Imprimir.Enabled = False
    Marco_Mayor.Enabled = False
End Sub

Private Sub Ejercicio_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_Desde_GotFocus()
    If Fecha_Desde.Text = "" Then Fecha_Desde.Text = Fecha_Fiscal
    
    Fecha_Desde.SelStart = 0
    Fecha_Desde.SelText = ""
    Fecha_Desde.SelLength = 10
End Sub

Private Sub Fecha_Desde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_Desde_LostFocus()
    Fecha_Desde.Text = ValidarFecha(Fecha_Desde.Text)
    
    If Fecha_Desde.Text = "error" Then Fecha_Desde_GotFocus
End Sub

Private Sub Fecha_Hasta_GotFocus()
    If Fecha_Hasta.Text = "" Then Fecha_Hasta.Text = Fecha_Fiscal
    
    Fecha_Hasta.SelStart = 0
    Fecha_Hasta.SelText = ""
    Fecha_Hasta.SelLength = 10
End Sub

Private Sub Fecha_Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_Hasta_LostFocus()
    Fecha_Hasta.Text = ValidarFecha(Fecha_Hasta.Text)
    
    If Fecha_Hasta.Text = "error" Then Fecha_Hasta_GotFocus
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Libro Mayor.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 15
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Ejercicio.Clear
    
    qy = "SELECT * FROM Ejercicio WHERE Id_Empresa = " & Val(Id_Empresa) & " "
    qy = qy & "ORDER BY Periodo DESC"
    AbreRs
    
    While Not Rs.EOF
        Ejercicio.AddItem Trim(Str(Val(Rs.Fields("Id_Ejercicio")))) + Space(4 - Len(Trim(Str(Val(Rs.Fields("Id_Ejercicio")))))) & " " & Rs.Fields("Periodo") & " " & Trim(Rs.Fields("Cierre"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Hasta_Encontrada_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Hasta_Encontrada_LostFocus
End Sub

Private Sub Hasta_Encontrada_LostFocus()
    Hasta_Nivel_1.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 37, 4))
    Hasta_Nivel_2.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 42, 4))
    Hasta_Nivel_3.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 47, 4))
    Hasta_Nivel_4.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 52, 4))
    Hasta_Nivel_5.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 57, 4))
    Hasta_Nivel_6.Text = Val(Mid(Hasta_Encontrada.List(Hasta_Encontrada.ListIndex), 62, 4))
    
    Hasta_Encontrada.Visible = False
    
    Hasta_Nivel_6_LostFocus
    Fecha_Desde.SetFocus
End Sub

Private Sub Hasta_Nivel_1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Leer_Cuenta_Final()
    If Val(Hasta_Nivel_1.Text) > 0 Then
        
        qy = "SELECT * FROM Cuenta WHERE "
        qy = qy & "Id_Nivel_1 = " & Trim(Str(Val(Hasta_Nivel_1.Text))) & " "
        qy = qy & "AND Id_Nivel_2 = " & Trim(Str(Val(Hasta_Nivel_2.Text))) & " "
        qy = qy & "AND Id_Nivel_3 = " & Trim(Str(Val(Hasta_Nivel_3.Text))) & " "
        qy = qy & "AND Id_Nivel_4 = " & Trim(Str(Val(Hasta_Nivel_4.Text))) & " "
        qy = qy & "AND Id_Nivel_5 = " & Trim(Str(Val(Hasta_Nivel_5.Text))) & " "
        qy = qy & "AND Id_Nivel_6 = " & Trim(Str(Val(Hasta_Nivel_6.Text))) & " "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        AbreRs
        
        If Not Rs.EOF Then
            Nombre_Hasta.Text = Rs.Fields("Denominacion")
            Fecha_Desde.SetFocus
        Else
            MsgBox "La cuenta es inexistente...", vbInformation, "Atenci�n.!"
            Borrar_Campo
            Desde_Nivel_1.SetFocus
        End If
    Else
        Hasta_Nivel_1.SetFocus
    End If
End Sub

Private Sub Hasta_Nivel_2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_3_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_4_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_5_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_6_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_Nivel_6_LostFocus()
    If Val(Hasta_Nivel_1.Text) > 0 Then
        Leer_Cuenta_Final
    End If
End Sub

Private Sub Salir_Click()
    If Desde_Nivel_1.Text = "" And Hasta_Nivel_1.Text = "" And Fecha_Desde.Text = "" And Fecha_Hasta.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
        Ejercicio.SetFocus
    End If
End Sub
