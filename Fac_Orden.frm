VERSION 5.00
Begin VB.Form Fac_Orden 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Orden de Retiro de Articulos"
   ClientHeight    =   7680
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11715
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7680
   ScaleWidth      =   11715
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Presupuestos_Encontrados 
      Height          =   5310
      Left            =   5625
      TabIndex        =   46
      Top             =   675
      Visible         =   0   'False
      Width           =   5850
      Begin VB.CommandButton Cancelar 
         Caption         =   "&Cancelar"
         Height          =   390
         Left            =   4725
         TabIndex        =   50
         Top             =   4800
         Width           =   990
      End
      Begin VB.CommandButton Aceptar 
         Caption         =   "&Aceptar"
         Default         =   -1  'True
         Height          =   390
         Left            =   75
         TabIndex        =   48
         Top             =   4800
         Width           =   990
      End
      Begin VB.ComboBox Presupuestos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4455
         Left            =   75
         Style           =   1  'Simple Combo
         TabIndex        =   47
         Top             =   300
         Width           =   5640
      End
   End
   Begin VB.Frame Marco_Captura 
      Height          =   3135
      Left            =   1920
      TabIndex        =   65
      Top             =   2160
      Visible         =   0   'False
      Width           =   7815
      Begin VB.TextBox Id_Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         TabIndex        =   49
         Top             =   240
         Width           =   4335
      End
      Begin VB.CommandButton Buscar_Articulos 
         Caption         =   "&Art�culo:"
         Height          =   255
         Left            =   240
         TabIndex        =   69
         TabStop         =   0   'False
         ToolTipText     =   "Buscar Art�culos en la Base de datos.-"
         Top             =   600
         Width           =   1095
      End
      Begin VB.ComboBox Presentacion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3720
         TabIndex        =   53
         Top             =   960
         Width           =   3015
      End
      Begin VB.TextBox Existencia 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   10
         TabIndex        =   68
         Top             =   1320
         Width           =   1215
      End
      Begin VB.TextBox Precio_Venta 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         MaxLength       =   10
         TabIndex        =   54
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Cantidad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3840
         MaxLength       =   10
         TabIndex        =   60
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Total 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         Locked          =   -1  'True
         MaxLength       =   10
         TabIndex        =   61
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Medida 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   10
         TabIndex        =   67
         Top             =   960
         Width           =   1215
      End
      Begin VB.TextBox Espesor 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   9
         TabIndex        =   56
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Ancho 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3840
         MaxLength       =   10
         TabIndex        =   57
         Top             =   2040
         Width           =   1335
      End
      Begin VB.TextBox Largo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         MaxLength       =   9
         TabIndex        =   58
         Top             =   2040
         Width           =   1335
      End
      Begin VB.ComboBox Articulos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2400
         TabIndex        =   66
         Top             =   600
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.TextBox Final_Unitario 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   240
         TabIndex        =   55
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Rec_Desc 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   10
         TabIndex        =   59
         Top             =   2640
         Width           =   1335
      End
      Begin VB.CommandButton Termina 
         Caption         =   "&Termina"
         Height          =   375
         Left            =   6840
         TabIndex        =   64
         ToolTipText     =   "Terminar o cancelar.-"
         Top             =   2640
         Width           =   855
      End
      Begin VB.CommandButton Borrar 
         Caption         =   "&Borra"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6840
         TabIndex        =   63
         ToolTipText     =   "Descargar el art�culo de la factura.-"
         Top             =   2280
         Width           =   855
      End
      Begin VB.CommandButton Confirma 
         Caption         =   "&Confirma"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6840
         TabIndex        =   62
         ToolTipText     =   "Cargar el Art�culo de la factura.-"
         Top             =   1920
         Width           =   855
      End
      Begin VB.TextBox Articulo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1560
         MaxLength       =   5
         TabIndex        =   51
         Top             =   600
         Width           =   735
      End
      Begin VB.TextBox Descripcion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2400
         MaxLength       =   30
         TabIndex        =   52
         Top             =   600
         Width           =   4335
      End
      Begin VB.Label Label31 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   81
         Top             =   240
         Width           =   2055
      End
      Begin VB.Label Label19 
         Alignment       =   1  'Right Justify
         Caption         =   "Existencia:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   960
         TabIndex        =   79
         Top             =   1320
         Width           =   1335
      End
      Begin VB.Label Label20 
         Alignment       =   2  'Center
         Caption         =   "Precio Vta. Unit."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   78
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label21 
         Alignment       =   2  'Center
         Caption         =   "Cantidad a Retirar"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   77
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label Label22 
         Alignment       =   2  'Center
         Caption         =   "Total"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5280
         TabIndex        =   76
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label Label23 
         Alignment       =   1  'Right Justify
         Caption         =   "Medida/Presentaci�n:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   75
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label26 
         Alignment       =   2  'Center
         Caption         =   "Espesor """
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2400
         TabIndex        =   74
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label27 
         Alignment       =   2  'Center
         Caption         =   "Ancho """
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3840
         TabIndex        =   73
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label28 
         Alignment       =   2  'Center
         Caption         =   "Largo M"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5280
         TabIndex        =   72
         Top             =   1800
         Width           =   1335
      End
      Begin VB.Label Label30 
         Alignment       =   2  'Center
         Caption         =   "P/Final Unitario"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   71
         Top             =   2400
         Width           =   1335
      End
      Begin VB.Label Label24 
         Alignment       =   2  'Center
         Caption         =   "% Rec +/ Desc -"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2400
         TabIndex        =   70
         Top             =   2400
         Width           =   1335
      End
   End
   Begin VB.Frame Marco_Factura 
      Height          =   1740
      Left            =   75
      TabIndex        =   13
      Top             =   0
      Width           =   11565
      Begin VB.CommandButton Buscar_Presupuesto 
         Caption         =   "&Numero:"
         Height          =   315
         Left            =   7425
         TabIndex        =   45
         Top             =   225
         Width           =   990
      End
      Begin VB.CommandButton Buscar_Clientes 
         Caption         =   "&Cliente:"
         Height          =   315
         Left            =   150
         TabIndex        =   16
         TabStop         =   0   'False
         ToolTipText     =   "Buscar Clientes en la base de datos.-"
         Top             =   600
         Width           =   915
      End
      Begin VB.TextBox Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1125
         MaxLength       =   5
         TabIndex        =   1
         Top             =   600
         Width           =   765
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         MaxLength       =   10
         TabIndex        =   0
         ToolTipText     =   "Fecha de Emisi�n.-"
         Top             =   225
         Width           =   1365
      End
      Begin VB.TextBox Direccion 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         MaxLength       =   30
         TabIndex        =   3
         Top             =   975
         Width           =   3765
      End
      Begin VB.ComboBox Localidad 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         TabIndex        =   4
         Top             =   1350
         Width           =   3765
      End
      Begin VB.TextBox Numero 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8550
         MaxLength       =   10
         TabIndex        =   15
         ToolTipText     =   "N�mero.-"
         Top             =   225
         Width           =   1815
      End
      Begin VB.ComboBox Condicion_Iva 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8550
         TabIndex        =   5
         Top             =   600
         Width           =   2940
      End
      Begin VB.TextBox Cuit 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8550
         MaxLength       =   13
         TabIndex        =   6
         Top             =   975
         Width           =   1740
      End
      Begin VB.TextBox Plazo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   8550
         MaxLength       =   4
         TabIndex        =   8
         ToolTipText     =   "Cantidad de d�as al Vencimiento del documento (ciendo cero, el documento es de contado).-"
         Top             =   1350
         Width           =   765
      End
      Begin VB.TextBox Vto 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10125
         MaxLength       =   10
         TabIndex        =   9
         Top             =   1350
         Width           =   1365
      End
      Begin VB.TextBox Tipo_Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   10950
         MaxLength       =   1
         TabIndex        =   7
         Top             =   975
         Width           =   540
      End
      Begin VB.ComboBox Clientes_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         TabIndex        =   14
         Top             =   600
         Visible         =   0   'False
         Width           =   3765
      End
      Begin VB.TextBox Nombre 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1950
         MaxLength       =   30
         TabIndex        =   2
         Top             =   600
         Width           =   3765
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1200
         TabIndex        =   24
         Top             =   240
         Width           =   615
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Direcci�n:"
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   150
         TabIndex        =   23
         Top             =   975
         Width           =   1740
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Localidad:"
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   150
         TabIndex        =   22
         Top             =   1350
         Width           =   1740
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Condici�n de Iva:"
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   6750
         TabIndex        =   21
         Top             =   600
         Width           =   1740
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "C.U.I.T.:"
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   6750
         TabIndex        =   20
         Top             =   975
         Width           =   1740
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Plazo:"
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   6750
         TabIndex        =   19
         Top             =   1350
         Width           =   1740
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Vto:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8400
         TabIndex        =   18
         Top             =   1320
         Width           =   615
      End
      Begin VB.Label Label25 
         Caption         =   "(P/R):"
         ForeColor       =   &H00800000&
         Height          =   315
         Left            =   10350
         TabIndex        =   17
         Top             =   975
         Width           =   540
      End
   End
   Begin VB.Frame Marco_Factura_Item 
      Enabled         =   0   'False
      Height          =   4365
      Left            =   75
      TabIndex        =   25
      Top             =   1650
      Width           =   11565
      Begin VB.CommandButton cmdLimpiarLista 
         Caption         =   "Limpiar Lista"
         Height          =   315
         Left            =   3075
         TabIndex        =   84
         Top             =   3900
         Width           =   1440
      End
      Begin VB.CommandButton btnImportar 
         Caption         =   "Importar Art�culos"
         Height          =   315
         Left            =   1575
         TabIndex        =   83
         Top             =   3900
         Width           =   1440
      End
      Begin VB.CommandButton Agregar 
         Caption         =   "Agregar Art�culos"
         Height          =   315
         Left            =   75
         TabIndex        =   82
         ToolTipText     =   "Comenzar a agregar art�culos a la factura.-"
         Top             =   3900
         Width           =   1440
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   3435
         Left            =   75
         TabIndex        =   26
         ToolTipText     =   "Haciendo un doble click sobre un item puede borrarlo o modificarlo.-"
         Top             =   375
         Width           =   11415
      End
      Begin VB.Label Label10 
         Caption         =   $"Fac_Orden.frx":0000
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   80
         Top             =   150
         Width           =   11295
      End
   End
   Begin VB.Frame Marco_Totales 
      Enabled         =   0   'False
      Height          =   765
      Left            =   75
      TabIndex        =   27
      Top             =   5925
      Width           =   11565
      Begin VB.TextBox Neto_Gravado 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   120
         MaxLength       =   9
         TabIndex        =   29
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Total_Factura 
         BackColor       =   &H00E0E0E0&
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9960
         MaxLength       =   10
         TabIndex        =   28
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox Exento 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   9
         TabIndex        =   30
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Sub_Total 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         MaxLength       =   9
         TabIndex        =   31
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Iva_Inscrip 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3720
         MaxLength       =   9
         TabIndex        =   32
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Sobre_Tasa 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4920
         MaxLength       =   9
         TabIndex        =   33
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Retencion_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6120
         MaxLength       =   9
         TabIndex        =   34
         Top             =   360
         Width           =   1215
      End
      Begin VB.TextBox Retencion_IIBB 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   7320
         MaxLength       =   9
         TabIndex        =   35
         Top             =   360
         Width           =   1215
      End
      Begin VB.Label Label11 
         Alignment       =   2  'Center
         Caption         =   "Neto Gravado"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   43
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label12 
         Alignment       =   2  'Center
         Caption         =   "Exento"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1320
         TabIndex        =   42
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label13 
         Alignment       =   2  'Center
         Caption         =   "Sub Total"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2520
         TabIndex        =   41
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label14 
         Alignment       =   2  'Center
         Caption         =   "IVA Resp. Ins."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   40
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label15 
         Alignment       =   2  'Center
         Caption         =   "Sobre Tasa"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4920
         TabIndex        =   39
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label16 
         Alignment       =   2  'Center
         Caption         =   "Retenc. IVA"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6120
         TabIndex        =   38
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label17 
         Alignment       =   2  'Center
         Caption         =   "Retenc. IIBB"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7320
         TabIndex        =   37
         Top             =   120
         Width           =   1215
      End
      Begin VB.Label Label18 
         Alignment       =   2  'Center
         Caption         =   "TOTAL"
         BeginProperty Font 
            Name            =   "MS Sans Serif"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   9960
         TabIndex        =   36
         Top             =   120
         Width           =   1455
      End
   End
   Begin VB.Frame Botonera 
      Height          =   990
      Left            =   75
      TabIndex        =   44
      Top             =   6675
      Width           =   11565
      Begin VB.CommandButton Borrar_Presupuesto 
         Enabled         =   0   'False
         Height          =   615
         Left            =   1200
         Picture         =   "Fac_Orden.frx":00CB
         Style           =   1  'Graphical
         TabIndex        =   11
         ToolTipText     =   "Borrar el Presupuesto de la Base de Datos.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Fac_Orden.frx":6355
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Grabar e imprimir la factura.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10350
         Picture         =   "Fac_Orden.frx":665F
         Style           =   1  'Graphical
         TabIndex        =   12
         ToolTipText     =   "Salir del m�dulo o cancelar.-"
         Top             =   225
         Width           =   1140
      End
   End
End
Attribute VB_Name = "Fac_Orden"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Private mvarPuedeActualizarPrecios As Boolean
Private mvarComprobantesAsociados As String

Private Sub btnImportar_Click()
    
    frmBuscadorComprobantes.TipoComprobante = OrdenEntrega
    frmBuscadorComprobantes.ClienteId = Val(Cliente.Text)
    frmBuscadorComprobantes.Show vbModal
    
    If (frmBuscadorComprobantes.Confirma) Then
        
        CargarArticulosImportados frmBuscadorComprobantes.ResultItems, frmBuscadorComprobantes.ActualizarPrecios
        mvarComprobantesAsociados = frmBuscadorComprobantes.NumerosComprobantesSeleccionados
        
        Numero.Tag = "Alta"
        Cancelar_Click
        Calcular_Total_Factura
        Borrar_Presupuesto.Enabled = True
        Agregar_Click
    
    End If

End Sub

Private Sub CargarArticulosImportados(rstDatos As ADODB.Recordset, ActualizarPrecios As Boolean)
    Dim objArticulo As New clsArticulo

    If Not (rstDatos.State = adStateClosed) Then

        mvarPuedeActualizarPrecios = True

        While Not rstDatos.EOF
            
             Txt = Formateado(Str(Val(rstDatos("Rubro"))), 0, 4, " ", False) & " "
             Txt = Txt & Formateado(Str(Val(rstDatos("Articulo"))), 0, 5, " ", False) & " "
             If Val(rstDatos("Presentacion")) = 3 Or Val(rstDatos("Presentacion")) = 4 Or Val(rstDatos("Presentacion")) = 5 Then
                 Txt = Txt & Trim(Mid(Trim(Mid(rstDatos("Descripcion"), 1, 28)) & "x(" & Trim(Val(rstDatos("Ancho"))) & "x" & Trim(Val(rstDatos("Largo"))) & ")", 1, 37)) + Space(37 - Len(Trim(Mid(Trim(Mid(rstDatos("Descripcion"), 1, 28)) & "x(" & Trim(Val(rstDatos("Ancho"))) & "x" & Trim(Val(rstDatos("Largo")) & ")"), 1, 37)))) & " "
             Else
                 Txt = Txt & Trim(rstDatos("Descripcion")) + Space(37 - Len(Trim(rstDatos("Descripcion")))) & " "
             End If
                         
             Txt = Txt & IIf(Val(Medida.Text) > 0, Formateado(Str(Val(Medida.Text)), 2, 7, " ", False) & " ", Space(8))
             
             If Val(rstDatos("Presentacion")) = 0 Then
                 Txt = Txt & Space(3) & " "
             ElseIf Val(rstDatos("Presentacion")) = 1 Then
                 Txt = Txt & "Us." & " "
             ElseIf Val(rstDatos("Presentacion")) = 2 Then
                 Txt = Txt & "Lts" & " "
             ElseIf Val(rstDatos("Presentacion")) = 3 Then
                 Txt = Txt & "P2 " & " "
             ElseIf Val(rstDatos("Presentacion")) = 4 Then
                 Txt = Txt & "M2 " & " "
             ElseIf Val(rstDatos("Presentacion")) = 5 Then
                 Txt = Txt & "Ml " & " "
             ElseIf Val(rstDatos("Presentacion")) = 6 Then
                 Txt = Txt & "Ml " & " "
             ElseIf Val(rstDatos("Presentacion")) = 7 Then
                 Txt = Txt & "Kgs" & " "
             ElseIf Val(rstDatos("Presentacion")) = 8 Then
                 Txt = Txt & "AxL" & " "
             End If
             
             Dim dblPrecioVenta As Double
             
             If (rstDatos("Rubro") > 0 And rstDatos("Articulo") > 0) And ActualizarPrecios Then
                dblPrecioVenta = objArticulo.GetPrecioVentaPorTipoCliente(rstDatos("Rubro"), rstDatos("Articulo"), Tipo_Cliente.Text, Plazo.Text)
             Else
                If (rstDatos("Precio_Vta") > 0) Then
                    dblPrecioVenta = rstDatos("Precio_Vta")
                Else
                    dblPrecioVenta = (rstDatos("Importe_Total") / rstDatos("Cantidad")) - rstDatos("Impuestos")
                End If
             End If
             
             If (ActualizarPrecios) Then
                    
                Dim dblImporteUnitario As Double
            
                dblImporteUnitario = objArticulo.GetPrecioArticulo(rstDatos("Rubro"), rstDatos("Articulo"), Tipo_Cliente.Text, Plazo.Text, rstDatos("Espesor"), rstDatos("Ancho"), rstDatos("Largo"))
                
                If Not Mid(Condicion_Iva.Text, 1, 2) = "RI" Then
                    dblImporteUnitario = ((dblImporteUnitario * Alicuota_Iva) / 100) + dblImporteUnitario
                End If
                
                Txt = Txt & Formateado(Str(Val(dblImporteUnitario)), 2, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(dblImporteUnitario)), 4, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(rstDatos("Cantidad"))), 2, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(dblImporteUnitario) * rstDatos("Cantidad")), 2, 10, " ", False) & "   "

             Else
             
                Txt = Txt & Formateado(Str(Val(rstDatos("Importe_Total") / rstDatos("Cantidad"))), 2, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(rstDatos("Importe_Total") / rstDatos("Cantidad"))), 4, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(rstDatos("Cantidad"))), 2, 10, " ", False) & " "
                Txt = Txt & Formateado(Str(Val(rstDatos("Importe_Total"))), 2, 10, " ", False) & "   "
             
             End If
             
             Txt = Txt & Formateado(Str(Val(0)), 2, 10, " ", False) & " " 'Existencia
             Txt = Txt & Formateado(Str(Val(rstDatos("Espesor"))), 2, 9, " ", False) & " "
             Txt = Txt & Formateado(Str(Val(rstDatos("Ancho"))), 2, 9, " ", False) & " "
             Txt = Txt & Formateado(Str(Val(rstDatos("Largo"))), 2, 9, " ", False) & " "
             Txt = Txt & Formateado(Str(Val(rstDatos("Precio_Vta"))), 4, 9, " ", False) & " "
             Txt = Txt & Formateado(Str(Val(rstDatos("Presentacion"))), 0, 1, " ", False) & " "
             Txt = Txt & Formateado(Str(Val(0)), 0, 10, " ", False)
             List1.AddItem Txt
             
             If (IsNull(rstDatos("Precio_Vta")) Or Val(rstDatos("Precio_Vta")) = 0) Then
                
                mvarPuedeActualizarPrecios = False
             
             End If
             
             rstDatos.MoveNext
        
         Wend
         
'         If Not mvarPuedeActualizarPrecios Then
'
'            MsgBox "La importaci�n se realiz� correctamente, pero no ser� posible corregir los valores de los items, si desea corregir alg�n importe deber� borrar el item y volver a cargarlo.", vbCritical, "Error en la importaci�n"
'
'         End If
         
    End If
     
End Sub

Private Sub Aceptar_Click()
       
    Dim strSql As String
    Dim rstDatos As New ADODB.Recordset
    
    If Val(Mid(Presupuestos_Encontrados.List(Presupuestos_Encontrados.ListIndex), 40, 10)) > 0 Then

        strSql = "SELECT * FROM Orden_Entrega "
        strSql = strSql & "WHERE Orden_Entrega.Id_Nro_Orden_Entrega = " & Trim(Str(Val(Mid(Presupuestos_Encontrados.List(Presupuestos_Encontrados.ListIndex), 40, 10)))) & " "
        strSql = strSql & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        rstDatos.Open strSql, Db

        Numero.Text = Formateado(Str(Val(rstDatos("Id_Nro_Orden_Entrega"))), 0, 10, "0", False)
        Fecha.Text = Format(rstDatos("Fecha"), "dd/MM/yyyy")
        Plazo.Text = Val(rstDatos("Plazo"))
        Vto.Text = DateAdd("d", Val(Plazo.Text), Trim(Fecha.Text))

        If Not IsNull(rstDatos("PresupuestoReferencia")) Then
            mvarComprobantesAsociados = rstDatos("PresupuestoReferencia")
        End If

        Cliente.Text = Trim(Str(Val(rstDatos("Cliente"))))
        Leer_Cliente
        
        rstDatos.Close

        strSql = "SELECT * FROM Orden_Entrega_Item p "
        strSql = strSql & "WHERE p.Id_Nro_Orden_Entrega = " & Trim(Str(Val(Mid(Presupuestos_Encontrados.List(Presupuestos_Encontrados.ListIndex), 40, 10)))) & " "
        strSql = strSql & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        strSql = strSql & "Order by Linea"
        rstDatos.Open strSql, Db
        
        If Not (rstDatos.EOF) Then
        
            Call CargarArticulosImportados(rstDatos, False)
        
        End If
        
    End If
    
    Numero.Tag = "Modificaci�n"
    Cancelar_Click
    Calcular_Total_Factura
    Borrar_Presupuesto.Enabled = True
    Agregar_Click
    
    Set rstDatos = Nothing
       
End Sub

Private Sub Agregar_Click()
    If Trim(Fecha.Text) <> "" And Trim(Mid(Condicion_Iva.Text, 1, 2)) <> "" And Cliente.Text <> "" And Tipo_Cliente.Text <> "error" Then
        Marco_Captura.Visible = True
        Marco_Captura.Enabled = True
        Marco_Factura_Item.Enabled = False
        Marco_Factura.Enabled = False
        Botonera.Enabled = False
        Termina.Cancel = True
        
        AbrirBusquedaArticulos
    Else
        Fecha.SetFocus
    End If
End Sub

Private Sub AbrirBusquedaArticulos()
    Load Fac_Vta_Bus
    Fac_Vta_Bus.Show vbModal
    
    If Fac_Vta_Bus.ArticuloSeleccionado > 0 And Fac_Vta_Bus.RubroSeleccionado > 0 Then
    
        Articulo.Text = Fac_Vta_Bus.ArticuloSeleccionado
        Id_Rubro.Text = Fac_Vta_Bus.RubroSeleccionado
        
        Id_Rubro.SetFocus
        SendKeys "{enter}"
        SendKeys "{tab}"
        SendKeys "{enter}"

    Else
        Id_Rubro.SetFocus
    End If
End Sub

Private Sub Cargar_Numero_Comprobante()
    If Val(Numero.Text) = 0 Then
        qy = "SELECT MAX(Id_Nro_Orden_Entrega) FROM Orden_Entrega "
        qy = qy & "WHERE Empresa = " & Str(Val(Id_Empresa))
        AbreRs
        
        Numero.Text = 1
        If Not Rs.Fields(0) = "" Then Numero.Text = Rs.Fields(0) + 1
        
        Numero.Text = Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
    End If
End Sub

Private Sub Ancho_GotFocus()
    Ancho.Text = Trim(Ancho.Text)
    Ancho.SelStart = 0
    Ancho.SelText = ""
    Ancho.SelLength = Len(Ancho.Text)
End Sub

Private Sub Ancho_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ancho_LostFocus()
    Ancho.Text = Formateado(Str(Val(Ancho.Text)), 2, 9, " ", False)
End Sub

Private Sub Articulo_GotFocus()
    Articulo.Text = Trim(Articulo.Text)
    Articulo.SelStart = 0
    Articulo.SelText = ""
    Articulo.SelLength = Len(Articulo.Text)
End Sub

Private Sub Articulo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo_LostFocus
End Sub

Private Sub Articulo_LostFocus()
    If Val(Articulo.Text) > 0 And Val(Mid(Id_Rubro.Text, 31)) > 0 Then
        Leer_Articulo
    ElseIf Trim(UCase(Id_Rubro.Text)) = "M" Then
        Id_Rubro.Text = "MANUAL"
        Descripcion.Enabled = True
        Presentacion.Enabled = True
        Precio_Venta.Enabled = True
        Precio_Venta.BackColor = vbWhite
        Final_Unitario.Enabled = True
        Final_Unitario.BackColor = vbWhite
        Cantidad.Enabled = True
        Rec_Desc.Enabled = True
        
        Descripcion.SetFocus
    End If
End Sub

Private Sub Leer_Articulo()
    If Val(Articulo.Text) > 0 And Val(Mid(Id_Rubro.Text, 31)) > 0 Then
        qy = "SELECT * FROM Articulo WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text)))
        qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Mid(Id_Rubro.Text, 31))))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "El Art�culo es inexistente...", vbInformation, "Atenci�n.!"
            Id_Rubro.Text = ""
            Articulo.Text = ""
            Id_Rubro.SetFocus
        Else
            Habilitar_Campos
            Mostrar_Articulo
        End If
    Else
        Articulo.Text = ""
        Id_Rubro.SetFocus
    End If
End Sub

Private Sub Mostrar_Articulo()
    Descripcion.Text = Rs.Fields("Descripcion")
    
    If Trim(UCase(Tipo_Cliente.Text)) = "P" Then
        Presentacion.ListIndex = Val(Rs.Fields("Presentacion_Pub"))
        If Val(Plazo.Text) = 0 Then
            Precio_Venta.Text = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Pub"))) / 100) + Val(Rs.Fields("Precio_Compra"))
        Else
            Precio_Venta.Text = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Pub"))) / 100) + Val(Rs.Fields("Precio_Compra"))
            Precio_Venta.Text = ((Val(Precio_Venta.Text) * Val(Rs.Fields("Rec_CtaCte_Pub"))) / 100) + Val(Precio_Venta)
        End If
    Else
        Presentacion.ListIndex = Val(Rs.Fields("Presentacion_Rev"))
        
        If Val(Plazo.Text) = 0 Then
            Precio_Venta.Text = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Rev"))) / 100) + Val(Rs.Fields("Precio_Compra"))
        Else
            Precio_Venta.Text = ((Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Rev"))) / 100) + Val(Rs.Fields("Precio_Compra"))
            Precio_Venta.Text = ((Val(Precio_Venta.Text) * Val(Rs.Fields("Rec_CtaCte_Rev"))) / 100) + Val(Precio_Venta)
        End If
    End If
    
    'Precio_Venta.Text = ((Val(Precio_Venta.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Venta.Text)
    Precio_Venta.Text = Formateado(Str(Val(Precio_Venta.Text)), 4, 10, " ", False)
    Precio_Venta.Tag = Val(Precio_Venta.Text)
    
    Existencia.Text = Formateado(Str(Val(Rs.Fields("Existencia"))), 2, 10, " ", False)
    
    If Val(Espesor.Text) = 0 And Val(Ancho.Text) = 0 And Val(Largo.Text) = 0 Then
        Espesor.Text = Formateado(Str(Val(Rs.Fields("Espesor"))), 2, 9, " ", False)
        Ancho.Text = Formateado(Str(Val(Rs.Fields("Ancho"))), 2, 9, " ", False)
        Largo.Text = Formateado(Str(Val(Rs.Fields("Largo"))), 2, 9, " ", False)
    End If
    
    HabilitarCamposMedida
    Mostrar_Medidas
    
    If Ancho.Enabled = True Then
        Ancho.SetFocus
    Else
        Rec_Desc.SetFocus
    End If
End Sub

Private Sub Mostrar_Medidas()
    If Presentacion.ListIndex = 4 Then
        Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
    Else
        If Presentacion.ListIndex = 5 Then
            Medida.Text = Largo.Text
        Else
            If Presentacion.ListIndex = 8 Then
                Medida.Text = Formateado(Str(Val(Ancho.Text * Largo.Text)), 2, 9, " ", False)
            Else
                Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
            End If
        End If
    End If
End Sub

Private Sub Articulos_Encontrados_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Borrar_Click()
    If Articulo.Text <> "" Then
        If MsgBox("Desea Remover el Art�culo de la Orden ?.", vbYesNo + vbInformation, "Atenci�n.!") = vbYes Then
            List1.RemoveItem Confirma.Tag
            List1.Refresh
        End If
        
        Calcular_Total_Factura
        Termina_Click
    End If
End Sub

Private Sub Borrar_Presupuesto_Click()
    If Val(Numero.Text) > 0 Then
        If MsgBox("Desea Eliminar definitivamente este presupuesto de la base de datos. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            
            'Primero Borro los item del presupuesto para no romper la integridad.-
            qy = "DELETE FROM Orden_Entrega_Item WHERE Id_Nro_Orden_Entrega = " & Trim(Str(Val(Numero.Text))) & " "
            qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
            Db.Execute (qy)
            
            qy = "DELETE FROM Orden_Entrega WHERE Id_Nro_Orden_Entrega = " & Trim(Str(Val(Numero.Text))) & " "
            qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
            Db.Execute (qy)
        End If
        
        Salir_Click
    Else
        Salir.SetFocus
    End If
End Sub


Private Sub Buscar_Articulos_Click()
    AbrirBusquedaArticulos
End Sub

Private Sub Articulos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Articulo.SetFocus
End Sub

Private Sub Articulos_Encontrados_LostFocus()
    Id_Rubro.Text = Trim(Mid(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 32, 4))
    Articulo.Text = Trim(Mid(Articulos_Encontrados.List(Articulos_Encontrados.ListIndex), 37))
    If Val(Articulo.Text) > 0 Then
        Leer_Rubro
        Leer_Articulo
    End If
    Articulos_Encontrados.Visible = False
End Sub

Private Sub Buscar_Clientes_Click()
    Clientes_Encontrados.Visible = True
    If Clientes_Encontrados.ListCount = 0 Then
        MousePointer = 11
        qy = "SELECT * FROM Cliente ORDER BY Nombre"
        AbreRs
        
        While Not Rs.EOF
            Clientes_Encontrados.AddItem Trim(Rs.Fields("Nombre")) + Space$(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields("Id_Cliente"))
            Rs.MoveNext
        Wend
        MousePointer = 0
    End If
    Clientes_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Buscar_Presupuesto_Click()
    qy = "SELECT Orden_Entrega.*,Cliente.* FROM Orden_Entrega,Cliente "
    qy = qy & "WHERE Orden_Entrega.Cliente = Cliente.Id_Cliente "
    If Val(Cliente.Text) > 0 Then qy = qy & "AND Orden_Entrega.Cliente = " & Trim(Str(Val(Cliente.Text))) & " "
    qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "ORDER BY Nombre, Fecha DESC"
    AbreRs
    
    While Not Rs.EOF
        Txt = Trim(Mid(Rs.Fields("Nombre"), 1, 27)) + Space$(27 - Len(Trim(Mid(Rs.Fields("Nombre"), 1, 27)))) & " "
        Txt = Txt & Trim(Format(Rs.Fields("Fecha"), "dd/mm/yyyy")) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Nro_Orden_Entrega"))), 0, 10, " ", False)
        
        Presupuestos_Encontrados.AddItem Txt
        Rs.MoveNext
    Wend
    
    Marco_Factura.Enabled = False
    Marco_Factura_Item.Enabled = False
    Botonera.Enabled = False
    Marco_Presupuestos_Encontrados.Visible = True
    Cancelar.Cancel = True
    Presupuestos_Encontrados.SetFocus
End Sub

Private Sub Cancelar_Click()
    Marco_Factura.Enabled = True
    Marco_Factura_Item.Enabled = True
    Botonera.Enabled = True
    Marco_Presupuestos_Encontrados.Visible = False
    Presupuestos_Encontrados.Clear
    Salir.Cancel = True
    Fecha.SetFocus
End Sub

Private Sub Cantidad_GotFocus()
    Cantidad.Text = Trim(Cantidad.Text)
    Cantidad.SelStart = 0
    Cantidad.SelText = ""
    Cantidad.SelLength = Len(Cantidad.Text)
End Sub

Private Sub Cantidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cantidad_LostFocus()
    If Val(Cantidad.Text) <= 999 Then
        If Val(Largo.Text) > 0 Then Largo_LostFocus
        
        Cantidad.Text = Formateado(Str(Val(Cantidad.Text)), 2, 10, " ", False)
        If Val(Cantidad.Text) > 0 Then
            Largo_LostFocus
            
            Medida.Text = Formateado(Str(Val(Val(Medida.Text) * Val(Cantidad.Text))), 2, 9, " ", False)
        End If
        
        Rec_Desc_GotFocus
        Rec_Desc_LostFocus
        
        Total_GotFocus
        Total_LostFocus
        
        If Confirma.Enabled = True Then Confirma.SetFocus
    Else
        MsgBox "La cantidad tiene que ser menor o igual a 999", vbCritical, "Atenci�n.!"
        Cantidad.Text = ""
        Cantidad.SetFocus
    End If
End Sub

Private Sub Cliente_GotFocus()
    Nombre.Enabled = False
    Direccion.Enabled = False
    Localidad.Enabled = False
    Condicion_Iva.Enabled = False
    'Tipo_Cliente.Enabled = False
    Cuit.Enabled = False
    
    Cliente.Text = Trim(Cliente.Text)
    Cliente.SelStart = 0
    Cliente.SelText = ""
    Cliente.SelLength = Len(Cliente.Text)
End Sub

Private Sub Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: If Val(Cliente.Text) > 0 Then Leer_Cliente
End Sub

Private Sub Cliente_LostFocus()
    If Val(Cliente.Text) > 0 Then
        Leer_Cliente
    ElseIf Trim(UCase(Cliente.Text)) = "M" Then
        Habilitar_Campo_Cliente
        Nombre.SetFocus
    End If
End Sub

Private Sub Habilitar_Campo_Cliente()
    Nombre.Enabled = True
    Direccion.Enabled = True
    Localidad.Enabled = True
    Condicion_Iva.Enabled = True
    Tipo_Cliente.Enabled = True
    Cuit.Enabled = True
End Sub

Private Sub Habilitar_Campos()
    Espesor.Enabled = True
    Largo.Enabled = True
    Rec_Desc.Enabled = True
    Cantidad.Enabled = True
End Sub

Private Sub Leer_Cliente()
    If Val(Cliente.Text) > 0 Then
        qy = "SELECT * FROM Cliente WHERE Id_Cliente = " & Trim(Str(Val(Cliente.Text)))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "El Cliente es inexistente...", vbInformation, "Atenci�n.!"
            Cliente.Text = ""
            Cliente.SetFocus
        Else
            Mostrar_Campo_Cliente
            If Tipo_Cliente.Enabled = True Then Tipo_Cliente.SetFocus
        End If
    Else
        Cliente.Text = ""
        Cliente.SetFocus
    End If
End Sub

Private Sub Mostrar_Campo_Cliente()
    Nombre.Text = Trim(Rs.Fields("Nombre"))
    Direccion.Text = Rs.Fields("Domicilio")
    Localidad.Text = Rs.Fields("Codigo_Postal")
    Condicion_Iva.Text = Rs.Fields("Condicion_Iva")
    Cuit.Text = Rs.Fields("Nro")
    
    If UCase(Condicion_Iva.Text) = "CF" Then
        Condicion_Iva.ListIndex = 0
    ElseIf UCase(Condicion_Iva.Text) = "RI" Then
        Condicion_Iva.ListIndex = 1
    ElseIf UCase(Condicion_Iva.Text) = "NI" Then
        Condicion_Iva.ListIndex = 2
    ElseIf UCase(Condicion_Iva.Text) = "MT" Then
        Condicion_Iva.ListIndex = 3
    ElseIf UCase(Condicion_Iva.Text) = "ET" Then
        Condicion_Iva.ListIndex = 4
    ElseIf UCase(Condicion_Iva.Text) = "NC" Then
        Condicion_Iva.ListIndex = 5
    End If
    
    If Val(Rs.Fields("Categoria")) = 1 Then
        Tipo_Cliente.Text = "R"
    ElseIf Val(Rs.Fields("Categoria")) = 2 Then
        Tipo_Cliente.Text = "P"
    End If
    
    'Busco la localidad
    If Val(Localidad.Text) > 0 Then
        qy = "SELECT * FROM Localidad WHERE Id_Localidad = " & Trim(Str(Val(Localidad.Text)))
        AbreRs
        
        If Not Rs.EOF Then
            Localidad.Text = Trim(Rs.Fields("Id_LocalidaD")) + Space$(5 - Len(Trim(Rs.Fields("Id_LocalidaD")))) & " " & Trim(Rs.Fields("Localidad"))
        End If
    End If
End Sub

Private Sub Clientes_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Clientes_Encontrados_LostFocus
End Sub

Private Sub Clientes_Encontrados_LostFocus()
    Cliente.Text = Mid(Clientes_Encontrados.List(Clientes_Encontrados.ListIndex), 32)
    Clientes_Encontrados.Visible = False
    Cliente.SetFocus
    Leer_Cliente
End Sub

Private Sub cmdLimpiarLista_Click()
    List1.Clear
    Calcular_Total_Factura
End Sub

Private Sub Condicion_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    If List1.ListCount <= 15 Then
        Dim Unidad As String
        
        Txt = Formateado(Str(Val(Mid(Id_Rubro.Text, 31))), 0, 4, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Articulo.Text)), 0, 5, " ", False) & " "
        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
            Txt = Txt & Trim(Mid(Trim(Mid(Descripcion.Text, 1, 28)) & "x" & Trim(Val(Ancho.Text)) & """x" & Trim(Val(Largo.Text)), 1, 37) & "m") + Space$(37 - Len(Trim(Mid(Trim(Mid(Descripcion.Text, 1, 28)) & "x" & Trim(Val(Ancho.Text)) & """x" & Trim(Val(Largo.Text)), 1, 37) & "m"))) & " "
        Else
            Txt = Txt & Trim(Descripcion.Text) + Space$(37 - Len(Trim(Descripcion.Text))) & " "
        End If
        Txt = Txt & IIf(Val(Medida.Text) > 0, Formateado(Str(Val(Medida.Text)), 2, 7, " ", False) & " ", Space$(8))
        
        If Val(Mid(Presentacion.Text, 1, 1)) = 0 Then
            Txt = Txt & Space$(3) & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 1 Then
            Txt = Txt & "Us." & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 2 Then
            Txt = Txt & "Lts" & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 3 Then
            Txt = Txt & "P2 " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 4 Then
            Txt = Txt & "M2 " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 5 Then
            Txt = Txt & "Ml " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 6 Then
            Txt = Txt & "Ml " & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 7 Then
            Txt = Txt & "Kgs" & " "
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 8 Then
            Txt = Txt & "AxL" & " "
        End If
        
        If UCase(Trim(Mid(Condicion_Iva.Text, 1, 2))) <> "RI" Then
            Final_Unitario.Text = (((Val(Final_Unitario.Text) * Val(Alicuota_Iva)) / 100) + Val(Final_Unitario.Text))
            Total.Text = ((Val(Cantidad.Text) * Val(Final_Unitario.Text)))
        End If

        Txt = Txt & Formateado(Str(Val(Final_Unitario.Text)), 2, 10, " ", False) & " "
            
        Txt = Txt & Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Cantidad.Text)), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Total.Text)), 2, 10, " ", False) & "   " '3 espacios porque empieza la carga de datos que no se ve.-
        
        Txt = Txt & Formateado(Str(Val(Existencia.Text)), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Espesor.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Ancho.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Largo.Text)), 2, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Precio_Venta.Text)), 4, 9, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Mid(Presentacion.Text, 1, 2))), 0, 1, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rec_Desc.Text)), 2, 10, " ", False) & " "
        
        If Confirma.Tag = "" Then
            List1.AddItem Txt
        Else
            List1.RemoveItem Val(Confirma.Tag)
            List1.Refresh
            List1.AddItem Txt, List1.Tag
        End If
        
        Calcular_Total_Factura
    Else
        MsgBox "No se pueden agregar m�s art�culos a �sta Orden de Entrega.!", vbCritical, "Atenci�n.!"
    End If
    
    Termina_Click
End Sub

Private Sub Calcular_Total_Factura()
    Dim i As Long
    
    i = 0
    Neto_Gravado.Text = 0
    Exento.Text = 0
    Sobre_Tasa.Text = 0
    Retencion_Iva.Text = 0
    Retencion_IIBB.Text = 0
    Iva_Inscrip.Text = 0
    Neto_Gravado.Text = 0
    Sub_Total.Text = 0
    Total_Factura.Text = ""
    
    For i = 0 To List1.ListCount - 1
        If Trim(UCase(Mid(Condicion_Iva.Text, 1, 2))) = "RI" Then
            Total_Factura.Tag = Val(Mid(List1.List(i), 73, 10))
            Total_Factura.Tag = ((Val(Total_Factura.Tag) * Val(Alicuota_Iva)) / 100) + Val(Total_Factura.Tag)
            Total_Factura.Tag = Val(Total_Factura.Tag) * Val(Mid(List1.List(i), 84, 10))
            
            Total_Factura.Text = Val(Total_Factura.Text) + Val(Total_Factura.Tag)
        Else
            Total_Factura.Tag = Val(Mid(List1.List(i), 95, 10)) / Val(Mid(List1.List(i), 84, 10))
            Total_Factura.Tag = Val(Total_Factura.Tag) / Val("1." & Alicuota_Iva)
            
            Total_Factura.Tag = ((Val(Total_Factura.Tag) * Val(Alicuota_Iva)) / 100) + Val(Total_Factura.Tag)
            Total_Factura.Tag = Val(Total_Factura.Tag) * Val(Mid(List1.List(i), 84, 10))
            
            Total_Factura.Text = Val(Total_Factura.Text) + Val(Total_Factura.Tag)
        End If
    Next
    Neto_Gravado.Text = Formateado(Str(Val(Val(Total_Factura.Text) / Val("1." & Alicuota_Iva))), 2, 9, " ", False)
    
    Iva_Inscrip.Text = (Val(Total_Factura.Text) - Val(Neto_Gravado.Text))
    Iva_Inscrip.Text = Formateado(Str(Val(Iva_Inscrip.Text)), 2, 9, " ", False)
    
    Sub_Total.Text = Val(Neto_Gravado.Text) + Val(Exento.Text)
    Sub_Total.Text = Formateado(Str(Val(Sub_Total.Text)), 2, 9, " ", False)
    
    Exento.Text = Formateado(Str(Val(Exento.Text)), 2, 9, " ", False)
    Sobre_Tasa.Text = Formateado(Str(Val(Sobre_Tasa.Text)), 2, 9, " ", False)
    Retencion_Iva.Text = Formateado(Str(Val(Retencion_Iva.Text)), 2, 9, " ", False)
    Retencion_IIBB.Text = Formateado(Str(Val(Retencion_IIBB.Text)), 2, 9, " ", False)
    
    Total_Factura.Text = Val(Sub_Total.Text) + Val(Iva_Inscrip.Text) + Val(Sobre_Tasa.Text) + Val(Retencion_Iva.Text) + Val(Retencion_IIBB.Text)
    Total_Factura.Text = Formateado(Str(Val(Total_Factura.Text)), 2, 10, " ", False)
End Sub

Private Sub Cuit_GotFocus()
    Cuit.Text = Trim(Cuit.Text)
    Cuit.SelStart = 0
    Cuit.SelText = ""
    Cuit.SelLength = Len(Cuit.Text)
End Sub

Private Sub Cuit_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuit_LostFocus()
    Cuit.Text = ValidarCuit(Cuit.Text)
    
    If Cuit.Text = "error" Then
        MsgBox "El Cuit Ingresado no es correcto, verifique e ingrese nuevamente.", vbInformation, "Atenci�n.!"
        Cuit.Text = ""
        Cuit.SetFocus
    End If
End Sub

Private Sub Descripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Descripcion_LostFocus()
    Descripcion.Text = FiltroCaracter(Descripcion.Text)
    Descripcion.Text = UCase(Descripcion.Text)
End Sub

Private Sub Direccion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Direccion_LostFocus()
    Direccion.Text = FiltroCaracter(Direccion.Text)
    Direccion.Text = UCase(Direccion.Text)
End Sub

Private Sub Espesor_GotFocus()
    Espesor.Text = Trim(Espesor.Text)
    Espesor.SelStart = 0
    Espesor.SelText = ""
    Espesor.SelLength = Len(Espesor.Text)
End Sub

Private Sub Espesor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Espesor_LostFocus()
    Espesor.Text = Formateado(Str(Val(Espesor.Text)), 2, 9, " ", False)
End Sub

Private Sub Fecha_GotFocus()
    Fecha.Text = Fecha_Fiscal
    Fecha.SelStart = 0
    Fecha.SelText = ""
    Fecha.SelLength = 10
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
    
    If Trim(Fecha.Text) = "error" Then
        MsgBox "La Fecha ingresada es incorrecta, ingrese nuevamente.", vbInformation, "Atenci�n.!"
        Fecha.SetFocus
    End If
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Emisi�n de Presupuestos.-"
End Sub

Private Sub Form_Load()
    Dim i As Long

    Me.Top = (Screen.Height - Me.Height) / 18
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    For i = 1 To 6
        Condicion_Iva.AddItem Tipo_Iva(i)
    Next
    
    For i = 0 To 8
        Presentacion.AddItem Articulo_Presentacion(i)
    Next
    
    'Cargar_Rubro
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    Dim i                 As Long
    Dim Rec_Rubro         As String
    Dim Rec_Articulo      As String
    Dim Rec_Descripcion   As String
    Dim Rec_Presentacion  As String
    Dim Rec_Cantidad      As String
    Dim Rec_Espesor       As String
    Dim Rec_Ancho         As String
    Dim Rec_Largo         As String
    Dim Rec_Precio        As String
    Dim Rec_Precio_Vta    As String
    Dim Rec_Impuesto      As String
    Dim Rec_Existencia    As String
    Dim Rec_Asiento       As String
    
    If Val(List1.ListCount) > 0 Then
        MousePointer = 11
        
        Cargar_Numero_Comprobante
        
        If Trim(UCase(Cliente.Text)) = "M" Then
            qy = "SELECT MAX(Id_Cliente) FROM Cliente"
            AbreRs
            
            Cliente.Text = 1
            If Not Rs.Fields(0) = "" Then Cliente.Text = Val(Rs.Fields(0)) + 1
            
            qy = "INSERT INTO Cliente VALUES ('"
            qy = qy & Trim(Str(Val(Cliente.Text))) & "'"
            qy = qy & ", '" & Trim(Nombre.Text) & "'"
            qy = qy & ", '" & Trim(Direccion.Text) & "'"
            qy = qy & ", '0', '0', ' '"
            qy = qy & ", '" & Trim(Str(Val(Localidad.Text))) & "'"
            qy = qy & ", ' ', ' '"
            qy = qy & ", '" & Trim(UCase(Mid(Condicion_Iva.Text, 1, 2))) & "'"
            qy = qy & ", '00'"
            qy = qy & ", '" & Trim(Cuit.Text) & "'"
            qy = qy & ", '00', '00'"
            qy = qy & ", " & IIf(Trim(Tipo_Cliente.Text) = "R", "1", "2")
            qy = qy & ", 0.00 "
            qy = qy & ", '" & Trim(Fecha.Text) & "'"
            qy = qy & ", ' ')"
            Db.Execute (qy)
            
            MsgBox "El Cliente ha sido registrado en la base de datos con el n�mero: " & Trim(Str(Val(Cliente.Text))), vbInformation, "Atenci�n.!"
        End If
    
        qy = "SELECT * FROM Orden_Entrega WHERE Id_Nro_Orden_Entrega = " & Trim(Str(Val(Numero.Text))) & " AND Empresa =  " & Trim(Str(Val(Id_Empresa)))
        AbreRs
        
        If Rs.EOF Then
            qy = "INSERT INTO Orden_Entrega VALUES("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Numero.Text)))
            qy = qy & ", " & Trim(Str(Val(Plazo.Text)))
            qy = qy & ", '" & Trim(Fecha.Text) & " " & Trim(Hora_Fiscal) & "'"
            qy = qy & ", " & Trim(Str(Val(Cliente.Text)))
            qy = qy & ", " & Trim(Str(Val(Neto_Gravado.Text)))
            qy = qy & ", 0,0, " & Trim(Str(Val(Exento.Text)))
            qy = qy & ", " & Trim(Str(Val(Sub_Total.Text)))
            qy = qy & ", " & Trim(Str(Val(Iva_Inscrip.Text)))
            qy = qy & ", 0, " & Trim(Str(Val(Retencion_Iva.Text)))
            qy = qy & ", " & Trim(Str(Val(Retencion_IIBB.Text)))
            qy = qy & ", " & Trim(Str(Val(Total_Factura.Text)))
            qy = qy & ", 0, '" & Trim(mvarComprobantesAsociados) & "')"
            Db.Execute (qy)
            
        Else
        
            qy = "UPDATE Orden_Entrega SET "
            qy = qy & "Gravado = " & Trim(Str(Val(Neto_Gravado.Text))) & ","
            qy = qy & "Exento = " & Trim(Str(Val(Exento.Text))) & ","
            qy = qy & "Subtotal = " & Trim(Str(Val(Sub_Total.Text))) & ","
            qy = qy & "Iva = " & Trim(Str(Val(Iva_Inscrip.Text))) & ","
            qy = qy & "Ret_iva = " & Trim(Str(Val(Retencion_Iva.Text))) & ","
            qy = qy & "Ret_IIBB = " & Trim(Str(Val(Retencion_IIBB.Text))) & ","
            qy = qy & "Total_Factura = " & Trim(Str(Val(Total_Factura.Text))) & " "
            qy = qy & "WHERE Id_Nro_Orden_Entrega = " & Trim(Str(Val(Numero.Text))) & " "
            qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
            Db.Execute (qy)
         
            qy = "UPDATE Articulo SET "
            qy = qy & "Existencia = Existencia + Orden_Entrega_Item.Cantidad "
            qy = qy & "FROM Orden_Entrega_Item "
            qy = qy & "INNER JOIN Articulo ON Articulo.Id_Articulo = Orden_Entrega_Item.Articulo  "
            qy = qy & "AND Articulo.Id_Rubro = Orden_Entrega_Item.Rubro  "
            qy = qy & "AND Orden_Entrega_Item.Empresa = " & Val(Id_Empresa) & " "
            qy = qy & "WHERE Id_Nro_Orden_Entrega = " & Trim(Str(Val(Numero.Text))) & " "
            Db.Execute (qy)
         
            qy = "DELETE FROM Orden_Entrega_Item WHERE Id_Nro_Orden_Entrega = " & Trim(Str(Val(Numero.Text))) & " "
            qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
            Db.Execute (qy)
         
        End If
        
        For i = 0 To List1.ListCount - 1
            
            Rec_Rubro = Val(Mid(List1.List(i), 1, 4))
            Rec_Articulo = Val(Mid(List1.List(i), 6, 5))
            Rec_Descripcion = Trim(Mid(List1.List(i), 12, 30))
            Rec_Presentacion = Trim(Mid(List1.List(i), 159, 1))
            Rec_Espesor = Mid(List1.List(i), 119, 9)
            Rec_Ancho = Mid(List1.List(i), 129, 9)
            Rec_Largo = Mid(List1.List(i), 139, 9)
            Rec_Precio_Vta = Mid(List1.List(i), 148, 10)
            Rec_Precio = Trim(Mid(List1.List(i), 73, 10))
            Rec_Impuesto = Val(Rec_Precio) / Val("1." & Val(Alicuota_Iva))
            Rec_Impuesto = (Val(Rec_Impuesto) * Val(Alicuota_Iva)) / 100
            Rec_Cantidad = Trim(Mid(List1.List(i), 84, 10))
            Rec_Existencia = Trim(Mid(List1.List(i), 98, 10))
            
            qy = "INSERT INTO Orden_Entrega_Item VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Numero.Text)))
            qy = qy & ", " & Trim(Str(Val(i)))
            qy = qy & ", " & Trim(Str(Val(Rec_Rubro)))
            qy = qy & ", " & Trim(Str(Val(Rec_Articulo)))
            qy = qy & ", '" & Trim(Rec_Descripcion) & "'"
            qy = qy & ", " & Trim(Str(Val(Rec_Presentacion)))
            qy = qy & ", " & IIf(Val(Rec_Espesor) > 0, Trim(Str(Val(Rec_Espesor))), 0)
            qy = qy & ", " & IIf(Val(Rec_Ancho) > 0, Trim(Str(Val(Rec_Ancho))), 0)
            qy = qy & ", " & IIf(Val(Rec_Largo) > 0, Trim(Str(Val(Rec_Largo))), 0)
            qy = qy & ", " & Trim(Str(Val(Rec_Cantidad)))
            qy = qy & ", " & Trim(Str(Val(Rec_Precio_Vta)))
            qy = qy & ", " & Trim(Str(Val(Rec_Impuesto)))
            qy = qy & ", " & (Val(Rec_Precio) * Val(Rec_Cantidad)) & ")"
            Db.Execute (qy)

            If Val(Rec_Articulo) > 0 Then
                
                qy = "UPDATE Articulo SET "
                qy = qy & "Existencia = Existencia - " & Trim(Str(Val(Rec_Cantidad))) & " "
                qy = qy & "WHERE Id_Articulo = " & Trim(Str(Val(Rec_Articulo))) & " "
                qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Rec_Rubro)))
                Db.Execute (qy)
                
            End If

        Next
        
        MousePointer = 0
        
        If MsgBox("La Orden de Entrega ha sigo registrada, desea imprimirla ahora. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            Imprimir
        End If
    End If
    
    Salir_Click
End Sub

Private Sub Imprimir()
    On Error GoTo Error_Impre
    
Inicia:
    Dim i As Long
    
    Close #1
    'Open "\\Cliente\epson" For Output As #1
    'Open "LPT1:" For Output As #1
    
    Open Trim(Puerto_MPto) For Output As #1
    
    Print #1, " "; Trim(cEmpresa) + Space$(30 - Len(Trim(cEmpresa))); "                   |      ORDEN NRO: "; Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
    Print #1, " Avda. Srgto. Cabral y los Medanos                |          FECHA: "; Trim(Fecha.Text)
    Print #1, " -------------------------------------------------+---------------------------"
    Print #1, " CLIENTE: "; Formateado(Str(Val(Cliente.Text)), 0, 5, " ", False); " - "; Trim(Nombre.Text) + Space$(30 - Len(Trim(Nombre.Text))); "C.U.I.T.: "; Trim(Cuit.Text)
    Print #1, " -----------------------------------------------------------------------------"
    Print #1, "    CODIGO DESCRIPCION                         CANTIDAD"
    Print #1, " -----------------------------------------------------------------------------"
    For i = 0 To List1.ListCount - 1
        Print #1, Mid(List1.List(i), 1, 10) & " " & Mid(List1.List(i), 12, 32) & " " & Mid(List1.List(i), 84, 10)
    Next
    For i = List1.ListCount To 12
        Print #1,
    Next
    Print #1, ""
    Print #1, "  .....................     ......................     ......................"
    Print #1, "      Autorizant�                  Entreg�                   Cliente"
    Print #1, " -----------------------------------------------------------------------------"
    'Print #1, Chr$(12);
    'Close #1
    
    'Open "\\Cliente\epson" For Output As #1
    'Open "LPT1:" For Output As #1
    
    'Open trim(Puerto_MPto) For Output As #1
    
    Dim k As Long
    
    For k = 0 To 1
        Print #1, " "; Trim(cEmpresa) + Space$(30 - Len(Trim(cEmpresa))); "                   |      ORDEN NRO: "; Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
        Print #1, " Avda. Srgto. Cabral y los Medanos                |          FECHA: "; Trim(Fecha.Text)
        Print #1, " -------------------------------------------------+---------------------------"
        Print #1, " CLIENTE: "; Formateado(Str(Val(Cliente.Text)), 0, 5, " ", False); " - "; Trim(Nombre.Text) + Space$(30 - Len(Trim(Nombre.Text))); "C.U.I.T.: "; Trim(Cuit.Text)
        Print #1, " -----------------------------------------------------------------------------"
        Print #1, "    CODIGO DESCRIPCION                         CANTIDAD"
        Print #1, " -----------------------------------------------------------------------------"
        
        For i = 0 To List1.ListCount - 1
            Print #1, Mid(List1.List(i), 1, 10) & " " & Mid(List1.List(i), 12, 32) & " " & Mid(List1.List(i), 84, 10)
        Next
        For i = List1.ListCount To 12
            Print #1,
        Next
        
        If k = 0 Then
            Print #1, "  .....................     ......................     ......................"
            Print #1, "      Autorizant�                  Entreg�                   Cliente"
        End If
        
        Print #1, " -----------------------------------------------------------------------------"
    Next
    
    Print #1, Chr$(12);
    Close #1
    
    Exit Sub
Error_Impre:
    If MsgBox("Error en la impresora, que desea hacer. ?", vbQuestion + vbRetryCancel, "Atenci�n.!") = vbRetry Then
        Resume Inicia
    End If
End Sub

Private Sub Largo_GotFocus()
    Largo.Text = Trim(Largo.Text)
    Largo.SelStart = 0
    Largo.SelText = ""
    Largo.SelLength = Len(Largo.Text)
End Sub

Private Sub Largo_KeyPress(KeyAscii As Integer)
     If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Largo_LostFocus()
    Largo.Text = Formateado(Str(Val(Largo.Text)), 2, 9, " ", False)
    
    If Val(Largo.Text) > 0 Then
        Mostrar_Medidas
        If Val(Mid(Presentacion.Text, 1, 1)) = 3 Then 'P2
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, False)
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 4 Then
            Medida.Text = CalcMed(Espesor.Text, Ancho.Text, Largo.Text, True)
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 5 Then 'ML
            Medida.Text = Largo.Text & " " & Trim(Mid(Presentacion.Text, 6))
            Final_Unitario.Text = (((Val(Espesor.Text) * Val(Ancho.Text)) * 0.2734) * Val(Largo)) * Val(Precio_Venta.Text)
        ElseIf Val(Mid(Presentacion.Text, 1, 1)) = 8 Then 'ML
            Medida.Text = Formateado(Str(Val(Largo.Text * Ancho.Text)), 2, 9, " ", False)
        End If
    End If
    
    If Val(Mid(Presentacion.Text, 1, 1)) = 3 Or Val(Mid(Presentacion.Text, 1, 1)) = 4 Or Val(Mid(Presentacion.Text, 1, 1)) = 8 Then
        Final_Unitario.Text = Val(Precio_Venta.Text) * Val(Medida.Text)
    End If
    Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
    Final_Unitario.Tag = Val(Final_Unitario.Text)
    
    '/// ATENCI�N SE REALIZA UN REMARQUE EN LOS ART�CULOS EN ESTE PROCEDIMIENTO!!
    Aderir_Recargos
        
    Final_Unitario.Tag = Val(Final_Unitario.Text)
    Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
End Sub

Private Sub List1_DblClick()
    If Trim(Mid(List1.List(List1.ListIndex), 1, 5)) <> "" Then
        Agregar_Click
        
        If Trim(Str(Val(Mid(List1.List(List1.ListIndex), 1, 5)))) > 0 Then
            Id_Rubro.Text = Mid(List1.List(List1.ListIndex), 1, 4)
            Articulo.Text = Mid(List1.List(List1.ListIndex), 6, 5)
            Leer_Rubro
            Leer_Articulo
        Else
            Id_Rubro.Text = "M"
            Descripcion.Text = Trim(Mid(List1.List(List1.ListIndex), 12, 31))
            Presentacion.ListIndex = Str(Val(Mid(List1.List(List1.ListIndex), 150, 1)))
            Descripcion.Enabled = True
            Habilitar_Campos
            Descripcion.SetFocus
        End If
        
        Rec_Desc.Text = Mid(List1.List(List1.ListIndex), 161, 10)
        Precio_Venta.Text = Mid(List1.List(List1.ListIndex), 148, 10)
        Cantidad.Text = Mid(List1.List(List1.ListIndex), 84, 10)
        Espesor.Text = Mid(List1.List(List1.ListIndex), 119, 9)
        Ancho.Text = Mid(List1.List(List1.ListIndex), 129, 9)
        Largo.Text = Mid(List1.List(List1.ListIndex), 139, 9)
        List1.Tag = Val(List1.ListIndex)
        
        If Largo.Enabled = True Then
            Largo_LostFocus
        Else
            CalcularPrecioVenta
        End If
        
        Rec_Desc_LostFocus
        Cantidad_LostFocus
        Total_GotFocus
        Total_LostFocus
        
        Borrar.Enabled = True
        Confirma.Tag = List1.ListIndex
    End If
End Sub

Private Sub Localidad_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Localidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_LostFocus()
    Nombre.Text = FiltroCaracter(Nombre.Text)
    Nombre.Text = UCase(Nombre.Text)
End Sub

Private Sub Plazo_GotFocus()
    If Trim(Plazo.Text) = "" Or Val(Plazo.Text) = 0 Then Plazo.Text = "C"
    
    Plazo.Text = Trim(Plazo.Text)
    Plazo.SelStart = 0
    Plazo.SelText = ""
    Plazo.SelLength = Len(Plazo.Text)
End Sub

Private Sub Plazo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Plazo_LostFocus()
    'Plazo.Text = Val(Plazo.Text)
    
    If Val(Plazo.Text) > 0 Then
        Vto.Text = DateAdd("d", Plazo.Text, Fecha.Text)
        Agregar_Click
    ElseIf Trim(UCase(Plazo.Text)) = "C" Then
        Plazo.Text = 0
        Vto.Text = Fecha.Text
        Agregar_Click
    Else
        MsgBox "Debe ingresar 'C' para especificar que el PRESUPUESTO es de contado.-", vbInformation, "Atenci�n.!"
        Plazo.Text = ""
        Plazo.SetFocus
    End If
End Sub

Private Sub Presentacion_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Presentacion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub HabilitarCamposMedida()
    If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Or Presentacion.ListIndex = 8 Then
        Precio_Venta.BackColor = 14737632
        Precio_Venta.Enabled = False
        Largo.Enabled = True
        Largo.BackColor = vbWhite
        Ancho.Enabled = True
        Ancho.BackColor = vbWhite
        'Precio_Unidad.Enabled = True
        If Trim(UCase(Id_Rubro.Text)) = "M" Then
            Precio_Venta.Enabled = True
            Precio_Venta.BackColor = vbWhite
            If Presentacion.ListIndex <> 8 Then
                Espesor.BackColor = vbWhite
                Espesor.Enabled = True
            End If
            
            Precio_Venta.SetFocus
        End If
    Else
        Precio_Venta.BackColor = vbWhite
        Precio_Venta.Enabled = True
        Largo.Enabled = False
        Largo.BackColor = 14737632
        Ancho.BackColor = 14737632
        Ancho.Enabled = False
        Espesor.Enabled = False
        Espesor.BackColor = 14737632
        'Precio_Unidad.Enabled = False
    End If
End Sub

Private Sub Presentacion_LostFocus()

    HabilitarCamposMedida

End Sub

Private Sub Presupuestos_Encontrados_DblClick()
    Aceptar_Click
End Sub

Private Sub Rec_Desc_GotFocus()
    'If Val(Rec_Desc.Text) > 0 Then
        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
            Largo_LostFocus
        Else
            CalcularPrecioVenta
        End If
    'End If
    Rec_Desc.Text = Trim(Rec_Desc.Text)
    Rec_Desc.SelStart = 0
    Rec_Desc.SelText = ""
    Rec_Desc.SelLength = Len(Rec_Desc.Text)
End Sub

Private Sub Rec_Desc_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rec_Desc_LostFocus()
    Dim Valor As Single
    
    Valor = Val(Final_Unitario.Tag) / Val("1." & Val(Alicuota_Iva))
    
    If Val(Final_Unitario.Tag) = 0 Then
        Valor = Val(Final_Unitario.Text) / Val("1." & Val(Alicuota_Iva))
        Final_Unitario.Tag = Valor
    End If
    
    'Valor = ((Val(Final_Unitario.Text) * Val(Alicuota_Iva)) / 100) + Val(Final_Unitario.Text)
    
    If Val(Rec_Desc.Text) <> 0 Then
        If Val(Rec_Desc.Text) > 0 Then
            Final_Unitario.Text = ((Val(Valor) * Val(Rec_Desc.Text)) / 100) + Val(Valor)
        ElseIf Val(Rec_Desc.Text) < 0 Then
            Final_Unitario.Text = Val(Valor) - ((Val(Valor) * (Val(Rec_Desc.Text) * -1)) / 100)
        End If
        
        Final_Unitario.Text = ((Val(Final_Unitario.Text) * Val(Alicuota_Iva)) / 100) + Val(Final_Unitario.Text)
        Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
    ElseIf Val(Rec_Desc.Text) = 0 Then
'        If Presentacion.ListIndex = 3 Or Presentacion.ListIndex = 4 Or Presentacion.ListIndex = 5 Then
'            Largo_LostFocus
'        Else
'            CalcularPrecioVenta
'        End If
    End If
    
    Rec_Desc.Text = Formateado(Str(Val(Rec_Desc.Text)), 2, 10, " ", False)
End Sub

Private Sub Salir_Click()
    If Cliente.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Grabar.Enabled = False
    Borrar_Presupuesto.Enabled = False
    Marco_Factura_Item.Enabled = False
    Marco_Factura.Enabled = True
    Cliente.Text = ""
    Fecha.Text = ""
    Vto.Text = ""
    Cuit.Text = ""
    Nombre.Text = ""
    List1.Clear
    Condicion_Iva.Text = ""
    Numero.Text = ""
    Direccion.Text = ""
    Localidad.Text = ""
    Plazo.Text = ""
    Tipo_Cliente.Text = ""
    Vto.Text = ""
    
    
    Borrar_Campo_Importe
    Fecha.SetFocus
End Sub

Private Sub Borrar_Campo_Importe()
    Neto_Gravado.Text = ""
    Exento.Text = ""
    Iva_Inscrip.Text = ""
    Sub_Total.Text = ""
    Sobre_Tasa.Text = ""
    Retencion_Iva.Text = ""
    Retencion_IIBB.Text = ""
    Total_Factura.Text = ""
End Sub

Private Sub Termina_Click()
    If Articulo.Text = "" And Id_Rubro.Text = "" Then
        Marco_Factura_Item.Enabled = True
        Marco_Captura.Visible = False
        Botonera.Enabled = True
        Salir.Cancel = True
        
        Grabar.Enabled = True
        Grabar.SetFocus
    Else
        Borrar_Campo_Captura
        AbrirBusquedaArticulos
    End If
End Sub

Private Sub Borrar_Campo_Captura()
    Id_Rubro.Text = ""
    Articulo.Text = ""
    Descripcion.Text = ""
    Presentacion.Text = ""
    Existencia.Text = ""
    Final_Unitario.Text = ""
    Final_Unitario.Tag = ""
    Precio_Venta.Text = ""
    Precio_Venta.BackColor = 14737632
    Espesor.BackColor = 14737632
    Ancho.BackColor = 14737632
    Largo.BackColor = 14737632
    Precio_Venta.BackColor = 14737632
    Precio_Venta.Tag = ""
    Rec_Desc.Text = ""
    Confirma.Tag = ""
    Cantidad.Text = ""
    Total.Text = ""
    
    Espesor.Text = ""
    Largo.Text = ""
    Ancho.Text = ""
    Medida.Text = ""
    
    Espesor.Enabled = False
    Largo.Enabled = False
    Rec_Desc.Enabled = False
    Cantidad.Enabled = False
    Borrar.Enabled = False
    
    List1.Tag = ""
End Sub

Private Sub Tipo_Cliente_GotFocus()
    Tipo_Cliente.SelStart = 0
    Tipo_Cliente.SelText = ""
    Tipo_Cliente.SelLength = Len(Tipo_Cliente.Text)
End Sub

Private Sub Tipo_Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Tipo_Cliente_LostFocus()
    Tipo_Cliente.Text = UCase(Tipo_Cliente.Text)
    If Not (Trim(UCase(Tipo_Cliente.Text)) = "P" Or Trim(UCase(Tipo_Cliente.Text)) = "R") Then Tipo_Cliente.Text = "error"
End Sub

Private Sub Total_Change()
    If Val(Total.Text) > 0 And Val(Cantidad.Text) > 0 And (Trim(UCase(Id_Rubro.Text)) = "MANUAL" Or (Val(Articulo.Text) > 0 And Val(Mid(Id_Rubro.Text, 31)) > 0)) Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Total_GotFocus()
    Total.Text = Val(Final_Unitario.Text) * Val(Cantidad.Text)
    
    Total.SelStart = 0
    Total.SelText = ""
    Total.SelLength = Len(Total.Text)
End Sub

Private Sub Total_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Total_LostFocus()
    Total.Text = Formateado(Str(Val(Total.Text)), 2, 10, " ", False)
End Sub

Private Sub Vto_GotFocus()
    Vto.SelStart = 0
    Vto.SelText = ""
    Vto.SelLength = Len(Vto.Text)
End Sub

Private Sub Vto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Vto_LostFocus()
    Vto.Text = ValidarFecha(Vto.Text)
End Sub

Private Sub Aderir_Recargos()
    Dim Recargo  As Boolean
    Recargo = False
    Dim Precio As Single
    
    qy = "SELECT * FROM Articulo WHERE Id_Articulo = " & Trim(Str(Val(Articulo.Text))) & " "
    qy = qy & "AND Id_Rubro = " & Trim(Str(Val(Mid(Id_Rubro.Text, 31))))
    AbreRs
    
    If Not Rs.EOF Then
                
        Precio = Val(Final_Unitario.Text)
        
        If Val(Ancho.Text) < Val(Rs.Fields("Ancho_Menor")) Then
            Precio = ((Val(Precio) * Val(Rs.Fields("Rec_Ancho_Menor"))) / 100) + Val(Precio)
        End If
        If Val(Ancho.Text) > Val(Rs.Fields("Ancho_Mayor")) Then
            Precio = ((Val(Precio) * Val(Rs.Fields("Rec_Ancho_Mayor"))) / 100) + Val(Precio)
        End If
        If Val(Largo.Text) < Val(Rs.Fields("Largo_Menor")) Then
            Precio = ((Val(Precio) * Val(Rs.Fields("Rec_Largo_Menor"))) / 100) + Val(Precio)
        End If
        If Val(Largo.Text) > Val(Rs.Fields("Largo_Mayor")) Then
            Precio = ((Val(Precio) * Val(Rs.Fields("Rec_Largo_Mayor"))) / 100) + Val(Precio)
        End If
        
        Final_Unitario.Tag = Val(Precio)
        Final_Unitario.Text = Val(Final_Unitario.Tag)
        Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
        
    End If ' Termino el Procedimiento.-
End Sub

Private Sub Id_Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        
        If Trim(UCase(Id_Rubro.Text)) = "M" Then
            Articulo_LostFocus
        ElseIf Val(Id_Rubro.Text) > 0 Or Val(Mid(Id_Rubro.Text, 31)) > 0 Then
            
            If Val(Mid(Id_Rubro.Text, 31)) > 0 Or Val(Id_Rubro.Text) > 0 Then
                qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & IIf(Val(Id_Rubro.Text) > 0, Val(Id_Rubro.Text), Trim(Str(Val(Mid(Id_Rubro.Text, 31)))))
                AbreRs
                
                If Rs.EOF Then
                    MsgBox "El Rubro especificado es inexistente...", vbCritical, "Atenci�n.!"
                    Id_Rubro.Text = ""
                    Id_Rubro.SetFocus
                Else
                    'Id_Rubro.Text = Formateado(str(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & trim(Rs.Fields("Denominacion"))
                    Id_Rubro.Text = Trim(Mid(Rs.Fields("Denominacion"), 1, 30)) + Space$(30 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 30)))) & " " & Trim(Rs.Fields("Id_Rubro"))
                End If
            Else
                SendKeys "{tab}"
            End If
        End If
    End If
End Sub

Private Sub Id_Rubro_LostFocus()
    Id_Rubro.Text = UCase(Id_Rubro.Text)
End Sub

Private Sub Precio_Venta_GotFocus()
    Precio_Venta.Text = Trim(Precio_Venta.Text)
    Precio_Venta.SelStart = 0
    Precio_Venta.SelText = ""
    Precio_Venta.SelLength = Len(Precio_Venta.Text)
End Sub

Private Sub Precio_Venta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Precio_Venta_LostFocus()
    Precio_Venta.Text = Formateado(Str(Val(Precio_Venta.Text)), 4, 10, " ", False)
    CalcularPrecioVenta
End Sub

Private Sub CalcularPrecioVenta()
    If Val(Precio_Venta.Text) > 0 Then
        If Largo.Enabled = True Then
            Largo_LostFocus
        Else
            'Final_Unitario.Text = ((Val(Precio_Venta.Text) * Val(Alicuota_Iva)) / 100) + Val(Precio_Venta.Text)
            Final_Unitario.Text = Val(Precio_Venta.Text)
            Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
            Final_Unitario.Tag = Final_Unitario.Text
        End If
    ElseIf Val(Precio_Venta.Text) = 0 Then
        If (Val(Id_Rubro.Text) > 0 Or Trim(UCase(Id_Rubro.Text)) = "M") Then
            Precio_Venta.Text = ""
            
            Final_Unitario.Enabled = True
            Final_Unitario.BackColor = vbWhite
            Final_Unitario.Text = ""
            Final_Unitario.SetFocus
        End If
    End If
End Sub

Private Sub Final_Unitario_GotFocus()
    Final_Unitario.Text = Trim(Final_Unitario.Text)
    Final_Unitario.SelStart = 0
    Final_Unitario.SelText = ""
    Final_Unitario.SelLength = Len(Final_Unitario.Text)
End Sub

Private Sub Final_Unitario_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Final_Unitario_LostFocus()
    CalcularPrecioVenta
    
    Final_Unitario.Text = Formateado(Str(Val(Final_Unitario.Text)), 4, 10, " ", False)
    Final_Unitario.Enabled = False
    Final_Unitario.BackColor = 14737632
End Sub

Private Sub Leer_Rubro()
    qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & Trim(Str(Val(Id_Rubro.Text)))
    AbreRs
                
    If Rs.EOF Then
        MsgBox "El Rubro especificado es inexistente...", vbCritical, "Atenci�n.!"
        Id_Rubro.Text = ""
        Id_Rubro.SetFocus
    Else
        Id_Rubro.Text = Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
        Id_Rubro.Text = Trim(Mid(Rs.Fields("Denominacion"), 1, 30)) + Space$(30 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 30)))) & " " & Trim(Rs.Fields("Id_Rubro"))
    End If
End Sub
