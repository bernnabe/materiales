VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Contab_AperEj 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Apertura de un nuevo ciclo contable.-"
   ClientHeight    =   4335
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7215
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4335
   ScaleWidth      =   7215
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Ejercicio 
      Height          =   3375
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   7215
      Begin VB.Frame Marco_Progreso 
         BorderStyle     =   0  'None
         Height          =   615
         Left            =   120
         TabIndex        =   9
         Top             =   2640
         Visible         =   0   'False
         Width           =   6975
         Begin MSComctlLib.ProgressBar Barra 
            Height          =   255
            Left            =   0
            TabIndex        =   10
            Top             =   360
            Width           =   6975
            _ExtentX        =   12303
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
         End
         Begin VB.Label Label3 
            Caption         =   $"Contab_AperEj.frx":0000
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   0
            TabIndex        =   11
            Top             =   120
            Width           =   6975
         End
      End
      Begin VB.TextBox Nuevo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         MaxLength       =   4
         TabIndex        =   1
         Top             =   840
         Width           =   735
      End
      Begin VB.TextBox Ultimo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         MaxLength       =   4
         TabIndex        =   0
         Top             =   360
         Width           =   735
      End
      Begin VB.Label Label4 
         Caption         =   "(Ingrese cuatro d�gitos)"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2760
         TabIndex        =   12
         Top             =   840
         Width           =   2895
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Nuevo Ejercicio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   8
         Top             =   840
         Width           =   1695
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "�ltimo Ejercicio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   1695
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   6
      Top             =   3360
      Width           =   7215
      Begin VB.CommandButton Ayuda 
         Height          =   615
         Left            =   4680
         Picture         =   "Contab_AperEj.frx":0091
         Style           =   1  'Graphical
         TabIndex        =   3
         ToolTipText     =   "Mostrar Ayuda.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   6000
         Picture         =   "Contab_AperEj.frx":3103
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Contab_AperEj.frx":938D
         Style           =   1  'Graphical
         TabIndex        =   2
         ToolTipText     =   "Confirma la Apertura del Nuevo Ejercicio.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Contab_AperEj"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Confirma_Click()
    If MsgBox("Confirma la Apertura del nuevo Ejercicio. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
    
        MousePointer = 11
        
        
        'Traigo todas las cuentas que van a ser insertadas en "Saldo_Inicial"
        qy = "SELECT Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6 "
        qy = qy & "FROM Cuenta "
        qy = qy & "WHERE Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "GROUP BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6 "
        qy = qy & "ORDER BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6 "
        AbreRs
        
        If Rs.RecordCount > 0 Then
            Confirma.Tag = 100 / Val(Rs.RecordCount)
        End If
        
        Marco_Progreso.Visible = True
        Marco_Progreso.Refresh
        
        While Not Rs.EOF
            'Inserto las cuentas encontradas
            qy = "INSERT INTO Saldo_Inicial VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_1"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_2"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_3"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_4"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_5"))))
            qy = qy & ", " & Trim(Str(Val(Rs.Fields("Id_Nivel_6"))))
            qy = qy & ", '" & Trim(Str(Val(Nuevo.Text) - 1)) & "'"
            qy = qy & ", 0.00)"
            Db.Execute (qy)
            
            If Not Val(Barra.Value + Confirma.Tag) > 100 Then Barra.Value = Barra.Value + Val(Confirma.Tag)
            
            Rs.MoveNext
        Wend
        
        Barra.Value = 0
        Barra.Refresh
        
        qy = "SELECT Asiento.Nro_Asiento, Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6, SUM(Debe) AS Debitos, SUM(Haber) AS Creditos "
        qy = qy & "FROM Asiento, Cuenta "
        qy = qy & "WHERE Asiento.Nivel_1 = Cuenta.Id_Nivel_1 "
        qy = qy & "AND Asiento.Nivel_2 = Cuenta.Id_Nivel_2 "
        qy = qy & "AND Asiento.Nivel_3 = Cuenta.Id_Nivel_3 "
        qy = qy & "AND Asiento.Nivel_4 = Cuenta.Id_Nivel_4 "
        qy = qy & "AND Asiento.Nivel_5 = Cuenta.Id_Nivel_5 "
        qy = qy & "AND Asiento.Nivel_6 = Cuenta.Id_Nivel_6 "
        qy = qy & "AND Cuenta.Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "GROUP BY Asiento.Nro_Asiento, Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6 "
        qy = qy & "ORDER BY Cuenta.Id_Nivel_1, Cuenta.Id_Nivel_2, Cuenta.Id_Nivel_3, Cuenta.Id_Nivel_4, Cuenta.Id_Nivel_5, Cuenta.Id_Nivel_6 "
        AbreRs
        
        If Rs.RecordCount > 0 Then
            Confirma.Tag = 100 / Val(Rs.RecordCount)
        End If
        
        While Not Rs.EOF
            qy = "UPDATE Saldo_Inicial SET "
            qy = qy & "Saldo_Inicial = Saldo_Inicial + " & Formateado(Str(Val(Val(Rs.Fields("Debitos")) - Val(Rs.Fields("Creditos")))), 2, 12, " ", False) & " "
            qy = qy & "WHERE Id_Nivel_1 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_1")))) & " "
            qy = qy & "AND Id_Nivel_2 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_2")))) & " "
            qy = qy & "AND Id_Nivel_3 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_3")))) & " "
            qy = qy & "AND Id_Nivel_4 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_4")))) & " "
            qy = qy & "AND Id_Nivel_5 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_5")))) & " "
            qy = qy & "AND Id_Nivel_6 = " & Trim(Str(Val(Rs.Fields("Id_Nivel_6")))) & " "
            qy = qy & "AND Ejercicio = " & Val(Nuevo.Text) - 1 & " "
            qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
            Db.Execute (qy)
            
            If Not Val(Barra.Value + Confirma.Tag) > 100 Then Barra.Value = Barra.Value + Val(Confirma.Tag)
            Barra.Refresh
            Rs.MoveNext
        Wend
        
        Mostrar_Ultimo_Ejercicio
        
        MousePointer = 0
        Barra.Value = 100
        Barra.Refresh
        
        Marco_Progreso.Visible = False
        Barra.Value = 0
    End If
    
    Salir_Click
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Apertura de Nuevo Ciclo Contable.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 3.5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Mostrar_Ultimo_Ejercicio
End Sub

Private Sub Mostrar_Ultimo_Ejercicio()
    qy = "SELECT MAX(Ejercicio) FROM Saldo_Inicial"
    AbreRs
    
    Ultimo.Text = "Vacio"
    If Not IsNull(Rs.Fields(0)) = True Then
        Ultimo.Text = Val(Rs.Fields(0))
        Ultimo.Enabled = False
    Else
        Ultimo.Enabled = True
        Ultimo.Text = Val(Format(Mid(Fecha_Fiscal, 7, 4)) - 1)
        Ultimo.Tag = "VACIO"
    End If
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Apertura de Nuevo Ciclo Contable.-"
End Sub

Private Sub Nuevo_Change()
    If Val(Nuevo.Text) >= (Val(Ultimo.Text) + 2) Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Nuevo_GotFocus()
    Nuevo.Text = Trim(Nuevo.Text)
    
    Nuevo.SelStart = 0
    Nuevo.SelText = ""
    Nuevo.SelLength = Len(Nuevo.Text)
End Sub

Private Sub Nuevo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nuevo_LostFocus()
    If Len(Nuevo.Text) >= 4 Then
        Nuevo.Text = Formateado(Str(Val(Nuevo.Text)), 0, 4, " ", False)
    Else
        Nuevo.Text = ""
        Nuevo.SetFocus
    End If
End Sub

Private Sub Salir_Click()
    If Nuevo.Text = "" Then
        Unload Me
    Else
        Nuevo.Text = ""
        Nuevo.SetFocus
    End If
End Sub

Private Sub Ultimo_Change()
    If Val(Ultimo.Text) > 0 And Val(Nuevo.Text) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Ultimo_GotFocus()
    Ultimo.Text = Trim(Ultimo.Text)
    Ultimo.SelStart = 0
    Ultimo.SelText = ""
    Ultimo.SelLength = Len(Ultimo.Text)
End Sub

Private Sub Ultimo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ultimo_LostFocus()
    Ultimo.Text = Formateado(Str(Val(Ultimo.Text)), 0, 4, " ", False)
End Sub
