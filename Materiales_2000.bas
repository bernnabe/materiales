Attribute VB_Name = "Materiales_2000"
Option Explicit

Dim Db_Name                           As String
Public Db                             As ADODB.Connection
Public Rs                             As ADODB.Recordset
Public qy                             As String
Public Txt                            As String

Public Mes_Letra(1 To 12)             As String
Public Tipo_Iva(1 To 6)               As String
Public Articulo_Presentacion(0 To 8)  As String
Public Cliente_Categ(0 To 2)          As String

Dim Numeros(103)                      As String
Dim Letras                            As String

Public Fecha_Fiscal                   As String
Public Hora_Fiscal                    As String
Public Desplegar_Combos               As Boolean

'Variables de Parametrizaci�n del Sistema
Public Alicuota_Iva                   As Single
Public Alicuota_Sta                   As Single
Public cCta_Caja                      As String
Public cCta_CtaCte                    As String
Public cCta_Debito_Iva                As String
Public cCta_Debito_STA                As String
Public cCta_Credito_Iva               As String
Public cCta_Impuestos                 As String
Public cCentro_Emisor                 As Long
Public Id_Empresa                     As Long
Public cEmpresa                       As String
Public cSlogan                        As String
Dim Puerto_Impresora                  As Long
Public cCopias                        As Long

'Reconocimiento de Usuario
Public Numero_Usuario                 As Long
Public Permiso_Mod                    As Long
Public Permiso_Admin                  As Long
Public Permiso_Alta                   As Long
Public Permiso_Cons                   As Long
Public Puerto_MPto                    As String

Public RunTimeEnviropment             As SystemEnviropment
Public ComprobanteEstados             As New clsPresupuestoEstados
Public DbServerName                   As String

Sub Main()

    'RunTimeEnviropment = Prodution
    RunTimeEnviropment = Development
    
    Abrir_Base_Datos

    Load Menu
    Menu.Show

    Menu.MousePointer = 11
    
    Menu.Estado.Panels(2).Text = "Inicializando el Sistema..."
           
    Tipo_Iva(1) = "CF - CONSUMIDOR FINAL      "
    Tipo_Iva(2) = "RI - RESPONSABLE INSCRIPTO "
    Tipo_Iva(3) = "NI - NO INSCRIPTO          "
    Tipo_Iva(4) = "MT - MONOTRIBUTO           "
    Tipo_Iva(5) = "ET - RESPONSABLE EXENTO    "
    Tipo_Iva(6) = "NC - NO CATEGORIZADO       "
    
    Cliente_Categ(0) = "00 - NO ESPECIFICADA "
    Cliente_Categ(1) = "01 - REVENDEDORES    "
    Cliente_Categ(2) = "02 - P�BLICOS        "
    
    Mes_Letra(1) = "01 - ENERO     "
    Mes_Letra(2) = "02 - FEBRERO   "
    Mes_Letra(3) = "03 - MARZO     "
    Mes_Letra(4) = "04 - ABRIL     "
    Mes_Letra(5) = "05 - MAYO      "
    Mes_Letra(6) = "06 - JUNIO     "
    Mes_Letra(7) = "07 - JULIO     "
    Mes_Letra(8) = "08 - AGOSTO    "
    Mes_Letra(9) = "09 - SETIEMBRE "
    Mes_Letra(10) = "10 - OCTUBRE   "
    Mes_Letra(11) = "11 - NOVIEMBRE "
    Mes_Letra(12) = "12 - DICIEMBRE "
    
    Articulo_Presentacion(0) = "0 - NO ESPECIFICADO "
    Articulo_Presentacion(1) = "1 - Unds.           "
    Articulo_Presentacion(2) = "2 - Lts.            "
    Articulo_Presentacion(3) = "3 - P2              "
    Articulo_Presentacion(4) = "4 - M2              "
    Articulo_Presentacion(5) = "5 - ML (MADERA)     "
    Articulo_Presentacion(6) = "6 - ML              "
    Articulo_Presentacion(7) = "7 - Kgs.            "
    Articulo_Presentacion(8) = "8 - M2 (AxL)        "
    
    Menu.Estado.Panels(2).Text = "Buscando Impresora..."
    'Inicializar_Impresora

    'Fecha_Fiscal = mid(Menu.HASAR1.FechaHoraFiscal, 1, 10)
    'Hora_Fiscal = trim(mid(Menu.HASAR1.FechaHoraFiscal, 11))
    'Menu.HASAR1.Finalizar
    
    Menu.MousePointer = 0
    Menu.Estado.Panels(2).Text = "Libre..."
    
    'Abrir_Llave
    
    Load Inicial
    Inicial.Show
End Sub

Function Parametrizar()
    If Val(Id_Empresa) > 0 Then
        
        qy = "SELECT * FROM Empresa WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        AbreRs
        
        cEmpresa = Trim(Rs.Fields("Denominacion"))
        cSlogan = Trim(Rs.Fields("Slogan"))
        Alicuota_Iva = Val(Rs.Fields("Alicuota_Iva"))
        Alicuota_Sta = Val(Rs.Fields("Alicuota_Sta"))
        cCta_Caja = Val(Rs.Fields("Cta_Caja"))
        cCta_CtaCte = Val(Rs.Fields("Cta_CtaCte"))
        cCta_Debito_Iva = Val(Rs.Fields("Cta_Debito_Iva"))
        cCta_Debito_STA = Val(Rs.Fields("Cta_Debito_Sta"))
        cCta_Credito_Iva = Val(Rs.Fields("Cta_Credito_Iva"))
        Puerto_Impresora = Val(Rs.Fields("Puerto_Impresora"))
        cCentro_Emisor = Val(Rs.Fields("Centro_Emisor"))
        cCopias = Val(Rs.Fields("Copias"))
        
        If Trim(Rs.Fields("Desplegar_Combos")) = "Falso" Then
            Desplegar_Combos = False
        Else
            Desplegar_Combos = True
        End If
        
        Menu.Caption = Trim(cEmpresa) & " - Sistema de Gesti�n Integral.-"
        
        Msg_Empresa.Show
        Msg_Empresa.Nombre.Caption = Trim(cEmpresa)
    End If
End Function

Function GetDateFromServer() As Date

    qy = "Select GetDate() DateOnServer"
    AbreRs
    
    GetDateFromServer = CDate(Rs("DateOnServer"))

End Function

Sub Inicializar_Impresora()
    On Error GoTo Impresora_Apag

Procesar:

    Menu.HASAR1.Puerto = Val(Puerto_Impresora)
    Menu.HASAR1.Modelo = MODELO_P320
    Menu.HASAR1.Comenzar
    Menu.HASAR1.ConfigurarControlador COPIAS_DOCUMENTOS, Val(cCopias)

    Menu.HASAR1.TratarDeCancelarTodo

    Exit Sub

Impresora_Apag:

    If MsgBox("Error Impresora:" & Err.Description, vbRetryCancel, "Atenci�n.!") = vbRetry Then
        Resume Procesar
    Else
        End
    End If
End Sub

Sub Abrir_Base_Datos()
    
    On Error GoTo Errores

Procesar:

    Set Db = New ADODB.Connection
    
    Select Case RunTimeEnviropment
    
        Case SystemEnviropment.Prodution
            
            DbServerName = "SERV_PIER"
            Db.ConnectionString = "Driver={SQL Server}; Server=" & DbServerName & "; Database=Materiales"
        
        Case SystemEnviropment.Development
            
            DbServerName = "."
            'Db.ConnectionString = "Driver={SQL Server}; Server=SERV_PIER; Database=MaterialesdESA"
            Db.ConnectionString = "Driver={SQL Server}; Server=" & DbServerName & "; Database=Materiales; Persist Security Info=true;User ID=sa"
        
    End Select
        
    Db.Open
    
    qy = "Set DateFormat dmy"
    AbreRs
    
    Exit Sub
    
Errores:
    
    If MsgBox("No se ha podido establecer la conexi�n con la base de datos.", vbCritical + vbRetryCancel, "Error de servidor") = vbRetry Then
        Resume Procesar
    Else
        End
    End If
   
End Sub

Sub AbreRs()
    Set Rs = New ADODB.Recordset
    
    Rs.CursorType = adOpenKeyset
    Rs.LockType = adLockOptimistic
    
    Rs.Open qy, Db
End Sub

Function Formateado(Numero As String, Decimales As Long, Spaces As Long, Caracter As String, Millares As Boolean)
    Dim i         As Long
    Dim Mascara   As String
    
    Numero = Trim(Str(Val(Numero)))
    
    If Millares Then
        Mascara = "#,###,###,###,##0"
    Else
        Mascara = "############0"
    End If
    If Decimales > 0 Then
        Mascara = Mascara & "." & String(Decimales, "0")
    End If
    
    Numero = Format(Val(Numero), Mascara)
    
    If Len(Numero) < Spaces Then
        Formateado = String(Spaces - Len(Numero), Caracter) & Numero
    Else
        Formateado = Numero
    End If
End Function

Function ValidarCuit(Cuit As String)
    Dim Indice     As Long
    Dim Factor     As Long
    Dim Control    As Long
    Dim Suma       As Long
    Dim Resultado  As Long
    
    Cuit = Trim(Cuit)
    
    If Mid(Cuit, 3, 1) = "-" And Mid(Cuit, 12, 1) = "-" Then
        Factor = 2
        Control = Val(Mid(Cuit, 13, 1))
        For Indice = 11 To 1 Step -1
            If Indice = 3 Then
               'Lo ignoro porque es el gui�n
            Else
                Suma = Suma + Val(Mid(Cuit, Indice, 1)) * Factor
                Factor = Factor + 1
                If Factor = 8 Then Factor = 2
            End If
        Next
        
        Resultado = Suma Mod 11
        
        If Resultado <> 0 Then
            Resultado = 11 - Resultado
        End If
        
        If Resultado = 10 Then
            Resultado = 9
        End If
        
        If Resultado = Control Then
            ValidarCuit = Cuit
        Else
            ValidarCuit = "error"
        End If
    End If
End Function

Function ValidarFecha(Fecha)
    Dim Dia         As Long
    Dim Mes         As Long
    Dim A�o         As Single
    Dim Slashed     As String
    Dim X           As Long
    Dim Valido      As Boolean
        
    Valido = False
    Slashed = Trim(Fecha)
    Fecha = ""
    
    For X = 1 To Len(Slashed)
        If Mid(Slashed, X, 1) >= "0" And Mid(Slashed, X, 1) <= "9" Then
            Fecha = Fecha + Mid(Slashed, X, 1)
        End If
    Next
    
    If Len(Fecha) = 6 Or Len(Fecha) = 8 Then
        Dia = Val(Mid(Fecha, 1, 2))
        Mes = Val(Mid(Fecha, 3, 2))
        If Len(Fecha) = 8 Then
            A�o = Val(Mid(Fecha, 5, 4))
        Else
            A�o = Val(Mid(Fecha, 5, 2))
            If A�o > 50 Then
                A�o = 1900 + A�o
            Else
                A�o = 2000 + A�o
            End If
        End If
        Fecha = Mid(Fecha, 1, 4) + Trim(Str(A�o))
        If Mes > 0 And Mes < 13 Then
            If Dia > 0 And Dia < 32 Then
                If Mes = 2 And A�o / 4 <> Int(A�o / 4) And Dia < 29 Then Valido = True
                If Mes = 2 And A�o / 4 = Int(A�o / 4) And Dia < 30 Then Valido = True
                If (Mes = 4 Or Mes = 6 Or Mes = 9 Or Mes = 11) And Dia < 31 Then Valido = True
                If Mes = 1 Or Mes = 3 Or Mes = 5 Or Mes = 7 Or Mes = 8 Or Mes = 10 Or Mes = 12 Then Valido = True
            End If
        End If
    End If
    If Valido = True Then
        ValidarFecha = Mid(Fecha, 1, 2) + "/" + Mid(Fecha, 3, 2) + "/" + Mid(Fecha, 5, 4)
    Else
        ValidarFecha = "error"
    End If
End Function

Function FiltroCaracter(Campo As String)
    Dim i As Long
    
    Campo = Trim(Campo)
    i = 0
    
    For i = 1 To Len(Campo)
        If Mid(Campo, i, 1) = "'" Then Mid(Campo, i, 1) = "^"
        
        If UCase(Mid(Campo, i, 1)) = "�" Then Mid(Campo, i, 1) = "A"
        If UCase(Mid(Campo, i, 1)) = "�" Then Mid(Campo, i, 1) = "E"
        If UCase(Mid(Campo, i, 1)) = "�" Then Mid(Campo, i, 1) = "I"
        If UCase(Mid(Campo, i, 1)) = "�" Then Mid(Campo, i, 1) = "O"
        If UCase(Mid(Campo, i, 1)) = "�" Then Mid(Campo, i, 1) = "U"
    Next
    
    FiltroCaracter = UCase(Campo)
End Function

Function CalcMed(Espesor As Single, Ancho As Single, Largo As Single, M2 As Boolean)
    Dim Resultado    As String
    Dim Coeficiente  As String
    
    Espesor = Val(Espesor)
    Ancho = Val(Ancho)
    Largo = Val(Largo)
    Coeficiente = 0.2734
        
    If M2 = False Then
        Resultado = ((Val(Espesor) * Val(Ancho)) * Val(Coeficiente)) * Val(Largo)
    Else
        Resultado = (Val(Ancho) * 0.0254) * Val(Largo)
    End If
    
    CalcMed = Formateado(Str(Val(Resultado)), 2, 9, " ", False)
End Function

Function Centenas(VCentena As Double) As String
    If VCentena = 1 Then
        Centenas = Numeros(100)
    Else
        If VCentena = 5 Then
            Centenas = Numeros(101)
        Else
            If VCentena = 7 Then
                Centenas = Letras & Numeros(102)
            Else
                If VCentena = 9 Then
                    Centenas = Letras & Numeros(103)
                Else
                    Centenas = Numeros(VCentena)
                End If
            End If
        End If
    End If
End Function

Function Unidades(VUnidad As Double) As String
    Unidades = Numeros(VUnidad)
End Function

Function Decenas(VDecena As Double) As String
    Decenas = Numeros(VDecena)
End Function

Sub Inicializar()
    Numeros(0) = "Cero"
    Numeros(1) = "Uno"
    Numeros(2) = "Dos"
    Numeros(3) = "Tres"
    Numeros(4) = "Cuatro"
    Numeros(5) = "Cinco"
    Numeros(6) = "Seis"
    Numeros(7) = "Siete"
    Numeros(8) = "Ocho"
    Numeros(9) = "Nueve"
    Numeros(10) = "Diez"
    Numeros(11) = "Once"
    Numeros(12) = "Doce"
    Numeros(13) = "Trece"
    Numeros(14) = "Catorce"
    Numeros(15) = "Quince"
    Numeros(16) = "Dieciseis"
    Numeros(17) = "Diecisiete"
    Numeros(18) = "Dieciocho"
    Numeros(19) = "Diecinueve"
    Numeros(20) = "Veinte"
    Numeros(21) = "Veintiuno"
    Numeros(22) = "Veintidos"
    Numeros(23) = "Veintitres"
    Numeros(24) = "Veinticuatro"
    Numeros(25) = "Veinticinco"
    Numeros(26) = "Veintiseis"
    Numeros(27) = "Veintisiete"
    Numeros(28) = "Veintiocho"
    Numeros(29) = "Veintinueve"
    Numeros(30) = "Treinta"
    Numeros(40) = "Cuarenta"
    Numeros(50) = "Cincuenta"
    Numeros(60) = "Sesenta"
    Numeros(70) = "Setenta"
    Numeros(80) = "Ochenta"
    Numeros(90) = "Noventa"
    Numeros(100) = "Ciento"
    Numeros(101) = "Quinientos"
    Numeros(102) = "Setecientos"
    Numeros(103) = "Novecientos"
End Sub

Function NumerosALetras(Numero As Double, Valor1 As Boolean, Valor2 As Boolean) As String
    Dim Letras As String
    Dim HuboCentavos As Boolean
    Dim Decimales As Double

    Decimales = Numero - Int(Numero)
    Numero = Int(Numero)
    Inicializar

    Letras = ""
    Do
        '*---> Validaci�n si se pasa de 100 millones
        If Numero >= 1000000000 Then
            Letras = "Error en Conversi�n a Letras"
            Numero = 0
            Decimales = 0
        End If
   
   '*---> Centenas de Mill�n
    If (Numero < 1000000000) And (Numero >= 100000000) Then
        If (Int(Numero / 100000000) = 1) And ((Numero - (Int(Numero / 100000000) * 100000000)) < 1000000) Then
        Letras = Letras & "cien millones "
    Else
        Letras = Letras & Centenas(Int(Numero / 100000000))
        If (Int(Numero / 100000000) <> 1) And (Int(Numero / 100000000) <> 5) And (Int(Numero / 100000000) <> 7) And (Int(Numero / 100000000) <> 9) Then
            Letras = Letras & "cientos "
        Else
            Letras = Letras & " "
        End If
    End If
        Numero = Numero - (Int(Numero / 100000000) * 100000000)
    End If
   
    '*---> Decenas de Mill�n
    If (Numero < 100000000) And (Numero >= 10000000) Then
        If Int(Numero / 1000000) < 30 Then
            Letras = Letras & Decenas(Int(Numero / 1000000))
            Letras = Letras & " millones "
            Numero = Numero - (Int(Numero / 1000000) * 1000000)
        Else
            Letras = Letras & Decenas(Int(Numero / 10000000) * 10)
            Numero = Numero - (Int(Numero / 10000000) * 10000000)
            If Numero > 1000000 Then
                Letras = Letras & " y "
            End If
        End If
    End If
   
    '*---> Unidades de Mill�n
    If (Numero < 10000000) And (Numero >= 1000000) Then
        If Int(Numero / 1000000) = 1 Then
            Letras = Letras & " un mill�n "
        Else
            Letras = Letras & Unidades(Int(Numero / 1000000))
            Letras = Letras & " millones "
        End If
        Numero = Numero - (Int(Numero / 1000000) * 1000000)
    End If
   
    '*---> Centenas de Millar
    If (Numero < 1000000) And (Numero >= 100000) Then
        If (Int(Numero / 100000) = 1) And ((Numero - (Int(Numero / 100000) * 100000)) < 1000) Then
            Letras = Letras & "cien mil "
        Else
            Letras = Letras & Centenas(Int(Numero / 100000))
            If (Int(Numero / 100000) <> 1) And (Int(Numero / 100000) <> 5) And (Int(Numero / 100000) <> 7) And (Int(Numero / 100000) <> 9) Then
                Letras = Letras & "cientos "
            Else
                Letras = Letras & " "
            End If
        End If
        Numero = Numero - (Int(Numero / 100000) * 100000)
   End If
   
    '*---> Decenas de Millar
    If (Numero < 100000) And (Numero >= 10000) Then
        If Int(Numero / 1000) < 30 Then
            Letras = Letras & Decenas(Int(Numero / 1000))
            Letras = Letras & " mil "
            Numero = Numero - (Int(Numero / 1000) * 1000)
        Else
            Letras = Letras & Decenas(Int(Numero / 10000) * 10)
            Numero = Numero - (Int((Numero / 10000)) * 10000)
            If Numero > 1000 Then
                Letras = Letras & " y "
            Else
                Letras = Letras & " mil "
            End If
       End If
    End If
   
    '*---> Unidades de Millar
    If (Numero < 10000) And (Numero >= 1000) Then
        If Int(Numero / 1000) = 1 Then
            Letras = Letras & "un"
        Else
            Letras = Letras & Unidades(Int(Numero / 1000))
        End If
        Letras = Letras & " mil "
        If Trim(Letras) = "un mil" Then Letras = " mil "
        Numero = Numero - (Int(Numero / 1000) * 1000)
    End If
   
    '*---> Centenas
    If (Numero < 1000) And (Numero > 99) Then
        If (Int(Numero / 100) = 1) And ((Numero - (Int(Numero / 100) * 100)) < 1) Then
            Letras = Letras & "cien "
        Else
            Letras = Letras & Centenas(Int(Numero / 100))
            If (Int(Numero / 100) <> 1) And (Int(Numero / 100) <> 5) And (Int(Numero / 100) <> 7) And (Int(Numero / 100) <> 9) Then
                Letras = Letras & "cientos "
            Else
                Letras = Letras & " "
            End If
        End If
        Numero = Numero - (Int(Numero / 100) * 100)
    End If
   
    '*---> Decenas
    If (Numero < 100) And (Numero > 9) Then
        If Numero < 29 Then
            Letras = Letras & Decenas(Int(Numero))
            Numero = Numero - Int(Numero)
        Else
            Letras = Letras & Decenas(Int((Numero / 10)) * 10)
            Numero = Numero - (Int((Numero / 10)) * 10)
            If Numero > 0.99 Then
                Letras = Letras & " y "
            End If
      End If
    End If
   
    '*---> Unidades
    If (Numero < 10) And (Numero > 0.99) Then
        Letras = Letras & Unidades(Int(Numero))
        Numero = Numero - Int(Numero)
    End If
    Loop Until (Numero = 0)

    '*---> Decimales
    If (Decimales > 0) Then
        Letras = Letras & " con "
        Letras = Letras & Format(Decimales * 100, "00") & "/100"
    Else
        If (Letras <> "Error en Conversi�n a Letras") And (Len(Trim(Letras)) > 0) Then
            Select Case Valor1
                Case True
                Letras = Letras & " exactos"
            End Select
        End If
    End If
    NumerosALetras = Letras
    Select Case Valor2
        Case True
        NumerosALetras = UCase(Left(Letras, 1)) & Right(Letras, Len(Letras) - 1)
    End Select
End Function

Sub Actualizar_NumComp()
    Dim Fc_A          As Long 'Facturas (Impresi�n Fiscal)
    Dim Fc_B          As Long
    'Dim Nc_A          As Long 'Notas de Cr�ditos
    'Dim Nc_B          As Long
    'Dim Nd_A          As Long 'Notas de D�bitos
    'Dim Nd_B          As Long
    
    Menu.MousePointer = 11
    
    Menu.Estado.Panels(2).Text = "Abriendo la Base de Datos..."
    Abrir_Base_Datos
    
    Menu.Estado.Panels(2).Text = "Inicializando la impresora..."
    Inicializar_Impresora
    
    Menu.Estado.Panels(2).Text = "Verificando igualdad de numeraci�n en los comprobantes..."
    
    'Facturas A
    qy = "SELECT MAX(Id_Nro_Factura) FROM Factura_Venta WHERE Id_Tipo_Factura = 'FC' AND Id_Letra_Factura = 'A' AND Id_Nro_Factura = " & Trim(Str(Val(Menu.HASAR1.UltimaFactura) + 1))
    AbreRs
    
    If Not Rs.EOF Then
        If MsgBox("El Ticket Facura 'A' N�mero: " & Trim(Str(Val(Menu.HASAR1.UltimaFactura) + 1)) & " ya ah sido impreso, desea borrarlo y volver a emitirlo. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            qy = "DELETE FROM Factura_Venta_Item WHERE Id_Tipo_Factura = 'FC' "
            qy = qy & "AND Id_Letra_Factura = 'A' "
            qy = qy & "AND Id_Nro_Factura = " & Trim(Str(Val(Menu.HASAR1.UltimaFactura) + 1))
            Db.Execute (qy)
            
            qy = "DELETE FROM Factura_Venta WHERE Id_Tipo_Factura = 'FC' "
            qy = qy & "AND Id_Letra_Factura = 'A' "
            qy = qy & "AND Id_Nro_Factura = " & Trim(Str(Val(Menu.HASAR1.UltimaFactura) + 1))
            Db.Execute (qy)
        End If
    End If
    
    'Facturas B
    qy = "SELECT MAX(Id_Nro_Factura) FROM Factura_Venta WHERE Id_Tipo_Factura = 'FC' AND Id_Letra_Factura = 'B' AND Id_Nro_Factura = " & Trim(Str(Val(Menu.HASAR1.UltimoTicket) + 1))
    AbreRs
    
    If Not Rs.EOF Then
        If MsgBox("El Ticket Facura 'B' N�mero: " & Trim(Str(Val(Menu.HASAR1.UltimoTicket) + 1)) & " ya ah sido impreso, desea borrarlo y volver a emitirlo. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            qy = "DELETE FROM Factura_Venta_Item WHERE Id_Tipo_Factura = 'FC' "
            qy = qy & "AND Id_Letra_Factura = 'B' "
            qy = qy & "AND Id_Nro_Factura = " & Trim(Str(Val(Menu.HASAR1.UltimoTicket) + 1))
            Db.Execute (qy)
            
            qy = "DELETE FROM Factura_Venta WHERE Id_Tipo_Factura = 'FC' "
            qy = qy & "AND Id_Letra_Factura = 'B' "
            qy = qy & "AND Id_Nro_Factura = " & Trim(Str(Val(Menu.HASAR1.UltimoTicket) + 1))
            Db.Execute (qy)
        End If
    End If

    'Actualizo de igual modo los n�meros de comprobantes impresos
    qy = "UPDATE Parametro SET Ultima_FC_A = " & Trim(Str(Val(Menu.HASAR1.UltimaFactura))) & ", "
    qy = qy & "Ultima_FC_B = " & Trim(Str(Val(Menu.HASAR1.UltimoTicket)))
    Db.Execute (qy)
    
    Menu.HASAR1.Finalizar
    Menu.MousePointer = 0
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Public Function BusPosEnCombo(Combo As ComboBox, ByVal intCodigo As Variant) As Integer

    Dim i As Integer
    Dim Encontrado As Boolean
    
    Encontrado = False
    
    If Not IsNull(intCodigo) Then
    
        While i <= Combo.ListCount And Encontrado = False
            
            If Combo.ItemData(i) = intCodigo Then
                
                Encontrado = True
            Else
                
                i = i + 1
            End If
        Wend
                
    End If
    
    If Encontrado = True Then
        
        BusPosEnCombo = i
    Else
        
        BusPosEnCombo = -1
    End If
           
End Function

