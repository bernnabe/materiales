VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsCliente"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "SavedWithClassBuilder6" ,"Yes"
Attribute VB_Ext_KEY = "Top_Level" ,"Yes"
'variables locales para almacenar los valores de las propiedades
Private mvarIdCliente As Long 'copia local
Private mvarNombre As String 'copia local
Private mvarCondicionIva As String 'copia local

Public Property Let Nombre(ByVal vdata As String)
'se usa al asignar un valor a la propiedad, en la parte izquierda de una asignación.
'Syntax: X.Nombre = 5
    mvarNombre = vdata
End Property


Public Property Get Nombre() As String
'se usa al recuperar un valor de una propiedad, en la parte derecha de una asignación.
'Syntax: Debug.Print X.Nombre
    Nombre = mvarNombre
End Property



Public Property Let IdCliente(ByVal vdata As Long)
'se usa al asignar un valor a la propiedad, en la parte izquierda de una asignación.
'Syntax: X.IdCliente = 5
    mvarIdCliente = vdata
End Property


Public Property Get IdCliente() As Long
'se usa al recuperar un valor de una propiedad, en la parte derecha de una asignación.
'Syntax: Debug.Print X.IdCliente
    IdCliente = mvarIdCliente
End Property

Public Property Get CondicionIva() As String
    CondicionIva = mvarCondicionIva
End Property

Public Property Let CondicionIva(vdata As String)
    mvarCondicionIva = vdata
End Property

Public Function GetAll() As ADODB.Recordset
    
    Dim rstdatos As New ADODB.Recordset
    Dim strSql As String
    
    strSql = "Select * From Cliente Order By Nombre"
    rstdatos.Open strSql, Db
    
    Set GetAll = rstdatos

End Function

Public Function GetById(IdCliente As Long) As clsCliente

    Dim rstdatos As New ADODB.Recordset
    Dim objCliente As New clsCliente
    Dim strSql As String
    
    strSql = "Select * FRom Cliente Where Id_Cliente = " & CStr(IdCliente)
    rstdatos.Open strSql, Db
    
    With objCliente
        .IdCliente = rstdatos("Id_Cliente")
        .Nombre = rstdatos("Nombre")
        .CondicionIva = rstdatos("Condicion_Iva")
    End With
    
    Set GetById = objCliente
    Set rstdatos = Nothing

End Function
