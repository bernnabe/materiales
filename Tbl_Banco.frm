VERSION 5.00
Begin VB.Form Tbl_Banco 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaciσn de Entidades Bancarias.-"
   ClientHeight    =   4095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6495
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4095
   ScaleWidth      =   6495
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Datos 
      Height          =   3135
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   6495
      Begin VB.TextBox Telefono 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   4
         Top             =   1920
         Width           =   4095
      End
      Begin VB.TextBox Direccion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   3
         Top             =   1560
         Width           =   4095
      End
      Begin VB.TextBox Sucursal 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   2
         Top             =   1200
         Width           =   4095
      End
      Begin VB.ComboBox Tipos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   10
         Top             =   240
         Visible         =   0   'False
         Width           =   4095
      End
      Begin VB.CommandButton Buscar_Tipos 
         Caption         =   "&Cσdigo:"
         Height          =   255
         Left            =   1080
         TabIndex        =   11
         ToolTipText     =   "Buscar Registros existentes en la base de datos.-"
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Codigo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   4
         TabIndex        =   0
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox Descripcion 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   1
         Top             =   840
         Width           =   4095
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Telιfono:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   16
         Top             =   1920
         Width           =   1935
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Direcciσn:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   15
         Top             =   1560
         Width           =   1935
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Sucursal:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   14
         Top             =   1200
         Width           =   1935
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Denominaciσn:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   720
         TabIndex        =   12
         Top             =   840
         Width           =   1455
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   13
      Top             =   3120
      Width           =   6495
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   1200
         Picture         =   "Tbl_Banco.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Borrar.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Tbl_Banco.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Grabar.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   5280
         Picture         =   "Tbl_Banco.frx":6594
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   3960
         Picture         =   "Tbl_Banco.frx":C81E
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Imprimir Lista.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Tbl_Banco"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Cuenta_Paginas As Single

Private Sub Borrar_Click()
    If MsgBox("Desea Borrar ιste registro. ?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        qy = "DELETE FROM Banco WHERE Id_Banco = " & Trim(Str(Val(Codigo.Text)))
        Db.Execute (qy)
        
        Salir_Click
    Else
        Salir.SetFocus
    End If
End Sub

Private Sub Buscar_Tipos_Click()
    Tipos_Encontrados.Visible = True
    If Tipos_Encontrados.ListCount = 0 Then
        MousePointer = 11
        
        qy = "SELECT * FROM Banco ORDER BY Denominacion"
        AbreRs
        
        While Not Rs.EOF
            Tipos_Encontrados.AddItem Trim(Rs.Fields("Denominacion")) + Space(31 - Len(Trim(Rs.Fields("Denominacion")))) + Trim(Rs.Fields("Id_Banco"))
            Rs.MoveNext
        Wend
        MousePointer = 0
    End If
    Tipos_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Codigo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        
        If Val(Codigo.Text) = 0 Then
            qy = "SELECT MAX(Id_Banco) FROM Banco"
            AbreRs
        
            Codigo.Text = 1
            If Not Rs.Fields(0) = "" Then Codigo.Text = Val(Rs.Fields(0)) + 1
        End If
        
        If Val(Codigo.Text) > 0 Then
            Leer_Codigo
        End If
    End If
End Sub

Private Sub Leer_Codigo()
    If Val(Codigo.Text) > 0 Then
        qy = "SELECT * FROM Banco WHERE Id_Banco = " & Trim(Str(Val(Codigo.Text)))
        AbreRs
        
        If Rs.EOF Then
            If MsgBox("El Registro es inexistente, desea darlo de alta.?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
                Grabar.Tag = "Alta"
                Grabar.Enabled = True
                
                Descripcion.SetFocus
            Else
                Salir_Click
            End If
        Else
            Grabar.Tag = "Modificaciσn"
            Mostrar_Tipo
        End If
    Else
        Codigo.Text = ""
        Codigo.SetFocus
    End If
End Sub

Private Sub Borrar_Campo()
    Descripcion.Text = ""
    Sucursal.Text = ""
    Direccion.Text = ""
    Telefono.Text = ""
    Codigo.Text = ""
    
    Grabar.Enabled = False
    Borrar.Enabled = False
End Sub

Private Sub Mostrar_Tipo()
    Descripcion.Text = Rs.Fields("Denominacion")
    Sucursal.Text = Rs.Fields("Sucursal")
    Direccion.Text = Rs.Fields("Direccion")
    Telefono.Text = Rs.Fields("Telefono")
    
    Grabar.Enabled = True
    Borrar.Enabled = True
    
    Descripcion.SetFocus
End Sub

Private Sub Descripcion_GotFocus()
    Descripcion.Text = Trim(Descripcion.Text)
    
    Descripcion.SelStart = 0
    Descripcion.SelText = ""
    Descripcion.SelLength = Len(Descripcion.Text)
End Sub

Private Sub Descripcion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub Descripcion_LostFocus()
    Descripcion.Text = UCase(Descripcion.Text)
    Descripcion.Text = FiltroCaracter(Descripcion.Text)
End Sub

Private Sub Sucursal_GotFocus()
    Sucursal.Text = Trim(Sucursal.Text)
    
    Sucursal.SelStart = 0
    Sucursal.SelText = ""
    Sucursal.SelLength = Len(Sucursal.Text)
End Sub

Private Sub Sucursal_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub Sucursal_LostFocus()
    Sucursal.Text = UCase(Sucursal.Text)
    Sucursal.Text = FiltroCaracter(Sucursal.Text)
End Sub

Private Sub Direccion_GotFocus()
    Direccion.Text = Trim(Direccion.Text)
    
    Direccion.SelStart = 0
    Direccion.SelText = ""
    Direccion.SelLength = Len(Direccion.Text)
End Sub

Private Sub Direccion_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub Direccion_LostFocus()
    Direccion.Text = UCase(Direccion.Text)
    Direccion.Text = FiltroCaracter(Direccion.Text)
End Sub

Private Sub Telefono_GotFocus()
    Telefono.Text = Trim(Telefono.Text)
    
    Telefono.SelStart = 0
    Telefono.SelText = ""
    Telefono.SelLength = Len(Telefono.Text)
End Sub

Private Sub Telefono_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub Telefono_LostFocus()
    Telefono.Text = UCase(Telefono.Text)
    Telefono.Text = FiltroCaracter(Telefono.Text)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualizaciσn de Entidades Bancarias.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 3
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        
        qy = "INSERT INTO Banco VALUES ('"
        qy = qy & Trim(Str(Val(Codigo.Text))) & "'"
        qy = qy & ", '" & Trim(Descripcion.Text) & "'"
        qy = qy & ", '" & Trim(Sucursal.Text) & "'"
        qy = qy & ", '" & Trim(Direccion.Text) & "'"
        qy = qy & ", '" & Trim(Telefono.Text) & "')"
        Db.Execute (qy)
    Else
        
        qy = "UPDATE Banco SET "
        qy = qy & "Denominacion = '" & Trim(Descripcion.Text) & "', "
        qy = qy & "Sucursal = '" & Trim(Sucursal.Text) & "',"
        qy = qy & "Direccion = '" & Trim(Direccion.Text) & "',"
        qy = qy & "Telefono = '" & Trim(Telefono.Text) & "' "
        qy = qy & "WHERE Id_Banco = " & Trim(Str(Val(Codigo.Text)))
        Db.Execute (qy)
    End If
    
    Salir_Click
End Sub

Private Sub Salir_Click()
    If Val(Codigo.Text) = 0 Then
        Unload Me
    Else
        
        Borrar_Campo
        Codigo.SetFocus
    End If
End Sub

Private Sub Tipos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Codigo.SetFocus
End Sub

Private Sub Tipos_Encontrados_LostFocus()
    Codigo.Text = Mid(Tipos_Encontrados.List(Tipos_Encontrados.ListIndex), 32)
    Tipos_Encontrados.Visible = False
End Sub

Private Sub Imprimir_Click()
    If MsgBox("Confirma la impresiσn del listado de Rubros. ?.", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        Dim l As Long
        
        qy = "SELECT * FROM Banco ORDER BY Denominacion"
        AbreRs
        
        If Rs.RecordCount > 0 Then
            l = 0
            
            Printer.Font = "Courier New"
            Printer.FontSize = 9
            
            Imprimir_Encabezado
            
            While Not Rs.EOF
                
                If l <= 64 Then
                    Printer.Print "  " & Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
                    l = l + 1
                Else
                    Printer.Print " "
                    
                    l = 0
                    Printer.NewPage
                    Imprimir_Encabezado
                End If
                
                Rs.MoveNext
            Wend
            
            Printer.Print " "
            Printer.EndDoc
        End If
        
        Salir.SetFocus
    Else
        Codigo.SetFocus
    End If
End Sub

Private Sub Imprimir_Encabezado()
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                      Pαgina.: " & Printer.Page
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                   Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                             Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " e-mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print "                            LISTA DE ENTIDADES BANCARIAS POR ORDEN ALFABΙTICO"
    Printer.Print
    Printer.Print " "
    Printer.Print "   Cσd. Denominaciσn "
    Printer.Print " "
End Sub
