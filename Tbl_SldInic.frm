VERSION 5.00
Begin VB.Form Tbl_SldInic 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaci�n de Saldos Iniciales.-"
   ClientHeight    =   4695
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   6135
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   4695
   ScaleWidth      =   6135
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Cuenta 
      Height          =   3735
      Left            =   0
      TabIndex        =   6
      Top             =   0
      Width           =   6135
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1920
         TabIndex        =   19
         Top             =   1080
         Visible         =   0   'False
         Width           =   4095
      End
      Begin VB.TextBox Saldo_Actual 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   15
         Top             =   3240
         Width           =   1335
      End
      Begin VB.TextBox Mvnt_Cta 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   14
         Top             =   2880
         Width           =   1335
      End
      Begin VB.TextBox Saldo_Inicial 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   3
         Top             =   2520
         Width           =   1335
      End
      Begin VB.TextBox Telefono 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   11
         Top             =   1800
         Width           =   4095
      End
      Begin VB.TextBox Domicilio 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   10
         Top             =   1440
         Width           =   4095
      End
      Begin VB.TextBox Nombre 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1920
         TabIndex        =   9
         Top             =   1080
         Width           =   4095
      End
      Begin VB.TextBox Cuenta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         MaxLength       =   5
         TabIndex        =   2
         Top             =   1080
         Width           =   855
      End
      Begin VB.CommandButton Buscar_Cuentas 
         Caption         =   "&Cuenta:"
         Height          =   255
         Left            =   120
         TabIndex        =   8
         ToolTipText     =   "Buscar Cuenta en la Base de Datos.-"
         Top             =   1080
         Width           =   735
      End
      Begin VB.OptionButton AProveedor 
         Caption         =   "A Cuenta Proveedor."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   2055
      End
      Begin VB.OptionButton ACliente 
         Caption         =   "A Cuenta Cliente."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Value           =   -1  'True
         Width           =   2055
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Nuevo Saldo Actual:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   18
         Top             =   3240
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Movimientos en CtaCte:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   17
         Top             =   2880
         Width           =   1695
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo Inicial:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   16
         Top             =   2520
         Width           =   1695
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Tel�fono:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   1800
         Width           =   1695
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Domicilio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   1440
         Width           =   1695
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   7
      Top             =   3720
      Width           =   6135
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   4920
         Picture         =   "Tbl_SldInic.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Tbl_SldInic.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Grabar los Datos.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Tbl_SldInic"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub ACliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub AProveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Buscar_Cuentas_Click()
    MousePointer = 11
    Cuentas_Encontradas.Visible = True
    Cuentas_Encontradas.Clear
    qy = "SELECT * FROM " & IIf(ACliente.Value = True, "Cliente", "Proveedor") & " ORDER BY Nombre "
    AbreRs
    
    While Not Rs.EOF
        Cuentas_Encontradas.AddItem Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields(0))
        Rs.MoveNext
    Wend
    MousePointer = 0
    Cuentas_Encontradas.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Cuenta_Change()
    If Val(Cuenta.Text) > 0 Then
        Saldo_Inicial.Enabled = True
    Else
        Saldo_Inicial.Enabled = False
    End If
End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuenta_LostFocus
End Sub

Private Sub Cuenta_LostFocus()
    If Val(Cuenta.Text) > 0 Then
        Leer_Cuenta
    End If
End Sub

Private Sub Leer_Cuenta()
    qy = "SELECT * FROM " & IIf(ACliente.Value = True, "Cliente", "Proveedor") & " WHERE " & IIf(ACliente.Value = True, "Id_Cliente", "Id_Proveedor") & " = " & Trim(Str(Val(Cuenta.Text))) & " "
    AbreRs
    
    If Not Rs.EOF Then
        Nombre.Text = Rs.Fields("Nombre")
        Domicilio.Text = Rs.Fields("Domicilio") & " " & Rs.Fields("Numero")
        Telefono.Text = Rs.Fields("Telefono")
        
        qy = "SELECT SUM(Debe - Haber) FROM " & IIf(ACliente.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " WHERE Id_Cuenta = " & Trim(Str(Val(Cuenta.Text))) & " "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        AbreRs
        
        If Not Rs.Fields(0) = "" Then Mvnt_Cta.Text = Rs.Fields(0)
        Saldo_Actual.Text = Mvnt_Cta.Text
        
        Mvnt_Cta.Text = Formateado(Str(Val(Mvnt_Cta.Text)), 2, 10, " ", False)
        Saldo_Actual.Text = Formateado(Str(Val(Saldo_Actual.Text)), 2, 10, " ", False)
        
        Grabar.Enabled = True
        Saldo_Inicial.SetFocus
    Else
        MsgBox "Cuenta Inexistente...", vbInformation, "Atenci�n.!"
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    ACliente.Value = True
    Cuenta.Text = ""
    Nombre.Text = ""
    Domicilio.Text = ""
    Telefono.Text = ""
    Saldo_Actual.Text = ""
    Mvnt_Cta.Text = ""
    Saldo_Inicial.Text = ""
    
    ACliente.SetFocus
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuenta.SetFocus
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Cuenta.Text = Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 32)
    Cuentas_Encontradas.Visible = False
    Leer_Cuenta
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualizaci�n de Saldos.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Val(Saldo_Inicial.Text) <> 0 Then
        
        If Val(Permiso_Alta) = 1 And Val(Permiso_Mod) = 1 Then
            qy = "INSERT INTO " & IIf(ACliente.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Cuenta.Text))) & " "
            qy = qy & ", '" & Trim(Format(Fecha_Fiscal, "dd/mm/yyyy")) & " " & Trim(Hora_Fiscal) & "'"
            qy = qy & ", 'AA X 0000-0000000000'"
            qy = qy & ", 'ACTUALIZACI�N DEL SALDO'"
            qy = qy & ", '" & Trim(Format(Fecha_Fiscal, "dd/mm/yyyy")) & "'"
            If Val(Saldo_Inicial.Text) > 0 Then
                qy = qy & ", '" & Trim(Str(Val(Saldo_Inicial.Text))) & "'"
                qy = qy & ", '0')"
            Else
                qy = qy & ", '0'"
                qy = qy & ", '" & Trim(Str(Val(Saldo_Inicial.Text * -1))) & "')"
            End If
            Db.Execute (qy)
            
            Salir_Click
        Else
            MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
            Salir_Click
        End If
    Else
        Saldo_Inicial.SetFocus
    End If
End Sub

Private Sub Saldo_Inicial_Change()
    If Val(Saldo_Inicial.Text) <> 0 Then
        Saldo_Actual.Text = Val(Saldo_Inicial.Text) + Val(Mvnt_Cta.Text)
    ElseIf Val(Saldo_Inicial.Text) = 0 Then
        Saldo_Actual.Text = Mvnt_Cta.Text
    End If
    
    Saldo_Actual.Text = Formateado(Str(Val(Saldo_Actual.Text)), 2, 10, " ", False)
End Sub

Private Sub Saldo_Inicial_GotFocus()
    Saldo_Inicial.Text = Trim(Saldo_Inicial.Text)
    Saldo_Inicial.SelStart = 0
    Saldo_Inicial.SelText = ""
    Saldo_Inicial.SelLength = Len(Saldo_Inicial.Text)
End Sub

Private Sub Saldo_Inicial_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Saldo_Inicial_LostFocus()
    Saldo_Inicial.Text = Formateado(Str(Val(Saldo_Inicial.Text)), 2, 10, " ", False)
End Sub

Private Sub Salir_Click()
    If Cuenta.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub
