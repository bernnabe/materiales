VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form Fac_Recibo 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Recibo de Pago.-"
   ClientHeight    =   8625
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9495
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   8625
   ScaleWidth      =   9495
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Recibo 
      Height          =   660
      Left            =   0
      TabIndex        =   18
      Top             =   -75
      Width           =   9495
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8040
         MaxLength       =   10
         TabIndex        =   12
         TabStop         =   0   'False
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Numero 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   960
         MaxLength       =   10
         TabIndex        =   11
         TabStop         =   0   'False
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7200
         TabIndex        =   24
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Nϊmero:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Width           =   735
      End
   End
   Begin VB.Frame Marco_Cuenta 
      Height          =   1335
      Left            =   0
      TabIndex        =   19
      Top             =   525
      Width           =   9495
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   3465
         TabIndex        =   31
         Top             =   240
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.TextBox Saldo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3465
         MaxLength       =   10
         TabIndex        =   17
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox Domicilio 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3465
         MaxLength       =   30
         TabIndex        =   16
         Top             =   600
         Width           =   4335
      End
      Begin VB.TextBox Nombre 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3465
         MaxLength       =   30
         TabIndex        =   15
         Top             =   240
         Width           =   4335
      End
      Begin VB.TextBox Cuenta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2505
         MaxLength       =   5
         TabIndex        =   0
         Top             =   240
         Width           =   735
      End
      Begin VB.CommandButton Buscar_Cuenta 
         Caption         =   "&Cuenta:"
         Height          =   255
         Left            =   1425
         TabIndex        =   25
         Top             =   240
         Width           =   855
      End
      Begin VB.OptionButton Proveedores 
         Caption         =   "Proveedores"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   7950
         TabIndex        =   14
         Top             =   825
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.OptionButton Clientes 
         Caption         =   "Clientes"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   7950
         TabIndex        =   13
         Top             =   600
         Value           =   -1  'True
         Visible         =   0   'False
         Width           =   1335
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1665
         TabIndex        =   27
         Top             =   960
         Width           =   1695
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Domicilio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1665
         TabIndex        =   26
         Top             =   600
         Width           =   1695
      End
   End
   Begin VB.Frame Marco_Facturas 
      Caption         =   "Facturas Adeudadas:"
      Enabled         =   0   'False
      Height          =   1935
      Left            =   0
      TabIndex        =   20
      Top             =   1875
      Width           =   9495
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1335
         Left            =   120
         Sorted          =   -1  'True
         Style           =   1  'Checkbox
         TabIndex        =   1
         Top             =   480
         Width           =   9255
      End
      Begin VB.Label Label9 
         Caption         =   $"Fac_Recibo.frx":0000
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   240
         Width           =   9255
      End
   End
   Begin VB.Frame Marco_Formas 
      Enabled         =   0   'False
      Height          =   3045
      Left            =   0
      TabIndex        =   33
      Top             =   3750
      Width           =   9495
      Begin VB.TextBox txtTotalCheques 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   330
         Left            =   7950
         TabIndex        =   36
         Top             =   2625
         Width           =   1365
      End
      Begin VB.CommandButton cmdBorrarCheque 
         Caption         =   "Borrar Cheque"
         Height          =   315
         Left            =   1500
         TabIndex        =   4
         Top             =   2625
         Width           =   1365
      End
      Begin VB.CommandButton cmdAgregarCheque 
         Caption         =   "Agregar Cheque"
         Height          =   315
         Left            =   150
         TabIndex        =   3
         Top             =   2625
         Width           =   1365
      End
      Begin MSComctlLib.ListView lvwCheques 
         Height          =   1440
         Left            =   150
         TabIndex        =   35
         Top             =   1125
         Width           =   9165
         _ExtentX        =   16166
         _ExtentY        =   2540
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
      Begin VB.TextBox txtTotalEfectivo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7875
         MaxLength       =   10
         TabIndex        =   2
         Top             =   450
         Width           =   1365
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Total en Efectivo"
         ForeColor       =   &H00800000&
         Height          =   180
         Index           =   2
         Left            =   6225
         TabIndex        =   40
         Top             =   450
         Width           =   1605
      End
      Begin VB.Line Line1 
         Index           =   1
         X1              =   150
         X2              =   9300
         Y1              =   225
         Y2              =   225
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Efectivo"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   1
         Left            =   150
         TabIndex        =   39
         Top             =   225
         Width           =   2655
      End
      Begin VB.Line Line1 
         Index           =   0
         X1              =   150
         X2              =   9300
         Y1              =   900
         Y2              =   900
      End
      Begin VB.Label Label12 
         BackStyle       =   0  'Transparent
         Caption         =   "Cheques"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         Height          =   255
         Index           =   0
         Left            =   150
         TabIndex        =   38
         Top             =   900
         Width           =   2655
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Total en Cheques"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   6300
         TabIndex        =   37
         Top             =   2625
         Width           =   1605
      End
   End
   Begin VB.Frame Marco_Importe_Recibo 
      Enabled         =   0   'False
      Height          =   975
      Left            =   0
      TabIndex        =   21
      Top             =   6750
      Width           =   9495
      Begin VB.TextBox Ret_Gcias 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7950
         MaxLength       =   10
         TabIndex        =   7
         Top             =   225
         Width           =   1365
      End
      Begin VB.TextBox Ret_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4515
         MaxLength       =   10
         TabIndex        =   6
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Importe_Letras 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   8
         Top             =   600
         Width           =   8295
      End
      Begin VB.TextBox Importe 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   10
         TabIndex        =   5
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Ret. Gcias.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   6750
         TabIndex        =   34
         Top             =   225
         Width           =   1110
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Ret. Iva:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   3675
         TabIndex        =   32
         Top             =   240
         Width           =   735
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Son Pesos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Importe:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   120
         TabIndex        =   28
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   22
      Top             =   7650
      Width           =   9495
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   8280
         Picture         =   "Fac_Recibo.frx":0090
         Style           =   1  'Graphical
         TabIndex        =   10
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Fac_Recibo.frx":631A
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Grabar los Datos del Recibo.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Fac_Recibo"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private mcolReciboCheques As New Collection

Private Const mconstListColNumeroCheque = 0
Private Const mconstListColTitular = 1
Private Const mconstListColFechaEmision = 2
Private Const mconstListColFechaCobranza = 3
Private Const mconstListColImporte = 4
Private Const mconstListColNuestraFirma = 5
Private Const mconstListColBancoNombre = 6


'---------------------------------------------------------------------------------
'                           Control de eventos
'---------------------------------------------------------------------------------


Private Sub Buscar_Cuenta_Click()
    MousePointer = 11
    Cuentas_Encontradas.Visible = True
    Cuentas_Encontradas.Clear
    qy = "SELECT * FROM " & IIf(Clientes.Value = True, "Cliente", "Proveedor") & " ORDER BY Nombre "
    AbreRs
    
    While Not Rs.EOF
        Cuentas_Encontradas.AddItem Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields(0))
        Rs.MoveNext
    Wend
    MousePointer = 0
    Cuentas_Encontradas.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Clientes_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub cmdAgregarCheque_Click()
   
    Dim objOrigenPago As New OrigenPago
    
    Fac_Recibo_Cheque.ChequesActuales = mcolReciboCheques
    Fac_Recibo_Cheque.OrigenPagoCheque = objOrigenPago.Cliente
    
    Load Fac_Recibo_Cheque
    Fac_Recibo_Cheque.Show vbModal
    
    If Fac_Recibo_Cheque.CargaConfirmada Then
        
        CargarNuevoCheque Fac_Recibo_Cheque.ChequeCargado
        
        CargarListaCheques
        CalcularTotalRecibo
        
    End If
    
End Sub

Private Sub cmdBorrarCheque_Click()
    
    If Not lvwCheques.SelectedItem Is Nothing Then
    
        Dim Aux As Variant
        
        Aux = Split(lvwCheques.SelectedItem.Key, "|")
        
        lvwCheques.ListItems.Remove lvwCheques.SelectedItem.Index
        
        BorrarCheque CLng(Aux(1)), CLng(Aux(0))
        
        CalcularTotalRecibo
    
    End If
    
End Sub

Private Sub Cuenta_GotFocus()
    Cuenta.Text = Trim(Cuenta.Text)
    Cuenta.SelStart = 0
    Cuenta.SelText = ""
    Cuenta.SelLength = Len(Cuenta.Text)
End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_LostFocus()
    If Val(Cuenta.Text) > 0 Then
        Leer_Cuenta
    End If
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuenta.SetFocus
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Cuenta.Text = Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 32)
    Cuentas_Encontradas.Visible = False
    Leer_Cuenta
End Sub

Private Sub Fecha_GotFocus()
    Numero_GotFocus
    If Fecha.Text = "" Then Fecha.Text = Fecha_Fiscal
    Fecha.SelStart = 0
    Fecha.SelText = ""
    Fecha.SelLength = Len(Fecha.Text)
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
    
    If Fecha.Text = "error" Then
        MsgBox "Error ingresando las fechas, verifique e ingrese nuevamente.", vbInformation, "Atenciσn.!"
        Fecha.Text = ""
        Fecha.SetFocus
    End If
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 7
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Menu.Estado.Panels(2).Text = "Pagos en Cuenta Corriente.-"
    Fecha.Text = Fecha_Fiscal
    
    CrearLista
    CargarNumeroRecibo
End Sub



Private Sub Importe_Change()
    If Val(Importe.Text) > 0 And Val(Fecha.Text) Then
        Grabar.Enabled = True
    Else
        Grabar.Enabled = False
    End If
End Sub

Private Sub Importe_GotFocus()
    CalcularTotalRecibo
    
    Importe.Text = Trim(Importe.Text)
    Importe.SelStart = 0
    Importe.SelText = ""
    Importe.SelLength = Len(Importe.Text)
End Sub

Private Sub Importe_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Importe_LostFocus()
    Importe.Text = Formateado(Str(Val(Importe.Text)), 2, 10, " ", False)
    Importe_Letras.Text = NumerosALetras(Str(Val(Importe.Text)), False, False)
End Sub

Private Sub Numero_GotFocus()
    Numero.SelStart = 0
    Numero.SelText = ""
    Numero.SelLength = Len(Numero.Text)
End Sub

Private Sub Numero_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Numero_LostFocus()
    Numero.Text = Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
    
    If Val(Numero.Text) > 0 Then
        qy = "SELECT * FROM Recibo WHERE Id_Recibo = " & Trim(Str(Val(Numero.Text)))
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        AbreRs
        
        If Not Rs.EOF Then
            MsgBox "El numero de recibo ingresado ya existe en la base de datos!", vbCritical, "Atenciσn.!"
            Numero.SetFocus
        End If
    End If
End Sub

Private Sub Ret_Gcias_GotFocus()
    Ret_Gcias.Text = Trim(Ret_Gcias.Text)
    Ret_Gcias.SelStart = 0
    Ret_Gcias.SelLength = Len(Ret_Gcias.Text)
End Sub

Private Sub Ret_Gcias_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ret_Gcias_LostFocus()
    Ret_Gcias.Text = Formateado(Str(Val(Ret_Gcias.Text)), 2, 9, " ", False)
End Sub

Private Sub Ret_Iva_GotFocus()
    Ret_Iva.Text = Trim(Ret_Iva.Text)
    Ret_Iva.SelStart = 0
    Ret_Iva.SelText = ""
    Ret_Iva.SelLength = Len(Ret_Iva.Text)
End Sub

Private Sub Ret_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Ret_Iva_LostFocus()
    Ret_Iva.Text = Formateado(Str(Val(Ret_Iva.Text)), 2, 10, " ", False)
End Sub

Private Sub Salir_Click()
    If Cuenta.Text = Empty Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub


Private Sub txtTotalEfectivo_GotFocus()
    txtTotalEfectivo.Text = Trim(txtTotalEfectivo.Text)
    txtTotalEfectivo.SelLength = Len(txtTotalEfectivo.Text)
End Sub

Private Sub txtTotalEfectivo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub txtTotalEfectivo_LostFocus()
    txtTotalEfectivo.Text = Formateado(Str(Val(txtTotalEfectivo.Text)), 2, 10, " ", False)
    
    CalcularTotalRecibo
End Sub


'---------------------------------------------------------------------------------
'                           Mιtodos y funciones
'---------------------------------------------------------------------------------


Private Function CalcularImporteEnCheques()

    Dim myCheque As New ReciboCheque
    Dim dblTotalCheque As Double
    
    dblTotalCheque = 0
    
    For Each myCheque In mcolReciboCheques
        
        dblTotalCheque = dblTotalCheque + myCheque.Importe
            
    Next
    
    txtTotalCheques.Text = Formateado(Str(Val(dblTotalCheque)), 2, 10, " ", False)
    
End Function

Private Function CargarNuevoCheque(ChequeCargado As ReciboCheque)

    mcolReciboCheques.Add ChequeCargado

End Function

Private Sub CrearLista()

    Dim ctmX As ColumnHeader
    
    lvwCheques.ColumnHeaders.Clear
    
    Set ctmX = lvwCheques.ColumnHeaders.Add(, , "Nro. Cheque", 1000)
    Set ctmX = lvwCheques.ColumnHeaders.Add(, , "Titular", 3000)
    Set ctmX = lvwCheques.ColumnHeaders.Add(, , "Fecha Emision", 1300)
    Set ctmX = lvwCheques.ColumnHeaders.Add(, , "Fecha Cobro", 1300)
    Set ctmX = lvwCheques.ColumnHeaders.Add(, , "Importe", 1000, 1)
    Set ctmX = lvwCheques.ColumnHeaders.Add(, , "Nuestra Firma", 1000, 1)
    Set ctmX = lvwCheques.ColumnHeaders.Add(, , "Banco", 1000)
    
    Set ctmX = Nothing
    
End Sub

Private Function CargarListaCheques()

    Dim Item As ListItem
    Dim keyLista As String
    Dim objCheque As ReciboCheque
    
    lvwCheques.ListItems.Clear
     
    For Each objCheque In mcolReciboCheques
         
        keyLista = objCheque.NroCheque & "|" & objCheque.Banco
        
        Set Item = lvwCheques.ListItems.Add(, keyLista, objCheque.NroCheque)
         
        With Item
        
            .SubItems(mconstListColTitular) = objCheque.Titular
            .SubItems(mconstListColFechaEmision) = objCheque.FechaEmision
            .SubItems(mconstListColFechaCobranza) = objCheque.FechaCobranza
            .SubItems(mconstListColImporte) = Format(objCheque.Importe, "0.00")
            .SubItems(mconstListColNuestraFirma) = objCheque.NuestraFirma
            .SubItems(mconstListColBancoNombre) = objCheque.BancoNombre
        
        End With
        
    Next
    
End Function

Private Function BorrarCheque(Banco As Long, NumeroCheque As Long)
    
    Dim objCheque As ReciboCheque
    Dim i As Long
    
    i = 0
    
    For Each objCheque In mcolReciboCheques
    
        i = i + 1
    
        If objCheque.Banco = Banco And objCheque.NroCheque = NumeroCheque Then
            
            mcolReciboCheques.Remove (i)
            Exit For
            
        End If
    
    Next

End Function

Private Sub Leer_Cuenta()
    If Val(Cuenta.Text) > 0 Then
        qy = "SELECT " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Cuenta,Cliente.Id_Cliente,Cliente.Nombre,Cliente.Domicilio", "CtaCte_Proveedor.Id_Cuenta,Proveedor.Id_Proveedor,Proveedor.Nombre,Proveedor.Domicilio") & ", SUM(Debe - Haber) AS Saldo_Cuenta FROM " & IIf(Clientes.Value = True, "CtaCte_Cliente,Cliente", "CtaCte_Proveedor,Proveedor") & " WHERE Id_Cuenta = " & Trim(Str(Val(Cuenta.Text))) & " "
        qy = qy & "AND " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Cuenta = Cliente.Id_Cliente", "CtaCte_Proveedor.Id_Cuenta = Proveedor.Id_Proveedor") & " "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "GROUP BY " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Cuenta,Cliente.Id_Cliente,Cliente.Nombre,Cliente.Domicilio", "CtaCte_Proveedor.Id_Cuenta,Proveedor.Id_Proveedor,Proveedor.Nombre,Proveedor.Domicilio")
        AbreRs
        
        If Rs.EOF Then
            MsgBox "La Cuenta es inexistente...", vbInformation, "Atenciσn.!"
            Borrar_Campo_Cuenta
            Cuenta.SetFocus
        Else
            Nombre.Text = Rs.Fields("Nombre")
            Domicilio.Text = Rs.Fields("Domicilio")
            Saldo.Text = Formateado(Str(Val(Rs.Fields("Saldo_Cuenta"))), 2, 10, " ", False)
            
            Mostrar_Facturas_Pendientes
            
            Marco_Facturas.Enabled = True
            Marco_Formas.Enabled = True
            Marco_Importe_Recibo.Enabled = True
            
            Cuenta.Enabled = False
            Buscar_Cuenta.Enabled = False
            
            List1.SetFocus
        End If
    Else
        Cuenta.Text = ""
        Cuenta.SetFocus
    End If
End Sub

Private Sub Mostrar_Facturas_Pendientes()
    Dim Haberes      As Single
    Dim Pagado       As Single
    Dim Pagos        As Single
    Dim Deuda        As Single
    Dim Adeuda       As Single
    Dim Acumulado    As Single
    
    MousePointer = 11
    
    List1.Clear
        
    'Traigo las facturas que han recibido algϊn pago
    qy = "SELECT Recibo_Item.Comprobante, Recibo_Item.Importe, Recibo_Item.Pagado, " & IIf(Clientes.Value = True, "CtaCte_Cliente.Vencimiento, CtaCte_Cliente.Fecha ", "CtaCte_Proveedor.Vencimiento, CtaCte_Proveedor.Fecha ")
    qy = qy & "FROM Recibo, Recibo_Item, " & IIf(Clientes.Value = True, "CtaCte_Cliente ", "CtaCte_Proveedor ")
    qy = qy & "WHERE Recibo.Id_Empresa = Recibo_Item.Id_Empresa "
    qy = qy & "AND Recibo.Id_Empresa = " & Val(Id_Empresa)
    qy = qy & "AND " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Empresa", "CtaCte_Proveedor.Id_Empresa") & " = Recibo.Id_Empresa "
    'Qy = Qy & "AND Recibo.Id_Recibo = Recibo_Item.Id_Recibo "
    qy = qy & "AND Recibo.Cuenta = " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Cuenta ", "CtaCte_Proveedor.Id_Cuenta ")
    qy = qy & "AND Recibo.Cuenta = " & Val(Cuenta.Text) & " "
    qy = qy & "AND Recibo.Tipo_Cuenta = " & IIf(Clientes.Value = True, "'C'", "'P'") & " "
    qy = qy & "AND " & IIf(Clientes.Value = True, "CtaCte_Cliente.Comprobante", "CtaCte_Proveedor.Comprobante") & " = Recibo_Item.Comprobante "
    qy = qy & "AND Recibo_Item.Importe > Recibo_Item.Pagado "
    qy = qy & "GROUP BY Recibo_Item.Comprobante, Recibo_Item.Importe, Recibo_Item.Pagado, " & IIf(Clientes.Value = True, "CtaCte_Cliente.Vencimiento, CtaCte_Cliente.Fecha ", "CtaCte_Proveedor.Vencimiento, CtaCte_Proveedor.Fecha ")
    AbreRs
    
    While Not Rs.EOF
        Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & " "
        Txt = Txt & Trim(Rs.Fields("Comprobante")) + Space(20 - Len(Trim(Rs.Fields("Comprobante")))) & " "
        Txt = Txt & Format(Rs.Fields("Vencimiento"), "dd/mm/yy") & " "
        If Not IsNull(Rs.Fields("Vencimiento")) = True Then
            'Txt = Txt & Formateado(str(Val(DateDiff("d", Date, Rs.Fields("Vencimiento")))), 0, 3, " ", False) & " "
            Txt = Txt & Space(4)
        Else
            Txt = Txt & "            "
        End If
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Importe"))), 2, 10, " ", False)
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Pagado"))), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Importe") - Rs.Fields("Pagado"))), 2, 10, " ", False) & " "
            
        List1.AddItem Txt
        Rs.MoveNext
    Wend
    
    'Traigo las facturas que no han recibido ningun pago
    qy = "SELECT * FROM " & IIf(Clientes.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " "
    qy = qy & "WHERE " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Empresa", "CtaCte_Proveedor.Id_Empresa") & " = " & Val(Id_Empresa) & " "
    qy = qy & "AND " & IIf(Clientes.Value = True, "CtaCte_Cliente.Id_Cuenta", "CtaCte_Proveedor.Id_Cuenta") & " = " & Val(Cuenta.Text) & " "
    qy = qy & "AND Debe > 0 "
    qy = qy & "AND Comprobante NOT IN(SELECT Recibo_Item.Comprobante FROM Recibo INNER JOIN Recibo_Item ON Recibo.Id_Recibo = Recibo_Item.Id_Recibo WHERE Recibo.Cuenta = " & Val(Cuenta.Text) & " AND Recibo.Id_Empresa = Recibo_Item.Id_Empresa AND Recibo.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " AND Recibo.Tipo_Cuenta = '" & IIf(Clientes.Value = True, "C", "P") & "')"

    AbreRs
    
    While Not Rs.EOF
        Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & " "
        Txt = Txt & Trim(Rs.Fields("Comprobante")) + Space(20 - Len(Trim(Rs.Fields("Comprobante")))) & " "
        Txt = Txt & Format(Rs.Fields("Vencimiento"), "dd/mm/yy") & " "
        If Not IsNull(Rs.Fields("Vencimiento")) = True Then
            'Txt = Txt & Formateado(str(Val(DateDiff("d", Date, Rs.Fields("Vencimiento")))), 0, 3, " ", False) & " "
            Txt = Txt & Space(4)
        Else
            Txt = Txt & "            "
        End If
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Debe"))), 2, 10, " ", False)
        Txt = Txt & Formateado(Str(Val(0)), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Debe"))), 2, 10, " ", False)
        
        List1.AddItem Txt
        Rs.MoveNext
    Wend
    
    MousePointer = 0
End Sub

Private Sub Borrar_Campo_Cuenta()
    Nombre.Text = ""
    Cuenta.Text = ""
    Cuenta.Enabled = True
    Buscar_Cuenta.Enabled = True
    Domicilio.Text = ""
    Saldo.Text = ""
    Marco_Importe_Recibo.Enabled = False
End Sub

Private Sub CargarNumeroRecibo()
    
    qy = "SELECT MAX(Id_Recibo) FROM Recibo"
    AbreRs
    
    Numero.Text = 1
    If Not Rs.Fields(0) = "" Then Numero.Text = Val(Rs.Fields(0)) + 1
    
    Numero.Text = Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)

End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()

    Db.BeginTrans
    
    If GrabarRecibo() Then
    
        Db.CommitTrans
        
        If MsgBox("Imprime el comprobante de esta operaciσn. ?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        
'            If Val(Id_Empresa) = 1 Then
'                Imprimir_Hassar
'            End If
        
            ImprimirReciboImpresoraLaser "Original"
            ImprimirReciboImpresoraLaser "Duplicado"
        
        End If

    Else
    
        Db.RollbackTrans
        
        MsgBox "La operaciσn no ha sido confirmada verifique los datos ingresados e intente nuevamente.", vbCritical, "Atenciσn"
        
    End If
    
    Salir_Click
    
End Sub

Private Sub ImprimirReciboImpresoraLaser(Observaciones As String)
    Dim i As Long
    Dim intLineaComprobante As Long
    
    i = 0
    intLineaComprobante = 0
    
    Printer.Font = "Courier New"
    Printer.FontSize = 9
    
    Printer.Print " ++"
    Printer.Print " | " & cEmpresa + Space(30 - Len(cEmpresa)) & "             RECIBO DE PAGO                         NUMERO: " & Numero.Text & " | "
    Printer.Print " |                                                                                   FECHA.: " & Fecha.Text & " | "
    Printer.Print " ++"
    Printer.Print " | CUENTA: " & Cuenta.Text + Space(5 - Len(Cuenta.Text)) & " - " & Nombre.Text + Space(40 - Len(Nombre.Text)) & "                                             | "
    Printer.Print " ++"
    Printer.Print " |                                      Comprobantes Afectados                                          |"
    Printer.Print " ++"
    Printer.Print " | Fecha    Comprobante             Importe                                                             |"
    Printer.Print " ++"
    
    For i = 0 To List1.ListCount - 1
        If List1.Selected(i) Then
        
            intLineaComprobante = intLineaComprobante + 1
            Printer.Print " | " & Mid(List1.List(i), 1, 29) & " " & Mid(List1.List(i), 44, 10) & "                                                             |"
            
        End If
    Next
    
    For i = intLineaComprobante To 10
        Printer.Print " |                                                                                                      |"
    Next
    
    Printer.Print " ++"
    Printer.Print " |                                          Formas de pago                                              |"
    Printer.Print " ++"
    Printer.Print " | EFECTIVO: " & Formateado(Str(Val(txtTotalEfectivo.Text)), 2, 10, " ", False) & "                                                                                 |"
    Printer.Print " | CHEQUE..: " & Formateado(Str(Val(txtTotalCheques.Text)), 2, 10, " ", False) & "                                                                                 |"
    Printer.Print " ++"
    
    If lvwCheques.ListItems.Count > 0 Then
    
        i = 0
        Printer.Print " |++|"
        Printer.Print " || N.Cheque Titular                        Emision    Cobro         Importe Banco                     ||"
        Printer.Print " |++|"
        
        For i = 1 To lvwCheques.ListItems.Count
            Printer.Print " || " & lvwCheques.ListItems(i).Text + Space(8 - Len(lvwCheques.ListItems(i).Text)) & " " & lvwCheques.ListItems(i).SubItems(1) + Space(30 - Len(lvwCheques.ListItems(i).SubItems(1))) & " " & lvwCheques.ListItems(i).SubItems(2) & " " & lvwCheques.ListItems(i).SubItems(3) & " " & Formateado(Str(Val(lvwCheques.ListItems(i).SubItems(4))), 2, 10, " ", True) & " " & Mid(lvwCheques.ListItems(i).SubItems(6), 1, 25) + Space(25 - Len(Mid(lvwCheques.ListItems(i).SubItems(6), 1, 25))) & " ||"
        Next
        For i = lvwCheques.ListItems.Count To 8
            Printer.Print " ||                                                                                                    ||"
        Next
        
        Printer.Print " |++|"
        Printer.Print " ++"
        
    End If
    
    Printer.Print " |                                                                             TOTAL RECIBO: " & Importe.Text & " |"
    Printer.Print " |"; Observaciones + Space(102 - Len(Observaciones)); "|"
    Printer.Print " ++"
    Printer.EndDoc
    
End Sub

Private Function GrabarRecibo() As Boolean

    Dim i           As Long
    Dim l           As Long
    Dim Rec_Asiento As String
    Dim Guarda      As Boolean
    
    On Error GoTo Errores:
    
    Guarda = False
    l = 0
    i = 0
    MousePointer = 11
        
    If Val(Importe.Text) > 0 And Val(Cuenta.Text) > 0 And Val(Fecha.Text) > 0 And Val(Numero.Text) > 0 Then
        For i = 0 To List1.ListCount - 1
            If List1.Selected(i) = True Then
            
                l = l + 1
                
                qy = "SELECT Recibo.*,Recibo_Item.* FROM Recibo, Recibo_Item WHERE Comprobante = '" & Trim(Mid(List1.List(i), 10, 20)) & "' "
                qy = qy & "AND Recibo.Id_Empresa = " & Val(Id_Empresa) & " "
                qy = qy & "AND Recibo.Id_Empresa = Recibo_Item.Id_Empresa "
                qy = qy & "AND Recibo.Id_Recibo = Recibo_Item.Id_Recibo "
                qy = qy & "AND Recibo.Tipo_Cuenta = '" & IIf(Clientes.Value = True, "C", "P") & "'"
                qy = qy & "AND Recibo.Cuenta = " & Trim(Str(Val(Cuenta.Text)))
                AbreRs
                
                If Rs.EOF Then
                
                    qy = "INSERT INTO Recibo_Item VALUES ("
                    qy = qy & Trim(Str(Val(Id_Empresa)))
                    qy = qy & ", " & Trim(Str(Val(Numero.Text)))
                    qy = qy & ", " & Val(i)
                    qy = qy & ", '" & Trim(Mid(List1.List(i), 10, 20)) & "'"
                    qy = qy & ", 'RC X " & Trim(Numero.Text) & "'"
                    qy = qy & ", " & Trim(Str(Val(Mid(List1.List(i), 44, 10))))
                    
                    If Val(Importe.Text) >= Val(Mid(List1.List(i), 44, 10)) Then
                        qy = qy & ", " & Trim(Str(Val(Mid(List1.List(i), 44, 10))))
                    Else
                        If Val(l) = 1 Then
                            qy = qy & ", " & Val(Importe.Text)
                        Else
                            MsgBox "Si el importe del recibo a emitir es menor que el de las facturas adeudadas, sσlo puede elegir una sola facutra a la cual imputar el pago.", vbInformation, "Atenciσn.!"
                            List1.SetFocus
                            MousePointer = 0
                            
                            GrabarRecibo = False
                            
                            Exit Function
                            
                        End If
                    End If
                    
                    qy = qy & ", 0)"
                    
                    Db.Execute (qy)
                Else
                    If l = 1 Then
                        Dim Comp_Asoc As String
                        
                        qy = "SELECT Comprobantes_Adicionales FROM Recibo_Item, Recibo "
                        qy = qy & "WHERE Recibo_Item.Comprobante = '" & Trim(Mid(List1.List(i), 10, 20)) & "' "
                        qy = qy & "AND Recibo_Item.Id_Empresa = " & Val(Id_Empresa) & " "
                        qy = qy & "AND Recibo.Id_Empresa = Recibo_Item.Id_Empresa "
                        qy = qy & "AND Recibo.Id_Recibo = Recibo_Item.Id_Recibo "
                        qy = qy & "AND Recibo.Tipo_Cuenta = '" & IIf(Clientes.Value = True, "C", "P") & "' "
                        qy = qy & "AND Recibo.Cuenta = " & Trim(Str(Val(Cuenta.Text))) & " "
                        AbreRs
                        
                        If Not IsNull(Rs.Fields(0)) = True Then
                            Comp_Asoc = Rs.Fields(0)
                        Else
                            Comp_Asoc = ""
                        End If
                        
                        qy = "UPDATE Recibo_Item SET "
                        qy = qy & "Pagado = Pagado + " & Val(Importe.Text) & ", "
                        If Trim(Comp_Asoc) <> "" Then
                            qy = qy & "Comprobantes_Adicionales = '" & Trim(Comp_Asoc) & ", " & "RC X " & Trim(Numero.Text) & "' "
                        Else
                            qy = qy & "Comprobantes_Adicionales = 'RC X " & Trim(Numero.Text) & "' "
                        End If
                        qy = qy & "FROM Recibo_Item, Recibo "
                        qy = qy & "WHERE Recibo_Item.Comprobante = '" & Trim(Mid(List1.List(i), 10, 20)) & "' "
                        qy = qy & "AND Recibo_Item.Id_Empresa = " & Val(Id_Empresa) & " "
                        qy = qy & "AND Recibo.Id_Empresa = Recibo_Item.Id_Empresa "
                        qy = qy & "AND Recibo.Id_Recibo = Recibo_Item.Id_Recibo "
                        qy = qy & "AND Recibo.Tipo_Cuenta = '" & IIf(Clientes.Value = True, "C", "P") & "' "
                        qy = qy & "AND Recibo.Cuenta = " & Trim(Str(Val(Cuenta.Text))) & " "
                        Db.Execute (qy)
                    
                    End If
                End If
                
                Guarda = True
          
            End If
        Next
        
        If Guarda Then
        
            qy = "INSERT INTO Recibo VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Numero.Text)))
            qy = qy & ", '" & Trim(Fecha.Text) & " " & Trim(Hora_Fiscal) & "'"
            qy = qy & ", '" & IIf(Clientes.Value = True, "C", "P") & "'"
            qy = qy & ", " & Trim(Str(Val(Cuenta.Text)))
            qy = qy & ", " & Trim(Str(Val(Ret_Iva.Text)))
            qy = qy & ", " & Trim(Str(Val(Importe.Text)))
            qy = qy & ", " & Trim(Str(Val(Ret_Gcias.Text))) & ")"
            Db.Execute (qy)
            
        Else
        
            MsgBox "Debe Seleccionar al menos una factura a la cual imputar el pago!", vbCritical, "Atenciσn.!"
            List1.SetFocus
            
            GrabarRecibo = False
            
            Exit Function
        End If
        
        qy = "INSERT INTO " & IIf(Clientes.Value = True, "CtaCte_Cliente", "CtaCte_Proveedor") & " VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Cuenta.Text)))
        qy = qy & ", '" & Trim(Fecha.Text) & " " & Trim(Hora_Fiscal) & "'"
        qy = qy & ", 'RC X " & Trim(Numero.Text) & "'"
        qy = qy & ", 'PAGO A CUENTA CORRIENTE'"
        qy = qy & ", '" & Trim(Fecha.Text) & "'"
        qy = qy & ", 0, " & Trim(Str(Val(Importe.Text))) & ")"
        Db.Execute (qy)
        
        If Val(mcolReciboCheques.Count) > 0 Then
            
            Dim myCheque As New ReciboCheque
            
            For Each myCheque In mcolReciboCheques
            
                qy = "INSERT INTO Cheque VALUES ("
                qy = qy & Trim(Str(Val(Id_Empresa)))
                qy = qy & ", " & Trim(Str(Val(myCheque.NroCheque)))
                qy = qy & ", " & Trim(Str(Val(myCheque.Banco)))
                qy = qy & ", 'RC X " & Trim(Numero.Text) & "'"
                qy = qy & ", '-'"
                qy = qy & ", '" & IIf(Clientes.Value = True, "C", "P") & "'"
                qy = qy & ", '" & Trim(myCheque.Titular) & "'"
                qy = qy & ", " & Val(IIf(myCheque.NuestraFirma, "1", "0"))
                qy = qy & ", '" & Trim(myCheque.FechaEmision) & "'"
                qy = qy & ", " & Val(DateDiff("d", myCheque.FechaEmision, myCheque.FechaCobranza))
                qy = qy & ", '" & Trim(myCheque.FechaCobranza) & "'"
                qy = qy & ", 1"
                qy = qy & ", " & Trim(Str(Val(myCheque.Importe)))
                qy = qy & ", 1, '-')"
                Db.Execute (qy)
                
                qy = "INSERT INTO Recibo_Cheque VALUES ("
                qy = qy & Trim(Str(Val(Id_Empresa)))
                qy = qy & ", " & Trim(Str(Val(Numero.Text)))
                qy = qy & ", " & Trim(Str(Val(myCheque.NroCheque))) & ")"
                Db.Execute (qy)
                
                If Not Clientes.Value Then
            
                    qy = "UPDATE Cheque SET "
                    qy = qy & "Disponible = 2, "
                    qy = qy & "Comprobante_Destino = 'RC X " & Trim(Numero.Text) & "' "
                    qy = qy & "WHERE Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
                    qy = qy & "AND Banco = " & Trim(Str(Val(myCheque.Banco))) & " "
                    qy = qy & "AND Id_Cheque = " & Trim(Str(Val(myCheque.NroCheque)))
                    Db.Execute (qy)
                
                End If
                
            Next
            
        End If
            
        qy = "SELECT MAX(Nro_Asiento) FROM Asiento WHERE Ejercicio = " & Trim(Str(Val(Mid(Fecha.Text, 7, 4)))) & " "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
        'AbreRs
            
        'Rec_Asiento = 1
        'If Not Rs.Fields(0) = "" Then Rec_Asiento = Val(Rs.Fields(0)) + 1
            
        qy = "INSERT INTO Asiento VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
        qy = qy & ", " & Trim(Str(Val(Mid(Fecha.Text, 7, 4))))
        qy = qy & ", '" & Trim(Fecha.Text) & "'"
        qy = qy & ", 1"
        qy = qy & ", 'RC X " & Trim(Numero.Text) & " " & IIf(Clientes.Value = True, "CLIENTE", "PROVEEDOR") & "'"
        qy = qy & ", " & Trim(Str(Val(Importe.Text)))
        qy = qy & ", 0)"
        'Db.Execute (Qy)
        
        qy = "INSERT INTO Asiento VALUES ("
        qy = qy & Trim(Str(Val(Id_Empresa)))
        qy = qy & ", " & Trim(Str(Val(Rec_Asiento)))
        qy = qy & ", " & Trim(Str(Val(Mid(Fecha.Text, 7, 4))))
        qy = qy & ", '" & Trim(Fecha.Text) & "'"
        qy = qy & ", 2"
        If Clientes.Value = True Then
            qy = qy & ", 1, 3, 2, 1, 0, 0"
        Else
            qy = qy & ", 2, 1, 2, 1, 0, 0"
        End If
        qy = qy & ", 'RC X " & Trim(Numero.Text) & " " & IIf(Clientes.Value = True, "CLIENTE", "PROVEEDOR") & "'"
        qy = qy & ", 0"
        qy = qy & ", " & Trim(Str(Val(Importe.Text))) & ")"
        'Db.Execute (Qy)
        
        MousePointer = 0
        
    Else
        MsgBox "Faltan datos en la carga del recibo, verifique e ingrese nuevamente. ", vbCritical, "Atenciσn.!"
        Importe.SetFocus
    End If

    GrabarRecibo = True

    Exit Function
    
Errores:

    GrabarRecibo = False

End Function

Private Sub Imprimir_Hassar()
    Dim i As Long
    
    On Error GoTo Impresora_Apag

Procesar:

    Inicializar_Impresora

    Menu.HASAR1.DatosCliente Trim(Nombre.Text), "00000000", TIPO_DNI, CONSUMIDOR_FINAL, "Domicilio: " & Trim(Domicilio.Text)
    Menu.HASAR1.AbrirComprobanteNoFiscalHomologado RECIBO_X, Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
    For i = 0 To List1.ListCount - 1
        If List1.Selected(i) = True Then
            Menu.HASAR1.ImprimirItem Trim(Mid(List1.List(i), 1, 30)), 1, Val(Mid(List1.List(i), 44, 10)), 0, 0
        End If
    Next
    
    Menu.HASAR1.DetalleRecibo "      Emisiσn         Comprobante                           Importe"
    
    For i = 0 To List1.ListCount - 1
        If List1.Selected(i) = True Then
            Menu.HASAR1.DetalleRecibo Mid(List1.List(i), 1, 30) & " " & Mid(List1.List(i), 44, 10)
        End If
    Next
    
    Menu.HASAR1.DetalleRecibo "."
    Menu.HASAR1.DetalleRecibo "."
    Menu.HASAR1.DetalleRecibo "."
    
'    If Efectivo.Value = True Then
'        Menu.HASAR1.DetalleRecibo "FORMA DE PAGO: EFECTIVO"
'    Else
'        Menu.HASAR1.DetalleRecibo "FORMA DE PAGO: CHEQUE: " & Trim(Cheque.Text)
'        Menu.HASAR1.DetalleRecibo "                BANCO: " & Trim(Mid(Banco.Text, 1, 30))
'    End If
    
    Menu.HASAR1.CerrarComprobanteNoFiscalHomologado
    
    Menu.HASAR1.Finalizar
    
    Exit Sub

Impresora_Apag:

    If MsgBox("Error Impresora:" & Err.Description, vbRetryCancel, "Errores") = vbRetry Then
        Resume Procesar
    End If
End Sub

Private Sub Borrar_Campo()
    Set mcolReciboCheques = New Collection

    CargarNumeroRecibo
    
    Clientes.Value = True
    Borrar_Campo_Cuenta
    List1.Clear
    Importe.Text = Empty
    Importe_Letras.Text = Empty
    Ret_Iva.Text = Empty
    Ret_Gcias.Text = Empty
    
    Marco_Importe_Recibo.Enabled = False
    Marco_Facturas.Enabled = False
    Marco_Formas.Enabled = False
    
    lvwCheques.ListItems.Clear
    
    txtTotalCheques.Text = Empty
    txtTotalEfectivo.Text = Empty
    
    Cuenta.SetFocus
End Sub

Private Function CalcularTotalRecibo()
    
    Dim importeRecibo As Double
    
    CalcularImporteEnCheques
    
    importeRecibo = Val(txtTotalEfectivo.Text) + Val(txtTotalCheques.Text)
    
    Importe.Text = Formateado(Str(Val(importeRecibo)), 2, 10, " ", False)
    
    Importe_Letras.Text = NumerosALetras(Str(Val(Importe.Text)), False, False)
    
End Function

