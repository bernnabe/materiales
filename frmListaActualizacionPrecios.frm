VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCTL.OCX"
Begin VB.Form frmListaActualizacionPrecios 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Lista de actualizaciones de precios"
   ClientHeight    =   7185
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10890
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7185
   ScaleWidth      =   10890
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Botonera 
      Height          =   990
      Left            =   75
      TabIndex        =   13
      Top             =   6150
      Width           =   10740
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   150
         Picture         =   "frmListaActualizacionPrecios.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Confirma la Carga del Resumen de la Cuenta.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   8325
         Picture         =   "frmListaActualizacionPrecios.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Imprimir el Resumen de Cuenta.-"
         Top             =   225
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   9525
         Picture         =   "frmListaActualizacionPrecios.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   9
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
   End
   Begin MSComctlLib.ListView lvwLista 
      Height          =   4590
      Left            =   75
      TabIndex        =   10
      Top             =   1500
      Width           =   10740
      _ExtentX        =   18944
      _ExtentY        =   8096
      View            =   3
      LabelEdit       =   1
      LabelWrap       =   -1  'True
      HideSelection   =   -1  'True
      FullRowSelect   =   -1  'True
      GridLines       =   -1  'True
      _Version        =   393217
      ForeColor       =   -2147483640
      BackColor       =   -2147483643
      BorderStyle     =   1
      Appearance      =   1
      NumItems        =   0
   End
   Begin VB.Frame Frame1 
      Height          =   1440
      Left            =   75
      TabIndex        =   2
      Top             =   0
      Width           =   10740
      Begin VB.OptionButton optActualizadosFueraDeDias 
         Caption         =   "Mostrar art�culos que no hayan sido actualizados dentro de los d�as indicados"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   4575
         TabIndex        =   4
         Top             =   525
         Width           =   5940
      End
      Begin VB.OptionButton optTodos 
         Caption         =   "Mostrar Todos los art�culos"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   4575
         TabIndex        =   6
         Top             =   1125
         Width           =   5940
      End
      Begin VB.OptionButton optNoActualizados 
         Caption         =   "Mostrar s�lo los articulos que nunca hayan sido actualizados"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   4575
         TabIndex        =   5
         Top             =   825
         Width           =   5940
      End
      Begin VB.OptionButton optActualizados 
         Caption         =   "Mostrar art�culos que hayan sido actualizados dentro de los d�as indicados"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   4575
         TabIndex        =   3
         Top             =   225
         Value           =   -1  'True
         Width           =   6015
      End
      Begin VB.TextBox txtDias 
         Height          =   285
         Left            =   825
         MaxLength       =   3
         TabIndex        =   1
         Top             =   600
         Width           =   840
      End
      Begin VB.ComboBox cboRubros 
         Height          =   315
         ItemData        =   "frmListaActualizacionPrecios.frx":12126
         Left            =   825
         List            =   "frmListaActualizacionPrecios.frx":12128
         Style           =   2  'Dropdown List
         TabIndex        =   0
         Top             =   225
         Width           =   3540
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "D�as"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   150
         TabIndex        =   12
         Top             =   600
         Width           =   615
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro"
         ForeColor       =   &H00800000&
         Height          =   240
         Left            =   75
         TabIndex        =   11
         Top             =   225
         Width           =   690
      End
   End
End
Attribute VB_Name = "frmListaActualizacionPrecios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const mconstListColDescripcion = 1
Private Const mconstListColPrecioActual = 2
Private Const mconstListColUltimaActualizacion = 3
'Private Const mconstListColUsuario = 4

Private Sub cmdSalir_Click()
    Unload Me
End Sub

Private Sub cboRubros_KeyPress(KeyAscii As Integer)
    
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"

End Sub

Private Sub Confirma_Click()
    
    Dim lngRubroId As Long

    lngRubroId = 0

    If cboRubros.ListIndex <> -1 Then
    
        lngRubroId = cboRubros.ItemData(cboRubros.ListIndex)
    
    End If

    CargarDatos lngRubroId, Val(txtDias.Text)

End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2

    CrearLista
    CargarCombos
End Sub

Private Sub CargarCombos()
    
    
    Dim rstDatos As New ADODB.Recordset
    Dim qy As String
    
    qy = "Select * From Rubro Order by Denominacion"
    rstDatos.Open qy, Db
    
    While Not rstDatos.EOF
    
        cboRubros.AddItem rstDatos("Denominacion")
        cboRubros.ItemData(cboRubros.NewIndex) = rstDatos("Id_Rubro")
    
        rstDatos.MoveNext
    
    Wend
    
    Set rstDatos = Nothing
    
End Sub

Private Sub CrearLista()

    Dim ctmX As ColumnHeader
    
    lvwLista.ColumnHeaders.Clear
    
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "C�digo", 1200)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Descripci�n", 3500)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Precio Compra Actual", 1700, 1)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Ultima Atualiazci�n", 1700)
 '   Set ctmX = lvwLista.ColumnHeaders.Add(, , "Usuario", 1500, 1)
    
    Set ctmX = Nothing
    
End Sub

Public Function CargarDatos(idRubro As Long, dias As Long)

    Dim rstDatos As New ADODB.Recordset
    Dim Item As ListItem
    Dim keyLista As String
        
    qy = "Select    Art.Id_Articulo, art.Id_Rubro, art.Descripcion, art.Precio_Compra, Max(ahp.Fecha) As UltimaActualizacion From " & _
            "Articulo art " & _
                "Left Join Articulo_Historial_Precio ahp " & _
                    "On  art.Id_Articulo = ahp.Id_Articulo   And " & _
                        " art.Id_Rubro = ahp.Id_Rubro " & _
                    "Left Join Usuario usu On ahp.Usuario = usu.Id_Usuario " & _
            "Where "
            
    If idRubro > 0 Then
        qy = qy & " art.Id_Rubro = " & idRubro & " "
    Else
        qy = qy & " art.Id_Rubro = art.Id_Rubro "
    End If

    If (optActualizados.Value) Then
        qy = qy & " And (ahp.Fecha Is Null Or DateDiff(day, ahp.Fecha, '" & Fecha_Fiscal & "') <= " & dias & ") "
    ElseIf (optActualizadosFueraDeDias.Value) Then
        qy = qy & " And (ahp.Fecha Is Null Or DateDiff(day, ahp.Fecha, '" & Fecha_Fiscal & "') >= " & dias & ") "
    ElseIf optNoActualizados.Value Then
        qy = qy & " And ahp.Fecha Is Null "
    End If
    qy = qy & " Group By Art.Id_Articulo, art.Id_Rubro, art.Descripcion, art.Precio_Compra "
    qy = qy & " Order By art.Descripcion "

    rstDatos.Open qy, Db
      
    lvwLista.ListItems.Clear
    
    While Not rstDatos.EOF
         
        keyLista = rstDatos("Id_Rubro") & "|" & rstDatos("Id_Articulo")
        
        Set Item = lvwLista.ListItems.Add(, keyLista, rstDatos("Id_Articulo"))
         
        With Item
        
            .SubItems(mconstListColDescripcion) = rstDatos("Descripcion")
            .SubItems(mconstListColPrecioActual) = Format(rstDatos("Precio_Compra"), "0.00")
            .SubItems(mconstListColUltimaActualizacion) = IIf(IsNull(rstDatos("UltimaActualizacion")), " ", Format(rstDatos("UltimaActualizacion"), "dd/MM/yyyy hh:mm"))
            '.SubItems(mconstListColUsuario) = IIf(IsNull(rstDatos("Nombre")), " ", rstDatos("Nombre"))
        
        End With
        
        rstDatos.MoveNext
        
    Wend
    
    Set rstDatos = Nothing

End Function

Private Sub lvwLista_DblClick()
    
    Dim lngRubro As Long
    Dim lngArticulo As Long
    Dim Aux
    
    If Not lvwLista.SelectedItem Is Nothing Then
    
        Aux = Split(lvwLista.SelectedItem.Key, "|")
    
        lngRubro = Aux(0)
        lngArticulo = Aux(1)
        
        Load Dat_Arti
        Dat_Arti.Show
        
        Dat_Arti.CargarDatos lngArticulo, lngRubro
        
    End If

End Sub

Private Sub Salir_Click()

    If (lvwLista.ListItems.Count) > 0 Then
        Borrar_Campo
    Else
        Unload Me
    End If
    
End Sub

Private Sub txtDias_GotFocus()
    txtDias.SelLength = Len(txtDias.Text)
End Sub

Private Sub txtDias_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub txtDias_LostFocus()
    txtDias.Text = Val(txtDias.Text)
End Sub

Private Sub Borrar_Campo()

    lvwLista.ListItems.Clear
    
    cboRubros.ListIndex = -1
    txtDias.Text = Empty
    optActualizados.Value = True
    
    cboRubros.SetFocus

End Sub
