VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form frmHistorialPrecios 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Historial de precios"
   ClientHeight    =   4065
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6795
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   4065
   ScaleWidth      =   6795
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Salir 
      Cancel          =   -1  'True
      Height          =   615
      Left            =   5625
      Picture         =   "frmHistorialPrecios.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   2
      ToolTipText     =   "Cancelar - Salir.-"
      Top             =   3375
      Width           =   1095
   End
   Begin VB.Frame Frame1 
      Height          =   3315
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   6765
      Begin MSComctlLib.ListView lvwLista 
         Height          =   3015
         Left            =   150
         TabIndex        =   1
         Top             =   225
         Width           =   6540
         _ExtentX        =   11536
         _ExtentY        =   5318
         View            =   3
         LabelEdit       =   1
         LabelWrap       =   -1  'True
         HideSelection   =   -1  'True
         FullRowSelect   =   -1  'True
         GridLines       =   -1  'True
         _Version        =   393217
         ForeColor       =   -2147483640
         BackColor       =   -2147483643
         BorderStyle     =   1
         Appearance      =   1
         NumItems        =   0
      End
   End
End
Attribute VB_Name = "frmHistorialPrecios"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Const mconstListColValorAnterior = 1
Private Const mconstListColValorActual = 2
Private Const mconstListColUsuario = 3

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2

    CrearLista
End Sub

Private Sub CrearLista()

    Dim ctmX As ColumnHeader
    
    lvwLista.ColumnHeaders.Clear
    
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Fecha", 1500)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Valor Anterior", 1300, 1)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Nuevo Valor", 1300, 1)
    Set ctmX = lvwLista.ColumnHeaders.Add(, , "Usuario", 1800)
    
    Set ctmX = Nothing
    
End Sub

Public Function CargarDatos(idArticulo As Long, idRubro As Long)

    Dim rstDatos As New ADODB.Recordset
    Dim Item As ListItem
    Dim keyLista As String
        
    qy = "Select Distinct * From Articulo_Historial_Precio, Usuario Where Id_Articulo = " & idArticulo & " "
    qy = qy & " And Id_Rubro = " & idRubro & " "
    qy = qy & " And Usuario.Id_Usuario = Articulo_Historial_Precio.Usuario "
    qy = qy & " Order by Fecha Desc "
    rstDatos.Open qy, Db
    
    lvwLista.ListItems.Clear
     
    While Not rstDatos.EOF
         
        keyLista = rstDatos("Id_Rubro") & "|" & rstDatos("Id_Articulo") & "|" & Format(rstDatos("Fecha"))
        
        Set Item = lvwLista.ListItems.Add(, keyLista, Format(rstDatos("Fecha"), "dd/MM/yyyy hh:mm"))
         
        With Item
        
            .SubItems(mconstListColValorAnterior) = Format(rstDatos("ValorAnterior"), "0.00")
            .SubItems(mconstListColValorActual) = Format(rstDatos("ValorActual"), "0.00")
            .SubItems(mconstListColUsuario) = rstDatos("Nombre")
        
        End With
        
        rstDatos.MoveNext
        
    Wend

End Function

Private Sub Salir_Click()
    Unload Me
End Sub
