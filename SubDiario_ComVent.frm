VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form SubDiario_ComVent 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Sub Diario de Compras - Ventas.-"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   225
   ClientWidth     =   11295
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   11295
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Periodo 
      Height          =   1095
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11295
      Begin VB.ComboBox Proveedor 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6720
         TabIndex        =   4
         Top             =   600
         Width           =   4455
      End
      Begin VB.ComboBox Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   6720
         TabIndex        =   5
         Top             =   600
         Width           =   4455
      End
      Begin VB.TextBox Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9840
         MaxLength       =   10
         TabIndex        =   3
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Desde 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6720
         MaxLength       =   10
         TabIndex        =   2
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton Compras 
         Caption         =   "Sub Diaro &Compras"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.OptionButton Ventas 
         Caption         =   "Sub Diario &Ventas"
         ForeColor       =   &H00800000&
         Height          =   375
         Left            =   120
         TabIndex        =   1
         Top             =   600
         Width           =   1815
      End
      Begin VB.Label Etiqueta 
         Alignment       =   1  'Right Justify
         Caption         =   "Proveedor:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5400
         TabIndex        =   14
         Top             =   600
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Hasta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8880
         TabIndex        =   13
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Desde:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5760
         TabIndex        =   12
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Libro 
      Enabled         =   0   'False
      Height          =   5055
      Left            =   0
      TabIndex        =   10
      Top             =   1080
      Width           =   11295
      Begin MSFlexGridLib.MSFlexGrid Lista 
         Height          =   4695
         Left            =   120
         TabIndex        =   15
         Top             =   240
         Width           =   11055
         _ExtentX        =   19500
         _ExtentY        =   8281
         _Version        =   393216
         Cols            =   12
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         FillStyle       =   1
         SelectionMode   =   1
         AllowUserResizing=   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   11
      Top             =   6120
      Width           =   11295
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8640
         Picture         =   "SubDiario_ComVent.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   16
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "SubDiario_ComVent.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Confirma la Carga del Sub Diario.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7560
         Picture         =   "SubDiario_ComVent.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Imprimir Libro de IVA.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   10080
         Picture         =   "SubDiario_ComVent.frx":C80E
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Cancelar - Salir"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "SubDiario_ComVent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Compras_Click()
    If Compras.Value = True Then
        Armar_Lista
        Etiqueta.Caption = "Proveedor:"
        Cliente.Visible = False
        Proveedor.Visible = True
        
        Cliente.Text = ""
        Proveedor.Text = ""
    Else
        Armar_Lista
        Etiqueta.Caption = "Cliente:"
        Proveedor.Visible = False
        Cliente.Visible = True
        
        Cliente.Text = ""
        Proveedor.Text = ""
    End If
End Sub

Private Sub Compras_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    If Val(Desde.Text) > 0 And Val(Hasta.Text) > 0 Then
        
        Dim Gravado   As String
        Dim Exento    As String
        Dim Iva       As String
        Dim Perc      As String
        Dim Ret_Iva   As String
        Dim Total     As String
        Dim i         As Long
        
        Lista.Clear
        Armar_Lista
        Marco_Libro.Enabled = True
        
        qy = "SELECT " & IIf(Compras.Value = True, "Proveedor.*,Factura_Compra.*", "Cliente.*,Factura_Venta.*") & " FROM "
        qy = qy & IIf(Compras.Value = True, "Proveedor,Factura_Compra", "Cliente,Factura_Venta") & " WHERE (Fecha BETWEEN CONVERT(DATETIME, '" & Format(Desde.Text, "YYYY-MM-DD") & " 00:00:00', 102) AND CONVERT(DATETIME, '" & Format(Hasta.Text, "YYYY-MM-DD") & " 23:59:59.99', 102))"
        If Compras.Value = True Then
            qy = qy & "AND Factura_Compra.Proveedor = Proveedor.Id_Proveedor "
            If Val(Mid(Proveedor.List(Proveedor.ListIndex), 31)) > 0 Then qy = qy & "AND Factura_Compra.Proveedor = " & Val(Mid(Proveedor.List(Proveedor.ListIndex), 31)) & " "
        Else
            qy = qy & "AND Factura_Venta.Cliente = Cliente.Id_Cliente "
            If Val(Mid(Cliente.List(Cliente.ListIndex), 31)) > 0 Then qy = qy & "AND Factura_Venta.Cliente= " & Val(Mid(Cliente.List(Cliente.ListIndex), 31)) & " "
        End If
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "ORDER BY Fecha, Id_Tipo_Factura, Id_Letra_Factura, Id_Centro_Emisor, Id_Nro_Factura, Nombre"
        AbreRs
        Lista.Rows = 2
        
        For i = 1 To Rs.RecordCount
            Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & Chr(9)
            Txt = Txt & Trim(Rs.Fields("Id_Tipo_Factura")) & " " & Trim(Rs.Fields("Id_Letra_Factura")) & " " & Formateado(Str(Val(Rs.Fields("Id_Centro_Emisor"))), 0, 4, "0", False) & "-" & Formateado(Str(Val(Rs.Fields("Id_Nro_Factura"))), 0, 10, "0", False) + Space(20 - Len(Trim(Rs.Fields("Id_Tipo_Factura")) & " " & Trim(Rs.Fields("Id_Letra_Factura")) & " " & Formateado(Str(Val(Rs.Fields("Id_Centro_Emisor"))), 0, 4, "0", False) & "-" & Formateado(Str(Val(Rs.Fields("Id_Nro_Factura"))), 0, 10, "0", False))) & Chr(9)
            Txt = Txt & Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & Chr(9)
            Txt = Txt & Trim(Rs.Fields("Condicion_IVA")) + Space(2 - Len(Trim(Rs.Fields("Condicion_Iva")))) & Chr(9)
            Txt = Txt & Trim(Rs.Fields("Nro")) + Space(13 - Len(Trim(Rs.Fields("Nro")))) & Chr(9)
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Gravado"))), 2, 10, " ", False) & Chr(9): Gravado = Val(Gravado) + Val(Rs.Fields("Gravado"))
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Exento"))), 2, 10, " ", False) & Chr(9): Exento = Val(Exento) + Val(Rs.Fields("Exento"))
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Iva"))), 2, 10, " ", False) & Chr(9): Iva = Val(Iva) + Val(Rs.Fields("Iva"))
            Txt = Txt & Formateado(Str(Val(0)), 2, 10, " ", False) & Chr(9): Perc = 0
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Ret_Iva"))), 2, 10, " ", False) & Chr(9): Ret_Iva = Val(Ret_Iva) + Val(Rs.Fields("Ret_Iva"))
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Total_Factura"))), 2, 10, " ", False): Total = Val(Total) + Val(Rs.Fields("Total_Factura"))
            
            Lista.AddItem Txt, i
            Rs.MoveNext
        Next
        
        Lista.AddItem Chr(9) & Chr(9) & Chr(9) & Chr(9) & "         TOTALES:" & Chr(9) & Formateado(Str(Val(Gravado)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Exento)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Iva)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Perc)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Ret_Iva)), 2, 10, " ", True) & Chr(9) & Formateado(Str(Val(Total)), 2, 10, " ", True), i + 1
        Imprimir.Enabled = True
        Lista.SetFocus
    Else
        MsgBox "Error en las fechas ingresadas, verifique e ingrese nuevamente.", vbInformation, "Atenciσn.!"
        Desde.Text = ""
        Hasta.Text = ""
        Desde.SetFocus
    End If
End Sub

Private Sub Desde_GotFocus()
    If Desde.Text = "error" Or Trim(Desde.Text) = "" Then Desde.Text = Fecha_Fiscal
    Desde.Text = Trim(Desde.Text)
    Desde.SelStart = 0
    Desde.SelText = ""
    Desde.SelLength = Len(Desde.Text)
End Sub

Private Sub Desde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_LostFocus()
    Desde.Text = ValidarFecha(Desde.Text)
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 12
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Menu.Estado.Panels(2).Text = "Sub Diario de Compras y Ventas.-"
    
    Cargar_Cliente
    
    Armar_Lista
End Sub

Private Sub Cargar_Cliente()
    Cliente.Clear
    Proveedor.Clear
    
    qy = "SELECT * FROM Cliente ORDER BY Nombre"
    AbreRs
        
    While Not Rs.EOF
        Cliente.AddItem Trim(Rs.Fields("Nombre")) + Space(31 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields("Id_Cliente"))
        Rs.MoveNext
    Wend
    
    qy = "SELECT * FROM Proveedor ORDER BY Nombre"
    AbreRs
        
    While Not Rs.EOF
        Proveedor.AddItem Trim(Rs.Fields("Nombre")) + Space(31 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields("Id_Proveedor"))
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Hasta_GotFocus()
    If Hasta.Text = "error" Or Trim(Hasta.Text) = "" Then Hasta.Text = Fecha_Fiscal
    Hasta.Text = Trim(Hasta.Text)
    Hasta.SelStart = 0
    Hasta.SelText = ""
    Hasta.SelLength = Len(Hasta.Text)
End Sub

Private Sub Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_LostFocus()
    Hasta.Text = ValidarFecha(Hasta.Text)
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
    
    'If Lista.ListCount > 0 Then
        
     '   i = 0
     '   l = i
     '
     '   Printer.Font = "Courier New"
     '   Printer.FontSize = 6.5
     '
     '   Imprimir_Encabezado
     '
     '   For i = 0 To List1.ListCount - 1
     '       If l <= 74 Then
     '           Printer.Print " " & mid(List1.List(i), 1, 250)
     '           l = l + 1
     '       Else
     '           Printer.NewPage
     '           Imprimir_Encabezado
     '           l = 0
     '       End If
     '   Next
     '
     '   Printer.Print " "
     '   Printer.Print " "; List2.List(0)
     '   Printer.EndDoc
    'End If
    
    'Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
'    Imprimir.Tag = Int(List1.ListCount / 74) + 1
'    Printer.Print " " & trim(ucase(cEmpresa)) & "                                                                                              Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
'    Printer.Print "                                                                                                                       Fecha..: " & Format(Now, "dd/mm/yyyy")
'    Printer.Print "                                                                                                                       Hora...: " & Format(Time, "hh.mm"); ""
'    Printer.Print
'    Printer.Print
'    Printer.Print
'    Printer.Print "                                                 SUBDIARIO DE " & IIf(Compras.Value = True, "COMPRAS", "VENTAS")
'    Printer.Print "Desde:" & trim(Desde.Text)
'    Printer.Print "Hasta:" & trim(Hasta.Text)
'    Printer.Print " "
'    If Compras.Value = True Then
'        Printer.Print "    Fecha Comprobante          Nombre                         Iva C.U.I.T.        Gravado     Exento        Iva        P5&    Ret.Iva      Total"
'    Else
'        Printer.Print "    Fecha Comprobante          Nombre                         Iva C.U.I.T.        Gravado     Exento        Iva         NC    Ret.Iva      Total"
'    End If
'    Printer.Print " "
End Sub

Private Sub List1_DblClick()
'    If trim(mid(List1.List(List1.ListIndex), 10, 2)) = "FC" Or trim(mid(List1.List(List1.ListIndex), 10, 2)) = "NC" Or trim(mid(List1.List(List1.ListIndex), 10, 2)) = "ND" Then
'        Detalle_Factura.Comprobante.Text = trim(mid(List1.List(List1.ListIndex), 10, 2)) & " "
'        Detalle_Factura.Comprobante.Text = Detalle_Factura.Comprobante.Text & trim(mid(List1.List(List1.ListIndex), 13, 1)) & " "
'        Detalle_Factura.Comprobante.Text = Detalle_Factura.Comprobante.Text & trim(mid(List1.List(List1.ListIndex), 15, 15)) & " "
'
'        Detalle_Factura.Importe.Text = mid(List1.List(List1.ListIndex), 134, 10) & " "
'        Detalle_Factura.Codigo.Text = "00000"
'        Detalle_Factura.Codigo.Tag = IIf(Compras.Value = True, "Proveedor", "Cliente")
'        Detalle_Factura.Nombre.Text = mid(List1.List(List1.ListIndex), 31, 25)
'    End If
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: List1_DblClick
End Sub

Private Sub Lista_DblClick()
    
    If Lista.RowSel > 0 Then
        
        MostrarDetalleFactura
        
    End If

End Sub

Private Sub MostrarDetalleFactura()

    Detalle_Factura.Show
    
    If Ventas.Value Then
        
        Detalle_Factura.Codigo.Tag = "Cliente"
        
    Else
        
        Detalle_Factura.Codigo.Tag = "Proveedor"
        
    End If
    
    Detalle_Factura.Comprobante.Text = Lista.TextMatrix(Lista.RowSel, 1)
    Detalle_Factura.Nombre.Text = Lista.TextMatrix(Lista.RowSel, 2)
    Detalle_Factura.Importe.Text = Lista.TextMatrix(Lista.RowSel, 12)
    
    Detalle_Factura.Confirma_Click
    
End Sub

Private Sub Proveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If Desde.Text = "" And Hasta.Text = "" And Compras.Value = True Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Desde.Text = ""
    Hasta.Text = ""
    Lista.Clear
    Armar_Lista
    Compras.Value = True
    Marco_Libro.Enabled = False
    Imprimir.Enabled = False
    
    Compras.SetFocus
End Sub

Private Sub Ventas_Click()
    If Compras.Value = True Then
        Etiqueta.Caption = "Proveedor:"
        Cliente.Visible = False
        Proveedor.Visible = True
        Armar_Lista
    Else
        Armar_Lista
        Etiqueta.Caption = "Cliente:"
        Proveedor.Visible = False
        Cliente.Visible = True
    End If
End Sub

Private Sub Ventas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Armar_Lista()
    Lista.Clear
    
    Lista.Cols = 11
    Lista.ColWidth(0) = 900
    Lista.ColWidth(1) = 1900
    Lista.ColWidth(2) = 3700
    Lista.ColWidth(3) = 450
    Lista.ColAlignment(4) = 6
    Lista.ColWidth(4) = 1200
    Lista.ColAlignment(5) = 6
    Lista.ColWidth(5) = 900
    Lista.ColAlignment(6) = 6
    Lista.ColWidth(6) = 900
    Lista.ColAlignment(7) = 6
    Lista.ColWidth(7) = 900
    Lista.ColAlignment(8) = 6
    Lista.ColWidth(8) = 900
    Lista.ColAlignment(9) = 6
    Lista.ColWidth(9) = 900
    Lista.ColAlignment(10) = 6
    Lista.ColWidth(10) = 900
    
    Lista.Rows = 1
    Lista.Col = 0
    Lista.Row = 0
    
    Lista.Text = "Fecha"
    Lista.Col = 1
    Lista.Text = "Comprobante"
    Lista.Col = 2
    Lista.Text = IIf(Ventas.Value = True, "Cliente", "Proveedor")
    Lista.Col = 3
    Lista.Text = "Iva"
    Lista.Col = 4
    Lista.Text = "Cuit"
    Lista.Col = 5
    Lista.Text = "Gravado"
    Lista.Col = 6
    Lista.Text = "Exento"
    Lista.Col = 7
    Lista.Text = "IVA"
    Lista.Col = 8
    Lista.Text = IIf(Ventas.Value = True, "Perc. 5%", "Perc. NC")
    Lista.Col = 9
    Lista.Text = "Ret_Iva"
    Lista.Col = 10
    Lista.Text = "TOTAL"
End Sub

