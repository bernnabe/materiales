VERSION 5.00
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomctl.ocx"
Begin VB.Form Lst_Stk 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Listado de Existencias.-"
   ClientHeight    =   6615
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11175
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6615
   ScaleWidth      =   11175
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Articulos 
      Height          =   1095
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   11175
      Begin VB.ComboBox Proveedor 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7080
         TabIndex        =   3
         Text            =   "    0 TODOS LOS PROVEEDORES"
         Top             =   240
         Width           =   3975
      End
      Begin VB.ComboBox Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   7080
         TabIndex        =   4
         Text            =   "    0 TODOS LOS RUBROS"
         Top             =   600
         Width           =   3975
      End
      Begin VB.OptionButton Numerico 
         Caption         =   "ﾓrden Num駻ico."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Value           =   -1  'True
         Width           =   1815
      End
      Begin VB.OptionButton Alfabetico 
         Caption         =   "ﾓrden Alfab騁ico."
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   1815
      End
      Begin VB.OptionButton Orden_Rubro 
         Caption         =   "Ordenado por Rubro."
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   1815
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Proveedor:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5280
         TabIndex        =   8
         Top             =   240
         Width           =   1695
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5280
         TabIndex        =   7
         Top             =   600
         Width           =   1695
      End
   End
   Begin VB.Frame Marco_Lista 
      Height          =   4575
      Left            =   0
      TabIndex        =   9
      Top             =   1080
      Width           =   11175
      Begin VB.Frame Marco_Progreso 
         Height          =   1575
         Left            =   3120
         TabIndex        =   11
         Top             =   1560
         Visible         =   0   'False
         Width           =   5655
         Begin MSComctlLib.ProgressBar Barra 
            Height          =   255
            Left            =   120
            TabIndex        =   12
            Top             =   1200
            Width           =   5415
            _ExtentX        =   9551
            _ExtentY        =   450
            _Version        =   393216
            Appearance      =   1
         End
         Begin VB.Label Label5 
            Caption         =   "Aguarde un momento..."
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   14
            Top             =   120
            Width           =   5415
         End
         Begin VB.Label Label4 
            Caption         =   "0%                                                Progreso                                             100%"
            ForeColor       =   &H00800000&
            Height          =   255
            Left            =   120
            TabIndex        =   13
            Top             =   960
            Width           =   5415
         End
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4110
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   10935
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         Caption         =   $"Lst_Stk.frx":0000
         ForeColor       =   &H00800000&
         Height          =   195
         Index           =   0
         Left            =   120
         TabIndex        =   10
         Top             =   120
         Width           =   10095
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   15
      Top             =   5640
      Width           =   11175
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8520
         Picture         =   "Lst_Stk.frx":00B8
         Style           =   1  'Graphical
         TabIndex        =   19
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   9960
         Picture         =   "Lst_Stk.frx":0A2A
         Style           =   1  'Graphical
         TabIndex        =   18
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   7440
         Picture         =   "Lst_Stk.frx":6CB4
         Style           =   1  'Graphical
         TabIndex        =   17
         ToolTipText     =   "Imprimir Lista.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Lst_Stk.frx":C8C6
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Confirma la Carga de la Lista.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Lst_Stk"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Alfabetico_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    Dim Medida     As String
    Dim cPrecio    As Single
    Dim rRubro     As Long
    Dim i          As Long
    Dim Prog       As String
    Dim Precio_Art As Single
    
    List1.Clear
    MousePointer = 11
    Barra.Value = 0
    
    Marco_Progreso.Visible = True
    Marco_Progreso.Refresh
    
    qy = "SELECT Articulo.*,Rubro.* FROM Articulo,Rubro "
    'Qy = Qy & "WHERE Integra_Listado = 1 "
    qy = qy & "WHERE Articulo.Id_Rubro = Rubro.Id_Rubro "
    If Val(Proveedor.Text) > 0 Then qy = qy & "AND Proveedor = " & Trim(Str(Val(Proveedor.Text))) & " "
    If Val(Mid(Rubro.Text, 1, 5)) > 0 Then qy = qy & "AND Articulo.Id_Rubro = " & Trim(Str(Val(Mid(Rubro.Text, 1, 5)))) & " "
    If Numerico.Value = True Then
        qy = qy & "ORDER BY Rubro.Id_Rubro, Id_Articulo, Articulo.Descripcion "
    ElseIf Alfabetico.Value = True Then
        qy = qy & "ORDER BY Rubro.Denominacion, Rubro.Id_Rubro, Articulo.Descripcion "
    ElseIf Orden_Rubro.Value = True Then
        qy = qy & "ORDER BY Rubro.Id_Rubro, Rubro.Denominacion, Articulo.Descripcion "
    End If
    AbreRs
    If Not Rs.EOF Then
        Prog = Val(100 / Rs.RecordCount)
        rRubro = Val(Rs.Fields("Id_Rubro"))
        List1.AddItem Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 9, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
        List1.AddItem "ｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯ"
    End If
    
    While Not Rs.EOF
        If Val(rRubro) = Val(Rs.Fields("Id_Rubro")) Then
            Txt = Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 4, " ", False)
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Articulo"))), 0, 5, " ", False) & " "
            
            If Val(Rs.Fields("Presentacion_Rev")) = 5 Or Val(Rs.Fields("Presentacion_Rev")) = 4 Or Val(Rs.Fields("Presentacion_Rev")) = 3 Then
                Txt = Txt & Trim(Mid(Rs.Fields("Descripcion"), 1, 28) & "x" & Formateado(Str(Val(Rs.Fields("Ancho"))), 2, 0, "", False) & "x" & Formateado(Str(Val(Rs.Fields("Largo"))), 2, 0, "", False)) + Space(40 - Len(Trim(Mid(Rs.Fields("Descripcion"), 1, 28) & "x" & Formateado(Str(Val(Rs.Fields("Ancho"))), 2, 0, "", False) & "x" & Formateado(Str(Val(Rs.Fields("Largo"))), 2, 0, "", False))))
            Else
                Txt = Txt & Trim(Mid(Rs.Fields("Descripcion"), 1, 40)) + Space(40 - Len(Trim(Mid(Rs.Fields("Descripcion"), 1, 40))))
            End If
            
            Txt = Txt & " "
            
            If Val(Rs.Fields("Presentacion_Rev")) = 3 Or Val(Rs.Fields("Presentacion_Rev")) = 4 Or Val(Rs.Fields("Presentacion_Rev")) = 5 Then
                If Val(Rs.Fields("Presentacion_Rev")) = 3 Then
                    Medida = Formateado(Str(Val(CalcMed(Rs.Fields("Espesor"), Rs.Fields("Ancho"), Rs.Fields("Largo"), False))), 2, 8, " ", True) & " "
                ElseIf Val(Rs.Fields("Presentacion_Rev")) = 4 Then
                    Medida = Formateado(Str(Val(CalcMed(Rs.Fields("Espesor"), Rs.Fields("Ancho"), Rs.Fields("Largo"), True))), 2, 8, " ", False) & " "
                ElseIf Val(Rs.Fields("Presentacion_Rev")) = 5 Then
                    Medida = Formateado(Str(Val(Rs.Fields("Largo"))), 2, 8, " ", False) & " "
                End If
            Else
                Medida = Formateado(Str(Val(0)), 2, 8, " ", False) & " "
            End If
            
            Txt = Txt & IIf(Val(Medida) > 0, Medida, Space(9))
            
            If Val(Rs.Fields("Presentacion_Rev")) = 0 Then
                Txt = Txt & Space(5) & " "
            ElseIf Val(Rs.Fields("Presentacion_Rev")) = 5 Then
                Txt = Txt & Trim(Mid(Articulo_Presentacion(Val(Rs.Fields("Presentacion_Rev"))), 5, 2)) + Space(5 - Len(Trim(Mid(Articulo_Presentacion(Val(Rs.Fields("Presentacion_Rev"))), 5, 2)))) & " "
            Else
                Txt = Txt & Trim(Mid(Articulo_Presentacion(Val(Rs.Fields("Presentacion_Rev"))), 5, 5)) + Space(5 - Len(Trim(Mid(Articulo_Presentacion(Val(Rs.Fields("Presentacion_Rev"))), 5, 5)))) & " "
            End If
            
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Existencia"))), 2, 9, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Stock_Minimo"))), 2, 9, " ", False)
            
            Precio_Art = Formateado(Str(Val(Val(Rs.Fields("Precio_Compra")) * Val(Rs.Fields("Margen_Rev")) / 100) + Val(Rs.Fields("Precio_Compra"))), 3, 9, " ", False)
            
            If Val(Rs.Fields("Presentacion_Rev")) = 5 Then  'ML
                cPrecio = (((Val(Rs.Fields("Espesor")) * Val(Rs.Fields("Ancho"))) * 0.2734) * Val(Precio_Art)) * Val(Rs.Fields("Largo"))
                cPrecio = ((Val(cPrecio) * Val(Alicuota_Iva)) / 100) + Val(cPrecio)
                Txt = Txt & Formateado(Str(Val(cPrecio)), 3, 9, " ", False) & " "
            ElseIf Val(Rs.Fields("Presentacion_Rev")) = 4 Then 'M2
                cPrecio = ((Val(Rs.Fields("Ancho"))) * 0.0254) * Val(Precio_Art) * Val(Rs.Fields("Largo"))
                cPrecio = ((Val(cPrecio) * Val(Alicuota_Iva)) / 100) + Val(cPrecio)
                Txt = Txt & Formateado(Str(Val(cPrecio)), 3, 9, " ", False) & " "
            Else
                Txt = Txt & Formateado(Str((Val(Precio_Art)) * Alicuota_Iva / 100) + (Val(Precio_Art)), 3, 9, " ", False) & " "
            End If
                
            If (Val(Barra.Value) + Val(Prog)) < 100 Then
                Barra.Value = Barra.Value + Val(Prog)
            End If
            
            List1.AddItem Txt
            Rs.MoveNext
        Else
            List1.AddItem ""
            rRubro = Val(Rs.Fields("Id_Rubro"))
            Txt = Formateado(Str(Val(Rs.Fields("id_Rubro"))), 0, 9, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
            List1.AddItem Txt
            List1.AddItem "ｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯｯ"
        End If
    Wend
    
    Barra.Value = 100
    Marco_Progreso.Visible = False
    MousePointer = 0
    List1.SetFocus
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Listado de Existencias.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 5
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Rubro_Proveedor
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Imprimir_Click()
    Dim i As Long
    Dim l As Long
    
    If List1.ListCount > 0 Then
        
        i = 0
        l = 0
        Printer.Font = "Courier New"
        Printer.FontSize = 9
        
        Imprimir_Encabezado
        
        For i = 0 To List1.ListCount - 1
            
            If l <= 69 Then
                Printer.Print " " & Mid(List1.List(i), 1, 170)
                l = l + 1
            Else
                Printer.Print " 覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧"
                Printer.Print " Los Precios de lista incluyen la al�cuota de IVA (" & Formateado(Str(Val(Alicuota_Iva)), 2, 0, "", False) & "%) "
                
                l = 0
                Printer.NewPage
                Imprimir_Encabezado
            End If
        Next
        
        Printer.Print " 覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧"
        Printer.Print " Los Precios de lista incluyen la al�cuota de IVA (" & Formateado(Str(Val(Alicuota_Iva)), 2, 0, "", False) & "%) "
        Printer.EndDoc
    End If
    
    Salir.SetFocus
End Sub

Private Sub Imprimir_Encabezado()
    Dim Titulo As String
    
    Titulo = "                                            LISTADO DE EXISTENCIAS "
    If Numerico.Value = True Then
        Titulo = Titulo & "POR ORDEN NUMﾉRICO."
    ElseIf Alfabetico.Value = True Then
        Titulo = Titulo & "POR ORDEN ALFABﾉTICO."
    ElseIf Orden_Rubro.Value = True Then
        Titulo = Titulo & "ORDENADO POR RUBROS."
    End If
    
    Imprimir.Tag = Int(List1.ListCount / 69) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                      P疊ina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                   Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                             Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print " TELﾉFONO / FAX: 02343 - 440304"
    Printer.Print " e-mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print Titulo
    Printer.Print
    Printer.Print " 覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧"
    Printer.Font = "MS Sans Serif"
    Printer.FontSize = 8.5
    Printer.Print "      C�digo Art.  Descripci�n                                                                                   Medida  Present.    Existencia   Pto. M�nimo        Final Ctdo."
    Printer.Font = "Courier New"
    Printer.FontSize = 9
    Printer.Print " 覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧覧"
End Sub

Private Sub Numerico_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Orden_Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Proveedor_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Proveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Rubro_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        If Val(Rubro.Text) > 0 Then
            qy = "SELECT * FROM Rubro WHERE Id_Rubro = " & Trim(Str(Val(Rubro.Text)))
            AbreRs
            
            If Not Rs.EOF Then
                Rubro.Text = Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
            Else
                Rubro.Text = ""
                Rubro.SetFocus
            End If
        End If
        
        SendKeys "{TAB}"
    End If
End Sub

Private Sub Rubro_LostFocus()
    If Val(Mid(Rubro.Text, 1, 5)) = 0 Then Rubro.Text = "    0 TODOS LOS RUBROS"
End Sub

Private Sub Salir_Click()
    If Val(Proveedor.Text) = 0 And Val(Mid(Rubro.Text, 1, 5)) = 0 And List1.ListCount = 0 Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    List1.Clear
    Numerico.Value = True
    Proveedor.Text = "    0 TODOS LOS PROVEEDORES"
    Rubro.Text = ""
    Rubro_LostFocus
        
    Numerico.SetFocus
End Sub

Private Sub Cargar_Rubro_Proveedor()
    qy = "SELECT * FROM Rubro ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Rubro.AddItem Formateado(Str(Val(Rs.Fields("Id_Rubro"))), 0, 5, " ", False) & " " & Trim(Rs.Fields("Denominacion"))
        Rs.MoveNext
    Wend
    
    qy = "SELECT * FROM Proveedor ORDER BY Nombre"
    AbreRs
    
    While Not Rs.EOF
        Proveedor.AddItem Formateado(Str(Val(Rs.Fields("ID_Proveedor"))), 0, 5, " ", False) & " " & Trim(Rs.Fields("Nombre"))
        Rs.MoveNext
    Wend
End Sub
