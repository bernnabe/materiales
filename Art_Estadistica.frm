VERSION 5.00
Begin VB.Form Art_Estadistica 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consulta estad�stica del movimiento del stock"
   ClientHeight    =   6990
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6990
   ScaleWidth      =   10575
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Frame1 
      Height          =   1215
      Left            =   0
      TabIndex        =   7
      Top             =   0
      Width           =   10575
      Begin VB.ComboBox Rubro 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5760
         TabIndex        =   3
         Top             =   720
         Width           =   4695
      End
      Begin VB.TextBox A�o 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5760
         MaxLength       =   4
         TabIndex        =   2
         Top             =   360
         Width           =   735
      End
      Begin VB.OptionButton Option2 
         Caption         =   "&Compras"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   1
         Top             =   720
         Width           =   1575
      End
      Begin VB.OptionButton Ventas 
         Caption         =   "&Ventas"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   120
         TabIndex        =   0
         Top             =   360
         Value           =   -1  'True
         Width           =   1695
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Rubro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   4680
         TabIndex        =   36
         Top             =   720
         Width           =   975
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "A�o:"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   4680
         TabIndex        =   10
         Top             =   360
         Width           =   975
      End
   End
   Begin VB.Frame Frame2 
      Height          =   4815
      Left            =   0
      TabIndex        =   8
      Top             =   1200
      Width           =   10575
      Begin VB.Frame Frame3 
         Height          =   4815
         Left            =   4440
         TabIndex        =   49
         Top             =   0
         Width           =   6135
         Begin VB.ListBox List2 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3840
            Left            =   3120
            TabIndex        =   52
            Top             =   720
            Width           =   2895
         End
         Begin VB.ListBox List1 
            BeginProperty Font 
               Name            =   "Courier New"
               Size            =   8.25
               Charset         =   0
               Weight          =   400
               Underline       =   0   'False
               Italic          =   0   'False
               Strikethrough   =   0   'False
            EndProperty
            Height          =   3840
            Left            =   120
            TabIndex        =   51
            Top             =   720
            Width           =   2775
         End
         Begin VB.Label Label7 
            Height          =   495
            Left            =   3000
            TabIndex        =   53
            Top             =   240
            Width           =   3015
         End
         Begin VB.Label Label6 
            Caption         =   "Ranking de Mvtos. de Art�culos del Rubro seleccionado:"
            ForeColor       =   &H00800000&
            Height          =   495
            Left            =   120
            TabIndex        =   50
            Top             =   240
            Width           =   2415
         End
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   12
         Left            =   1560
         TabIndex        =   35
         Top             =   4320
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   11
         Left            =   1560
         TabIndex        =   34
         Top             =   3960
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   10
         Left            =   1560
         TabIndex        =   33
         Top             =   3600
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   9
         Left            =   1560
         TabIndex        =   32
         Top             =   3240
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   8
         Left            =   1560
         TabIndex        =   31
         Top             =   2880
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   7
         Left            =   1560
         TabIndex        =   30
         Top             =   2520
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   6
         Left            =   1560
         TabIndex        =   29
         Top             =   2160
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   5
         Left            =   1560
         TabIndex        =   28
         Top             =   1800
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   4
         Left            =   1560
         TabIndex        =   27
         Top             =   1440
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   3
         Left            =   1560
         TabIndex        =   26
         Top             =   1080
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   2
         Left            =   1560
         TabIndex        =   25
         Top             =   720
         Width           =   1575
      End
      Begin VB.TextBox Mes 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Index           =   1
         Left            =   1560
         TabIndex        =   24
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   11
         Left            =   3360
         TabIndex        =   48
         Top             =   1080
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   10
         Left            =   3360
         TabIndex        =   47
         Top             =   1440
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   9
         Left            =   3360
         TabIndex        =   46
         Top             =   1800
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   8
         Left            =   3360
         TabIndex        =   45
         Top             =   2160
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   7
         Left            =   3360
         TabIndex        =   44
         Top             =   2520
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   6
         Left            =   3360
         TabIndex        =   43
         Top             =   2880
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   5
         Left            =   3360
         TabIndex        =   42
         Top             =   3240
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   4
         Left            =   3360
         TabIndex        =   41
         Top             =   3600
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   3
         Left            =   3360
         TabIndex        =   40
         Top             =   3960
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   2
         Left            =   3360
         TabIndex        =   39
         Top             =   4320
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   1
         Left            =   3360
         TabIndex        =   38
         Top             =   720
         Width           =   2175
      End
      Begin VB.Label Label5 
         Caption         =   "Unidades"
         Height          =   255
         Index           =   0
         Left            =   3360
         TabIndex        =   37
         Top             =   360
         Width           =   2175
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Diciembre"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   10
         Left            =   240
         TabIndex        =   23
         Top             =   4320
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Noviembre"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   9
         Left            =   240
         TabIndex        =   22
         Top             =   3960
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Octubre"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   8
         Left            =   240
         TabIndex        =   21
         Top             =   3600
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Septiembre"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   7
         Left            =   240
         TabIndex        =   20
         Top             =   3240
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Agosto"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   6
         Left            =   240
         TabIndex        =   19
         Top             =   2880
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Julio"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   5
         Left            =   240
         TabIndex        =   18
         Top             =   2520
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Junio"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   4
         Left            =   240
         TabIndex        =   17
         Top             =   2160
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Mayo"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   3
         Left            =   240
         TabIndex        =   16
         Top             =   1800
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Marzo"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   2
         Left            =   240
         TabIndex        =   15
         Top             =   1080
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Abril"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   1
         Left            =   240
         TabIndex        =   14
         Top             =   1440
         Width           =   1215
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Febrero"
         ForeColor       =   &H00800000&
         Height          =   255
         Index           =   0
         Left            =   240
         TabIndex        =   13
         Top             =   720
         Width           =   1215
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Enero"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   240
         TabIndex        =   11
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   9
      Top             =   6000
      Width           =   10575
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Art_Estadistica.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Confirma la Carga de la Lista.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   9360
         Picture         =   "Art_Estadistica.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   6
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7920
         Picture         =   "Art_Estadistica.frx":C514
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Imprimir el Asiento.-"
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Label Label3 
      Caption         =   "Label3"
      Height          =   495
      Left            =   4680
      TabIndex        =   12
      Top             =   3000
      Width           =   1215
   End
End
Attribute VB_Name = "Art_Estadistica"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub A�o_GotFocus()
    A�o.Text = Trim(A�o.Text)
    A�o.SelStart = 0
    A�o.SelLength = Len(A�o.Text)
End Sub

Private Sub A�o_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub A�o_LostFocus()
    A�o.Text = Val(A�o.Text)
End Sub

Private Sub Borrar_Campo()
    Dim i As Long
    
    For i = 1 To 12
        Mes(i).Text = ""
    Next
End Sub

Private Sub Confirma_Click()
    Dim i As Long
    
    Borrar_Campo
    
    For i = 1 To 12
        'If A�o.Text <> Year(Fecha_Fiscal) And i < Month(Fecha_Fiscal) Then
            qy = "SELECT SUM (Cantidad) "
            qy = qy & "FROM " & IIf(Ventas.Value = True, "Factura_Venta_Item, Factura_Venta ", "Factura_Compra_Item, Factura_Compra ")
            If Ventas.Value = True Then
                qy = qy & "WHERE Factura_Venta.Empresa = " & Val(Id_Empresa) & " "
                qy = qy & "AND Factura_Venta_Item.Id_Tipo_Factura = Factura_Venta.Id_Tipo_Factura "
                qy = qy & "AND Factura_Venta_Item.Id_Letra_Factura = Factura_Venta.Id_Letra_Factura "
                qy = qy & "AND Factura_Venta_Item.Id_Centro_Emisor = Factura_Venta.Id_Centro_Emisor "
                qy = qy & "AND Factura_Venta_Item.Id_Nro_Factura = Factura_Venta.Id_Nro_Factura "
            Else
                qy = qy & "WHERE Factura_Compra.Empresa = " & Val(Id_Empresa) & " "
                qy = qy & "AND Factura_Compra_Item.Id_Tipo_Factura = Factura_Compra.Id_Tipo_Factura "
                qy = qy & "AND Factura_Compra_Item.Id_Letra_Factura = Factura_Compra.Id_Letra_Factura "
                qy = qy & "AND Factura_Compra_Item.Id_Centro_Emisor = Factura_Compra.Id_Centro_Emisor "
                qy = qy & "AND Factura_Compra_Item.Id_Nro_Factura = Factura_Compra.Id_Nro_Factura "
            End If
            qy = qy & "AND Rubro = " & Val(Mid(Rubro.Text, 30)) & " "
            qy = qy & "AND Mes_Libro = " & Val(i) & " "
            qy = qy & "AND A�o_Libro = " & Val(A�o.Text) & " "
            AbreRs
            
            If Not IsNull(Rs.Fields(0)) = True Then Mes(i).Text = Formateado(Str(Val(Rs.Fields(0))), 2, 12, " ", True)
        'End If
    Next
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 7
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Rubro
End Sub

Private Sub Cargar_Rubro()
    qy = "SELECT * FROM Rubro ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Rubro.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Trim(Rs.Fields("Id_Rubro"))
        
        Rs.MoveNext
    Wend
End Sub

Private Sub Option2_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub Rubro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub

Private Sub Salir_Click()
    If A�o.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
        Borrar_Campo_Qy
    End If
End Sub

Private Sub Borrar_Campo_Qy()
    A�o.Text = ""
    Rubro.Text = ""
    Ventas.Value = True
    
    Ventas.SetFocus
End Sub

Private Sub Ventas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{tab}"
End Sub
