VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Begin VB.Form Contab_Asientos 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaci�n de Asientos Contables.-"
   ClientHeight    =   7110
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   10575
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7110
   ScaleWidth      =   10575
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Captura 
      Height          =   4215
      Left            =   2040
      TabIndex        =   32
      Top             =   1080
      Visible         =   0   'False
      Width           =   6735
      Begin VB.CommandButton Borrar 
         Caption         =   "&Borrar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   120
         TabIndex        =   36
         Top             =   3720
         Width           =   975
      End
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   2130
         Left            =   120
         Style           =   1  'Simple Combo
         TabIndex        =   35
         Top             =   360
         Width           =   6495
      End
      Begin VB.CommandButton Termina 
         Caption         =   "&Termina"
         Height          =   375
         Left            =   5640
         TabIndex        =   16
         Top             =   3720
         Width           =   975
      End
      Begin VB.CommandButton Confirma 
         Caption         =   "&Confirma"
         Enabled         =   0   'False
         Height          =   375
         Left            =   4560
         TabIndex        =   15
         Top             =   3720
         Width           =   975
      End
      Begin VB.TextBox Haber 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         TabIndex        =   14
         Top             =   3120
         Width           =   1335
      End
      Begin VB.TextBox Debe 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         TabIndex        =   13
         Top             =   2760
         Width           =   1335
      End
      Begin VB.TextBox Id_Nivel_6 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4680
         TabIndex        =   11
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_5 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3960
         TabIndex        =   10
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_4 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   9
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_3 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2520
         TabIndex        =   8
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_2 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1800
         TabIndex        =   7
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Id_Nivel_1 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   6
         Top             =   600
         Visible         =   0   'False
         Width           =   615
      End
      Begin VB.TextBox Nombre 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         TabIndex        =   12
         Top             =   600
         Width           =   5535
      End
      Begin VB.Label Label8 
         Caption         =   "Seleccione una Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   41
         Top             =   120
         Width           =   2535
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Haber:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4080
         TabIndex        =   34
         Top             =   3120
         Width           =   1095
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Debe:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4080
         TabIndex        =   33
         Top             =   2760
         Width           =   1095
      End
   End
   Begin VB.Frame Marco_Asiento 
      Height          =   1095
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   10575
      Begin VB.TextBox Detalle 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1080
         MaxLength       =   50
         TabIndex        =   3
         Top             =   600
         Width           =   9375
      End
      Begin VB.ComboBox Ejercicio 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1080
         TabIndex        =   0
         Top             =   240
         Width           =   3255
      End
      Begin VB.ComboBox Asientos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5280
         TabIndex        =   38
         Top             =   240
         Visible         =   0   'False
         Width           =   5175
      End
      Begin VB.CommandButton Buscar_Asiento 
         Caption         =   "&N�mero:"
         Height          =   255
         Left            =   4440
         TabIndex        =   37
         ToolTipText     =   "Buscar asientos en la base datos pertenecientes al ejercicio seleccionado.-"
         Top             =   240
         Width           =   735
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9120
         MaxLength       =   10
         TabIndex        =   2
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Numero 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5280
         MaxLength       =   10
         TabIndex        =   1
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Detalle:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   600
         Width           =   855
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8160
         TabIndex        =   24
         Top             =   240
         Width           =   855
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Ejercicio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   23
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Cuentas 
      Enabled         =   0   'False
      Height          =   5055
      Left            =   0
      TabIndex        =   21
      Top             =   1080
      Width           =   10575
      Begin MSFlexGridLib.MSFlexGrid Lista 
         Height          =   3975
         Left            =   120
         TabIndex        =   42
         Top             =   240
         Width           =   10335
         _ExtentX        =   18230
         _ExtentY        =   7011
         _Version        =   393216
         Cols            =   4
         FixedCols       =   0
         FocusRect       =   0
         HighLight       =   2
         FillStyle       =   1
         ScrollBars      =   2
         SelectionMode   =   1
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Tahoma"
            Size            =   8.25
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Frame Frame1 
         BorderStyle     =   0  'None
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   120
         Width           =   10335
      End
      Begin VB.TextBox Diferencia 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9120
         TabIndex        =   28
         Top             =   4680
         Width           =   1335
      End
      Begin VB.TextBox Total_Creditos 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   27
         Top             =   4680
         Width           =   1335
      End
      Begin VB.TextBox Total_Debitos 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   26
         Top             =   4320
         Width           =   1335
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Diferencia:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8160
         TabIndex        =   31
         Top             =   4680
         Width           =   855
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Total Cr�ditos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   4680
         Width           =   1095
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Total D�bitos:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   4320
         Width           =   1095
      End
      Begin VB.Label Label4 
         Caption         =   "C�digo de la Cuenta           Denominaci�n         Detalle del Movnto.          Debe     Haber"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   120
         Width           =   9975
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   22
      Top             =   6120
      Width           =   10575
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7920
         Picture         =   "Contab_Asientos.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   19
         ToolTipText     =   "Imprimir el Asiento.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Borrar_Asiento 
         Enabled         =   0   'False
         Height          =   615
         Left            =   6840
         Picture         =   "Contab_Asientos.frx":5C12
         Style           =   1  'Graphical
         TabIndex        =   18
         ToolTipText     =   "Borrar el asiento.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   9360
         Picture         =   "Contab_Asientos.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   20
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   5760
         Picture         =   "Contab_Asientos.frx":12126
         Style           =   1  'Graphical
         TabIndex        =   17
         ToolTipText     =   "Grabar el Asiento en la Base de Datos.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Agregar 
         Height          =   615
         Left            =   120
         Picture         =   "Contab_Asientos.frx":12430
         Style           =   1  'Graphical
         TabIndex        =   4
         ToolTipText     =   "Ingresat nuevas cuentas al asiento.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Contab_Asientos"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Agregar_Click()
    If Val(Ejercicio.Text) > 0 And Val(Numero.Text) > 0 And Val(Fecha.Text) > 0 Then
        Borrar_Campo_Captura
        
        Marco_Asiento.Enabled = False
        Marco_Cuentas.Enabled = False
        Botonera.Enabled = False
        Marco_Captura.Visible = True
        Marco_Captura.Enabled = True
        Termina.Cancel = True
        
        Cuentas_Encontradas.Text = ""
        Cuentas_Encontradas.SetFocus
    Else
        Ejercicio.SetFocus
    End If
End Sub

Private Sub Asientos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Fecha.SetFocus
End Sub

Private Sub Asientos_Encontrados_LostFocus()
    Fecha.Text = Format(Mid(Asientos_Encontrados.List(Asientos_Encontrados.ListIndex), 1, 8), "dd/mm/yyyy")
    Ejercicio.Text = Mid(Asientos_Encontrados.List(Asientos_Encontrados.ListIndex), 52, 4)
    Numero.Text = Mid(Asientos_Encontrados.List(Asientos_Encontrados.ListIndex), 41, 10)
    
    Asientos_Encontrados.Visible = False
    If Val(Numero.Text) > 0 And Val(Ejercicio) > 0 And Val(Fecha.Text) > 0 Then
        Leer_Asiento
    Else
        Ejercicio.SetFocus
    End If
End Sub

Private Sub Borrar_Asiento_Click()
    'If MsgBox("Est� seguro de borrar �ste movimiento de la contabilidad. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
    '
    '        Qy = "DELETE FROM Asiento WHERE Nro_Asiento = " & trim(str(Val(Numero.Text))) & " "
    '        Qy = Qy & "AND Id_Ejercicio = " & trim(str(Val(Ejercicio.Text))) & " "
    '        Qy = Qy & "AND Id_Empresa = " & trim(str(Val(Id_Empresa)))
    '        Db.Execute (Qy)
    '
            'If trim(ucase(mid(Detalle.Text, 1, 2))) = "FC" Or trim(ucase(mid(Detalle.Text, 1, 2))) = "RC" Or trim(ucase(mid(List1.List(0), 52, 2))) = "NC" Or trim(ucase(mid(List1.List(0), 52, 2))) = "OP" Or trim(ucase(mid(List1.List(0), 52, 2))) = "ND" Then
                
                'If MsgBox("Desea Borrar los movimientos asociados a este registro contable. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
                    
                    'If trim(ucase(mid(List1.List(0), 52, 2))) = "FC" Or trim(ucase(mid(List1.List(0), 52, 2))) = "NC" Or trim(ucase(mid(List1.List(0), 52, 2))) = "ND" Then
                     '
                     '   If trim(ucase(mid(List1.List(0), 73, 1))) = "V" Then
                     '       Qy = "DELETE Factura_Venta_Item.* FROM Factura_Venta_Item WHERE Factura_Venta_Item.Id_Tipo_Factura = '" & trim(ucase(mid(List1.List(0), 52, 2))) & "' "
                     '       Qy = Qy & "AND Factura_Venta_Item.Id_Letra_Factura = '" & trim(ucase(mid(List1.List(0), 55, 1))) & "' "
                     '       Qy = Qy & "AND Factura_Venta_Item.Id_Centro_Emisor = " & trim(str(Val(mid(List1.List(0), 57, 4)))) & " "
                     '       Qy = Qy & "AND Factura_Venta_Item.Id_Nro_Factura = " & trim(str(Val(mid(List1.List(0), 63, 10)))) & " "
                     '       Qy = Qy & "AND Empresa = " & trim(str(Val(Id_Empresa)))
                     '       Db.Execute (Qy)'

                     '       Qy = "DELETE Factura_Venta.* FROM Factura_Venta WHERE Factura_Venta.Id_Tipo_Factura = '" & trim(ucase(mid(List1.List(0), 52, 2))) & "' "
                     '       Qy = Qy & "AND Factura_Venta.Id_Letra_Factura = '" & trim(ucase(mid(List1.List(0), 55, 1))) & "' "
                     '       Qy = Qy & "AND Factura_Venta.Id_Centro_Emisor = " & trim(str(Val(mid(List1.List(0), 57, 4)))) & " "
                     '       Qy = Qy & "AND Factura_Venta.Id_Nro_Factura = " & trim(str(Val(mid(List1.List(0), 63, 10)))) & " "
                     '       Qy = Qy & "AND Empresa = " & trim(str(Val(Id_Empresa)))
                     '       Db.Execute (Qy)
                            
                     '       Qy = "DELETE * FROM CtaCte_Cliente WHERE MID(Comprobante, 1, 2) = '" & trim(ucase(mid(List1.List(0), 52, 2))) & "' "
                     '       Qy = Qy & "AND MID(Comprobante, 4, 1) = '" & trim(ucase(mid(List1.List(0), 55, 1))) & "' "
                     '       Qy = Qy & "AND MID(Comprobante, 6, 4) = '" & trim(mid(List1.List(0), 57, 4)) & "' "
                     '       Qy = Qy & "AND MID(Comprobante, 11, 10) = '" & trim(mid(List1.List(0), 62, 10)) & "' "
                     '       Db.Execute (Qy)
                     '   Else
                     '       Qy = "DELETE Factura_Compra_Item.* FROM Factura_Compra_Item WHERE Factura_Compra_Item.Id_Tipo_Factura = '" & trim(ucase(mid(List1.List(0), 52, 2))) & "' "
                     '       Qy = Qy & "AND Factura_Compra_Item.Id_Letra_Factura = '" & trim(ucase(mid(List1.List(0), 55, 1))) & "' "
                     '       Qy = Qy & "AND Factura_Compra_Item.Id_Centro_Emisor = " & trim(str(Val(mid(List1.List(0), 57, 4)))) & " "
                     '       Qy = Qy & "AND Factura_Compra_Item.Id_Nro_Factura = " & trim(str(Val(mid(List1.List(0), 63, 10)))) & " "
                     '       Qy = Qy & "AND Empresa = " & trim(str(Val(Id_Empresa)))
                     '       Db.Execute (Qy)

        '                    Qy = "DELETE Factura_Compra.* FROM Factura_Compra WHERE Factura_Compra.Id_Tipo_Factura = '" & trim(ucase(mid(List1.List(0), 52, 2))) & "' "
        '                    Qy = Qy & "AND Factura_Compra.Id_Letra_Factura = '" & trim(ucase(mid(List1.List(0), 55, 1))) & "' "
        '                    Qy = Qy & "AND Factura_Compra.Id_Centro_Emisor = " & trim(str(Val(mid(List1.List(0), 57, 4)))) & " "
        '                    Qy = Qy & "AND Factura_Compra.Id_Nro_Factura = " & trim(str(Val(mid(List1.List(0), 63, 10)))) & " "
        '                    Qy = Qy & "AND Empresa = " & trim(str(Val(Id_Empresa)))
        '                    Db.Execute (Qy)
        '
        '                    Qy = "DELETE * FROM CtaCte_Proveedor WHERE MID(Comprobante, 1, 2) = '" & trim(ucase(mid(List1.List(0), 52, 2))) & "' "
        '                    Qy = Qy & "AND MID(Comprobante, 4, 1) = '" & trim(ucase(mid(List1.List(0), 55, 1))) & "' "
        '                    Qy = Qy & "AND MID(Comprobante, 6, 4) = '" & trim(mid(List1.List(0), 57, 4)) & "' "
        '                    Qy = Qy & "AND MID(Comprobante, 11, 10) = '" & trim(mid(List1.List(0), 62, 10)) & "' "
        '                    Db.Execute (Qy)
        '                End If
        '            ElseIf trim(ucase(mid(List1.List(0), 52, 2))) = "RC" Then
        '                Qy = "DELETE * FROM Recibo WHERE Id_Recibo = " & trim(mid(List1.List(0), 57, 10)) & " "
        '                Qy = Qy & "AND Tipo_Cuenta = '" & IIf(ucase(mid(List1.List(0), 68, 3)) = "CLI", "C", "P") & "' "
        '                Qy = Qy & "AND Empresa = " & trim(str(Val(Id_Empresa)))
        '                Db.Execute (Qy)
         '
         '               Qy = "DELETE * FROM " & IIf(ucase(mid(List1.List(0), 68, 3)) = "CLI", "CtaCte_Cliente", "CtaCte_Proveedor") & " "
        '                Qy = Qy & "WHERE MID(Comprobante, 1, 2) = 'RC' "
        '                Qy = Qy & "AND MID(Comprobante, 4, 1) = '" & trim(ucase(mid(List1.List(0), 55, 1))) & "'"
         '               Qy = Qy & "AND MID(Comprobante, 6, 10) = '" & trim(mid(List1.List(0), 57, 10)) & "'"
         '               Db.Execute (Qy)
         '           ElseIf trim(ucase(mid(List1.List(0), 52, 2))) = "OP" Then
         '               Qy = "DELETE * FROM Orden_Pago WHERE Id_Orden_Pago = " & trim(mid(List1.List(0), 57, 10)) & " "
         '               Qy = Qy & "AND Tipo_Cuenta = '" & IIf(ucase(mid(List1.List(0), 68, 3)) = "CLI", "C", "P") & "'"
         '               Db.Execute (Qy)
         ''
         '               Qy = "DELETE * FROM " & IIf(ucase(mid(List1.List(0), 68, 3)) = "CLI", "CtaCte_Cliente", "CtaCte_Proveedor") & " "
         '               Qy = Qy & "WHERE MID(Comprobante, 1, 2) = 'OP' "
         '               Qy = Qy & "AND MID(Comprobante, 4, 1) = '" & trim(ucase(mid(List1.List(0), 55, 1))) & "'"
         '               Qy = Qy & "AND MID(Comprobante, 6, 10) = '" & trim(mid(List1.List(0), 57, 10)) & "'"
         '               Db.Execute (Qy)
         '           End If
         '       'End If
         '   End If
   ' End If
    Salir_Click
End Sub

'Private Sub Borrar_Click()
'    If MsgBox("Desea Borrar esta cuenta del asiento.?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
'        List1.RemoveItem Val(Confirma.Tag)
'
'        Calcular_Totales
'        Termina_Click
'    End If
'End Sub

Private Sub Buscar_Asiento_Click()
    Asientos_Encontrados.Visible = True
    Asientos_Encontrados.Clear
    MousePointer = 11
    
    qy = "SELECT Id_Asiento, Fecha, Detalle, Ejercicio.Id_Ejercicio FROM Asiento, Ejercicio "
    qy = qy & "WHERE Asiento.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "AND Ejercicio.Id_Empresa = Asiento.Id_Empresa "
    If Val(Ejercicio.Text) > 0 Then qy = qy & "AND Ejercicio.Id_Ejercicio = " & Trim(Str(Val(Mid(Ejercicio.Text, 1, 4)))) & " "
    qy = qy & "GROUP BY Id_Asiento, Fecha, Detalle, Ejercicio.Id_Ejercicio "
    qy = qy & "ORDER BY Fecha DESC, Detalle"
    AbreRs
    
    While Not Rs.EOF
        Asientos_Encontrados.AddItem Format(Rs.Fields("Fecha"), "dd/mm/yy") & " " & Trim(Rs.Fields("Detalle")) + Space(30 - Len(Trim(Rs.Fields("Detalle")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Asiento"))), 0, 10, "0", False) & " " & Trim(Rs.Fields("Id_Ejercicio"))
        Rs.MoveNext
    Wend
    
    MousePointer = 0
    Asientos_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Confirma_Click()
    
    If Val(Mid(Cuentas_Encontradas.Text, 35)) > 0 Then
        
        Txt = Formateado(Str(Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 37, 4))), 0, 4, " ", False) & "."
        Txt = Txt & Formateado(Str(Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 42, 4))), 0, 4, " ", False) & "."
        Txt = Txt & Formateado(Str(Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 47, 4))), 0, 4, " ", False) & "."
        Txt = Txt & Formateado(Str(Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 52, 4))), 0, 4, " ", False) & "."
        Txt = Txt & Formateado(Str(Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 57, 4))), 0, 4, " ", False) & "."
        Txt = Txt & Formateado(Str(Val(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 62, 4))), 0, 4, " ", False) & Chr(9)
        Txt = Txt & Trim(Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 1, 35)) & Chr(9)
        Txt = Txt & Formateado(Str(Val(Debe.Text)), 2, 10, " ", False) & Chr(9)
        Txt = Txt & Formateado(Str(Val(Haber.Text)), 2, 10, " ", False)
            
        If Confirma.Tag = "" Then
            Lista.AddItem Txt
        Else
            Lista.RemoveItem Val(Confirma.Tag)
            Lista.Refresh
            Lista.AddItem Txt, Val(Confirma.Tag)
        End If
        
        Calcular_Totales
        
        Termina_Click
    Else
        MsgBox "Debe Seleccionar una cuenta.", vbInformation, "Atenci�n.!"
        Borrar_Campo_Captura
    End If
End Sub

Private Sub Calcular_Totales()
    Dim i As Long
    'Procedimiento para Calcular los totales del Asiento
    
    Total_Debitos.Text = ""
    Total_Creditos.Text = ""
        
    For i = 0 To Lista.Rows - 1
        Lista.Row = i
        
        Lista.Col = 2
        Total_Debitos.Text = Val(Total_Debitos.Text) + Val(Lista.Text)
        
        Lista.Col = 3
        Total_Creditos.Text = Val(Total_Creditos.Text) + Val(Lista.Text)
    Next
        
    Total_Debitos.Text = Formateado(Str(Val(Total_Debitos.Text)), 2, 10, " ", False)
    Total_Creditos.Text = Formateado(Str(Val(Total_Creditos.Text)), 2, 10, " ", False)
    Diferencia.Text = Formateado(Str(Val(Total_Debitos.Text) - Val(Total_Creditos.Text)), 2, 10, " ", False)
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
'    Id_Nivel_1.Text = Val(mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 37, 4))
'    Id_Nivel_2.Text = Val(mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 42, 4))
'    Id_Nivel_3.Text = Val(mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 47, 4))
'    Id_Nivel_4.Text = Val(mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 52, 4))
'    Id_Nivel_5.Text = Val(mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 57, 4))
'    Id_Nivel_6.Text = Val(mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 62, 4))
    
'    Cuentas_Encontradas.Visible = False
'    Leer_Cuenta
End Sub

Private Sub Leer_Cuenta()
    If Val(Id_Nivel_1.Text) > 0 Then
        If Val(Id_Nivel_1.Text) = 1 And Val(Id_Nivel_2.Text) = 3 And Val(Id_Nivel_3.Text) = 2 And Val(Id_Nivel_4.Text) = 1 And Val(Id_Nivel_5.Text) = 0 And Val(Id_Nivel_6.Text) = 0 Then
            MsgBox "Los movimientos de esta cuenta se realizan por m�dulo de facturaci�n !", vbCritical, "Atenci�n.!"
            Borrar_Campo_Captura
        Else
            qy = "SELECT * FROM Cuenta WHERE "
            qy = qy & "Id_Nivel_1 = " & Trim(Str(Val(Id_Nivel_1.Text))) & " "
            qy = qy & "AND Id_Nivel_2 = " & Trim(Str(Val(Id_Nivel_2.Text))) & " "
            qy = qy & "AND Id_Nivel_3 = " & Trim(Str(Val(Id_Nivel_3.Text))) & " "
            qy = qy & "AND Id_Nivel_4 = " & Trim(Str(Val(Id_Nivel_4.Text))) & " "
            qy = qy & "AND Id_Nivel_5 = " & Trim(Str(Val(Id_Nivel_5.Text))) & " "
            qy = qy & "AND Id_Nivel_6 = " & Trim(Str(Val(Id_Nivel_6.Text))) & " "
            qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa)))
            AbreRs
            
            If Rs.EOF Then
                
                MsgBox "La Cuenta es inexistente...", vbInformation, "Atenci�n.!"
                Borrar_Campo_Captura
            Else
                
                Nombre.Text = Rs.Fields("Denominacion")
                'Inputable.Value = IIf(Rs.Fields("Recibe_Asiento") = "Falso", 0, 1)
                
                'If Inputable.Value = 0 Then
                '    MsgBox "La cuenta no recibe asientos.", vbInformation, "Atenci�n.!"
                '    Borrar_Campo_Captura
                'Else
                '    If Detalle.Enabled = True And Marco_Captura.Enabled = True And Marco_Captura.Visible = True Then
                '        Detalle.SetFocus
                '    Else
                        'If Debe.Enabled = True Then
                        '    Debe.SetFocus
                        'Else
                        '    Haber.SetFocus
                        'End If
                '    End If
                'End If
            End If
        End If
    'Else
    '    Id_Nivel_1.Text = ""
    '    Id_Nivel_1.SetFocus
    End If
End Sub

Private Sub Borrar_Campo_Captura()
    Id_Nivel_1.Text = ""
    Id_Nivel_2.Text = ""
    Id_Nivel_3.Text = ""
    Id_Nivel_4.Text = ""
    Id_Nivel_5.Text = ""
    Id_Nivel_6.Text = ""

    Nombre.Text = ""
    Confirma.Tag = ""
    Debe.Text = ""
    Haber = ""
    Confirma.Enabled = False
    Borrar.Enabled = False
    
    'Cuentas_Encontradas.SetFocus
End Sub

Private Sub Debe_Change()
    Haber.Text = ""
    If Val(Debe.Text) > 0 Then
        Haber.Enabled = False
        Confirma.Enabled = True
    Else
        Haber.Enabled = True
        Confirma.Enabled = False
    End If
End Sub

Private Sub Debe_Click()
    Debe_Change
End Sub

Private Sub Debe_GotFocus()
    Debe.Text = Trim(Debe.Text)
    Debe.SelStart = 0
    Debe.SelText = ""
    Debe.SelLength = Len(Debe.Text)
End Sub

Private Sub Debe_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Debe_LostFocus()
    Debe.Text = Formateado(Str(Val(Debe.Text)), 2, 10, " ", False)
End Sub

Private Sub Detalle_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Detalle_LostFocus()
    Detalle.Text = FiltroCaracter(Detalle.Text)
    Detalle.Text = UCase(Detalle.Text)
End Sub

Private Sub Ejercicio_GotFocus()
    Ejercicio.Text = Trim(Ejercicio.Text)
    Ejercicio.SelStart = 0
    Ejercicio.SelText = ""
    Ejercicio.SelLength = Len(Ejercicio.Text)
End Sub

Private Sub Ejercicio_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_GotFocus()
    If Fecha.Text = "" Or Fecha.Text = "error" Then Fecha.Text = Fecha_Fiscal
    Fecha.SelStart = 0
    Fecha.SelText = ""
    Fecha.SelLength = Len(Fecha.Text)
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    If Val(Fecha.Text) > 0 Then Fecha.Text = ValidarFecha(Fecha.Text)
    
    If Fecha.Text = "error" Then
        MsgBox "La fecha ingresada es incorrecta.-"
        Fecha.SetFocus
    End If
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualizaci�n de Asientos Contables.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 10
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Cargar_Ejercicios
    
    Armar_Lista
    'Ejercicio.Text = Format(Now, "yyyy")
End Sub

Private Sub Armar_Lista()
    Lista.Clear
    
    Lista.Cols = 4
    Lista.ColWidth(0) = 2500
    Lista.ColWidth(1) = 4900
    Lista.ColWidth(2) = 1200
    Lista.ColWidth(3) = 1200
    Lista.ColAlignment(2) = 6
    Lista.ColAlignment(3) = 6
    
    Lista.Rows = 1
    Lista.Col = 0
    Lista.Row = 0
    
    Lista.Text = "C�digo"
    Lista.Col = 1
    Lista.Text = "Cuenta"
    Lista.Col = 2
    Lista.Text = "Debe"
    Lista.Col = 3
    Lista.Text = "Haber"
End Sub

Private Sub Cargar_Ejercicios()
    Ejercicio.Clear
    
    qy = "SELECT * FROM Ejercicio WHERE Id_Empresa = " & Val(Id_Empresa) & " "
    qy = qy & "ORDER BY Periodo DESC"
    AbreRs
    
    While Not Rs.EOF
        Ejercicio.AddItem Trim(Str(Val(Rs.Fields("Id_Ejercicio")))) + Space(4 - Len(Trim(Str(Val(Rs.Fields("Id_Ejercicio")))))) & " " & Rs.Fields("Periodo") & " " & Trim(Rs.Fields("Cierre"))
        Rs.MoveNext
    Wend
    
    qy = "SELECT * FROM Cuenta WHERE Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
    qy = qy & "AND Recibe_Asiento <> 0 "
    qy = qy & "ORDER BY Denominacion"
    AbreRs
        
    While Not Rs.EOF
        Cuentas_Encontradas.AddItem Trim(Rs.Fields("Denominacion")) + Space(35 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Formateado(Str(Val(Rs.Fields("Id_Nivel_1"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_2"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_3"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_4"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_5"))), 0, 4, " ", False) & "." & Formateado(Str(Val(Rs.Fields("Id_Nivel_6"))), 0, 4, " ", False)
            
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    Dim i As Long
    
    i = 0
    
    If Grabar.Tag = "Alta" Then
        If Lista.Rows > 0 Then
            qy = "INSERT INTO Asiento VALUES ("
            qy = qy & Val(Id_Empresa)
            qy = qy & ", " & Trim(Str(Val(Numero.Text)))
            qy = qy & ", " & Str(Val(Mid(Ejercicio.Text, 1, 4)))
            qy = qy & ", '" & Trim(Fecha.Text) & "'"
            qy = qy & ", '" & Trim(Detalle.Text) & "')"
            Db.Execute (qy)
            
            For i = 1 To Lista.Rows - 1
                qy = "INSERT INTO Asiento_Item VALUES ("
                qy = qy & Trim(Str(Val(Id_Empresa)))
                qy = qy & ", " & Trim(Str(Val(Numero.Text)))
                qy = qy & ", " & Trim(Str(Val(Mid(Ejercicio.Text, 1, 4))))
                qy = qy & ", " & Val(i)
                Lista.Row = i
                Lista.Col = 0
                qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 1, 4))))
                qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 6, 4))))
                qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 11, 4))))
                qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 16, 4))))
                qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 21, 4))))
                qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 26, 4))))
                Lista.Col = 2
                qy = qy & ", " & Trim(Str(Val(Lista.Text)))
                Lista.Col = 3
                qy = qy & ", " & Trim(Str(Val(Lista.Text))) & ")"
                Db.Execute (qy)
            Next
        Else
            MsgBox "ERROR: El Asiento no fue grabado!", vbInformation, "Atenci�n.!"
        End If
    Else
        
        qy = "DELETE FROM Asiento "
        qy = qy & "WHERE Asiento.Id_Asiento = " & Trim(Str(Val(Numero.Text))) & " "
        qy = qy & "AND Asiento.Id_Ejercicio = " & Trim(Str(Val(Ejercicio.Text))) & " "
        qy = qy & "AND Asiento.Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        Db.Execute (qy)
        
        qy = "DELETE FROM Asiento_Item "
        qy = qy & "WHERE Asiento_Item.Id_Asiento = " & Trim(Str(Val(Numero.Text))) & " "
        qy = qy & "AND Asiento_Item.Id_Ejercicio = " & Trim(Str(Val(Ejercicio.Text))) & " "
        qy = qy & "AND Asiento_Item.Id_Empresa = " & Trim(Str(Val(Id_Empresa)))
        Db.Execute (qy)
        
        qy = "INSERT INTO Asiento VALUES ("
        qy = qy & Val(Id_Empresa)
        qy = qy & ", " & Trim(Str(Val(Numero.Text)))
        qy = qy & ", " & Str(Val(Mid(Ejercicio.Text, 1, 4)))
        qy = qy & ", '" & Trim(Fecha.Text) & "'"
        qy = qy & ", '" & Trim(Detalle.Text) & "')"
        Db.Execute (qy)
            
        For i = 1 To Lista.Rows - 1
            qy = "INSERT INTO Asiento_Item VALUES ("
            qy = qy & Trim(Str(Val(Id_Empresa)))
            qy = qy & ", " & Trim(Str(Val(Numero.Text)))
            qy = qy & ", " & Trim(Str(Val(Mid(Ejercicio.Text, 1, 4))))
            qy = qy & ", " & Val(i)
            Lista.Row = i
            Lista.Col = 0
            qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 1, 4))))
            qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 6, 4))))
            qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 11, 4))))
            qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 16, 4))))
            qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 21, 4))))
            qy = qy & ", " & Trim(Str(Val(Mid(Lista.Text, 26, 4))))
            Lista.Col = 2
            qy = qy & ", " & Trim(Str(Val(Lista.Text)))
            Lista.Col = 3
            qy = qy & ", " & Trim(Str(Val(Lista.Text))) & ")"
            Db.Execute (qy)
        Next
    End If
    
    Salir_Click
End Sub

Private Sub Haber_Change()
    Debe.Text = ""
    If Val(Haber.Text) > 0 Then
        Debe.Enabled = False
        Confirma.Enabled = True
    Else
        Debe.Enabled = True
        Confirma.Enabled = False
    End If
End Sub

Private Sub Haber_Click()
    Haber_Change
End Sub

Private Sub Haber_GotFocus()
    Haber.Text = Trim(Haber.Text)
    Haber.SelStart = 0
    Haber.SelText = ""
    Haber.SelLength = Len(Haber.Text)
End Sub

Private Sub Haber_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Haber_LostFocus()
    Haber.Text = Formateado(Str(Val(Haber.Text)), 2, 10, " ", False)
End Sub

Private Sub Id_Nivel_1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Id_Nivel_1_LostFocus()
    If Val(Id_Nivel_1.Text) > 0 Then
        Leer_Cuenta
    End If
End Sub

Private Sub Numero_GotFocus()
    If Val(Numero.Text) = 0 Then
        qy = "SELECT MAX(Id_Asiento) FROM Asiento WHERE Id_Ejercicio = " & Trim(Str(Val(Mid(Ejercicio.Text, 1, 4)))) & " "
        qy = qy & "AND Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        AbreRs
        
        Numero.Text = 1
        If Not Rs.Fields(0) = "" Then Numero.Text = Val(Rs.Fields(0)) + 1
    End If
    
    Numero.Text = Trim(Numero.Text)
    Numero.SelStart = 0
    Numero.SelText = ""
    Numero.SelLength = Len(Numero.Text)
End Sub

Private Sub Numero_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Numero_LostFocus()
    Numero.Text = Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
    
    If Val(Numero.Text) > 0 Then
        Leer_Asiento
    End If
End Sub

Private Sub Leer_Asiento()
    If Val(Numero.Text) > 0 Then
        
        qy = "SELECT * FROM Asiento, Cuenta, Asiento_Item "
        qy = qy & "WHERE Asiento.Id_Asiento = " & Trim(Str(Val(Numero.Text))) & " "
        qy = qy & "AND Asiento.Id_Asiento = Asiento_Item.Id_Asiento "
        qy = qy & "AND Asiento.Id_Ejercicio = Asiento_Item.Id_Ejercicio "
        qy = qy & "AND Asiento.Id_Ejercicio = " & Trim(Str(Val(Mid(Ejercicio.Text, 1, 4))))
        qy = qy & "AND Asiento.Id_Empresa = Cuenta.Id_Empresa "
        qy = qy & "AND Asiento.Id_Empresa = Asiento_Item.Id_Empresa "
        qy = qy & "AND Asiento.Id_Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "AND Asiento_Item.Nivel_1 = Cuenta.Id_Nivel_1 "
        qy = qy & "AND Asiento_Item.Nivel_2 = Cuenta.Id_Nivel_2 "
        qy = qy & "AND Asiento_Item.Nivel_3 = Cuenta.Id_Nivel_3 "
        qy = qy & "AND Asiento_Item.Nivel_4 = Cuenta.Id_Nivel_4 "
        qy = qy & "AND Asiento_Item.Nivel_5 = Cuenta.Id_Nivel_5 "
        qy = qy & "AND Asiento_Item.Nivel_6 = Cuenta.Id_Nivel_6 "
        qy = qy & "ORDER BY Asiento_Item.Id_Linea "
        AbreRs
        
        If Not Rs.EOF Then
            Fecha.Text = Format(Rs.Fields("Fecha"), "dd/mm/yyyy")
            Detalle.Text = Rs.Fields("Detalle")
            
            While Not Rs.EOF
                Txt = Formateado(Str(Val(Rs.Fields("Id_Nivel_1"))), 0, 4, " ", False) & "."
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Nivel_2"))), 0, 4, " ", False) & "."
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Nivel_3"))), 0, 4, " ", False) & "."
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Nivel_4"))), 0, 4, " ", False) & "."
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Nivel_5"))), 0, 4, " ", False) & "."
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Id_Nivel_6"))), 0, 4, " ", False) & Chr$(9)
                Txt = Txt & Trim(Mid(Rs.Fields("Denominacion"), 1, 30)) + Space(30 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 20)))) & Chr$(9)
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Debe"))), 2, 10, " ", False) & Chr$(9)
                Txt = Txt & Formateado(Str(Val(Rs.Fields("Haber"))), 2, 10, " ", False)
                
                Total_Creditos.Text = Val(Total_Creditos.Text) + Val(Rs.Fields("Haber"))
                Total_Debitos.Text = Val(Total_Debitos.Text) + Val(Rs.Fields("Debe"))
                
                Lista.AddItem Txt
                Rs.MoveNext
            Wend
            
            Total_Debitos.Text = Formateado(Str(Val(Total_Debitos.Text)), 2, 10, " ", False)
            Total_Creditos.Text = Formateado(Str(Val(Total_Creditos.Text)), 2, 10, " ", False)
            Diferencia.Text = Formateado(Str(Val(Total_Debitos.Text) - Val(Total_Creditos.Text)), 2, 10, " ", False)
            
            Grabar.Tag = "Modificar"
            Borrar_Asiento.Enabled = True
            Agregar_Click
        Else
            Grabar.Tag = "Alta"
        End If
    End If
End Sub

Private Sub Salir_Click()
    If Val(Ejercicio.Text) = 0 And Numero.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    Ejercicio.Text = ""
    Numero.Text = ""
    Fecha.Text = ""
    Detalle.Text = ""
    Total_Debitos.Text = ""
    Total_Creditos.Text = ""
    Diferencia.Text = ""
    Lista.Clear
    Armar_Lista
    
    Grabar.Enabled = False
    Borrar.Enabled = False
    Imprimir.Enabled = False
    Borrar_Asiento.Enabled = False
    Marco_Cuentas.Enabled = False
    Marco_Asiento.Enabled = True
    
    Ejercicio.SetFocus
End Sub

Private Sub Termina_Click()
    If Id_Nivel_1.Text = "" Then
        Botonera.Enabled = True
        Marco_Captura.Visible = False
        Marco_Captura.Enabled = False
        Marco_Cuentas.Enabled = True
        Salir.Cancel = True
        
        If Val(Diferencia.Text) = 0 And Val(Total_Debitos.Text) > 0 And Val(Total_Creditos.Text) > 0 Then
            Grabar.Enabled = True
            Imprimir.Enabled = True
            Grabar.SetFocus
        Else
            Grabar.Enabled = False
            Imprimir.Enabled = False
            Agregar.SetFocus
        End If

    Else
        Borrar_Campo_Captura
        Id_Nivel_1.SetFocus
    End If
End Sub

