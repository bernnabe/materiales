VERSION 5.00
Begin VB.Form Fac_Pago_Prov 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Emisi�n de Ordenes de Pago"
   ClientHeight    =   7230
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   9735
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7230
   ScaleWidth      =   9735
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Captura 
      Height          =   3135
      Left            =   1320
      TabIndex        =   24
      Top             =   1920
      Visible         =   0   'False
      Width           =   7095
      Begin VB.CommandButton Buscar_Cheque 
         Caption         =   "N.Cheque:"
         Enabled         =   0   'False
         Height          =   255
         Left            =   120
         TabIndex        =   56
         ToolTipText     =   "Buscar Cheques de terceros disponibles en la base de datos"
         Top             =   1560
         Width           =   975
      End
      Begin VB.CheckBox Nuestro 
         Caption         =   "Nuestra firma"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2640
         TabIndex        =   16
         Top             =   1560
         Width           =   2775
      End
      Begin VB.CommandButton Terminar 
         Caption         =   "&Termina"
         Height          =   375
         Left            =   6000
         TabIndex        =   23
         Top             =   2640
         Width           =   975
      End
      Begin VB.CommandButton Borrar 
         Caption         =   "&Borrar"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6000
         TabIndex        =   22
         Top             =   2280
         Width           =   975
      End
      Begin VB.CommandButton Confirma 
         Caption         =   "&Confirma"
         Enabled         =   0   'False
         Height          =   375
         Left            =   6000
         TabIndex        =   21
         Top             =   1920
         Width           =   975
      End
      Begin VB.ComboBox Banco 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   17
         Top             =   1920
         Width           =   4215
      End
      Begin VB.TextBox Cobro 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4080
         MaxLength       =   10
         TabIndex        =   20
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Importe_Cheque 
         BackColor       =   &H00FFFFFF&
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4080
         MaxLength       =   10
         TabIndex        =   14
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox Importe_Efectivo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         MaxLength       =   10
         TabIndex        =   13
         Top             =   960
         Width           =   1335
      End
      Begin VB.TextBox Emision 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         MaxLength       =   10
         TabIndex        =   19
         Top             =   2640
         Width           =   1335
      End
      Begin VB.TextBox Titular 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         TabIndex        =   18
         Top             =   2280
         Width           =   4215
      End
      Begin VB.TextBox Nro_Cheque 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         MaxLength       =   10
         TabIndex        =   15
         Top             =   1560
         Width           =   1335
      End
      Begin VB.TextBox Total_Factura 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4200
         TabIndex        =   11
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Adeuda 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5640
         TabIndex        =   12
         Top             =   240
         Width           =   1335
      End
      Begin VB.ComboBox Comprobante 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         Style           =   2  'Dropdown List
         TabIndex        =   10
         Top             =   240
         Width           =   2895
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Nro. Cheque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   1560
         Width           =   975
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Cobro:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3000
         TabIndex        =   31
         Top             =   2640
         Width           =   975
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Emision:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   2640
         Width           =   975
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "Titular:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   2280
         Width           =   975
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Banco:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   28
         Top             =   1920
         Width           =   975
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Cheque:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   2640
         TabIndex        =   27
         Top             =   960
         Width           =   1335
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "Efectivo:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   26
         Top             =   960
         Width           =   975
      End
      Begin VB.Label Label18 
         Caption         =   "Comprobante:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   25
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Marco_Cuenta 
      Height          =   1455
      Left            =   0
      TabIndex        =   33
      Top             =   0
      Width           =   9735
      Begin VB.ComboBox Cuentas_Encontradas 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   5760
         TabIndex        =   57
         Top             =   720
         Visible         =   0   'False
         Width           =   3855
      End
      Begin VB.TextBox Fecha 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         TabIndex        =   0
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Numero 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8280
         TabIndex        =   1
         Top             =   240
         Width           =   1335
      End
      Begin VB.OptionButton Cliente 
         Caption         =   "Cliente"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1200
         TabIndex        =   36
         Top             =   1080
         Visible         =   0   'False
         Width           =   1455
      End
      Begin VB.OptionButton Proveedor 
         Caption         =   "Proveedor"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   1200
         TabIndex        =   35
         Top             =   720
         Value           =   -1  'True
         Width           =   1455
      End
      Begin VB.CommandButton Buscar_Cuenta 
         Caption         =   "&Cuenta:"
         Height          =   255
         Left            =   3840
         TabIndex        =   34
         Top             =   720
         Width           =   855
      End
      Begin VB.TextBox Cuenta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4920
         TabIndex        =   2
         Top             =   720
         Width           =   735
      End
      Begin VB.TextBox Nombre 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5760
         TabIndex        =   4
         Top             =   720
         Width           =   3855
      End
      Begin VB.TextBox Saldo 
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5760
         TabIndex        =   5
         Top             =   1080
         Width           =   1335
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo de la Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3960
         TabIndex        =   39
         Top             =   1080
         Width           =   1695
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   240
         Width           =   975
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Numero:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7320
         TabIndex        =   37
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Factura 
      Enabled         =   0   'False
      Height          =   2055
      Left            =   0
      TabIndex        =   40
      Top             =   1440
      Width           =   9735
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1590
         Left            =   120
         Sorted          =   -1  'True
         Style           =   1  'Checkbox
         TabIndex        =   6
         Top             =   360
         Width           =   9495
      End
      Begin VB.Label Label11 
         AutoSize        =   -1  'True
         Caption         =   "Fecha"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   360
         TabIndex        =   46
         Top             =   120
         Width           =   450
      End
      Begin VB.Label Label12 
         AutoSize        =   -1  'True
         Caption         =   "Comprobante"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   1320
         TabIndex        =   45
         Top             =   120
         Width           =   945
      End
      Begin VB.Label Label13 
         AutoSize        =   -1  'True
         Caption         =   "Importe Total"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   4560
         TabIndex        =   44
         Top             =   120
         Width           =   930
      End
      Begin VB.Label Label14 
         AutoSize        =   -1  'True
         Caption         =   "Pagado"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   6120
         TabIndex        =   43
         Top             =   120
         Width           =   555
      End
      Begin VB.Label Label15 
         AutoSize        =   -1  'True
         Caption         =   "Adeuda"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   7320
         TabIndex        =   42
         Top             =   120
         Width           =   675
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         Caption         =   "Vencimiento"
         ForeColor       =   &H00800000&
         Height          =   195
         Left            =   3480
         TabIndex        =   41
         Top             =   120
         Width           =   870
      End
   End
   Begin VB.Frame Marco_Pago 
      Enabled         =   0   'False
      Height          =   2055
      Left            =   0
      TabIndex        =   47
      Top             =   3480
      Width           =   9735
      Begin VB.ListBox List2 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   1410
         Left            =   120
         TabIndex        =   7
         Top             =   360
         Width           =   9495
      End
   End
   Begin VB.Frame Marco_Total 
      Enabled         =   0   'False
      Height          =   735
      Left            =   0
      TabIndex        =   48
      Top             =   5520
      Width           =   9735
      Begin VB.TextBox TOTAL 
         BackColor       =   &H00FFFF80&
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   8040
         TabIndex        =   51
         Top             =   360
         Width           =   1575
      End
      Begin VB.TextBox Total_Efectivo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4800
         Locked          =   -1  'True
         MaxLength       =   11
         TabIndex        =   49
         Top             =   360
         Width           =   1455
      End
      Begin VB.TextBox Total_Cheque 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6240
         Locked          =   -1  'True
         MaxLength       =   11
         TabIndex        =   50
         Top             =   360
         Width           =   1455
      End
      Begin VB.Label Label17 
         Alignment       =   2  'Center
         Caption         =   "IMPORTE TOTAL"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   8040
         TabIndex        =   54
         Top             =   120
         Width           =   1575
      End
      Begin VB.Label Label19 
         Alignment       =   2  'Center
         Caption         =   "Importe Cheque"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6240
         TabIndex        =   53
         Top             =   120
         Width           =   1455
      End
      Begin VB.Label Label20 
         Alignment       =   2  'Center
         Caption         =   "Importe Efectivo"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   4800
         TabIndex        =   52
         Top             =   120
         Width           =   1455
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   55
      Top             =   6240
      Width           =   9735
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   8520
         Picture         =   "Fac_Pago_Prov.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   9
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7200
         Picture         =   "Fac_Pago_Prov.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Grabar e imprimir la factura.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Agregar 
         Height          =   615
         Left            =   120
         Picture         =   "Fac_Pago_Prov.frx":6594
         Style           =   1  'Graphical
         TabIndex        =   3
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Fac_Pago_Prov"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Agregar_Click()
    'Dim k As Integer
    Dim l As Integer
    Dim i As Integer
    Dim C As Integer
    
    If Val(Cuenta.Text) > 0 Then
        Comprobante.Clear
        
        i = 0
        l = 0
        
        If List2.ListCount = 0 Then
            For l = 0 To List1.ListCount - 1
                If List1.Selected(l) = True Then
                    Comprobante.AddItem Mid(List1.List(l), 10, 20) & " " & Mid(List1.List(l), 40, 10) & " " & Mid(List1.List(l), 62, 10)
                End If
            Next
        End If
        
        If Comprobante.ListCount > 0 Then
            Marco_Cuenta.Enabled = False
            Marco_Factura.Enabled = False
            Botonera.Enabled = False
                    
            Marco_Captura.Enabled = True
            Marco_Captura.Visible = True
            Terminar.Cancel = True
                    
            Comprobante.SetFocus
        Else
            List1.SetFocus
        End If
    Else
        Cuenta.Text = ""
        Cuenta.SetFocus
    End If
End Sub

Private Sub Banco_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Buscar_Cuenta_Click()
    MousePointer = 11

    If Cuentas_Encontradas.ListCount = 0 Then
        Qy = "SELECT Proveedor.Id_Proveedor, Proveedor.Nombre FROM Proveedor, CtaCte_Proveedor "
        Qy = Qy & "WHERE Id_Proveedor = Ctacte_Proveedor.Id_Cuenta "
        Qy = Qy & "GROUP BY Proveedor.Id_Proveedor, Proveedor.Nombre "
        Qy = Qy & "ORDER BY Proveedor.Nombre"
        AbreRs
        
        While Not Rs.EOF
            Txt = Trim(Rs.Fields("Nombre")) + Space$(30 - Len(Trim(Rs.Fields("Nombre"))))
            Txt = Txt & Trim(Rs.Fields("Id_Proveedor"))
            
            Cuentas_Encontradas.AddItem Txt
            Rs.MoveNext
        Wend
    End If
    MousePointer = 0
    
    Cuentas_Encontradas.Visible = True
    Cuentas_Encontradas.SetFocus
End Sub

Private Sub Cuentas_Encontradas_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Cuentas_Encontradas_LostFocus
End Sub

Private Sub Cuentas_Encontradas_LostFocus()
    Cuenta.Text = Mid(Cuentas_Encontradas.List(Cuentas_Encontradas.ListIndex), 31)
    Cuentas_Encontradas.Visible = False
    
    Cuenta.SetFocus
    Leer_Cuenta
End Sub

Private Sub Nuestro_Click()
    If Nuestro.Value = 1 Then
        If Trim(Titular.Text) = "" Then
            Titular.Text = Trim(cEmpresa)
        End If
    End If
End Sub

Private Sub Nuestro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Proveedor_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cobro_Change()
    If Val(Importe_Cheque.Text) > 0 And Val(Nro_Cheque.Text) > 0 And Val(Emision.Text) > 0 And Val(Cobro.Text) > 0 And Val(Mid(Banco.Text, 31)) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Cobro_GotFocus()
    Cobro.Text = Trim(Cobro.Text)
    Cobro.SelStart = 0
    Cobro.SelLength = Len(Cobro.Text)
End Sub

Private Sub Cobro_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cobro_LostFocus()
    Cobro.Text = ValidarFecha(Cobro.Text)
End Sub

Private Sub Comprobante_Click()
    If Trim(Comprobante.Text) <> "" Then
        Total_Factura.Text = Mid(Comprobante.Text, 22, 10)
        Adeuda.Text = Mid(Comprobante.Text, 33, 10)
    End If
End Sub

Private Sub Comprobante_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    If Val(Adeuda.Text) >= Val(Importe_Efectivo.Text) And Val(Adeuda.Text) >= Val(Importe_Cheque.Text) Then
        If Val(Importe_Efectivo.Text) > 0 Then
            Txt = "0 - CONTADO" '11 ESPACIOS
            Txt = Txt & Formateado(Val(Str(Importe_Efectivo.Text)), 2, 10, " ", False) & " "
            Txt = Txt & Space(11 + 11 + 4 + 3 + 31 + 31 + 12)
        Else
            Txt = "1 - CHEQUE " '11 espacios
            Txt = Txt & Formateado(Str(Val(Importe_Cheque.Text)), 2, 10, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(Nro_Cheque.Text)), 0, 10, " ", False) & " "
            Txt = Txt & Formateado(Str(Val(Mid(Banco.Text, 31))), 0, 4, " ", False) & " - "
            Txt = Txt & Trim(Mid(Banco.Text, 1, 30)) + Space(30 - Len(Trim(Mid(Banco.Text, 1, 30)))) & " "
            Txt = Txt & Trim(Titular.Text) + Space(30 - Len(Trim(Titular.Text))) & " "
            Txt = Txt & Format(Emision.Text, "DD/MM/YYYY") & " "
            Txt = Txt & Format(Cobro.Text, "DD/MM/YYYY") & " "
            Txt = Txt & Val(Nuestro.Value)
        End If
        Txt = Txt & Trim(Mid(Comprobante.Text, 1, 20))
        List2.AddItem Txt
        
        'Comprobante.RemoveItem Comprobante.ListIndex
        
        Calcular_Total_Orden_Pago
        
        Terminar_Click
    Else
        If Importe_Efectivo.Enabled = True Then
            Importe_Efectivo.Text = ""
            Importe_Efectivo.SetFocus
        Else
            Importe_Cheque.Text = ""
            Importe_Cheque.SetFocus
        End If
    End If
End Sub

'Private Sub Generar_Asiento()
'    Dim sCta_Deudora     As String
'    Dim sCta_Acreedora   As String
'
''    Fac_Pago_Imp.List1.Clear
''    Fac_Pago_Imp.Total_Debe.Text = ""
''    Fac_Pago_Imp.Total_Haber.Text = ""
''    Fac_Pago_Imp.Cuenta.Text = ""
''    Fac_Pago_Imp.Debe.Text = ""
''    Fac_Pago_Imp.Haber = ""
'    Txt = ""
'
'    If Val(TOTAL.Text) > 0 Then
'
'        Txt = Trim(cCta_Prov) + Space(20 - Len(Trim(cCta_Prov))) & " "
'        Txt = Txt & Mid(Trim(Nombre_Cuenta(cCta_Prov)), 1, 25) + Space(25 - Len(Mid(Trim(Nombre_Cuenta(cCta_Prov)), 1, 25))) & " "
'        Txt = Txt & Space(12)
'        Txt = Txt & Formateado(Str(Val(TOTAL.Text)), 2, 11, " ", False) & " "
'
'        Fac_Pago_Imp.List1.AddItem Txt
'
'
'        If Val(Total_Efectivo.Text) > 0 Then
'            Txt = Trim(cCta_Caja) + Space(20 - Len(Trim(cCta_Caja))) & " "
'            Txt = Txt & Mid(Trim(Nombre_Cuenta(cCta_Caja)), 1, 25) + Space(25 - Len(Mid(Trim(Nombre_Cuenta(cCta_Caja)), 1, 25))) & " "
'            Txt = Txt & Formateado(Str(Val(Total_Efectivo.Text)), 2, 11, " ", False) & " "
'            Txt = Txt & Space(12)
'
'            Fac_Pago_Imp.List1.AddItem Txt
'        End If
'
'        If Val(Total_Cheque.Text) > 0 Then
'            Txt = Trim(cCta_DocsCob) + Space(20 - Len(Trim(cCta_DocsCob))) & " "
'            Txt = Txt & Mid(Trim(Nombre_Cuenta(cCta_DocsCob)), 1, 25) + Space(25 - Len(Mid(Trim(Nombre_Cuenta(cCta_DocsCob)), 1, 25))) & " "
'            Txt = Txt & Formateado(Str(Val(Total_Cheque.Text)), 2, 11, " ", False) & " "
'            Txt = Txt & Space(12)
'
'            Fac_Pago_Imp.List1.AddItem Txt
'        End If
'    End If
'End Sub

Private Sub Calcular_Total_Orden_Pago()
    Dim i As Integer
    
    i = 0
    TOTAL.Text = ""
    Total_Cheque.Text = ""
    Total_Efectivo.Text = ""
    
    For i = 0 To List2.ListCount - 1
    
        TOTAL.Text = Val(TOTAL.Text) + Val(Mid(List2.List(i), 12, 10))
        
        If Val(Mid(List2.List(i), 1, 1)) = 0 Then
            
            Total_Efectivo.Text = Val(Total_Efectivo.Text) + Val(Mid(List2.List(i), 12, 10))
        
        ElseIf Val(Mid(List2.List(i), 1, 1)) = 1 Then
            
            Total_Cheque.Text = Val(Total_Cheque.Text) + Val(Mid(List2.List(i), 12, 10))
        
        End If
    
    Next
    
    Total_Efectivo.Text = Formateado(Str(Val(Total_Efectivo.Text)), 2, 11, " ", False)
    Total_Cheque.Text = Formateado(Str(Val(Total_Cheque.Text)), 2, 11, " ", False)
    TOTAL.Text = Formateado(Str(Val(TOTAL.Text)), 2, 11, " ", False)
End Sub

Private Sub Borrar_Campo_Captura()
    Nuestro.Value = 0
    Importe_Efectivo.Text = ""
    Importe_Cheque.Text = ""
    Nro_Cheque.Text = ""
    Titular.Text = ""
    Emision.Text = ""
    Cobro.Text = ""
    
    Comprobante.SetFocus
End Sub

'Private Sub Contable_Click()
'    MousePointer = 11
'
'    Fac_Pago_Prov.Enabled = False
'    Fac_Pago_Imp.Visible = True
'
'    If Val(Fac_Pago_Imp.Fecha.Text) = 0 Then Fac_Pago_Imp.Fecha.Text = Fac_Pago_Prov.Fecha.Text
'
'    Fac_Pago_Imp.Concepto.Text = "OP X " & Formateado(Str(Val(Fac_Pago_Prov.Numero.Text)), 0, 10, "0", False)
'
'    Generar_Asiento
'
'    MousePointer = 0
'    Fac_Pago_Imp.Tag = "Pago_Pro"
'    Fac_Pago_Imp.List1.SetFocus
'End Sub

Private Sub Cuenta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cuenta_LostFocus()
    If Val(Cuenta.Text) > 0 Then
        Leer_Cuenta
    End If
End Sub

Private Sub Leer_Cuenta()
    If Val(Cuenta.Text) > 0 Then
        Qy = "SELECT " & IIf(Proveedor.Value = True, "CtaCte_Proveedor.Id_Cuenta,Proveedor.Id_Proveedor,Proveedor.Nombre,Proveedor.Domicilio", "CtaCte_Proveedor.Id_Cuenta,Proveedor.Id_Proveedor,Proveedor.Nombre,Proveedor.Domicilio") & ", SUM(Debe - Haber) AS Saldo_Cuenta FROM " & IIf(Proveedor.Value = True, "CtaCte_Proveedor,Proveedor", "CtaCte_Proveedor,Proveedor") & " WHERE Id_Cuenta = " & Val(Cuenta.Text)
        Qy = Qy & " AND " & IIf(Proveedor.Value = True, "CtaCte_Proveedor.Id_Cuenta = Proveedor.Id_Proveedor", "CtaCte_Proveedor.Id_Cuenta = Proveedor.Id_Proveedor") & " "
        Qy = Qy & "GROUP BY " & IIf(Proveedor.Value = True, "CtaCte_Proveedor.Id_Cuenta,Proveedor.Id_Proveedor,Proveedor.Nombre,Proveedor.Domicilio", "CtaCte_Proveedor.Id_Cuenta,Proveedor.Id_Proveedor,Proveedor.Nombre,Proveedor.Domicilio")
        AbreRs
        
        If Rs.EOF Then
            MsgBox "La Cuenta es inexistente...", vbInformation, "Atenci�n.!"
            'Borrar_Campo_Cuenta
            Cuenta.SetFocus
        Else
            Nombre.Text = Rs.Fields("Nombre")
            Saldo.Text = Formateado(Str(Val(Rs.Fields("Saldo_Cuenta"))), 2, 10, " ", False)
            
            Mostrar_Facturas_Pendientes
            Marco_Factura.Enabled = True
            List1.SetFocus
        
        End If
    Else
        Cuenta.Text = ""
        Cuenta.SetFocus
    End If
End Sub

Private Sub Mostrar_Facturas_Pendientes()
    Dim Rs_Pago As Recordset
    
    List1.Clear
    
    'MUESTRO LAS FC SIN PAGO ALGUNO
    Qy = "SELECT CtaCte_Proveedor.Fecha, CtaCte_Proveedor.Comprobante, CtaCte_Proveedor.Vencimiento, CtaCte_Proveedor.Debe "
    Qy = Qy & "FROM CtaCte_Proveedor "
    Qy = Qy & "WHERE Estado = 0 "
    Qy = Qy & "AND Id_Cuenta = " & Val(Cuenta.Text) & " "
    Qy = Qy & "AND Debe > 0 "
    Qy = Qy & "AND Comprobante NOT IN (SELECT Referencia FROM Ctacte_Proveedor)"
    Qy = Qy & "GROUP BY CtaCte_Proveedor.Fecha, CtaCte_Proveedor.Comprobante, CtaCte_Proveedor.Vencimiento, CtaCte_Proveedor.Debe"
    AbreRs
    
    While Not Rs.EOF
        Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & " "
        Txt = Txt & Trim(Rs.Fields("Comprobante")) + Space(20 - Len(Trim(Rs.Fields("Comprobante")))) & " "
        Txt = Txt & Format(Rs.Fields("Vencimiento"), "dd/mm/yy") & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Debe"))), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(0)), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Debe"))), 2, 10, " ", False)
        
        List1.AddItem Txt
        
        Rs.MoveNext
    Wend
    
    Qy = "SELECT Ctacte_Proveedor.Fecha, Ctacte_Proveedor.Comprobante, Ctacte_Proveedor.Vencimiento, Ctacte_Proveedor.Debe FROM Ctacte_Proveedor "
    Qy = Qy & "WHERE Id_Cuenta = " & Val(Cuenta.Text) & " "
    Qy = Qy & "AND Estado = 0 "
    Qy = Qy & "AND Comprobante IN (SELECT Referencia FROM Ctacte_Proveedor WHERE Id_Cuenta = " & Val(Cuenta.Text) & ") "
    Qy = Qy & "GROUP BY Ctacte_Proveedor.Fecha, Ctacte_Proveedor.Comprobante, Ctacte_Proveedor.Vencimiento, Ctacte_Proveedor.Debe"
    AbreRs
    
    While Not Rs.EOF
        Txt = Format(Rs.Fields("Fecha"), "dd/mm/yy") & " "
        Txt = Txt & Trim(Rs.Fields("Comprobante")) + Space(20 - Len(Trim(Rs.Fields("Comprobante")))) & " "
        Txt = Txt & Format(Rs.Fields("Vencimiento"), "dd/mm/yy") & " "
        Txt = Txt & Formateado(Str(Val(Rs.Fields("Debe"))), 2, 10, " ", False) & " "
        
        Qy = "SELECT SUM(Haber) FROM Ctacte_Proveedor "
        Qy = Qy & "WHERE Id_Cuenta = " & Val(Cuenta.Text) & " "
        Qy = Qy & "AND Referencia = '" & Trim(Rs.Fields("Comprobante")) & "'"
        Set Rs_Pago = Db.OpenRecordset(Qy)
        
        Txt = Txt & Formateado(Str(Val(Rs_Pago.Fields(0))), 2, 10, " ", False) & " "
        Txt = Txt & Formateado(Str(Val(Val(Rs.Fields("Debe")) - Val(Rs_Pago.Fields(0)))), 2, 10, " ", False)
        
        List1.AddItem Txt
        Rs.MoveNext
    Wend
End Sub

Private Sub Emision_Change()
    If Val(Importe_Cheque.Text) > 0 And Val(Nro_Cheque.Text) > 0 And Val(Emision.Text) > 0 And Val(Cobro.Text) > 0 And Val(Mid(Banco.Text, 31)) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Emision_GotFocus()
    Emision.Text = Trim(Emision.Text)
    Emision.SelStart = 0
    Emision.SelLength = Len(Emision.Text)
End Sub

Private Sub Emision_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Emision_LostFocus()
    Emision.Text = ValidarFecha(Emision.Text)
End Sub

Private Sub Fecha_GotFocus()
    If Val(Fecha.Text) = 0 Then Fecha.Text = Fecha_Fiscal
    Fecha.SelStart = 0
    Fecha.SelLength = Len(Fecha.Text)
End Sub

Private Sub Fecha_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Fecha_LostFocus()
    Fecha.Text = ValidarFecha(Fecha.Text)
    
        If Val(Mid(Fecha.Text, 7, 4) & Mid(Fecha.Text, 4, 2) & Mid(Fecha.Text, 1, 2)) >= 20060330 Then
            MsgBox "No se pueden ingresar registros a la base de datos, contactese con el administrador.", vbInformation, "Atenci�n.!"
            Unload Me
        End If
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 18
    Me.Left = (Screen.Width - Me.Width) / 2
    
    Cargar_Datos
    
    'Load Fac_Pago_Imp
'    Fac_Pago_Imp.Show
'    Fac_Pago_Imp.Visible = False
End Sub

Private Sub Cargar_Datos()
    Qy = "SELECT * FROM Banco ORDER BY Denominacion"
    AbreRs
    
    While Not Rs.EOF
        Banco.AddItem Trim(Rs.Fields("Denominacion")) + Space(30 - Len(Trim(Rs.Fields("Denominacion")))) & " " & Val(Rs.Fields("Id_Banco"))
        
        Rs.MoveNext
    Wend
End Sub

Private Sub Form_Unload(Cancel As Integer)
    'Unload Fac_Pago_Imp
End Sub

Private Sub Grabar_Click()
    Dim Asiento As Integer
    Dim i       As Integer
    Dim l       As Integer
    Dim Alm_Cta As String
        Dim fcDebe  As Double
    Dim fcHaber As Double
                
'    If Fac_Pago_Imp.List1.ListCount = 0 Then
'        Generar_Asiento
'    End If
    
    If Val(List2.ListCount) > 0 And Val(TOTAL.Text) > 0 And Val(Fecha.Text) > 0 And Val(Numero.Text) > 0 And Val(Cuenta.Text) > 0 Then
    
        'Grabo el Orden_Pago
        Qy = "INSERT INTO Orden_Pago VALUES ("
        Qy = Qy & Val(Numero.Text)
        Qy = Qy & ", #" & Trim(Format(Fecha.Text, "mm/dd/yyyy")) & "#"
        Qy = Qy & ", 'P'"
        Qy = Qy & ", " & Val(Cuenta.Text)
        Qy = Qy & ", 0"
        Qy = Qy & ", " & Val(TOTAL.Text)
        Qy = Qy & ", " & Val(Asiento)
        Qy = Qy & ", " & Val(0) & ")"
        Db.Execute (Qy)
        
        'Grabo las formas de pago y actualizo la ctacte
        For i = 0 To List2.ListCount - 1
        
            'Grabo las formas de pago
            Qy = "INSERT INTO Orden_Pago_Item VALUES ("
            Qy = Qy & Val(Numero.Text)
            Qy = Qy & ", " & Val(i)
            Qy = Qy & ", " & Val(Mid(List2.List(i), 1, 1))
            Qy = Qy & ", " & Val(Mid(List2.List(i), 24, 10))
            Qy = Qy & ", " & Val(Mid(List2.List(i), 36, 4))
            Qy = Qy & ", " & Val(Mid(List2.List(i), 12, 10)) & ")"
            Db.Execute (Qy)
            
            'Actualizo la Cuenta Corriente del Proveedor
            Qy = "INSERT INTO CtaCte_Proveedor VALUES ("
            Qy = Qy & Val(Cuenta.Text)
            Qy = Qy & ", #" & Trim(Format(Fecha.Text, "mm/dd/yyyy")) & "#"
            Qy = Qy & ", 'OP X " & Formateado(Str(Val(Numero.Text)), 0, 10, "0", False) & "'"
            Qy = Qy & ", '" & Trim(Mid(List2.List(i), 126, 20)) & "'"
            Qy = Qy & ", " & Val(i)
            Qy = Qy & ", '" & "PAGO A CUENTA" & "'"
            Qy = Qy & ", #" & Trim(Format(Fecha.Text, "mm/dd/yyyy")) & "# "
            Qy = Qy & ", 0"
            Qy = Qy & ", " & Val(Mid(List2.List(i), 12, 10))
            Qy = Qy & ", 0"
            Qy = Qy & ", " & Val(Asiento)
            Qy = Qy & ", " & Val(0) & ")"
            Db.Execute (Qy)
            
            'ACTUALIZO EL ESTADO DEL COMPROBANTE AL QUE LE IMPUTO EL PAGO
            fcDebe = 0
            fcHaber = 0
            
            Qy = "SELECT SUM (Debe) FROM CtaCte_Proveedor WHERE Id_Cuenta = " & Val(Cuenta.Text) & " "
            Qy = Qy & "AND Comprobante = '" & Trim(Mid(List2.List(i), 126, 20)) & "'"
            AbreRs
            
            If Not IsNull(Rs.Fields(0)) = True Then fcDebe = Val(Rs.Fields(0))
                
            Qy = "SELECT SUM (Haber) FROM CtaCte_Proveedor WHERE Id_Cuenta = " & Val(Cuenta.Text) & " "
            Qy = Qy & "AND Referencia = '" & Trim(Mid(List2.List(i), 126, 20)) & "'"
            AbreRs

            If Not IsNull(Rs.Fields(0)) = True Then fcHaber = Val(Rs.Fields(0))
            
            fcDebe = Formateado(Str(Val(fcDebe)), 2, 0, "", False)
            fcHaber = Formateado(Str(Val(fcHaber)), 2, 0, "", False)
            
            If Val(fcDebe) <= Val(fcHaber) Then
                Qy = "UPDATE CtaCte_Proveedor SET "
                Qy = Qy & "Estado = 1 "
                Qy = Qy & "WHERE Comprobante = '" & Trim(Mid(List2.List(i), 126, 20)) & "' "
                Qy = Qy & "AND Id_Cuenta = " & Val(Cuenta.Text)
                Db.Execute (Qy)
            End If
            
            If Val(Mid(List2.List(i), 1, 1)) = 1 Then
                
                Qy = "INSERT INTO Cheque VALUES ("
                Qy = Qy & Val(Mid(List2.List(i), 23, 10))
                Qy = Qy & ", " & Val(Mid(List2.List(i), 34, 4))
                Qy = Qy & ", 'OP X " & Trim(Numero.Text) & "'"
                Qy = Qy & ", '-'"
                Qy = Qy & ", 'P'"
                Qy = Qy & ", '" & Trim(Mid(List2.List(i), 72, 30)) & "'"
                Qy = Qy & ", " & Val(Mid(List2.List(i), 115, 1))
                Qy = Qy & ", '" & Trim(Mid(List2.List(i), 103, 10)) & "'"
                Qy = Qy & ", " & Val(DateDiff("d", Trim(Mid(List2.List(i), 103, 10)), Trim(Mid(List2.List(i), 114, 10))))
                Qy = Qy & ", '" & Trim(Mid(List2.List(i), 114, 10)) & "'"
                Qy = Qy & ", 0"
                Qy = Qy & ", " & Val(Mid(List2.List(i), 12, 10))
                Qy = Qy & ", 1, '-')"
                Db.Execute (Qy)
            
            End If
        Next
        
        If MsgBox("El comprobante ha sido guardado, desea imprimirlo ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
            Imprimir
        End If
        
        Salir_Click
    Else
        MsgBox "Se han encontrado errores en los datos del Orden_Pago, verifique e intente guardar nuevamente.", vbInformation, "El Comprobante no ha sido guardado."
        Agregar.SetFocus
    End If
End Sub

Private Sub Imprimir()
    MsgBox "Esta Opci�n no esta disponible en esta versi�n del sistema.", vbInformation, "Atenci�n"
End Sub

Private Sub Importe_Cheque_Change()
    If Val(Importe_Cheque.Text) > 0 And Val(Nro_Cheque.Text) > 0 And Val(Emision.Text) > 0 And Val(Cobro.Text) > 0 And Val(Mid(Banco.Text, 31)) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
    
    If Val(Importe_Cheque.Text) > 0 Then
        Importe_Efectivo.Enabled = False
        Nro_Cheque.Enabled = True
        Banco.Enabled = True
        Titular.Enabled = True
        Emision.Enabled = True
        Cobro.Enabled = True
        
        Importe_Efectivo.BackColor = &HE0E0E0
        Nro_Cheque.BackColor = vbWhite
        Banco.BackColor = vbWhite
        Titular.BackColor = vbWhite
        Emision.BackColor = vbWhite
        Cobro.BackColor = vbWhite
        Nuestro.Enabled = True
    Else
        Importe_Efectivo.Enabled = True
        Nro_Cheque.Enabled = False
        Banco.Enabled = False
        Titular.Enabled = False
        Emision.Enabled = False
        Cobro.Enabled = False
        Nuestro.Enabled = False
        
        Importe_Efectivo.BackColor = vbWhite
        Nro_Cheque.BackColor = &HE0E0E0
        Banco.BackColor = &HE0E0E0
        Titular.BackColor = &HE0E0E0
        Emision.BackColor = &HE0E0E0
        Cobro.BackColor = &HE0E0E0
    End If
End Sub

Private Sub Importe_Cheque_GotFocus()
    Importe_Cheque.Text = Trim(Importe_Cheque.Text)
    Importe_Cheque.SelStart = 0
    Importe_Cheque.SelLength = Len(Importe_Cheque.Text)
End Sub

Private Sub Importe_Cheque_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Importe_Cheque_LostFocus()
    Importe_Cheque.Text = Formateado(Str(Val(Importe_Cheque.Text)), 2, 10, " ", False)
End Sub

Private Sub Importe_Efectivo_Change()
    If Val(Importe_Efectivo.Text) > 0 Then
        Importe_Cheque.Enabled = False
        Importe_Cheque.BackColor = &HE0E0E0
        Confirma.Enabled = True
    Else
        Importe_Cheque.Enabled = True
        Importe_Cheque.BackColor = vbWhite
        Confirma.Enabled = False
    End If
End Sub

Private Sub Importe_Efectivo_GotFocus()
    Importe_Efectivo.Text = Trim(Importe_Efectivo.Text)
    Importe_Efectivo.SelStart = 0
    Importe_Efectivo.SelLength = Len(Importe_Efectivo.Text)
End Sub

Private Sub Importe_Efectivo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Importe_Efectivo_LostFocus()
    Importe_Efectivo.Text = Formateado(Str(Val(Importe_Efectivo.Text)), 2, 10, " ", False)
End Sub

Private Sub List1_Click()
    Dim l As Integer
    Dim i As Integer
    Dim C As Integer
    
    i = 0
    l = 0
    C = List2.ListCount
    
    For l = 0 To List1.ListCount - 1
        If List1.Selected(l) = False Then
            While i <= C
                If Trim(Mid(List2.List(i), 126, 20)) = Trim(Mid(List1.List(l), 10, 20)) Then
                    List2.RemoveItem i
                    i = i - 1
                End If
                
                i = i + 1
            Wend
        End If
            
        i = 0
        C = List2.ListCount
    Next
End Sub

Private Sub List1_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Agregar.SetFocus
End Sub

Private Sub Nro_Cheque_Change()
    If Val(Importe_Cheque.Text) > 0 And Val(Nro_Cheque.Text) > 0 And Val(Emision.Text) > 0 And Val(Cobro.Text) > 0 And Val(Mid(Banco.Text, 31)) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Nro_Cheque_GotFocus()
    Nro_Cheque.Text = Trim(Nro_Cheque.Text)
    Nro_Cheque.SelStart = 0
    Nro_Cheque.SelLength = Len(Nro_Cheque.Text)
End Sub

Private Sub Nro_Cheque_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nro_Cheque_LostFocus()
    Nro_Cheque.Text = Formateado(Str(Val(Nro_Cheque.Text)), 0, 10, " ", False)
End Sub

Private Sub Numero_GotFocus()
    If Val(Numero.Text) = 0 Then
        Qy = "SELECT MAX(Id_Orden_Pago) FROM Orden_Pago"
        AbreRs
        
        Numero.Text = 1
        If Not IsNull(Rs.Fields(0)) = True Then Numero.Text = Val(Rs.Fields(0)) + 1
    End If
    Numero.Text = Val(Numero.Text)
    Numero.SelStart = 0
    Numero.SelLength = Len(Numero.Text)
End Sub

Private Sub Numero_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Numero_LostFocus()
    Numero.Text = Formateado(Str(Val(Numero.Text)), 0, 10, "0", False)
    
    Qy = "SELECT Id_Orden_Pago FROM Orden_Pago WHERE Id_Orden_Pago = " & Val(Numero.Text)
    AbreRs
    
    If Not Rs.EOF Then
        MsgBox "El n�mero ingresado ya existe en la base de datos, verifique e ingrese nuevamente.", vbInformation, "Atenci�n!"
        Numero.Text = ""
        Numero.SetFocus
    End If
End Sub

Private Sub Salir_Click()
    If Cuenta.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
'    Fac_Pago_Imp.List1.Clear
'    Fac_Pago_Imp.Total_Debe.Text = ""
'    Fac_Pago_Imp.Total_Haber.Text = ""
'    Fac_Pago_Imp.Cuenta.Text = ""
'    Fac_Pago_Imp.Debe.Text = ""
'    Fac_Pago_Imp.Haber = ""
'    Fac_Pago_Imp.Fecha.Text = ""
'    Fac_Pago_Imp.Concepto.Text = ""

    Fecha.Text = ""
    Numero.Text = ""
    Cuenta.Text = ""
    Nombre.Text = ""
    Saldo.Text = ""
    TOTAL.Text = ""
    Total_Efectivo.Text = ""
    Total_Cheque.Text = ""
    List1.Clear
    List2.Clear
    
    Grabar.Enabled = False
    Marco_Cuenta.Enabled = True
    Marco_Factura.Enabled = False
    Marco_Total.Enabled = False
    
    Fecha.SetFocus
End Sub

Private Sub Terminar_Click()
    If Val(Importe_Efectivo.Text) = 0 And Val(Importe_Cheque.Text) = 0 Then
        Marco_Factura.Enabled = True
        Marco_Total.Enabled = True
        Botonera.Enabled = True
        Marco_Captura.Visible = False
        Salir.Cancel = True
        
        If List2.ListCount = 0 Then
            List1.SetFocus
        Else
            Contable.SetFocus
        End If
    Else
        Borrar_Campo_Captura
    End If
End Sub

Private Sub Titular_Change()
    If Val(Importe_Cheque.Text) > 0 And Val(Nro_Cheque.Text) > 0 And Val(Emision.Text) > 0 And Val(Cobro.Text) > 0 And Val(Mid(Banco.Text, 31)) > 0 Then
        Confirma.Enabled = True
    Else
        Confirma.Enabled = False
    End If
End Sub

Private Sub Titular_GotFocus()
    Titular.Text = Trim(Titular.Text)
    Titular.SelStart = 0
    Titular.SelLength = Len(Titular.Text)
End Sub

Private Sub Titular_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Titular_LostFocus()
    Titular.Text = FiltroCaracter(Titular.Text)
End Sub

Private Sub Total_Change()
    
    If Val(TOTAL.Text) > 0 Then
        
        Grabar.Enabled = True
        Contable.Enabled = True
    
    Else
        
        Grabar.Enabled = False
        Contable.Enabled = False
    
    End If

End Sub

