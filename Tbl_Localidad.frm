VERSION 5.00
Begin VB.Form Tbl_Localidad 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaciσn de Localidades.-"
   ClientHeight    =   3855
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   5655
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   MinButton       =   0   'False
   ScaleHeight     =   3855
   ScaleWidth      =   5655
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Codigo_Postal 
      Height          =   735
      Left            =   0
      TabIndex        =   8
      Top             =   0
      Width           =   5655
      Begin VB.ComboBox Codigos_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1200
         TabIndex        =   16
         Top             =   240
         Visible         =   0   'False
         Width           =   4335
      End
      Begin VB.TextBox Encontrada 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2160
         TabIndex        =   12
         Top             =   240
         Width           =   3375
      End
      Begin VB.TextBox Codigo_Postal 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         MaxLength       =   5
         TabIndex        =   0
         Top             =   240
         Width           =   855
      End
      Begin VB.CommandButton Buscar_Postales 
         Caption         =   "&Cσdigo:"
         Height          =   255
         Left            =   120
         TabIndex        =   11
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Datos 
      Enabled         =   0   'False
      Height          =   2175
      Left            =   0
      TabIndex        =   9
      Top             =   720
      Width           =   5655
      Begin VB.TextBox Prefijo 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         TabIndex        =   3
         Top             =   960
         Width           =   1335
      End
      Begin VB.ComboBox Pcia 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   1320
         Sorted          =   -1  'True
         TabIndex        =   2
         Top             =   600
         Width           =   4215
      End
      Begin VB.TextBox Localidad 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1320
         MaxLength       =   30
         TabIndex        =   1
         Top             =   240
         Width           =   4215
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Telediscado:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Provincia:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   14
         Top             =   600
         Width           =   1095
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Localidad:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   13
         Top             =   240
         Width           =   1095
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   10
      Top             =   2880
      Width           =   5655
      Begin VB.CommandButton Imprimir 
         Height          =   615
         Left            =   3480
         Picture         =   "Tbl_Localidad.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Imprimir Lista de Localidades.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   4560
         Picture         =   "Tbl_Localidad.frx":5C12
         Style           =   1  'Graphical
         TabIndex        =   7
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   1080
         Picture         =   "Tbl_Localidad.frx":BE9C
         Style           =   1  'Graphical
         TabIndex        =   6
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Tbl_Localidad.frx":12126
         Style           =   1  'Graphical
         TabIndex        =   4
         Top             =   240
         Width           =   975
      End
   End
End
Attribute VB_Name = "Tbl_Localidad"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim Cuenta_Paginas As Single

Private Sub Borrar_Click()
    If MsgBox("Desea Borrar ιste registro. ?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        qy = "DELETE FROM Localidad WHERE Id_Localidad = " & Trim(Str(Val(Codigo_Postal.Text)))
        Db.Execute (qy)
    End If
    
    Salir_Click
End Sub

Private Sub Buscar_Postales_Click()
    Codigos_Encontrados.Visible = True
    Codigos_Encontrados.Clear
    MousePointer = 11
    qy = "SELECT * FROM Localidad ORDER BY Localidad"
    AbreRs
    
    While Not Rs.EOF
        Codigos_Encontrados.AddItem Trim(Rs.Fields("Localidad")) + Space(30 - Len(Trim(Rs.Fields("Localidad")))) & " " & Trim(Rs.Fields("Id_Localidad"))
        Rs.MoveNext
    Wend
    MousePointer = 0
    Codigos_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Codigo_Postal_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Codigo_Postal_LostFocus
End Sub

Private Sub Codigo_Postal_LostFocus()
    If Val(Codigo_Postal.Text) > 0 Then
        Leer_Localidad
    End If
End Sub

Private Sub Leer_Localidad()
    If Val(Codigo_Postal.Text) > 0 Then
        
        qy = "SELECT * FROM Localidad WHERE Id_Localidad = " & Trim(Str(Val(Codigo_Postal.Text)))
        AbreRs
        
        If Rs.EOF Then
            If MsgBox("La Localidad es inexistente, la incorpora ahora. ?", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
                Grabar.Tag = "Alta"
                Encontrada.Text = "NUEVA LOCALIDAD."
                Grabar.Enabled = True
                Borrar.Enabled = False
                
                Marco_Datos.Enabled = True
                Marco_Codigo_Postal.Enabled = False
                
                Localidad.SetFocus
            Else
                Borrar_Campo
            End If
        Else
            Grabar.Enabled = True
            Borrar.Enabled = True
            Marco_Datos.Enabled = True
            Marco_Codigo_Postal.Enabled = False
            Mostrar_Localidad
            
            Localidad.SetFocus
        End If
    Else
        Codigo_Postal.Text = ""
        Codigo_Postal.SetFocus
    End If
End Sub

Private Sub Mostrar_Localidad()
    Localidad.Text = Rs.Fields("Localidad")
    Pcia.Text = Rs.Fields("Provincia")
    Prefijo.Text = Rs.Fields("Telediscado")
End Sub

Private Sub Borrar_Campo()
    Localidad.Text = ""
    Codigo_Postal.Text = ""
    Prefijo.Text = ""
    Pcia.Text = ""
    Encontrada.Text = ""
    
    Marco_Datos.Enabled = False
    Marco_Codigo_Postal.Enabled = True
    Grabar.Enabled = False
    Borrar.Enabled = False
    
    Codigo_Postal.SetFocus
End Sub

Private Sub Codigos_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Codigo_Postal.SetFocus
End Sub

Private Sub Codigos_Encontrados_LostFocus()
    Codigo_Postal.Text = Mid(Codigos_Encontrados.List(Codigos_Encontrados.ListIndex), 32)
    Codigos_Encontrados.Visible = False
    Codigo_Postal_LostFocus
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualizaciσn del Registro de Localidades.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    Pcia.AddItem "BUENOS AIRES"
    Pcia.AddItem "CATAMARCA"
    Pcia.AddItem "JUJUY"
    Pcia.AddItem "LA RIOJA"
    Pcia.AddItem "LA PAMPA"
    Pcia.AddItem "RIO NEGRO"
    Pcia.AddItem "NEUQUEN"
    Pcia.AddItem "SANTA CRUZ"
    Pcia.AddItem "USUAIA"
    Pcia.AddItem "CΣRDOBA"
    Pcia.AddItem "MENDOZA"
    Pcia.AddItem "FORMOZA"
    Pcia.AddItem "CHACO"
    Pcia.AddItem "MISIONES"
    Pcia.AddItem "ENTRE RIOS"
    Pcia.AddItem "TUCUMΑN"
    Pcia.AddItem "SANTIAGO DEL ESTERO"
    Pcia.AddItem "CHUBUT"
    Pcia.AddItem "SAN JUAN"
    Pcia.AddItem "SALTA"
    Pcia.AddItem "SANTA FΙ"
    Pcia.AddItem "SAN LUIS"
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        
        qy = "INSERT INTO Localidad VALUES ("
        qy = qy & Trim(Str(Val(Codigo_Postal.Text)))
        qy = qy & ", '" & Trim(Localidad.Text) & "'"
        qy = qy & ", '" & Trim(Pcia.Text) & "'"
        qy = qy & ", '" & Trim(Prefijo.Text) & "')"
        Db.Execute (qy)
        
    Else
        
        qy = "UPDATE Localidad SET "
        qy = qy & "Localidad = '" & Trim(Localidad.Text) & "',"
        qy = qy & "Provincia = '" & Trim(Pcia.Text) & "',"
        qy = qy & "Telediscado = '" & Trim(Prefijo.Text) & "' "
        qy = qy & "WHERE Id_Localidad = " & Trim(Str(Val(Codigo_Postal.Text)))
        Db.Execute (qy)
        
    End If
    
    Salir_Click
End Sub

Private Sub Localidad_GotFocus()
    Localidad.SelStart = 0
    Localidad.SelText = ""
    Localidad.SelLength = Len(Localidad.Text)
End Sub

Private Sub Localidad_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Localidad_LostFocus()
    Localidad.Text = FiltroCaracter(Localidad.Text)
    Localidad.Text = UCase(Localidad.Text)
End Sub

Private Sub Pcia_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Prefijo_GotFocus()
    Prefijo.SelStart = 0
    Prefijo.SelText = ""
    Prefijo.SelLength = Len(Prefijo.Text)
End Sub

Private Sub Prefijo_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Prefijo_LostFocus()
    Prefijo.Text = FiltroCaracter(Prefijo.Text)
End Sub

Private Sub Salir_Click()
    If Codigo_Postal.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Imprimir_Click()
    If MsgBox("Confirma la impresiσn del listado de localidades. ?.", vbQuestion + vbYesNo, "Atenciσn.!") = vbYes Then
        Dim l As Long
        
        qy = "SELECT * FROM Localidad ORDER BY Localidad"
        AbreRs
        
        If Rs.RecordCount > 0 Then
            Cuenta_Paginas = Val(Rs.RecordCount)
            l = 0
            
            Printer.Font = "Courier New"
            Printer.FontSize = 9
            
            Imprimir_Encabezado
            
            While Not Rs.EOF
                
                If l <= 65 Then
                    Printer.Print " " & Formateado(Str(Val(Rs.Fields("Id_Localidad"))), 0, 5, " ", False) & " " & Trim(Rs.Fields("Localidad")) + Space(30 - Len(Trim(Rs.Fields("Localidad")))) & " " & Trim(Rs.Fields("Provincia")) + Space(30 - Len(Trim(Rs.Fields("Provincia")))) & " " & Trim(Rs.Fields("Telediscado"))
                    l = l + 1
                Else
                    Printer.Print " "
                    
                    l = 0
                    Printer.NewPage
                    Imprimir_Encabezado
                End If
                
                Rs.MoveNext
            Wend
            
            Printer.Print " "
            Printer.EndDoc
        End If
        
        Salir.SetFocus
    Else
        If Marco_Codigo_Postal.Enabled = True Then
            Codigo_Postal.SetFocus
        Else
            Salir.SetFocus
        End If
    End If
End Sub

Private Sub Imprimir_Encabezado()
    Imprimir.Tag = Int(Cuenta_Paginas / 65) + 1
    Printer.Print " " & Trim(cEmpresa) + Space(30 - Len(Trim(cEmpresa))) & "                                                       Pαgina.: " & Printer.Page & "/" & Imprimir.Tag
    Printer.Print " Avda. Srgto. Cabral y Los Medanos                                                   Fecha..: " & Format(Now, "dd/mm/yyyy")
    Printer.Print " N. DE LA RIESTRA (6663)                                                             Hora...: " & Format(Time, "hh.mm"); ""
    Printer.Print " TELΙFONO / FAX: 02343 - 440304"
    Printer.Print " e-mail: pierttei@nriestra.com.ar"
    Printer.Print
    Printer.Print "                                    LISTA DE LOCALIDADES POR ORDEN ALFABΙTICO"
    Printer.Print
    Printer.Print " "
    Printer.Print "  Cσd. Localidad                      Provincia                      Telediscado"
    Printer.Print " "
End Sub
