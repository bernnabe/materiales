VERSION 5.00
Begin VB.Form Dat_Cli 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Actualizaci�n de Clientes.-"
   ClientHeight    =   6735
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   7335
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   6735
   ScaleWidth      =   7335
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox Memo 
      BeginProperty Font 
         Name            =   "Courier"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   4695
      Left            =   120
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   46
      Top             =   960
      Visible         =   0   'False
      Width           =   7095
   End
   Begin VB.Frame Id_Cliente 
      Height          =   735
      Left            =   0
      TabIndex        =   19
      Top             =   0
      Width           =   7335
      Begin VB.ComboBox Clientes_Encontrados 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   44
         Top             =   240
         Visible         =   0   'False
         Width           =   4935
      End
      Begin VB.TextBox Fecha 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5880
         TabIndex        =   28
         Top             =   240
         Width           =   1335
      End
      Begin VB.TextBox Encontrado 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         TabIndex        =   24
         Top             =   240
         Width           =   3615
      End
      Begin VB.CommandButton Buscar_Clientes 
         Caption         =   "&Cliente:"
         Height          =   255
         Left            =   120
         TabIndex        =   20
         ToolTipText     =   "Mostrar una Lista de Clientes.-"
         Top             =   240
         Width           =   855
      End
      Begin VB.TextBox Cliente 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   1200
         MaxLength       =   5
         TabIndex        =   0
         Top             =   240
         Width           =   855
      End
   End
   Begin VB.Frame Marco_Datos 
      Enabled         =   0   'False
      Height          =   5055
      Left            =   0
      TabIndex        =   22
      Top             =   720
      Width           =   7335
      Begin VB.ComboBox Categoria 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   14
         Top             =   4200
         Width           =   4935
      End
      Begin VB.TextBox Saldo 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5880
         TabIndex        =   27
         Top             =   4560
         Width           =   1335
      End
      Begin VB.TextBox Saldo_Autorizado 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   10
         TabIndex        =   15
         Top             =   4560
         Width           =   1335
      End
      Begin VB.ComboBox Estado 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   13
         Top             =   3840
         Width           =   4935
      End
      Begin VB.ComboBox Forma_Pago 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   12
         Top             =   3480
         Width           =   4935
      End
      Begin VB.TextBox Nro_Documento 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   5520
         MaxLength       =   13
         TabIndex        =   11
         Top             =   3120
         Width           =   1695
      End
      Begin VB.ComboBox Tipo_Documento 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   10
         Top             =   3120
         Width           =   3135
      End
      Begin VB.ComboBox Condicion_Iva 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   315
         Left            =   2280
         TabIndex        =   9
         Top             =   2760
         Width           =   4935
      End
      Begin VB.TextBox Email 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   50
         TabIndex        =   8
         Top             =   2400
         Width           =   4935
      End
      Begin VB.TextBox Telefono 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   20
         TabIndex        =   7
         Top             =   2040
         Width           =   3015
      End
      Begin VB.TextBox Provincia 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         TabIndex        =   26
         Top             =   1680
         Width           =   4935
      End
      Begin VB.TextBox Localidad 
         BackColor       =   &H00E0E0E0&
         Enabled         =   0   'False
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   3240
         TabIndex        =   25
         Top             =   1320
         Width           =   3975
      End
      Begin VB.TextBox Codigo_Postal 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   5
         TabIndex        =   6
         Top             =   1320
         Width           =   855
      End
      Begin VB.TextBox Depto 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   6360
         MaxLength       =   2
         TabIndex        =   5
         Top             =   960
         Width           =   855
      End
      Begin VB.TextBox Piso 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   4440
         MaxLength       =   2
         TabIndex        =   4
         Top             =   960
         Width           =   855
      End
      Begin VB.TextBox Numero 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   5
         TabIndex        =   3
         Top             =   960
         Width           =   855
      End
      Begin VB.TextBox Domicilio 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   25
         TabIndex        =   2
         Top             =   600
         Width           =   4935
      End
      Begin VB.TextBox Nombre 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   2280
         MaxLength       =   30
         TabIndex        =   1
         Top             =   240
         Width           =   4935
      End
      Begin VB.Label Label16 
         Alignment       =   1  'Right Justify
         Caption         =   "Categor�a:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   45
         Top             =   4200
         Width           =   2055
      End
      Begin VB.Label Label15 
         Alignment       =   1  'Right Justify
         Caption         =   "Depto:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   5400
         TabIndex        =   43
         Top             =   960
         Width           =   855
      End
      Begin VB.Label Label14 
         Alignment       =   1  'Right Justify
         Caption         =   "Piso:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3240
         TabIndex        =   42
         Top             =   960
         Width           =   1095
      End
      Begin VB.Label Label13 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo Actual:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   3720
         TabIndex        =   41
         Top             =   4560
         Width           =   2055
      End
      Begin VB.Label Label12 
         Alignment       =   1  'Right Justify
         Caption         =   "Saldo Autorizado:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   40
         Top             =   4560
         Width           =   2055
      End
      Begin VB.Label Label11 
         Alignment       =   1  'Right Justify
         Caption         =   "Estado de Cuenta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   39
         Top             =   3840
         Width           =   2055
      End
      Begin VB.Label Label10 
         Alignment       =   1  'Right Justify
         Caption         =   "Forma de Pago:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   38
         Top             =   3480
         Width           =   2055
      End
      Begin VB.Label Label9 
         Alignment       =   1  'Right Justify
         Caption         =   "Tipo y N�mero de Doc.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   37
         Top             =   3120
         Width           =   2055
      End
      Begin VB.Label Label8 
         Alignment       =   1  'Right Justify
         Caption         =   "Condici�n de I.V.A.:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   36
         Top             =   2760
         Width           =   2055
      End
      Begin VB.Label Label7 
         Alignment       =   1  'Right Justify
         Caption         =   "E - Mail:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   35
         Top             =   2400
         Width           =   2055
      End
      Begin VB.Label Label6 
         Alignment       =   1  'Right Justify
         Caption         =   "Tel�fono:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   34
         Top             =   2040
         Width           =   2055
      End
      Begin VB.Label Label5 
         Alignment       =   1  'Right Justify
         Caption         =   "Provincia:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   33
         Top             =   1680
         Width           =   2055
      End
      Begin VB.Label Label4 
         Alignment       =   1  'Right Justify
         Caption         =   "C�digo Postal:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   32
         Top             =   1320
         Width           =   2055
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Altura:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   31
         Top             =   960
         Width           =   2055
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Domicilio:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   30
         Top             =   600
         Width           =   2055
      End
      Begin VB.Label Label1 
         Alignment       =   1  'Right Justify
         Caption         =   "Nombre:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   29
         Top             =   240
         Width           =   2055
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   23
      Top             =   5760
      Width           =   7335
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   6240
         Picture         =   "Dat_Cli.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   18
         ToolTipText     =   "Cancelar Salir.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Borrar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   5040
         Picture         =   "Dat_Cli.frx":628A
         Style           =   1  'Graphical
         TabIndex        =   17
         ToolTipText     =   "Borrar el Cliente de la Base de Datos siempre y cuando no tenga movimientos asociados.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Grabar 
         Enabled         =   0   'False
         Height          =   615
         Left            =   4080
         Picture         =   "Dat_Cli.frx":C514
         Style           =   1  'Graphical
         TabIndex        =   16
         ToolTipText     =   "Grabar los Datos del Cliente.-"
         Top             =   240
         Width           =   975
      End
      Begin VB.CommandButton Observaciones 
         Enabled         =   0   'False
         Height          =   615
         Left            =   120
         Picture         =   "Dat_Cli.frx":C81E
         Style           =   1  'Graphical
         TabIndex        =   21
         ToolTipText     =   "Mostrar el Campo Comentarios.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Dat_Cli"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Borrar_Click()
    If MsgBox("Desea Borrar �ste registro. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
        If Val(Permiso_Alta) = 1 Then
            qy = "DELETE FROM Cliente WHERE Id_Cliente = " & Trim(Str(Val(Cliente.Text)))
            Db.Execute (qy)
        Else
            MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
        End If
    End If
    
    Salir_Click
End Sub

Private Sub Buscar_Clientes_Click()
    Clientes_Encontrados.Visible = True
    If Clientes_Encontrados.ListCount = 0 Then
        MousePointer = 11
        qy = "SELECT * FROM Cliente ORDER BY Nombre"
        AbreRs
        
        While Not Rs.EOF
            Clientes_Encontrados.AddItem Trim(Rs.Fields("Nombre")) + Space(30 - Len(Trim(Rs.Fields("Nombre")))) & " " & Trim(Rs.Fields("Id_Cliente"))
            Rs.MoveNext
        Wend
        MousePointer = 0
    End If
    Clientes_Encontrados.SetFocus
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Categoria_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Categoria_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Cliente_GotFocus()
    Cliente.Text = Trim(Cliente.Text)
    Cliente.SelStart = 0
    Cliente.SelText = ""
    Cliente.SelLength = Len(Cliente.Text)
End Sub

Private Sub Cliente_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then
        KeyAscii = 0:
        If Val(Cliente.Text) = 0 Then
            qy = "SELECT MAX(Id_Cliente) FROM Cliente"
            AbreRs
            
            Cliente.Text = 1
            If Not Rs.Fields(0) = "" Then Cliente.Text = Rs.Fields(0) + 1
        Else
            Leer_Cliente
        End If
    End If
End Sub

Private Sub Leer_Cliente()
    If Val(Cliente.Text) > 0 Then
        qy = "SELECT * FROM Cliente WHERE Id_Cliente = " & Trim(Str(Val(Cliente.Text)))
        AbreRs
        
        If Rs.EOF Then
            If MsgBox("El Cliente es inexistente, desea incorporarlo ahora. ?", vbQuestion + vbYesNo, "Atenci�n.!") = vbYes Then
                If Val(Permiso_Alta) = 1 Then
                    Grabar.Tag = "Alta"
                    Encontrado.Text = "INGRESANDO UN NUEVO CLIENTE."
                    Fecha.Text = Fecha_Fiscal
                    
                    Marco_Datos.Enabled = True
                    Id_Cliente.Enabled = False
                    Grabar.Enabled = True
                    Observaciones.Enabled = True
                    
                    Nombre.SetFocus
                Else
                    MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
                    Borrar_Campo
                End If
            Else
                Borrar_Campo
            End If
        Else
            Grabar.Tag = "Modificaci�n"
            If Val(Permiso_Cons) = 1 Then
                Mostrar_Cliente
                Nombre.SetFocus
            Else
                MsgBox "Usted no est� autorizado para estos procesos.", vbCritical, "Atenci�n.!"
                Borrar_Campo
            End If
        End If
    Else
        Cliente.Text = ""
        Cliente.SetFocus
    End If
End Sub

Private Sub Mostrar_Cliente()
    Nombre.Text = Rs.Fields("Nombre")
    Encontrado.Text = Nombre.Text
    Domicilio.Text = Rs.Fields("Domicilio")
    Numero.Text = Rs.Fields("Numero")
    Piso.Text = Rs.Fields("Piso")
    Depto.Text = Rs.Fields("Depto")
    Codigo_Postal.Text = Rs.Fields("Codigo_Postal")
    Telefono.Text = Rs.Fields("Telefono")
    email.Text = Rs.Fields("Email")
    
    If Rs.Fields("Condicion_Iva") = "CF" Then
        Condicion_Iva.ListIndex = 0
    ElseIf Rs.Fields("Condicion_Iva") = "RI" Then
        Condicion_Iva.ListIndex = 1
    ElseIf Rs.Fields("Condicion_Iva") = "NI" Then
        Condicion_Iva.ListIndex = 2
    ElseIf Rs.Fields("Condicion_Iva") = "MT" Then
        Condicion_Iva.ListIndex = 3
    ElseIf Rs.Fields("Condicion_Iva") = "ET" Then
        Condicion_Iva.ListIndex = 4
    ElseIf Rs.Fields("Condicion_Iva") = "NC" Then
        Condicion_Iva.ListIndex = 5
    End If
    
    Categoria.ListIndex = Val(Rs.Fields("Categoria"))
    
    Tipo_Documento.Text = Rs.Fields("Tipo_Documento")
    Leer_Tipo_Documento
    
    Nro_Documento.Text = Rs.Fields("Nro")
    Forma_Pago.ListIndex = Val(Rs.Fields("Forma_Pago"))
    Estado.ListIndex = Val(Rs.Fields("Estado"))
    
    Saldo_Autorizado.Text = Formateado(Str(Val(Rs.Fields("Saldo_Autorizado"))), 2, 10, " ", False)
    Fecha.Text = Rs.Fields("Fecha_Alta")
    
    If Val(Rs.Fields("Codigo_Postal")) > 0 Then Leer_Localidad
    
    Leer_Saldo
    
    Marco_Datos.Enabled = True
    Id_Cliente.Enabled = False
    Borrar.Enabled = True
    Grabar.Enabled = True
    Observaciones.Enabled = True
End Sub

Private Sub Leer_Tipo_Documento()
    Select Case Trim(Val(Mid(Tipo_Documento.Text, 1, 2)))
       Case Is = "0"
          Tipo_Documento.Text = "00 - C.U.I.T."
       Case Is = "1"
          Tipo_Documento.Text = "01 - C.U.I.L."
       Case Is = "2"
          Tipo_Documento.Text = "02 - D.N.I.  "
       Case Is = "3"
          Tipo_Documento.Text = "03 - L.E.    "
       Case Is = "4"
          Tipo_Documento.Text = "04 - L.C.    "
       Case Is = "5"
          Tipo_Documento.Text = "05 - CI. CAP."
    End Select
End Sub

Private Sub Borrar_Campo()
    Encontrado.Text = ""
    Nombre.Text = ""
    Domicilio.Text = ""
    Piso.Text = ""
    Numero.Text = ""
    Depto.Text = ""
    Codigo_Postal.Text = ""
    Localidad.Text = ""
    Provincia.Text = ""
    Telefono.Text = ""
    email.Text = ""
    Condicion_Iva.Text = ""
    Tipo_Documento.Text = ""
    Numero.Text = ""
    Forma_Pago.Text = ""
    Estado.Text = ""
    Categoria.Text = ""
    Saldo_Autorizado.Text = ""
    Saldo.Text = ""
    Fecha.Text = ""
    Nro_Documento.Text = ""
    Memo.Text = ""
    Memo.Visible = False
    
    Grabar.Enabled = False
    Borrar.Enabled = False
    Observaciones.Enabled = False
    Marco_Datos.Enabled = False
    Id_Cliente.Enabled = True
    Salir.Cancel = True
    
    Cliente.Text = ""
    Cliente.SetFocus
End Sub

Private Sub Clientes_Encontrados_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: Clientes_Encontrados_LostFocus
End Sub

Private Sub Clientes_Encontrados_LostFocus()
    Cliente.Text = Mid(Clientes_Encontrados.List(Clientes_Encontrados.ListIndex), 32)
    Clientes_Encontrados.Visible = False
    Leer_Cliente
End Sub

Private Sub Codigo_Postal_GotFocus()
    Codigo_Postal.Text = Trim(Codigo_Postal.Text)
    Codigo_Postal.SelStart = 0
    Codigo_Postal.SelText = ""
    Codigo_Postal.SelLength = Len(Codigo_Postal.Text)
End Sub

Private Sub Codigo_Postal_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Codigo_Postal_LostFocus()
    If Val(Codigo_Postal.Text) > 0 Then
        Leer_Localidad
    End If
End Sub

Private Sub Leer_Localidad()
    If Val(Codigo_Postal.Text) > 0 Then
        
        qy = "SELECT * FROM Localidad WHERE Id_Localidad = " & Trim(Str(Val(Codigo_Postal.Text)))
        AbreRs
        
        If Rs.EOF Then
            MsgBox "La Localidad es inexistente..."
            Codigo_Postal.Text = ""
            Localidad.Text = ""
            Provincia.Text = ""
            
            Codigo_Postal.SetFocus
        Else
            Localidad.Text = Rs.Fields("Localidad")
            Provincia.Text = Rs.Fields("Provincia")
        End If
    End If
End Sub

Private Sub Condicion_Iva_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Condicion_Iva_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Depto_GotFocus()
    Depto.Text = Trim(Depto.Text)
    Depto.SelStart = 0
    Depto.SelText = ""
    Depto.SelLength = Len(Depto.Text)
End Sub

Private Sub Depto_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Depto_LostFocus()
    Depto.Text = UCase(Depto.Text)
    Depto.Text = FiltroCaracter(Depto.Text)
End Sub

Private Sub Domicilio_GotFocus()
    Domicilio.SelStart = 0
    Domicilio.SelText = ""
    Domicilio.SelLength = Len(Domicilio.Text)
End Sub

Private Sub Domicilio_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Domicilio_LostFocus()
    Domicilio.Text = UCase(Domicilio.Text)
    Domicilio.Text = FiltroCaracter(Domicilio.Text)
End Sub

Private Sub Email_GotFocus()
    email.Text = Trim(email.Text)
    email.SelStart = 0
    email.SelText = ""
    email.SelLength = Len(email.Text)
End Sub

Private Sub Email_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Estado_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Estado_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Actualizaci�n del Registro de Clientes.-"
End Sub

Private Sub Form_Load()
    Dim i As Long
    
    Me.Top = (Screen.Height - Me.Height) / 4
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
    
    For i = 1 To 6
        Condicion_Iva.AddItem Tipo_Iva(i)
        
        If i <= 3 Then
            Categoria.AddItem Cliente_Categ(i - 1)
        End If
    Next
    
    Forma_Pago.AddItem "00 - CONTADO EFECTIVO    "
    Forma_Pago.AddItem "01 - CUENTA CORRIENTE    "
    Forma_Pago.AddItem "02 - CHEQUES             "
    Forma_Pago.AddItem "03 - TARGETAS DE CR�DITO "
    
    Tipo_Documento.AddItem "00 - C.U.I.T. "
    Tipo_Documento.AddItem "01 - C.U.I.L. "
    Tipo_Documento.AddItem "02 - D.N.I.   "
    Tipo_Documento.AddItem "03 - L.E.     "
    Tipo_Documento.AddItem "04 - L.C.     "
    Tipo_Documento.AddItem "05 - CI. CAP. "
    
    Estado.AddItem "00 - NO POSEE   "
    Estado.AddItem "01 - HABILITADA "
    Estado.AddItem "02 - SUSPENDIDA "
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Forma_Pago_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Forma_Pago_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Grabar_Click()
    If Grabar.Tag = "Alta" Then
        
        qy = "INSERT INTO Cliente VALUES ('"
        qy = qy & Trim(Str(Val(Cliente.Text))) & "'"
        qy = qy & ", '" & Trim(Nombre.Text) & "'"
        qy = qy & ", '" & Trim(Domicilio.Text) & "'"
        qy = qy & ", '" & Trim(Val(Numero.Text)) & "'"
        qy = qy & ", '" & Trim(Val(Piso.Text)) & "'"
        qy = qy & ", '" & Trim(Depto.Text) & " '"
        qy = qy & ", '" & Trim(Str(Val(Codigo_Postal.Text))) & "'"
        qy = qy & ", '" & Trim(Telefono.Text) & "'"
        qy = qy & ", '" & Trim(email.Text) & "'"
        qy = qy & ", '" & Trim(UCase(Mid(Condicion_Iva.Text, 1, 2))) & "'"
        qy = qy & ", '" & Trim(Str(Val(Mid(Tipo_Documento.Text, 1, 2)))) & "'"
        qy = qy & ", '" & Trim(Nro_Documento.Text) & "'"
        qy = qy & ", '" & Trim(Str(Val(Mid(Forma_Pago.Text, 1, 2)))) & "'"
        qy = qy & ", '" & Trim(Str(Val(Mid(Estado.Text, 1, 2)))) & "'"
        qy = qy & ", " & Trim(Str(Val(Categoria.Text)))
        qy = qy & ", " & Trim(Str(Val(Saldo_Autorizado.Text))) & " "
        qy = qy & ", '" & Trim(Fecha.Text) & "'"
        qy = qy & ", '" & Trim(Memo.Text) & "')"
        Db.Execute (qy)
    Else
        
        qy = "UPDATE Cliente SET "
        qy = qy & "Nombre = '" & Trim(Nombre.Text) & "',"
        qy = qy & "Domicilio = '" & Trim(Domicilio.Text) & "',"
        qy = qy & "Numero = '" & Trim(Str(Val(Numero.Text))) & "',"
        qy = qy & "Piso = '" & Trim(Str(Val(Piso.Text))) & "',"
        qy = qy & "Depto = '" & Trim(Depto.Text) & "',"
        qy = qy & "Codigo_Postal = '" & Trim(Str(Val(Codigo_Postal.Text))) & "',"
        qy = qy & "Telefono = '" & Trim(Telefono.Text) & "',"
        qy = qy & "Email = '" & Trim(email.Text) & "',"
        qy = qy & "Condicion_Iva = '" & Trim(UCase(Mid(Condicion_Iva.Text, 1, 2))) & "',"
        qy = qy & "Tipo_Documento = '" & Trim(Str(Val(Mid(Tipo_Documento.Text, 1, 2)))) & "',"
        qy = qy & "Nro = '" & Trim(Nro_Documento.Text) & "',"
        qy = qy & "Forma_Pago = '" & Trim(Str(Val(Mid(Forma_Pago.Text, 1, 2)))) & "',"
        qy = qy & "Estado = '" & Trim(Str(Val(Mid(Estado.Text, 1, 2)))) & "',"
        qy = qy & "Categoria = " & Trim(Str(Val(Categoria.Text))) & ", "
        qy = qy & "Saldo_Autorizado = " & Trim(Str(Val(Saldo_Autorizado.Text))) & ", "
        qy = qy & "Observaciones = '" & Trim(Memo.Text) & "' "
        qy = qy & "WHERE Id_Cliente = " & Trim(Str(Val(Cliente.Text)))
        Db.Execute (qy)
    End If
        
    Salir_Click
End Sub

Private Sub Nombre_GotFocus()
    Nombre.SelStart = 0
    Nombre.SelText = ""
    Nombre.SelLength = Len(Nombre.Text)
End Sub

Private Sub Nombre_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nombre_LostFocus()
    Nombre.Text = UCase(Nombre.Text)
    Nombre.Text = FiltroCaracter(Nombre.Text)
End Sub

Private Sub Nro_Documento_GotFocus()
    If Trim(UCase(Mid(Condicion_Iva.Text, 1, 2))) = "CF" And Val(Nro_Documento.Text) = 0 Then Nro_Documento.Text = "00-00000000-0"
    
    Nro_Documento.Text = Trim(Nro_Documento.Text)
    Nro_Documento.SelStart = 0
    Nro_Documento.SelText = ""
    Nro_Documento.SelLength = Len(Nro_Documento.Text)
End Sub

Private Sub Nro_Documento_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Nro_Documento_LostFocus()
    If Mid(Tipo_Documento.Text, 1, 2) = "00" Then 'CUIT
        Nro_Documento.Text = ValidarCuit(Nro_Documento.Text)
    End If
    
    If Nro_Documento.Text = "error" Then
        MsgBox "El C.U.I.T. Ingresado no es correcto, verifique e ingrese nuevamente...", vbInformation, "Atenci�n.!"
        Nro_Documento.Text = ""
        
        Tipo_Documento.SetFocus
    End If
End Sub

Private Sub Numero_GotFocus()
    Numero.Text = Trim(Numero.Text)
    Numero.SelStart = 0
    Numero.SelText = ""
    Numero.SelLength = Len(Numero.Text)
End Sub

Private Sub Numero_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Observaciones_Click()
    If Memo.Visible = True Then
        Marco_Datos.Enabled = True
        Memo.Visible = False
        Salir.Cancel = True
        Grabar.SetFocus
    Else
        Marco_Datos.Enabled = False
        Memo.Visible = True
        Observaciones.Cancel = True
        Memo.SetFocus
    End If
End Sub

Private Sub Piso_GotFocus()
    Piso.Text = Trim(Piso.Text)
    Piso.SelStart = 0
    Piso.SelText = ""
    Piso.SelLength = Len(Piso.Text)
End Sub

Private Sub Piso_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Saldo_Autorizado_GotFocus()
    Saldo_Autorizado.Text = Trim(Saldo_Autorizado.Text)
    Saldo_Autorizado.SelStart = 0
    Saldo_Autorizado.SelText = ""
    Saldo_Autorizado.SelLength = Len(Saldo_Autorizado.Text)
End Sub

Private Sub Saldo_Autorizado_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Saldo_Autorizado_LostFocus()
    Saldo_Autorizado.Text = Formateado(Str(Val(Saldo_Autorizado.Text)), 2, 10, " ", False)
End Sub

Private Sub Salir_Click()
    If Cliente.Text = "" Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Telefono_GotFocus()
    Telefono.Text = Trim(Telefono.Text)
    Telefono.SelStart = 0
    Telefono.SelText = ""
    Telefono.SelLength = Len(Telefono.Text)
End Sub

Private Sub Telefono_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Tipo_Documento_GotFocus()
    If Desplegar_Combos = True Then SendKeys "{F4}"
End Sub

Private Sub Tipo_Documento_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Leer_Saldo()
    If Val(Cliente.Text) > 0 Then
        qy = "SELECT SUM(Debe - Haber) FROM CtaCte_Cliente WHERE Id_Cuenta = " & Trim(Str(Val(Cliente.Text)))
        AbreRs
        
        If Not Rs.Fields(0) = "" Then Saldo.Text = Rs.Fields(0)
        Saldo.Text = Formateado(Str(Val(Saldo.Text)), 2, 10, " ", False)
    End If
End Sub
