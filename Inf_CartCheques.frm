VERSION 5.00
Begin VB.Form Inf_CartCheques 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Consultas a Cartera de Cheques.-"
   ClientHeight    =   7095
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   11055
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MDIChild        =   -1  'True
   ScaleHeight     =   7095
   ScaleWidth      =   11055
   ShowInTaskbar   =   0   'False
   Begin VB.Frame Marco_Consulta 
      Height          =   1095
      Left            =   0
      TabIndex        =   9
      Top             =   0
      Width           =   11055
      Begin VB.OptionButton Propios 
         Caption         =   "De nuestra Firma.-"
         Enabled         =   0   'False
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   2
         Top             =   720
         Width           =   1815
      End
      Begin VB.OptionButton Pagos 
         Caption         =   "Pagos Realizados.-"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   1
         Top             =   480
         Width           =   1935
      End
      Begin VB.OptionButton Cartera 
         Caption         =   "Cheques en Cartera.-"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   0
         Top             =   240
         Value           =   -1  'True
         Width           =   2055
      End
      Begin VB.TextBox Hasta 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9600
         TabIndex        =   4
         Top             =   600
         Width           =   1335
      End
      Begin VB.TextBox Desde 
         BeginProperty Font 
            Name            =   "Courier"
            Size            =   9.75
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   285
         Left            =   9600
         TabIndex        =   3
         Top             =   240
         Width           =   1335
      End
      Begin VB.Label Label3 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de cobro Hasta:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   6720
         TabIndex        =   14
         Top             =   600
         Width           =   2775
      End
      Begin VB.Label Label2 
         Alignment       =   1  'Right Justify
         Caption         =   "Fecha de cobro Desde:"
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   7200
         TabIndex        =   13
         Top             =   240
         Width           =   2295
      End
   End
   Begin VB.Frame Marco_Lista 
      Enabled         =   0   'False
      Height          =   5055
      Left            =   0
      TabIndex        =   10
      Top             =   1080
      Width           =   11055
      Begin VB.CommandButton Command7 
         Caption         =   "Importe"
         Height          =   255
         Left            =   9600
         TabIndex        =   21
         Top             =   120
         Width           =   1335
      End
      Begin VB.CommandButton Command6 
         Caption         =   "Fecha Cob."
         Height          =   255
         Left            =   8640
         TabIndex        =   20
         Top             =   120
         Width           =   975
      End
      Begin VB.CommandButton Command5 
         Caption         =   "D�as"
         Height          =   255
         Left            =   8160
         TabIndex        =   19
         Top             =   120
         Width           =   495
      End
      Begin VB.CommandButton Command4 
         Caption         =   "Emisi�n"
         Height          =   255
         Left            =   7320
         TabIndex        =   18
         Top             =   120
         Width           =   855
      End
      Begin VB.CommandButton Command3 
         Caption         =   "Banco"
         Height          =   255
         Left            =   4560
         TabIndex        =   17
         Top             =   120
         Width           =   2775
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Titular"
         Height          =   255
         Left            =   1320
         TabIndex        =   16
         Top             =   120
         Width           =   3255
      End
      Begin VB.CommandButton Command1 
         Caption         =   "Nro. Cheque"
         Height          =   255
         Left            =   120
         TabIndex        =   15
         Top             =   120
         Width           =   1215
      End
      Begin VB.ListBox List1 
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Height          =   4560
         Left            =   120
         TabIndex        =   6
         Top             =   360
         Width           =   10815
      End
      Begin VB.Label Label1 
         Caption         =   "     N�mero Titular                        Banco                     Emisi�n D�as  Cobrar    Importe"
         BeginProperty Font 
            Name            =   "Courier New"
            Size            =   9
            Charset         =   0
            Weight          =   400
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00800000&
         Height          =   255
         Left            =   120
         TabIndex        =   12
         Top             =   120
         Width           =   10815
      End
   End
   Begin VB.Frame Botonera 
      Height          =   975
      Left            =   0
      TabIndex        =   11
      Top             =   6120
      Width           =   11055
      Begin VB.CommandButton Excel 
         Height          =   615
         Left            =   8400
         Picture         =   "Inf_CartCheques.frx":0000
         Style           =   1  'Graphical
         TabIndex        =   22
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Salir 
         Cancel          =   -1  'True
         Height          =   615
         Left            =   9840
         Picture         =   "Inf_CartCheques.frx":0972
         Style           =   1  'Graphical
         TabIndex        =   8
         ToolTipText     =   "Cancelar - Salir.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Imprimir 
         Enabled         =   0   'False
         Height          =   615
         Left            =   7320
         Picture         =   "Inf_CartCheques.frx":6BFC
         Style           =   1  'Graphical
         TabIndex        =   7
         ToolTipText     =   "Imprimir los Resultados de la Consulta.-"
         Top             =   240
         Width           =   1095
      End
      Begin VB.CommandButton Confirma 
         Height          =   615
         Left            =   120
         Picture         =   "Inf_CartCheques.frx":C80E
         Style           =   1  'Graphical
         TabIndex        =   5
         ToolTipText     =   "Confirma la Carga de Datos.-"
         Top             =   240
         Width           =   1095
      End
   End
End
Attribute VB_Name = "Inf_CartCheques"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub Cartera_Click()
    If Pagos.Value = True Then
        Propios.Enabled = True
    Else
        Propios.Enabled = False
    End If
End Sub

Private Sub Cartera_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Confirma_Click()
    If Val(Desde.Text) > 0 And Val(Hasta.Text) > 0 Then
        List1.Clear
        qy = "SELECT * FROM Cheque, Banco WHERE (Fecha_Cobrar BETWEEN CONVERT(DATETIME, '" & Format(Desde.Text, "YYYY-MM-DD") & " 00:00:00', 102) AND CONVERT(DATETIME, '" & Format(Hasta.Text, "YYYY-MM-DD") & " 23:59:59.99', 102)) "
        If Cartera.Value = True Then qy = qy & "AND Tipo_Cuenta = 'C' "
        If Pagos.Value = True Then qy = qy & "AND Tipo_Cuenta = 'P' "
        If Propios.Value = True Then qy = qy & "AND Nuestra_Firma = 1 "
        qy = qy & "AND Empresa = " & Trim(Str(Val(Id_Empresa))) & " "
        qy = qy & "AND Banco.Id_Banco = Cheque.Banco "
        qy = qy & "ORDER BY Fecha_Cobrar"
        AbreRs
        
        While Not Rs.EOF
            Txt = Formateado(Str(Val(Rs.Fields("Id_Cheque"))), 0, 10, " ", False) & " "
            Txt = Txt & Trim(Rs.Fields("Titular")) + Space(30 - Len(Trim(Rs.Fields("Titular")))) & " "
            Txt = Txt & Trim(Mid(Rs.Fields("Denominacion"), 1, 25)) + Space(25 - Len(Trim(Mid(Rs.Fields("Denominacion"), 1, 25)))) & " "
            Txt = Txt & Format(Rs.Fields("Fecha_Emision"), "dd/mm/yy") & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Dias_Cobro"))), 0, 3, " ", False) & " "
            Txt = Txt & Format(Rs.Fields("Fecha_Cobrar"), "dd/mm/yy") & " "
            Txt = Txt & Formateado(Str(Val(Rs.Fields("Importe"))), 2, 10, " ", False)
            
            List1.AddItem Txt
            Rs.MoveNext
        Wend
        
        Marco_Lista.Enabled = True
        List1.SetFocus
    Else
        Desde.SetFocus
    End If
End Sub

Private Sub Desde_GotFocus()
    If Trim(Desde.Text) = "" Then Desde.Text = Fecha_Fiscal
    
    Desde.Text = Trim(Desde.Text)
    Desde.SelStart = 0
    Desde.SelText = ""
    Desde.SelLength = Len(Desde.Text)
End Sub

Private Sub Desde_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Desde_LostFocus()
    Desde.Text = ValidarFecha(Desde.Text)
End Sub

Private Sub Form_Activate()
    Menu.Estado.Panels(2).Text = "Movimientos de con Cheques.-"
End Sub

Private Sub Form_Load()
    Me.Top = (Screen.Height - Me.Height) / 9
    Me.Left = (Screen.Width - Me.Width) / 2
    Abrir_Base_Datos
End Sub

Private Sub Form_Unload(Cancel As Integer)
    Menu.Estado.Panels(2).Text = "Libre..."
End Sub

Private Sub Hasta_GotFocus()
    If Trim(Hasta.Text) = "" Then Hasta.Text = Fecha_Fiscal

    Hasta.Text = Trim(Hasta.Text)
    Hasta.SelStart = 0
    Hasta.SelText = ""
    Hasta.SelLength = Len(Hasta.Text)
End Sub

Private Sub Hasta_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Hasta_LostFocus()
    Hasta.Text = ValidarFecha(Hasta.Text)
End Sub

Private Sub Pagos_Click()
    If Pagos.Value = True Then
        Propios.Enabled = True
    Else
        Propios.Enabled = True
    End If
End Sub

Private Sub Pagos_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Propios_KeyPress(KeyAscii As Integer)
    If KeyAscii = 13 Then KeyAscii = 0: SendKeys "{TAB}"
End Sub

Private Sub Salir_Click()
    If Desde.Text = "" And Hasta.Text = "" And List1.ListCount = 0 Then
        Unload Me
    Else
        Borrar_Campo
    End If
End Sub

Private Sub Borrar_Campo()
    List1.Clear
    Desde.Text = ""
    Hasta.Text = ""
    Marco_Lista.Enabled = False
    Imprimir.Enabled = False
    
    Cartera.SetFocus
End Sub
